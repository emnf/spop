<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTValidasiFinal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_validasi_final', function (Blueprint $table) {
            $table->increments('t_id_validasi_final');
            $table->foreignId('t_id_pelayanan')->references('t_id_pelayanan')->on('t_pelayanan');
            $table->date('t_tgl_validasi_final');
            $table->smallInteger('t_no_validasi_final');
            $table->foreignId('t_id_status_validasi_final')->references('s_id_status_validasi_final')->on('s_status_validasi_final');
            $table->foreignId('created_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_validasi_final');
    }
}
