<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTValidasiTglBatasVerlap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_validasi', function (Blueprint $table) {
            $table->date('t_tgl_akhir_verlap')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_validasi', function (Blueprint $table) {
            $table->dropColumn('t_tgl_akhir_verlap');
        });
    }
}
