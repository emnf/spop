<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSAlurPelayanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_alur_pelayanan', function (Blueprint $table) {
            $table->increments('s_id_alur');
            $table->string('s_judul_alur', 50);
            $table->string('s_deskripsi_alur', 255);
            $table->string('s_langkah_alur', 30);
            $table->string('s_user_alur', 30);
            $table->string('s_icon_alur', 30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_alur_pelayanan');
    }
}
