<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_pegawai', function (Blueprint $table) {
            $table->increments('s_id_pegawai');
            $table->string('s_nama_pegawai')->nullable();
            $table->string('s_jabatan_pegawai')->nullable();
			$table->string('s_pangkat_pegawai')->nullable();
			$table->string('s_nip_pegawai')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_pegawai');
    }
};
