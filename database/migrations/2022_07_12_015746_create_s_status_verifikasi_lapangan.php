<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSStatusVerifikasiLapangan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_status_verifikasi_lapangan', function (Blueprint $table) {
            $table->increments('s_id_status_verifikasi_lapangan');
            $table->string('s_nama_status_verifikasi_lapangan', 30);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_status_verifikasi_lapangan');
    }
}
