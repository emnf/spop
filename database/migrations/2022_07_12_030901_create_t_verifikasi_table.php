<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTVerifikasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('t_verifikasi', function (Blueprint $table) {
            $table->increments('t_id_verifikasi');
            $table->integer('t_id_pelayanan')->nullable();
            $table->date('t_tgl_verifikasi')->nullable();
            $table->integer('t_no_verifikasi')->nullable();
            $table->foreignId('t_id_status_verifikasi')->references('s_id_status_verifikasi')->on('s_status_verifikasi');
            $table->string('t_keterangan_verifikasi')->nullable();
            $table->integer('created_by')->nullable();
            $table->string('t_checklist_persyaratan')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_verifikasi');
    }
}
