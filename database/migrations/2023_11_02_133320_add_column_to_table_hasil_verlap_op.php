<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToTableHasilVerlapOp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_hasil_verlap_op', function (Blueprint $table) {
            $table->string('t_kode_znt', 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_hasil_verlap_op', function (Blueprint $table) {
            $table->dropColumn('t_kode_znt');
        });
    }
}
