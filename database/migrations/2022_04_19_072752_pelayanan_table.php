<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_pelayanan', function (Blueprint $table) {
            $table->increments('t_id_pelayanan');
            $table->string('t_no_pelayanan')->nullable();
            $table->date('t_tgl_pelayanan');
            $table->foreignId('t_id_jenis_pelayanan')->references('s_id_jenis_pelayanan')->on('s_jenis_pelayanan');
            $table->string('t_nop', 18)->nullable();
            $table->string('t_nama_pemohon', 50)->nullable();
            $table->string('t_blokkav', 50)->nullable();
            $table->string('t_nik_pemohon', 18)->nullable();
            $table->string('t_jalan_pemohon', 30)->nullable();
            $table->string('t_rt_pemohon', 3)->nullable();
            $table->string('t_rw_pemohon', 3)->nullable();
            $table->string('t_kelurahan_pemohon', 30)->nullable();
            $table->string('t_kecamatan_pemohon', 30)->nullable();
            $table->string('t_kabupaten_pemohon', 30)->nullable();
            $table->string('t_kode_pos_pemohon')->nullable();
            $table->string('t_no_hp_pemohon')->nullable();
            $table->string('t_email_pemohon')->nullable();
            $table->string('t_keterangan', 255)->nullable();
            $table->foreignId('t_id_jenis_pajak')->references('s_id_jenis_pajak')->on('s_jenis_pajak');
            $table->string('t_tahun_pajak')->nullable();
            $table->integer('t_nourut')->nullable();
            $table->integer('t_besar_pengurangan')->nullable();
            $table->string('t_multiple_tahun', 100)->nullable();
            $table->date('t_tgl_permintaan')->nullable();
            $table->integer('t_jumlah_angsuran')->nullable();
            $table->char('t_tahun_pajak_kompensasi', 4)->nullable();
            $table->string('t_nop_kompensasi', 30)->nullable();
            $table->string('t_no_sk', 50)->nullable();
            $table->date('t_tgl_sk')->nullable();
            $table->foreignId('created_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_pelayanan');
    }
};
