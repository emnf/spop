<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTHasilVerlap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_hasil_verlap', function (Blueprint $table) {
            $table->increments('t_id');
            $table->foreignId('t_id_pelayanan')->references('t_id_pelayanan')->on('t_pelayanan');
            $table->integer('t_no');
            $table->date('t_tgl_penelitian');
            $table->text('t_tempat_penelitian');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_hasil_verlap');
    }
}
