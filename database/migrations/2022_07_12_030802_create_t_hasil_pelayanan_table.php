<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTHasilPelayananTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('t_hasil_pelayanan', function (Blueprint $table) {
            $table->increments('t_id_sk');
            $table->integer('t_id_pelayanan')->nullable();
            $table->integer('t_id_validasi')->nullable();
            $table->date('t_tgl_sk')->nullable();
            $table->string('t_no_sk')->nullable();
            $table->string('t_keterangan_sk')->nullable();
            $table->text('t_lokasi_file')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('t_no_urut')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_hasil_pelayanan');
    }
}
