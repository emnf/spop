<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableDetailBangunan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_detail_bangunan', function (Blueprint $tbl) {
            $tbl->integer('t_id_op')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_detail_bangunan', function (Blueprint $tbl) {
            $tbl->dropColumn('t_id_op');
        });
    }
}
