<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_pemda', function (Blueprint $table) {
            $table->increments('s_id_pemda');
            $table->string('s_nama_prov')->nullable();
            $table->string('s_nama_kabkota')->nullable();
			$table->string('s_nama_ibukota_kabkota')->nullable();
			$table->char('s_kode_provinsi', 2)->nullable();
			$table->char('s_kode_kabkot', 2)->nullable();
			$table->string('s_nama_instansi')->nullable();
			$table->string('s_nama_singkat_instansi')->nullable();
			$table->text('s_alamat_instansi')->nullable();
			$table->string('s_notelp_instansi', 15)->nullable();
			$table->text('s_logo')->nullable();
			$table->char('s_kode_pos', 5)->nullable();
            $table->string('s_latitude')->nullable();
            $table->string('s_longitude')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_pemda');
    }
};
