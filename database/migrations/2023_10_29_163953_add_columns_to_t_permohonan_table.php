<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToTPermohonanTable extends Migration
{
    public function up()
    {
        Schema::table('t_pelayanan', function (Blueprint $table) {
            $table->integer('t_id_spt_bphtb')->nullable();
            $table->string('t_kode_bayar_bphtb')->nullable();
            $table->integer('t_redirect_validasi_by')->nullable();
            $table->timestamp('t_redirect_validasi_tgl')->nullable();
            $table->integer('t_tipe_dari_bphtb')->nullable();
        });
    }

    public function down()
    {
        Schema::table('t_pelayanan', function (Blueprint $table) {
            $table->dropColumn(['t_id_spt_bphtb', 't_kode_bayar_bphtb', 't_redirect_validasi_by', 't_redirect_validasi_tgl', 't_tipe_dari_bphtb']);
        });
    }
}
