<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTVerifikasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_verifikasi', function (Blueprint $table) {
            $table->renameColumn('t_kelengkapan', 't_keabsahan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('t_verifikasi', function (Blueprint $table) {
        //     $table->delete('t_kelengkapan', 't_keabsahan')->nullable();
        // });
    }
}
