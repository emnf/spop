<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTHasilVerlapWp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_hasil_verlap_wp', function (Blueprint $table) {
            $table->increments('t_id_wp');
            $table->foreignId('t_id_pelayanan')->references('t_id_pelayanan')->on('t_pelayanan');
            $table->string('t_nop',18)->nullable();
            $table->string('t_nama_wp',50)->nullable();
            $table->string('t_nik_wp',16)->nullable();
            $table->string('t_jalan_wp',30)->nullable();
            $table->string('t_rt_wp',3)->nullable();
            $table->string('t_rw_wp',3)->nullable();
            $table->string('t_kelurahan_wp',30)->nullable();
            $table->string('t_kecamatan_wp',30)->nullable();
            $table->string('t_kabupaten_wp',30)->nullable();
            $table->string('t_no_hp_wp')->nullable();
            $table->string('t_npwpd', 30)->nullable();
            $table->string('t_email', 50)->nullable();
            $table->foreignId('created_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_hasil_verlap_wp');
    }
}
