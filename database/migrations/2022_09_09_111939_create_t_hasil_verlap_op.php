<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTHasilVerlapOp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_hasil_verlap_op', function (Blueprint $table) {
            $table->increments('t_id_op');
            $table->foreignId('t_id_pelayanan')->references('t_id_pelayanan')->on('t_pelayanan');
            $table->string('kd_propinsi',2)->nullable();
            $table->string('kd_dati2',2)->nullable();
            $table->string('kd_kecamatan',3)->nullable();
            $table->string('kd_kelurahan',3)->nullable();
            $table->string('kd_blok',3)->nullable();
            $table->string('no_urut',4)->nullable();
            $table->string('kd_jns_op',1)->nullable();
            $table->string('t_jalan_op', 50)->nullable();
            $table->string('t_blok_kav', 50)->nullable();
            $table->string('t_rt_op',3)->nullable();
            $table->string('t_rw_op',3)->nullable();
            $table->string('t_kelurahan_op',30)->nullable();
            $table->string('t_kecamatan_op',30)->nullable();
            $table->integer('t_luas_tanah');
            $table->integer('t_luas_bangunan')->nullable();
            $table->char('t_jenis_tanah', 2)->nullable();
            $table->char('t_kode_lookup_item', 2)->nullable();
            $table->string('t_latitude', 30)->nullable();
            $table->string('t_longitude', 30)->nullable();
            $table->foreignId('created_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_hasil_verlap_op');
    }
}
