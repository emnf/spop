<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddKolomHisPbbTerhutangPengurangYgHarusDibayar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_pelayanan', function (Blueprint $table) {
            $table->integer('t_his_pbb_terhutang')->nullable();
            $table->integer('t_his_faktor_pengurang')->nullable();
            $table->integer('t_his_pbb_yg_harus_dibayar')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
