<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSDokumenPendukung extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_dokumen_pendukung', function (Blueprint $table) {
            $table->increments('s_id_dokumen_pendukung');
            $table->foreignId('s_id_kategori_pajak')->references('s_id_jenis_pajak')->on('s_jenis_pajak');
            $table->foreignId('s_id_jenis_pelayanan')->references('s_id_jenis_pelayanan')->on('s_jenis_pelayanan');
            $table->string('s_nama_dokumen_pendukung');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_dokumen_pendukung');
    }
}
