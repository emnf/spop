<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_kategori_pajak', function (Blueprint $table) {
            $table->increments('s_id_kategori_pajak');
            $table->string('s_nama_kategori_pajak')->nullable();
        });

        Schema::create('s_jenis_pajak', function (Blueprint $table) {
            $table->increments('s_id_jenis_pajak');
            $table->string('s_nama_jenis_pajak')->nullable();
            $table->string('s_nama_singkat_pajak')->nullable();
            $table->foreignId('s_id_kategori_pajak')->references('s_id_kategori_pajak')->on('s_kategori_pajak');
            $table->boolean('s_active');
            $table->string('s_icon')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_jenis_pajak');
        Schema::dropIfExists('s_kategori_pajak');
    }
};
