<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTRegisterUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_register_user', function (Blueprint $table) {
            $table->increments('t_id_register_user');
            $table->date('t_tgl_register_user');
            $table->string('t_nama_register_user');
            $table->char('t_no_hp_register_user', 13)->nullable();
            $table->string('t_email_register_user');
            $table->string('t_username_register_user');
            $table->string('t_password_register_user');
            $table->smallInteger('t_agree_term_register_user')->nullable();
            $table->text('t_token_register_user')->nullable();
            $table->smallInteger('t_id_status_register_user')->nullable();
            $table->char('t_nik_register_user', 16);
            $table->date('t_tgl_active_register_user')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_register_user');
    }
}
