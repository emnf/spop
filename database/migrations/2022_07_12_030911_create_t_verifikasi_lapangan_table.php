<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTVerifikasiLapanganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('t_verifikasi_lapangan', function (Blueprint $table) {
            $table->increments('t_id_verifikasi_lapangan');
            $table->integer('t_id_pelayanan')->nullable();
            $table->date('t_tgl_verifikasi_lapangan')->nullable();
            $table->integer('t_no_verifikasi_lapangan')->nullable();
            $table->integer('t_id_status_verifikasi_lapangan')->nullable();
            $table->text('t_keterangan_verifikasi_lapangan')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_verifikasi_lapangan');
    }
}
