<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('t_persyaratan', function (Blueprint $table) {
            $table->increments('t_id_persyaratan');
            $table->string('t_nama_persyaratan',50)->nullable();
            $table->string('t_lokasi_file')->nullable();
            $table->foreignId('s_id_persyaratan')->references('s_id_persyaratan')->on('s_persyaratan');
            $table->foreignId('t_id_pelayanan')->references('t_id_pelayanan')->on('t_pelayanan');
            $table->string('t_nama_file')->nullable();
            $table->foreignId('created_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_persyaratan');
    }
};
