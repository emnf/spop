<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TEsignPenetapanTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_esign_penetapan', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->string('t_nop');
            $table->foreignId('t_id_pejabat')->references('s_id_pegawai')->on('s_pegawai');
            $table->foreignId('created_by')->references('id')->on('users');
            $table->text('imgQr');
            $table->text('file');
            $table->text('signed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_esign_penetapan');
    }
}
