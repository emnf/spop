<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTOpLama extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_op_lama', function (Blueprint $table) {
            $table->increments('t_id_op_lama');
            $table->foreignId('t_id_pelayanan')->references('t_id_pelayanan')->on('t_pelayanan');
            $table->char('kd_propinsi', 2);
            $table->char('kd_dati2', 2);
            $table->char('kd_kecamatan', 3);
            $table->char('kd_kelurahan', 3);
            $table->char('kd_blok', 3);
            $table->char('no_urut', 4);
            $table->char('kd_jns_op', 1);
            $table->string('t_jalan_op', 100);
            $table->char('t_rt_op', 3);
            $table->char('t_rw_op', 3);
            $table->string('t_kelurahan_op', 30);
            $table->string('t_kecamatan_op', 30);
            $table->integer('t_luas_tanah');
            $table->integer('t_luas_bangunan');
            $table->foreignId('created_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_op_lama');
    }
}
