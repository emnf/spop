<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJnsBumiColumnToTOpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_op', function (Blueprint $table) {
            $table->integer('jns_bumi')->nullable(); // Define the new column
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_op', function (Blueprint $table) {
            $table->dropColumn('jns_bumi'); // Drop the new column
        });
    }
}
