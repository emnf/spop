<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Ramsey\Uuid\Uuid;

class CreateBlankoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_blanko', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->integer('blanko');
            $table->text('thumbnail')->nullable();
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });

        DB::table('s_blanko')->insert([
            [
                'id' => 1,
                'uuid' => Uuid::uuid4()->getHex(1),
                'blanko' => 1,
                'thumbnail' => '
                
                ',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_blanko');
    }
}
