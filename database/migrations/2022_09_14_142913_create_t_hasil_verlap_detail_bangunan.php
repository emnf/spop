<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTHasilVerlapDetailBangunan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_hasil_verlap_detail_bangunan', function (Blueprint $table) {
            $table->increments('t_id_detail_bangunan');
            $table->foreignId('t_id_pelayanan')->references('t_id_pelayanan')->on('t_pelayanan');
            $table->string('t_jenis_penggunaan_bangunan',50)->nullable();
            $table->string('t_tahun_bangunan',10)->nullable();
            $table->string('t_tahun_renovasi',10)->nullable();
            $table->string('t_kondisi_bangunan',50)->nullable();
            $table->string('t_konstruksi',50)->nullable();
            $table->string('t_atap',50)->nullable();
            $table->string('t_dinding',50)->nullable();
            $table->string('t_lantai',50)->nullable();
            $table->string('t_langit_langit',50)->nullable();
            $table->string('t_ac_split',50)->nullable();
            $table->string('t_ac_window',50)->nullable();
            $table->string('t_panjang_pagar',50)->nullable();
            $table->string('t_bahan_pagar',50)->nullable();
            $table->string('t_jumlah_lantai',50)->nullable();
            $table->integer('t_listrik')->nullable();
            $table->integer('t_luas')->nullable();
            $table->integer('t_no_urut_bangunan')->nullable();
            $table->foreignId('created_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_hasil_verlap_detail_bangunan');
    }
}
