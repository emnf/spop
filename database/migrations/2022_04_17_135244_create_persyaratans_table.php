<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_persyaratan', function (Blueprint $table) {
            $table->increments('s_id_persyaratan');
            $table->foreignId('s_id_jenis_pelayanan')->references('s_id_jenis_pelayanan')->on('s_jenis_pelayanan');
            $table->string('s_nama_persyaratan')->nullable();
            $table->boolean('s_is_optional')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_persyaratan');
    }
};
