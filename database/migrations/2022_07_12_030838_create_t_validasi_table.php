<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTValidasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('t_validasi', function (Blueprint $table) {
            $table->increments('t_id_validasi');
            $table->integer('t_id_pelayanan')->nullable();
            $table->date('t_tgl_validasi')->nullable();
            $table->integer('t_no_validasi')->nullable();
            $table->integer('t_id_status_validasi')->nullable();
            $table->string('t_keterangan_validasi')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_validasi');
    }
}
