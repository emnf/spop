<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenandatanganPenetapanTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tte_penetapan', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->integer('idpenetapan');
            $table->foreignId('t_id_pejabat')->references('s_id_pegawai')->on('s_pegawai');
            $table->foreignId('created_by')->references('id')->on('users');
            $table->text('file');
            $table->text('signed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tte_penetapan');
    }
}
