<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterVerifikasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_verifikasi', function (Blueprint $table) {
            $table->integer('t_kejelasan')->nullable();
            $table->integer('t_kebenaran')->nullable();
            $table->integer('t_kelengkapan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('t_verifikasi', function (Blueprint $table) {
        //     $table->integer('t_kejelasan')->nullable();
        //     $table->integer('t_kebenaran')->nullable();
        //     $table->integer('t_kelengkapan')->nullable();
        // });
    }
}
