<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PenetapanTerseleksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_penetapan_terseleksi', function (Blueprint $table) {
            $table->id();
            $table->integer('created_by');
            $table->string('nip_perekam');
            $table->string('kd_propinsi');
            $table->string('kd_dati2');
            $table->string('kd_kecamatan');
            $table->string('kd_kelurahan');
            $table->string('kd_blok');
            $table->string('no_urut');
            $table->string('kd_jns_op');
            $table->string('thn_pajak_sppt');
            $table->string('t_id_jns_data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_penetapan_terseleksi');
    }
}
