<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTHasilVerlapOpBaru extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_hasil_verlap_op_baru', function (Blueprint $table) {
            $table->increments('t_id');
            $table->foreignId('t_id_pelayanan')->references('t_id_pelayanan')->on('t_pelayanan');
            $table->string('t_no_shm')->nullable();
            $table->date('t_tgl_shm');
            $table->integer('t_luas_tanah_shm');
            $table->string('t_atas_nama_shm');
            $table->string('t_nop_terdekat', 24);
            $table->char('t_kode_blok', 3);
            $table->char('t_kode_znt', 10);
            $table->char('t_kelas_tanah', 5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_hasil_verlap_op_baru');
    }
}
