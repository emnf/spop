<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTWpLama extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_wp_lama', function (Blueprint $table) {
            $table->increments('t_id_wp_lama');
            $table->foreignId('t_id_pelayanan')->references('t_id_pelayanan')->on('t_pelayanan');
            $table->string('t_nik_wp', 18)->nullable();
            $table->string('t_nama_wp');
            $table->string('t_jalan_wp',30);
            $table->string('t_rt_wp',3)->nullable();
            $table->string('t_rw_wp',3)->nullable();
            $table->string('t_kelurahan_wp',30)->nullable();
            $table->string('t_kecamatan_wp',30)->nullable();
            $table->string('t_kabupaten_wp',30)->nullable();
            $table->string('t_no_hp_wp')->nullable();
            $table->foreignId('created_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_wp_lama');
    }
}
