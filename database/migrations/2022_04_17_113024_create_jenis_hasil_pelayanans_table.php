<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_jenis_hasil_pelayanan', function (Blueprint $table) {
            $table->increments('s_id_hasil_pelayanan');
            $table->foreignId('s_id_jenis_pajak')->references('s_id_jenis_pajak')->on('s_jenis_pajak');
            $table->foreignId('s_id_jenis_pelayanan')->references('s_id_jenis_pelayanan')->on('s_jenis_pelayanan');
            $table->string('s_nama_hasil_pelayanan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_jenis_hasil_pelayanan');
    }
};
