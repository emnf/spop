<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTOpAddBlokKavling extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_op', function (Blueprint $table) {
            $table->string('t_blok_kav', 50)->nullable();
        });
        Schema::table('t_op_lama', function (Blueprint $table) {
            $table->string('t_blok_kav', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
