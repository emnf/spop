<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_jenis_pelayanan', function (Blueprint $table) {
            $table->increments('s_id_jenis_pelayanan');
            $table->string('s_nama_jenis_pelayanan')->nullable();
            $table->string('s_keterangan_jenis_pelayanan', 500)->nullable();
            $table->foreignId('s_id_jenis_pajak')->references('s_id_jenis_pajak')->on('s_jenis_pajak');
            $table->boolean('s_active');
            $table->integer('s_order')->unique();
            $table->smallInteger('s_waktu_pelayanan')->default(7)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_jenis_pelayanan');
    }
};
