<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SUserManual extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_user_manual', function (Blueprint $table) {
            $table->increments('s_id_usermanual');
            $table->string('s_nama_menu',30)->nullable();
            $table->string('s_nama_file',30)->nullable();
			$table->string('s_lokasi_file')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_user_manual');
    }
}
