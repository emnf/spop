<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTPelayananApproveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_pelayanan_approve', function (Blueprint $table) {
            $table->increments('t_id_pelayanan_approve');
            $table->integer('t_id_pelayanan')->nullable();
            $table->timestamp('t_tgl_perkiraan_selesai')->nullable();
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_pelayanan_approve');
    }
}
