<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToTPermohonanTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_pelayanan', function (Blueprint $table) {
            $table->integer('t_nomor_permohonan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_pelayanan', function (Blueprint $table) {
            $table->dropColumn(['t_nomor_permohonan']);
        });
    }
}
