<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTPenandatanganan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_penandatanganan', function (Blueprint $table) {
            $table->increments('t_id_penandatanganan');
            $table->foreignId('t_id_pelayanan')->references('t_id_pelayanan')->on('t_pelayanan');
            $table->foreignId('created_by')->references('id')->on('users');
            $table->date('t_tgl_penandatanganan');
            $table->foreignId('s_id_pegawai')->references('s_id_pegawai')->on('s_pegawai');
            $table->string('s_nama_pegawai');
            $table->string('s_jabatan_pegawai', 100);
            $table->string('s_nip_pegawai', 30);
            $table->char('s_nik', 16);
            $table->string('s_pangkat_pegawai', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_penandatanganan');
    }
}
