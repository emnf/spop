<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class EsignConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('esign_config', function (Blueprint $table) {
            $table->id();
            $table->string('url');
            $table->string('username');
            $table->string('password');
            $table->string('keterangan');
            $table->string('is_aktif');
            $table->timestamps();
        });

        DB::table('esign_config')->insert([
            [
                "url" => "http://180.214.248.15",
                "username" => "mpu_api",
                "password" => "irfanmpu",
                "keterangan" => "Development",
                "is_aktif" => 0
            ],
            [
                "url" => "https://esign.blorakab.go.id",
                "username" => "bppkad_bphtb",
                "password" => "bphtbbppkad",
                "keterangan" => "Production",
                "is_aktif" => 1
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('esign_config');
    }
}
