<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusValidasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('pgsql')->table('s_status_validasi')->insert([
            [
                's_id_status_validasi' => 1,
                's_nama_status_validasi' => 'Belum diproses'
            ],
            [
                's_id_status_validasi' => 2,
                's_nama_status_validasi' => 'Pending'
            ],
            [
                's_id_status_validasi' => 3,
                's_nama_status_validasi' => 'Ditolak'
            ],
            [
                's_id_status_validasi' => 4,
                's_nama_status_validasi' => 'Diterima'
            ],
            [
                's_id_status_validasi' => 5,
                's_nama_status_validasi' => 'Diajukan Verifikasi Lapangan'
            ],
            [
                's_id_status_validasi' => 6,
                's_nama_status_validasi' => 'Selesai Verifikasi Lapangan'
            ],
        ]);
    }
}
