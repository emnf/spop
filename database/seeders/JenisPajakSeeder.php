<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JenisPajakSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('s_kategori_pajak')->insert([
            [
				's_id_kategori_pajak' => 1,
                's_nama_kategori_pajak' => 'PDL'
            ],
            [
				's_id_kategori_pajak' => 2,
                's_nama_kategori_pajak' => 'PBB'
            ],
            [
				's_id_kategori_pajak' => 3,
                's_nama_kategori_pajak' => 'BPHTB'
            ]
        ]);

        DB::unprepared("ALTER SEQUENCE s_kategori_pajak_s_id_kategori_pajak_seq RESTART WITH 3;");

        DB::table('s_jenis_pajak')->insert([
            [
				's_id_jenis_pajak' => 1,
                's_nama_jenis_pajak' => 'Pajak Hotel',
                's_nama_singkat_pajak' => 'Pajak Hotel',
                's_id_kategori_pajak' => 1,
				's_active' => true,
				's_icon' => "fa fa-bed text-green",
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
				's_id_jenis_pajak' => 2,
                's_nama_jenis_pajak' => 'Pajak Restoran',
                's_nama_singkat_pajak' => 'Pajak Restoran',
                's_id_kategori_pajak' => 1,
				's_active' => true,
				's_icon' => "fa fa-utensils text-orange",
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
				's_id_jenis_pajak' => 3,
                's_nama_jenis_pajak' => 'Pajak Hiburan',
                's_nama_singkat_pajak' => 'Pajak Hiburan',
                's_id_kategori_pajak' => 1,
				's_active' => true,
				's_icon' => "fa fa-music text-info",
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
				's_id_jenis_pajak' => 4,
                's_nama_jenis_pajak' => 'Pajak Reklame',
                's_nama_singkat_pajak' => 'Pajak Reklame',
                's_id_kategori_pajak' => 1,
				's_active' => true,
				's_icon' => "fa fa-desktop text-maroon",
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
				's_id_jenis_pajak' => 5,
                's_nama_jenis_pajak' => 'Pajak Penerangan Jalan',
                's_nama_singkat_pajak' => 'PPJ',
                's_id_kategori_pajak' => 1,
				's_active' => true,
				's_icon' => "fa fa-plug text-yellow",
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
				's_id_jenis_pajak' => 6,
                's_nama_jenis_pajak' => 'Pajak Mineral Bukan Logam dan Batuan',
                's_nama_singkat_pajak' => 'Pajak MBLB',
                's_id_kategori_pajak' => 1,
				's_active' => true,
				's_icon' => "fa fa-anchor text-indigo",
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
				's_id_jenis_pajak' => 7,
                's_nama_jenis_pajak' => 'Pajak Parkir',
                's_nama_singkat_pajak' => 'Pajak Parkir',
                's_id_kategori_pajak' => 1,
				's_active' => true,
				's_icon' => "fa fa-car text-teal",
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
				's_id_jenis_pajak' => 8,
                's_nama_jenis_pajak' => 'Pajak Air Tanah',
                's_nama_singkat_pajak' => 'Pajak Air Tanah',
                's_id_kategori_pajak' => 1,
				's_active' => true,
				's_icon' => "fa fa-water text-primary",
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
				's_id_jenis_pajak' => 9,
                's_nama_jenis_pajak' => 'Pajak Sarang Burung Walet',
                's_nama_singkat_pajak' => 'Pajak Sarang Burung Walet',
                's_id_kategori_pajak' => 1,
				's_active' => true,
				's_icon' => "fa fa-dove text-pink",
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
				's_id_jenis_pajak' => 10,
                's_nama_jenis_pajak' => 'Pajak Bumi dan Bangunan',
                's_nama_singkat_pajak' => 'PBB',
                's_id_kategori_pajak' => 2,
				's_active' => true,
				's_icon' => "fa fa-city text-blue",
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
				's_id_jenis_pajak' => 11,
                's_nama_jenis_pajak' => 'Bea Perolehan Hak Atas Tanah dan Bangunan',
                's_nama_singkat_pajak' => 'BPHTB',
                's_id_kategori_pajak' => 3,
				's_active' => true,
				's_icon' => "fa fa-credit-card text-red",
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);

        DB::unprepared("ALTER SEQUENCE s_jenis_pajak_s_id_jenis_pajak_seq RESTART WITH 11;");
    }
}
