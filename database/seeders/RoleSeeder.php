<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = array(
            [
                'name' => 'Admin',
                'guard_name' => 'web'
            ],
            [
                'name' => 'Pemohon',
                'guard_name' => 'web'
            ],
            [
                'name' => 'Verifikator Berkas',
                'guard_name' => 'web'
            ],
            [
                'name' => 'Verifikator Lapangan',
                'guard_name' => 'web'
            ],
            [
                'name' => 'Validator',
                'guard_name' => 'web'
            ],
            [
                'name' => 'Perangkat Desa',
                'guard_name' => 'web'
            ],
            // [
            //     'name' => 'Validator Final',
            //     'guard_name' => 'web'
            // ],
            [
                'name' => 'Super Admin',
                'guard_name' => 'web'
            ],
            // [
            //     'name' => 'Operator',
            //     'guard_name' => 'web'
            // ]
        );

        foreach ($roles as $key => $role) {
            Role::create($role);
        }
    }
}
