<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusValidasiFinalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('pgsql')->table('s_status_validasi_final')->insert([
            [
                's_id_status_validasi_final' => 1,
                's_nama_status_validasi_final' => 'Belum',
            ],
            [
                's_id_status_validasi_final' => 2,
                's_nama_status_validasi_final' => 'Sudah',
            ],
            [
                's_id_status_validasi_final' => 3,
                's_nama_status_validasi_final' => 'Ditolak',
            ]
        ]);
    }
}
