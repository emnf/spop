<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class JenisPelayananSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('s_jenis_pelayanan')->insert([
            [
                "s_id_jenis_pelayanan" => 1,
                "s_nama_jenis_pelayanan" => "Objek Pajak Baru",
                "s_id_jenis_pajak" => 10,
                "s_active" => true,
                "s_keterangan_jenis_pelayanan" => "Permohonan untuk mendaftarkan objek pajaknya yang secara nyata mempunyai suatu hak atas bumi, dan/atau memperoleh manfaat atas bumi, dan/atau memiliki, menguasai, dan/atau memperoleh manfaat atas bangunan.",
                "s_order" => 1
            ],
            [
                "s_id_jenis_pelayanan" => 3,
                "s_nama_jenis_pelayanan" => "Mutasi Objek (Pecah Bidang) ",
                "s_id_jenis_pajak" => 10,
                "s_active" => true,
                "s_keterangan_jenis_pelayanan" => "Mutasi Objek (Pecah Bidang). Mutasi Objek adalah jika objek terjadi pecah bidang.",
                "s_order" => 3

            ],
            [
                "s_id_jenis_pelayanan" => 2,
                "s_nama_jenis_pelayanan" => "Mutasi Subjek",
                "s_id_jenis_pajak" => 10,
                "s_active" => true,
                "s_keterangan_jenis_pelayanan" => "Permohonan untuk melakukan mutasi subjek.",
                "s_order" => 2
            ],
            [
                "s_id_jenis_pelayanan" => 4,
                "s_nama_jenis_pelayanan" => "Pembetulan SPPT",
                "s_id_jenis_pajak" => 10,
                "s_active" => true,
                "s_keterangan_jenis_pelayanan" => "Permohonan pelayanan pembetulan  SPPT  karena salah nama, salah alamat, salah hitung atau salah penerapan undang-undang.",
                "s_order" => 4
            ],
            [
                "s_id_jenis_pelayanan" => 5,
                "s_nama_jenis_pelayanan" => "Pembatalan SPPT",
                "s_id_jenis_pajak" => 10,
                "s_active" => false,
                "s_keterangan_jenis_pelayanan" => "Permohonan pembatalan atas suatu ketetapan pajak oleh sebab tertentu, misalnya karena objek dobel atau objek tidak ditemukan.",
                "s_order" => 5
            ],
            [
                "s_id_jenis_pelayanan" => 6,
                "s_nama_jenis_pelayanan" => "Salinan SPPT",
                "s_id_jenis_pajak" => 10,
                "s_active" => false,
                "s_keterangan_jenis_pelayanan" => "Permohonan wajib pajak atas salinan SPPT untuk tahun pajak tertentu.",
                "s_order" => 6
            ],
            [
                "s_id_jenis_pelayanan" => 7,
                "s_nama_jenis_pelayanan" => "Keberatan Atas Pajak Terhutang",
                "s_id_jenis_pajak" => 10,
                "s_active" => false,
                "s_keterangan_jenis_pelayanan" => "Keberatan diajukan atas Surat Pemberitahuan Pajak Terutang (SPPT).",
                "s_order" => 7
            ],
            [
                "s_id_jenis_pelayanan" => 8,
                "s_nama_jenis_pelayanan" => "Pengurangan atas besarnya pajak terutang",
                "s_id_jenis_pajak" => 10,
                "s_active" => false,
                "s_keterangan_jenis_pelayanan" => "Pemberian keringanan/pengurangan atas pajak yang terutang karena kondisi tertentu.",
                "s_order" => 8
            ],
            [
                "s_id_jenis_pelayanan" => 9,
                "s_nama_jenis_pelayanan" => "Penentuan Kembali Tanggal Jatuh Tempo",
                "s_id_jenis_pajak" => 10,
                "s_active" => false,
                "s_keterangan_jenis_pelayanan" => "Permohonan penentuan kembali tanggal jatuh tempo pembayaran atas SPPT. Efek bila jatuh tempo dimundurkan adalah tidak dikenakan denda atas tagihan yang belum dibayarkan.",
                "s_order" => 9
            ],
            [
                "s_id_jenis_pelayanan" => 10,
                "s_nama_jenis_pelayanan" => "Pengurangan Ketetapan",
                "s_id_jenis_pajak" => 10,
                "s_active" => false,
                "s_keterangan_jenis_pelayanan" => "Permohonan pengurangan atas besarnya pajak terhutang.",
                "s_order" => 10
            ],
            [
                "s_id_jenis_pelayanan" => 11,
                "s_nama_jenis_pelayanan" => "Angsuran Ketetapan",
                "s_id_jenis_pajak" => 10,
                "s_active" => false,
                "s_keterangan_jenis_pelayanan" => "Permohonan Wajib Pajak untuk melakukan angsuran atas pajak yang dikenakan dikarenakan alasan tertentu.",
                "s_order" => 11
            ],
            [
                "s_id_jenis_pelayanan" => 12,
                "s_nama_jenis_pelayanan" => "Surat Keterangan Nilai Jual Objek Pajak (NJOP) PBB P2",
                "s_id_jenis_pajak" => 10,
                "s_active" => false,
                "s_keterangan_jenis_pelayanan" => "Permohonan Surat Keterangan NJOP (Nilai Jual Objek Pajak)",
                "s_order" => 12
            ],
            [
                "s_id_jenis_pelayanan" => 13,
                "s_nama_jenis_pelayanan" => "Pembetulan SK Keberatan",
                "s_id_jenis_pajak" => 10,
                "s_active" => false,
                "s_keterangan_jenis_pelayanan" => "Permohonan pembetulan Surat Keputusan Keberatan dari Wajib Pajak.",
                "s_order" => 13
            ],
            [
                "s_id_jenis_pelayanan" => 14,
                "s_nama_jenis_pelayanan" => "Keberatan Penunjukan Wajib Pajak",
                "s_id_jenis_pajak" => 10,
                "s_active" => false,
                "s_keterangan_jenis_pelayanan" => "Permohonan Wajib Pajak atas ketidaksetujuan seseorang dan / atau badan atas penunjukan sebagai Wajib Pajak. Pemohon tidak mengakui sebagai wajib pajak atas suatu objek pajak.",
                "s_order" => 14
            ],
            [
                "s_id_jenis_pelayanan" => 15,
                "s_nama_jenis_pelayanan" => "Surat Keterangan  Bebas Pajak  (SKB)",
                "s_id_jenis_pajak" => 10,
                "s_active" => false,
                "s_keterangan_jenis_pelayanan" => "Surat permohonan keterangan bebas pajak (SKB) dikarenakan objek pajak yang dimaksud termasuk fasilitas umum atau fasilitas sosial.",
                "s_order" => 15
            ],
            [
                "s_id_jenis_pelayanan" => 16,
                "s_nama_jenis_pelayanan" => "Kompensasi",
                "s_id_jenis_pajak" => 10,
                "s_active" => false,
                "s_keterangan_jenis_pelayanan" => "Kompensasi adalah kelebihan pembayaran PBB yang diperhitungkan dengan hutang PBB lainnya yang sudah atau belum jatuh tempo atau atas permintaan wajib pajak diperhitungkan dengan ketetapan PBB tahun yang akan datang.",
                "s_order" => 16
            ],
            [
                "s_id_jenis_pelayanan" => 17,
                "s_nama_jenis_pelayanan" => "Penundaan pembayaran PBB P2",
                "s_id_jenis_pajak" => 10,
                "s_active" => false,
                "s_keterangan_jenis_pelayanan" => "Permohonan penundaan pembayaran PBB dikarenakan beberapa alasan diantaranya kondisi keuangan yang kurang baik. Permohonan ini tidak mengurangi kewajiban untuk pembayaran PBB, hanya saja tidak dilakukan penagihan sampai batas waktu penundaan yang diajukan oleh wajib pajak.",
                "s_order" => 17
            ],
            [
                "s_id_jenis_pelayanan" => 18,
                "s_nama_jenis_pelayanan" => "Surat keterangan lunas PBB P2",
                "s_id_jenis_pajak" => 10,
                "s_active" => false,
                "s_keterangan_jenis_pelayanan" => "Permohonan surat keterangan lunas PBB.",
                "s_order" => 18
            ],
            [
                "s_id_jenis_pelayanan" => 19,
                "s_nama_jenis_pelayanan" => "Pembukaan blokir SPPT PBB P2 yang tidak terbit",
                "s_id_jenis_pajak" => 10,
                "s_active" => false,
                "s_keterangan_jenis_pelayanan" => "Permohonan wajib pajak atas pembukaan blokir SPPT yang tidak terbit.",
                "s_order" => 19
            ],
            [
                "s_id_jenis_pelayanan" => 20,
                "s_nama_jenis_pelayanan" => "Restitusi",
                "s_id_jenis_pajak" => 10,
                "s_active" => false,
                "s_keterangan_jenis_pelayanan" => "Restitusi adalah kelebihan pembayaran PBB yang dikembalikan kepada wajib pajak dalam bentuk uang tunai atau pemindahbukuan.",
                "s_order" => 20
            ],
            [
                "s_id_jenis_pelayanan" => 21,
                "s_nama_jenis_pelayanan" => "Pengurangan Sanksi Administrasi",
                "s_id_jenis_pajak" => 10,
                "s_active" => false,
                "s_keterangan_jenis_pelayanan" => "Permohonan pengurangan denda administrasi terhadap pajak yang dibebankan kepada wajib pajak.",
                "s_order" => 21
            ],
            [
                "s_id_jenis_pelayanan" => 22,
                "s_nama_jenis_pelayanan" => "Mutasi Obyek / Subyek PBB-P2 (Gabung)",
                "s_id_jenis_pajak" => 10,
                "s_active" => false,
                "s_keterangan_jenis_pelayanan" => "Permohonan pengurangan denda administrasi terhadap pajak yang dibebankan kepada wajib pajak.",
                "s_order" => 22
            ]
        ]);
        DB::unprepared("ALTER SEQUENCE s_jenis_pelayanan_s_id_jenis_pelayanan_seq RESTART WITH 22;");
    }
}
