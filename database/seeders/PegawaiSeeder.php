<?php

namespace Database\Seeders;

use App\Models\Setting\Pegawai;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PegawaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('s_pegawai')->insert([
        //     [
        //         's_nama_pegawai' => 'RADITYA DIMAS, SH,MM,M.Dev',
        //         's_jabatan_pegawai' => 'FRONT OFFICE',
        //         's_pangkat_pegawai' => '',
        //         's_nip_pegawai' => '',
        //         'created_at' => now(),
        //         'updated_at' => now(),
        //     ]
        // ]);

        // DB::table('s_pegawai')->insert([
        //     [
        //         's_nama_pegawai' => 'AHMAD HUDIL KHOIRI, SE',
        //         's_jabatan_pegawai' => 'ADMINISTRATOR',
        //         's_pangkat_pegawai' => 'Pembina IV.a',
        //         's_nip_pegawai' => "197303151993031005",
        //         'created_at' => now(),
        //         'updated_at' => now(),
        //     ]
        // ]);

        // // DB::unprepared("ALTER SEQUENCE s_pegawai_s_id_pegawai_seq RESTART WITH 3;");
        $jsonString = file_get_contents(base_path('public/json/pegawai.json'));
        $data = json_decode($jsonString, true);

        $i = 1;
        foreach ($data as $v) {
            Pegawai::create([
                's_id_pegawai' => $v['S_ID_PEGAWAI'],
                's_nama_pegawai' => $v['S_NAMA_PEGAWAI'],
                's_jabatan_pegawai' => $v['S_JABATAN_PEGAWA'],
                's_pangkat_pegawai' => $v['S_PANGKAT_PEGAWAI'],
                's_nip_pegawai' => $v['S_NIP_PEGAWA'],
                's_nik' => $v['S_NIK'],
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }

        $statement = "ALTER SEQUENCE s_pegawai_s_id_pegawai_seq RESTART WITH " . (count($data) + 1) . ";";
        DB::unprepared($statement);
    }
}
