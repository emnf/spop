<?php

namespace Database\Seeders;

use App\Models\Setting\JenisHasilPelayanan;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(JenisPajakSeeder::class);
        // $this->call(JenisHasilPelayanan::class);
        $this->call(JenisPelayananSeeder::class);
        $this->call(PersyaratanSeeder::class);
        $this->call(PegawaiSeeder::class);
        $this->call(PemdaSeeder::class);

        $this->call(StatusVerifikasiSeeder::class);
        $this->call(StatusVerifikasiLapanganSeeder::class);
        $this->call(StatusValidasiSeeder::class);
        $this->call(StatusValidasiFinalSeeder::class);

        $this->call(AlurPelayananSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(RolePermissionSeeder::class);

        // $this->call(PelayananSeeder::class);
        // $this->call(OpLamaSeeder::class);
        // $this->call(WpLamaSeeder::class);
    }
}
