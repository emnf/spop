<?php

namespace Database\Seeders;

use App\Models\Pelayanan\Pelayanan;
use Illuminate\Database\Seeder;
use Faker\Factory as FakerFactory;

class PelayananSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pelayanan::create([
            't_id_pelayanan' => '100',
            't_id_jenis_pajak' => '10',
            't_id_jenis_pelayanan' => '1',
            't_nourut' => '100',
            'created_by' => 1,
            't_no_pelayanan' => '100',
            't_tgl_pelayanan' => '2022-07-11',
            't_rt_pemohon' => '01',
            't_rw_pemohon' => '02',
            't_kelurahan_pemohon' => 'Kelurahan',
            't_kecamatan_pemohon' => 'Kecamatan',
            't_kabupaten_pemohon' => 'Kabupaten',
            't_kode_pos_pemohon' => '95881',
            't_no_hp_pemohon' => '082292613349',
            't_email_pemohon' => 'email@email.com',
            't_nama_pemohon' => 'EXTRA',
            't_nik_pemohon' => '12345',
            't_jalan_pemohon' => 'jalan bahagia selamanya',
        ]);
    }
}
