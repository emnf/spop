<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusVerifikasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('pgsql')->table('s_status_verifikasi')->insert([
            [
                's_id_status_verifikasi' => 1,
                's_nama_status_verifikasi' => 'Belum diproses'
            ],
            [
                's_id_status_verifikasi' => 2,
                's_nama_status_verifikasi' => 'Pending'
            ],
            [
                's_id_status_verifikasi' => 3,
                's_nama_status_verifikasi' => 'Ditolak'
            ],
            [
                's_id_status_verifikasi' => 4,
                's_nama_status_verifikasi' => 'Terverifikasi'
            ],
            [
                's_id_status_verifikasi' => 5,
                's_nama_status_verifikasi' => 'Diajukan ke Validator'
            ],
        ]);
    }
}
