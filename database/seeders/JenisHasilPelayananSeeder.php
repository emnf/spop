<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class JenisHasilPelayananSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('s_jenis_hasil_pelayanan')->insert([
            [
                "s_id_hasil_pelayanan" => 1,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 1,
                "s_nama_hasil_pelayanan" => "SPPT"
            ],
            [
                "s_id_hasil_pelayanan" => 3,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 3,
                "s_nama_hasil_pelayanan" => "SK PEMBETULAN"
            ],
            [
                "s_id_hasil_pelayanan" => 4,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 7,
                "s_nama_hasil_pelayanan" => "SK PEMBATALAN"
            ],
            [
                "s_id_hasil_pelayanan" => 5,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 5,
                "s_nama_hasil_pelayanan" => "SPPT"
            ],
            [
                "s_id_hasil_pelayanan" => 6,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 14,
                "s_nama_hasil_pelayanan" => "SK KEBERATAN"
            ],
            [
                "s_id_hasil_pelayanan" => 7,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 6,
                "s_nama_hasil_pelayanan" => "SK KEBERATAN"
            ],
            [
                "s_id_hasil_pelayanan" => 8,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 13,
                "s_nama_hasil_pelayanan" => "SK PEMBETULAN KEBERATAN"
            ],
            [
                "s_id_hasil_pelayanan" => 9,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 4,
                "s_nama_hasil_pelayanan" => "SK PENGURANGAN"
            ],
            [
                "s_id_hasil_pelayanan" => 10,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 11,
                "s_nama_hasil_pelayanan" => "SK ANGSURAN"
            ],
            [
                "s_id_hasil_pelayanan" => 11,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 8,
                "s_nama_hasil_pelayanan" => "SK PENGURANGAN"
            ],
            [
                "s_id_hasil_pelayanan" => 12,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 10,
                "s_nama_hasil_pelayanan" => "SK RESTITUSI"
            ],
            [
                "s_id_hasil_pelayanan" => 13,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 16,
                "s_nama_hasil_pelayanan" => "SK KOMPENSASI"
            ],
            [
                "s_id_hasil_pelayanan" => 14,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 15,
                "s_nama_hasil_pelayanan" => "SKB"
            ],
            [
                "s_id_hasil_pelayanan" => 15,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 12,
                "s_nama_hasil_pelayanan" => "SPPT"
            ],
            [
                "s_id_hasil_pelayanan" => 16,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 17,
                "s_nama_hasil_pelayanan" => "SK PENUNDAAN"
            ],
            [
                "s_id_hasil_pelayanan" => 17,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 9,
                "s_nama_hasil_pelayanan" => "SK NJOP"
            ],
            [
                "s_id_hasil_pelayanan" => 18,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 18,
                "s_nama_hasil_pelayanan" => "SK LUNAS"
            ],
            [
                "s_id_hasil_pelayanan" => 19,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 19,
                "s_nama_hasil_pelayanan" => "SK BUKA BLOKIR"
            ],
            [
                "s_id_hasil_pelayanan" => 20,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 2,
                "s_nama_hasil_pelayanan" => "SPPT"
            ],
            [
                "s_id_hasil_pelayanan" => 2,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 2,
                "s_nama_hasil_pelayanan" => "SK NJOP"
            ],
            [
                "s_id_hasil_pelayanan" => 21,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 3,
                "s_nama_hasil_pelayanan" => "SPPT"
            ],
            [
                "s_id_hasil_pelayanan" => 22,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 6,
                "s_nama_hasil_pelayanan" => "SPPT"
            ],
            [
                "s_id_hasil_pelayanan" => 23,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 13,
                "s_nama_hasil_pelayanan" => "SPPT"
            ],
            [
                "s_id_hasil_pelayanan" => 24,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 4,
                "s_nama_hasil_pelayanan" => "SPPT"
            ],
            [
                "s_id_hasil_pelayanan" => 25,
                "s_id_jenis_pajak" => 10,
                "s_id_jenis_pelayanan" => 19,
                "s_nama_hasil_pelayanan" => "SPPT"
            ]
        ]);

        DB::unprepared("ALTER SEQUENCE s_jenis_hasil_pelayanan_s_id_hasil_pelayanan_seq RESTART WITH 26;");
    }
}
