<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            "Pelayanan",
            "Verfikasi",
            "Validasi",
            "VerifiaksiLapangan",
            "HasilPelayanan",
            "SettingPemda",
            "SettingPegawai",
            "SettingJenisPajak",
            "SettingJenisPelayanan",
            "SettingHasilPelayanan",
            "SettingPersyaratan",
            "SettingDokumenPendukung",
            "SettingPengguna",
            "RolePermission",
            "TrackingPelayanan",
            "Penandatangan",
            "DokumenTte"
        ];

        foreach ($permissions as $value) {
            // if (!Permission::where('name', $value)->where('guard_name', 'web')->exists()) {
            //     Permission::create(['name' => $value, 'guard_name' => 'web']);
            // }
            Permission::create([
                'name' => $value,
                'guard_name' => 'web',
            ]);
        }
    }
}
