<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class WpLamaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('t_wp_lama')->insert([
            [
                't_id_wp_lama' => '1',
                't_id_pelayanan' => '100',
                't_nik_wp' => '99999999999999',
                't_nama_wp' => 'nama wp lama',
                't_jalan_wp' => 'jalan wp lama',
                't_rt_wp' => '012',
                't_rw_wp' => '002',
                't_kelurahan_wp' => 'kelurahan wp lama',
                't_kecamatan_wp' => 'kecamatan wp lama',
                't_kabupaten_wp' => 'kabupaten wp lama',
                't_no_hp_wp' => '092282613349',
                'created_by' => '1',
            ]
        ]);
    }
}
