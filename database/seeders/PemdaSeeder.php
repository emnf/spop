<?php

namespace Database\Seeders;

use App\Models\Setting\PemdaModel;
use Illuminate\Database\Seeder;

class PemdaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PemdaModel::insert([
            's_id_pemda' => 1,
            's_nama_prov' => 'Jawa Tengah ',
            's_nama_kabkota' => 'Kabupaten Blora',
            's_nama_ibukota_kabkota' => 'Blora',
            's_kode_provinsi' => '33',
            's_kode_kabkot' => '16',
            's_nama_instansi' => 'Badan Pendapatan Pengelolaan Keuangan dan Aset Daerah',
            's_nama_singkat_instansi' => 'BPPKAD',
            's_alamat_instansi' => 'Jl. Gatot Subroto No.111',
            's_notelp_instansi' => '(0296) 531202',
            's_logo' => 'logo-pemda/pS8pypziWooSDSgjxCFiZWmyGwFLUC82OoYQEehd.png',
            's_kode_pos' => '58213',
            's_latitude' => '-7.1101832',
            's_longitude' => '111.2034004',
            'created_at' => now()
        ]);
    }
}
