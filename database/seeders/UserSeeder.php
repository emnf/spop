<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Faker\Factory as FakerFactory;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = FakerFactory::create('id_ID');

        $admin = User::create([
            'id' => '1',
            'name' => 'Super Admin',
            'email' => 'admin@mail.com',
            'password' => bcrypt('admin'),
            'nik' => $faker->nik(),
            'nohp' => $faker->phoneNumber(),
            'email_verified_at' => Carbon::now(),
            'username' => 'admin'
        ]);

        $admin->syncRoles('Super Admin');

        //     for ($i=2; $i < 10; $i++) {
        //         $role = Role::findById(rand(2,7));

        //         $user = User::create([
        //             'id' => $i,
        //             'name' => $faker->name,
        //             'email' => $faker->email,
        //             'password' => bcrypt('1234567890'),
        //             'nik' => $faker->nik(),
        //             'nohp' => $faker->phoneNumber(),
        //             'email_verified_at' => Carbon::now(),
        //             'username' => $faker->firstName
        //         ]);

        //         $user->assignRole($role);
        //    }

        DB::unprepared("ALTER SEQUENCE users_id_seq RESTART WITH 10;");
    }
}
