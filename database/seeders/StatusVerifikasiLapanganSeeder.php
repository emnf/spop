<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusVerifikasiLapanganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('pgsql')->table('s_status_verifikasi_lapangan')->insert([
            [
                's_id_status_verifikasi_lapangan' => 1,
                's_nama_status_verifikasi_lapangan' => 'Belum',
            ],
            [
                's_id_status_verifikasi_lapangan' => 2,
                's_nama_status_verifikasi_lapangan' => 'Sudah',
            ],
            [
                's_id_status_verifikasi_lapangan' => 3,
                's_nama_status_verifikasi_lapangan' => 'Ditolak',
            ],
        ]);
    }
}
