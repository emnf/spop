<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OpLamaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('t_op_lama')->insert([
            [
                't_id_op_lama' => '1',
                't_id_pelayanan' => '100',
                'kd_propinsi' => '11',
                'kd_dati2' => '22',
                'kd_kecamatan' => '333',
                'kd_kelurahan' => '444',
                'kd_blok' => '555',
                'no_urut' => '6666',
                'kd_jns_op' => '7',
                't_jalan_op' => 'jalan op lama',
                't_rt_op' => '101',
                't_rw_op' => '102',
                't_kelurahan_op' => 'keluarahan op lama',
                't_kecamatan_op' => 'kecamatan op lama',
                't_luas_tanah' => '110',
                't_luas_bangunan' => '100',
                'created_by' => '1',
            ]
        ]);


    }
}
