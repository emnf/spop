<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jsonString = file_get_contents(base_path('public/json/role_has_permissions.json'));
        $data = json_decode($jsonString, true);
        foreach ($data as $v) {
            $seed = [
                'permission_id' => $v['permission_id'],
                'role_id' => $v['role_id']
            ];
            DB::table('role_has_permissions')->insert($seed);
        }
    }
}
