<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class AlurPelayananSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('s_alur_pelayanan')->insert([
            [
                "s_id_alur" => 1,
                "s_judul_alur" => "Pendaftaran",
                "s_deskripsi_alur" => "Pemohon mendaftarkan berkas dan maksud dari pendaftaran",
                "s_langkah_alur" => 1
            ],
            [
                "s_id_alur" => 2,
                "s_judul_alur" => "Verifikasi Berkas",
                "s_deskripsi_alur" => "Berkas yang diajukan pemohon akan diverifikasi kelengkapan dan kesesuaianya",
                "s_langkah_alur" => 2
            ],
            [
                "s_id_alur" => 3,
                "s_judul_alur" => "Validasi",
                "s_deskripsi_alur" => "Berkas yang sudah diverifikasi selanjutnya akan divalidasi",
                "s_langkah_alur" => 3
            ],
            [
                "s_id_alur" => 4,
                "s_judul_alur" => "Verifikasi Lapangan",
                "s_deskripsi_alur" => "Berkas yang diajukan pemohon jika perlu diverifikasi lapangan maka bagian validasi akan diajukan verifikasi lapangan",
                "s_langkah_alur" => 4
            ],
            [
                "s_id_alur" => 5,
                "s_judul_alur" => "Hasil Pelayanan",
                "s_deskripsi_alur" => "Hasil pelayanan yang sudah selesai akan diupload dan pemohon bisa menguduhnya",
                "s_langkah_alur" => 5
            ]
        ]);

        DB::unprepared("ALTER SEQUENCE s_alur_pelayanan_s_id_alur_seq RESTART WITH 6;");
    }
}
