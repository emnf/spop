<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PersyaratanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('s_persyaratan')->insert([
        [
            "s_id_persyaratan" => 1,
            "s_nama_persyaratan" => "Pengajuan Permohonan Objek Baru",
            "s_id_jenis_pelayanan" => 1
          ],
          [
            "s_id_persyaratan" => 2,
            "s_nama_persyaratan" => "FC KTP / SIM dan KK",
            "s_id_jenis_pelayanan" => 1
          ],
          [
            "s_id_persyaratan" => 3,
            "s_nama_persyaratan" => "FC Sertifikat / Bukti Kepemilikan",
            "s_id_jenis_pelayanan" => 1
          ],
          [
            "s_id_persyaratan" => 4,
            "s_nama_persyaratan" => "Surat Kuasa (jika ada)",
            "s_id_jenis_pelayanan" => 1
          ],
          [
            "s_id_persyaratan" => 5,
            "s_nama_persyaratan" => "Surat Pernyataan Tanah/Bangunan Camat/Lurah",
            "s_id_jenis_pelayanan" => 1
          ],
          [
            "s_id_persyaratan" => 6,
            "s_nama_persyaratan" => "NPWP / NPWPD/Surat Pernyataan Tidak Memiliki NPWP",
            "s_id_jenis_pelayanan" => 1
          ],
          [
            "s_id_persyaratan" => 7,
            "s_nama_persyaratan" => "Pengajuan Permohonan Mutasi",
            "s_id_jenis_pelayanan" => 2
          ],
          [
            "s_id_persyaratan" => 9,
            "s_nama_persyaratan" => "FC KTP / SIM dan KK",
            "s_id_jenis_pelayanan" => 2
          ],
          [
            "s_id_persyaratan" => 10,
            "s_nama_persyaratan" => "FC Sertifikat / Bukti Kepemilikan",
            "s_id_jenis_pelayanan" => 2
          ],
          [
            "s_id_persyaratan" => 11,
            "s_nama_persyaratan" => "Surat Kuasa (jika ada)",
            "s_id_jenis_pelayanan" => 2
          ],
          [
            "s_id_persyaratan" => 12,
            "s_nama_persyaratan" => "NPWP / NPWPD/Surat Pernyataan Tidak Memiliki NPWP",
            "s_id_jenis_pelayanan" => 2
          ],
          [
            "s_id_persyaratan" => 13,
            "s_nama_persyaratan" => "Surat Keterangan/Pernyataan Tanah/Bangunan ",
            "s_id_jenis_pelayanan" => 2
          ],
          [
            "s_id_persyaratan" => 14,
            "s_nama_persyaratan" => "Surat Keterangan Lurah",
            "s_id_jenis_pelayanan" => 2
          ],
          [
            "s_id_persyaratan" => 15,
            "s_nama_persyaratan" => "Pengajuan Permohonan Mutasi",
            "s_id_jenis_pelayanan" => 3
          ],
          [
            "s_id_persyaratan" => 16,
            "s_nama_persyaratan" => "FC KTP / SIM dan KK",
            "s_id_jenis_pelayanan" => 3
          ],
          [
            "s_id_persyaratan" => 17,
            "s_nama_persyaratan" => "FC Sertifikat / Bukti Kepemilikan",
            "s_id_jenis_pelayanan" => 3
          ],
          [
            "s_id_persyaratan" => 18,
            "s_nama_persyaratan" => "Surat Kuasa (jika ada)",
            "s_id_jenis_pelayanan" => 3
          ],
          [
            "s_id_persyaratan" => 19,
            "s_nama_persyaratan" => "NPWP / NPWPD/Surat Pernyataan Tidak Memiliki NPWP",
            "s_id_jenis_pelayanan" => 3
          ],
          [
            "s_id_persyaratan" => 20,
            "s_nama_persyaratan" => "Surat Keterangan/Pernyataan Tanah/Bangunan ",
            "s_id_jenis_pelayanan" => 3
          ],
          [
            "s_id_persyaratan" => 21,
            "s_nama_persyaratan" => "Surat Keterangan Lurah",
            "s_id_jenis_pelayanan" => 3
          ],
          [
            "s_id_persyaratan" => 22,
            "s_nama_persyaratan" => "Pengajuan Permohonan",
            "s_id_jenis_pelayanan" => 4
          ],
          [
            "s_id_persyaratan" => 23,
            "s_nama_persyaratan" => "FC KTP / SIM dan KK",
            "s_id_jenis_pelayanan" => 4
          ],
          [
            "s_id_persyaratan" => 24,
            "s_nama_persyaratan" => "FC Sertifikat / Bukti Kepemilikan",
            "s_id_jenis_pelayanan" => 4
          ],
          [
            "s_id_persyaratan" => 25,
            "s_nama_persyaratan" => "Surat Kuasa (jika ada)",
            "s_id_jenis_pelayanan" => 4
          ],
          [
            "s_id_persyaratan" => 26,
            "s_nama_persyaratan" => "NPWP / NPWPD/Surat Pernyataan Tidak Memiliki NPWP",
            "s_id_jenis_pelayanan" => 4
          ],
          [
            "s_id_persyaratan" => 27,
            "s_nama_persyaratan" => "Surat Keterangan/Pernyataan Tanah/Bangunan ",
            "s_id_jenis_pelayanan" => 4
          ],
          [
            "s_id_persyaratan" => 28,
            "s_nama_persyaratan" => "Surat Keterangan Lurah",
            "s_id_jenis_pelayanan" => 4
          ],
          [
            "s_id_persyaratan" => 29,
            "s_nama_persyaratan" => "Pengajuan Permohonan",
            "s_id_jenis_pelayanan" => 5
          ],
          [
            "s_id_persyaratan" => 30,
            "s_nama_persyaratan" => "FC KTP / SIM ",
            "s_id_jenis_pelayanan" => 5
          ],
          [
            "s_id_persyaratan" => 31,
            "s_nama_persyaratan" => "Kartu Keluarga/KK",
            "s_id_jenis_pelayanan" => 5
          ],
          [
            "s_id_persyaratan" => 32,
            "s_nama_persyaratan" => "NPWP / NPWPD/Surat Pernyataan Tidak Memiliki NPWP",
            "s_id_jenis_pelayanan" => 5
          ],
          [
            "s_id_persyaratan" => 33,
            "s_nama_persyaratan" => "Pengajuan Permohonan",
            "s_id_jenis_pelayanan" => 6
          ],
          [
            "s_id_persyaratan" => 34,
            "s_nama_persyaratan" => "FC KTP / SIM ",
            "s_id_jenis_pelayanan" => 6
          ],
          [
            "s_id_persyaratan" => 35,
            "s_nama_persyaratan" => "Kartu Keluarga/KK",
            "s_id_jenis_pelayanan" => 6
          ],
          [
            "s_id_persyaratan" => 36,
            "s_nama_persyaratan" => "Pengajuan Permohonan Keberatan",
            "s_id_jenis_pelayanan" => 7
          ],
          [
            "s_id_persyaratan" => 37,
            "s_nama_persyaratan" => "FC KTP / SIM dan KK",
            "s_id_jenis_pelayanan" => 7
          ],
          [
            "s_id_persyaratan" => 38,
            "s_nama_persyaratan" => "FC Sertifikat / Bukti Kepemilikan",
            "s_id_jenis_pelayanan" => 7
          ],
          [
            "s_id_persyaratan" => 39,
            "s_nama_persyaratan" => "Surat Kuasa (jika ada)",
            "s_id_jenis_pelayanan" => 7
          ],
          [
            "s_id_persyaratan" => 40,
            "s_nama_persyaratan" => "NPWP / NPWPD/Surat Pernyataan Tidak Memiliki NPWP",
            "s_id_jenis_pelayanan" => 7
          ],
          [
            "s_id_persyaratan" => 41,
            "s_nama_persyaratan" => "Surat Keterangan/Pernyataan Tanah/Bangunan ",
            "s_id_jenis_pelayanan" => 7
          ],
          [
            "s_id_persyaratan" => 42,
            "s_nama_persyaratan" => "Surat Keterangan Lurah",
            "s_id_jenis_pelayanan" => 7
          ],
          [
            "s_id_persyaratan" => 43,
            "s_nama_persyaratan" => "Pengajuan Permohonan",
            "s_id_jenis_pelayanan" => 8
          ],
          [
            "s_id_persyaratan" => 44,
            "s_nama_persyaratan" => "FC KTP / SIM ",
            "s_id_jenis_pelayanan" => 8
          ],
          [
            "s_id_persyaratan" => 45,
            "s_nama_persyaratan" => "Kartu Keluarga/KK",
            "s_id_jenis_pelayanan" => 8
          ],
          [
            "s_id_persyaratan" => 46,
            "s_nama_persyaratan" => "Surat Keterangan Tidak Mampu (SKTM)",
            "s_id_jenis_pelayanan" => 8
          ],
          [
            "s_id_persyaratan" => 47,
            "s_nama_persyaratan" => "NPWP / NPWPD/Surat Pernyataan Tidak Memiliki NPWP",
            "s_id_jenis_pelayanan" => 8
          ],
          [
            "s_id_persyaratan" => 48,
            "s_nama_persyaratan" => "Pengajuan Permohonan",
            "s_id_jenis_pelayanan" => 9
          ],
          [
            "s_id_persyaratan" => 49,
            "s_nama_persyaratan" => "FC KTP / SIM ",
            "s_id_jenis_pelayanan" => 9
          ],
          [
            "s_id_persyaratan" => 50,
            "s_nama_persyaratan" => "Kartu Keluarga/KK",
            "s_id_jenis_pelayanan" => 9
          ],
          [
            "s_id_persyaratan" => 51,
            "s_nama_persyaratan" => "NPWP / NPWPD/Surat Pernyataan Tidak Memiliki NPWP",
            "s_id_jenis_pelayanan" => 9
          ],
          [
            "s_id_persyaratan" => 52,
            "s_nama_persyaratan" => "FORM SPOP/LSPOP",
            "s_id_jenis_pelayanan" => 2
          ],
          [
            "s_id_persyaratan" => 53,
            "s_nama_persyaratan" => "SEMUA SPOP/LSPOP(PECAH/GABUNG)",
            "s_id_jenis_pelayanan" => 3
          ],
          [
            "s_id_persyaratan" => 54,
            "s_nama_persyaratan" => "SEMUA SPOP/LSPOP(PECAH/GABUNG)",
            "s_id_jenis_pelayanan" => 22
          ],
          [
            "s_id_persyaratan" => 55,
            "s_nama_persyaratan" => "FC KTP / SIM dan KK",
            "s_id_jenis_pelayanan" => 22
          ],
          [
            "s_id_persyaratan" => 56,
            "s_nama_persyaratan" => "FC Sertifikat / Bukti Kepemilikan",
            "s_id_jenis_pelayanan" => 22
          ],
          [
            "s_id_persyaratan" => 57,
            "s_nama_persyaratan" => "Pengajuan Permohonan Mutasi",
            "s_id_jenis_pelayanan" => 22
          ],
          [
            "s_id_persyaratan" => 58,
            "s_nama_persyaratan" => "Surat Kuasa (jika ada)",
            "s_id_jenis_pelayanan" => 22
          ],
          [
            "s_id_persyaratan" => 59,
            "s_nama_persyaratan" => "NPWP / NPWPD/Surat Pernyataan Tidak Memiliki NPWP",
            "s_id_jenis_pelayanan" => 22
          ],
          [
            "s_id_persyaratan" => 60,
            "s_nama_persyaratan" => "Surat Keterangan/Pernyataan Tanah/Bangunan",
            "s_id_jenis_pelayanan" => 22
          ],
          [
            "s_id_persyaratan" => 61,
            "s_nama_persyaratan" => "Surat Keterangan Lurah",
            "s_id_jenis_pelayanan" => 22
          ],
        ]);

        DB::unprepared("ALTER SEQUENCE s_persyaratan_s_id_persyaratan_seq RESTART WITH 61;");

    }
}
