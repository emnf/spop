<?php

namespace App\Models\Esign;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Esign extends Model
{
    use HasFactory;
    protected $table = 't_esign';
    protected $fillable = [
        't_id_pelayanan',
        't_id_pejabat',
        'created_by',
        'imgQr',
        'file',
        'signed',
        'uuid',
        'created_at',
        'updated_at'
    ];
}
