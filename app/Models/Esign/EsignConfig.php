<?php

namespace App\Models\Esign;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EsignConfig extends Model
{
    use HasFactory;
    protected $table = 'esign_config';
}
