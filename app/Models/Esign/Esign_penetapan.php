<?php

namespace App\Models\Esign;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Esign_penetapan extends Model
{
    use HasFactory;
    use HasFactory;
    protected $table = 't_esign_penetapan';
    protected $fillable = [
        't_nop',
        't_id_pejabat',
        'created_by',
        'imgQr',
        'file',
        'signed',
        'uuid',
        'created_at',
        'updated_at'
    ];
}
