<?php

namespace App\Models\Validasi;

use App\Models\Pelayanan\Pelayanan;
use App\Models\Status\StatusValidasi;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Validasi extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 't_validasi';
    protected $primaryKey = 't_id_validasi';
    protected $fillable = [
        't_id_pelayanan',
        't_tgl_validasi',
        't_no_validasi',
        't_id_status_validasi',
        't_keterangan_validasi',
        't_tgl_akhir_verlap',
        'created_by',
        't_besar_pengurangan'
    ];

    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = [ 'created_at', 'updated_at'];
    protected static $logOnlyDirty = true;

    public static function getMaxNoUrut()
    {
        return self::whereRaw("EXTRACT(YEAR FROM t_tgl_validasi) = '". date('Y') ."'")->max('t_no_validasi');
    }

    public function statusValidasi()
    {
        return $this->belongsTo(StatusValidasi::class, 't_id_status_validasi');
    }

    public function pelayanan()
    {
        return $this->belongsTo(Pelayanan::class, 't_id_pelayanan');
    }
}
