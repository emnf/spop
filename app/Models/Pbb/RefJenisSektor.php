<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefJenisSektor extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'REF_JNS_SEKTOR';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'KD_SEKTOR';
    protected $fillable = ['KD_SEKTOR', 'NM_SEKTOR'];
}
