<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;

class DatOpBangunan extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'dat_op_bangunan';
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = [
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_blok',
        'no_urut',
        'kd_jns_op',
        'no_bng'
    ];
    protected $fillable = [
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_blok',
        'no_urut',
        'kd_jns_op',
        'no_bng',
        'kd_jpb',
        'no_formulir_lspop',
        'thn_dibangun_bng',
        'thn_renovasi_bng',
        'luas_bng',
        'jml_lantai_bng',
        'kondisi_bng',
        'jns_konstruksi_bng',
        'jns_atap_bng',
        'kd_dinding',
        'kd_lantai',
        'kd_langit_langit',
        'nilai_sistem_bng',
        'jns_transaksi_bng',
        'tgl_pendataan_bng',
        'nip_pendata_bng',
        'tgl_pemeriksaan_bng',
        'nip_pemeriksa_bng',
        'tgl_perekaman_bng',
        'nip_perekam_bng'
    ];

    public function getNopFormatedAttribute()
    {
        return "{$this->kd_propinsi}.{$this->kd_dati2}.{$this->kd_kecamatan}.{$this->kd_kelurahan}.{$this->kd_blok}.{$this->no_urut}.{$this->kd_jns_op}";
    }

    public function getNopAttribute()
    {
        return "{$this->kd_propinsi}{$this->kd_dati2}{$this->kd_kecamatan}{$this->kd_kelurahan}{$this->kd_blok}{$this->no_urut}{$this->kd_jns_op}";
    }

    public function scopeGetByNop($query, $nop)
    {
        return $query->where([
            'kd_propinsi' => $nop['KD_PROPINSI'],
            'kd_dati2' => $nop['KD_DATI2'],
            'kd_kecamatan' => $nop['KD_KECAMATAN'],
            'kd_kelurahan' => $nop['KD_KELURAHAN'],
            'kd_blok' => $nop['KD_BLOK'],
            'no_urut' => $nop['NO_URUT'],
            'kd_jns_op' => $nop['KD_JNS_OP'],
        ]);
    }

    public function jpb()
    {
        return $this->hasOne(RefJpb::class, 'kd_jpb', 'kd_jpb');
    }

    public function scopeGetByNopNoBangunan($query, $nop, $noBangunan)
    {
        return $query->where([
            'kd_propinsi' => substr($nop, 0, 2),
            'kd_dati2' => substr($nop, 2, 2),
            'kd_kecamatan' => substr($nop, 4, 3),
            'kd_kelurahan' => substr($nop, 7, 3),
            'kd_blok' => substr($nop, 10, 3),
            'no_urut' => substr($nop, 13, 4),
            'kd_jns_op' => substr($nop, 17, 1),
            'no_bng' => $noBangunan
        ]);
    }


    public function scopeWhereNop($query, $nop)
    {
        return $query->where([
            'kd_propinsi' => substr($nop, 0, 2),
            'kd_dati2' => substr($nop, 2, 2),
            'kd_kecamatan' => substr($nop, 4, 3),
            'kd_kelurahan' => substr($nop, 7, 3),
            'kd_blok' => substr($nop, 10, 3),
            'no_urut' => substr($nop, 13, 4),
            'kd_jns_op' => substr($nop, 17, 1),
        ]);
    }

    public function datFasilitas()
    {
        return $this->hasMany(DatFasilitasBangunan::class, 'no_bng', 'no_bng')
            ->where('kd_propinsi', $this->kd_propinsi)
            ->where('kd_dati2', $this->kd_dati2)
            ->where('kd_kecamatan', $this->kd_kecamatan)
            ->where('kd_kelurahan', $this->kd_kelurahan)
            ->where('kd_blok', $this->kd_blok)
            ->where('no_urut', $this->no_urut)
            ->where('kd_jns_op', $this->kd_jns_op);
    }

    public function detailBangunan($nop)
    {
        $data = DB::connection($this->connection)->select(DB::raw(
            "SELECT  
            TO_CHAR(TRIM(DAT_SUBJEK_PAJAK.SUBJEK_PAJAK_ID)) AS SUBJEK_PAJAK_ID,
        DAT_SUBJEK_PAJAK.*,
        REF_KELURAHAN.*,
        REF_KECAMATAN.*,
        DAT_OBJEK_PAJAK.*,
        DAT_OP_BANGUNAN.*,
        -- DAT_FASILITAS_BANGUNAN.*,
        DAT_OP_BUMI.*

        FROM DAT_OBJEK_PAJAK
        
        LEFT JOIN DAT_SUBJEK_PAJAK ON DAT_SUBJEK_PAJAK.SUBJEK_PAJAK_ID = DAT_OBJEK_PAJAK.SUBJEK_PAJAK_ID

        LEFT JOIN DAT_OP_BUMI ON
        DAT_OP_BUMI.KD_PROPINSI = DAT_OBJEK_PAJAK.KD_PROPINSI
        AND DAT_OP_BUMI.KD_DATI2 = DAT_OBJEK_PAJAK.KD_DATI2
        AND DAT_OP_BUMI.KD_KECAMATAN = DAT_OBJEK_PAJAK.KD_KECAMATAN
        AND DAT_OP_BUMI.KD_KELURAHAN = DAT_OBJEK_PAJAK.KD_KELURAHAN
        AND DAT_OP_BUMI.KD_BLOK = DAT_OBJEK_PAJAK.KD_BLOK 
        AND DAT_OP_BUMI.NO_URUT = DAT_OBJEK_PAJAK.NO_URUT 
        AND DAT_OP_BUMI.KD_JNS_OP = DAT_OBJEK_PAJAK.KD_JNS_OP

        LEFT JOIN DAT_OP_BANGUNAN ON
        DAT_OP_BANGUNAN.KD_PROPINSI = DAT_OBJEK_PAJAK.KD_PROPINSI
        AND DAT_OP_BANGUNAN.KD_DATI2 = DAT_OBJEK_PAJAK.KD_DATI2
        AND DAT_OP_BANGUNAN.KD_KECAMATAN = DAT_OBJEK_PAJAK.KD_KECAMATAN
        AND DAT_OP_BANGUNAN.KD_KELURAHAN = DAT_OBJEK_PAJAK.KD_KELURAHAN
        AND DAT_OP_BANGUNAN.KD_BLOK = DAT_OBJEK_PAJAK.KD_BLOK 
        AND DAT_OP_BANGUNAN.NO_URUT = DAT_OBJEK_PAJAK.NO_URUT 
        AND DAT_OP_BANGUNAN.KD_JNS_OP = DAT_OBJEK_PAJAK.KD_JNS_OP

        -- LEFT JOIN DAT_FASILITAS_BANGUNAN ON
        -- DAT_FASILITAS_BANGUNAN.KD_PROPINSI = DAT_OBJEK_PAJAK.KD_PROPINSI
        -- AND DAT_FASILITAS_BANGUNAN.KD_DATI2 = DAT_OBJEK_PAJAK.KD_DATI2
        -- AND DAT_FASILITAS_BANGUNAN.KD_KECAMATAN = DAT_OBJEK_PAJAK.KD_KECAMATAN
        -- AND DAT_FASILITAS_BANGUNAN.KD_KELURAHAN = DAT_OBJEK_PAJAK.KD_KELURAHAN
        -- AND DAT_FASILITAS_BANGUNAN.KD_BLOK = DAT_OBJEK_PAJAK.KD_BLOK 
        -- AND DAT_FASILITAS_BANGUNAN.NO_URUT = DAT_OBJEK_PAJAK.NO_URUT 
        -- AND DAT_FASILITAS_BANGUNAN.KD_JNS_OP = DAT_OBJEK_PAJAK.KD_JNS_OP

        LEFT JOIN REF_KECAMATAN ON
        REF_KECAMATAN.KD_PROPINSI = DAT_OBJEK_PAJAK.KD_PROPINSI
        AND REF_KECAMATAN.KD_DATI2 = DAT_OBJEK_PAJAK.KD_DATI2
        AND REF_KECAMATAN.KD_KECAMATAN = DAT_OBJEK_PAJAK.KD_KECAMATAN

        LEFT JOIN REF_KELURAHAN ON
        REF_KELURAHAN.KD_PROPINSI = DAT_OBJEK_PAJAK.KD_PROPINSI
        AND REF_KELURAHAN.KD_DATI2 = DAT_OBJEK_PAJAK.KD_DATI2
        AND REF_KELURAHAN.KD_KECAMATAN = DAT_OBJEK_PAJAK.KD_KECAMATAN
        AND REF_KELURAHAN.KD_KELURAHAN = DAT_OBJEK_PAJAK.KD_KELURAHAN

        WHERE

        DAT_OBJEK_PAJAK.KD_PROPINSI = '" . $nop['KD_PROPINSI'] . "'
        AND DAT_OBJEK_PAJAK.KD_DATI2 = '" . $nop['KD_DATI2'] . "'
        AND DAT_OBJEK_PAJAK.KD_KECAMATAN = '" . $nop['KD_KECAMATAN'] . "'
        AND DAT_OBJEK_PAJAK.KD_KELURAHAN = '" . $nop['KD_KELURAHAN'] . "'
        AND DAT_OBJEK_PAJAK.KD_BLOK = '" . $nop['KD_BLOK'] . "'
        AND DAT_OBJEK_PAJAK.NO_URUT = '" . $nop['NO_URUT'] . "'
        AND DAT_OBJEK_PAJAK.KD_JNS_OP = '" . $nop['KD_JNS_OP'] . "'
        "
        ))[0];

        return $data;
    }

    public function detailFasilitas($nop)
    {
        $data = DB::connection($this->connection)->select(DB::raw(
            "SELECT  
        DAT_FASILITAS_BANGUNAN.*,
        FASILITAS.*

        FROM DAT_OBJEK_PAJAK
        
        LEFT JOIN DAT_FASILITAS_BANGUNAN ON
        DAT_FASILITAS_BANGUNAN.KD_PROPINSI = DAT_OBJEK_PAJAK.KD_PROPINSI
        AND DAT_FASILITAS_BANGUNAN.KD_DATI2 = DAT_OBJEK_PAJAK.KD_DATI2
        AND DAT_FASILITAS_BANGUNAN.KD_KECAMATAN = DAT_OBJEK_PAJAK.KD_KECAMATAN
        AND DAT_FASILITAS_BANGUNAN.KD_KELURAHAN = DAT_OBJEK_PAJAK.KD_KELURAHAN
        AND DAT_FASILITAS_BANGUNAN.KD_BLOK = DAT_OBJEK_PAJAK.KD_BLOK 
        AND DAT_FASILITAS_BANGUNAN.NO_URUT = DAT_OBJEK_PAJAK.NO_URUT 
        AND DAT_FASILITAS_BANGUNAN.KD_JNS_OP = DAT_OBJEK_PAJAK.KD_JNS_OP

        LEFT JOIN FASILITAS ON
        FASILITAS.KD_FASILITAS = DAT_FASILITAS_BANGUNAN.KD_FASILITAS

        WHERE

        DAT_OBJEK_PAJAK.KD_PROPINSI = '" . $nop['KD_PROPINSI'] . "'
        AND DAT_OBJEK_PAJAK.KD_DATI2 = '" . $nop['KD_DATI2'] . "'
        AND DAT_OBJEK_PAJAK.KD_KECAMATAN = '" . $nop['KD_KECAMATAN'] . "'
        AND DAT_OBJEK_PAJAK.KD_KELURAHAN = '" . $nop['KD_KELURAHAN'] . "'
        AND DAT_OBJEK_PAJAK.KD_BLOK = '" . $nop['KD_BLOK'] . "'
        AND DAT_OBJEK_PAJAK.NO_URUT = '" . $nop['NO_URUT'] . "'
        AND DAT_OBJEK_PAJAK.KD_JNS_OP = '" . $nop['KD_JNS_OP'] . "'
        "
        ));
// dd($data);
        return $data;
    }
}
