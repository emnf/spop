<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MaxUrutPst extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'max_urut_pst';
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = [
        'kd_kanwil',
        'kd_kppbb',
        'thn_pelayanan',
        'bundel_pelayanan'
    ];

    protected $fillable = [
        'kd_kanwil',
        'kd_kppbb',
        'thn_pelayanan',
        'bundel_pelayanan',
        'no_urut_pelayanan'
    ];

    public static function getMaxNoUrutPst()
    {
        $noUrut = self::where('thn_pelayanan', date('Y'))->first();

        if (is_null($noUrut)) {
            $noUrut = self::first();
            $noUrut->thn_pelayanan = $noUrut->thn_pelayanan + 1;
            $noUrut->bundel_pelayanan = '0001';
            $noUrut->no_urut_pelayanan = '001';

            return $noUrut;
        }

        if ($noUrut->no_urut_pelayanan == 200) {
            $bundel_pelayanan = (int) $noUrut->bundel_pelayanan +1;
            $noUrut->bundel_pelayanan = str_pad($bundel_pelayanan, 4, '0', STR_PAD_LEFT);
            $noUrut->no_urut_pelayanan = '001';
        } else {
            $no_urut_pelayanan = (int) $noUrut->no_urut_pelayanan +1;
            $noUrut->no_urut_pelayanan = str_pad($no_urut_pelayanan, 3, '0', STR_PAD_LEFT);
        }

        return $noUrut;
    }
}
