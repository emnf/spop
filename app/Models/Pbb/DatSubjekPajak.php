<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatSubjekPajak extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'dat_subjek_pajak';
    public $incrementing = false;
    public $timestamps = false;
    protected $keyType = 'string';
    protected $primaryKey = 'subjek_pajak_id';
    protected $fillable = [
        'subjek_pajak_id',
        'nm_wp',
        'jalan_wp',
        'blok_kav_no_wp',
        'rw_wp',
        'rt_wp',
        'kelurahan_wp',
        'kota_wp',
        'kd_pos_wp',
        'telp_wp',
        'npwp',
        'status_pekerjaan_wp'
        // 'npwpd',  // yang sismiop gak ada
        // 'email' // yang sismiop gak ada
    ];

    public function scopeWhereSubjekPajakId($query, $subjekPajakId)
    {
        return $query->whereRaw("trim(subjek_pajak_id) =?", [$subjekPajakId]);
    }
}
