<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NomorBundelSismiop extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'temp_max_bundel';
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = [
        'kd_kanwil',
        'kd_kppbb',
        'temp_thn_bundel',
        'temp_no_bundel',
        'temp_urut_bundel'
    ];

    protected $fillable = [
        'kd_kanwil',
        'kd_kppbb',
        'temp_thn_bundel',
        'temp_no_bundel',
        'temp_urut_bundel'
    ];

    public function getMaxNomorBundel()
    {
        $nomorBundel = self::selectRaw("MAX(temp_no_bundel) as nomor_bundul")
            ->whereRaw("TEMP_THN_BUNDEL=? AND TEMP_URUT_BUNDEL < 999", [date('Y')])
            ->first();
        $nomorBundel->nomor_bundul ??= '0001';
        $urutBundel = self::selectRaw("LPAD(MAX(temp_urut_bundel)+1,3, '0') as urut_bundel")
            ->whereRaw("TEMP_THN_BUNDEL=? AND TEMP_NO_BUNDEL=?", [date('Y'), $nomorBundel->nomor_bundul])
            ->first();
        $urutBundel->urut_bundel ??= '001';
        return date('Y') . $nomorBundel->nomor_bundul . $urutBundel->urut_bundel;
    }
}
