<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefKppbb extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'ref_kppbb';
    public $incrementing = false;
    protected $primaryKey = ['kd_kanwil', 'kd_kppbb'];

    public static function getKdkanwilKdKantor()
    {
        return self::select('kd_kanwil', 'kd_kppbb')->first()->toArray();
    }
}
