<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Sppt extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'SPPT';
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = [
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_blok',
        'no_urut',
        'kd_jns_op',
        'thn_pajak_sppt'
    ];
    protected $fillable = [
        'nm_wp_sppt',
        'jln_wp_sppt',
        'blok_kav_no_wp_sppt',
        'rw_wp_sppt',
        'rt_wp_sppt',
        'kelurahan_wp_sppt',
        'kota_wp_sppt',
        'kd_pos_wp_sppt',
        'npwp_sppt',
        'no_persil_sppt',
        'kd_kls_tanah',
        'thn_awal_kls_tanah',
        'kd_kls_bng',
        'thn_awal_kls_bng',
        'tgl_jatuh_tempo_sppt',
        'luas_bumi_sppt',
        'luas_bng_sppt',
        'njop_bumi_sppt',
        'njop_bng_sppt',
        'njop_sppt',
        'njoptkp_sppt',
        'pbb_terhutang_sppt',
        'faktor_pengurang_sppt',
        'pbb_yg_harus_dibayar_sppt',
        'status_pembayaran_sppt',
        'status_tagihan_sppt',
        'status_cetak_sppt',
        'tgl_terbit_sppt',
        'tgl_cetak_sppt',
        'nip_pencetak_sppt',
        'tgl_pembayaran_sppt'
    ];

    public function getTahunPajakTerahir($nop)
    {

        $data = DB::connection($this->connection)->table($this->table)
            ->selectRaw('MAX(thn_pajak_sppt) as tahunpajak')
            ->where('kd_propinsi', substr($nop, 0, 2))
            ->where('kd_dati2', substr($nop, 3, 2))
            ->where('kd_kecamatan', substr($nop, 6, 3))
            ->where('kd_kelurahan', substr($nop, 10, 3))
            ->where('kd_blok', substr($nop, 14, 3))
            ->where('no_urut', substr($nop, 18, 4))
            ->where('kd_jns_op', substr($nop, 23, 1))
            ->first()->tahunpajak;

        return $data;
    }

    public function getDataKetetapan($nop, $tahun)
    {

        try {
            $data = DB::connection($this->connection)->select(DB::raw(
                "
        SELECT
            TO_CHAR(TRIM(DAT_SUBJEK_PAJAK.SUBJEK_PAJAK_ID)) AS SUBJEK_PAJAK_ID,
            SPPT.*,
            DAT_SUBJEK_PAJAK.*,
            REF_KELURAHAN.*,
            REF_KECAMATAN.*,
            DAT_OBJEK_PAJAK.*,
            REF_JNS_SEKTOR.NM_SEKTOR

        FROM SPPT

        LEFT JOIN DAT_OBJEK_PAJAK ON
            DAT_OBJEK_PAJAK.KD_PROPINSI = SPPT.KD_PROPINSI AND
            DAT_OBJEK_PAJAK.KD_DATI2 = SPPT.KD_DATI2 AND
            DAT_OBJEK_PAJAK.KD_KECAMATAN = SPPT.KD_KECAMATAN AND
            DAT_OBJEK_PAJAK.KD_KELURAHAN = SPPT.KD_KELURAHAN AND
            DAT_OBJEK_PAJAK.KD_BLOK = SPPT.KD_BLOK AND
            DAT_OBJEK_PAJAK.NO_URUT = SPPT.NO_URUT AND
            DAT_OBJEK_PAJAK.KD_JNS_OP = SPPT.KD_JNS_OP

        LEFT JOIN DAT_SUBJEK_PAJAK ON DAT_SUBJEK_PAJAK.SUBJEK_PAJAK_ID = DAT_OBJEK_PAJAK.SUBJEK_PAJAK_ID

        LEFT JOIN REF_KECAMATAN ON REF_KECAMATAN.KD_PROPINSI = DAT_OBJEK_PAJAK.KD_PROPINSI AND
            REF_KECAMATAN.KD_DATI2 = DAT_OBJEK_PAJAK.KD_DATI2 AND
            REF_KECAMATAN.KD_KECAMATAN = DAT_OBJEK_PAJAK.KD_KECAMATAN
        LEFT JOIN REF_KELURAHAN ON REF_KELURAHAN.KD_PROPINSI = DAT_OBJEK_PAJAK.KD_PROPINSI AND
            REF_KELURAHAN.KD_DATI2 = DAT_OBJEK_PAJAK.KD_DATI2 AND
            REF_KELURAHAN.KD_KECAMATAN = DAT_OBJEK_PAJAK.KD_KECAMATAN AND
            REF_KELURAHAN.KD_KELURAHAN = DAT_OBJEK_PAJAK.KD_KELURAHAN
        LEFT JOIN REF_JNS_SEKTOR ON REF_JNS_SEKTOR.KD_SEKTOR = REF_KELURAHAN.KD_SEKTOR
        WHERE
            SPPT.KD_PROPINSI = '" . $nop['KD_PROPINSI'] . "'
            AND SPPT.KD_DATI2 = '" . $nop['KD_DATI2'] . "'
            AND SPPT.KD_KECAMATAN = '" . $nop['KD_KECAMATAN'] . "'
            AND SPPT.KD_KELURAHAN = '" . $nop['KD_KELURAHAN'] . "'
            AND SPPT.KD_BLOK = '" . $nop['KD_BLOK'] . "'
            AND SPPT.NO_URUT = '" . $nop['NO_URUT'] . "'
            AND SPPT.KD_JNS_OP = '" . $nop['KD_JNS_OP'] . "'
            AND SPPT.THN_PAJAK_SPPT = '" . $tahun . "'
            "
            ))[0];
            // AND SPPT.STATUS_PEMBAYARAN_SPPT = '0'
        } catch (\Throwable $th) {
            $data = [];
            //throw $th;
        }

        return $data;
    }

    public function getDataKetetapanByNop($nop)
    {

        $data = DB::connection($this->connection)->table($this->table)
            ->select(
                'SPPT.KD_PROPINSI',
                'SPPT.KD_DATI2',
                'SPPT.KD_KECAMATAN',
                'SPPT.KD_KELURAHAN',
                'SPPT.KD_BLOK',
                'SPPT.NO_URUT',
                'SPPT.KD_JNS_OP',
                'SPPT.THN_PAJAK_SPPT',
                'SPPT.SIKLUS_SPPT',
                // 'SPPT.KD_KANWIL', //simpbb
                // 'SPPT.KD_KANTOR', //simpbb
                'SPPT.KD_KANWIL_BANK', //sismiop
                'SPPT.KD_KPPBB_BANK', //sismiop
                'SPPT.KD_TP',
                'SPPT.NM_WP_SPPT',
                'SPPT.JLN_WP_SPPT',
                'SPPT.BLOK_KAV_NO_WP_SPPT',
                'SPPT.RW_WP_SPPT',
                'SPPT.RT_WP_SPPT',
                'SPPT.KELURAHAN_WP_SPPT',
                'SPPT.KOTA_WP_SPPT',
                'SPPT.KD_POS_WP_SPPT',
                'SPPT.NPWP_SPPT',
                'SPPT.NO_PERSIL_SPPT',
                'SPPT.KD_KLS_TANAH',
                'SPPT.THN_AWAL_KLS_TANAH',
                'SPPT.KD_KLS_BNG',
                'SPPT.THN_AWAL_KLS_BNG',
                'SPPT.TGL_JATUH_TEMPO_SPPT',
                'SPPT.LUAS_BUMI_SPPT',
                'SPPT.LUAS_BNG_SPPT',
                'SPPT.NJOP_BUMI_SPPT',
                'SPPT.NJOP_BNG_SPPT',
                'SPPT.NJOP_SPPT',
                'SPPT.NJOPTKP_SPPT',
                'SPPT.PBB_TERHUTANG_SPPT',
                'SPPT.FAKTOR_PENGURANG_SPPT',
                'SPPT.PBB_YG_HARUS_DIBAYAR_SPPT',
                'SPPT.STATUS_PEMBAYARAN_SPPT',
                'SPPT.STATUS_TAGIHAN_SPPT',
                'SPPT.STATUS_CETAK_SPPT',
                'SPPT.TGL_TERBIT_SPPT',
                'SPPT.TGL_CETAK_SPPT',
                'SPPT.NIP_PENCETAK_SPPT',
                'SPPT.TGL_PEMBAYARAN_SPPT',
                'DAT_OBJEK_PAJAK.JALAN_OP',
                'DAT_OBJEK_PAJAK.RT_OP',
                'DAT_OBJEK_PAJAK.RW_OP',
                'DAT_OBJEK_PAJAK.BLOK_KAV_NO_OP',
                'DAT_OBJEK_PAJAK.TOTAL_LUAS_BUMI',
                'DAT_OBJEK_PAJAK.TOTAL_LUAS_BNG',
                // 'DAT_OBJEK_PAJAK.LONGITUDE',
                // 'DAT_OBJEK_PAJAK.LATITUDE',
                'DAT_SUBJEK_PAJAK.TELP_WP',
                'REF_KECAMATAN.NM_KECAMATAN',
                'REF_KELURAHAN.NM_KELURAHAN',
            )
            ->leftJoin('REF_KECAMATAN', [
                ['REF_KECAMATAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['REF_KECAMATAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['REF_KECAMATAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN']
            ])
            ->leftJoin('REF_KELURAHAN', [
                ['REF_KELURAHAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['REF_KELURAHAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['REF_KELURAHAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['REF_KELURAHAN.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN']
            ])
            ->leftJoin('DAT_OBJEK_PAJAK', [
                ['DAT_OBJEK_PAJAK.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['DAT_OBJEK_PAJAK.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['DAT_OBJEK_PAJAK.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['DAT_OBJEK_PAJAK.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                ['DAT_OBJEK_PAJAK.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                ['DAT_OBJEK_PAJAK.NO_URUT', '=', 'SPPT.NO_URUT'],
                ['DAT_OBJEK_PAJAK.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP']
            ])
            ->leftJoin(
                'DAT_SUBJEK_PAJAK',
                'DAT_SUBJEK_PAJAK.SUBJEK_PAJAK_ID',
                '=',
                'DAT_OBJEK_PAJAK.SUBJEK_PAJAK_ID'
            )
            ->where([
                ['SPPT.KD_PROPINSI', '=', $nop['KD_PROPINSI']],
                ['SPPT.KD_DATI2', '=', $nop['KD_DATI2']],
                ['SPPT.KD_KECAMATAN', '=', $nop['KD_KECAMATAN']],
                ['SPPT.KD_KELURAHAN', '=', $nop['KD_KELURAHAN']],
                ['SPPT.KD_BLOK', '=', $nop['KD_BLOK']],
                ['SPPT.NO_URUT', '=', $nop['NO_URUT']],
                ['SPPT.KD_JNS_OP', '=', $nop['KD_JNS_OP']],
            ])
            ->orderByDesc('THN_PAJAK_SPPT')
            ->limit(5)
            ->get();

        return $data;
    }

    // public function getPegawai($nip)
    // {
    //     $data = DB::connection($this->connection)->table($this->table)
    //         ->select(
    //             'PEGAWAI.*'
    //         )
    //         ->where(['NIP' => $nip])
    //         ->first();

    //     return $data;
    // }

    public function scopeGetByNopTahunSppt($query, $nop, $thnSppt)
    {
        return $query->where([
            'kd_propinsi' => substr($nop, 0, 2),
            'kd_dati2' => substr($nop, 2, 2),
            'kd_kecamatan' => substr($nop, 4, 3),
            'kd_kelurahan' => substr($nop, 7, 3),
            'kd_blok' => substr($nop, 10, 3),
            'no_urut' => substr($nop, 13, 4),
            'kd_jns_op' => substr($nop, 17, 1),
            'thn_pajak_sppt' => $thnSppt
        ]);
    }

    public function getNopAttribute()
    {
        return "{$this->kd_propinsi}.{$this->kd_dati2}.{$this->kd_kecamatan}.{$this->kd_kelurahan}.{$this->kd_blok}.{$this->no_urut}.{$this->kd_jns_op}";
    }

    public function scopeWhereNop($query, $nop)
    {
        if (!is_array($nop)) {
            $nop = str_replace('.', '', $nop);
            $nop = [
                'kd_propinsi' => substr($nop, 0, 2),
                'kd_dati2' => substr($nop, 2, 2),
                'kd_kecamatan' => substr($nop, 4, 3),
                'kd_kelurahan' => substr($nop, 7, 3),
                'kd_blok' => substr($nop, 10, 3),
                'no_urut' => substr($nop, 13, 4),
                'kd_jns_op' => substr($nop, 17, 1)
            ];
        }

        return $query->where([
            'kd_propinsi' => $nop['kd_propinsi'],
            'kd_dati2' => $nop['kd_dati2'],
            'kd_kecamatan' => $nop['kd_kecamatan'],
            'kd_kelurahan' => $nop['kd_kelurahan'],
            'kd_blok' => $nop['kd_blok'],
            'no_urut' => $nop['no_urut'],
            'kd_jns_op' => $nop['kd_jns_op'],
        ]);
    }

    public function getCodingsppt($nop, $tahun, $sppt)
    {
        $KD_PROV = substr($nop, 0, 2);
        $KD_DATI2 = substr($nop, 3, 2);
        $KD_KEC = substr($nop, 6, 3);
        $KD_KEL = substr($nop, 10, 3);
        $KD_BLOK = substr($nop, 14, 3);
        $NO_URUT = substr($nop, 18, 4);
        $KD_JNS_OP = substr($nop, 23, 1);

        $res = DB::connection('simpbb')->select("SELECT CODING_SPPT(
            '" . $KD_PROV . "',
            '" . $KD_DATI2 . "',
            '" . $KD_KEC . "',
            '" . $KD_KEL . "',
            '" . $KD_BLOK . "',
            '" . $NO_URUT . "',
            '" . $KD_JNS_OP . "',
            '" . $tahun . "',
            '" . $sppt->kd_kanwil_bank . "',
            '" . $sppt->kd_kppbb_bank . "',
            '" . $sppt->nm_wp_sppt . "',
            '" . $sppt->pbb_yg_harus_dibayar_sppt . "', 'OL') AS codingsppt FROM DUAL");
        return $res;
    }

    public function getCodingsppt2($nop, $tahun, $sppt)
    {
        // try {
        $KD_PROV = $nop['KD_PROPINSI'];
        $KD_DATI2 = $nop['KD_DATI2'];
        $KD_KEC = $nop['KD_KECAMATAN'];
        $KD_KEL = $nop['KD_KELURAHAN'];
        $KD_BLOK = $nop['KD_BLOK'];
        $NO_URUT = $nop['NO_URUT'];
        $KD_JNS_OP = $nop['KD_JNS_OP'];

        $res = DB::connection('simpbb')->select("SELECT CODING_SPPT(
            '" . $KD_PROV . "',
            '" . $KD_DATI2 . "',
            '" . $KD_KEC . "',
            '" . $KD_KEL . "',
            '" . $KD_BLOK . "',
            '" . $NO_URUT . "',
            '" . $KD_JNS_OP . "',
            '" . $tahun . "',
            '" . $sppt->kd_kanwil_bank . "',
            '" . $sppt->kd_kppbb_bank . "',
            '" . $sppt->nm_wp_sppt . "',
            '" . $sppt->pbb_yg_harus_dibayar_sppt . "', 'OL') AS codingsppt FROM DUAL")[0];
        // } catch (\Throwable $th) {
        //     $res['getCodingsppt'] = 0;
        //     //throw $th;
        // }
        return $res;
    }

    public function getAngkaKontrol($nop, $siklus, $tahun)
    {
        return DB::connection('simpbb')->select("SELECT ANGKA_KONTROL(" . $nop . ", " . $siklus . ", " . $tahun . ") AS angka_kontrol FROM DUAL");
    }
}
