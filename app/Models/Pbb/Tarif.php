<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tarif extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'TARIF';
    public $incrementing = false;
    public $timestamps = false;
    protected $primarKey = [
        'KD_PROPINSI',
        'KD_DATI2',
        'THN_AWAL',
        'THN_AKHIR',
        'NJOP_MIN'
    ];
    protected $fillable = [
        'KD_PROPINSI',
        'KD_DATI2',
        'THN_AWAL',
        'THN_AKHIR',
        'NJOP_MIN',
        'NJOP_MAX',
        'NILAI_TARIF'
    ];
}
