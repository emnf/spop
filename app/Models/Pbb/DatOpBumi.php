<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use PDO;

class DatOpBumi extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'dat_op_bumi';
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = [
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_blok',
        'no_urut',
        'kd_jns_op',
        'no_bumi',
        'kd_znt',
        'luas_bumi',
        'jns_bumi',
        'nilai_sistem_bumi',
    ];
    protected $fillable = [
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_blok',
        'no_urut',
        'kd_jns_op',
        'no_bumi',
        'kd_znt',
        'luas_bumi',
        'jns_bumi',
        'nilai_sistem_bumi'
    ];

    public function scopeFindByNop($query, $nop)
    {
        if (!is_array($nop)) {
            $nop = str_replace('.', '', $nop);
            $nop = [
                'kd_propinsi' => substr($nop, 0, 2),
                'kd_dati2' => substr($nop, 2, 2),
                'kd_kecamatan' => substr($nop, 4, 3),
                'kd_kelurahan' => substr($nop, 7, 3),
                'kd_blok' => substr($nop, 10, 3),
                'no_urut' => substr($nop, 13, 4),
                'kd_jns_op' => substr($nop, 17, 1)
            ];
        }

        return $query->where([
            'dat_op_bumi.kd_propinsi' => $nop['kd_propinsi'],
            'dat_op_bumi.kd_dati2' => $nop['kd_dati2'],
            'dat_op_bumi.kd_kecamatan' => $nop['kd_kecamatan'],
            'dat_op_bumi.kd_kelurahan' => $nop['kd_kelurahan'],
            'dat_op_bumi.kd_blok' => $nop['kd_blok'],
            'dat_op_bumi.no_urut' => $nop['no_urut'],
            'dat_op_bumi.kd_jns_op' => $nop['kd_jns_op'],
        ]);
    }

    public function getNopAttribute()
    {
        return "{$this->kd_propinsi}{$this->kd_dati2}{$this->kd_kecamatan}{$this->kd_kelurahan}{$this->kd_blok}{$this->no_urut}{$this->kd_jns_op}";
    }

    public function updateNjopBumi($dataOpBumi)
    {
        $pdo = DB::connection($this->connection)->getPdo();
        $procedureName = 'PENENTUAN_NJOP_BUMI';
        $njopBumi = 0;
        $params = [
            'vlc_kd_propinsi' => $dataOpBumi->KD_PROPINSI,
            'vlc_kd_dati2' => $dataOpBumi->KD_DATI2,
            'vlc_kd_kecamatan' => $dataOpBumi->KD_KECAMATAN,
            'vlc_kd_kelurahan' => $dataOpBumi->KD_KELURAHAN,
            'vlc_kd_blok' => $dataOpBumi->KD_BLOK,
            'vlc_no_urut' => $dataOpBumi->NO_URUT,
            'vlc_kd_jns_op' => $dataOpBumi->KD_JNS_OP,
            'vlc_tahun_pajak' => date('Y'),
            'vln_flag_update' => 1,
        ];

        $stmt = $pdo->prepare("begin " . $procedureName . "(:vlc_kd_propinsi, :vlc_kd_dati2, :vlc_kd_kecamatan, :vlc_kd_kelurahan, :vlc_kd_blok, :vlc_no_urut, :vlc_kd_jns_op, :vlc_tahun_pajak, :vln_flag_update, :vln_njop_bumi); end;");

        $stmt->bindParam(':vlc_kd_propinsi', $params['vlc_kd_propinsi'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_kd_dati2', $params['vlc_kd_dati2'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_kd_kecamatan', $params['vlc_kd_kecamatan'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_kd_kelurahan', $params['vlc_kd_kelurahan'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_kd_blok', $params['vlc_kd_blok'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_no_urut', $params['vlc_no_urut'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_kd_jns_op', $params['vlc_kd_jns_op'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_tahun_pajak', $params['vlc_tahun_pajak'], PDO::PARAM_STR);
        $stmt->bindParam(':vln_flag_update', $params['vln_flag_update'], PDO::PARAM_INT);
        $stmt->bindParam(':vln_njop_bumi', $njopBumi, PDO::PARAM_INT);
        $stmt->execute();
        return;
    }
}
