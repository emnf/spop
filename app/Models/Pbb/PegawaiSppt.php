<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PegawaiSppt extends Model
{
    use HasFactory;
    protected $connection = 'simpbb';
    protected $table = 'PEGAWAI';
}
