<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefUmum extends Model
{
    use HasFactory;
    protected $connection = 'simpbb';
    protected $table = 'REF_UMUM';
}
