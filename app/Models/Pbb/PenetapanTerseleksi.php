<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenetapanTerseleksi extends Model
{
    use HasFactory;
    protected $table = 't_penetapan_terseleksi';
    protected $fillable = [
        'created_by',
        'nip_perekam',
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_blok',
        'no_urut',
        'kd_jns_op',
        'thn_pajak_sppt',
        't_id_jns_data',
        'created_at',
        'updated_at',
    ];

    public function datagrid($filter, $pagination, $sorting, $helper)
    {

        $data = new PenetapanTerseleksi();
        $data = $data->leftjoin('users', 'users.id', 't_penetapan_terseleksi.created_by');
        if ($filter['nip'] != null) {
            $data = $data->where('nip_perekam', $filter['nip']);
        }

        if ($filter['nop'] != null) {
            $nop = $helper->parseNop($filter['nop']);

            $data = $data->where([
                ['kd_propinsi', '=', $nop['KD_PROPINSI']],
                ['kd_dati2', '=', $nop['KD_DATI2']],
                ['kd_kecamatan', '=', $nop['KD_KECAMATAN']],
                ['kd_kelurahan', '=', $nop['KD_KELURAHAN']],
                ['kd_blok', '=', $nop['KD_BLOK']],
                ['no_urut', '=', $nop['NO_URUT']],
                ['kd_jns_op', '=', $nop['KD_JNS_OP']]
            ]);
        }

        if ($filter['tahun'] != null) {
            $data = $data->where('thn_pajak_sppt', $filter['tahun']);
        }

        if ($filter['user'] != null) {
            $data = $data->where('users.name', 'like', '%' . $filter['user'] . '%');
        }
        $data = $data->paginate($pagination['pageSize'], ['*'], 'page', $pagination['pageNumber'] + 1);
        return $data;
    }
}
