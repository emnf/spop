<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatPetaBlok extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'DAT_PETA_BLOK';
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = [
        'KD_PROPINSI',
        'KD_DATI2',
        'KD_KECAMATAN',
        'KD_KELURAHAN',
        'KD_BLOK',
    ];
    protected $fillable = [
        'KD_PROPINSI',
        'KD_DATI2',
        'KD_KECAMATAN',
        'KD_KELURAHAN',
        'KD_BLOK',
        'STATUS_PETA_BLOK'
    ];

    public function scopeFindByKdKecamtanKelurahan($query, $kdKecamatan, $kdKelurahan)
    {
        return $query
            ->where('KD_KECAMATAN', $kdKecamatan)
            ->where('KD_KELURAHAN', $kdKelurahan);
    }
}
