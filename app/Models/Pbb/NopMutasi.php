<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NopMutasi extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'nop_mutasi';
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = [
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_blok',
        'no_urut',
        'kd_jns_op',
        'indeks_mutasi'
    ];
    protected $fillable = [
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_blok',
        'no_urut',
        'kd_jns_op',
        'indeks_mutasi',
        'kd_propinsi_mutasi',
        'kd_dati2_mutasi',
        'kd_kecamatan_mutasi',
        'kd_kelurahan_mutasi',
        'kd_blok_mutasi',
        'no_urut_mutasi',
        'kd_jns_op_mutasi',
        'luas_bumi_mutasi',
        'tgl_rekam_nop_mutasi',
        'nip_perekam_nop_mutasi'
    ];

    public function scopeGetByNop($query, $nop)
    {
        if (!is_array($nop)) {
            $nop = str_replace('.', '', $nop);
            $nop = [
                'kd_propinsi' => substr($nop, 0, 2),
                'kd_dati2' => substr($nop, 2, 2),
                'kd_kecamatan' => substr($nop, 4, 3),
                'kd_kelurahan' => substr($nop, 7, 3),
                'kd_blok' => substr($nop, 10, 3),
                'no_urut' => substr($nop, 13, 4),
                'kd_jns_op' => substr($nop, 17, 1)
            ];
        }

        return $query->where([
            'kd_propinsi' => $nop['kd_propinsi'],
            'kd_dati2' => $nop['kd_dati2'],
            'kd_kecamatan' => $nop['kd_kecamatan'],
            'kd_kelurahan' => $nop['kd_kelurahan'],
            'kd_blok' => $nop['kd_blok'],
            'no_urut' => $nop['no_urut'],
            'kd_jns_op' => $nop['kd_jns_op'],
        ]);
    }
}
