<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class DatFasilitasBangunan extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'dat_fasilitas_bangunan';
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = [
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_blok',
        'no_urut',
        'kd_jns_op',
        'no_bng',
        'kd_fasilitas'
    ];
    protected $fillable = [
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_blok',
        'no_urut',
        'kd_jns_op',
        'no_bng',
        'kd_fasilitas',
        'jml_satuan'
    ];

    public function scopeWhereNop($query, $nop)
    {
        return $query->where([
            'kd_propinsi' => substr($nop, 0, 2),
            'kd_dati2' => substr($nop, 2, 2),
            'kd_kecamatan' => substr($nop, 4, 3),
            'kd_kelurahan' => substr($nop, 7, 3),
            'kd_blok' => substr($nop, 10, 3),
            'no_urut' => substr($nop, 13, 4),
            'kd_jns_op' => substr($nop, 17, 1),
        ]);
    }

    public function scopeWhereNopAndNoBng($query, $nop, $nobng)
    {
        return $query->where([
            'kd_propinsi' => substr($nop, 0, 2),
            'kd_dati2' => substr($nop, 2, 2),
            'kd_kecamatan' => substr($nop, 4, 3),
            'kd_kelurahan' => substr($nop, 7, 3),
            'kd_blok' => substr($nop, 10, 3),
            'no_urut' => substr($nop, 13, 4),
            'kd_jns_op' => substr($nop, 17, 1),
            'no_bng' => $nobng
        ]);
    }

    public function fasilitas()
    {
        return $this->belongsTo(Fasilitas::class, 'kd_fasilitas', 'kd_fasilitas');
    }

    public function bangunan()
    {
        return $this->belongsTo(
            DatFasilitasBangunan::class,
            'kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,no_bng',
            'kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,no_bng',

        );
    }
}
