<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PegawaiPbb extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'PEGAWAI';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'NIP';
    protected $fillable = ['NIP', 'NM_PEGAWAI'];

    public function getPejabat()
    {
        $sql = self::select(
            'PEGAWAI.NM_PEGAWAI',
            'PEGAWAI.NIP_BARU',
            'REF_JABATAN.NM_JABATAN'
        )
            ->leftJoin('POSISI_PEGAWAI', 'POSISI_PEGAWAI.NIP', '=', 'PEGAWAI.NIP')
            ->leftJoin('REF_JABATAN', 'REF_JABATAN.KD_JABATAN', '=', 'POSISI_PEGAWAI.KD_JABATAN')
            ->where('POSISI_PEGAWAI.KD_JABATAN', 10)->first();
        return $sql;
    }
}
