<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SkSismiop extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'sk_sk';
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = ['kd_kanwil', 'kd_kppbb', 'jns_sk', 'no_sk'];
    protected $fillable = [
        'kd_kanwil',
        'kd_kppbb',
        'jns_sk',
        'no_sk',
        'tgl_sk',
        'no_ba_kantor',
        'tgl_ba_kantor',
        'no_ba_lapangan',
        'tgl_ba_lapangan',
        'tgl_cetak_sk',
        'nip_pencetak_sk'
    ];
}
