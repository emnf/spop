<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'PEMBAYARAN_SPPT';
    public $incrementing = false;
    public $timestamps = false;
    protected $keyType = 'string';
    protected $primaryKey = [
        'KD_PROPINSI',
        'KD_DATI2',
        'KD_KECAMATAN',
        'KD_KELURAHAN',
        'KD_BLOK',
        'NO_URUT',
        'KD_JNS_OP',
        'THN_PAJAK_SPPT'
    ];
    protected $fillable = [
        'KD_PROPINSI',
        'KD_DATI2',
        'KD_KECAMATAN',
        'KD_KELURAHAN',
        'KD_BLOK',
        'NO_URUT',
        'KD_JNS_OP',
        'THN_PAJAK_SPPT',
        'PEMBAYARAN_SPPT_KE',
        'KD_KANWIL',
        'KD_KANTOR',
        'KD_TP',
        'DENDA_SPPT',
        'JML_SPPT_YG_DIBAYAR',
        'TGL_PEMBAYARAN_SPPT',
        'TGL_REKAM_BYR_SPPT',
        'NIP_REKAM_BYR_SPPT',
        'GW_REFNUM',
        'KODE_BANK',
        'SWITCHER_ID',
        'SETTLEMENT_DATE',
        'NO_TRANSAKSI_BYR_SPPT_BANK',
        'ISO_MESSAGE_ID',
        'KD_SUMBER_DATA',
        'NO_TRANSAKSI_BYR_SPPT'
    ];
}
