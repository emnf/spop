<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefJpb extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'REF_JPB';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'KD_JPB';
}
