<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatPetaZnt extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'DAT_PETA_ZNT';
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = [
        'KD_PROPINSI',
        'KD_DATI2',
        'KD_KECAMATAN',
        'KD_KELURAHAN',
        'KD_BLOK',
        'KD_ZNT'
    ];
    protected $fillable = [
        'KD_PROPINSI',
        'KD_DATI2',
        'KD_KECAMATAN',
        'KD_KELURAHAN',
        'KD_BLOK',
        'KD_ZNT'
    ];

    public function scopeFindByKdKecamtanKelurahan($query, $kdKecamatan, $kdKelurahan, $kdBlok)
    {
        return $query
            ->where('KD_KECAMATAN', $kdKecamatan)
            ->where('KD_KELURAHAN', $kdKelurahan)
            ->where('KD_BLOK', $kdBlok);
    }
}
