<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'REF_KECAMATAN';
    public $incrementing = false;
    protected $primaryKey = ['KD_PROPINSI', 'KD_DATI2', 'KD_KECAMATAN'];

    public function scopeByDati($query, $dati2)
    {
        return $query->where('KD_PROPINSI', substr($dati2, 0, 2))->where('KD_DATI2', substr($dati2, 2, 2));
    }

    public function scopeByKecamatanId($query, $kecamatanId)
    {
        return $query->where('KD_PROPINSI', substr($kecamatanId, 0, 2))
            ->where('KD_DATI2', substr($kecamatanId, 2, 2))
            ->where('KD_KECAMATAN', substr($kecamatanId, 4, 3));
    }
}
