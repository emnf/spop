<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fasilitas extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'fasilitas';
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = 'kd_fasilitas';
}
