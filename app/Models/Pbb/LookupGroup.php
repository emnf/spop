<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LookupGroup extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'LOOKUP_GROUP';
    protected $primaryKey = 'KD_LOOKUP_GROUP';
}
