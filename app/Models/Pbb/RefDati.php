<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefDati extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'REF_DATI2';
    public $incrementing = false;
    protected $primaryKey = ['KD_PROPINSI', 'KD_DATI2'];
    // protected $fillable = ['KD_PROPINSI', 'KD_DATI2'];

    public function getKodeKabkotaAttribute()
    {
        // return $this->KD_PROPINSI;
        return "{$this->kd_propinsi}{$this->kd_dati2}";
    }
}
