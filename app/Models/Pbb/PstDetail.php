<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PstDetail extends Model
{
    use HasFactory;


    protected $connection = 'simpbb';
    protected $table = 'pst_detail';
    public $incrementing = false;
    public $timestamps = false;

    /**
     * untuk SIMPBB gunakan KD_KANTOR
     * untuk SISMIOP gunakan KD_KPPBB, patikan dulu denngan database
     */
    protected $primaryKey = [
        'kd_kanwil',
        'kd_kppbb',
        'thn_pelayanan',
        'bundel_pelayanan',
        'no_urut_pelayanan',
        'kd_propinsi_pemohon',
        'kd_dati2_pemohon',
        'kd_kecamatan_pemohon',
        'kd_kelurahan_pemohon',
        'kd_blok_pemohon',
        'no_urut_pemohon',
        'kd_jns_op_pemohon',
    ];
    protected $fillable = [
        'kd_kanwil',
        'kd_kppbb',
        'thn_pelayanan',
        'bundel_pelayanan',
        'no_urut_pelayanan',
        'kd_propinsi_pemohon',
        'kd_dati2_pemohon',
        'kd_kecamatan_pemohon',
        'kd_kelurahan_pemohon',
        'kd_blok_pemohon',
        'no_urut_pemohon',
        'kd_jns_op_pemohon',
        'kd_jns_pelayanan',
        'thn_pajak_permohonan',
        'nama_penerima',
        'catatan_penyerahan',
        'status_selesai',
        'tgl_selesai',
        'kd_seksi_berkas',
        'tgl_penyerahan',
        'nip_penyerah',
    ];
}
