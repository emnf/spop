<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LookupItem extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'LOOKUP_ITEM';
    public $incrementing = false;
    protected $primaryKey = ['KD_LOOKUP_GROUP', 'KD_LOOKUP_ITEM'];

    public function scopeByLookupItemId($query, $kdLookupItem)
    {
        return $query->where([
            ['KD_LOOKUP_GROUP', '=', substr($kdLookupItem, 0, 2)],
            ['KD_LOOKUP_ITEM', '=', substr($kdLookupItem, 2, 1)]
        ]);
    }
}
