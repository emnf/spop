<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class HistOpBumi extends Model
{
    protected $connection = 'simpbb';
    protected $table = 'hist_dat_op_bumi';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_blok',
        'no_urut',
        'kd_jns_op',
        'hist_ke',
        'no_bumi',
        'kd_znt',
        'luas_bumi',
        'jns_bumi',
        'nilai_sistem_bumi',
        'nip_rekam_hist',
        'tgl_rekam_hist'
    ];
    use HasFactory;

    public function saveData($dataOpBumiLama, $nip)
    {

        // if (!Schema::hasTable('hist_dat_op_bumi')) {
        //     $sql="CREATE TABLE HIST_DAT_OP_BUMI (
        //           KD_PROPINSI CHAR (2) NOT NULL,
        //           KD_DATI2 CHAR (2) NOT NULL,
        //           KD_KECAMATAN CHAR(3) NOT NULL,
        //           KD_KELURAHAN CHAR(3) NOT NULL,
        //           KD_BLOK CHAR(3) NOT NULL,
        //           NO_URUT CHAR(4) NOT NULL,
        //           KD_JNS_OP CHAR(1) NOT NULL,
        //           HIST_KE NUMBER(2,0) NOT NULL,
        //           NO_BUMI NUMBER(2) DEFAULT 1 NOT NULL,
        //           KD_ZNT CHAR(2),
        //           LUAS_BUMI NUMBER(12) DEFAULT 0,
        //           JNS_BUMI CHAR(1) DEFAULT '1',
        //           NILAI_SISTEM_BUMI NUMBER(15) DEFAULT 0,
        //           NIP_REKAM_HIST CHAR(18) NOT NULL,
        //           TGL_REKAM_HIST DATE DEFAULT SYSDATE
        //           )";
        //     DB::statement($sql);
        // }
        // die;
        $cekHis = HistOpBumi::where([
            'kd_propinsi' => $dataOpBumiLama->kd_propinsi,
            'kd_dati2' => $dataOpBumiLama->kd_dati2,
            'kd_kecamatan' => $dataOpBumiLama->kd_kecamatan,
            'kd_kelurahan' => $dataOpBumiLama->kd_kelurahan,
            'kd_blok' => $dataOpBumiLama->kd_blok,
            'no_urut' => $dataOpBumiLama->no_urut,
            'kd_jns_op' => $dataOpBumiLama->kd_jns_op,
        ])->orderBy('hist_ke', 'DESC')->first();
        $data = [
            'kd_propinsi' => $dataOpBumiLama->kd_propinsi,
            'kd_dati2' => $dataOpBumiLama->kd_dati2,
            'kd_kecamatan' => $dataOpBumiLama->kd_kecamatan,
            'kd_kelurahan' => $dataOpBumiLama->kd_kelurahan,
            'kd_blok' => $dataOpBumiLama->kd_blok,
            'no_urut' => $dataOpBumiLama->no_urut,
            'kd_jns_op' => $dataOpBumiLama->kd_jns_op,
            'hist_ke' => (!empty($cekHis)) ? ($cekHis->hist_ke + 1) : 1,
            'no_bumi' => $dataOpBumiLama->no_bumi,
            'kd_znt' => $dataOpBumiLama->kd_znt,
            'luas_bumi' => $dataOpBumiLama->luas_bumi,
            'jns_bumi' => $dataOpBumiLama->jns_bumi,
            'nilai_sistem_bumi' => $dataOpBumiLama->nilai_sistem_bumi,
            'nip_rekam_hist' => $nip,
            'tgl_rekam_hist' => date('Y-m-d H:i:s')

        ];
        // dd($data);
        return HistOpBumi::create($data);
    }
}
