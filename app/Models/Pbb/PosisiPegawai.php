<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PosisiPegawai extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'POSISI_PEGAWAI';
    public $incrementing = false;
    protected $primaryKey = [
        'KD_KANWIL',
        'KD_KANTOR',
        'NIP'
    ];
    protected $fillable = [
        'KD_KANWIL',
        'KD_KANTOR',
        'NIP',
        'KD_SEKSI',
        'TGL_AWAL_BERLAKU',
        'TGL_AKHIR_BERLAKU',
        'KD_WEWENANG',
        'KD_JABATAN'
    ];
}
