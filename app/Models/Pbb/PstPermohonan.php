<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PstPermohonan extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'pst_permohonan';
    public $incrementing = false;
    public $timestamps = false;

    /**
     * untuk SIMPBB gunakan KD_KANTOR
     * untuk SISMIOP gunakan KD_KPPBB, patikan dulu denngan database
     */
    protected $primaryKey = [
        'kd_kanwil',
        'kd_kppbb',
        'thn_pelayanan',
        'bundel_pelayanan',
        'no_urut_pelayanan',
    ];
    protected $fillable = [
        'kd_kanwil',
        'kd_kppbb',
        'thn_pelayanan',
        'bundel_pelayanan',
        'no_urut_pelayanan',
        'no_srt_permohonan',
        'tgl_surat_permohonan',
        'nama_pemohon',
        'alamat_pemohon',
        'keterangan_pst',
        'catatan_pst',
        'status_kolektif',
        'tgl_terima_dokumen_wp',
        'tgl_perkiraan_selesai',
        'nip_penerima'
    ];
}
