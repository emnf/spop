<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefKantor extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'ref_kantor';
    public $incrementing = false;
    protected $primaryKey = ['kd_kanwil', 'kd_kantor'];

    public static function getKdkanwilKdKantor()
    {
        return self::select('kd_kanwil', 'kd_kantor')->first()->toArray();
    }
}
