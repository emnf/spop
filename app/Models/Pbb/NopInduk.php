<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NopInduk extends Model
{
    protected $connection = 'simpbb';
    protected $table = 'nop_induk';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'kd_propinsi_induk',
        'kd_dati2_induk',
        'kd_kecamatan_induk',
        'kd_kelurahan_induk',
        'kd_blok_induk',
        'no_urut_induk',
        'kd_jns_op_induk',
        'luas_bumi_induk',
        'indeks_mutasi',
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_blok',
        'no_urut',
        'kd_jns_op',
        'jns_transaksi',
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_blok',
        'no_urut',
        'kd_jns_op',
        'luas_bumi',
        'keterangan_spop',
        'keterangan_op',
        'tgl_rekam_nop_induk',
        'nip_perekam_nop_induk'
    ];
    use HasFactory;
    public function saveData($oPLama, $histOpBumi = null, $luas, $nopBaru)
    {
        $keteranganSpop = '';
        $keteranganOp = '';
        if (!empty($oPLama->keterangan_spop)) {
            $keteranganSpop = $oPLama->keterangan_spop;
        }
        if (!empty($oPLama->keterangan_op)) {
            $keteranganOp = $oPLama->keterangan_op;
        }

        $data = [
            'kd_propinsi_induk' => $oPLama->kd_propinsi,
            'kd_dati2_induk' => $oPLama->kd_dati2,
            'kd_kecamatan_induk' => $oPLama->kd_kecamatan,
            'kd_kelurahan_induk' => $oPLama->kd_kelurahan,
            'kd_blok_induk' => $oPLama->kd_blok,
            'no_urut_induk' => $oPLama->no_urut,
            'kd_jns_op_induk' => $oPLama->kd_jns_op,
            'luas_bumi_induk' => $oPLama->total_luas_bumi,
            'indeks_mutasi' => $histOpBumi->hist_ke,
            'kd_propinsi' => $nopBaru->kd_propinsi,
            'kd_dati2' => $nopBaru->kd_dati2,
            'kd_kecamatan' => $nopBaru->kd_kecamatan,
            'kd_kelurahan' => $nopBaru->kd_kelurahan,
            'kd_blok' => $nopBaru->kd_blok,
            'no_urut' => $nopBaru->no_urut,
            'kd_jns_op' => $nopBaru->kd_jns_op,
            'luas_bumi' => $nopBaru->luas_bumi,

            'keterangan_spop' => $keteranganSpop,
            'keterangan_op' => $keteranganOp,
            'tgl_rekam_nop_induk' => $oPLama->tgl_pendataan_op,
            'nip_perekam_nop_induk' => $oPLama->nip_pendata,
            // 'user_elayanan'=>'989898989898989898'

        ];
        return NopInduk::create($data);
    }
}
