<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'REF_KELURAHAN';
    public $incrementing = false;
    protected $primaryKey = ['KD_PROPINSI', 'KD_DATI2', 'KD_KECAMATAN', 'KD_KELURAHAN'];

    public function scopeByKecamatanId($query, $kecmatanId)
    {
        return $query->where('KD_PROPINSI', substr($kecmatanId, 0, 2))
            ->where('KD_DATI2', substr($kecmatanId, 2, 2))
            ->where('KD_KECAMATAN', substr($kecmatanId, 4, 3));
    }

    public function scopeByKelurahanId($query, $kelurahanId)
    {
        return $query->where('KD_PROPINSI', substr($kelurahanId, 0, 2))
            ->where('KD_DATI2', substr($kelurahanId, 2, 2))
            ->where('KD_KECAMATAN', substr($kelurahanId, 4, 3))
            ->where('KD_KELURAHAN', substr($kelurahanId, 7, 3));
    }
}
