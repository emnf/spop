<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DatObjekPajak extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'dat_objek_pajak';
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = [
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_blok',
        'no_urut',
        'kd_jns_op'
    ];

    protected $fillable = [
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_blok',
        'no_urut',
        'kd_jns_op',
        'subjek_pajak_id',
        'no_formulir_spop',
        'no_persil',
        'jalan_op',
        'blok_kav_no_op',
        'rw_op',
        'rt_op',
        'kd_status_cabang',
        'kd_status_wp',
        'total_luas_bumi',
        'total_luas_bng',
        'njop_bumi',
        'njop_bng',
        'status_peta_op',
        'jns_transaksi_op',
        'tgl_pendataan_op',
        'nip_pendata',
        'tgl_pemeriksaan_op',
        'nip_pemeriksa_op',
        'tgl_perekaman_op',
        'nip_perekam_op',
        'no_sertifikat',
        // 'user_elayanan',
        // 'latitude',
        // 'longitude'
    ];

    public function scopeWhereNop($query, $nop)
    {
        if (!is_array($nop)) {
            $nop = str_replace('.', '', $nop);
            $nop = [
                'kd_propinsi' => substr($nop, 0, 2),
                'kd_dati2' => substr($nop, 2, 2),
                'kd_kecamatan' => substr($nop, 4, 3),
                'kd_kelurahan' => substr($nop, 7, 3),
                'kd_blok' => substr($nop, 10, 3),
                'no_urut' => substr($nop, 13, 4),
                'kd_jns_op' => substr($nop, 17, 1)
            ];
        }

        return $query->where([
            'dat_objek_pajak.kd_propinsi' => $nop['kd_propinsi'],
            'dat_objek_pajak.kd_dati2' => $nop['kd_dati2'],
            'dat_objek_pajak.kd_kecamatan' => $nop['kd_kecamatan'],
            'dat_objek_pajak.kd_kelurahan' => $nop['kd_kelurahan'],
            'dat_objek_pajak.kd_blok' => $nop['kd_blok'],
            'dat_objek_pajak.no_urut' => $nop['no_urut'],
            'dat_objek_pajak.kd_jns_op' => $nop['kd_jns_op'],
        ]);
    }

    public function subjekPajak()
    {
        return $this->belongsTo(DatSubjekPajak::class, 'subjek_pajak_id', 'subjek_pajak_id');
    }

    public function opBumi()
    {
        return $this->belongsTo(
            DatOpBumi::class,
            ['kd_propinsi', 'kd_dati2', 'kd_kecamatan', 'kd_kelurahan', 'kd_blok', 'no_urut', 'kd_jns_op'],
            ['kd_propinsi', 'kd_dati2', 'kd_kecamatan', 'kd_kelurahan', 'kd_blok', 'no_urut', 'kd_jns_op']
        );
    }

    public function getNopAttribute()
    {
        $nop = "{$this->KD_PROPINSI}{$this->KD_DATI2}{$this->KD_KECAMATAN}{$this->KD_KELURAHAN}{$this->KD_BLOK}{$this->NO_URUT}{$this->KD_JNS_OP}";
        $nop2 = "{$this->kd_propinsi}{$this->kd_dati2}{$this->kd_kecamatan}{$this->kd_kelurahan}{$this->kd_blok}{$this->no_urut}{$this->kd_jns_op}";

        return empty($nop) ? $nop2 : $nop;
    }

    public function getNopFormatedAttribute()
    {
        return "{$this->kd_propinsi}.{$this->kd_dati2}.{$this->kd_kecamatan}.{$this->kd_kelurahan}.{$this->kd_blok}.{$this->no_urut}.{$this->kd_jns_op}";
    }

    public function getDataOpWpKecKel($nop)
    {
        $data = DB::connection($this->connection)->select(DB::raw(
            "SELECT  TO_CHAR(TRIM(DAT_SUBJEK_PAJAK.SUBJEK_PAJAK_ID)) AS SUBJEK_PAJAK_ID,
        DAT_SUBJEK_PAJAK.*,
        REF_KELURAHAN.*,
        REF_KECAMATAN.*,
        DAT_OBJEK_PAJAK.*,
        DAT_OP_BUMI.*

        FROM DAT_OBJEK_PAJAK
        LEFT JOIN DAT_SUBJEK_PAJAK ON DAT_SUBJEK_PAJAK.SUBJEK_PAJAK_ID = DAT_OBJEK_PAJAK.SUBJEK_PAJAK_ID

        LEFT JOIN DAT_OP_BUMI ON
        DAT_OP_BUMI.KD_PROPINSI = DAT_OBJEK_PAJAK.KD_PROPINSI
        AND DAT_OP_BUMI.KD_DATI2 = DAT_OBJEK_PAJAK.KD_DATI2
        AND DAT_OP_BUMI.KD_KECAMATAN = DAT_OBJEK_PAJAK.KD_KECAMATAN
        AND DAT_OP_BUMI.KD_KELURAHAN = DAT_OBJEK_PAJAK.KD_KELURAHAN
        AND DAT_OP_BUMI.KD_BLOK = DAT_OBJEK_PAJAK.KD_BLOK
        AND DAT_OP_BUMI.NO_URUT = DAT_OBJEK_PAJAK.NO_URUT
        AND DAT_OP_BUMI.KD_JNS_OP = DAT_OBJEK_PAJAK.KD_JNS_OP

        LEFT JOIN REF_KECAMATAN ON
        REF_KECAMATAN.KD_PROPINSI = DAT_OBJEK_PAJAK.KD_PROPINSI
        AND REF_KECAMATAN.KD_DATI2 = DAT_OBJEK_PAJAK.KD_DATI2
        AND REF_KECAMATAN.KD_KECAMATAN = DAT_OBJEK_PAJAK.KD_KECAMATAN

        LEFT JOIN REF_KELURAHAN ON
        REF_KELURAHAN.KD_PROPINSI = DAT_OBJEK_PAJAK.KD_PROPINSI
        AND REF_KELURAHAN.KD_DATI2 = DAT_OBJEK_PAJAK.KD_DATI2
        AND REF_KELURAHAN.KD_KECAMATAN = DAT_OBJEK_PAJAK.KD_KECAMATAN
        AND REF_KELURAHAN.KD_KELURAHAN = DAT_OBJEK_PAJAK.KD_KELURAHAN

        WHERE

        DAT_OBJEK_PAJAK.KD_PROPINSI = '" . $nop['KD_PROPINSI'] . "'
        AND DAT_OBJEK_PAJAK.KD_DATI2 = '" . $nop['KD_DATI2'] . "'
        AND DAT_OBJEK_PAJAK.KD_KECAMATAN = '" . $nop['KD_KECAMATAN'] . "'
        AND DAT_OBJEK_PAJAK.KD_KELURAHAN = '" . $nop['KD_KELURAHAN'] . "'
        AND DAT_OBJEK_PAJAK.KD_BLOK = '" . $nop['KD_BLOK'] . "'
        AND DAT_OBJEK_PAJAK.NO_URUT = '" . $nop['NO_URUT'] . "'
        AND DAT_OBJEK_PAJAK.KD_JNS_OP = '" . $nop['KD_JNS_OP'] . "'
        "
        ))[0];

        return $data;
    }
}
