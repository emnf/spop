<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DafnomOp extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'dafnom_op';
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = [
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_blok',
        'no_urut',
        'kd_jns_op'
    ];

    protected $fillable = [
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_blok',
        'no_urut',
        'kd_jns_op',
        'jalan_op',
        'blok_kav_no_op',
        'rw_op',
        'rt_op',
        'jns_bumi',
        'kd_jpb',
        'kd_status_wp',
        'kategori_op',
        'keterangan',
        'no_formulir',
        'tgl_pembentukan',
        'nip_pembentuk',
        'tgl_pemutakhiran',
        'nip_pemutakhir',
        'thn_pembentukan',
    ];

    public function scopeWhereNop($query, $nop)
    {
        if (!is_array($nop)) {
            $nop = str_replace('.', '', $nop);
            $nop = [
                'kd_propinsi' => substr($nop, 0, 2),
                'kd_dati2' => substr($nop, 2, 2),
                'kd_kecamatan' => substr($nop, 4, 3),
                'kd_kelurahan' => substr($nop, 7, 3),
                'kd_blok' => substr($nop, 10, 3),
                'no_urut' => substr($nop, 13, 4),
                'kd_jns_op' => substr($nop, 17, 1)
            ];
        }

        return $query->where([
            'kd_propinsi' => $nop['kd_propinsi'],
            'kd_dati2' => $nop['kd_dati2'],
            'kd_kecamatan' => $nop['kd_kecamatan'],
            'kd_kelurahan' => $nop['kd_kelurahan'],
            'kd_blok' => $nop['kd_blok'],
            'no_urut' => $nop['no_urut'],
            'kd_jns_op' => $nop['kd_jns_op'],
        ]);
    }
}
