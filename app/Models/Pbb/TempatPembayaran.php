<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TempatPembayaran extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'TEMPAT_PEMBAYARAN';
    public $incrementing = false;
    protected $primaryKey = [
        'KD_KANWIL',
        'KD_KANTOR',
        'KD_TP'
    ];
    protected $fillable = [
        'KD_KANWIL',
        'KD_KANTOR',
        'KD_TP',
        'NM_TP',
        'ALAMAT_TP',
        'NO_REK_TP'
    ];
}
