<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TempatPembayaranSpptMasal extends Model
{
    use HasFactory;
    protected $connection = "simpbb";
    protected $table = 'tempat_pembayaran_sppt_masal';

    protected $primaryKey = 'thn_tp_sppt_masal';

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];
}
