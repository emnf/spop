<?php

namespace App\Models\Hasil;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class HasilVerlapWp extends Model
{
    use HasFactory, LogsActivity;

    protected $connection = 'pgsql';
    protected $table = 't_hasil_verlap_wp';
    protected $primaryKey = 't_id_wp';
    protected $fillable = [
        't_id_pelayanan',
        't_nop',
        't_nama_wp',
        't_nik_wp',
        't_jalan_wp',
        't_rt_wp',
        't_rw_wp',
        't_kelurahan_wp',
        't_kecamatan_wp',
        't_kabupaten_wp',
        't_no_hp_wp',
        't_email',
        't_npwpd',
        'created_by'
    ];

    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = [ 'created_at', 'updated_at'];
    protected static $logOnlyDirty = true;
}
