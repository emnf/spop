<?php

namespace App\Models\Hasil;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class HasilVerlapOpBaru extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 't_hasil_verlap_op_baru';
    protected $primaryKey = 't_id';
    protected $fillable = [
        't_id_pelayanan',
        't_no_shm',
        't_tgl_shm',
        't_luas_tanah_shm',
        't_atas_nama_shm',
        't_nop_terdekat',
        't_kode_blok',
        't_kode_znt',
        't_kelas_tanah',
    ];

    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = [ 'created_at', 'updated_at'];
    protected static $logOnlyDirty = true;
}
