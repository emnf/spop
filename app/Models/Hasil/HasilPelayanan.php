<?php

namespace App\Models\Hasil;

use App\Models\Pelayanan\Pelayanan;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class HasilPelayanan extends Model
{
    use HasFactory, LogsActivity;

    const SEMUA_DATA = 1;
    const BELUM_UPLOAD = 2;
    const SUDAH_UPLOAD = 3;

    protected $table = 't_hasil_pelayanan';
    protected $primaryKey = 't_id_sk';
    protected $fillable = [
        't_id_pelayanan',
        't_id_validasi',
        't_tgl_sk',
        't_no_sk',
        't_keterangan_sk',
        't_lokasi_file',
        'created_by',
        't_no_urut',
    ];

    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = ['created_at', 'updated_at'];
    protected static $logOnlyDirty = true;

    public static function maxNoUrut()
    {
        return self::whereRaw("EXTRACT(YEAR FROM created_at) = '" . date('Y') . "'")->max('t_no_urut');
    }

    public function daatgrid($filter, $pagination, $sorting, $status)
    {
        return Pelayanan::select(
            't_pelayanan.t_id_pelayanan',
            't_pelayanan.t_no_pelayanan',
            't_pelayanan.t_tgl_pelayanan',
            't_pelayanan.t_nama_pemohon',
            't_pelayanan.t_nik_pemohon',
            't_pelayanan.t_nik_pemohon',
            't_pelayanan.t_id_jenis_pelayanan',
            't_pelayanan.uuid',
            't_hasil_pelayanan.t_id_sk',
            't_hasil_pelayanan.t_tgl_sk',
            't_hasil_pelayanan.t_no_sk',
            't_hasil_pelayanan.t_keterangan_sk',
            't_hasil_pelayanan.t_lokasi_file',
            's_jenis_pelayanan.s_nama_jenis_pelayanan',
            's_jenis_pajak.s_nama_singkat_pajak',
        )
            ->leftJoin('t_pelayanan_approve', 't_pelayanan_approve.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_validasi', 't_validasi.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin($this->table, $this->table . '.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('s_jenis_pelayanan', 's_jenis_pelayanan.s_id_jenis_pelayanan', '=', 't_pelayanan.t_id_jenis_pelayanan')
            ->leftJoin('s_jenis_pajak', 's_jenis_pajak.s_id_jenis_pajak', '=', 't_pelayanan.t_id_jenis_pajak')
            ->where('t_validasi.t_id_status_validasi', 4)
            ->when($status == self::SUDAH_UPLOAD, function ($query) {
                $query->whereNotNull('t_hasil_pelayanan.t_id_sk')
                    ->orderBy('t_hasil_pelayanan.created_at', 'desc');
            })
            ->when($status == self::BELUM_UPLOAD, function ($query) {
                $query->whereNull('t_hasil_pelayanan.t_id_sk')
                    ->orderBy('t_tgl_pelayanan', 'desc');
            })
            ->when(!empty($filter['tglPelayanan']), function ($query) use ($filter) {
                $date = explode(' - ', $filter['tglPelayanan']);
                $startDate = date('Y-m-d', strtotime($date[0]));
                $endDate = date('Y-m-d', strtotime($date[1]));
                $query->whereBetween('t_pelayanan.t_tgl_pelayanan', [$startDate, $endDate]);
            })
            ->when(!empty($filter['idJenisPelayanan']), function ($query) use ($filter) {
                return $query->where('t_pelayanan.t_id_jenis_pelayanan', $filter['idJenisPelayanan']);
            })
            ->when(!empty($filter['noPelayanan']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_no_pelayanan',  'like', "%" . $filter['noPelayanan'] . "%");
            })
            ->when(!empty($filter['namaPemohon']), function ($query) use ($filter) {
                return $query->where('t_pelayanan.t_nama_pemohon', 'ilike', "%" . $filter['namaPemohon'] . "%");
            })
            ->when(!empty($filter['nikPemohon']), function ($query) use ($filter) {
                return $query->where('t_pelayanan.t_nik_pemohon', 'ilike', "%" . $filter['nikPemohon'] . "%");
            })
            ->paginate($pagination['pageSize'], ['*'], 'page', $pagination['pageNumber'] + 1);
    }
}
