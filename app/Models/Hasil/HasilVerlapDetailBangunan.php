<?php

namespace App\Models\Hasil;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class HasilVerlapDetailBangunan extends Model
{
    use HasFactory, LogsActivity;

    protected $connection = 'pgsql';
    protected $table = 't_hasil_verlap_detail_bangunan';
    protected $primaryKey = 't_id_detail_bangunan';
    protected $fillable = [
        't_id_pelayanan',
        't_jenis_penggunaan_bangunan',
        't_tahun_bangunan',
        't_tahun_renovasi',
        't_kondisi_bangunan',
        't_konstruksi',
        't_atap',
        't_dinding',
        't_lantai',
        't_langit_langit',
        't_ac_split',
        't_ac_window',
        't_panjang_pagar',
        't_bahan_pagar',
        't_jumlah_lantai',
        't_luas',
        't_no_urut_bangunan',
        't_listrik',
        'created_by',
        't_id_objek'
    ];

    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = [ 'created_at', 'updated_at'];
    protected static $logOnlyDirty = true;

    public function getMaxNourut($pelayananId)
    {
        return self::where('t_id_pelayanan', $pelayananId)->max('t_no_urut_bangunan') + 1;
    }
}
