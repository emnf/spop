<?php

namespace App\Models\Hasil;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class HasilVerlap extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 't_hasil_verlap';
    protected $primaryKey = 't_id';
    protected $fillable = [
        't_id_pelayanan',
        't_no',
        't_tgl_penelitian',
        't_tempat_penelitian',
    ];

    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = [ 'created_at', 'updated_at'];
    protected static $logOnlyDirty = true;

    public static function maxHasilVerlap($tahun)
    {
        return self::whereRaw("EXTRACT(YEAR from t_tgl_penelitian) = ?", $tahun)->max('t_no');
    }
}
