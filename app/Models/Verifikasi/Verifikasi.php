<?php

namespace App\Models\Verifikasi;

use App\Models\Pelayanan\Pelayanan;
use App\Models\Status\StatusVerifikasi;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Verifikasi extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 't_verifikasi';
    protected $primaryKey = 't_id_verifikasi';
    protected $fillable = [
        't_id_pelayanan',
        't_tgl_verifikasi',
        't_no_verifikasi',
        't_id_status_verifikasi',
        't_keterangan_verifikasi',
        'created_by',
        't_checklist_persyaratan',
        't_kejelasan',
        't_kebenaran',
        't_keabsahan'
    ];

    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = [ 'created_at', 'updated_at'];
    protected static $logOnlyDirty = true;

    public function pelayanan()
    {
        return $this->belongsTo(Pelayanan::class, 't_id_pelayanan');
    }

    public function statusVerifikasi()
    {
        return $this->belongsTo(StatusVerifikasi::class, 't_id_status_verifikasi', 's_id_status_verifikasi');
    }

    public static function maxNoUrut()
    {
        return self::whereRaw("EXTRACT(YEAR FROM t_tgl_verifikasi) = '". date('Y') ."'")->max('t_no_verifikasi');
    }
}
