<?php

namespace App\Models\Verifikasi;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class VerifikasiLapanagan extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 't_verifikasi_lapangan';
    protected $primaryKey = 't_id_verifikasi_lapangan';
    protected $fillable = [
        't_id_pelayanan',
        't_tgl_verifikasi_lapangan',
        't_no_verifikasi_lapangan',
        't_id_status_verifikasi_lapangan',
        't_keterangan_verifikasi_lapangan',
        'created_by'
    ];

    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = [ 'created_at', 'updated_at'];
    protected static $logOnlyDirty = true;

    public function pelayanan()
    {
        return $this->belongsTo(Pelayanan::class, 't_id_pelayanan');
    }

    public static function maxNoUrut()
    {
        return self::whereRaw("EXTRACT(YEAR FROM t_tgl_verifikasi_lapangan) = '". date('Y') ."'")->max('t_no_verifikasi_lapangan');
    }
}
