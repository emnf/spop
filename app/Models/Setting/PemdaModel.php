<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PemdaModel extends Model
{
    use HasFactory, LogsActivity;

    protected $connection = 'pgsql';
    protected $table = 's_pemda';
    protected $primaryKey = 's_id_pemda';
    protected $fillable = [
        's_nama_prov',
        's_nama_kabkota',
        's_nama_ibukota_kabkota',
        's_kode_provinsi',
        's_kode_kabkot',
        's_nama_instansi',
        's_nama_singkat_instansi',
        's_alamat_instansi',
        's_notelp_instansi',
        's_logo',
        's_kode_pos',
        's_latitude',
        's_longitude'
    ];

    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = ['created_at', 'updated_at'];
    protected static $logOnlyDirty = true;
}
