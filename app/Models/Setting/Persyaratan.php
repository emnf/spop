<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Persyaratan extends Model
{
    use HasFactory;

    protected $connection = 'pgsql';
    protected $table = 's_persyaratan';
    protected $primaryKey = 's_id_persyaratan';
    protected $fillable = [
        's_id_jenis_pelayanan',
        's_nama_persyaratan',
        's_is_optional'
    ];

    public function datagrid($request)
    {
        $response = (new Persyaratan())->with('dataJenisPelayanan');
        $response = !empty($request['sorting']) ? $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']) : $response->orderBy('s_id_jenis_pelayanan');
        $response = ($request['filter']['s_nama_persyaratan'] != '') ? $response->where('s_nama_persyaratan', 'ilike', "%" . $request['filter']['s_nama_persyaratan'] . "%") : $response;
        $response = ($request['filter']['s_id_jenis_pelayanan'] != '') ? $response->where('s_id_jenis_pelayanan', $request['filter']['s_id_jenis_pelayanan']) : $response;
        $response = ($request['filter']['s_is_optional'] != '') ? $response->where('s_is_optional', $request['filter']['s_is_optional']) : $response;
        return $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
    }

    public function dataJenisPelayanan()
    {
        return $this->hasOne(JenisPelayanan::class, 's_id_jenis_pelayanan', 's_id_jenis_pelayanan');
    }

}
