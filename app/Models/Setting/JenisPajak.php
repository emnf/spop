<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisPajak extends Model
{
    use HasFactory;

    protected $connection = 'pgsql';
    protected $table = 's_jenis_pajak';
    protected $primaryKey = 's_id_jenis_pajak';
    protected $fillable = [
        's_nama_jenis_pajak',
        's_nama_singkat_pajak',
        's_id_kategori_pajak',
        's_active',
        's_icon',
        'created_at',
        'updated_at'
    ];

    public function scopeIsActive($query)
    {
        return $query->where('s_active', true);
    }

    public function dataKategoriPajak()
    {
        return $this->belongsTo(JenisKategoriPajak::class, 's_id_kategori_pajak', 's_id_kategori_pajak');
    }

    public function dataJenisPelayanan()
    {
        return $this->hasMany(JenisPelayanan::class, 's_id_jenis_pajak', 's_id_jenis_pajak');
    }

    public function datagrid($request)
    {
        $response = (new JenisPajak())->with('dataKategoriPajak');
        $response = !empty($request['sorting']) ? $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']) : $response->orderBy('s_id_jenis_pajak');
        $response = ($request['filter']['s_nama_jenis_pajak'] != '') ? $response->where('s_nama_jenis_pajak', 'ilike', "%" . $request['filter']['s_nama_jenis_pajak'] . "%") : $response;
        $response = ($request['filter']['s_id_kategori_pajak'] != '') ? $response->where('s_id_kategori_pajak', $request['filter']['s_id_kategori_pajak']) : $response;
        $response = ($request['filter']['s_nama_singkat_pajak'] != '') ? $response->where('s_nama_singkat_pajak', 'ilike', "%" . $request['filter']['s_nama_singkat_pajak'] . "%") : $response;
        $response = ($request['filter']['s_active'] != '') ? $response->where('s_active', $request['filter']['s_active']) : $response;
        return $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
    }
}
