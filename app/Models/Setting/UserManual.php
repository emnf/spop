<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class UserManual extends Model
{
    use HasFactory, LogsActivity;

    protected $connection = 'pgsql';
    protected $table = 's_user_manual';
    protected $primaryKey = 's_id_usermanual';
    protected $fillable = [
        's_nama_menu',
        's_nama_file',
        's_lokasi_file',
        'created_at',
        'updated_at'
    ];

    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = ['updated_at','created_at'];
    protected static $logOnlyDirty = true;

    public function datagrid($request){
        $response = new UserManual();
        $response = !empty($request['sorting']) ? $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']) : $response->orderBy('s_id_usermanual');
        $response = ($request['filter']['s_nama_menu'] != '') ? $response->where('s_nama_menu', 'ilike', "%" . $request['filter']['s_nama_menu'] . "%") : $response;
        $response = ($request['filter']['s_nama_file'] != '') ? $response->where('s_nama_file', 'ilike', "%" . $request['filter']['s_nama_file'] . "%") : $response;
        
        return $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
    }
}
