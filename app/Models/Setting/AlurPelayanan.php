<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class AlurPelayanan extends Model
{
    use HasFactory, LogsActivity;

    protected $connection = 'pgsql';
    protected $table = 's_alur_pelayanan';
    protected $primaryKey = 's_id_alur';
    protected $fillable = [
        's_judul_alur',
        's_deskripsi_alur',
        's_langkah_alur',
        's_user_alur',
        's_icon_alur',
        'created_at',
        'updated_at'
    ];

    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = [ 'created_at', 'updated_at'];
    protected static $logOnlyDirty = true;

    public function datagrid($request)
    {
        $response = new AlurPelayanan();
        $response = !empty($request['sorting']) ? $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']) : $response->orderBy('s_id_alur');
        $response = ($request['filter']['s_judul_alur'] != '') ? $response->where('s_judul_alur', 'ilike', "%" . $request['filter']['s_judul_alur'] . "%") : $response;
        $response = ($request['filter']['s_langkah_alur'] != '') ? $response->where('s_langkah_alur', 'ilike', "%" . $request['filter']['s_langkah_alur'] . "%") : $response;
        $response = ($request['filter']['s_user_alur'] != '') ? $response->where('s_user_alur', 'ilike', "%" . $request['filter']['s_user_alur'] . "%") : $response;
        
        return $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
    }
}