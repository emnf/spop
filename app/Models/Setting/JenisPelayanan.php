<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisPelayanan extends Model
{
    use HasFactory;

    protected $connection = 'pgsql';
    protected $table = 's_jenis_pelayanan';
    protected $primaryKey = 's_id_jenis_pelayanan';
    protected $fillable = [
        's_nama_jenis_pelayanan',
        's_keterangan_jenis_pelayanan',
        's_id_jenis_pajak',
        's_active',
        's_order',
        's_waktu_pelayanan',
        'created_at',
        'updated_at',
        's_lunas_pbb'
    ];

    public function scopeIsActive($query)
    {
        return $query->where('s_active', true);
    }

    public function dataJenisPajak()
    {
        return $this->belongsTo(JenisPajak::class, 's_id_jenis_pajak', 's_id_jenis_pajak')->with('dataKategoriPajak');
    }

    public function persyaratan()
    {
        return $this->hasMany(Persyaratan::class, 's_id_jenis_pelayanan');
    }

    public function datagrid($request)
    {
        $response = (new JenisPelayanan())->with('dataJenisPajak');
        $response = !empty($request['sorting']) ? $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']) : $response->orderBy('s_id_jenis_pajak');
        // $response = ($request['filter']['s_nama_jenis_pelayanan'] != '') ? $response->where('s_nama_jenis_pelayanan', 'ilike', "%" . $request['filter']['s_nama_jenis_pelayanan'] . "%") : $response;
        $response = ($request['filter']['s_id_jenis_pajak'] != '') ? $response->where('s_id_jenis_pajak', $request['filter']['s_id_jenis_pajak']) : $response;
        $response = ($request['filter']['s_order'] != '') ? $response->where('s_order', $request['filter']['s_order']) : $response;
        $response = ($request['filter']['s_active'] != '') ? $response->where('s_active', $request['filter']['s_active']) : $response;
        $response = ($request['filter']['s_lunas_pbb'] != '') ? $response->where('s_lunas_pbb', $request['filter']['s_lunas_pbb']) : $response;
        $response = ($request['filter']['id_jenis_pelayanan'] != '') ? $response->where('s_id_jenis_pelayanan',$request['filter']['id_jenis_pelayanan']) : $response;
        return $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
    }
}
