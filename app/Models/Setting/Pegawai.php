<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Pegawai extends Model
{
    use HasFactory, LogsActivity;

    protected $connection = 'pgsql';
    protected $table = 's_pegawai';
    protected $primaryKey = 's_id_pegawai';
    protected $fillable = [
        's_nama_pegawai',
        's_jabatan_pegawai',
        's_pangkat_pegawai',
        's_nip_pegawai',
        'created_at',
        'updated_at',
        's_nik'

    ];

    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = ['created_at', 'updated_at'];
    protected static $logOnlyDirty = true;

    public function datagrid($request)
    {
        $response = new Pegawai();
        $response = !empty($request['sorting']) ? $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']) : $response->orderBy('s_id_pegawai');
        $response = ($request['filter']['s_nama_pegawai'] != '') ? $response->where('s_nama_pegawai', 'ilike', "%" . $request['filter']['s_nama_pegawai'] . "%") : $response;
        $response = ($request['filter']['s_jabatan_pegawai'] != '') ? $response->where('s_jabatan_pegawai', 'ilike', "%" . $request['filter']['s_jabatan_pegawai'] . "%") : $response;
        $response = ($request['filter']['s_pangkat_pegawai'] != '') ? $response->where('s_pangkat_pegawai', 'ilike', "%" . $request['filter']['s_pangkat_pegawai'] . "%") : $response;
        $response = ($request['filter']['s_nip_pegawai'] != '') ? $response->where('s_nip_pegawai', 'ilike', "%" . $request['filter']['s_nip_pegawai'] . "%") : $response;
        return $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
    }
}
