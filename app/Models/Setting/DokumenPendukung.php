<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Setting\JenisPajak;

class DokumenPendukung extends Model
{
    use HasFactory;

    protected $connection = 'pgsql';
    protected $table = 's_dokumen_pendukung';
    protected $primaryKey = 's_id_dokumen_pendukung';
    protected $fillable = [
        's_id_kategori_pajak',
        's_id_jenis_pelayanan',
        's_nama_dokumen_pendukung'
    ];

    public function dataJenisPelayanan()
    {
        return $this->hasOne(JenisPelayanan::class, 's_id_jenis_pelayanan', 's_id_jenis_pelayanan');
    }

    public function dataJenisPajak()
    {
        return $this->hasOne(JenisPajak::class, 's_id_jenis_pajak', 's_id_kategori_pajak');
    }

    public function datagrid($request)
    {
        // dd($request);
        $response = (new DokumenPendukung())->with('dataJenisPelayanan','dataJenisPajak');
        $response = !empty($request['sorting']) ? $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']) : $response->orderBy('s_id_dokumen_pendukung');
        $response = ($request['filter']['s_nama_dokumen_pendukung'] != '') ? $response->where('s_nama_dokumen_pendukung', 'ilike', "%" . $request['filter']['s_nama_dokumen_pendukung'] . "%") : $response;
        $response = ($request['filter']['s_id_jenis_pelayanan'] != '') ? $response->where('s_id_jenis_pelayanan', $request['filter']['s_id_jenis_pelayanan']) : $response;
        // $response = ($request['filter']['s_id_kategori_pajak'] != '') ? $response->where('s_id_kategori_pajak', $request['filter']['s_id_kategori_pajak']) : $response;
        return $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
    }
}
