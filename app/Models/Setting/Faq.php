<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Faq extends Model
{
    use HasFactory, LogsActivity;

    protected $connection = 'pgsql';
    protected $table = 's_faq';
    protected $primaryKey = 's_id_faq';
    protected $fillable = [
        's_pertanyaan',
        's_jawaban',
        'created_at',
        'updated_at'
    ];

    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = [ 'created_at', 'updated_at'];
    protected static $logOnlyDirty = true;

    public function datagrid($request)
    {
        $response = self::when($request['sorting'], 
            function($q) use($request){ $q->orderBy($request['sorting']['key'], $request['sorting']['order']);},
            function($q){ $q->orderBy('s_id_faq');}
        );

        $response = ($request['filter']['s_pertanyaan'] != '') ? $response->where('s_pertanyaan', 'ilike', "%" . $request['filter']['s_pertanyaan'] . "%") : $response;
        $response = ($request['filter']['s_jawaban'] != '') ? $response->where('s_jawaban', 'ilike', "%" . $request['filter']['s_jawaban'] . "%") : $response;
        
        return $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
    }
}
