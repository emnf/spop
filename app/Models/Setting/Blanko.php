<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Blanko extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 's_blanko';
    protected $fillable = ['uuid', 'blanko', 'thumbnail', 'is_active'];
    protected static $logAttributes = ['uuid', 'blanko', 'thumbnail', 'is_active'];
}
