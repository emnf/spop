<?php

namespace App\Models\Setting;

use App\Models\Setting\JenisPelayanan;
use Illuminate\Database\Eloquent\Model;
use App\Models\Setting\JenisPajak;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class JenisHasilPelayanan extends Model
{
    use HasFactory;

    protected $connection = 'pgsql';
    protected $table = 's_jenis_hasil_pelayanan';
    protected $primaryKey = 's_id_hasil_pelayanan';
    protected $fillable = [
        's_id_jenis_pajak',
        's_id_jenis_pelayanan',
        's_nama_hasil_pelayanan'
    ];

    public function dataJenisPelayanan()
    {
        return $this->hasOne(JenisPelayanan::class, 's_id_jenis_pelayanan', 's_id_jenis_pelayanan');
    }

    public function dataJenisPajak()
    {
        return $this->hasOne(JenisPajak::class, 's_id_jenis_pajak', 's_id_jenis_pajak');
    }

    public function datagrid($request)
    {
        $response = self::with('dataJenisPelayanan', 'dataJenisPajak');
        $response = !empty($request['sorting']) ? $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']) : $response->orderBy('s_id_hasil_pelayanan');
        $response = ($request['filter']['s_nama_hasil_pelayanan'] != '') ? $response->where('s_nama_hasil_pelayanan', 'ilike', "%" . $request['filter']['s_nama_hasil_pelayanan'] . "%") : $response;
        $response = ($request['filter']['s_id_jenis_pelayanan'] != '') ? $response->where('s_id_jenis_pelayanan', $request['filter']['s_id_jenis_pelayanan']) : $response;
        $response = ($request['filter']['s_id_jenis_pajak'] != '') ? $response->where('s_id_jenis_pajak', $request['filter']['s_id_jenis_pajak']) : $response;
        return $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
    }
}
