<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisKategoriPajak extends Model
{
    use HasFactory;

    protected $connection = 'pgsql';
    protected $table = 's_kategori_pajak';
    protected $primaryKey = 's_id_kategori_pajak';
    protected $fillable = [
        's_nama_kategori_pajak'
    ];

    public function dataJenisPajak()
    {
        return $this->hasMany(JenisPajak::class, 's_id_kategori_pajak', 's_id_kategori_pajak');
    }
}
