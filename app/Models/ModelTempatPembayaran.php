<?php

namespace App\Models;

use App\Models\Referensi\TempatPembayaran\BankPersepsi;
use App\Models\Referensi\TempatPembayaran\BankTunggal;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelTempatPembayaran extends Model
{
    use HasFactory;
    protected $connection = "simpbb";
    protected $table = 'tempat_pembayaran';

    protected $primaryKey = 'kd_tp';

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];

    public function kanwil()
    {
        return $this->belongsTo(KantorWilayah::class, 'kd_kanwil');
    }

    public function kantor()
    {
        return $this->belongsTo(Kantor::class, 'kd_kppbb');
    }
    public function bankTunggal()
    {
        return $this->belongsTo(BankTunggal::class, 'kd_bank_tunggal');
    }
    public function bankPersepsi()
    {
        return $this->belongsTo(BankPersepsi::class, 'kd_bank_persepsi');
    }

    public function spptMasal()
    {
        return $this->hasMany(TempatPembayaranSppt::class, 'kd_tp');
    }
}
