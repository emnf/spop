<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PBBMinimal extends Model
{
    use HasFactory;
    protected $connection = 'simpbb';
    protected $table = 'pbb_minimal';

    protected $primaryKey = 'tgl_sk_pbb_minimal';

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];
}
