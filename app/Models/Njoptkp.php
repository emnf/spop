<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Njoptkp extends Model
{
    use HasFactory;
    protected $connection = 'simpbb';

    protected $table = 'njoptkp';

    protected $primaryKey = 'nilai_njoptk';

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];
}
