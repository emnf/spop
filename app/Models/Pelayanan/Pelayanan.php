<?php

namespace App\Models\Pelayanan;

use App\Models\Hasil\HasilPelayanan;
use App\Models\Hasil\HasilVerlapDetailBangunan;
use App\Models\Hasil\HasilVerlapOp;
use App\Models\Hasil\HasilVerlapWp;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Pelayanan\WpLama;
use App\Models\Setting\JenisPajak;
use App\Models\Setting\JenisPelayanan;
use App\Models\Validasi\Validasi;
use App\Models\Verifikasi\Verifikasi;
use App\Models\Verifikasi\VerifikasiLapanagan;
use Spatie\Activitylog\Traits\LogsActivity;

class Pelayanan extends Model
{
    use HasFactory, LogsActivity;

    protected $connection = 'pgsql';
    protected $table = 't_pelayanan';
    protected $primaryKey = 't_id_pelayanan';
    protected $fillable = [
        't_no_pelayanan',
        't_tgl_pelayanan',
        't_id_jenis_pelayanan',
        't_nop',
        't_nama_pemohon',
        't_blokkav',
        't_nik_pemohon',
        't_jalan_pemohon',
        't_rt_pemohon',
        't_rw_pemohon',
        't_kelurahan_pemohon',
        't_kecamatan_pemohon',
        't_kabupaten_pemohon',
        't_kode_pos_pemohon',
        't_no_hp_pemohon',
        't_email_pemohon',
        't_keterangan',
        't_id_jenis_pajak',
        't_tahun_pajak',
        't_nourut',
        't_besar_pengurangan',
        't_multiple_tahun',
        't_tgl_permintaan',
        't_jumlah_angsuran',
        't_tahun_pajak_kompensasi',
        't_nop_kompensasi',
        't_no_sk',
        't_tgl_sk',
        'created_by',
        'uuid',
        't_his_pbb_terhutang',
        't_his_faktor_pengurang',
        't_his_pbb_yg_harus_dibayar',
    ];

    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = ['created_at', 'updated_at'];
    protected static $logOnlyDirty = true;

    public function WpLama()
    {
        return $this->hasOne(WpLama::class, 't_id_pelayanan');
    }

    public function wajibPajak()
    {
        return $this->hasOne(Wp::class, 't_id_pelayanan');
    }

    public function objekPajak()
    {
        return $this->hasOne(Op::class, 't_id_pelayanan');
    }

    public function wajibPajaks()
    {
        return $this->hasMany(Wp::class, 't_id_pelayanan')->orderBy('t_id_wp');
    }

    public function objekPajaks()
    {
        return $this->hasMany(Op::class, 't_id_pelayanan')->orderBy('t_id_op');
    }

    public function OpLama()
    {
        return $this->hasOne(OpLama::class, 't_id_pelayanan');
    }

    public function OpLamas()
    {
        return $this->hasMany(OpLama::class, 't_id_pelayanan');
    }

    public function WpLamas()
    {
        return $this->hasMany(WpLama::class, 't_id_pelayanan');
    }

    public function detailBangunan()
    {
        return $this->hasMany(DetailBangunan::class, 't_id_pelayanan');
    }

    public function persyaratan()
    {
        return $this->hasMany(PersyaratanPelayanan::class, 't_id_pelayanan');
    }

    public function pelayananAprove()
    {
        return $this->hasOne(PelayananApprove::class, 't_id_pelayanan');
    }

    public function jenisPajak()
    {
        return $this->belongsTo(JenisPajak::class, 't_id_jenis_pajak');
    }

    public function jenisPelayanan()
    {
        return $this->belongsTo(JenisPelayanan::class, 't_id_jenis_pelayanan');
    }

    public function verifikasi()
    {
        return $this->hasOne(Verifikasi::class, 't_id_pelayanan', 't_id_pelayanan');
    }

    public function validasi()
    {
        return $this->hasOne(Validasi::class, 't_id_pelayanan', 't_id_pelayanan');
    }

    public function verifikasiLapangan()
    {
        return $this->hasOne(VerifikasiLapanagan::class, 't_id_pelayanan', 't_id_pelayanan');
    }

    public function hasilPelayanan()
    {
        return $this->hasMany(HasilPelayanan::class, 't_id_pelayanan', 't_id_pelayanan');
    }

    public function hasilVerlapOp()
    {
        return $this->hasMany(HasilVerlapOp::class, 't_id_pelayanan');
    }

    public function hasilVerlapWp()
    {
        return $this->hasMany(HasilVerlapWp::class, 't_id_pelayanan');
    }

    public function hasilVerlapDetailBangunan()
    {
        return $this->hasMany(HasilVerlapDetailBangunan::class, 't_id_pelayanan');
    }

    public static function getMaxNoUrut($tahun)
    {
        $pelayana = self::whereRaw("EXTRACT(YEAR from t_tgl_pelayanan) = ?", $tahun)->whereRaw('t_no_pelayanan is not null')->select(DB::raw("MAX(t_no_pelayanan::INTEGER)"))->first();
        return $pelayana->max;
    }

    public function scopeIsApproved($query)
    {
        return $query->has('pelayananAprove');
    }

    public function scopeGetByPelayananId($query, $pleyananId)
    {
        return $query->where('t_id_pelayanan', $pleyananId)->first();
    }

    public function datagrid($request)
    {
        $response = DB::table('t_pelayanan');
        $response = $response->selectRaw('t_pelayanan.*,s_jenis_pelayanan.s_nama_jenis_pelayanan as nama_jenis,
                                            s_jenis_pajak.s_nama_singkat_pajak as nama_pajak,t_tgl_perkiraan_selesai,
                                            t_no_verifikasi,t_tgl_validasi,t_id_status_validasi,t_tgl_verifikasi_lapangan,
                                            t_id_status_verifikasi_lapangan,t_pelayanan_approve.t_id_pelayanan_approve,
                                            t_verifikasi.t_id_verifikasi, t_verifikasi.t_tgl_verifikasi,
                                            t_verifikasi.t_id_status_verifikasi,users.name');

        $response = $response->leftJoin('s_jenis_pelayanan', 's_jenis_pelayanan.s_id_jenis_pelayanan', '=', 't_pelayanan.t_id_jenis_pelayanan');
        $response = $response->leftJoin('s_jenis_pajak', 's_jenis_pajak.s_id_jenis_pajak', '=', 't_pelayanan.t_id_jenis_pajak');
        $response = $response->leftJoin('t_pelayanan_approve', 't_pelayanan_approve.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan');
        $response = $response->leftJoin('t_verifikasi', 't_verifikasi.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan');
        $response = $response->leftJoin('t_validasi', 't_validasi.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan');
        $response = $response->leftJoin('t_verifikasi_lapangan', 't_verifikasi_lapangan.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan');
        $response = $response->leftJoin('t_hasil_pelayanan', 't_hasil_pelayanan.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan');
        $response = $response->leftJoin('users', 'users.id', '=', 't_pelayanan.created_by');

        $response = $response
            ->when(!empty($request['filter']['jenis_pajak']), function ($q) use ($request) {
                return $q->where('t_pelayanan.t_id_jenis_pajak', $request['filter']['jenis_pajak']);
            })
            ->when(!empty($request['filter']['nama_jenis_pelayanan']), function ($q) use ($request) {
                return $q->where('s_jenis_pelayanan.s_nama_jenis_pelayanan', 'ilike', "%" . $request['filter']['nama_jenis_pelayanan'] . "%");
            })
            ->when(!empty($request['filter']['id_jenis_pelayanan']), function ($q) use ($request) {
                return $q->where('s_jenis_pelayanan.s_id_jenis_pelayanan', 'ilike', "%" . $request['filter']['id_jenis_pelayanan'] . "%");
            })
            ->when(!empty($request['filter']['nama_pemohon']), function ($q) use ($request) {
                return $q->where('t_pelayanan.t_nama_pemohon', 'ilike', "%" . $request['filter']['nama_pemohon'] . "%");
            })
            ->when(in_array('Wajib Pajak', auth()->user()->getRoleNames()->toArray()), function ($q) {
                $q->where('t_pelayanan.created_by', auth()->user()->id);
            })
            ->when(!empty($request['filter']['tgl_pelayanan']), function ($q) use ($request) {
                $date = explode(' - ', $request['filter']['tgl_pelayanan']);
                $startDate = date('Y-m-d', strtotime($date[0]));
                $endDate = date('Y-m-d', strtotime($date[1]));
                return $q->whereBetween('t_pelayanan.t_tgl_pelayanan', [$startDate, $endDate]);
            })
            ->when(!empty($request['filter']['status']), function ($q) use ($request) {
                if ($request['filter']['status'] == 1) {
                    $q->whereNull('t_pelayanan_approve.t_id_pelayanan_approve');
                }

                if ($request['filter']['status'] == 2) {
                    $q->whereNotNull('t_pelayanan_approve.t_id_pelayanan_approve');
                    // $q->where('t_verifikasi.t_id_status_verifikasi', '!=', 3);
                    // $q->whereNull('t_validasi');
                    // $q->whereNull('t_hasil_pelayanan.t_id_sk');
                    $q->whereRaw("t_pelayanan.t_id_pelayanan NOT IN ( SELECT t_id_pelayanan FROM t_verifikasi WHERE t_id_status_verifikasi = 3) AND t_pelayanan.t_id_pelayanan NOT IN ( SELECT t_id_pelayanan FROM t_validasi WHERE t_id_status_validasi = 4)");
                }

                if ($request['filter']['status'] == 3) {
                    $q->where('t_verifikasi.t_id_status_verifikasi', 3);
                }

                if ($request['filter']['status'] == 4) {
                    $q->where('t_validasi.t_id_status_validasi', 4);
                    $q->whereNull('t_hasil_pelayanan.t_id_sk');
                }

                if ($request['filter']['status'] == 5) {
                    $q->whereNotNull('t_hasil_pelayanan.t_id_sk');
                }
            })
            ->when(!empty($request['filter']['t_id_status_verifikasi']), function ($q) use ($request) {
                return $q->where('t_verifikasi.t_id_status_verifikasi', '=', $request['filter']['t_id_status_verifikasi']);
            })
            ->when(!empty($request['filter']['t_id_status_validasi']), function ($q) use ($request) {
                return $q->where('t_verifikasi.t_id_status_validasi', '=', $request['filter']['t_id_status_validasi']);
            })
            ->when(!empty($request['filter']['t_id_status_verifikasi_lapangan']), function ($q) use ($request) {
                return $q->where('t_verifikasi_lapangan.t_id_status_verifikasi_lapangan', '=', $request['filter']['t_id_status_verifikasi_lapangan']);
            })
            ->orderBy('t_id_pelayanan', 'desc')
            ->orderBy('t_tgl_pelayanan', 'desc');
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);

        return $response;
    }

    public function getNoPelayananAttribute()
    {
        return date('Y', strtotime($this->t_tgl_pelayanan)) . '.' . str_pad($this->t_id_jenis_pajak, 2, 0, STR_PAD_LEFT) . '.' . str_pad($this->t_no_pelayanan, 6, 0, STR_PAD_LEFT);
    }
}
