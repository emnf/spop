<?php

namespace App\Models\Pelayanan;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class OpLama extends Model
{
    use HasFactory, LogsActivity;

    protected $connection = 'pgsql';
    protected $table = 't_op_lama';
    protected $primaryKey = 't_id_op_lama';
    protected $fillable = [
        't_id_pelayanan',
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_blok',
        'no_urut',
        'kd_jns_op',
        't_jalan_op',
        't_rt_op',
        't_rw_op',
        't_kelurahan_op',
        't_kecamatan_op',
        't_luas_tanah',
        't_luas_bangunan',
        'created_by',
        't_blok_kav',
    ];

    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = ['created_at', 'updated_at'];
    protected static $logOnlyDirty = true;

    public function Pelayanan()
    {
        return $this->belongsTo(Pelayanan::class);
    }

    public function getNopAttribute()
    {
        $nop = "{$this->kd_propinsi}{$this->kd_dati2}{$this->kd_kecamatan}{$this->kd_kelurahan}{$this->kd_blok}{$this->no_urut}{$this->kd_jns_op}";
        return strlen($nop) != 18 ? null : $nop;
    }

    public function getNopFormatedAttribute()
    {
        $nop =  "{$this->kd_propinsi}.{$this->kd_dati2}.{$this->kd_kecamatan}.{$this->kd_kelurahan}.{$this->kd_blok}.{$this->no_urut}.{$this->kd_jns_op}";
        return strlen($nop) < 20 ? null : $nop;
    }
}
