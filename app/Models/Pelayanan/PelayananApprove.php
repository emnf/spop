<?php

namespace App\Models\Pelayanan;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PelayananApprove extends Model
{
    use HasFactory, LogsActivity;

    protected $connection = 'pgsql';
    protected $table = 't_pelayanan_approve';
    protected $primaryKey = 't_id_pelayanan_approve';
    protected $fillable = [
        't_id_pelayanan',
        'created_by',
        't_tgl_perkiraan_selesai'
    ];

    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = [ 'created_at', 'updated_at'];
    protected static $logOnlyDirty = true;
}
