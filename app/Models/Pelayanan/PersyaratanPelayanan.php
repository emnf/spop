<?php

namespace App\Models\Pelayanan;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PersyaratanPelayanan extends Model
{
    use HasFactory, LogsActivity;

    protected $connection = 'pgsql';
    protected $table = 't_persyaratan';
    protected $primaryKey = 't_id_persyaratan';
    protected $fillable = [
        't_nama_persyaratan',
        't_lokasi_file',
        's_id_persyaratan',
        't_id_pelayanan',
        't_nama_file',
        'created_by',
        'created_at',
        'updated_at'
    ];

    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = [ 'created_at', 'updated_at'];
    protected static $logOnlyDirty = true;
}
