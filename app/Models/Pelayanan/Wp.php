<?php

namespace App\Models\Pelayanan;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Wp extends Model
{
    use HasFactory, LogsActivity;

    protected $connection = 'pgsql';
    protected $table = 't_wp';
    protected $primaryKey = 't_id_wp';
    protected $fillable = [
        't_id_pelayanan',
        't_nop',
        't_nama_wp',
        't_nik_wp',
        't_jalan_wp',
        't_rt_wp',
        't_rw_wp',
        't_kelurahan_wp',
        't_kecamatan_wp',
        't_kabupaten_wp',
        't_no_hp_wp',
        't_email',
        't_npwpd',
        'created_by',
        't_blok_kav_wp',
        't_id_op'
    ];

    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = [ 'created_at', 'updated_at'];
    protected static $logOnlyDirty = true;

    public function getAlamatLengkapAttribute()
    {
        return $this->t_jalan_wp .' RT/RW '. $this->t_rt_wp .'/'. $this->t_rw_wp .', '. $this->t_kelurahan_wp .', '. $this->t_kecamatan_wp .', '. $this->t_kabupaten_wp;
    }
}
