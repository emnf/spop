<?php

namespace App\Models\Pelayanan;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class WpLama extends Model
{
    use HasFactory, LogsActivity;

    protected $connection = 'pgsql';
    protected $table = 't_wp_lama';
    protected $primaryKey = 't_id_wp_lama';
    protected $fillable = [
        't_id_pelayanan',
        't_nik_wp',
        't_nama_wp',
        't_jalan_wp',
        't_rt_wp',
        't_rw_wp',
        't_kelurahan_wp',
        't_kecamatan_wp',
        't_kabupaten_wp',
        't_no_hp_wp',
        'created_by',
        't_blok_kav_wp'
    ];

    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = [ 'created_at', 'updated_at'];
    protected static $logOnlyDirty = true;

    public function Pelayanan()
    {
        return $this->belongsTo(Pelayanan::class);
    }


}
