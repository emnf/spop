<?php

namespace App\Models\Pelayanan;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Op extends Model
{
    use HasFactory, LogsActivity;

    protected $connection = 'pgsql';
    protected $table = 't_op';
    protected $primaryKey = 't_id_op';
    protected $fillable = [
        't_id_pelayanan',
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_blok',
        'no_urut',
        'kd_jns_op',
        't_jalan_op',
        't_rt_op',
        't_rw_op',
        't_blokkav',
        't_kelurahan_op',
        't_kecamatan_op',
        't_luas_tanah',
        't_luas_bangunan',
        't_latitude',
        't_longitude',
        't_jenis_tanah',
        't_kode_lookup_item',
        'created_by',
        't_blok_kav',
        't_kode_znt',
        'jns_bumi'
    ];

    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = ['created_at', 'updated_at'];
    protected static $logOnlyDirty = true;

    public function getNopAttribute()
    {
        return "{$this->kd_propinsi}{$this->kd_dati2}{$this->kd_kecamatan}{$this->kd_kelurahan}{$this->kd_blok}{$this->no_urut}{$this->kd_jns_op}";
    }

    public function getNopFormatedAttribute()
    {
        return "{$this->kd_propinsi}.{$this->kd_dati2}.{$this->kd_kecamatan}.{$this->kd_kelurahan}.{$this->kd_blok}.{$this->no_urut}.{$this->kd_jns_op}";
    }

    public function detailBangunan()
    {
        return $this->hasMany(DetailBangunan::class, 't_id_op', 't_id_op');
    }
}
