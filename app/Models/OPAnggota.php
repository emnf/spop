<?php

namespace App\Models;

use App\Models\Traits\NopScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OPAnggota extends Model
{
    use HasFactory, NopScope;
    protected $connection = 'simpbb';

    protected $table = 'dat_op_anggota';

    // protected $primaryKey = 'no_bumi';

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];

    public function propinsi()
    {
        return $this->belongsTo(Propinsi::class, 'kd_propinsi');
    }

    public function dati()
    {
        return $this->belongsTo(Dati::class, 'kd_dati2');
    }

    public function kecamatan()
    {
        return $this->belongsTo(Kecamatan::class, 'kd_kecamatan');
    }

    public function kelurahan()
    {
        return $this->belongsTo(Kelurahan::class, 'kd_kelurahan');
    }

    public function datPetaBlok()
    {
        return $this->belongsTo(DatPetaBlok::class, 'kd_blok');
    }
}
