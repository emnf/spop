<?php

namespace App\Models\View;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KetetapanKecamatanView extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'REF_KECAMATAN';

    public function getRekapitulasi($request)
    {

        $tahun = $request->tahun;

        $res = KetetapanKecamatanView::select(
            'REF_KECAMATAN.KD_KECAMATAN',
            'REF_KECAMATAN.NM_KECAMATAN',
            'SPPT.THN_PAJAK_SPPT'
        )
            ->selectRaw('COUNT(SPPT.KD_PROPINSI) as COUNTSPPT')
            ->selectRaw('SUM(SPPT.PBB_YG_HARUS_DIBAYAR_SPPT) as KETETAPAN')
            ->selectRaw('COUNT(PEMBAYARAN_SPPT.KD_PROPINSI) as COUNTREAL')
            ->selectRaw('SUM(PEMBAYARAN_SPPT.JML_SPPT_YG_DIBAYAR) as REALISASI')
            ->selectRaw('SUM(PEMBAYARAN_SPPT.DENDA_SPPT) as DENDA')
            ->leftJoin('SPPT', [
                ['REF_KECAMATAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['REF_KECAMATAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['REF_KECAMATAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN']
            ])
            ->leftJoin('PEMBAYARAN_SPPT', [
                ['PEMBAYARAN_SPPT.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['PEMBAYARAN_SPPT.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['PEMBAYARAN_SPPT.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['PEMBAYARAN_SPPT.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                ['PEMBAYARAN_SPPT.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                ['PEMBAYARAN_SPPT.NO_URUT', '=', 'SPPT.NO_URUT'],
                ['PEMBAYARAN_SPPT.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP'],
                ['PEMBAYARAN_SPPT.THN_PAJAK_SPPT', '=', 'SPPT.THN_PAJAK_SPPT'],
            ])
            ->where([
                ['SPPT.THN_PAJAK_SPPT', $tahun]
            ])
            ->groupBy([
                'REF_KECAMATAN.KD_KECAMATAN',
                'REF_KECAMATAN.NM_KECAMATAN',
                'SPPT.THN_PAJAK_SPPT'
            ])
            ->orderBy('REF_KECAMATAN.KD_KECAMATAN')
            ->get();
        return $res;
    }
}
