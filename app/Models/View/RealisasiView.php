<?php

namespace App\Models\View;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RealisasiView extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'SPPT';

    public function getRekapitulasi($request)
    {

        $kecamatan = ($request->kec) ? $request->kec : $request['filter']['kecamatan'];
        $tahun = ($request->tahun) ? $request->tahun : $request['filter']['tahun'];

        $res = RealisasiView::select(
            'SPPT.KD_KECAMATAN',
            'SPPT.KD_KELURAHAN',
            'SPPT.KD_BLOK',
            'SPPT.THN_PAJAK_SPPT',
            'REF_KECAMATAN.NM_KECAMATAN',
            'REF_KELURAHAN.NM_KELURAHAN'
        )
            ->selectRaw('COUNT(SPPT.KD_BLOK) as COUNT')
            ->selectRaw('SUM(SPPT.PBB_YG_HARUS_DIBAYAR_SPPT) as JUMLAH')
            ->selectRaw('COUNT(PEMBAYARAN_SPPT.KD_BLOK) as COUNTREAL')
            ->selectRaw('SUM(PEMBAYARAN_SPPT.JML_SPPT_YG_DIBAYAR) as REALISASI')
            ->selectRaw('SUM(PEMBAYARAN_SPPT.DENDA_SPPT) as DENDA')
            ->leftJoin('PEMBAYARAN_SPPT', [
                ['PEMBAYARAN_SPPT.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['PEMBAYARAN_SPPT.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['PEMBAYARAN_SPPT.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['PEMBAYARAN_SPPT.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                ['PEMBAYARAN_SPPT.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                ['PEMBAYARAN_SPPT.NO_URUT', '=', 'SPPT.NO_URUT'],
                ['PEMBAYARAN_SPPT.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP'],
                ['PEMBAYARAN_SPPT.THN_PAJAK_SPPT', '=', 'SPPT.THN_PAJAK_SPPT'],
            ])
            ->leftJoin('REF_KECAMATAN', [
                ['REF_KECAMATAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
            ])
            ->leftJoin('REF_KELURAHAN', [
                ['REF_KELURAHAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['REF_KELURAHAN.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN']
            ])
            ->where([
                ['SPPT.KD_KECAMATAN', $kecamatan],
                ['SPPT.THN_PAJAK_SPPT', $tahun]
            ])
            ->groupBy([
                'SPPT.KD_KECAMATAN',
                'SPPT.KD_KELURAHAN',
                'SPPT.KD_BLOK',
                'SPPT.THN_PAJAK_SPPT',
                'REF_KECAMATAN.NM_KECAMATAN',
                'REF_KELURAHAN.NM_KELURAHAN'
            ]);
        return $res;
    }

    public function getRealisasiTanggal($request)
    {
        // dd($request);

        $tanggal_awal = date('Y-m-d 00:00:00', strtotime($request->tanggal_awal));
        $tanggal_akhir = date('Y-m-d 00:00:00', strtotime($request->tanggal_akhir));

        $response = RealisasiView::select(
            // 'SPPT.*',
            'SPPT.KD_PROPINSI',
            'SPPT.KD_DATI2',
            'SPPT.KD_KECAMATAN',
            'SPPT.KD_KELURAHAN',
            'SPPT.KD_BLOK',
            'SPPT.NO_URUT',
            'SPPT.KD_JNS_OP',
            'SPPT.THN_PAJAK_SPPT',
            'SPPT.NM_WP_SPPT',
            'SPPT.JLN_WP_SPPT',
            'REF_KECAMATAN.NM_KECAMATAN',
            'REF_KELURAHAN.NM_KELURAHAN',
            'PEMBAYARAN_SPPT.DENDA_SPPT',
            'PEMBAYARAN_SPPT.PEMBAYARAN_SPPT_KE',
            'PEMBAYARAN_SPPT.TGL_PEMBAYARAN_SPPT',
            'PEMBAYARAN_SPPT.JML_SPPT_YG_DIBAYAR',
            'DAT_OBJEK_PAJAK.SUBJEK_PAJAK_ID'
        )
            ->leftJoin('PEMBAYARAN_SPPT', [
                ['PEMBAYARAN_SPPT.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['PEMBAYARAN_SPPT.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['PEMBAYARAN_SPPT.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['PEMBAYARAN_SPPT.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                ['PEMBAYARAN_SPPT.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                ['PEMBAYARAN_SPPT.NO_URUT', '=', 'SPPT.NO_URUT'],
                ['PEMBAYARAN_SPPT.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP'],
                ['PEMBAYARAN_SPPT.THN_PAJAK_SPPT', '=', 'SPPT.THN_PAJAK_SPPT'],
            ])
            ->leftJoin('REF_KECAMATAN', [
                ['REF_KECAMATAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['REF_KECAMATAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['REF_KECAMATAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN']
            ])
            ->leftJoin('REF_KELURAHAN', [
                ['REF_KELURAHAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['REF_KELURAHAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['REF_KELURAHAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['REF_KELURAHAN.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN']
            ])
            ->leftJoin('DAT_OBJEK_PAJAK', [
                ['DAT_OBJEK_PAJAK.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['DAT_OBJEK_PAJAK.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['DAT_OBJEK_PAJAK.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['DAT_OBJEK_PAJAK.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                ['DAT_OBJEK_PAJAK.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                ['DAT_OBJEK_PAJAK.NO_URUT', '=', 'SPPT.NO_URUT'],
                ['DAT_OBJEK_PAJAK.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP'],
            ])
            // ->whereBetween(['date(PEMBAYARAN_SPPT.TGL_PEMBAYARAN_SPPT)',[ "TO_DATE($tanggal, YYYY/MM/DD)" , "TO_DATE($tanggal, YYYY/MM/DD)"],])
            ->where([
                ['PEMBAYARAN_SPPT.TGL_PEMBAYARAN_SPPT', ">=", "$tanggal_awal"],
                ['PEMBAYARAN_SPPT.TGL_PEMBAYARAN_SPPT', "<=", "$tanggal_akhir"],

            ])
            ->orderBy('PEMBAYARAN_SPPT.TGL_PEMBAYARAN_SPPT', 'ASC')
            ->get();
        // print_r($response);
        return $response;
    }
}
