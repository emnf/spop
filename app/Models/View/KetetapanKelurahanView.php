<?php

namespace App\Models\View;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KetetapanKelurahanView extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'REF_KELURAHAN';

    public function getRekapitulasi($request)
    {

        $kecamatan = $request->kec;
        $tahun = $request->tahun;

        $res = KetetapanKelurahanView::select(
            'REF_KELURAHAN.KD_KELURAHAN',
            'REF_KELURAHAN.NM_KELURAHAN',
            'SPPT.THN_PAJAK_SPPT'
        )
            ->selectRaw('COUNT(SPPT.KD_PROPINSI) as COUNTSPPT')
            ->selectRaw('SUM(SPPT.PBB_YG_HARUS_DIBAYAR_SPPT) as KETETAPAN')
            ->selectRaw('COUNT(PEMBAYARAN_SPPT.KD_PROPINSI) as COUNTREAL')
            ->selectRaw('SUM(PEMBAYARAN_SPPT.JML_SPPT_YG_DIBAYAR) as REALISASI')
            ->selectRaw('SUM(PEMBAYARAN_SPPT.DENDA_SPPT) as DENDA')
            ->leftJoin('SPPT', [
                ['REF_KELURAHAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['REF_KELURAHAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['REF_KELURAHAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['REF_KELURAHAN.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN']
            ])
            ->leftJoin('PEMBAYARAN_SPPT', [
                ['PEMBAYARAN_SPPT.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['PEMBAYARAN_SPPT.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['PEMBAYARAN_SPPT.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['PEMBAYARAN_SPPT.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                ['PEMBAYARAN_SPPT.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                ['PEMBAYARAN_SPPT.NO_URUT', '=', 'SPPT.NO_URUT'],
                ['PEMBAYARAN_SPPT.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP'],
                ['PEMBAYARAN_SPPT.THN_PAJAK_SPPT', '=', 'SPPT.THN_PAJAK_SPPT'],
            ])
            ->where([
                ['REF_KELURAHAN.KD_KECAMATAN', $kecamatan],
                ['SPPT.THN_PAJAK_SPPT', $tahun],
                ['SPPT.STATUS_PEMBAYARAN_SPPT', '!=', '2']
            ])
            ->groupBy([
                'REF_KELURAHAN.KD_KELURAHAN',
                'REF_KELURAHAN.NM_KELURAHAN',
                'SPPT.THN_PAJAK_SPPT'
            ])
            ->orderBy('REF_KELURAHAN.KD_KELURAHAN',)
            ->get();
        return $res;
    }

    public function getRekapitulasiBlok($request)
    {

        $kecamatan = $request->kec;
        $kelurahan = $request->kel;
        $tahun = $request->tahun;

        $res = KetetapanKelurahanView::select(
            'REF_KELURAHAN.KD_KELURAHAN',
            'REF_KELURAHAN.NM_KELURAHAN',
            'SPPT.KD_BLOK',
            'SPPT.THN_PAJAK_SPPT'
        )
            ->selectRaw('(SELECT count(*) FROM sppt a WHERE a.KD_KECAMATAN = ' . $kecamatan . ' AND a.KD_KELURAHAN = ' . $kelurahan . ' AND a.THN_PAJAK_SPPT = ' . $tahun . ' and a.KD_BLOK = SPPT.KD_BLOK) AS COUNTSPPT')
            ->selectRaw('SUM(SPPT.PBB_YG_HARUS_DIBAYAR_SPPT) as KETETAPAN')
            ->selectRaw('COUNT(PEMBAYARAN_SPPT.KD_PROPINSI) as COUNTREAL')
            ->selectRaw('SUM(PEMBAYARAN_SPPT.JML_SPPT_YG_DIBAYAR) as REALISASI')
            ->selectRaw('SUM(PEMBAYARAN_SPPT.DENDA_SPPT) as DENDA')
            ->leftJoin('SPPT', [
                ['REF_KELURAHAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['REF_KELURAHAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['REF_KELURAHAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['REF_KELURAHAN.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN']
            ])
            ->leftJoin('PEMBAYARAN_SPPT', [
                ['PEMBAYARAN_SPPT.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['PEMBAYARAN_SPPT.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['PEMBAYARAN_SPPT.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['PEMBAYARAN_SPPT.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                ['PEMBAYARAN_SPPT.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                ['PEMBAYARAN_SPPT.NO_URUT', '=', 'SPPT.NO_URUT'],
                ['PEMBAYARAN_SPPT.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP'],
                ['PEMBAYARAN_SPPT.THN_PAJAK_SPPT', '=', 'SPPT.THN_PAJAK_SPPT'],
            ])
            ->where([
                ['REF_KELURAHAN.KD_KECAMATAN', $kecamatan],
                ['REF_KELURAHAN.KD_KELURAHAN', $kelurahan],
                ['SPPT.THN_PAJAK_SPPT', $tahun]
            ])
            ->groupBy([
                'REF_KELURAHAN.KD_KELURAHAN',
                'REF_KELURAHAN.NM_KELURAHAN',
                'SPPT.KD_BLOK',
                'SPPT.THN_PAJAK_SPPT'
            ])
            ->orderBy('SPPT.KD_BLOK')
            ->get();
        return $res;
    }
}
