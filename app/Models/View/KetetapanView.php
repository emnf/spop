<?php

namespace App\Models\View;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KetetapanView extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'SPPT';

    public function getRekapitulasi($request)
    {

        $kecamatan = ($request->kec) ? $request->kec : $request['filter']['kecamatan'];
        $tahun = ($request->tahun) ? $request->tahun : $request['filter']['tahun'];

        $res = KetetapanView::select(
            'SPPT.KD_KECAMATAN',
            'SPPT.KD_KELURAHAN',
            'SPPT.KD_BLOK',
            'SPPT.THN_PAJAK_SPPT',
            'REF_KECAMATAN.NM_KECAMATAN',
            'REF_KELURAHAN.NM_KELURAHAN'
        )
            ->selectRaw('COUNT(SPPT.KD_BLOK) as COUNT')
            ->selectRaw('SUM(SPPT.PBB_YG_HARUS_DIBAYAR_SPPT) as JUMLAH')
            ->leftJoin('REF_KECAMATAN', [
                ['REF_KECAMATAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
            ])
            ->leftJoin('REF_KELURAHAN', [
                ['REF_KELURAHAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['REF_KELURAHAN.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN']
            ])
            ->where([
                ['SPPT.KD_KECAMATAN', $kecamatan],
                ['SPPT.THN_PAJAK_SPPT', $tahun]
            ])
            ->groupBy([
                'SPPT.KD_KECAMATAN',
                'SPPT.KD_KELURAHAN',
                'SPPT.KD_BLOK',
                'SPPT.THN_PAJAK_SPPT',
                'REF_KECAMATAN.NM_KECAMATAN',
                'REF_KELURAHAN.NM_KELURAHAN'
            ]);
        return $res;
    }
}
