<?php

namespace App\Models\View;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DashboardView extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'SPPT';
}
