<?php

namespace App\Models\View;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use PDO;

class TunggakanView extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'SPPT';

    public function getDataNop($nop, $tunggakan = null)
    {

        $nop = explode('.', $nop);
        $KD_PROV = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KEC = $nop[2];
        $KD_KEL = $nop[3];
        $KD_BLOK = $nop[4];
        DB::table('dual')->select(DB::raw('HITUNG_DENDA(TO_DATE(\'2023-01-01\',\'YYYY-MM-DD\')\'1000\')'))->get();
        $NO_URUT = $nop[5];
        $KD_JNS = $nop[6];

        $TAHUN = '2016';

        $response = TunggakanView::select(
            'SPPT.*',
            'PEMBAYARAN_SPPT.DENDA_SPPT',
            'PEMBAYARAN_SPPT.PEMBAYARAN_SPPT_KE',
            'PEMBAYARAN_SPPT.TGL_PEMBAYARAN_SPPT',
            'PEMBAYARAN_SPPT.JML_SPPT_YG_DIBAYAR',
            'DAT_OBJEK_PAJAK.SUBJEK_PAJAK_ID'
        )
            ->selectRaw("HITUNG_DENDA(SPPT.TGL_JATUH_TEMPO_SPPT, SPPT.PBB_YG_HARUS_DIBAYAR_SPPT) AS DENDA")
            ->leftJoin('PEMBAYARAN_SPPT', [
                ['PEMBAYARAN_SPPT.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['PEMBAYARAN_SPPT.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['PEMBAYARAN_SPPT.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['PEMBAYARAN_SPPT.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                ['PEMBAYARAN_SPPT.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                ['PEMBAYARAN_SPPT.NO_URUT', '=', 'SPPT.NO_URUT'],
                ['PEMBAYARAN_SPPT.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP'],
                ['PEMBAYARAN_SPPT.THN_PAJAK_SPPT', '=', 'SPPT.THN_PAJAK_SPPT'],
            ])
            ->leftJoin('DAT_OBJEK_PAJAK', [
                ['DAT_OBJEK_PAJAK.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['DAT_OBJEK_PAJAK.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['DAT_OBJEK_PAJAK.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['DAT_OBJEK_PAJAK.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                ['DAT_OBJEK_PAJAK.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                ['DAT_OBJEK_PAJAK.NO_URUT', '=', 'SPPT.NO_URUT'],
                ['DAT_OBJEK_PAJAK.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP'],
            ])
            ->where([
                ['SPPT.KD_PROPINSI', '=', $KD_PROV],
                ['SPPT.KD_DATI2', '=', $KD_DATI2],
                ['SPPT.KD_KECAMATAN', '=', $KD_KEC],
                ['SPPT.KD_KELURAHAN', '=', $KD_KEL],
                ['SPPT.KD_BLOK', '=', $KD_BLOK],
                ['SPPT.NO_URUT', '=', $NO_URUT],
                ['SPPT.KD_JNS_OP', '=', $KD_JNS],
                // ['SPPT.THN_PAJAK_SPPT', '>=', $TAHUN],
            ]);
        if ($tunggakan == 1) {
            $response = $response->whereIn('SPPT.STATUS_PEMBAYARAN_SPPT', ['0']);
        } else {
            $response = $response->whereIn('SPPT.STATUS_PEMBAYARAN_SPPT', ['0', '1']);
        }
        $response = $response->orderBy('SPPT.THN_PAJAK_SPPT', 'DESC')
            ->get();

        return $response;
    }

    public function getNopTahun($nop, $tahun)
    {
        $KD_PROV = substr($nop, 0, 2);
        $KD_DATI2 = substr($nop, 2, 2);
        $KD_KEC = substr($nop, 4, 3);
        $KD_KEL = substr($nop, 7, 3);
        $KD_BLOK = substr($nop, 10, 3);
        $NO_URUT = substr($nop, 13, 4);
        $KD_JNS = substr($nop, 17, 1);
        $TAHUNPAJAK = $tahun;
        $cek_data = TunggakanView::select()
            ->leftJoin('DAT_OBJEK_PAJAK', [
                ['SPPT.KD_PROPINSI', '=', 'DAT_OBJEK_PAJAK.KD_PROPINSI'],
                ['SPPT.KD_DATI2', '=', 'DAT_OBJEK_PAJAK.KD_DATI2'],
                ['SPPT.KD_KECAMATAN', '=', 'DAT_OBJEK_PAJAK.KD_KECAMATAN'],
                ['SPPT.KD_KELURAHAN', '=', 'DAT_OBJEK_PAJAK.KD_KELURAHAN'],
                ['SPPT.KD_BLOK', '=', 'DAT_OBJEK_PAJAK.KD_BLOK'],
                ['SPPT.NO_URUT', '=', 'DAT_OBJEK_PAJAK.NO_URUT'],
                ['SPPT.KD_JNS_OP', '=', 'DAT_OBJEK_PAJAK.KD_JNS_OP'],
            ])
            ->leftJoin('DAT_SUBJEK_PAJAK', [
                ['DAT_SUBJEK_PAJAK.SUBJEK_PAJAK_ID', '=', 'DAT_OBJEK_PAJAK.SUBJEK_PAJAK_ID']
            ])
            ->leftJoin('REF_DATI2', [
                ['REF_DATI2.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['REF_DATI2.KD_DATI2', '=', 'SPPT.KD_DATI2']
            ])
            ->leftJoin('REF_KECAMATAN', [
                ['REF_KECAMATAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['REF_KECAMATAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['REF_KECAMATAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN']
            ])
            ->leftJoin('REF_KELURAHAN', [
                ['REF_KELURAHAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['REF_KELURAHAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['REF_KELURAHAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['REF_KELURAHAN.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN']
            ])
            ->leftJoin('KELAS_TANAH', [
                ['KELAS_TANAH.KD_KLS_TANAH', '=', 'SPPT.KD_KLS_TANAH']
            ])
            ->leftJoin('KELAS_BANGUNAN', [
                ['KELAS_BANGUNAN.KD_KLS_BNG', '=', 'SPPT.KD_KLS_BNG']
            ])
            ->leftJoin('REF_JNS_SEKTOR', [
                ['REF_KELURAHAN.KD_SEKTOR', '=', 'REF_JNS_SEKTOR.KD_SEKTOR']
            ])
            ->leftJoin('TARIF', [
                ['SPPT.KD_PROPINSI', '=', 'REF_KECAMATAN.KD_PROPINSI'],
                ['SPPT.KD_DATI2', '=', 'REF_KECAMATAN.KD_DATI2']
            ])
            ->leftJoin('TEMPAT_PEMBAYARAN', [
                ['SPPT.KD_KANWIL_BANK', '=', 'TEMPAT_PEMBAYARAN.KD_KANWIL'],
                ['SPPT.KD_KPPBB_BANK', '=', 'TEMPAT_PEMBAYARAN.KD_KPPBB'],
                ['SPPT.KD_BANK_TUNGGAL', '=', 'TEMPAT_PEMBAYARAN.KD_BANK_TUNGGAL'],
                ['SPPT.KD_BANK_PERSEPSI', '=', 'TEMPAT_PEMBAYARAN.KD_BANK_PERSEPSI'],
                ['SPPT.KD_TP', '=', 'TEMPAT_PEMBAYARAN.KD_TP']
            ])
            ->leftJoin('REF_KANWIL', [
                ['SPPT.KD_KANWIL_BANK', '=', 'REF_KANWIL.KD_KANWIL']
            ])
            ->where([
                ['SPPT.KD_PROPINSI', '=', $KD_PROV],
                ['SPPT.KD_DATI2', '=', $KD_DATI2],
                ['SPPT.KD_KECAMATAN', '=', $KD_KEC],
                ['SPPT.KD_KELURAHAN', '=', $KD_KEL],
                ['SPPT.KD_BLOK', '=', $KD_BLOK],
                ['SPPT.NO_URUT', '=', $NO_URUT],
                ['SPPT.KD_JNS_OP', '=', $KD_JNS],
                ['SPPT.THN_PAJAK_SPPT', '=', $TAHUNPAJAK],
            ])->first();
        return $cek_data;
    }

    public function getTunggakanNop($nop, $tahun)
    {

        $KD_PROV = substr($nop, 0, 2);
        $KD_DATI2 = substr($nop, 2, 2);
        $KD_KEC = substr($nop, 4, 3);
        $KD_KEL = substr($nop, 7, 3);
        $KD_BLOK = substr($nop, 10, 3);
        $NO_URUT = substr($nop, 13, 4);
        $KD_JNS = substr($nop, 17, 1);

        $tahun_terakhir = ($tahun - 10);

        $res = TunggakanView::select(
            'SPPT.*',
            'REF_KECAMATAN.NM_KECAMATAN',
            'REF_KELURAHAN.NM_KELURAHAN',
            'KELAS_TANAH.NILAI_PER_M2_TANAH',
            'KELAS_BANGUNAN.NILAI_PER_M2_BNG',
            'PEMBAYARAN_SPPT.JML_SPPT_YG_DIBAYAR',
            'PEMBAYARAN_SPPT.TGL_REKAM_BYR_SPPT'
        )
            ->leftJoin('REF_KECAMATAN', [
                ['REF_KECAMATAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['REF_KECAMATAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['REF_KECAMATAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN']
            ])
            ->leftJoin('REF_KELURAHAN', [
                ['REF_KELURAHAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['REF_KELURAHAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['REF_KELURAHAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['REF_KELURAHAN.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN']
            ])
            ->leftJoin('KELAS_TANAH', [
                ['KELAS_TANAH.KD_KLS_TANAH', '=', 'SPPT.KD_KLS_TANAH']
            ])
            ->leftJoin('KELAS_BANGUNAN', [
                ['KELAS_BANGUNAN.KD_KLS_BNG', '=', 'SPPT.KD_KLS_BNG']
            ])
            ->leftJoin('PEMBAYARAN_SPPT', [
                ['PEMBAYARAN_SPPT.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['PEMBAYARAN_SPPT.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['PEMBAYARAN_SPPT.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['PEMBAYARAN_SPPT.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                ['PEMBAYARAN_SPPT.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                ['PEMBAYARAN_SPPT.NO_URUT', '=', 'SPPT.NO_URUT'],
                ['PEMBAYARAN_SPPT.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP'],
                ['PEMBAYARAN_SPPT.THN_PAJAK_SPPT', '=', 'SPPT.THN_PAJAK_SPPT']
            ])
            ->where([
                ['SPPT.KD_PROPINSI', '=', $KD_PROV],
                ['SPPT.KD_DATI2', '=', $KD_DATI2],
                ['SPPT.KD_KECAMATAN', '=', $KD_KEC],
                ['SPPT.KD_KELURAHAN', '=', $KD_KEL],
                ['SPPT.KD_BLOK', '=', $KD_BLOK],
                ['SPPT.NO_URUT', '=', $NO_URUT],
                ['SPPT.KD_JNS_OP', '=', $KD_JNS],
                ['SPPT.STATUS_PEMBAYARAN_SPPT', '=', '0'],
                ['SPPT.THN_PAJAK_SPPT', '>=', $tahun_terakhir],
                ['SPPT.THN_PAJAK_SPPT', '<', $tahun],
            ])
            ->orderBy('SPPT.THN_PAJAK_SPPT', 'DESC')
            ->get();
        return $res;
    }
}
