<?php

namespace App\Models\View;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KetetapanDukuhView extends Model
{
    use HasFactory;

    protected $connection = 'simpbb';
    protected $table = 'Z_DUKUH';

    public function getRekapitulasi($request)
    {

        $kecamatan = $request->kec;
        $kelurahan = $request->kel;
        $tahun = $request->tahun;

        $res = KetetapanDukuhView::select(
            'REF_KELURAHAN.KD_KELURAHAN',
            'REF_KELURAHAN.NM_KELURAHAN',
            'Z_DUKUH.KD_DUKUH',
            'Z_DUKUH.NM_DUKUH',
            'SPPT.KD_BLOK',
            'SPPT.THN_PAJAK_SPPT'
        )
            ->selectRaw('COUNT(SPPT.KD_PROPINSI) as COUNTSPPT')
            ->selectRaw('SUM(SPPT.PBB_YG_HARUS_DIBAYAR_SPPT) as KETETAPAN')
            ->selectRaw('COUNT(PEMBAYARAN_SPPT.KD_PROPINSI) as COUNTREAL')
            ->selectRaw('SUM(PEMBAYARAN_SPPT.JML_SPPT_YG_DIBAYAR) as REALISASI')
            ->selectRaw('SUM(PEMBAYARAN_SPPT.DENDA_SPPT) as DENDA')
            ->leftJoin('TTR_SPPT', [
                ['Z_DUKUH.KD_KECAMATAN', '=', 'TTR_SPPT.KD_KECAMATAN'],
                ['Z_DUKUH.KD_KELURAHAN', '=', 'TTR_SPPT.KD_KELURAHAN'],
                ['Z_DUKUH.KD_DUKUH', '=', 'TTR_SPPT.KD_DUKUH']
            ])
            ->leftJoin('SPPT', [
                ['TTR_SPPT.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['TTR_SPPT.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['TTR_SPPT.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['TTR_SPPT.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                ['TTR_SPPT.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                ['TTR_SPPT.NO_URUT', '=', 'SPPT.NO_URUT'],
                ['TTR_SPPT.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP'],
                ['TTR_SPPT.THN_PAJAK_SPPT', '=', 'SPPT.THN_PAJAK_SPPT'],
            ])
            ->leftJoin('PEMBAYARAN_SPPT', [
                ['PEMBAYARAN_SPPT.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['PEMBAYARAN_SPPT.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['PEMBAYARAN_SPPT.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['PEMBAYARAN_SPPT.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                ['PEMBAYARAN_SPPT.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                ['PEMBAYARAN_SPPT.NO_URUT', '=', 'SPPT.NO_URUT'],
                ['PEMBAYARAN_SPPT.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP'],
                ['PEMBAYARAN_SPPT.THN_PAJAK_SPPT', '=', 'SPPT.THN_PAJAK_SPPT'],
            ])
            ->leftJoin('REF_KELURAHAN', [
                ['REF_KELURAHAN.KD_KECAMATAN', '=', 'Z_DUKUH.KD_KECAMATAN'],
                ['REF_KELURAHAN.KD_KELURAHAN', '=', 'Z_DUKUH.KD_KELURAHAN']
            ])
            ->where([
                ['TTR_SPPT.KD_KECAMATAN', $kecamatan],
                ['TTR_SPPT.KD_KELURAHAN', $kelurahan],
                ['SPPT.THN_PAJAK_SPPT', $tahun]
            ])
            ->groupBy([
                'REF_KELURAHAN.KD_KELURAHAN',
                'REF_KELURAHAN.NM_KELURAHAN',
                'Z_DUKUH.KD_DUKUH',
                'Z_DUKUH.NM_DUKUH',
                'SPPT.KD_BLOK',
                'SPPT.THN_PAJAK_SPPT'
            ])
            ->orderBy('Z_DUKUH.KD_DUKUH');
        return $res;
    }
}
