<?php

namespace App\Models\Status;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusVerifikasi extends Model
{
    use HasFactory;

    protected $connection = 'pgsql';
    protected $table = 's_status_verifikasi';
    protected $priamryKey = 's_id_status_verifikasi';
}
