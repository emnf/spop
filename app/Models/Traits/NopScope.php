<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Casts\Attribute;

/**
 *
 */
trait NopScope
{
    /**
     * Cari data berdasar NOP
     */
    public function scopeWhereNop($query, $nop)
    {
        if (!is_array($nop)) {
            $nop = explode('.', $nop);
        }

        if (!isset($nop['kd_propinsi'])) {
            $nop = [
                'kd_propinsi' => $nop[0],
                'kd_dati2' => $nop[1],
                'kd_kecamatan' => $nop[2],
                'kd_kelurahan' => $nop[3],
                'kd_blok' => $nop[4],
                'no_urut' => $nop[5],
                'kd_jns_op' => $nop[6]
            ];
        }

        return $query->where([
            'kd_propinsi' => $nop['kd_propinsi'],
            'kd_dati2' => $nop['kd_dati2'],
            'kd_kecamatan' => $nop['kd_kecamatan'],
            'kd_kelurahan' => $nop['kd_kelurahan'],
            'kd_blok' => $nop['kd_blok'],
            'no_urut' => $nop['no_urut'],
            'kd_jns_op' => $nop['kd_jns_op'],
        ]);
    }

    public function scopeWhereNopIndukPecahan($query, $nop)
    {
        if (!is_array($nop)) {
            $nop = explode('.', $nop);
        }

        if (!isset($nop['kd_propinsi_induk'])) {
            $nop = [
                'kd_propinsi_induk' => $nop[0],
                'kd_dati2_induk' => $nop[1],
                'kd_kecamatan_induk' => $nop[2],
                'kd_kelurahan_induk' => $nop[3],
                'kd_blok_induk' => $nop[4],
                'no_urut_induk' => $nop[5],
                'kd_jns_op_induk' => $nop[6]
            ];
        }

        return $query->where([
            'kd_propinsi_induk' => $nop['kd_propinsi_induk'],
            'kd_dati2_induk' => $nop['kd_dati2_induk'],
            'kd_kecamatan_induk' => $nop['kd_kecamatan_induk'],
            'kd_kelurahan_induk' => $nop['kd_kelurahan_induk'],
            'kd_blok_induk' => $nop['kd_blok_induk'],
            'no_urut_induk' => $nop['no_urut_induk'],
            'kd_jns_op_induk' => $nop['kd_jns_op_induk'],
        ]);
    }

    /**
     * Cari data berdasar NOP
     */
    public function scopeWhereNopUnformated($query, string $nop)
    {
        return $query->where([
            'kd_propinsi' => substr($nop, 0, 2),
            'kd_dati2' => substr($nop, 2, 2),
            'kd_kecamatan' => substr($nop, 4, 3),
            'kd_kelurahan' => substr($nop, 7, 3),
            'kd_blok' => substr($nop, 10, 3),
            'no_urut' => substr($nop, 13, 4),
            'kd_jns_op' => substr($nop, 17, 1),
        ]);
    }

    /**
     * Belum bisa di pake yg di komen di bawah
     * Harus update model2nya dulu....
     * banyak yg dirubah
     */
    // public function getNopAttribute()
    // {
    //     return $this->kd_propinsi . $this->kd_dati2 . $this->kd_kecamatan . $this->kd_kelurahan . $this->kd_blok . $this->no_urut . $this->kd_jns_op;
    // }

    // public function getNopFormatedAttribute()
    // {
    //     return "{$this->kd_propinsi}.{$this->kd_dati2}.{$this->kd_kecamatan}.{$this->kd_kelurahan}.{$this->kd_blok}.{$this->no_urut}.{$this->kd_jns_op}";
    // }

    // public function getNopFormated2Attribute()
    // {
    //     return "{$this->kd_propinsi}.{$this->kd_dati2}.{$this->kd_kecamatan}.{$this->kd_kelurahan}.{$this->kd_blok}-{$this->no_urut}.{$this->kd_jns_op}";
    // }
}
