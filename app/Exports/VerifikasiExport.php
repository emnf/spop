<?php

namespace App\Exports;

use App\Models\Setting\PemdaModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class VerifikasiExport implements FromView, WithEvents
{
    private $data;
    private $viewName;
    private $tglCetak;
    private $pegawai;

    public function __construct($param, $viewName, $tglCetak, $pegawai)
    {
        $this->data = $param;
        $this->viewName = $viewName;
        $this->tglCetak = $tglCetak;
        $this->pegawai = $pegawai;
    }

    // public function styles(Worksheet $sheet)
    // {
    //     $sheet->getStyle('A1')->getFont()->setBold(true);
    // }

    public function registerEvents(): array
    {
        return [
            BeforeSheet::class => function (BeforeSheet $event) {
                $event->sheet->getPageSetup()
                    ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            }
        ];
    }

    public function view(): View
    {
        return view($this->viewName, [
            'pelayanan' => $this->data,
            'tglCetak' => $this->tglCetak,
            'pegawai' => $this->pegawai,
            'pemda' => PemdaModel::first(),
            'noImage' => true
        ]);
    }
}
