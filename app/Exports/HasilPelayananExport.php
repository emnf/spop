<?php

namespace App\Exports;

use App\Models\Setting\PemdaModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeSheet;

class HasilPelayananExport implements FromView, WithEvents
{
    private $data;
    private $viewName;

    public function __construct($param, $viewName)
    {
        $this->data = $param;
        $this->viewName = $viewName;
    }

    // public function styles(Worksheet $sheet)
    // {
    //     $sheet->getStyle('A1')->getFont()->setBold(true);
    // }

    public function registerEvents(): array
    {
        return [
            BeforeSheet::class => function (BeforeSheet $event) {
                $event->sheet->getPageSetup()
                    ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            }
        ];
    }

    public function view(): View
    {
        return view($this->viewName, [
            'pelayanan' => $this->data,
            'pemda' => PemdaModel::first(),
            'noImage' => true
        ]);
    }
}
