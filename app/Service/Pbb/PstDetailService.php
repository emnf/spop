<?php
namespace App\Service\Pbb;

use App\Models\Pbb\PstDetail;

class PstDetailService {
    public function create($noUrutPst, $pelayanan, $kdJnsPelayanan)
    {
        $dataPstDetail = [
            'kd_kanwil' => $noUrutPst->kd_kanwil,
            'kd_kppbb' => $noUrutPst->kd_kppbb,
            'thn_pelayanan' => $noUrutPst->thn_pelayanan,
            'bundel_pelayanan' => $noUrutPst->bundel_pelayanan,
            'no_urut_pelayanan' => $noUrutPst->no_urut_pelayanan,
            'kd_propinsi_pemohon' => substr($pelayanan->t_nop, 0, 2),
            'kd_dati2_pemohon' => substr($pelayanan->t_nop, 2, 2),
            'kd_kecamatan_pemohon' => substr($pelayanan->t_nop, 4, 3),
            'kd_kelurahan_pemohon' => substr($pelayanan->t_nop, 7, 3),
            'kd_blok_pemohon' => substr($pelayanan->t_nop, 10, 3),
            'no_urut_pemohon' => substr($pelayanan->t_nop, 13, 4),
            'kd_jns_op_pemohon' => substr($pelayanan->t_nop, 17, 1),
            'kd_jns_pelayanan' => $kdJnsPelayanan,
            'thn_pajak_permohonan' => $pelayanan->t_tahun_pajak,
            'nama_penerima' => 'WAJIB PAJAK',
            'status_selesai' => '2',
            'tgl_selesai' => date('Y-m-d H:i:s'),
            'kd_seksi_berkas' => '80',
        ];

        PstDetail::create($dataPstDetail);
    }
}
