<?php

namespace App\Service\Pbb;

use App\Models\Hasil\HasilVerlap;
use App\Models\Hasil\HasilVerlapOpBaru;
use App\Models\Pbb\DafnomOp;
use App\Models\Pbb\DatObjekPajak;
use App\Models\Pbb\DatOpBangunan;
use App\Models\Pbb\DatOpBumi;
use App\Models\Pbb\DatSubjekPajak;
use App\Models\Pbb\NopMutasi;
use App\Models\Pelayanan\Op;
use App\Models\Pelayanan\Pelayanan;
use App\Models\Pbb\HistOpBumi;
use App\Models\Pbb\NopInduk;
use App\Models\Hasil\HasilVerlapDetailBangunan;
use App\Models\Pelayanan\OpLama;
use App\Models\Pelayanan\WpLama;

class SpopLspopService
{
    /**
     * cek dulu NIP_PEREKAM admin di DBPBB.
     * kalau misal mau ditambah izin dulu dengan pemda
     */

    const NIP_PEREKAM = "060000000"; // SIMPBB_server
    // const NIP_PEREKAM = "989898989898989898"; // untuk Sismiop Kulonprogo

    protected $nomorBundelService;
    protected $subjekPajakService;
    protected $objekPajakService;
    protected $opBumiService;
    protected $opBangunanService;

    public function __construct(
        NomorBundelService $nomorBundelService,
        SubjekPajakService $subjekPajakService,
        ObjekPajakService $objekPajakService,
        OpBumiService $opBumiService,
        OpBangunanService $opBangunanService
    ) {
        $this->nomorBundelService = $nomorBundelService;
        $this->subjekPajakService = $subjekPajakService;
        $this->objekPajakService = $objekPajakService;
        $this->opBumiService = $opBumiService;
        $this->opBangunanService = $opBangunanService;
    }

    public function prosesOpBaru(Pelayanan $pelayanan)
    {
        $hasilVerlapOpBaru = HasilVerlapOpBaru::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first();

        $subjekPajak = $hasilVerlapOpBaru ?
            $this->subjekPajakService->createOrUpdate($pelayanan->hasilVerlapWp[0]) :
            $this->subjekPajakService->createOrUpdate($pelayanan->wajibPajak);

        $saveObjekPajak = $this->objekPajakService->create($pelayanan, $subjekPajak, $hasilVerlapOpBaru, self::NIP_PEREKAM);
        $op = Op::updateOrCreate(
            [
                't_id_op' => $pelayanan->objekPajak->t_id_op,
                't_id_pelayanan' => $pelayanan->t_id_pelayanan
            ],
            [
                'kd_propinsi' => $saveObjekPajak->kd_propinsi,
                'kd_dati2' => $saveObjekPajak->kd_dati2,
                'kd_kecamatan' => $saveObjekPajak->kd_kecamatan,
                'kd_kelurahan' => $saveObjekPajak->kd_kelurahan,
                'kd_blok' => $saveObjekPajak->kd_blok,
                'no_urut' => $saveObjekPajak->no_urut,
                'kd_jns_op' => $saveObjekPajak->kd_jns_op
            ]
        );

        $this->opBumiService->create($saveObjekPajak, $hasilVerlapOpBaru);
        $this->nomorBundelService->create($saveObjekPajak['no_formulir_spop']);

        if ($pelayanan->detailBangunan->count() > 0) {
            $this->opBangunanService->createOrUpdate($pelayanan, $saveObjekPajak, self::NIP_PEREKAM);
        }
    }

    public function prosesMutasiFull(Pelayanan $pelayanan)
    {
        $pelayanan->load('wajibPajak')->load('objekPajak');
        $hasilVerlap = HasilVerlap::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first();
        $wajibPajak = $hasilVerlap ? $pelayanan->hasilVerlapWp[0] : $pelayanan->wajibPajak;
        $objekPajak = $hasilVerlap ? $pelayanan->hasilVerlapOp[0] : $pelayanan->objekPajak;
        $dataBanguans = $hasilVerlap ? $pelayanan->hasilVerlapDetailBangunan : $pelayanan->objekPajak->detailBangunan;

        $dataSubjekPajak = $this->subjekPajakService->createOrUpdate($wajibPajak);

        $datObjekPajak = $this->objekPajakService->prosesMutasiPenuh(
            $dataSubjekPajak,
            $objekPajak,
            self::NIP_PEREKAM
        );

        $datOpBumi = $this->opBumiService->prosesMutasiPenuh($datObjekPajak, $objekPajak);


        if ($dataBanguans->count() > 0) {
            $this->opBangunanService->saveMutasiPenuh($pelayanan, $datObjekPajak, self::NIP_PEREKAM, $dataBanguans);
        }
    }

    public function prosesPembetulanSppt(Pelayanan $pelayanan)
    {
        $hasilVerlap = HasilVerlap::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first();
        $nop = $hasilVerlap ? $pelayanan->hasilVerlapOp[0]->nop : $pelayanan->objekPajak->nop;
        $wajibPajak = $hasilVerlap ? $pelayanan->hasilVerlapWp[0] : $pelayanan->wajibPajak;
        $objekPajak = $hasilVerlap ? $pelayanan->hasilVerlapOp[0] : $pelayanan->objekPajak;

        if ($pelayanan->detailBangunan->count() > 0) {
            $detailBangunan = $hasilVerlap ?
                $pelayanan->hasilVerlapDetailBangunan :
                $pelayanan->detailBangunan;

            foreach ($detailBangunan as $key => $bangunan) {
                $datOpBangunan = DatOpBangunan::getByNopNoBangunan(
                    $nop,
                    $bangunan->t_no_urut_bangunan
                )->update([
                    'luas_bng' => $bangunan->t_luas
                ]);
            }
        }


        DatOpBumi::findByNop($nop)->update([
            'luas_bumi' => $objekPajak->t_luas_tanah
        ]);

        $subjekPajak = $this->subjekPajakService->createOrUpdate($wajibPajak);

        DatObjekPajak::whereNop($nop)->update([
            'subjek_pajak_id' => $subjekPajak['subjek_pajak_id'] ?? $subjekPajak['subjek_pajak_id'],
            'total_luas_bumi' => $objekPajak->t_luas_tanah,
            'total_luas_bng' => $objekPajak->t_luas_bangunan ?? 0
        ]);

        $this->opBumiService->updateNjopBumi($nop);
        $this->opBangunanService->updateNjopBangunan($nop);
    }

    public function prosesMutasiPecah(Pelayanan $pelayanan)
    {
        $hasilVerlap = HasilVerlap::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first();
        $dataObjekPajakLama = DatObjekPajak::whereNop($pelayanan->OpLama->nop)->first();
        $dataObjekBumiLama = DatOpBumi::findByNop($pelayanan->OpLama->nop)->first();
        $objekPajaks = $hasilVerlap ? $pelayanan->hasilVerlapOp : $pelayanan->objekPajaks;
        $wajibPajaks = $hasilVerlap ? $pelayanan->hasilVerlapWp : $pelayanan->wajibPajaks;
        // if(!empty($hasilVerlap)){
        //     $latitude=$hasilVerlap
        //     $longitude=
        // }
        $luas = array();
        foreach ($objekPajaks as $v) {
            $luas[] = $v['t_luas_tanah'];
        }
        $luasSisa = $dataObjekPajakLama['total_luas_bumi'] - (array_sum($luas));
        $saveHistOpBumi = (new HistOpBumi())->saveData($dataObjekBumiLama, $dataObjekPajakLama->nip_pendata);

        $wajibPajak = WpLama::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first();
        // dd($dataObjekPajakLama);
        if ($wajibPajak) {
            $OpeLama = OpLama::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first();
            $dataSubjekPajak = $this->subjekPajakService->createOrUpdate($wajibPajak);

            $datObjekPajak = $this->objekPajakService->prosesMutasiPenuh(
                $dataSubjekPajak,
                $OpeLama,
                self::NIP_PEREKAM
            );
        }

        if ($saveHistOpBumi) {

            foreach ($objekPajaks as $key => $objekPajak) {
                // dd($objekPajak);
                $datSubjekPajak = $this->subjekPajakService->createOrUpdate($wajibPajaks[$key]);

                $saveObjekPajak = $this->objekPajakService->mutasiPecah(
                    $objekPajak,
                    $dataObjekPajakLama,
                    $datSubjekPajak,
                    self::NIP_PEREKAM,
                    $key
                );
                // dd($saveObjekPajak);
                //simpan op
                $opPelayanan = Op::updateOrCreate([
                    't_id_op' => $hasilVerlap ? $objekPajak->t_id_objek_sebelumnya : $objekPajak->t_id_op,
                    't_id_pelayanan' => $pelayanan->t_id_pelayanan
                ], [
                    'kd_propinsi' => $saveObjekPajak->kd_propinsi ?? $saveObjekPajak->KD_PROPINSI,
                    'kd_dati2' => $saveObjekPajak->kd_dati2 ?? $saveObjekPajak->KD_DATI2,
                    'kd_kecamatan' => $saveObjekPajak->kd_kecamatan ?? $saveObjekPajak->KD_KECAMATAN,
                    'kd_kelurahan' => $saveObjekPajak->kd_kelurahan ?? $saveObjekPajak->KD_KELURAHAN,
                    'kd_blok' => $saveObjekPajak->kd_blok ?? $saveObjekPajak->KD_BLOK,
                    'no_urut' => $saveObjekPajak->no_urut ?? $saveObjekPajak->NO_URUT,
                    'kd_jns_op' => $saveObjekPajak->kd_jns_op ?? $saveObjekPajak->KD_JNS_OP,
                    't_kode_znt' => $objekPajak->t_kode_znt

                ]);
                // dd($opPelayanan);
                //simpan op bumi
                // $saveObjekPajak = $this->objekPajakService->mutasiPecah(
                //     $objekPajak,
                //     $dataObjekPajakLama,
                //     $datSubjekPajak,
                //     self::NIP_PEREKAM,
                //     $key
                // );
                $dataOpBumiBaru = $this->opBumiService->saveMutasiPecah($opPelayanan, $saveObjekPajak, $dataObjekBumiLama, $key);
                // dd('a');
                // $saveNopInduk = (new NopInduk())->saveData($dataObjekPajakLama, null, $luasSisa, $dataOpBumiBaru); //$saveHistOpBumi
                $bangunan = HasilVerlapDetailBangunan::where('t_id_objek', $objekPajak->t_id_op)->get();
                // dd($bangunan);
                if (count($bangunan) > 0) {
                    // dd('ada');
                    $this->opBangunanService->saveMutasiPecah($opPelayanan, $saveObjekPajak, self::NIP_PEREKAM, $bangunan);
                }
                // dd('tdk ada');
                // if (count($objekPajak->detailBangunan) > 0) {
                //     $this->opBangunanService->saveMutasiPecah($opPelayanan, $saveObjekPajak, self::NIP_PEREKAM, $objekPajak->detailBangunan);
                // }


                $nopMutasiExist = NopMutasi::getByNop($dataObjekPajakLama->nop)->orderBy('indeks_mutasi', 'desc')->first();
                $indexMutasi = is_null($nopMutasiExist) ? 1 : $nopMutasiExist->indeks_mutasi + 1;

                $dataNopMutasi = [
                    'kd_propinsi' => $dataObjekPajakLama->kd_propinsi,
                    'kd_dati2' => $dataObjekPajakLama->kd_dati2,
                    'kd_kecamatan' => $dataObjekPajakLama->kd_kecamatan,
                    'kd_kelurahan' => $dataObjekPajakLama->kd_kelurahan,
                    'kd_blok' => $dataObjekPajakLama->kd_blok,
                    'no_urut' => $dataObjekPajakLama->no_urut,
                    'kd_jns_op' => $dataObjekPajakLama->kd_jns_op,
                    'indeks_mutasi' => $indexMutasi,
                    'kd_propinsi_mutasi' => $saveObjekPajak->kd_propinsi ?? $saveObjekPajak->KD_PROPINSI,
                    'kd_dati2_mutasi' => $saveObjekPajak->kd_dati2 ?? $saveObjekPajak->KD_DATI2,
                    'kd_kecamatan_mutasi' => $saveObjekPajak->kd_kecamatan ?? $saveObjekPajak->KD_KECAMATAN,
                    'kd_kelurahan_mutasi' => $saveObjekPajak->kd_kelurahan ?? $saveObjekPajak->KD_KELURAHAN,
                    'kd_blok_mutasi' => $saveObjekPajak->kd_blok ?? $saveObjekPajak->KD_BLOK,
                    'no_urut_mutasi' => $saveObjekPajak->no_urut ?? $saveObjekPajak->NO_URUT,
                    'kd_jns_op_mutasi' => $saveObjekPajak->kd_jns_op ?? $saveObjekPajak->KD_JNS_OP,
                    'luas_bumi_mutasi' => $saveObjekPajak->total_luas_bumi ?? $saveObjekPajak->TOTAL_LUAS_BUMI,
                    'tgl_rekam_nop_mutasi' => date('Y-m-d H:i:s'),
                    'nip_perekam_nop_mutasi' => self::NIP_PEREKAM,
                ];
                // dd($dataNopMutasi);
                NopMutasi::create($dataNopMutasi);
            }
            $updateInduk = DatObjekPajak::where([
                'kd_propinsi' => $dataObjekBumiLama->kd_propinsi,
                'kd_dati2' => $dataObjekBumiLama->kd_dati2,
                'kd_kecamatan' => $dataObjekBumiLama->kd_kecamatan,
                'kd_kelurahan' => $dataObjekBumiLama->kd_kelurahan,
                'kd_blok' => $dataObjekBumiLama->kd_blok,
                'no_urut' => $dataObjekBumiLama->no_urut,
                'kd_jns_op' => $dataObjekBumiLama->kd_jns_op,
            ])->update([
                'total_luas_bumi' => $luasSisa,
                // 'user_elayanan' => '989898989898989898'
            ]);
            $updateInduk = DatOpBumi::where([
                'kd_propinsi' => $dataObjekBumiLama->kd_propinsi,
                'kd_dati2' => $dataObjekBumiLama->kd_dati2,
                'kd_kecamatan' => $dataObjekBumiLama->kd_kecamatan,
                'kd_kelurahan' => $dataObjekBumiLama->kd_kelurahan,
                'kd_blok' => $dataObjekBumiLama->kd_blok,
                'no_urut' => $dataObjekBumiLama->no_urut,
                'kd_jns_op' => $dataObjekBumiLama->kd_jns_op,
            ])->update(['luas_bumi' => $luasSisa]);

            if ($updateInduk) {
                $penentuanNjopBumi = $this->opBumiService->updateNjopBumi($pelayanan->OpLama->nop);
            }
        } else {
            echo "error";
            die;
        }
    }

    public function prosesMutasiGabung(Pelayanan $pelayanan)
    {
        $opLamaArr = $pelayanan->OpLamas->toArray();
        // $pelayanan->objekPajak->t_luas_tanah = $pelayanan->OpLamas->sum('t_luas_tanah');
        $pelayanan->objekPajak->t_luas_bangunan = $pelayanan->OpLamas->sum('t_luas_bangunan');


        $datSubjekPajak = $this->subjekPajakService->createOrUpdate($pelayanan->wajibPajak);
        $saveObjekPajak = $this->objekPajakService->mutasiGabung($pelayanan->objekPajak, $datSubjekPajak, self::NIP_PEREKAM);
        $this->opBumiService->mutasiGabung($pelayanan->objekPajak, $saveObjekPajak);
        if (count($pelayanan->detailBangunan) > 0) {
            $this->opBangunanService->saveMutasiGabung($pelayanan->objekPajak, $saveObjekPajak, self::NIP_PEREKAM);
        }

        $keterangan = "ELAYANAN - GABUNG NO URUT : " . $saveObjekPajak->no_urut;

        foreach ($pelayanan->OpLamas as $key => $opLama) {
            if ($pelayanan->objekPajak->nop != $opLama->nop) {
                $datObjekPajak = DatObjekPajak::whereNop($opLama->nop)->first();
                $datOpBumi = DatOpBumi::findByNop($opLama->nop)->first();
                $datOpBangunan = DatOpBangunan::getByNopNoBangunan($opLama->nop, 1)->first();

                $dataForDafnomop = [
                    'jns_bumi' => $datOpBumi->jns_bumi,
                    'kd_jpb' => $datOpBangunan->kd_jpb ?? null,
                    'kd_status_wp' => $datObjekPajak->kd_status_wp,
                    'kategori_op' => "3",
                    'keterangan' => $keterangan,
                    'tgl_pembentukan' => date('Y-m-d H:i:s'),
                    'tgl_pemutakhiran' => date('Y-m-d H:i:s'),
                    'nip_pembentuk' => self::NIP_PEREKAM,
                    'nip_pemutakhir' => self::NIP_PEREKAM,
                    'thn_pembentukan' => date('Y'),
                ];

                $dafnomOp = DafnomOp::whereNop($datObjekPajak->nop)->where('thn_pembentukan', date('Y'))->first();

                $dataForDafnomop['kd_propinsi'] = $datObjekPajak->kd_propinsi;
                $dataForDafnomop['kd_dati2'] = $datObjekPajak->kd_dati2;
                $dataForDafnomop['kd_kecamatan'] = $datObjekPajak->kd_kecamatan;
                $dataForDafnomop['kd_kelurahan'] = $datObjekPajak->kd_kelurahan;
                $dataForDafnomop['kd_blok'] = $datObjekPajak->kd_blok;
                $dataForDafnomop['no_urut'] = $datObjekPajak->no_urut;
                $dataForDafnomop['kd_jns_op'] = $datObjekPajak->kd_jns_op;
                $dataForDafnomop['jalan_op'] = $datObjekPajak->jalan_op;
                $dataForDafnomop['blok_kav_no_op'] = $datObjekPajak->blok_kav_no_op;
                $dataForDafnomop['rw_op'] = $datObjekPajak->rw_op;
                $dataForDafnomop['rt_op'] = $datObjekPajak->rt_op;

                if (!$dafnomOp) {
                    DafnomOp::create($dataForDafnomop);
                } else {
                    DafnomOp::whereNop($datObjekPajak->nop)->where('thn_pembentukan', date('Y'))->update($dataForDafnomop);
                }
            }
        }
    }
}
