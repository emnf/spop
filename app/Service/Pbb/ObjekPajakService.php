<?php

namespace App\Service\Pbb;

use App\Models\Pbb\DatObjekPajak;
use App\Models\Pbb\DatPetaBlok;
use App\Models\Pbb\NomorBundel;

class ObjekPajakService
{
    protected $nomorBundelService;

    public function __construct(NomorBundelService $nomorBundelService)
    {
        $this->nomorBundelService = $nomorBundelService;
    }

    public function create($pelayanan, $subjekPajak, $hasilVerlapOpBaru, $nipPerekam)
    {
        // ambil no urut terakhir di blok yg sama
        $objekPajak = $hasilVerlapOpBaru ? $pelayanan->hasilVerlapOp[0] : $pelayanan->objekPajak;
        if ($hasilVerlapOpBaru) {
            $dataObjekPajak = DatObjekPajak::where([
                ['kd_kecamatan', '=', $objekPajak->kd_kecamatan],
                ['kd_kelurahan', '=', $objekPajak->kd_kelurahan],
                ['kd_blok', '=', $hasilVerlapOpBaru->t_kode_blok],
            ])->orderBy('no_urut', 'desc')->first();
        } else {
            $dataPetaBlok = DatPetaBlok::where([
                ['kd_kecamatan', '=', $objekPajak->kd_kecamatan],
                ['kd_kelurahan', '=', $objekPajak->kd_kelurahan],
                ['kd_blok', '!=', '000'],
            ])->orderBy('kd_blok', 'asc')->first();

            $dataObjekPajak = DatObjekPajak::where([
                ['kd_kecamatan', '=', $objekPajak->kd_kecamatan],
                ['kd_kelurahan', '=', $objekPajak->kd_kelurahan],
                ['kd_blok', '=', $dataPetaBlok->kd_blok],
            ])->orderBy('no_urut', 'desc')->first();
        }

        $noUrut = !empty($objekPajak->no_urut)
            ? $objekPajak->no_urut
            : $this->incrementNoUrut($dataObjekPajak->no_urut);

        $datObjekPajak = [
            'kd_propinsi' => $dataObjekPajak->kd_propinsi,
            'kd_dati2' => $dataObjekPajak->kd_dati2,
            'kd_kecamatan' => $dataObjekPajak->kd_kecamatan,
            'kd_kelurahan' => $dataObjekPajak->kd_kelurahan,
            'kd_blok' => $dataObjekPajak->kd_blok,
            'no_urut' => $noUrut,
            'kd_jns_op' => $dataObjekPajak->kd_jns_op,
            'subjek_pajak_id' => $subjekPajak->subjek_pajak_id,
            'no_formulir_spop' => $this->nomorBundelService->getMaxNomorBundel(),
            'no_persil' => '-',
            'jalan_op' => $objekPajak->t_jalan_op,
            'blok_kav_no_op' => $objekPajak->t_blok_kav,
            'rw_op' => str_pad((int) $objekPajak->t_rw_op, 2, '0', STR_PAD_LEFT),
            'rt_op' => str_pad((int) $objekPajak->t_rt_op, 3, '0', STR_PAD_LEFT),
            'kd_status_cabang' => 0,
            'kd_status_wp' => "1",
            'status_peta_op' => 0,
            'jns_transaksi_op' => "1",
            'total_luas_bumi' => $objekPajak->t_luas_tanah,
            'total_luas_bng' => $objekPajak->t_luas_bangunan ?? 0,
            'tgl_pendataan_op' => date('Y-m-d H:i:s'),
            'nip_pendata' => $nipPerekam,
            'tgl_pemeriksaan_op' => date('Y-m-d H:i:s'),
            'nip_pemeriksa_op' => $nipPerekam,
            'tgl_perekaman_op' => date('Y-m-d H:i:s'),
            'nip_perekam_op' => $nipPerekam,
            // 'user_elayanan' => '989898989898989898'
        ];

        if ($hasilVerlapOpBaru) {
            $datObjekPajak['no_persil'] = strlen($hasilVerlapOpBaru->t_no_shm) > 5 ?
                substr($hasilVerlapOpBaru->t_no_shm, 0, 5) : $hasilVerlapOpBaru->t_no_shm;
        }

        return DatObjekPajak::create($datObjekPajak);
    }

    private function incrementNoUrut($noUrut)
    {
        return str_pad($noUrut + 1, 4, 0, STR_PAD_LEFT);
    }

    public function mutasiPecah($objekPajak, $dataObjekPajakLama, $subjekPajak, $nipPerekam, $index)
    {

        $cariObjekPajak = DatObjekPajak::where([
            ['kd_kecamatan', '=', $dataObjekPajakLama->kd_kecamatan],
            ['kd_kelurahan', '=', $dataObjekPajakLama->kd_kelurahan],
            ['kd_blok', '=', $dataObjekPajakLama->kd_blok],
        ])->orderBy('NO_URUT', 'desc')->first();
        // dd($cariObjekPajak);
        $dataOpBaru = [
            'kd_propinsi' => $dataObjekPajakLama->kd_propinsi,
            'kd_dati2' => $dataObjekPajakLama->kd_dati2,
            'kd_kecamatan' => $dataObjekPajakLama->kd_kecamatan,
            'kd_kelurahan' => $dataObjekPajakLama->kd_kelurahan,
            'kd_blok' => $dataObjekPajakLama->kd_blok,
            'no_urut' => $this->incrementNoUrut($cariObjekPajak->no_urut),
            // $index == 0 ? $dataObjekPajakLama->no_urut : $this->incrementNoUrut($cariObjekPajak->no_urut),
            'kd_jns_op' => 0,
            // $index == 0 ? $dataObjekPajakLama->kd_jns_op : 0,
            'subjek_pajak_id' => $subjekPajak->subjek_pajak_id ?? $subjekPajak->SUBJEK_PAJAK_ID,
            'no_formulir_spop' => $this->nomorBundelService->getMaxNomorBundel(),
            'no_persil' => '-',
            'jalan_op' => $objekPajak->t_jalan_op,
            'blok_kav_no_op' => "-",
            'rw_op' => str_pad((int)$objekPajak->t_rw_op, 2, '0', STR_PAD_LEFT),
            'rt_op' => str_pad((int)$objekPajak->t_rt_op, 3, '0', STR_PAD_LEFT),
            'kd_status_cabang' => 0,
            'kd_status_wp' => "1",
            'status_peta_op' => 0,
            'jns_transaksi_op' => "1",
            'total_luas_bumi' => $objekPajak->t_luas_tanah,
            'total_luas_bng' => $objekPajak->t_luas_bangunan ?? 0,
            'tgl_pendataan_op' => date('Y-m-d H:i:s'),
            'tgl_pemeriksaan_op' => date('Y-m-d H:i:s'),
            'tgl_perekaman_op' => date('Y-m-d H:i:s'),
            'nip_pendata' => $nipPerekam,
            'nip_pemeriksa_op' => $nipPerekam,
            'nip_perekam_op' => $nipPerekam,
            'user_elayanan' => '989898989898989898'
        ];
        // dd($dataOpBaru);

        // if ($index == 0) {
        //     $datObjekPajak = DatObjekPajak::where([
        //         'kd_propinsi' => $dataOpBaru['kd_propinsi'],
        //         'kd_dati2' => $dataOpBaru['kd_dati2'],
        //         'kd_kecamatan' => $dataOpBaru['kd_kecamatan'],
        //         'kd_kelurahan' => $dataOpBaru['kd_kelurahan'],
        //         'kd_blok' => $dataOpBaru['kd_blok'],
        //         'no_urut' => $dataOpBaru['no_urut'],
        //         'kd_jns_op' => $dataOpBaru['kd_jns_op']
        //     ])->update($dataOpBaru);

        //     return DatObjekPajak::where([
        //         'kd_propinsi' => $dataOpBaru['kd_propinsi'],
        //         'kd_dati2' => $dataOpBaru['kd_dati2'],
        //         'kd_kecamatan' => $dataOpBaru['kd_kecamatan'],
        //         'kd_kelurahan' => $dataOpBaru['kd_kelurahan'],
        //         'kd_blok' => $dataOpBaru['kd_blok'],
        //         'no_urut' => $dataOpBaru['no_urut'],
        //         'kd_jns_op' => $dataOpBaru['kd_jns_op']
        //     ])->first();
        // }

        return DatObjekPajak::create($dataOpBaru);
    }

    public function mutasiGabung($objekPajak, $subjekPajak, $nipPerekam)
    {
        $dataObjekPajak = [
            'kd_propinsi' => $objekPajak->kd_propinsi,
            'kd_dati2' => $objekPajak->kd_dati2,
            'kd_kecamatan' => $objekPajak->kd_kecamatan,
            'kd_kelurahan' => $objekPajak->kd_kelurahan,
            'kd_blok' => $objekPajak->kd_blok,
            'no_urut' => $objekPajak->no_urut,
            'kd_jns_op' => $objekPajak->kd_jns_op,
            'subjek_pajak_id' => $subjekPajak->subjek_pajak_id,
            'jns_transaksi_op' => "2",
            'total_luas_bumi' => $objekPajak->t_luas_tanah,
            'total_luas_bng' => $objekPajak->t_luas_bangunan ?? 0,
            'tgl_pendataan_op' => date('Y-m-d H:i:s'),
            'tgl_pemeriksaan_op' => date('Y-m-d H:i:s'),
            'tgl_perekaman_op' => date('Y-m-d H:i:s'),
            'nip_pendata' => $nipPerekam,
            'nip_pemeriksa_op' => $nipPerekam,
            'nip_perekam_op' => $nipPerekam,
        ];
        // dd($dataObjekPajak);
        DatObjekPajak::whereNop($objekPajak->nop)->update($dataObjekPajak);

        return DatObjekPajak::whereNop($objekPajak->nop)->first();
    }

    public function prosesMutasiPenuh($subjekPajak, $opPelayanan, $nipPerekam)
    {
        $data = [
            'subjek_pajak_id' => $subjekPajak->subjek_pajak_id,
            'total_luas_bumi' => $opPelayanan->t_luas_tanah,
            'total_luas_bng' => $opPelayanan->t_luas_bangunan,
            'jns_transaksi_op' => "2",
            'tgl_pendataan_op' => date('Y-m-d H:i:s'),
            'nip_pendata' => $nipPerekam,
            'tgl_pemeriksaan_op' => date('Y-m-d H:i:s'),
            'nip_pemeriksa_op' => $nipPerekam,
            'tgl_perekaman_op' => date('Y-m-d H:i:s'),
            'nip_perekam_op' => $nipPerekam,
            // 'latitude' => $opPelayanan->t_latitude,
            // 'longitude' => $opPelayanan->t_longitude,
            // 'user_elayanan' => '989898989898989898'

        ];

        $objekPajak = DatObjekPajak::whereNop($opPelayanan->nop)->update($data);
        return DatObjekPajak::whereNop($opPelayanan->nop)->first();
    }
    // public function updateNopInduk($dataObjekPajakLama,$luas){
    //     $dataUpdate=[
    //         'total_luas_bumi' => $luas,
    //     ];
    //     // dd($dataUpdate);
    //         $datObjekPajak = DatObjekPajak::where([
    //             'kd_propinsi' => $dataObjekPajakLama['kd_propinsi'],
    //             'kd_dati2' => $dataObjekPajakLama['kd_dati2'],
    //             'kd_kecamatan' => $dataObjekPajakLama['kd_kecamatan'],
    //             'kd_kelurahan' => $dataObjekPajakLama['kd_kelurahan'],
    //             'kd_blok' => $dataObjekPajakLama['kd_blok'],
    //             'no_urut' => $dataObjekPajakLama['no_urut'],
    //             'kd_jns_op' => $dataObjekPajakLama['kd_jns_op']
    //         ])->update($dataUpdate);

    //         return DatObjekPajak::where([
    //             'kd_propinsi' => $dataObjekPajakLama['kd_propinsi'],
    //             'kd_dati2' => $dataObjekPajakLama['kd_dati2'],
    //             'kd_kecamatan' => $dataObjekPajakLama['kd_kecamatan'],
    //             'kd_kelurahan' => $dataObjekPajakLama['kd_kelurahan'],
    //             'kd_blok' => $dataObjekPajakLama['kd_blok'],
    //             'no_urut' => $dataObjekPajakLama['no_urut'],
    //             'kd_jns_op' => $dataObjekPajakLama['kd_jns_op']
    //         ])->first();
    // }
}
