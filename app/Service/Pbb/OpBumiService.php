<?php

namespace App\Service\Pbb;

use App\Helpers\PelayananHelper;
use App\Models\Pbb\DatOpBumi;
use Illuminate\Support\Facades\DB;
use PDO;

class OpBumiService
{
    public function create($saveObjekPajak, $hasilVerlapOpBaru)
    {
        $data = [
            'kd_propinsi' => $saveObjekPajak->kd_propinsi,
            'kd_dati2' => $saveObjekPajak->kd_dati2,
            'kd_kecamatan' => $saveObjekPajak->kd_kecamatan,
            'kd_kelurahan' => $saveObjekPajak->kd_kelurahan,
            'kd_blok' => $saveObjekPajak->kd_blok,
            'no_urut' => $saveObjekPajak->no_urut,
            'kd_jns_op' => $saveObjekPajak->kd_jns_op,
            'kd_znt' => trim($hasilVerlapOpBaru->t_kode_znt),
            'luas_bumi' => $saveObjekPajak->total_luas_bumi,
            'jns_bumi' => 1,
        ];

        $datOpBumi = DatOpBumi::create($data);
        $this->updateNjopBumi($datOpBumi->nop);
    }

    public function updateNjopBumi($nop)
    {
        $pdo = DB::connection('simpbb')->getPdo();
        $procedureName = 'PENENTUAN_NJOP_BUMI';
        $njopBumi = 1;
        $params = [
            'vlc_kd_propinsi' => substr($nop, 0, 2),
            'vlc_kd_dati2' => substr($nop, 2, 2),
            'vlc_kd_kecamatan' => substr($nop, 4, 3),
            'vlc_kd_kelurahan' => substr($nop, 7, 3),
            'vlc_kd_blok' => substr($nop, 10, 3),
            'vlc_no_urut' => substr($nop, 13, 4),
            'vlc_kd_jns_op' => substr($nop, 17, 1),
            'vlc_tahun_pajak' => date('Y'),

            'vln_flag_update' => 1,
        ];
        // dd($params);

        $stmt = $pdo->prepare("begin " . $procedureName . "(:vlc_kd_propinsi, :vlc_kd_dati2, :vlc_kd_kecamatan, :vlc_kd_kelurahan, :vlc_kd_blok, :vlc_no_urut, :vlc_kd_jns_op, :vlc_tahun_pajak, :vln_flag_update, :vln_njop_bumi); end;");

        $stmt->bindParam(':vlc_kd_propinsi', $params['vlc_kd_propinsi'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_kd_dati2', $params['vlc_kd_dati2'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_kd_kecamatan', $params['vlc_kd_kecamatan'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_kd_kelurahan', $params['vlc_kd_kelurahan'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_kd_blok', $params['vlc_kd_blok'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_no_urut', $params['vlc_no_urut'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_kd_jns_op', $params['vlc_kd_jns_op'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_tahun_pajak', $params['vlc_tahun_pajak'], PDO::PARAM_STR);
        $stmt->bindParam(':vln_flag_update', $params['vln_flag_update'], PDO::PARAM_INT);
        $stmt->bindParam(':vln_njop_bumi', $njopBumi, PDO::PARAM_INT);
        $stmt->execute();

        return;
    }

    // public function saveMutasiPecah($OpPelayanan, $datObjekPajak, $datOpBumiLama, $index,$verlap)
    public function saveMutasiPecah($OpPelayanan, $datObjekPajak, $datOpBumiLama, $index)
    // ($opPelayanan, $saveObjekPajak, $dataObjekBumiLama, $key);
    {
        // $opBumiLama = DatOpBumi::where([
        //     'kd_propinsi' => $datObjekPajak->kd_propinsi,
        //     'kd_dati2' => $datObjekPajak->kd_dati2,
        //     'kd_kecamatan' => $datObjekPajak->kd_kecamatan,
        //     'kd_kelurahan' => $datObjekPajak->kd_kelurahan,
        //     'kd_blok' => $datObjekPajak->kd_blok,
        //     'no_urut' => $datObjekPajak->no_urut,
        //     'kd_jns_op' => $datObjekPajak->kd_jns_op,
        // ])->first();

        $dataOpBumi = [
            'kd_propinsi' => $datObjekPajak->kd_propinsi ?? $datObjekPajak->KD_PROPINSI,
            'kd_dati2' => $datObjekPajak->kd_dati2 ?? $datObjekPajak->KD_DATI2,
            'kd_kecamatan' => $datObjekPajak->kd_kecamatan ?? $datObjekPajak->KD_KECAMATAN,
            'kd_kelurahan' => $datObjekPajak->kd_kelurahan ?? $datObjekPajak->KD_KELURAHAN,
            'kd_blok' => $datObjekPajak->kd_blok ?? $datObjekPajak->KD_BLOK,
            'no_urut' => $datObjekPajak->no_urut ?? $datObjekPajak->NO_URUT,
            'kd_jns_op' => $datObjekPajak->kd_jns_op ?? $datObjekPajak->KD_JNS_OP,
            'no_bumi' => 1,
            'kd_znt' => $OpPelayanan->t_kode_znt ?? $datOpBumiLama->kd_znt,
            'luas_bumi' => $OpPelayanan->t_luas_tanah,
            'jns_bumi' => (int) $OpPelayanan->t_jenis_tanah,
        ];

        $opBumiSave = DatOpBumi::create($dataOpBumi);

        // if ($opBumiLama) {
        //     DatOpBumi::where([
        //         'kd_propinsi' => $datObjekPajak->kd_propinsi,
        //         'kd_dati2' => $datObjekPajak->kd_dati2,
        //         'kd_kecamatan' => $datObjekPajak->kd_kecamatan,
        //         'kd_kelurahan' => $datObjekPajak->kd_kelurahan,
        //         'kd_blok' => $datObjekPajak->kd_blok,
        //         'no_urut' => $datObjekPajak->no_urut,
        //         'kd_jns_op' => $datObjekPajak->kd_jns_op,
        //     ])->update($dataOpBumi);

        //     $opBumiSave = DatOpBumi::where([
        //         'kd_propinsi' => $datObjekPajak->kd_propinsi,
        //         'kd_dati2' => $datObjekPajak->kd_dati2,
        //         'kd_kecamatan' => $datObjekPajak->kd_kecamatan,
        //         'kd_kelurahan' => $datObjekPajak->kd_kelurahan,
        //         'kd_blok' => $datObjekPajak->kd_blok,
        //         'no_urut' => $datObjekPajak->no_urut,
        //         'kd_jns_op' => $datObjekPajak->kd_jns_op,
        //     ])->first();
        // } else {
        //     $opBumiSave = DatOpBumi::create($dataOpBumi);
        // }

        $this->updateNjopBumi($opBumiSave->nop);
        return $opBumiSave;
    }

    public function mutasiGabung($objekPajak, $datObjekPajak)
    {
        $data = [
            'luas_bumi' => $datObjekPajak->total_luas_bumi,
            'jns_bumi' => (int) $objekPajak->t_jenis_tanah,
        ];
        // dd($data);
        DatOpBumi::findByNop($objekPajak->nop)->update($data);
        $datOpBumi = DatOpBumi::findByNop($objekPajak->nop)->first();

        $this->updateNjopBumi($datOpBumi->nop);
    }

    public function penilaianBumi($nop, $kdZnt, $luasBumi)
    {
        $pdo = DB::connection('simpbb')->getPdo();
        $procedureName = 'PENILAIAN_BUMI';
        $nilaiBumi = 0;
        $params = [
            'vlc_kd_propinsi' => substr($nop, 0, 2),
            'vlc_kd_dati2' => substr($nop, 2, 2),
            'vlc_kd_kecamatan' => substr($nop, 4, 3),
            'vlc_kd_kelurahan' => substr($nop, 7, 3),
            'vlc_kd_blok' => substr($nop, 10, 3),
            'vlc_no_urut' => substr($nop, 13, 4),
            'vlc_kd_jns_op' => substr($nop, 17, 1),
            'vln_no_bumi' => 1,
            'vlc_kd_znt' => $kdZnt,
            'vln_luas_bumi' => $luasBumi,
            'vlc_tahun' => date('Y'),
            'vln_flag_update' => 2,
        ];

        $stmt = $pdo->prepare("begin " . $procedureName . "(:vlc_kd_propinsi, :vlc_kd_dati2, :vlc_kd_kecamatan, :vlc_kd_kelurahan, :vlc_kd_blok, :vlc_no_urut, :vlc_kd_jns_op, :vln_no_bumi, :vlc_kd_znt, :vln_luas_bumi, :vlc_tahun, :vln_flag_update, :vln_nilai_bumi); end;");

        $stmt->bindParam(':vlc_kd_propinsi', $params['vlc_kd_propinsi'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_kd_dati2', $params['vlc_kd_dati2'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_kd_kecamatan', $params['vlc_kd_kecamatan'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_kd_kelurahan', $params['vlc_kd_kelurahan'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_kd_blok', $params['vlc_kd_blok'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_no_urut', $params['vlc_no_urut'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_kd_jns_op', $params['vlc_kd_jns_op'], PDO::PARAM_STR);

        $stmt->bindParam(':vln_no_bumi', $params['vln_no_bumi'], PDO::PARAM_INT);
        $stmt->bindParam(':vlc_kd_znt', $params['vlc_kd_znt'], PDO::PARAM_STR);
        $stmt->bindParam(':vln_luas_bumi', $params['vln_luas_bumi'], PDO::PARAM_INT);

        $stmt->bindParam(':vlc_tahun', $params['vlc_tahun'], PDO::PARAM_STR);
        $stmt->bindParam(':vln_flag_update', $params['vln_flag_update'], PDO::PARAM_INT);
        $stmt->bindParam(':vln_nilai_bumi', $nilaiBumi, PDO::PARAM_INT);
        $stmt->execute();

        return $nilaiBumi;
    }

    public function penentuanKelsaTanah($nilaiPermeter, $kdDati2)
    {
        $pdo = DB::connection('simpbb')->getPdo();
        $procedureName = 'PENENTUAN_KELAS';
        $njopPermeter = 0;
        $kdKelas = 'XXX';
        $thnAwalKelas = '1986';

        $param = [
            'vln_flag_jenis' => 1,
            'vlc_thn_pajak' => date('Y'),
            'vln_nilai_perm2' => $nilaiPermeter
        ];

        $stmt = $pdo->prepare("begin " . $procedureName . "(:vln_flag_jenis, :vlc_thn_pajak, :vln_nilai_perm2, :vlc_kd_kelas, :vlc_thn_awal_kls, :vln_njop_perm2); end;");

        $stmt->bindParam(':vln_flag_jenis', $param['vln_flag_jenis'], PDO::PARAM_INT);
        $stmt->bindParam(':vlc_thn_pajak', $param['vlc_thn_pajak'], PDO::PARAM_STR);
        $stmt->bindParam(':vln_nilai_perm2', $param['vln_nilai_perm2'], PDO::PARAM_INT);
        $stmt->bindParam(':vlc_kd_kelas', $kdKelas, PDO::PARAM_STR);
        $stmt->bindParam(':vlc_thn_awal_kls', $thnAwalKelas, PDO::PARAM_STR);
        $stmt->bindParam(':vln_njop_perm2', $njopPermeter, PDO::PARAM_INT);
        $stmt->execute();

        return [
            'kdKelas' => $kdKelas,
            'thnAwalKelas' => $thnAwalKelas,
            'njopPermeter' => $njopPermeter,
        ];
    }

    public function prosesMutasiPenuh($datObjekPajak, $opPelayanan)
    {
        $data = [
            'luas_bumi' => $opPelayanan->t_luas_tanah,
            'jns_bumi' => (int) $opPelayanan->t_jenis_tanah,
        ];

        DatOpBumi::findByNop($datObjekPajak->nop)->update($data);
        $this->updateNjopBumi($datObjekPajak->nop);

        return DatOpBumi::findByNop($datObjekPajak->nop)->first();
    }

    public function updateFasum($nop, $opPelayanan)
    {
        $nop = (new PelayananHelper())->formatStringToNop($nop);

        $data = [
            'jns_bumi' => 4
        ];

        DatOpBumi::findByNop($nop)->update($data);


        return DatOpBumi::findByNop($nop)->first();
    }
}
