<?php

namespace App\Service\Pbb;

use App\Models\Pbb\DatFasilitasBangunan;
use App\Models\Pbb\DatOpBangunan;
use App\Models\Pbb\LookupItem;
use App\Models\Pbb\NomorBundel;
use App\Models\Pbb\RefJpb;
use Illuminate\Support\Facades\DB;
use PDO;

class OpBangunanService
{
    const AC_SPLIT = "01";
    const AC_WINDOW = "02";
    const AC_CENTRAL_KANTOR = "03";
    const AC_CENTRAL_KAMAR_HOTEL = "04";
    const AC_CENTRAL_PERTOKOAN = "06";
    const AC_CENTRAL_KAMAR_RUMAH_SAKIT = "07";
    const AC_CENTRAL_APARTEMEN = "09";
    const AC_CENTRAL_BANGUNAN_LAIN = "11";
    const KOLAM_RENANG_DIPLESTER = "12";
    const KOLAM_RENANG_DENGAN_PELAPIS = "13";
    const PERKERASAN_KONSTRUKSI_RINGAN = "14";
    const PERKERASAN_KONSTRUKSI_SEDANG = "15";
    const PERKERASAN_KONSTRUKSI_BERAT = "16";
    const PAGAR_BAJA_BESI = "35";
    const PAGAR_BATA_BATAKO = "36";
    const GENSET = "40";
    const PABX = "41";
    const SUMUR_ARTESIS = "42";
    const LISTRIK = "44";
    protected $nomorBundelService;

    public function __construct(NomorBundelService $nomorBundelService)
    {
        $this->nomorBundelService = $nomorBundelService;
    }

    public function createOrUpdate($pelayanan, $saveObjekPajak, $nipPegawai)
    {
        $detailBangunan = $pelayanan->hasilVerlapDetailBangunan->count() > 0
            ? $pelayanan->hasilVerlapDetailBangunan
            : $pelayanan->detailBangunan;
        foreach ($detailBangunan as $key => $bangunan) {
            $kdJpb = RefJpb::whereRaw("nm_jpb=?", [strtoupper($bangunan->t_jenis_penggunaan_bangunan)])->pluck('kd_jpb')->first();

            $data = [
                'kd_propinsi' => $saveObjekPajak->kd_propinsi,
                'kd_dati2' => $saveObjekPajak->kd_dati2,
                'kd_kecamatan' => $saveObjekPajak->kd_kecamatan,
                'kd_kelurahan' => $saveObjekPajak->kd_kelurahan,
                'kd_blok' => $saveObjekPajak->kd_blok,
                'no_urut' => $saveObjekPajak->no_urut,
                'kd_jns_op' => $saveObjekPajak->kd_jns_op,
                'no_bng' => $bangunan->t_no_urut_bangunan,
                'kd_jpb' => $kdJpb,
                'no_formulir_lspop' => $this->nomorBundelService->getMaxNomorBundel(),
                'thn_dibangun_bng' => $bangunan->t_tahun_bangunan,
                'thn_renovasi_bng' => $bangunan->t_tahun_renovasi,
                'luas_bng' => $bangunan->t_luas,
                'jml_lantai_bng' => $bangunan->t_jumlah_lantai,
                'kondisi_bng' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_kondisi_bangunan)])->pluck('kd_lookup_item')->first(),
                'jns_konstruksi_bng' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_konstruksi)])->pluck('kd_lookup_item')->first(),
                'jns_atap_bng' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_atap)])->pluck('kd_lookup_item')->first(),
                'kd_dinding' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_dinding)])->pluck('kd_lookup_item')->first(),
                'kd_lantai' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_lantai)])->pluck('kd_lookup_item')->first(),
                'kd_langit_langit' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_langit_langit)])->pluck('kd_lookup_item')->first(),
                'nilai_sistem_bng' => 0,
                'jns_transaksi_bng' => 1,
                'tgl_pendataan_bng' => date('Y-m-d H:i:s'),
                'tgl_pemeriksaan_bng' => date('Y-m-d H:i:s'),
                'tgl_perekaman_bng' => date('Y-m-d H:i:s'),
                'nip_pendata_bng' => $nipPegawai,
                'nip_pemeriksa_bng' => $nipPegawai,
                'nip_perekam_bng' => $nipPegawai,
            ];
            DatOpBangunan::create($data);

            if ($bangunan->t_listrik) {
                $this->updateFasilitasBangunan($saveObjekPajak->nop, $bangunan->t_no_urut_bangunan, self::LISTRIK, $bangunan->t_listrik);
            }
            if ($bangunan->t_ac_split) {
                $this->updateFasilitasBangunan($saveObjekPajak->nop, $bangunan->t_no_urut_bangunan, self::AC_SPLIT, $bangunan->t_ac_split);
            }
            if ($bangunan->t_ac_window) {
                $this->updateFasilitasBangunan($saveObjekPajak->nop, $bangunan->t_no_urut_bangunan, self::AC_WINDOW, $bangunan->t_ac_window);
            }

            if ($bangunan->t_panjang_pagar) {
                switch ($bangunan->t_bahan_pagar) {
                    case 'BAJA/BESI':
                        $this->updateFasilitasBangunan($saveObjekPajak->nop, $bangunan->t_no_urut_bangunan, self::PAGAR_BAJA_BESI, $bangunan->t_panjang_pagar);
                        break;
                    case 'BATA/BATAKO':
                        $this->updateFasilitasBangunan($saveObjekPajak->nop, $bangunan->t_no_urut_bangunan, self::PAGAR_BATA_BATAKO, $bangunan->t_panjang_pagar);
                        break;
                }
            }

            $this->nomorBundelService->create($data['no_formulir_lspop']);
        }

        $this->updateNjopBangunan($saveObjekPajak->nop);
    }

    public function updateFasilitasBangunan($nop, $nomorBangunan, $kdFasilitas, $jumlahSatuan)
    {
        $fasilitasExist = DatFasilitasBangunan::where([
            'kd_propinsi' => substr($nop, 0, 2),
            'kd_dati2' => substr($nop, 2, 2),
            'kd_kecamatan' => substr($nop, 4, 3),
            'kd_kelurahan' => substr($nop, 7, 3),
            'kd_blok' => substr($nop, 10, 3),
            'no_urut' => substr($nop, 13, 4),
            'kd_jns_op' => substr($nop, 17, 1),
            'no_bng' => $nomorBangunan,
            'kd_fasilitas' => $kdFasilitas
        ])->first();

        if ($fasilitasExist) {
            DatFasilitasBangunan::where([
                'kd_propinsi' => substr($nop, 0, 2),
                'kd_dati2' => substr($nop, 2, 2),
                'kd_kecamatan' => substr($nop, 4, 3),
                'kd_kelurahan' => substr($nop, 7, 3),
                'kd_blok' => substr($nop, 10, 3),
                'no_urut' => substr($nop, 13, 4),
                'kd_jns_op' => substr($nop, 17, 1),
                'no_bng' => $nomorBangunan,
                'kd_fasilitas' => $kdFasilitas
            ])->update(['JML_SATUAN' => $jumlahSatuan]);
        } else {
            DatFasilitasBangunan::create([
                'kd_propinsi' => substr($nop, 0, 2),
                'kd_dati2' => substr($nop, 2, 2),
                'kd_kecamatan' => substr($nop, 4, 3),
                'kd_kelurahan' => substr($nop, 7, 3),
                'kd_blok' => substr($nop, 10, 3),
                'no_urut' => substr($nop, 13, 4),
                'kd_jns_op' => substr($nop, 17, 1),
                'no_bng' => $nomorBangunan,
                'kd_fasilitas' => $kdFasilitas,
                'jml_satuan' => $jumlahSatuan,
            ]);
        }
    }

    public function updateNjopBangunan(string $nop)
    {
        $pdo = DB::connection('simpbb')->getPdo();
        $procedureName = 'PENENTUAN_NJOP_BNG';
        $njopBangun = 1;
        $params = [
            'vlc_kd_propinsi' => substr($nop, 0, 2),
            'vlc_kd_dati2' => substr($nop, 2, 2),
            'vlc_kd_kecamatan' => substr($nop, 4, 3),
            'vlc_kd_kelurahan' => substr($nop, 7, 3),
            'vlc_kd_blok' => substr($nop, 10, 3),
            'vlc_no_urut' => substr($nop, 13, 4),
            'vlc_kd_jns_op' => substr($nop, 17, 1),
            'vlc_tahun_pajak' => 2022,
            'vln_flag_update' => 1,
        ];

        $stmt = $pdo->prepare("begin " . $procedureName . "(:vlc_kd_propinsi, :vlc_kd_dati2, :vlc_kd_kecamatan, :vlc_kd_kelurahan, :vlc_kd_blok, :vlc_no_urut, :vlc_kd_jns_op, :vlc_tahun_pajak, :vln_flag_update, :vln_njop_bng); end;");

        $stmt->bindParam(':vlc_kd_propinsi', $params['vlc_kd_propinsi'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_kd_dati2', $params['vlc_kd_dati2'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_kd_kecamatan', $params['vlc_kd_kecamatan'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_kd_kelurahan', $params['vlc_kd_kelurahan'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_kd_blok', $params['vlc_kd_blok'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_no_urut', $params['vlc_no_urut'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_kd_jns_op', $params['vlc_kd_jns_op'], PDO::PARAM_STR);
        $stmt->bindParam(':vlc_tahun_pajak', $params['vlc_tahun_pajak'], PDO::PARAM_STR);
        $stmt->bindParam(':vln_flag_update', $params['vln_flag_update'], PDO::PARAM_INT);
        $stmt->bindParam(':vln_njop_bng', $njopBangun, PDO::PARAM_INT);
        $stmt->execute();
        return;
    }

    public function saveMutasiPecah($pelayanan, $saveObjekPajak, $nipPegawai, $listBangunan)
    {
        foreach ($listBangunan as $key => $bangunan) {
            $kdJpb = RefJpb::whereRaw("nm_jpb=?", [strtoupper($bangunan->t_jenis_penggunaan_bangunan)])->pluck('kd_jpb')->first();
            $objekBangunanExist = DatOpBangunan::getByNopNoBangunan($saveObjekPajak->nop, $bangunan->t_no_urut_bangunan)->first();

            $data = [
                'kd_propinsi' => $saveObjekPajak->kd_propinsi,
                'kd_dati2' => $saveObjekPajak->kd_dati2,
                'kd_kecamatan' => $saveObjekPajak->kd_kecamatan,
                'kd_kelurahan' => $saveObjekPajak->kd_kelurahan,
                'kd_blok' => $saveObjekPajak->kd_blok,
                'no_urut' => $saveObjekPajak->no_urut,
                'kd_jns_op' => $saveObjekPajak->kd_jns_op,
                'no_bng' => $bangunan->t_no_urut_bangunan,
                'kd_jpb' => $kdJpb,
                'thn_dibangun_bng' => $bangunan->t_tahun_bangunan,
                'thn_renovasi_bng' => $bangunan->t_tahun_renovasi,
                'luas_bng' => $bangunan->t_luas,
                'jml_lantai_bng' => $bangunan->t_jumlah_lantai,
                'kondisi_bng' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_kondisi_bangunan)])->pluck('kd_lookup_item')->first(),
                'jns_konstruksi_bng' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_konstruksi)])->pluck('kd_lookup_item')->first(),
                'jns_atap_bng' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_atap)])->pluck('kd_lookup_item')->first(),
                'kd_dinding' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_dinding)])->pluck('kd_lookup_item')->first(),
                'kd_lantai' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_lantai)])->pluck('kd_lookup_item')->first(),
                'kd_langit_langit' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_langit_langit)])->pluck('kd_lookup_item')->first(),
                'nilai_sistem_bng' => 0,
                'jns_transaksi_bng' => isset($objekBangunanExist->jns_transaksi_bng) ? 2 : 1,
                'tgl_pendataan_bng' => date('Y-m-d H:i:s'),
                'tgl_pemeriksaan_bng' => date('Y-m-d H:i:s'),
                'tgl_perekaman_bng' => date('Y-m-d H:i:s'),
                'nip_pendata_bng' => $nipPegawai,
                'nip_pemeriksa_bng' => $nipPegawai,
                'nip_perekam_bng' => $nipPegawai,
            ];
            // dd($data);
            if ($objekBangunanExist) {
                DatOpBangunan::getByNopNoBangunan($saveObjekPajak->nop, $bangunan->t_no_urut_bangunan)->update($data);
                $datOpBangunan = DatOpBangunan::getByNopNoBangunan($saveObjekPajak->nop, $bangunan->t_no_urut_bangunan)->first();
            } else {
                $data['no_formulir_lspop'] = $this->nomorBundelService->getMaxNomorBundel();
                $datOpBangunan = DatOpBangunan::create($data);
            }

            if ($bangunan->t_listrik) {
                $this->updateFasilitasBangunan($saveObjekPajak->nop, $bangunan->t_no_urut_bangunan, self::LISTRIK, $bangunan->t_listrik);
            }
            if ($bangunan->t_ac_split) {
                $this->updateFasilitasBangunan($saveObjekPajak->nop, $bangunan->t_no_urut_bangunan, self::AC_SPLIT, $bangunan->t_ac_split);
            }
            if ($bangunan->t_ac_window) {
                $this->updateFasilitasBangunan($saveObjekPajak->nop, $bangunan->t_no_urut_bangunan, self::AC_WINDOW, $bangunan->t_ac_window);
            }

            if ($bangunan->t_panjang_pagar) {
                switch ($bangunan->t_bahan_pagar) {
                    case 'BAJA/BESI':
                        $this->updateFasilitasBangunan($saveObjekPajak->nop, $bangunan->t_no_urut_bangunan, self::PAGAR_BAJA_BESI, $bangunan->t_panjang_pagar);
                        break;
                    case 'BATA/BATAKO':
                        $this->updateFasilitasBangunan($saveObjekPajak->nop, $bangunan->t_no_urut_bangunan, self::PAGAR_BATA_BATAKO, $bangunan->t_panjang_pagar);
                        break;
                }
            }
            $this->nomorBundelService->create($datOpBangunan['no_formulir_lspop']);
        }

        $this->updateNjopBangunan($saveObjekPajak->nop);
    }

    public function saveMutasiGabung($objekPajak, $datObjekPajak, $nipPegawai)
    {
        foreach ($objekPajak->detailBangunan as $key => $bangunan) {
            $kdJpb = RefJpb::whereRaw("nm_jpb=?", [strtoupper($bangunan->t_jenis_penggunaan_bangunan)])->pluck('kd_jpb')->first();
            $objekBangunanExist = DatOpBangunan::getByNopNoBangunan($datObjekPajak->nop, $bangunan->t_no_urut_bangunan)->first();

            $data = [
                'kd_propinsi' => $datObjekPajak->kd_propinsi,
                'kd_dati2' => $datObjekPajak->kd_dati2,
                'kd_kecamatan' => $datObjekPajak->kd_kecamatan,
                'kd_kelurahan' => $datObjekPajak->kd_kelurahan,
                'kd_blok' => $datObjekPajak->kd_blok,
                'no_urut' => $datObjekPajak->no_urut,
                'kd_jns_op' => $datObjekPajak->kd_jns_op,
                'no_bng' => $bangunan->t_no_urut_bangunan,
                'kd_jpb' => $kdJpb,
                'thn_dibangun_bng' => $bangunan->t_tahun_bangunan,
                'thn_renovasi_bng' => $bangunan->t_tahun_renovasi,
                'luas_bng' => $bangunan->t_luas,
                'jml_lantai_bng' => $bangunan->t_jumlah_lantai,
                'kondisi_bng' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_kondisi_bangunan)])->pluck('kd_lookup_item')->first(),
                'jns_konstruksi_bng' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_konstruksi)])->pluck('kd_lookup_item')->first(),
                'jns_atap_bng' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_atap)])->pluck('kd_lookup_item')->first(),
                'kd_dinding' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_dinding)])->pluck('kd_lookup_item')->first(),
                'kd_lantai' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_lantai)])->pluck('kd_lookup_item')->first(),
                'kd_langit_langit' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_langit_langit)])->pluck('kd_lookup_item')->first(),
                'nilai_sistem_bng' => 0,
                'jns_transaksi_bng' => isset($objekBangunanExist->jns_transaksi_bng) ? 2 : 1,
                'tgl_pendataan_bng' => date('Y-m-d H:i:s'),
                'tgl_pemeriksaan_bng' => date('Y-m-d H:i:s'),
                'tgl_perekaman_bng' => date('Y-m-d H:i:s'),
                'nip_pendata_bng' => $nipPegawai,
                'nip_pemeriksa_bng' => $nipPegawai,
                'nip_perekam_bng' => $nipPegawai,
            ];

            if (!$objekBangunanExist) {
                $data['no_formulir_lspop'] = $this->nomorBundelService->getMaxNomorBundel();
            }

            if ($objekBangunanExist) {
                DatOpBangunan::getByNopNoBangunan($datObjekPajak->nop, $bangunan->t_no_urut_bangunan)->update($data);
                $saveOpBangunan = DatOpBangunan::getByNopNoBangunan($datObjekPajak->nop, $bangunan->t_no_urut_bangunan)->first();
            } else {
                $saveOpBangunan = DatOpBangunan::create($data);
            }

            if ($bangunan->t_listrik) {
                $this->updateFasilitasBangunan($datObjekPajak->nop, $bangunan->t_no_urut_bangunan, self::LISTRIK, $bangunan->t_listrik);
            }
            if ($bangunan->t_ac_split) {
                $this->updateFasilitasBangunan($datObjekPajak->nop, $bangunan->t_no_urut_bangunan, self::AC_SPLIT, $bangunan->t_ac_split);
            }
            if ($bangunan->t_ac_window) {
                $this->updateFasilitasBangunan($datObjekPajak->nop, $bangunan->t_no_urut_bangunan, self::AC_WINDOW, $bangunan->t_ac_window);
            }

            if ($bangunan->t_panjang_pagar) {
                switch ($bangunan->t_bahan_pagar) {
                    case 'BAJA/BESI':
                        $this->updateFasilitasBangunan($datObjekPajak->nop, $bangunan->t_no_urut_bangunan, self::PAGAR_BAJA_BESI, $bangunan->t_panjang_pagar);
                        break;
                    case 'BATA/BATAKO':
                        $this->updateFasilitasBangunan($datObjekPajak->nop, $bangunan->t_no_urut_bangunan, self::PAGAR_BATA_BATAKO, $bangunan->t_panjang_pagar);
                        break;
                }
            }

            $this->nomorBundelService->create($saveOpBangunan->no_formulir_lspop);
        }
        $this->updateNjopBangunan($datObjekPajak->nop);
    }

    public function saveMutasiPenuh($pelayanan, $saveObjekPajak, $nipPegawai, $dataBangunans)
    {
        foreach ($dataBangunans as $key => $bangunan) {
            $kdJpb = RefJpb::whereRaw("nm_jpb=?", [strtoupper($bangunan->t_jenis_penggunaan_bangunan)])->pluck('kd_jpb')->first();
            $objekBangunanExist = DatOpBangunan::getByNopNoBangunan($saveObjekPajak->nop, $bangunan->t_no_urut_bangunan)->first();

            $data = [
                'kd_propinsi' => $saveObjekPajak->kd_propinsi,
                'kd_dati2' => $saveObjekPajak->kd_dati2,
                'kd_kecamatan' => $saveObjekPajak->kd_kecamatan,
                'kd_kelurahan' => $saveObjekPajak->kd_kelurahan,
                'kd_blok' => $saveObjekPajak->kd_blok,
                'no_urut' => $saveObjekPajak->no_urut,
                'kd_jns_op' => $saveObjekPajak->kd_jns_op,
                'no_bng' => $bangunan->t_no_urut_bangunan,
                'kd_jpb' => $kdJpb,
                'thn_dibangun_bng' => $bangunan->t_tahun_bangunan,
                'thn_renovasi_bng' => $bangunan->t_tahun_renovasi,
                'luas_bng' => $bangunan->t_luas,
                'jml_lantai_bng' => $bangunan->t_jumlah_lantai,
                'kondisi_bng' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_kondisi_bangunan)])->pluck('kd_lookup_item')->first(),
                'jns_konstruksi_bng' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_konstruksi)])->pluck('kd_lookup_item')->first(),
                'jns_atap_bng' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_atap)])->pluck('kd_lookup_item')->first(),
                'kd_dinding' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_dinding)])->pluck('kd_lookup_item')->first(),
                'kd_lantai' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_lantai)])->pluck('kd_lookup_item')->first(),
                'kd_langit_langit' => LookupItem::whereRaw("NM_LOOKUP_ITEM=?", [strtoupper($bangunan->t_langit_langit)])->pluck('kd_lookup_item')->first(),
                'nilai_sistem_bng' => 0,
                'jns_transaksi_bng' => isset($objekBangunanExist->jns_transaksi_bng) ? 2 : 1,
                'tgl_pendataan_bng' => date('Y-m-d H:i:s'),
                'tgl_pemeriksaan_bng' => date('Y-m-d H:i:s'),
                'tgl_perekaman_bng' => date('Y-m-d H:i:s'),
                'nip_pendata_bng' => $nipPegawai,
                'nip_pemeriksa_bng' => $nipPegawai,
                'nip_perekam_bng' => $nipPegawai,
            ];

            if ($objekBangunanExist) {
                DatOpBangunan::getByNopNoBangunan($saveObjekPajak->nop, $bangunan->t_no_urut_bangunan)->update($data);
                $datOpBangunan = DatOpBangunan::getByNopNoBangunan($saveObjekPajak->nop, $bangunan->t_no_urut_bangunan)->first();
            } else {
                $data['no_formulir_lspop'] = $this->nomorBundelService->getMaxNomorBundel();
                $datOpBangunan = DatOpBangunan::create($data);
            }

            if ($bangunan->t_listrik) {
                $this->updateFasilitasBangunan($saveObjekPajak->nop, $bangunan->t_no_urut_bangunan, self::LISTRIK, $bangunan->t_listrik);
            }
            if ($bangunan->t_ac_split) {
                $this->updateFasilitasBangunan($saveObjekPajak->nop, $bangunan->t_no_urut_bangunan, self::AC_SPLIT, $bangunan->t_ac_split);
            }
            if ($bangunan->t_ac_window) {
                $this->updateFasilitasBangunan($saveObjekPajak->nop, $bangunan->t_no_urut_bangunan, self::AC_WINDOW, $bangunan->t_ac_window);
            }

            if ($bangunan->t_panjang_pagar) {
                switch ($bangunan->t_bahan_pagar) {
                    case 'BAJA/BESI':
                        $this->updateFasilitasBangunan($saveObjekPajak->nop, $bangunan->t_no_urut_bangunan, self::PAGAR_BAJA_BESI, $bangunan->t_panjang_pagar);
                        break;
                    case 'BATA/BATAKO':
                        $this->updateFasilitasBangunan($saveObjekPajak->nop, $bangunan->t_no_urut_bangunan, self::PAGAR_BATA_BATAKO, $bangunan->t_panjang_pagar);
                        break;
                }
            }
            $this->nomorBundelService->create($datOpBangunan['no_formulir_lspop']);
        }

        $this->updateNjopBangunan($saveObjekPajak->nop);
    }
}
