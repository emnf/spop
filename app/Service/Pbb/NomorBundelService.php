<?php

namespace App\Service\Pbb;

use App\Models\Pbb\NomorBundel;
use App\Models\Pbb\NomorBundelSismiop;
use App\Models\Pbb\RefKantor;
use App\Models\Pbb\RefKppbb;
use Illuminate\Support\Facades\Schema;

class NomorBundelService
{
    public function create($noFormulirSpop)
    {
        if (Schema::connection('simpbb')->hasTable('ref_kantor')) {
            $this->createNoBundel($noFormulirSpop);
        }

        if (Schema::connection('simpbb')->hasTable('ref_kppbb')) {
            $this->createNoBundelSismiop($noFormulirSpop);
        }
    }

    private function createNoBundel($noFormulirSpop)
    {
        $kdKanwilKantor = RefKantor::getKdkanwilKdKantor();
        $lastNOBundel = NomorBundel::where([
            'temp_thn_bundel' => substr($noFormulirSpop, 0, 4),
            'temp_no_bundel' => substr($noFormulirSpop, 4, 4),
        ])->orderBy('temp_no_bundel', 'desc')->first();

        if ($lastNOBundel) {
            $nomorBundel = [
                'temp_urut_bundel' => substr($noFormulirSpop, 8)
            ];
            NomorBundel::where([
                'temp_thn_bundel' => $lastNOBundel->temp_thn_bundel,
                'temp_no_bundel' => $lastNOBundel->temp_no_bundel,
            ])->update($nomorBundel);
        } else {
            $nomorBundel = [
                'kd_kanwil' => $kdKanwilKantor['kd_kanwil'],
                'kd_kantor' => $kdKanwilKantor['kd_kantor'],
                'temp_thn_bundel' => substr($noFormulirSpop, 0, 4),
                'temp_no_bundel' => substr($noFormulirSpop, 4, 4),
                'temp_urut_bundel' => substr($noFormulirSpop, 8)
            ];

            NomorBundel::create($nomorBundel);
        }
    }

    private function createNoBundelSismiop($noFormulirSpop)
    {
        $kdKanwilKantor = RefKppbb::getKdkanwilKdKantor();
        $lastNOBundel = NomorBundelSismiop::where([
            'temp_thn_bundel' => substr($noFormulirSpop, 0, 4),
            'temp_no_bundel' => substr($noFormulirSpop, 4, 4),
        ])->orderBy('temp_no_bundel', 'desc')->first();

        if ($lastNOBundel) {
            $nomorBundel = [
                'temp_urut_bundel' => substr($noFormulirSpop, 8)
            ];
            NomorBundelSismiop::where([
                'temp_thn_bundel' => $lastNOBundel->temp_thn_bundel,
                'temp_no_bundel' => $lastNOBundel->temp_no_bundel,
            ])->update($nomorBundel);
        } else {
            $nomorBundel = [
                'kd_kanwil' => $kdKanwilKantor['kd_kanwil'],
                'kd_kppbb' => $kdKanwilKantor['kd_kppbb'],
                'temp_thn_bundel' => substr($noFormulirSpop, 0, 4),
                'temp_no_bundel' => substr($noFormulirSpop, 4, 4),
                'temp_urut_bundel' => substr($noFormulirSpop, 8)
            ];

            NomorBundelSismiop::create($nomorBundel);
        }
    }

    public function getMaxNomorBundel()
    {
        if (Schema::connection('simpbb')->hasTable('ref_kantor')) {
            return (new NomorBundel())->getMaxNomorBundel();
        }

        if (Schema::connection('simpbb')->hasTable('ref_kppbb')) {
            return (new NomorBundelSismiop())->getMaxNomorBundel();
        }
    }
}
