<?php

namespace App\Service\Pbb;

use App\Models\Pbb\PstPermohonan;

class PstPermohonanService
{
    /**
     * untuk SISMIOP pake KD_KPPBB
     * untuk SIMPBB pake KD_KANTOR
     * untuk lebih pasti, cek di database PBBnya
     */

    public function create($noUrutPst, $pelayanan, $nipPenerima)
    {
        $dataPstPermohonan = [
            'kd_kanwil' => $noUrutPst->kd_kanwil,
            'kd_kppbb' => $noUrutPst->kd_kppbb,
            'thn_pelayanan' => $noUrutPst->thn_pelayanan,
            'bundel_pelayanan' => $noUrutPst->bundel_pelayanan,
            'no_urut_pelayanan' => $noUrutPst->no_urut_pelayanan,
            // 'no_srt_permohonan' => ,
            // 'tgl_surat_permohonan' => ,
            'nama_pemohon' => $pelayanan->t_nama_pemohon,
            'alamat_pemohon' => $pelayanan->t_jalan_pemohon,
            'keterangan_pst' => 'Permohonan Kepoin',
            'catatan_pst' => 'Permohonan Pengurangan Sebesar ' . $pelayanan->t_besar_pengurangan . '%',
            'status_kolektif' => '0',
            'tgl_terima_dokumen_wp' => date('Y-m-d', strtotime($pelayanan->t_tgl_pelayanan)),
            'tgl_perkiraan_selesai' => date('Y-m-d', strtotime($pelayanan->pelayananAprove->t_tgl_perkiraan_selesai)),
            'nip_penerima' => $nipPenerima,
        ];

        PstPermohonan::create($dataPstPermohonan);
    }
}
