<?php

namespace App\Service\Pbb;

use App\Models\Pbb\DatSubjekPajak;

class SubjekPajakService
{
    public function create($wajibPajak)
    {
        $subjekPajak = DatSubjekPajak::whereSubjekPajakId($wajibPajak->t_nik_wp)->first();
        if (!$subjekPajak) {
            $dataSubjekPajak  = [
                'subjek_pajak_id' => $wajibPajak->t_nik_wp,
                'nm_wp' => substr($wajibPajak->t_nama_wp, 0, 30),
                'jalan_wp' => $wajibPajak->t_jalan_wp,
                'rw_wp' => str_pad((int) $wajibPajak->t_rw_wp, 2, '0', STR_PAD_LEFT),
                'rt_wp' => str_pad((int) $wajibPajak->t_rt_wp, 3, '0', STR_PAD_LEFT),
                'kelurahan_wp' => $wajibPajak->t_kelurahan_wp,
                'kota_wp' => $wajibPajak->t_kabupaten_wp,
                'telp_wp' => $wajibPajak->t_no_hp_wp,
                'npwp' => "-",
                'status_pekerjaan_wp' => "5",
                'npwpd' => "-",
                'email' => $wajibPajak->t_email,
                'blok_kav_no_wp' => $wajibPajak->t_blok_kav_wp ?? null
            ];

            $subjekPajak = DatSubjekPajak::create($dataSubjekPajak);
        }

        return $subjekPajak;
    }

    public function createOrUpdate($wajibPajak)
    {
        $subjekPajak = $this->findFirstByNik($wajibPajak->t_nik_wp);

        $dataSubjekPajak = [
            'nm_wp' => substr($wajibPajak->t_nama_wp, 0, 30),
            'jalan_wp' => $wajibPajak->t_jalan_wp,
            'blok_kav_no_wp' => $wajibPajak->t_blok_kav_wp ?? null,
            'rw_wp' => str_pad((int) $wajibPajak->t_rw_wp, 2, '0', STR_PAD_LEFT),
            'rt_wp' => str_pad((int) $wajibPajak->t_rt_wp, 3, '0', STR_PAD_LEFT),
            'kelurahan_wp' => $wajibPajak->t_kelurahan_wp,
            'kota_wp' => $wajibPajak->t_kabupaten_wp,
            'npwp' => $wajibPajak->t_npwpd_wp,
            'telp_wp' => isset($wajibPajak->t_no_hp_wp) ? substr($wajibPajak->t_no_hp_wp, 0, 15) :  '',
            // 'email' => $wajibPajak->t_email,
            // 'npwpd' => $wajibPajak->t_npwpd ?? null
        ];

        if ($subjekPajak) {
            try {
                DatSubjekPajak::whereSubjekPajakId($wajibPajak->t_nik_wp)
                    ->update($dataSubjekPajak);
            } catch (\Throwable $th) {
                //throw $th;
                dd($th);
            }
            return $this->findFirstByNik($wajibPajak->t_nik_wp);
        } else {
            $dataSubjekPajak['subjek_pajak_id'] = $wajibPajak->t_nik_wp;
            $dataSubjekPajak['status_pekerjaan_wp'] = "5";
            // $dataSubjekPajak['npwpd'] = "-";

            return DatSubjekPajak::create($dataSubjekPajak);
        }
    }

    public function findFirstByNik($nik)
    {
        return DatSubjekPajak::whereSubjekPajakId($nik)->first();
    }
}
