<?php

namespace App\Service\Pbb;

use App\Models\Pbb\MaxUrutPst;
use App\Models\Pbb\PenguranganPstSimpbb;
use App\Models\Pbb\PenguranganPstSismiop;
use App\Models\Pbb\PstPermohonanPengurangan;
use App\Models\Pbb\RefKantor;
use App\Models\Pbb\RefKppbb;
use App\Models\Pbb\SkPbb;
use App\Models\Pbb\SkSismiop;
use App\Models\Pbb\Sppt;
use App\Models\Pelayanan\Pelayanan;
use Illuminate\Support\Facades\Schema;

class SkPenguranganService
{

    const NIP_PEREKAM = "060000000";
    const KD_SK = 'K';

    protected $pstDetailService;
    protected $pstPermohonanService;

    public function __construct(PstPermohonanService $pstPermohonanService, PstDetailService $pstDetailService)
    {
        $this->pstDetailService = $pstDetailService;
        $this->pstPermohonanService = $pstPermohonanService;
    }

    public function simpanSkPenguranganPst($noSk, $tglSk, Pelayanan $pelayanan)
    {
        if (Schema::connection('simpbb')->hasTable('ref_kantor')) {
            $kppbbKantor = RefKantor::getKdkanwilKdKantor();
            $this->simpanSkSimpbb($kppbbKantor, $noSk, $tglSk);
        }

        if (Schema::connection('simpbb')->hasTable('ref_kppbb')) {
            $kppbbKantor = RefKppbb::getKdkanwilKdKantor();
            $this->simpanSkSismiop($kppbbKantor, $noSk, $tglSk);
            $this->simpanPenguranganPstSismiop($pelayanan, $noSk);
        }

        $this->updateSppt($pelayanan);
    }

    public function simpanSkSismiop($kanwil, $noSk, $tglSk)
    {
        $dataSk = [
            'kd_kanwil' => $kanwil['kd_kanwil'],
            'kd_kppbb' => $kanwil['kd_kppbb'],
            'jns_sk' => self::KD_SK,
            'no_sk' => $noSk,
            'tgl_sk' => date('Y-m-d', strtotime($tglSk)),
            'tgl_cetak_sk' => date('Y-m-d H:i:s'),
            'nip_pencetak_sk' => self::NIP_PEREKAM
        ];

        SkSismiop::create($dataSk);
    }

    public function simpanSkSimpbb($kanwil, $noSk, $tglSk)
    {
        $dataSk = [
            'kd_kanwil' => $kanwil['kd_kanwil'],
            'kd_kantor' => $kanwil['kd_kantor'],
            'jns_sk' => self::KD_SK,
            'no_sk' => $noSk,
            'tgl_sk' => date('Y-m-d', strtotime($tglSk)),
            'tgl_cetak_sk' => date('Y-m-d H:i:s'),
            'nip_pencetak_sk' => self::NIP_PEREKAM
        ];

        SkPbb::create($dataSk);
    }

    public function simpanPenguranganPstSismiop($pelayanan, $noSk)
    {
        $maxUrutPst = MaxUrutPst::getMaxNoUrutPst();
        MaxUrutPst::where([
            'kd_kanwil' => $maxUrutPst->kd_kanwil,
            'kd_kppbb' => $maxUrutPst->kd_kppbb,
        ])->update([
            'thn_pelayanan' => $maxUrutPst->thn_pelayanan,
            'bundel_pelayanan' => $maxUrutPst->bundel_pelayanan,
            'no_urut_pelayanan' => $maxUrutPst->no_urut_pelayanan
        ]);

        $this->pstPermohonanService->create($maxUrutPst, $pelayanan, self::NIP_PEREKAM);
        $this->pstDetailService->create($maxUrutPst, $pelayanan, '08');

        PstPermohonanPengurangan::create([
            'kd_kanwil' => $maxUrutPst->kd_kanwil,
            'kd_kppbb' => $maxUrutPst->kd_kppbb,
            'thn_pelayanan' => $maxUrutPst->thn_pelayanan,
            'bundel_pelayanan' => $maxUrutPst->bundel_pelayanan,
            'no_urut_pelayanan' => $maxUrutPst->no_urut_pelayanan,
            'kd_propinsi_pemohon' => substr($pelayanan->t_nop, 0, 2),
            'kd_dati2_pemohon' => substr($pelayanan->t_nop, 2, 2),
            'kd_kecamatan_pemohon' => substr($pelayanan->t_nop, 4, 3),
            'kd_kelurahan_pemohon' => substr($pelayanan->t_nop, 7, 3),
            'kd_blok_pemohon' => substr($pelayanan->t_nop, 10, 3),
            'no_urut_pemohon' => substr($pelayanan->t_nop, 13, 4),
            'kd_jns_op_pemohon' => substr($pelayanan->t_nop, 17, 1),
            'jns_pengurangan' => '2',
            'pct_permohonan_pengurangan' => $pelayanan->t_besar_pengurangan,
        ]);

        $dataPengurangan = [
            'kd_kanwil' => $maxUrutPst->kd_kanwil,
            'kd_kppbb' => $maxUrutPst->kd_kppbb,
            'thn_pelayanan' => $maxUrutPst->thn_pelayanan,
            'bundel_pelayanan' => $maxUrutPst->bundel_pelayanan,
            'no_urut_pelayanan' => $maxUrutPst->no_urut_pelayanan,
            'kd_propinsi_pemohon' => substr($pelayanan->t_nop, 0, 2),
            'kd_dati2_pemohon' => substr($pelayanan->t_nop, 2, 2),
            'kd_kecamatan_pemohon' => substr($pelayanan->t_nop, 4, 3),
            'kd_kelurahan_pemohon' => substr($pelayanan->t_nop, 7, 3),
            'kd_blok_pemohon' => substr($pelayanan->t_nop, 10, 3),
            'no_urut_pemohon' => substr($pelayanan->t_nop, 13, 4),
            'kd_jns_op_pemohon' => substr($pelayanan->t_nop, 17, 1),
            'thn_peng_pst' => $pelayanan->t_tahun_pajak,
            'jns_sk' => self::KD_SK,
            'no_sk' => $noSk,
            'status_sk_peng_pst' => '0',
            'pct_pengurangan_pst' => $pelayanan->validasi->t_besar_pengurangan,
        ];

        PenguranganPstSismiop::create($dataPengurangan);
    }

    public function simpanPenguranganPstSimpbb($pelayanan, $noSk)
    {
        $maxUrutPst = MaxUrutPst::getMaxNoUrutPst();
        // MaxUrutPst::where([
        //     'kd_kanwil' => $maxUrutPst->kd_kanwil,
        //     'kd_kantor' => $maxUrutPst->kd_kantor,
        // ])->update([
        //     'thn_pelayanan' => $maxUrutPst->thn_pelayanan,
        //     'bundel_pelayanan' => $maxUrutPst->bundel_pelayanan,
        //     'no_urut_pelayanan' => $maxUrutPst->no_urut_pelayanan
        // ]);

        $dataPengurangan = [
            'kd_kanwil' => $maxUrutPst->kd_kanwil,
            'kd_kantor' => $maxUrutPst->kd_kantor,
            'thn_pelayanan' => $maxUrutPst->thn_pelayanan,
            'bundel_pelayanan' => $maxUrutPst->bundel_pelayanan,
            'no_urut_pelayanan' => $maxUrutPst->no_urut_pelayanan,
            'kd_propinsi_pemohon' => substr($pelayanan->t_nop, 0, 2),
            'kd_dati2_pemohon' => substr($pelayanan->t_nop, 2, 2),
            'kd_kecamatan_pemohon' => substr($pelayanan->t_nop, 4, 3),
            'kd_kelurahan_pemohon' => substr($pelayanan->t_nop, 7, 3),
            'kd_blok_pemohon' => substr($pelayanan->t_nop, 10, 3),
            'no_urut_pemohon' => substr($pelayanan->t_nop, 13, 4),
            'kd_jns_op_pemohon' => substr($pelayanan->t_nop, 17, 1),
            'thn_peng_pst' => $pelayanan->t_tahun_pajak,
            'jns_sk' => self::KD_SK,
            'no_sk' => $noSk,
            'status_sk_peng_pst' => '0',
            'pct_pengurangan_pst' => $pelayanan->validasi->t_besar_pengurangan,
        ];

        PenguranganPstSimpbb::create($dataPengurangan);
    }

    public function updateSppt($pelayanan)
    {
        $sppt = (new Sppt())->getByNopTahunSppt($pelayanan->t_nop, $pelayanan->t_tahun_pajak)->first();
        $pajakTerhutang = (int) $sppt->pbb_yg_harus_dibayar_sppt;
        $faktorPengurangSebelumnya = $sppt->faktor_pengurang_sppt;
        $faktorPengurang = round($pajakTerhutang * ($pelayanan->t_besar_pengurangan / 100));
        $pbbYgHarusDibayar = $pajakTerhutang - $faktorPengurang;

        Pelayanan::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->update([
            't_his_pbb_terhutang' => $sppt->pbb_terhutang_sppt,
            't_his_faktor_pengurang' => $sppt->faktor_pengurang_sppt,
            't_his_pbb_yg_harus_dibayar' => $sppt->pbb_yg_harus_dibayar_sppt,
        ]);

        $updateSppt = [
            'faktor_pengurang_sppt' => (int) $faktorPengurang + $faktorPengurangSebelumnya,
            'pbb_yg_harus_dibayar_sppt' => (int) $pbbYgHarusDibayar,
        ];
        (new Sppt())->getByNopTahunSppt($pelayanan->t_nop, $pelayanan->t_tahun_pajak)->update($updateSppt);
    }
}
