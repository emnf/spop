<?php

namespace App\Service;

use App\Helpers\PelayananHelper;
use App\Models\Pelayanan\Pelayanan;
use App\Models\User;
use App\Notifications\HasilPelayananPermohonanNotify;
use App\Notifications\PendaftaranNotify;
use App\Notifications\ValidasiPermohonanNotify;
use App\Notifications\VerifikasiLapanganPermohonanNotify;
use App\Notifications\VerifikasiPermohonanNotify;

class NotificationService
{
    public function notifyPendaftaran(Pelayanan $pelayanan)
    {
        // notif ke verifikator
        $users = User::role('Verifikator Berkas')->get();

        foreach ($users as $key => $user) {
            $user->notify(new PendaftaranNotify($pelayanan, auth()->user()));
        }
    }

    public function notifyVerifikasi(Pelayanan $pelayanan)
    {
        // notif ke pemohon
        $user = User::find($pelayanan->created_by);
        $message = "Permohonan nomor " . PelayananHelper::formatNoPelayanan(date('Y', strtotime($pelayanan->t_tgl_pelayanan)), $pelayanan->t_id_jenis_pajak, $pelayanan->t_no_pelayanan) . " sudah di Verifikasi. ";
        $user->notify(new VerifikasiPermohonanNotify($pelayanan, auth()->user(), $message));

        // notif ke validator
        $validators = User::role('Validator')->get();
        foreach ($validators as $key => $user) {
            $message = "Permohonan nomor " . PelayananHelper::formatNoPelayanan(date('Y', strtotime($pelayanan->t_tgl_pelayanan)), $pelayanan->t_id_jenis_pajak, $pelayanan->t_no_pelayanan) . " diajukan ke Validator. ";
            $user->notify(new VerifikasiPermohonanNotify($pelayanan, auth()->user(), $message));
        }
    }

    public function notifyValidasiVerlap(Pelayanan $pelayanan)
    {
        // notif ke pemohon
        $user = User::find($pelayanan->created_by);
        $user->notify(new ValidasiPermohonanNotify($pelayanan, auth()->user()));

        // notif ke verifikator lapangan
        $validators = User::role('Verifikator Lapangan')->get();
        foreach ($validators as $key => $user) {
            $user->notify(new ValidasiPermohonanNotify($pelayanan, auth()->user()));
        }
    }

    public function notifyValidasi(Pelayanan $pelayanan)
    {
        // notif ke pemohon
        $user = User::find($pelayanan->created_by);
        $user->notify(new ValidasiPermohonanNotify($pelayanan, auth()->user()));
    }

    public function notifyHasilPelayanan(Pelayanan $pelayanan)
    {
        // notif ke pemohon
        $user = User::find($pelayanan->created_by);
        $user->notify(new HasilPelayananPermohonanNotify($pelayanan, auth()->user()));
    }
}
