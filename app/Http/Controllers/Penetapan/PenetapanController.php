<?php

namespace App\Http\Controllers\Penetapan;

use App\Helpers\DatagridHelpers;
use App\Helpers\PelayananHelper;
use App\Http\Controllers\Controller;
use App\Models\Buku;
use App\Models\Hasil\HasilPelayanan;
use App\Models\ModelTempatPembayaran;
use App\Models\Njoptkp;
use App\Models\OPAnggota;
use App\Models\Pbb\DatObjekPajak;
use App\Models\Pbb\Kecamatan;
use App\Models\Pbb\PegawaiPbb;
use App\Models\Pbb\PenetapanTerseleksi;
use App\Models\Pbb\Procedure;
use App\Models\Pbb\Sppt;
use App\Models\Pbb\Tarif;
use App\Models\PBBMinimal;
use App\Models\Setting\JenisPelayanan;
use App\Models\Setting\Pegawai;
use App\Models\TempatPembayaranSpptMasal;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use PDO;

class PenetapanController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $data['pegawais'] = PegawaiPbb::all();
        $data['jenisPelayanan'] = JenisPelayanan::isActive()->get();
        $data['kd_propinsi'] = env('KD_PROPINSI');
        $data['kd_dati2'] = env(('KD_DATI2'));
        $data['kecamatan'] = Kecamatan::all();

        return view('penetapan.index', $data);
    }

    public function objekPajak(Request $request)
    {
        $nopDari = explode('.', $request['nopdari']);       //explode blok, urut, jenis
        $nopHingga = explode('.', $request['nophingga']);   //explode blok, urut, jenis

        $objekPajak = DatObjekPajak::where('kd_propinsi', $request['prop'])
            ->where('kd_dati2', $request['dati'])
            ->where('kd_kecamatan', $request['kec'])
            ->where('kd_kelurahan', $request['kel'])
            ->whereBetween('kd_blok', [$nopDari[0], $nopHingga[0]])
            ->whereBetween('no_urut', [$nopDari[1], $nopHingga[1]])
            ->whereBetween('kd_jns_op', [$nopDari[2], $nopHingga[2]])
            ->get();
        // varDumpHehe($objekPajak);

        // dd($objekPajak);
        return response()->json($objekPajak);
    }

    public function terseleksi(Request $request)
    {
        ini_set('max_execution_time', 600);                 //limit
        $arr = [];                                          //variable
        $validate = $request->all();                        //validate request
        try {
            if ($validate) {                                    //sukses validate
                // $pdo = DB::getPdo();                            //buat objek pdo
                $oracleConnection = DB::connection('simpbb');

                $nop = $request['nop'];                         //ambik kode nop
                $paramNop = [
                    'propinsi' => $nop['kd_propinsi'],                //ambil kode propinsi
                    'dati' => $nop['kd_dati2'],                       //ambil kode kabupaten/kota
                    'kecamatan' => $nop['kd_kecamatan'],              //ambil kode kecamatan
                    'kelurahan' => $nop['kd_kelurahan'],              //ambil kode kelurahan
                    'blok' => $nop['kd_blok'],                //simpan kd blok
                    'urut' => $nop['no_urut'],                //simpan urut
                    'jns' => $nop['kd_jns_op'],              //simpan jenis op
                    'thn' => $request['thn']                      //ambil tahun
                ];
                //   dd($paramNop);
                //  dd($nop['subjek_pajak_id']);

                // get data subjek_pajak_id supaya ga ditrim
                $wpId = $oracleConnection->select("SELECT subjek_pajak_id
            FROM dat_objek_pajak WHERE kd_propinsi = ? AND kd_dati2 = ? AND kd_kecamatan = ?
            AND kd_kelurahan = ? AND kd_blok = ? AND no_urut = ? AND kd_jns_op = ?", [
                    $nop['kd_propinsi'],
                    $nop['kd_dati2'],
                    $nop['kd_kecamatan'],
                    $nop['kd_kelurahan'],
                    $nop['kd_blok'],
                    $nop['no_urut'],
                    $nop['kd_jns_op']
                ]);

                // sebelum penetapan terseleksi cek dulu procedurenya ada atau engga
                $procedureName = 'PENETAPAN_TERSELEKSI';
                $exists = $oracleConnection->table('user_objects')
                    ->where('object_type', 'PROCEDURE')
                    ->where('object_name', $procedureName)
                    ->exists();

                if (!$exists) {
                    $data = ['tipe' => 'error', 'message' => 'Store Procedure ' . $procedureName . ' Tidak Ditemukan, Proses dihentikan.',];
                    return response()->json($data, 500);
                }

                // cari nilai njoptkp
                $njoptkp = Njoptkp::where('kd_propinsi', $paramNop['propinsi'])->where('kd_dati2', $paramNop['dati'])->where('thn_awal', '<=', $paramNop['thn'])->where('thn_akhir', '>=', $paramNop['thn'])->first();
                if (empty($njoptkp)) {
                    $data = ['tipe' => 'error', 'message' => 'Data Setting NJOPTKP Tidak Ditemukan. Cek Tabel NJOPTKP.',];
                    return response()->json($data, 500);
                } else {
                    $hitNjoptkp = $njoptkp->nilai_njoptkp * 1000;
                }

                //cari pbb minimal
                $pbbMin = PBBMinimal::where('kd_propinsi', $paramNop['propinsi'])->where('kd_dati2', $paramNop['dati'])->where('thn_pbb_minimal', $paramNop['thn'])->first();
                $pbbMinimal = $pbbMin ? $pbbMin->nilai_pbb_minimal : 0;

                // cari buku ketetapan
                $buku = Buku::where('thn_awal', '<=', $paramNop['thn'])->where('thn_akhir', '>=', $paramNop['thn'])->where('kd_buku', '<=', 5)
                    ->get();
                // dd($buku);
                if ($buku->isEmpty()) {
                    $data = ['tipe' => 'error', 'message' => 'Data Setting Buku Tidak Ditemukan. Cek Tabel REF_BUKU.',];
                    return response()->json($data, 500);
                }
                // lookup tempat pembayaran
                $tpMasal = TempatPembayaranSpptMasal::where('kd_propinsi', $paramNop['propinsi'])->where('kd_dati2', $paramNop['dati'])->where('kd_kecamatan', $paramNop['kecamatan'])->where('kd_kelurahan', $paramNop['kelurahan'])->where('thn_tp_sppt_masal', $paramNop['thn'])->first();
                if (empty($tpMasal)) {
                    $data = ['tipe' => 'error', 'message' => 'Data Tempat Pembayaran Masal Tidak Ditemukan. Cek Tabel TEMPAT_PEMBAYARAN_SPPT_MASAL.',];
                    return response()->json($data, 500);
                }

                $getNilaiNjop = $this->hitungDariAwal($paramNop);

                $this->hitNjoptkpIndividu($paramNop);

                // mulai proses penetapannya
                $parampenetapan = [
                    'propinsi'    => $paramNop['propinsi'],                //ambil kode propinsi
                    'dati'        => $paramNop['dati'],                       //ambil kode kabupaten/kota
                    'kecamatan'   => $paramNop['kecamatan'],              //ambil kode kecamatan
                    'kelurahan'   => $paramNop['kelurahan'],              //ambil kode kelurahan
                    'blok'        => $paramNop['blok'],                //simpan kd blok
                    'urut'        => $paramNop['urut'],                //simpan urut
                    'jns'         => $paramNop['jns'],               //simpan jenis op
                    'thn'         => $paramNop['thn'],                         //ambil tahun
                    'tempoBuku1'  => (string)date('Y-m-d', strtotime($request['tempos'])),   //ambil tgl jatuh tempo buku1
                    'tempoBuku2'  => (string)date('Y-m-d', strtotime($request['tempos'])),   //ambil tgl jatuh tempo buku2
                    'tempoBuku3'  => (string)date('Y-m-d', strtotime($request['tempos'])),   //ambil tgl jatuh tempo buku3
                    'tempoBuku4'  => (string)date('Y-m-d', strtotime($request['tempos'])),   //ambil tgl jatuh tempo buku4
                    'tempoBuku5'  => (string)date('Y-m-d', strtotime($request['tempos'])),   //ambil tgl jatuh tempo buku5
                    'terbitBuku1' => (string)date('Y-m-d', strtotime($request['terbits'])), //ambil tgl terbit buku1
                    'terbitBuku2' => (string)date('Y-m-d', strtotime($request['terbits'])), //ambil tgl terbit buku2
                    'terbitBuku3' => (string)date('Y-m-d', strtotime($request['terbits'])), //ambil tgl terbit buku3
                    'terbitBuku4' => (string)date('Y-m-d', strtotime($request['terbits'])), //ambil tgl terbit buku4
                    'terbitBuku5' => (string)date('Y-m-d', strtotime($request['terbits'])), //ambil tgl terbit buku5
                    'nip'         => $request['nip'],            //ambil nip user login
                    'tgl'         => (string) Carbon::now(),                           //ambil tanggal sekarang
                    'pbbMinimal'  => $pbbMinimal,
                    'kanwil'      => $tpMasal['kd_kanwil'],               //simpan kd_kanwil
                    'kppbb'       => $tpMasal['kd_kppbb'],               //simpan kd_kantor
                    'bankTunggal' => $tpMasal['kd_bank_tunggal'],
                    'bankPersepsi' => $tpMasal['kd_bank_persepsi'],
                    'tp'          => $tpMasal['kd_tp'],                       //simpan kd_tp
                    'subjek'      => $wpId[0]->subjek_pajak_id,      //simpan subjek nop
                    'persil'      => $nop['no_persil'],            //simpan persil nop
                    'njopBumi'    => $getNilaiNjop['njopBumi'],
                    'njopBng'     => $getNilaiNjop['njopBng'],
                    'luasBumi'    => (int)$nop['total_luas_bumi'], //simpan luas bumi
                    'luasBng'     => (int)$nop['total_luas_bng'],  //simpan luas bangunan
                    'njoptkp'     => $hitNjoptkp,  //nilai njoptkp x 1000 gak tau kenapa'
                    'bukuMin1'    => (int)$buku[0]->nilai_min_buku,      //min buku1
                    'bukuMax1'    => (int)$buku[0]->nilai_max_buku,      //max buku1
                    'bukuMin2'    => (int)$buku[1]->nilai_min_buku,      //min buku2
                    'bukuMax2'    => (int)$buku[1]->nilai_max_buku,      //max buku2
                    'bukuMin3'    => (int)$buku[2]->nilai_min_buku,      //min buku3
                    'bukuMax3'    => (int)$buku[2]->nilai_max_buku,      //max buku3
                    'bukuMin4'    => (int)$buku[3]->nilai_min_buku,      //min buku4
                    'bukuMax4'    => (int)$buku[3]->nilai_max_buku,      //max buku4
                    'bukuMin5'    => (int)$buku[4]->nilai_min_buku,      //min buku5
                    'bukuMax5'    => (int)$buku[4]->nilai_max_buku,      //max buku5
                    'vTap'        => strval(1),
                ];

                $procedureName = 'PENETAPAN_TERSELEKSI';
                $stmt = $oracleConnection->getPdo()->prepare("begin " . $procedureName .  " (:pPropinsi, :pDati, :pKecamatan, :pKelurahan, :pBlok, :pUrut, :pJenis, :pPersil, :pNjopBumi, :pNjopBng, :pLuasBumi, :pLuasBng, :pWP, :pKanwil, :pKppbb, :pBankTunggal, :pBankPersepsi, :pTp, :pNjoptkp, :pMinBuku1, :pMaxBuku1, :pMinBuku2, :pMaxBuku2, :pMinBuku3, :pMaxBuku3, :pMinBuku4, :pMaxBuku4, :pMinBuku5, :pMaxBuku5, :pJpt, :pTahun, :pTempoBuku1, :pTempoBuku2, :pTempoBuku3, :pTempoBuku4, :pTempoBuku5, :pTerbitBuku1, :pTerbitBuku2, :pTerbitBuku3, :pTerbitBuku4, :pTerbitBuku5, :pNip, :pNipTanggal, :pPbbMin); end;"); //, :pTap
                $stmt->bindParam(':pPropinsi', $parampenetapan['propinsi'], PDO::PARAM_STR);
                $stmt->bindParam(':pDati', $parampenetapan['dati'], PDO::PARAM_STR);
                $stmt->bindParam(':pKecamatan', $parampenetapan['kecamatan'], PDO::PARAM_STR);
                $stmt->bindParam(':pKelurahan', $parampenetapan['kelurahan'], PDO::PARAM_STR);
                $stmt->bindParam(':pBlok', $parampenetapan['blok'], PDO::PARAM_STR);
                $stmt->bindParam(':pUrut', $parampenetapan['urut'], PDO::PARAM_STR);
                $stmt->bindParam(':pJenis', $parampenetapan['jns'], PDO::PARAM_STR);
                $stmt->bindParam(':pPersil', $parampenetapan['persil'], PDO::PARAM_STR);
                $stmt->bindParam(':pNjopBumi', $parampenetapan['njopBumi'], PDO::PARAM_INT);
                $stmt->bindParam(':pNjopBng', $parampenetapan['njopBng'], PDO::PARAM_INT);
                $stmt->bindParam(':pLuasBumi', $parampenetapan['luasBumi'], PDO::PARAM_INT);
                $stmt->bindParam(':pLuasBng', $parampenetapan['luasBng'], PDO::PARAM_INT);
                $stmt->bindParam(':pWP', $parampenetapan['subjek'], PDO::PARAM_STR);
                $stmt->bindParam(':pKanwil', $parampenetapan['kanwil'], PDO::PARAM_STR);
                $stmt->bindParam(':pKppbb', $parampenetapan['kppbb'], PDO::PARAM_STR);
                $stmt->bindParam(':pBankTunggal', $parampenetapan['bankTunggal'], PDO::PARAM_STR);
                $stmt->bindParam(':pBankPersepsi', $parampenetapan['bankPersepsi'], PDO::PARAM_STR);
                $stmt->bindParam(':pTp', $parampenetapan['tp'], PDO::PARAM_STR);
                $stmt->bindParam(':pNjoptkp', $parampenetapan['njoptkp'], PDO::PARAM_INT);
                $stmt->bindParam(':pMinBuku1', $parampenetapan['bukuMin1'], PDO::PARAM_INT);
                $stmt->bindParam(':pMaxBuku1', $parampenetapan['bukuMax1'], PDO::PARAM_INT);
                $stmt->bindParam(':pMinBuku2', $parampenetapan['bukuMin2'], PDO::PARAM_INT);
                $stmt->bindParam(':pMaxBuku2', $parampenetapan['bukuMax2'], PDO::PARAM_INT);
                $stmt->bindParam(':pMinBuku3', $parampenetapan['bukuMin3'], PDO::PARAM_INT);
                $stmt->bindParam(':pMaxBuku3', $parampenetapan['bukuMax3'], PDO::PARAM_INT);
                $stmt->bindParam(':pMinBuku4', $parampenetapan['bukuMin4'], PDO::PARAM_INT);
                $stmt->bindParam(':pMaxBuku4', $parampenetapan['bukuMax4'], PDO::PARAM_INT);
                $stmt->bindParam(':pMinBuku5', $parampenetapan['bukuMin5'], PDO::PARAM_INT);
                $stmt->bindParam(':pMaxBuku5', $parampenetapan['bukuMax5'], PDO::PARAM_INT);
                $stmt->bindParam(':pJpt', $parampenetapan['thn'], PDO::PARAM_STR);
                $stmt->bindParam(':pTahun', $parampenetapan['thn'], PDO::PARAM_STR);
                $stmt->bindParam(':pTempoBuku1', $parampenetapan['tempoBuku1'], PDO::PARAM_STR);
                $stmt->bindParam(':pTempoBuku2', $parampenetapan['tempoBuku2'], PDO::PARAM_STR);
                $stmt->bindParam(':pTempoBuku3', $parampenetapan['tempoBuku3'], PDO::PARAM_STR);
                $stmt->bindParam(':pTempoBuku4', $parampenetapan['tempoBuku4'], PDO::PARAM_STR);
                $stmt->bindParam(':pTempoBuku5', $parampenetapan['tempoBuku5'], PDO::PARAM_STR);
                $stmt->bindParam(':pTerbitBuku1', $parampenetapan['terbitBuku1'], PDO::PARAM_STR);
                $stmt->bindParam(':pTerbitBuku2', $parampenetapan['terbitBuku2'], PDO::PARAM_STR);
                $stmt->bindParam(':pTerbitBuku3', $parampenetapan['terbitBuku3'], PDO::PARAM_STR);
                $stmt->bindParam(':pTerbitBuku4', $parampenetapan['terbitBuku4'], PDO::PARAM_STR);
                $stmt->bindParam(':pTerbitBuku5', $parampenetapan['terbitBuku5'], PDO::PARAM_STR);
                $stmt->bindParam(':pNip', $parampenetapan['nip'], PDO::PARAM_STR);
                $stmt->bindParam(':pNipTanggal', $parampenetapan['tgl'], PDO::PARAM_STR);
                $stmt->bindParam(':pPbbMin', $parampenetapan['pbbMinimal'], PDO::PARAM_INT);
                    // $stmt->bindParam(':pTap', $parampenetapan['vTap'], PDO::PARAM_STR)
                ;

                $stmt->execute();
            }
            $data = ['tipe' => 'success', 'status' => 'Proses Penetapan berhasil', 'data' => $arr,];
            $terseleksi = [
                'created_by' => auth()->user()->id,
                'nip_perekam' => $request['nip'],
                'kd_propinsi' => $paramNop['propinsi'],
                'kd_dati2' => $paramNop['dati'],
                'kd_kecamatan' => $paramNop['kecamatan'],
                'kd_kelurahan' => $paramNop['kelurahan'],
                'kd_blok' => $paramNop['blok'],
                'no_urut' => $paramNop['urut'],
                'kd_jns_op' => $paramNop['jns'],
                'thn_pajak_sppt' => $paramNop['thn']
            ];
            PenetapanTerseleksi::create($terseleksi);
            return response()->json($stmt);
        } catch (\Throwable $th) {
            //throw $th;
            dd($th);
        }
    }

    function hitungDariAwal($paramNop)
    {
        $pdo = DB::getPdo();
        $oracleConnection = DB::connection('simpbb');
        $outNjopBumi = 0;
        $outNjopBng = 0;

        $hitNjopBumi = 'PENENTUAN_NJOP_BUMI';
        $stmt = $oracleConnection->getPdo()->prepare("begin PENENTUAN_NJOP_BUMI (:pPropinsi, :pDati, :pKecamatan, :pKelurahan, :pBlok, :pUrut, :pJenis, :pThn, 1, :pNjopBumi); end;");
        $stmt->bindParam(':pPropinsi', $paramNop['propinsi'], PDO::PARAM_STR);
        $stmt->bindParam(':pDati', $paramNop['dati'], PDO::PARAM_STR);
        $stmt->bindParam(':pKecamatan', $paramNop['kecamatan'], PDO::PARAM_STR);
        $stmt->bindParam(':pKelurahan', $paramNop['kelurahan'], PDO::PARAM_STR);
        $stmt->bindParam(':pBlok', $paramNop['blok'], PDO::PARAM_STR);
        $stmt->bindParam(':pUrut', $paramNop['urut'], PDO::PARAM_STR);
        $stmt->bindParam(':pJenis', $paramNop['jns'], PDO::PARAM_STR);
        $stmt->bindParam(':pThn', $paramNop['thn'], PDO::PARAM_STR);
        $stmt->bindParam(':pNjopBumi', $outNjopBumi, PDO::PARAM_INT);
        $stmt->execute();


        // hit njop bangunan
        $hitNjopBng = 'PENENTUAN_NJOP_BNG';
        $stmt = $oracleConnection->getPdo()->prepare("begin " . $hitNjopBng .  " (:pPropinsi, :pDati, :pKecamatan, :pKelurahan, :pBlok, :pUrut, :pJenis, :pThn, 1, :pNjopBng); end;");
        $stmt->bindParam(':pPropinsi', $paramNop['propinsi'], PDO::PARAM_STR);
        $stmt->bindParam(':pDati', $paramNop['dati'], PDO::PARAM_STR);
        $stmt->bindParam(':pKecamatan', $paramNop['kecamatan'], PDO::PARAM_STR);
        $stmt->bindParam(':pKelurahan', $paramNop['kelurahan'], PDO::PARAM_STR);
        $stmt->bindParam(':pBlok', $paramNop['blok'], PDO::PARAM_STR);
        $stmt->bindParam(':pUrut', $paramNop['urut'], PDO::PARAM_STR);
        $stmt->bindParam(':pJenis', $paramNop['jns'], PDO::PARAM_STR);
        $stmt->bindParam(':pThn', $paramNop['thn'], PDO::PARAM_STR);
        $stmt->bindParam(':pNjopBng', $outNjopBng, PDO::PARAM_INT);
        $stmt->execute();

        $njopBumi = $outNjopBumi;
        $njopBng = $outNjopBng;

        return array(
            'njopBumi' => $njopBumi,
            'njopBng' => $njopBng
        );

        // proses penilaian anggota dari op bersama
        $DatOpAnggota = OPAnggota::where('kd_propinsi', $paramNop['propinsi'])
            ->where('kd_dati2', $paramNop['dati'])
            ->where('kd_kecamatan', $paramNop['kecamatan'])
            ->where('kd_kelurahan', $paramNop['kelurahan'])
            ->where('kd_blok', $paramNop['blok'])
            ->where('no_urut', $paramNop['urut'])
            ->where('kd_jns_op', $paramNop['jns'])
            ->first();

        if (!$DatOpAnggota) {
            return null;
        } else {
            $hitOpBersama = 'PENILAIAN_OP_ANGGOTA_MASSAL';
            $stmt = $oracleConnection->getPdo()->prepare("begin " . $hitOpBersama .  " (:pPropinsi, :pDati, :pKecamatan, :pKelurahan, :pBlok, :pUrut, :pJenis, :pThn, 1); end;");
            $stmt->bindParam(':pPropinsi', $DatOpAnggota['kd_propinsi_induk'], PDO::PARAM_STR);
            $stmt->bindParam(':pDati', $DatOpAnggota['kd_dati2_induk'], PDO::PARAM_STR);
            $stmt->bindParam(':pKecamatan', $DatOpAnggota['kd_kecamatan_induk'], PDO::PARAM_STR);
            $stmt->bindParam(':pKelurahan', $DatOpAnggota['kd_kelurahan_induk'], PDO::PARAM_STR);
            $stmt->bindParam(':pBlok', $DatOpAnggota['kd_blok_induk'], PDO::PARAM_STR);
            $stmt->bindParam(':pUrut', $DatOpAnggota['no_urut_induk'], PDO::PARAM_STR);
            $stmt->bindParam(':pJenis', $DatOpAnggota['kd_jns_op_induk'], PDO::PARAM_STR);
            $stmt->bindParam(':pThn', $paramNop['thn'], PDO::PARAM_STR);
            $stmt->execute();
        }
    }

    public function hitNjoptkpIndividu($paramNop)
    {
        $pdo = DB::getPdo();
        $oracleConnection = DB::connection('simpbb');
        // proses hitung njoptkp nopnya
        $hitNjoptkp = 'NJOPTKPINDIVIDU';
        $stmt = $oracleConnection->getPdo()->prepare("begin " . $hitNjoptkp .  " (:pPropinsi, :pDati, :pKecamatan, :pKelurahan, :pBlok, :pUrut, :pJenis, :pThn); end;");
        $stmt->bindParam(':pPropinsi', $paramNop['propinsi'], PDO::PARAM_STR);
        $stmt->bindParam(':pDati', $paramNop['dati'], PDO::PARAM_STR);
        $stmt->bindParam(':pKecamatan', $paramNop['kecamatan'], PDO::PARAM_STR);
        $stmt->bindParam(':pKelurahan', $paramNop['kelurahan'], PDO::PARAM_STR);
        $stmt->bindParam(':pBlok', $paramNop['blok'], PDO::PARAM_STR);
        $stmt->bindParam(':pUrut', $paramNop['urut'], PDO::PARAM_STR);
        $stmt->bindParam(':pJenis', $paramNop['jns'], PDO::PARAM_STR);
        $stmt->bindParam(':pThn', $paramNop['thn'], PDO::PARAM_STR);
        $stmt->execute();
    }

    public function datagridSudah(Request $request)
    {
        try {
            $helper = new PelayananHelper;
            $penetapan = (new PenetapanTerseleksi())->datagrid(
                $request->filter,
                $request->pagination,
                $request->sorting,
                $helper
            );
            $dataArr = [];
            foreach ($penetapan as $key => $v) {
                $nop = $v->kd_propinsi .
                    $v->kd_dati2 .
                    $v->kd_kecamatan .
                    $v->kd_kelurahan .
                    $v->kd_blok .
                    $v->no_urut .
                    $v->kd_jns_op;
                $formatNop = $helper->formatStringToNop($nop);
                $parsedNop = $helper->parseNop($formatNop);

                $sppt = (new Sppt())->getDataKetetapan($parsedNop, $v->thn_pajak_sppt);
                $user = User::find($v->created_by)->first();


                $dataArr[] = [
                    'tglpenetapan' => date('d-m-Y', strtotime($sppt->tgl_terbit_sppt)),
                    'nip_pencetak_sppt' => $sppt->nip_pencetak_sppt,
                    'nop' => $formatNop,
                    'tahun' => $sppt->thn_pajak_sppt,
                    'user_penetapan' => $user->name,
                    'actionList' => array()
                ];
            }
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $penetapan);
        } catch (\Throwable $th) {
            dd($th);
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $penetapan);
        }
    }

    public function rinciSppt(Request $request)
    {
        $sppt = [];
        $tempat = [];
        $bayar = [];
        $validate = Validator::make($request->all(), [
            'nop' => 'required',
            'thn' => 'required',
        ]);

        if (!$validate->fails()) {
            $nop = str_replace('.', '', $request['nop']);
            $arrNop = explode('.', $request['nop']);

            $oracleConnection = DB::connection('simpbb');
            $sppt = SPPT::where('kd_propinsi', $arrNop[0])
                ->where('kd_dati2', $arrNop[1])
                ->where('kd_kecamatan', $arrNop[2])
                ->where('kd_kelurahan', $arrNop[3])
                ->where('kd_blok', $arrNop[4])
                ->where('no_urut', $arrNop[5])
                ->where('kd_jns_op', $arrNop[6])
                ->where('thn_pajak_sppt', $request['thn'])
                ->get();

            if (count($sppt) > 0) {
                $sppt = $sppt->first();
                $tempat = ModelTempatPembayaran::where('kd_kanwil', $sppt->kd_kanwil_bank)
                    ->where('kd_kppbb', $sppt->kd_kppbb_bank)
                    ->where('kd_bank_tunggal', $sppt->kd_bank_tunggal)
                    ->where('kd_bank_persepsi', $sppt->kd_bank_persepsi)
                    ->where('kd_tp', $sppt->kd_tp)
                    ->get();

                $pembayaran = $oracleConnection->table('pembayaran_sppt')
                    ->select(
                        DB::raw("kd_propinsi || kd_dati2 || kd_kecamatan || kd_kelurahan || kd_blok || no_urut || kd_jns_op || thn_pajak_sppt as nopthn"),
                        DB::raw('SUM(nvl(denda_sppt,0)) as denda_sppt'),
                        DB::raw('SUM(nvl(jml_sppt_yg_dibayar,0)) - SUM(nvl(denda_sppt,0)) as pokok_dibayar'),
                        DB::raw('SUM(nvl(jml_sppt_yg_dibayar,0)) as total_pembayaran'),
                        DB::raw('MAX(tgl_pembayaran_sppt) as tgl_pembayaran_sppt')
                    )
                    ->where('kd_propinsi', $arrNop[0])
                    ->where('kd_dati2', $arrNop[1])
                    ->where('kd_kecamatan', $arrNop[2])
                    ->where('kd_kelurahan', $arrNop[3])
                    ->where('kd_blok', $arrNop[4])
                    ->where('no_urut', $arrNop[5])
                    ->where('kd_jns_op', $arrNop[6])
                    ->where('thn_pajak_sppt', $request['thn'])
                    ->groupBy('kd_propinsi', 'kd_dati2', 'kd_kecamatan', 'kd_kelurahan', 'kd_blok', 'no_urut', 'kd_jns_op', 'thn_pajak_sppt')
                    ->first();

                $dataPembayaran = [
                    'nopthn' => isset($pembayaran) ? $pembayaran->nopthn : null,
                    'tglBayar' => isset($pembayaran) ? $pembayaran->tgl_pembayaran_sppt : null,
                    'dendaDibayar' => isset($pembayaran) ? (int) $pembayaran->denda_sppt : 0,
                    'pokokDibayar' => isset($pembayaran) ? (int) $pembayaran->pokok_dibayar : 0,
                    'totalPembayaran' => isset($pembayaran) ? (int) $pembayaran->total_pembayaran : 0,
                ];
                // dd($dataPembayaran);

                $datOp = $oracleConnection->table('dat_objek_pajak')
                    ->select(
                        'subjek_pajak_id',
                        'jalan_op',
                        'blok_kav_no_op',
                        'rt_op',
                        'rw_op',
                        'total_luas_bumi',
                        'total_luas_bng',
                        'njop_bumi',
                        'njop_bng',
                        // 'keterangan_op',
                        // 'keterangan_spop'
                    )
                    ->where('kd_propinsi', $arrNop[0])
                    ->where('kd_dati2', $arrNop[1])
                    ->where('kd_kecamatan', $arrNop[2])
                    ->where('kd_kelurahan', $arrNop[3])
                    ->where('kd_blok', $arrNop[4])
                    ->where('no_urut', $arrNop[5])
                    ->where('kd_jns_op', $arrNop[6])
                    ->first();
                // dd($datOp);

                $opBumi = $oracleConnection->table('dat_op_bumi')
                    ->select('kd_znt')
                    ->where('kd_propinsi', $arrNop[0])
                    ->where('kd_dati2', $arrNop[1])
                    ->where('kd_kecamatan', $arrNop[2])
                    ->where('kd_kelurahan', $arrNop[3])
                    ->where('kd_blok', $arrNop[4])
                    ->where('no_urut', $arrNop[5])
                    ->where('kd_jns_op', $arrNop[6])
                    ->where('no_bumi', '=', '1')
                    ->get();
                // dd($opBumi);

                // $kdJpt = isset($datOp->keterangan_op) ? $datOp->keterangan_op : '00';
                // $jpt = $oracleConnection->table('jpb_jpt as a')
                //     ->join('lookup_item as b', 'a.kd_kelompok_jpt', '=', 'b.kd_lookup_item')
                //     ->select('a.kd_jpb_jpt', 'a.nm_jpb_jpt', 'a.kd_kelompok_jpt', 'b.nm_lookup_item as nm_kelompok_jpt')
                //     ->where('b.kd_lookup_group', '=', '85')
                //     ->where('a.kd_jpb_jpt', '=', $kdJpt)
                //     ->get();
                // dd($jpt);

                // if ($sppt->njkp_sppt == 0 || $sppt->njkp_total_sppt == null) {
                $tarif = Tarif::where('kd_propinsi', $arrNop[0])
                    ->where('kd_dati2', $arrNop[1])
                    ->where('thn_awal', '<=', $request['thn'])
                    ->where('thn_akhir', '>=', $request['thn'])
                    ->where('njop_min', '<=', $sppt->njop_sppt)
                    ->where('njop_max', '>=', $sppt->njop_sppt)
                    // ->where('kd_kelompok_jpt', '0')
                    // ->select('nilai_tarif')
                    ->first();
                // } else {
                // $tarif = Tarif::where('kd_propinsi', $arrNop[0])
                //     ->where('kd_dati2', $arrNop[1])
                //     ->where('thn_awal', '<=', $request['thn'])
                //     ->where('thn_akhir', '>=', $request['thn'])
                //     ->where('njop_min', '<=', $sppt->njop_sppt)
                //     ->where('njop_max', '>=', $sppt->njop_sppt)
                //     ->where('kd_kelompok_jpt', '=', $jpt[0]->kd_kelompok_jpt)
                //     ->select('nilai_tarif')
                //     ->first();
                // }
                if ($tarif->nilai_tarif == '.111') {
                    $tarifs = 0.111;
                }
                if ($tarif->nilai_tarif == '.222') {
                    $tarifs = 0.222;
                }
                if ($tarif->nilai_tarif == '.1') {
                    $tarifs = 0.1;
                }
                if ($tarif->nilai_tarif == '.2') {
                    $tarifs = 0.2;
                }
                $pegawai = $oracleConnection->table('pegawai')
                    ->select(
                        'nm_pegawai',
                        'nip_baru'
                    )
                    ->where('nip', $sppt->nip_pencetak_sppt)
                    ->first();
                // dd($pegawai);

                $tipe = 'success';
                $status = 'Menampilkan data';
            } else {
                $tipe = 'error';
                $status = 'NOP dan tahun pajak tidak ditemukan';
            }
        } else {
            $tipe = 'warning';
            $status = 'Terdapat inputan kosong';
        }

        $mBumi = $sppt->njop_bumi_sppt / $sppt->luas_bumi_sppt;
        $mBng = $sppt->luas_bng_sppt < 1 ? 0 : $sppt->njop_bng_sppt / $sppt->luas_bng_sppt;
        $njopHit = 0;
        if (($sppt->njop_sppt - $sppt->njoptkp_sppt) < 0 || ($sppt->njop_sppt -
            $sppt->njoptkp_sppt) == null) {
            $njopHit = 0;
        } else {
            $njopHit = ($sppt->njop_sppt - $sppt->njoptkp_sppt);
        }

        if ($sppt->faktor_pengurangan_sppt == null || $sppt->faktor_pengurangan_sppt == 0) {
            $faktorPengurang = 0;
        } else {
            $faktorPengurang = $sppt->faktor_pengurangan_sppt;
        }

        if ($dataPembayaran['dendaDibayar'] == null || $dataPembayaran['dendaDibayar'] == 0) {
            $dendaDibayar = 0;
        } else {
            $dendaDibayar = $dataPembayaran['dendaDibayar'];
        }

        if ($dataPembayaran['pokokDibayar'] == null || ($dataPembayaran['pokokDibayar']) == 0) {
            $pokokDiBayar = 0;
        } else {
            $pokokDiBayar = $dataPembayaran['pokokDibayar'];
        }

        // <dt class="col-sm-2">
        //                         Group JPT
        //                     </dt>
        //                     <dd class="col-sm-4">
        //                         : &nbsp; <span id="groupJpt">' . $jpt[0]->nm_kelompok_jpt . '</span>
        //                     </dd>
        //                     <dt class="col-sm-2">
        //                         Nama JPT
        //                     </dt>
        //                     <dd class="col-sm-4">
        //                         : &nbsp; <span id="nmJpt">' . $datOp->keterangan_op . '</span>
        //                     </dd>

        $modalBody = '
        <div class="row">
                    <div class="col-12">
                        <h6 class="text-dark-50"></h6>
                        <dl class="row">
                            <dt class="col-sm-2">
                                Nama Wajib Pajak
                            </dt>
                            <dd class="col-sm-10">
                                : &nbsp; <span id="namaWP">' . $sppt->nm_wp_sppt . '</span>
                            </dd>
                            <dt class="col-sm-2">
                                Alamat Wajib Pajak
                            </dt>
                            <dd class="col-sm-4">
                                : &nbsp; <span id="alWP">' . $sppt->jln_wp_sppt . $sppt->blok_kav_no_wp_sppt . ', Kelurahan ' . $sppt->kelurahan_wp_sppt . ',Kota ' . $sppt->kelurahan_wp_sppt . '</span>
                            </dd>
                            <dt class="col-sm-2">
                                RT/RW WP
                            </dt>
                            <dd class="col-sm-4">
                                : &nbsp; <span id="rtrwWp">' . ($sppt->rt_wp_sppt != null ? $sppt->rt_wp_sppt : '000') . '/' . ($sppt->rw_wp_sppt != null ? $sppt->rw_wp_sppt : '00') . '</span>
                            </dd>
                            <dt class="col-sm-2">
                                Letak Objek Pajak
                            </dt>
                            <dd class="col-sm-4">
                                : &nbsp; <span id="alOP">' .
            $datOp->jalan_op . $datOp->blok_kav_no_op . '</span>
                            </dd>
                            <dt class="col-sm-2">
                                RT/RW OP
                            </dt>
                            <dd class="col-sm-4">
                                : &nbsp; <span id="rtrwOp">' . ($datOp->rt_op != null ? $datOp->rt_op : '000') . '/' . ($datOp->rw_op != null ? $datOp->rw_op : '00') .  '</span>
                            </dd>
                            <dt class="col-sm-2">
                                Persil
                            </dt>
                            <dd class="col-sm-4">
                                : &nbsp; <span id="persil">' . $sppt->no_persil_sppt . '</span>
                            </dd>
                            <dt class="col-sm-2">
                                Kode ZNT
                            </dt>
                            <dd class="col-sm-4">
                                : &nbsp; <span id="kdZnt">' . $opBumi[0]->kd_znt . '</span>
                            </dd>
                            
                        </dl>
                    </div>

                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Luas</th>
                                        <th>Kelas</th>
                                        <th>NJOP(m2)</th>
                                        <th>Total NJOP</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="">Bumi</td>
                                        <td class="text-right" id="luasBumi">
                                            ' . $sppt->luas_bumi_sppt . '
                                        </td>
                                        <td class="text-center" id="kelasBumi">
                                            ' . $sppt->kd_kls_tanah . '
                                        </td>
                                        <td class="text-right" id="njopMBumi">
                                            ' . number_format($mBumi, 0, ',', '.') . '
                                        </td>
                                        <td class="text-right" id="njopTotalBumi">
                                            ' . number_format($sppt->njop_bumi_sppt, 0, ',', '.') . '
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="">Bangunan</td>
                                        <td class="text-right" id="luasBng">
                                            ' . $sppt->luas_bng_sppt . '
                                        </td>
                                        <td class="text-center" id="kelasBng">
                                            ' . $sppt->kd_kls_bng . '
                                        </td>
                                        <td class="text-right" id="njopMBng">
                                            ' . number_format($sppt->$mBng, 0, ',', '.') . '
                                        </td>
                                        <td class="text-right" id="njopTotalBng">
                                            ' . number_format($sppt->njop_bng_sppt, 0, ',', '.') . '
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="">Bumi*</td>
                                        <td class="text-right">

                                        </td>
                                        <td class="text-center">

                                        </td>
                                        <td class="text-right">

                                        </td>
                                        <td class="text-right">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="">Bangunan*</td>
                                        <td class="text-right">

                                        </td>
                                        <td class="text-center">

                                        </td>
                                        <td class="text-right">

                                        </td>
                                        <td class="text-right">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">Jumlah NJOP Bumi</td>
                                        <td class="text-right" id="jumlahNjopBumi">
                                            ' . $sppt->njop_bumi_sppt . '
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">Jumlah NJOP Bangunan</td>
                                        <td class="text-right" id="jumlahNjopBng">
                                            ' . $sppt->njop_bng_sppt . '
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">NJOP Sebagai Dasar Pengenaan PBB</td>
                                        <td class="text-right" id="njopTotal">
                                            ' . $sppt->njop_sppt . '
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">NJOPTKP</td>
                                        <td class="text-right" id="njoptkp">
                                            ' . $sppt->njoptkp_sppt . '
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">NJOP Sebagai Dasar Perhitungan PBB</td>
                                        <td class="text-right" id="njopHit">
                                            ' . $njopHit . '
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">NJKP</td>
                                        <td class="text-right" id="tarifPbb">
                                            ' . $sppt->njkp_sppt . '%
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">Tarif PBB</td>
                                        <td class="text-right" id="tarifPbb">
                                            ' . $tarifs . '%
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">PBB Terhutang</td>
                                        <td class="text-right" id="pbbTerhutang">
                                            ' . $sppt->pbb_terhutang_sppt . '
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">Faktor Pengurang</td>
                                        <td class="text-right" id="faktorPengurangan">
                                            ' . $faktorPengurang . '
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">PBB Harus Dibayar</td>
                                        <td class="text-right" id="pbbBayar">
                                            ' . $sppt->pbb_yg_harus_dibayar_sppt . '
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">Denda Telah Dibayar</td>
                                        <td class="text-right" id="dendaDibayar">
                                            ' . $dendaDibayar . '
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">PBB Telah Dibayar</td>
                                        <td class="text-right" id="pbbDibayar">
                                            ' . $pokokDiBayar . '
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Tanggal Jatuh Tempo : &nbsp; <span
                                                id="tglJatuhTempo">' . date('d-m-Y', strtotime($sppt->tgl_jatuh_tempo_sppt)) . '</span></td>
                                        <td colspan="1" style="text-align: end;">Tempat Pembayaran :</td>
                                        <td colspan="2" class="text-left"><span id="tempatPembayaran">' . $tempat[0]->nm_tp . '</span></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-12 col-md-4">
                        <label for="tglTerbit" class="col-form-label">Tanggal Terbit</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="tglTerbit" value="' . date('d-m-Y', strtotime($sppt->tgl_terbit_sppt)) . '" readonly>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-12 col-md-4">
                        <label for="tglCetak" class="col-form-label">Tanggal Cetak</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="tglCetak" value="' . date('d-m-Y', strtotime($sppt->tgl_cetak_sppt)) . '" readonly>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-12 col-md-4">
                        <label for="nipPencetak" class="col-form-label">NIP Pencetak</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="nipPencetak" value="' . $pegawai->nip_baru . '" readonly>
                        </div>
                    </div>
                </div>
        ';
        // dd($sppt);

        // $data = [
        //     'tipe' => $tipe,
        //     'status' => $status,
        //     'data' => [
        //         'sppt' => isset($sppt) ? $sppt : null,
        //         'tempat' => isset($tempat) ? $tempat : null,
        //         'bayar' => isset($dataPembayaran) ? $dataPembayaran : null,
        //         'objekPajak' => isset($datOp) ? $datOp : null,
        //         'opBumi' => isset($opBumi) ? $opBumi : null,
        //         'jpt' => isset($jpt) ? $jpt : null,
        //         'tarif' => isset($tarif) ? $tarif : null,
        //         'pegawai' => isset($pegawai) ? $pegawai : null
        //     ]
        // ];
        // dd($data);
        return response()->json($modalBody);
    }
}
