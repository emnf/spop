<?php

namespace App\Http\Controllers\Validasi;

use App\Helpers\DatagridHelpers;
use App\Http\Controllers\Controller;
use App\Models\Status\StatusValidasiFinal;
use Illuminate\Http\Request;

class ValidasiFinalController extends Controller
{
    public function index()
    {
        return view('validasi-final.index');
    }

    public function datagridBelum(Request $request)
    {
        try {
            $validasiFinal = StatusValidasiFinal;
            $dataArr = [];
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $validasiFinal);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }
}
