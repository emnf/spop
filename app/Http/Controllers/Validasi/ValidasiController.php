<?php

namespace App\Http\Controllers\Validasi;

use App\Helpers\CountHelper;
use App\Helpers\DatagridHelpers;
use App\Helpers\PelayananHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\Pelayanan\PelayananTrait;
use App\Http\Traits\Pelayanan\WpTrait;
use App\Http\Traits\Validasi\ValidasiTrait;
use App\Models\Hasil\HasilVerlap;
use App\Models\Hasil\HasilVerlapOp;
use App\Models\Hasil\HasilVerlapOpBaru;
use App\Models\Pelayanan\Op;
use App\Models\Pbb\Sppt;
use App\Models\Pelayanan\Pelayanan;
use App\Models\Pelayanan\PersyaratanPelayanan;
use App\Models\Pelayanan\Wp;
use App\Models\Setting\JenisPajak;
use App\Models\Setting\JenisPelayanan;
use App\Models\Setting\Pegawai;
use App\Models\Setting\PemdaModel;
use App\Models\User;
use App\Models\Validasi\Validasi;
use App\Models\Verifikasi\VerifikasiLapanagan;
use App\Notifications\ValidasiPermohonanNotify;
use App\Service\NotificationService;
use App\Service\Pbb\OpBumiService;
use App\Service\Pbb\SpopLspopService;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class ValidasiController extends Controller
{
    use ValidasiTrait, PelayananTrait, WpTrait;

    protected $spopLspopService;

    public function __construct(SpopLspopService $spopLspopService)
    {
        $this->spopLspopService = $spopLspopService;
    }

    public function index()
    {
        $param = (new CountHelper)->countPelayanan();
        $param['datapegawai'] = Pegawai::get();
        $param['jenispajak'] = JenisPajak::isActive()->get();
        $param['jenispelayanan'] = JenisPelayanan::isActive()->get();

        return view('validasi.index', $param);
    }

    public function create(Pelayanan $pelayanan)
    {
        if ($pelayanan->validasi) {
            return redirect('validasi');
        }

        $pelayanan->load('jenisPajak')
            ->load('jenisPelayanan')
            ->load('jenisPelayanan')
            ->load('objekPajak')
            ->load('wajibPajak')
            ->load('detailBangunan')
            ->load('verifikasi');

        $tglSelesai = Carbon::createFromFormat('Y-m-d H:i:s', $pelayanan->pelayananAprove->t_tgl_perkiraan_selesai);
        $sisa = Carbon::parse(date('Y-m-d H:i:s'))->diffInDaysFiltered(function (Carbon $date) {
            return $date->isWeekday();
        }, $tglSelesai);
        // dd($pelayanan);
        return view('validasi.tambah', [
            'pelayanan' => $pelayanan,
            'perkiraanSelesai' => PelayananHelper::formatTglPerkiraanSelesaai(date('Y-m-d H:i:s', strtotime($pelayanan->t_tgl_pelayanan)), $sisa),
        ]);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();

        try {
            $pelayanan = Pelayanan::where('t_id_pelayanan', $request->idPelayanan)->first();

            if ($pelayanan->validasi()->exists()) {
                if ($pelayanan->validasi->t_id_status_validasi == 4) {
                    return redirect('validasi')->with('success', 'Data berhasil disimpan');
                }
            }

            $dataValidasi = [
                't_id_pelayanan' => $request->idPelayanan,
                't_id_status_validasi' => $request->idStatusValidasi,
                't_keterangan_validasi' => $request->keterangan,
                'created_by' => auth()->user()->id
            ];

            if (($request->pengajuanVerlap == 2) && ($dataValidasi['t_id_status_validasi'] != 3)) {
                $dataValidasi['t_id_status_validasi'] = 5;
                $dataValidasi['t_tgl_akhir_verlap'] = carbon::now()->addDays(2);
            }

            if (($request->idStatusValidasi == 4) && $request->idJenisPelayanan == 8) {
                $dataValidasi['t_besar_pengurangan'] = $request->pengurangan_disetujui;
            }
            $opbaru = '';
            // dd($pelayanan, $dataValidasi['t_id_status_validasi']);
            if (!in_array($dataValidasi['t_id_status_validasi'], [3, 5])) {

                $pelayanan = Pelayanan::getByPelayananId($request->idPelayanan);
                // dd($pelayanan->t_id_jenis_pelayanan);
                if ($pelayanan->t_id_jenis_pelayanan == 2) {
                    // mutasi penuh
                    // dd("penuh");

                    $this->spopLspopService->prosesMutasiFull($pelayanan);
                }

                if ($pelayanan->t_id_jenis_pelayanan == 1) {
                    // op baru
                    $this->spopLspopService->prosesOpBaru($pelayanan);
                    $opbaru = ' ,Data OP baru otomatis diajukan ke verlap!';
                }

                if ($pelayanan->t_id_jenis_pelayanan == 4) {
                    // pembetualn sppt
                    $this->spopLspopService->prosesPembetulanSppt($pelayanan);
                }

                if ($pelayanan->t_id_jenis_pelayanan == 5) {
                    // Pembatan SPPT
                    $op = Op::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first();

                    if ($op->jns_bumi == 4) {
                        (new OpBumiService())->updateFasum($pelayanan->t_nop, $op);
                    }

                    (new Sppt())->getByNopTahunSppt($pelayanan->t_nop, $pelayanan->t_tahun_pajak)->update([
                        'STATUS_PEMBAYARAN_SPPT' => '2'
                    ]);
                }

                if ($pelayanan->t_id_jenis_pelayanan == 9) {
                    // Penundaan Jatuh Tempo
                    (new Sppt())->getByNopTahunSppt($pelayanan->t_nop, $pelayanan->t_tahun_pajak)->update([
                        'TGL_JATUH_TEMPO_SPPT' => Carbon::parse($pelayanan->t_tgl_permintaan)->format('Y-m-d H:i:s')
                    ]);
                }

                if ($pelayanan->t_id_jenis_pelayanan == 3) {
                    //pecah bidang
                    $this->spopLspopService->prosesMutasiPecah($pelayanan);
                }

                if ($pelayanan->t_id_jenis_pelayanan == 22) {
                    $this->spopLspopService->prosesMutasiGabung($pelayanan);
                }

                if ($pelayanan->t_id_jenis_pelayanan == 8) {
                    // pengurangan
                    $sppt = (new Sppt())->getByNopTahunSppt($pelayanan->t_nop, $pelayanan->t_tahun_pajak)->first();
                    $pajakTerhutang = (int) $sppt->pbb_terhutang_sppt;
                    $faktorPengurang = $pajakTerhutang * ($pelayanan->t_besar_pengurangan / 100);
                    $pbbYgHarusDibayar = $pajakTerhutang - $faktorPengurang;

                    $updateSppt = [
                        'FAKTOR_PENGURANG_SPPT' => (int) $faktorPengurang,
                        'PBB_YG_HARUS_DIBAYAR_SPPT' => (int) $pbbYgHarusDibayar,
                    ];
                    (new Sppt())->getByNopTahunSppt($pelayanan->t_nop, $pelayanan->t_tahun_pajak)->update($updateSppt);
                }
            }

            if (is_null($request->idValidasi)) {
                $dataValidasi['t_tgl_validasi'] = Carbon::now();
                $dataValidasi['t_no_validasi'] = Validasi::getMaxNoUrut() + 1;
            }

            $validasi = Validasi::updateOrCreate(
                ['t_id_validasi' => $request->idValidasi],
                $dataValidasi
            );

            $notifikasi = new NotificationService;

            if ($dataValidasi['t_id_status_validasi'] == 5) {
                $notifikasi->notifyValidasiVerlap($validasi->pelayanan);
            } else {
                $notifikasi->notifyValidasi($validasi->pelayanan);
            }
            DB::commit();
            // dd("asd", $pelayanan);
            // dd("asdu2");

            return redirect('validasi')->with('success', 'Data berhasil disimpan' . $opbaru);
        } catch (\Throwable $th) {
            dd($th);
            report($th);
            DB::rollBack();
            return redirect()->back()->with('error', 'Gagal saat menyimpan data');
        }
    }

    public function edit(Pelayanan $pelayanan)
    {
        $pelayanan->load('jenisPajak')
            ->load('jenisPelayanan')
            ->load('jenisPelayanan')
            ->load('objekPajak')
            ->load('wajibPajak')
            ->load('detailBangunan');

        $tglSelesai = Carbon::createFromFormat('Y-m-d H:i:s', $pelayanan->pelayananAprove->t_tgl_perkiraan_selesai);
        $sisa = Carbon::parse(date('Y-m-d H:i:s'))->diffInDaysFiltered(function (Carbon $date) {
            return $date->isWeekday();
        }, $tglSelesai);

        return view('validasi.edit', [
            'pelayanan' => $pelayanan,
            'perkiraanSelesai' => PelayananHelper::formatTglPerkiraanSelesaai(date('Y-m-d H:i:s', strtotime($pelayanan->t_tgl_pelayanan)), $sisa),
            'validasi' => Validasi::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first(),
            'verifikasiLapangan' => VerifikasiLapanagan::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first(),
            'hasilVerlap' => HasilVerlap::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first(),
            'hasilVerlapOPBaru' => HasilVerlapOpBaru::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first()
        ]);
    }

    public function datagridBelumProses(Request $request)
    {
        try {
            $validasi = $this->gridBelumProses($request->filter, $request->pagination, $request->sorting);
            $dataArr = [];

            foreach ($validasi as $v) {
                // dd($this->getNamaWp($v));
                $dataArr[] = [
                    'jenisPajak' => $v->s_nama_jenis_pajak,
                    'jenisPelayanan' => $v->s_nama_jenis_pelayanan,
                    'tglPermohonan' => date('d-m-Y', strtotime($v->t_tgl_verifikasi)),
                    'noPermohonan' => PelayananHelper::formatNoPelayanan(date('Y', strtotime($v->t_tgl_pelayanan)), $v->t_id_jenis_pajak, $v->t_no_pelayanan),
                    'nikPemohon' => $v->t_nik_pemohon,
                    'namaPemohon' => $v->t_nama_pemohon,
                    'namaWp' => $this->getNamaWp($v),
                    'actionList' => array(
                        [
                            'actionName' => 'input-validasi',
                            'actionUrl' => "validasi/tambah/" . $v->t_id_pelayanan,
                            'actionTitle' => 'Edit Pelayanan',
                            'actionActive' => true
                        ]
                    )
                ];
            }

            return (new DatagridHelpers)->getSuccessResponse($dataArr, $validasi);
        } catch (\Throwable $th) {
            dd($th);
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }

    public function datagridDiajukanVerlap(Request $request)
    {
        try {
            $validasi = $this->gridValidasi($request->filter, $request->pagination, $request->sorting, 5);
            $dataArr = [];

            foreach ($validasi as $v) {
                $dataArr[] = [
                    'jenisPajak' => $v->s_nama_jenis_pajak,
                    'jenisPelayanan' => $v->s_nama_jenis_pelayanan,
                    'tglPermohonan' => date('d-m-Y', strtotime($v->t_tgl_pelayanan)),
                    'noPermohonan' => PelayananHelper::formatNoPelayanan(date('Y', strtotime($v->t_tgl_pelayanan)), $v->t_id_jenis_pajak, $v->t_no_pelayanan),
                    'tglVerifikasiLapangan' => date('d-m-Y', strtotime($v->t_tgl_validasi)),
                    'namaPemohon' => $v->t_nama_pemohon,
                    'namaWp' => $this->getNamaWp($v),
                    'actionList' => array(
                        [
                            'actionName' => 'batal-verlap',
                            'actionUrl' => "javascript:showBatalVerlap('" .  $v->uuid . "')",
                            'actionTitle' => 'Edit Pelayanan',
                            'actionActive' => auth()->user()->hasRole(['Admin', 'Super Admin'])
                        ]
                    )
                ];
            }

            return (new DatagridHelpers)->getSuccessResponse($dataArr, $validasi);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }

    public function datagridSudahVerlap(Request $request)
    {
        try {
            $validasi = $this->gridValidasi($request->filter, $request->pagination, $request->sorting, 6);
            $dataArr = [];

            foreach ($validasi as $v) {
                $dataArr[] = [
                    'jenisPajak' => $v->s_nama_jenis_pajak,
                    'jenisPelayanan' => $v->s_nama_jenis_pelayanan,
                    'tglPermohonan' => date('d-m-Y', strtotime($v->t_tgl_pelayanan)),
                    'noPermohonan' => PelayananHelper::formatNoPelayanan(date('Y', strtotime($v->t_tgl_pelayanan)), $v->t_id_jenis_pajak, $v->t_no_pelayanan),
                    'tglVerifikasiLapangan' => $v->t_tgl_verifikasi_lapangan,
                    'nikPemohon' => $v->t_nik_pemohon,
                    'namaPemohon' => $v->t_nama_pemohon,
                    'actionList' => array(
                        [
                            'actionName' => 'edit-validasi',
                            'actionUrl' => "validasi/edit/" . $v->t_id_pelayanan,
                            'actionTitle' => 'Edit Pelayanan',
                            'actionActive' => true
                        ]
                    )
                ];
            }

            return (new DatagridHelpers)->getSuccessResponse($dataArr, $validasi);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }

    public function datagridDiterima(Request $request)
    {
        try {
            $validasi = $this->gridValidasi($request->filter, $request->pagination, $request->sorting, 4);
            $dataArr = [];

            foreach ($validasi as $v) {
                $dataArr[] = [
                    'jenisPajak' => $v->s_nama_jenis_pajak,
                    'jenisPelayanan' => $v->s_nama_jenis_pelayanan,
                    'tglPermohonan' => date('d-m-Y', strtotime($v->t_tgl_pelayanan)),
                    'noPermohonan' => PelayananHelper::formatNoPelayanan(date('Y', strtotime($v->t_tgl_pelayanan)), $v->t_id_jenis_pajak, $v->t_no_pelayanan),
                    'tglValidasi' => $v->t_tgl_validasi,
                    'nikPemohon' => $v->t_nik_pemohon,
                    'namaPemohon' => $v->t_nama_pemohon,
                    'namaWp' => $this->getNamaWp($v),
                    'actionList' => array(
                        [
                            'actionName' => 'hasil-validasi',
                            'actionUrl' => "javascript:printHasilValidasi(" . $v->t_id_pelayanan . ")",
                            'actionTitle' => 'Cetak Hasil Validasi',
                            'actionActive' => true
                        ]
                    )
                ];
            }

            return (new DatagridHelpers)->getSuccessResponse($dataArr, $validasi);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }

    public function datagridDitolak(Request $request)
    {
        try {
            $validasi = $this->gridValidasi($request->filter, $request->pagination, $request->sorting, 3);
            $dataArr = [];

            foreach ($validasi as $v) {
                $dataArr[] = [
                    'jenisPajak' => $v->s_nama_jenis_pajak,
                    'jenisPelayanan' => $v->s_nama_jenis_pelayanan,
                    'tglPermohonan' => date('d-m-Y', strtotime($v->t_tgl_pelayanan)),
                    'noPermohonan' => PelayananHelper::formatNoPelayanan(date('Y', strtotime($v->t_tgl_pelayanan)), $v->t_id_jenis_pajak, $v->t_no_pelayanan),
                    'tglValidasi' => $v->t_tgl_validasi,
                    'nikPemohon' => $v->t_nik_pemohon,
                    'keterangan' => $v->t_keterangan_validasi,
                    'actionList' => array(
                        [
                            'actionName' => 'hasil-validasi',
                            'actionUrl' => "javascript:printHasilValidasi(" . $v->t_id_pelayanan . ")",
                            'actionTitle' => 'Cetak Hasil Validasi',
                            'actionActive' => true
                        ],
                        [
                            'actionName' => 'batal-ditolak',
                            'actionUrl' => "javascript:showBatalDitolak('" .  $v->uuid . "')",
                            'actionTitle' => 'Edit Pelayanan',
                            'actionActive' => auth()->user()->hasRole(['Admin', 'Super Admin'])
                        ]
                    )
                ];
            }

            return (new DatagridHelpers)->getSuccessResponse($dataArr, $validasi);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }

    public function datagridValidasi(Request $request, string $status)
    {
        try {
            $validasi = $this->gridValidasi($request->filter, $request->pagination, $request->sorting, $status);
            $dataArr = [];

            foreach ($validasi as $v) {
                $dataArr[] = [
                    'jenisPajak' => $v->s_nama_jenis_pajak,
                    'jenisPelayanan' => $v->s_nama_jenis_pelayanan,
                    'tglPermohonan' => date('d-m-Y', strtotime($v->t_tgl_verifikasi)),
                    'noPermohonan' => PelayananHelper::formatNoPelayanan(date('Y', strtotime($v->t_tgl_pelayanan)), $v->t_id_jenis_pajak, $v->t_no_pelayanan),
                    'nikPemohon' => $v->t_nik_pemohon,
                    'namaPemohon' => $v->t_nama_pemohon,
                    'actionList' => array(
                        [
                            'actionName' => 'input-validasi',
                            'actionUrl' => "validasi/tambah/" . $v->t_id_pelayanan,
                            'actionTitle' => 'Edit Pelayanan',
                            'actionActive' => true
                        ]
                    )
                ];
            }

            return (new DatagridHelpers)->getSuccessResponse($dataArr, $validasi);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }

    public function datagrid(Request $request)
    {
        try {
            $validasi = $this->gridValidasi($request->filter, $request->pagination, $request->sorting);
            $dataArr = [];

            foreach ($validasi as $v) {
                $dataArr[] = [
                    'jenisPajak' => $v->s_nama_jenis_pajak,
                    'jenisPelayanan' => $v->s_nama_jenis_pelayanan,
                    'tglPermohonan' => date('d-m-Y', strtotime($v->t_tgl_verifikasi)),
                    'noPermohonan' => PelayananHelper::formatNoPelayanan(date('Y', strtotime($v->t_tgl_pelayanan)), $v->t_id_jenis_pajak, $v->t_no_pelayanan),
                    'tglValidasi' => $v->t_tgl_validasi,
                    'nikPemohon' => $v->t_nama_pemohon,
                    'namaWp' => $this->getNamaWp($v),
                    'actionList' => array(
                        [
                            'actionName' => 'status-pending',
                            'actionUrl' => "#",
                            'actionTitle' => $v->s_nama_status_validasi,
                            'actionActive' => $v->t_id_status_validasi == 2 ? true : false
                        ],
                        [
                            'actionName' => 'status-ditolak',
                            'actionUrl' => "#",
                            'actionTitle' => $v->s_nama_status_validasi,
                            'actionActive' => $v->t_id_status_validasi == 3 ? true : false
                        ],
                        [
                            'actionName' => 'status-diterima',
                            'actionUrl' => "#",
                            'actionTitle' => $v->s_nama_status_validasi,
                            'actionActive' => $v->t_id_status_validasi == 4 ? true : false
                        ],
                        [
                            'actionName' => 'status-diajukan-verlap',
                            'actionUrl' => "#",
                            'actionTitle' => $v->s_nama_status_validasi,
                            'actionActive' => $v->t_id_status_validasi == 5 ? true : false
                        ],
                        [
                            'actionName' => 'status-selesai-verlap',
                            'actionUrl' => "#",
                            'actionTitle' => $v->s_nama_status_validasi,
                            'actionActive' => $v->t_id_status_validasi == 6 ? true : false
                        ]
                    )
                ];
            }

            return (new DatagridHelpers)->getSuccessResponse($dataArr, $validasi);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }

    public function getUploadFilePersyaratan(Pelayanan $pelayanan)
    {
        return response()->json(PersyaratanPelayanan::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->get(), 200);
    }

    public function cetakHasilValidasi(Pelayanan $pelayanan, Pegawai $ttd)
    {
        if ($pelayanan->t_id_pelayanan == 6) {
            $objekArray = Op::selectRaw(
                "t_op.*,
                t_detail_bangunan.t_id_detail_bangunan,
                t_detail_bangunan.t_jenis_penggunaan_bangunan,
                t_detail_bangunan.t_tahun_bangunan,
                t_detail_bangunan.t_tahun_renovasi,
                t_detail_bangunan.t_kondisi_bangunan,
                t_detail_bangunan.t_konstruksi,
                t_detail_bangunan.t_atap,
                t_detail_bangunan.t_dinding,
                t_detail_bangunan.t_lantai,
                t_detail_bangunan.t_langit_langit,
                t_detail_bangunan.t_ac_split,
                t_detail_bangunan.t_ac_window,
                t_detail_bangunan.t_panjang_pagar,
                t_detail_bangunan.t_bahan_pagar,
                t_detail_bangunan.t_jumlah_lantai,
                t_detail_bangunan.t_listrik,
                t_detail_bangunan.t_luas,
                t_detail_bangunan.t_no_urut_bangunan,
                t_detail_bangunan.created_by,
                t_detail_bangunan.created_at,
                t_detail_bangunan.updated_at,
                t_detail_bangunan.t_id_op"
            )
                ->Where('t_op.t_id_pelayanan', $pelayanan->t_id_pelayanan)->leftJoin('t_detail_bangunan', 't_op.t_id_pelayanan', '=', 't_detail_bangunan.t_id_pelayanan')->get();
        } else {
            $objekArray = null;
        }

        $wp = Wp::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->get();

        $arrWPOp = []; // Initialize the array before the loop
        $wpop = []; // Initialize $wpop array

        foreach ($wp as $key => $v) {
            $op = Op::where('t_id_op', $v->t_id_op)->first();
            $wpop['wpnya'] = $v;
            $wpop['opnya'] = $op;
            if ($op) {
                $wpop['opnya']['nopnya'] =
                    $op->kd_propinsi . '.' .
                    $op->kd_dati2 . '.' .
                    $op->kd_kecamatan . '.' .
                    $op->kd_kelurahan . '.' .
                    $op->kd_blok . '.' .
                    $op->no_urut . '.' .
                    $op->kd_jns_op;
            }
            $arrWPOp[] = $wpop;
        }
        // dd($arrWPOp);
        // exit;
        $opnyaCount = 0;
        foreach ($arrWPOp as $item) {
            if (isset($item['opnya'])) {
                $opnyaCount++;
            }
        }

        $pdf = Pdf::setOption(['dpi' => 150, 'defaultFont' => 'Arial'])
            ->setPaper('legal', 'potrait')
            ->loadView('validasi.exports.hasil-validasi', [
                'pemda' => PemdaModel::first(),
                'pelayanan' => $pelayanan->load('jenisPajak')
                    ->load('jenisPelayanan')
                    ->load('wajibPajak')
                    ->load('objekPajak')
                    ->load('pelayananAprove'),
                'validasi' => Validasi::with('statusValidasi')->where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first(),
                'pejabat' => $ttd,
                'tte' => 0,
                'objekArray' => $objekArray,
                'arrWpOp' => $arrWPOp,
                'opnyaCount' => $opnyaCount

            ]);

        return $pdf->stream(Uuid::uuid4() . 'pdf');
    }

    public function getDataPelayanan(Pelayanan $pelayanan)
    {
        return response()->json([
            'id' => $pelayanan->uuid,
            'namaPemohon' => $pelayanan->t_nama_pemohon,
            'noPelayanan' => PelayananHelper::formatNoPelayanan(
                date('Y', strtotime($pelayanan->t_tgl_pelayanan)),
                $pelayanan->t_id_jenis_pajak,
                $pelayanan->t_no_pelayanan
            )
        ], 200);
    }

    public function batalDitolakDanPengajuanVerlap(Request $request)
    {
        try {
            $pelayanan = Pelayanan::whereUuid($request->pelayanan)->first();
            Validasi::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->delete();

            return response()->json(['success' => true, 'message' => 'Berhasil di batalkan'], 200);
        } catch (\Throwable $th) {
            report($th);
            return response()->json(['success' => false, 'message' => 'Gagal di batalkan'], 200);
        }
    }
}
