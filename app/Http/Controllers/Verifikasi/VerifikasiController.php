<?php

namespace App\Http\Controllers\Verifikasi;

use App\Exports\VerifikasiExport;
use App\Helpers\CountHelper;
use App\Helpers\DatagridHelpers;
use App\Helpers\PelayananHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\Pelayanan\WpTrait;
use App\Http\Traits\Verifikasi\VerifikasiTrait;
use App\Models\Pelayanan\Pelayanan;
use App\Models\Pelayanan\PersyaratanPelayanan;
use App\Models\Setting\JenisPajak;
use App\Models\Setting\JenisPelayanan;
use App\Models\Setting\Pegawai;
use App\Models\Setting\PemdaModel;
use App\Models\Setting\Persyaratan;
use App\Models\Status\StatusVerifikasi;
use App\Models\User;
use App\Models\Verifikasi\Verifikasi;
use App\Notifications\VerifikasiPermohonanNotify;
use App\Service\NotificationService;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Ramsey\Uuid\Uuid;

class VerifikasiController extends Controller
{
    use VerifikasiTrait, WpTrait;

    public function index()
    {
        $param = (new CountHelper)->countPelayanan();
        $param['jenispajak'] = JenisPajak::isActive()->get();
        $param['jenispelayanan'] = JenisPelayanan::isActive()->get();
        $param['datapegawai'] = Pegawai::get();
        $param['statusVerifikasi'] = StatusVerifikasi::get();

        return view('verifikasi.index', $param);
    }

    public function datagridBelumProses(Request $request)
    {
        try {
            $pelayanaAprove = $this->dataBelumProses($request->filter, $request->pagination, $request->sorting);
            $dataArr = [];

            foreach ($pelayanaAprove as $v) {
                $wajibPajak = $this->getNamaWp($v);

                $dataArr[] = [
                    'jenis_pajak' => $v->s_nama_jenis_pajak,
                    'jenis_pelayanan' => $v->s_nama_jenis_pelayanan,
                    'tgl_permohonan' => $v->t_tgl_pelayanan,
                    'no_permohonan' => PelayananHelper::formatNoPelayanan(date('Y', strtotime($v->t_tgl_pelayanan)), $v->t_id_jenis_pajak, $v->t_no_pelayanan),
                    'nama_pemohon' => $v->t_nama_pemohon,
                    'nama_wp' => $wajibPajak,
                    'actionList' => [
                        [
                            'actionName' => 'tambah-verifikasibelum',
                            'actionUrl' => "verifikasi/tambah/" . $v->uuid,
                            'actionTitle' => 'Edit Pelayanan',
                            'actionActive' => true
                        ]
                    ]
                ];
            }

            return (new DatagridHelpers)->getSuccessResponse($dataArr, $pelayanaAprove);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }

    public function datagirdPerbaikan(Request $request)
    {
        try {
            $dataPerbiakan = $this->datagridVerifikasi(
                $request->filter,
                $request->pagination,
                $request->sorting,
                2
            );
            $dataArr = [];
            foreach ($dataPerbiakan as $v) {
                $wajibPajak = $this->getNamaWp($v);

                $dataArr[] = [
                    'jenis_pajak' => $v->s_nama_jenis_pajak,
                    'jenis_pelayanan' => $v->s_nama_jenis_pelayanan,
                    'tgl_permohonan' => date('d-m-Y', strtotime($v->t_tgl_pelayanan)),
                    'no_permohonan' => PelayananHelper::formatNoPelayanan(date('Y'), $v->t_id_jenis_pajak, $v->t_no_pelayanan),
                    'nama_pemohon' => $v->t_nama_pemohon,
                    'nama_wp' => $wajibPajak,
                    'tgl_verifikasi' => date('d-m-Y', strtotime($v->t_tgl_verifikasi)),
                    'keterangan' => $v->t_keterangan_verifikasi,
                    'actionList' => [
                        [
                            'actionName' => 'edit-verifikasi-pending',
                            'actionUrl' => "verifikasi/edit/" . $v->uuid,
                            'actionTitle' => 'Verifikasi Ulang',
                            'actionActive' => true
                        ]
                    ]
                ];
            }
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $dataPerbiakan);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }

    public function datagirdDitolak(Request $request)
    {
        try {
            $dataPerbiakan = $this->datagridVerifikasi(
                $request->filter,
                $request->pagination,
                $request->sorting,
                3
            );
            $dataArr = [];

            foreach ($dataPerbiakan as $v) {
                $wajibPajak = $this->getNamaWp($v);

                $dataArr[] = [
                    'jenis_pajak' => $v->s_nama_jenis_pajak,
                    'jenis_pelayanan' => $v->s_nama_jenis_pelayanan,
                    'tgl_permohonan' => date('d-m-Y', strtotime($v->t_tgl_pelayanan)),
                    'no_permohonan' => PelayananHelper::formatNoPelayanan(date('Y'), $v->t_id_jenis_pajak, $v->t_no_pelayanan),
                    'nama_pemohon' => $v->t_nama_pemohon,
                    'nama_wp' => $wajibPajak,
                    'tgl_verifikasi' => date('d-m-Y', strtotime($v->t_tgl_verifikasi)),
                    'keterangan' => $v->t_keterangan_verifikasi,
                ];
            }
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $dataPerbiakan);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }

    public function datagirdDiterima(Request $request)
    {
        try {
            $dataPerbiakan = $this->datagridVerifikasi(
                $request->filter,
                $request->pagination,
                $request->sorting,
                4
            );
            $dataArr = [];

            foreach ($dataPerbiakan as $v) {
                $wajibPajak = $this->getNamaWp($v);

                $dataArr[] = [
                    'jenis_pajak' => $v->s_nama_jenis_pajak,
                    'jenis_pelayanan' => $v->s_nama_jenis_pelayanan,
                    'tgl_permohonan' => date('d-m-Y', strtotime($v->t_tgl_pelayanan)),
                    'no_permohonan' => PelayananHelper::formatNoPelayanan(date('Y'), $v->t_id_jenis_pajak, $v->t_no_pelayanan),
                    'nama_pemohon' => $v->t_nama_pemohon,
                    'nama_wp' => $wajibPajak,
                    'tgl_verifikasi' => date('d-m-Y', strtotime($v->t_tgl_verifikasi)),
                    'keterangan' => $v->t_keterangan_verifikasi,
                    'actionList' => [
                        [
                            'actionName' => 'tambah-pengajuan',
                            'actionUrl' => "verifikasi/pengajuan-validator/" . $v->uuid,
                            'actionTitle' => 'Pengajuan Validasi',
                            'actionActive' => true
                        ]
                    ]
                ];
            }
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $dataPerbiakan);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }

    public function datagirdDiajukanValidasi(Request $request)
    {
        try {
            $dataPerbiakan = $this->datagridVerifikasi(
                $request->filter,
                $request->pagination,
                $request->sorting,
                5
            );
            $dataArr = [];

            foreach ($dataPerbiakan as $v) {
                $wajibPajak = $this->getNamaWp($v);

                $dataArr[] = [
                    'jenis_pajak' => $v->s_nama_jenis_pajak,
                    'jenis_pelayanan' => $v->s_nama_jenis_pelayanan,
                    'tgl_permohonan' => date('d-m-Y', strtotime($v->t_tgl_pelayanan)),
                    'no_permohonan' => PelayananHelper::formatNoPelayanan(date('Y'), $v->t_id_jenis_pajak, $v->t_no_pelayanan),
                    'nama_pemohon' => $v->t_nama_pemohon,
                    'nama_wp' => $wajibPajak,
                    'tgl_verifikasi' => date('d-m-Y', strtotime($v->t_tgl_verifikasi)),
                    'keterangan' => $v->t_keterangan_verifikasi,
                ];
            }
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $dataPerbiakan);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }

    public function datagirdSemuaData(Request $request)
    {
        try {
            $dataPerbiakan = $this->datagridVerifikasi(
                $request->filter,
                $request->pagination,
                $request->sorting
            );
            $dataArr = [];

            foreach ($dataPerbiakan as $v) {
                $wajibPajak = $this->getNamaWp($v);

                $dataArr[] = [
                    'jenis_pajak' => $v->s_nama_jenis_pajak,
                    'jenis_pelayanan' => $v->s_nama_jenis_pelayanan,
                    'tgl_permohonan' => date('d-m-Y', strtotime($v->t_tgl_pelayanan)),
                    'no_permohonan' => PelayananHelper::formatNoPelayanan(date('Y'), $v->t_id_jenis_pajak, $v->t_no_pelayanan),
                    'nama_pemohon' => $v->t_nama_pemohon,
                    'nama_wp' => $wajibPajak,
                    'status_pelayana' => is_null($v->s_nama_status_verifikasi) ? '-' : $v->s_nama_status_verifikasi,
                    'tgl_verifikasi' => is_null($v->t_tgl_verifikasi) ? '-' : date('d-m-Y', strtotime($v->t_tgl_verifikasi)),
                    'keterangan' => is_null($v->t_keterangan_verifikasi) ? '-' : $v->t_keterangan_verifikasi,
                    'actionList' => [
                        [
                            'actionName' => 'edit-verifikasi-pending',
                            'actionUrl' => "verifikasi/edit/" . $v->uuid,
                            'actionTitle' => 'Verifikasi Ulang',
                            'actionActive' => $v->t_id_status_verifikasi == 2 ? true : false
                        ],
                        [
                            'actionName' => 'tambah-verifikasibelum',
                            'actionUrl' => "verifikasi/tambah/" . $v->uuid,
                            'actionTitle' => 'Edit Pelayanan',
                            'actionActive' => is_null($v->t_id_status_verifikasi) ? true : false
                        ],
                        [
                            'actionName' => 'tambah-pengajuan',
                            'actionUrl' => "verifikasi/pengajuan-validator/" . $v->uuid,
                            'actionTitle' => 'Pengajuan Validasi',
                            'actionActive' => $v->t_id_status_verifikasi == 4 ? true : false
                        ]
                    ]
                ];
            }
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $dataPerbiakan);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }

    public function create(Pelayanan $pelayanan)
    {
        $pelayanan->load('objekPajak')
            ->load('OpLama')
            ->load('jenisPajak')
            ->load('jenisPelayanan')
            ->load('detailBangunan')
            ->load('pelayananAprove');

        if ($pelayanan->t_id_jenis_pelayanan == 5) {
            $pelayanan->objekPajak = $pelayanan->OpLama;
        }

        $tglSelesai = Carbon::createFromFormat('Y-m-d H:i:s', $pelayanan->pelayananAprove->t_tgl_perkiraan_selesai);
        $sisa = Carbon::parse(date('Y-m-d H:i:s'))->diffInDaysFiltered(function (Carbon $date) {
            return $date->isWeekday();
        }, $tglSelesai);

        return view('verifikasi.tambah', [
            'pelayanan' => $pelayanan,
            'perkiraanSelesai' => PelayananHelper::formatTglPerkiraanSelesaai(date('Y-m-d H:i:s'), $sisa, $tglSelesai),
            'verifikasi' => new Verifikasi
        ]);
    }

    public function save(Request $request)
    {
        try {
            $pelayanan = Pelayanan::where('t_id_pelayanan', $request->t_id_pelayanan)->first();
            if (!$pelayanan->verifikasi()->exists()) {
                $persyaratanDiupload = PersyaratanPelayanan::join('s_persyaratan', 't_persyaratan.s_id_persyaratan', '=', 's_persyaratan.s_id_persyaratan')->where('s_persyaratan.s_is_optional', false)->where('t_id_pelayanan', $request->t_id_pelayanan)->pluck('t_id_persyaratan');
                $cekSyarat = is_null($request->checklistPersyaratan)
                    ? $persyaratanDiupload->toArray()
                    : array_diff($persyaratanDiupload->toArray(), $request->checklistPersyaratan);

                $checklist_persyaratan = is_null($request->checklistPersyaratan) ? '-' : implode(",", $request->checklistPersyaratan);
                $noUrut = Verifikasi::maxNoUrut() + 1;

                $insertData = [
                    't_id_pelayanan' => $request->t_id_pelayanan,
                    't_tgl_verifikasi' => date('Y-m-d', strtotime($request->t_tgl_verifikasi)),
                    't_no_verifikasi' => $noUrut,
                    't_id_status_verifikasi' => (count($cekSyarat) > 0 || $request->t_id_status_verifikasi == 3) ? $request->t_id_status_verifikasi : 4,
                    // 't_id_status_verifikasi' => (count($cekSyarat) > 0) ? 3 : 4,
                    't_keterangan_verifikasi' => $request->t_keterangan_verifikasi,
                    'created_by' => auth()->user()->id,
                    't_checklist_persyaratan' => $checklist_persyaratan,
                    't_kejelasan' => (int) $request->t_kejelasan,
                    't_kebenaran' => (int) $request->t_kebenaran,
                    't_keabsahan' => (int) $request->t_keabsahan,
                ];
                // dd($insertData);

                $verifikasi = Verifikasi::create($insertData);

                if ($verifikasi) {
                    session()->flash('success', 'Data berhasil disimpan!');
                } else {
                    session()->flash('error', 'Data gagal disimpan!');
                }

                if ($verifikasi->t_id_status_verifikasi == 4) {
                    $uuidPleayanan = Pelayanan::where('t_id_pelayanan', $verifikasi->t_id_pelayanan)->pluck('uuid')->first();
                    return redirect('verifikasi/pengajuan-validator/' . $uuidPleayanan);
                }
            } else {
                session()->flash('success', 'Data berhasil disimpan, silahkan ajukan ke validator!');
            }

            return redirect('verifikasi');
        } catch (\Throwable $th) {
            report($th);
            session()->flash('error', 'Data gagal disimpan!');
            return redirect('verifikasi');
        }
    }

    public function edit(Pelayanan $pelayanan)
    {
        $pelayanan = $pelayanan->load('objekPajak')
            ->load('jenisPajak')
            ->load('jenisPelayanan')
            ->load('pelayananAprove');

        $tglSelesai = Carbon::createFromFormat('Y-m-d H:i:s', $pelayanan->pelayananAprove->t_tgl_perkiraan_selesai);
        $sisa = Carbon::parse(date('Y-m-d H:i:s'))->diffInDaysFiltered(function (Carbon $date) {
            return $date->isWeekday();
        }, $tglSelesai);

        session()->flash('success', 'Data berhasil disimpan!');

        return view('verifikasi.edit', [
            'pelayanan' => $pelayanan,
            'perkiraanSelesai' => PelayananHelper::formatTglPerkiraanSelesaai(date('Y-m-d H:i:s', strtotime($pelayanan->t_tgl_pelayanan)), $sisa),
            'verifikasi' => Verifikasi::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first()
        ]);
    }

    public function update(Request $request)
    {
        try {
            $persyaratanDiupload = PersyaratanPelayanan::where('t_id_pelayanan', $request->t_id_pelayanan)->pluck('t_id_persyaratan');
            $cekSyarat = array_diff($persyaratanDiupload->toArray(), $request->checklistPersyaratan);
            $checklist_persyaratan = implode(",", $request->checklistPersyaratan);

            $updateVerif = [
                't_id_status_verifikasi' => (count($cekSyarat) > 0) ? $request->t_id_status_verifikasi : 4,
                't_keterangan_verifikasi' => $request->t_keterangan_verifikasi,
                'created_by' => auth()->user()->id,
                't_checklist_persyaratan' => $checklist_persyaratan,
            ];

            Verifikasi::where('t_id_verifikasi', $request->t_id_verifikasi)->update($updateVerif);

            session()->flash('success', 'Data gagal disimpan!');
            return redirect('verifikasi');
        } catch (\Throwable $th) {
            report($th);
            session()->flash('error', 'Data gagal disimpan!');
            return redirect('verifikasi');
        }
    }

    public function pengajuanValidator(Pelayanan $pelayanan)
    {
        $pelayanan = $pelayanan->load('objekPajak')
            ->load('jenisPajak')
            ->load('jenisPelayanan')
            ->load('pelayananAprove')
            ->load('detailBangunan')
            ->load('verifikasi');

        $tglSelesai = Carbon::createFromFormat('Y-m-d H:i:s', $pelayanan->pelayananAprove->t_tgl_perkiraan_selesai);
        $sisa = Carbon::parse(date('Y-m-d H:i:s'))->diffInDaysFiltered(function (Carbon $date) {
            return $date->isWeekday();
        }, $tglSelesai);

        return view('verifikasi.pengajuan-validator', [
            'pelayanan' => $pelayanan,
            'jenisLayanan' => $pelayanan->jenisPelayanan,
            'jenisPajak' => $pelayanan->jenisPajak,
            // 'objekPajak' => $pelayanan->objekPajak,
            'perkiraanSelesai' => PelayananHelper::formatTglPerkiraanSelesaai(date('Y-m-d H:i:s', strtotime($pelayanan->t_tgl_pelayanan)), $sisa),
        ]);
    }

    public function ajukan(Request $request)
    {
        try {
            Verifikasi::where('t_id_verifikasi', $request->t_id_verifikasi)->update([
                't_id_status_verifikasi' => $request->t_id_status_verifikasi
            ]);

            $verifikasi = Verifikasi::where('t_id_verifikasi', $request->t_id_verifikasi)->with('pelayanan')->first();
            $notifikasi = new NotificationService;
            $notifikasi->notifyVerifikasi($verifikasi->pelayanan);


            session()->flash('success', 'Data berhasil diajukan ke validator');
            return redirect('verifikasi');
        } catch (\Throwable $th) {
            report($th);
            session()->flash('error', 'Terjadi kesalahan');
            return redirect('verifikasi');
        }
    }

    public function checkedUpload(Pelayanan $pelayanan, Request $request)
    {
        $pelayanan->load('verifikasi');
        $checkArr = explode(',', $pelayanan->verifikasi->t_checklist_persyaratan);

        return response()->json(['status' => true, 'data' => $checkArr], 200);
    }

    public function cetakVerifikasiBelumProses(Request $request)
    {
        $pelayanan = Pelayanan::when($request->tglPelayanan, function ($q) use ($request) {
            $tglPelayanan = explode(' - ', $request->tglPelayanan);

            return $q->whereBetween('t_pelayanan.t_tgl_pelayanan', [
                date('Y-m-d', strtotime($tglPelayanan[0])),
                date('Y-m-d', strtotime($tglPelayanan[1]))
            ]);
        })
            ->when($request->idJenisPelayanan, function ($q) use ($request) {
                $q->where('t_pelayanan.t_id_jenis_pelayanan', $request->idJenisPelayanan);
            })
            ->when($request->namaPemohon, function ($q) use ($request) {
                $q->where('t_pelayanan.t_nama_pemohon', $request->namaPemohon);
            })
            ->when($request->noPelayanan, function ($q) use ($request) {
                $q->where('t_pelayanan.t_no_pelayanan', 'like', "%{{$request->noPelayanan}}%");
            })
            ->has('pelayananAprove')
            ->doesnthave('verifikasi')
            ->orderBy('t_tgl_pelayanan')->get();

        $viewName = 'verifikasi.exports.pelayanan-belum-verifikasi';
        $pegawai = Pegawai::find($request->ttd);

        if ($request->format == 'XLS') {
            return Excel::download(new VerifikasiExport($pelayanan, $viewName, $request->tglcetak, $pegawai), Uuid::uuid4() . '.xlsx');
        }

        return Pdf::setOption(['dpi' => 150, 'defaultFont' => 'Arial'])
            ->setPaper('a4', 'landscape')
            ->loadView($viewName, [
                'pelayanan' => $pelayanan,
                'tglCetak' => $request->tglcetak,
                'pegawai' => $pegawai,
                'pemda' => PemdaModel::first(),
                'noImage' => false
            ])
            ->download(Uuid::uuid4() . '.pdf');
    }

    public function cetakVerifikasiDitolak(Request $request)
    {
        $pelayanan = Pelayanan::when($request->tglPelayanan, function ($q) use ($request) {
            $tglPelayanan = explode(' - ', $request->tglPelayanan);

            return $q->whereBetween('t_pelayanan.t_tgl_pelayanan', [
                date('Y-m-d', strtotime($tglPelayanan[0])),
                date('Y-m-d', strtotime($tglPelayanan[1]))
            ]);
        })
            ->when($request->idJenisPelayanan, function ($q) use ($request) {
                $q->where('t_pelayanan.t_id_jenis_pelayanan', $request->idJenisPelayanan);
            })
            ->when($request->namaPemohon, function ($q) use ($request) {
                $q->where('t_pelayanan.t_nama_pemohon', $request->namaPemohon);
            })
            ->when($request->noPelayanan, function ($q) use ($request) {
                $q->where('t_pelayanan.t_no_pelayanan', 'like', "%{{$request->noPelayanan}}%");
            })
            ->whereHas('verifikasi', function ($q) {
                $q->where('t_id_status_verifikasi', 3);
            })
            ->orderBy('t_tgl_pelayanan')->get();

        $viewName = 'verifikasi.exports.pelayanan-verifikasi-ditolak';
        $pegawai = Pegawai::find($request->ttd);

        if ($request->format == 'XLS') {
            return Excel::download(new VerifikasiExport($pelayanan, $viewName, $request->tglcetak, $pegawai), Uuid::uuid4() . '.xlsx');
        }

        return Pdf::setOption(['dpi' => 150, 'defaultFont' => 'Arial'])
            ->setPaper('a4', 'landscape')
            ->loadView($viewName, [
                'pelayanan' => $pelayanan,
                'tglCetak' => $request->tglcetak,
                'pegawai' => $pegawai,
                'pemda' => PemdaModel::first(),
                'noImage' => false
            ])
            ->download(Uuid::uuid4() . '.pdf');
    }

    public function cetakVerifikasiDiterima(Request $request)
    {
        $pelayanan = Pelayanan::when($request->tglPelayanan, function ($q) use ($request) {
            $tglPelayanan = explode(' - ', $request->tglPelayanan);

            return $q->whereBetween('t_pelayanan.t_tgl_pelayanan', [
                date('Y-m-d', strtotime($tglPelayanan[0])),
                date('Y-m-d', strtotime($tglPelayanan[1]))
            ]);
        })
            ->when($request->idJenisPelayanan, function ($q) use ($request) {
                $q->where('t_pelayanan.t_id_jenis_pelayanan', $request->idJenisPelayanan);
            })
            ->when($request->namaPemohon, function ($q) use ($request) {
                $q->where('t_pelayanan.t_nama_pemohon', $request->namaPemohon);
            })
            ->when($request->noPelayanan, function ($q) use ($request) {
                $q->where('t_pelayanan.t_no_pelayanan', 'like', "%{{$request->noPelayanan}}%");
            })
            ->whereHas('verifikasi', function ($q) {
                $q->where('t_id_status_verifikasi', 4);
            })
            ->orderBy('t_tgl_pelayanan')->get();

        $viewName = 'verifikasi.exports.pelayanan-verifikasi-diterima';
        $pegawai = Pegawai::find($request->ttd);

        if ($request->format == 'XLS') {
            return Excel::download(new VerifikasiExport($pelayanan, $viewName, $request->tglcetak, $pegawai), Uuid::uuid4() . '.xlsx');
        }

        return Pdf::setOption(['dpi' => 150, 'defaultFont' => 'Arial'])
            ->setPaper('a4', 'landscape')
            ->loadView($viewName, [
                'pelayanan' => $pelayanan,
                'tglCetak' => $request->tglcetak,
                'pegawai' => $pegawai,
                'pemda' => PemdaModel::first(),
                'noImage' => false
            ])
            ->download(Uuid::uuid4() . '.pdf');
    }

    public function cetakVerifikasiDiajukan(Request $request)
    {
        $pelayanan = Pelayanan::when($request->tglPelayanan, function ($q) use ($request) {
            $tglPelayanan = explode(' - ', $request->tglPelayanan);

            return $q->whereBetween('t_pelayanan.t_tgl_pelayanan', [
                date('Y-m-d', strtotime($tglPelayanan[0])),
                date('Y-m-d', strtotime($tglPelayanan[1]))
            ]);
        })
            ->when($request->idJenisPelayanan, function ($q) use ($request) {
                $q->where('t_pelayanan.t_id_jenis_pelayanan', $request->idJenisPelayanan);
            })
            ->when($request->namaPemohon, function ($q) use ($request) {
                $q->where('t_pelayanan.t_nama_pemohon', $request->namaPemohon);
            })
            ->when($request->noPelayanan, function ($q) use ($request) {
                $q->where('t_pelayanan.t_no_pelayanan', 'like', "%{{$request->noPelayanan}}%");
            })
            ->whereHas('verifikasi', function ($q) {
                $q->where('t_id_status_verifikasi', 5);
            })
            ->orderBy('t_tgl_pelayanan')->get();

        $viewName = 'verifikasi.exports.pelayanan-verifikasi-diajukan';
        $pegawai = Pegawai::find($request->ttd);

        if ($request->format == 'XLS') {
            return Excel::download(new VerifikasiExport($pelayanan, $viewName, $request->tglcetak, $pegawai), Uuid::uuid4() . '.xlsx');
        }

        return Pdf::setOption(['dpi' => 150, 'defaultFont' => 'Arial'])
            ->setPaper('a4', 'landscape')
            ->loadView($viewName, [
                'pelayanan' => $pelayanan,
                'tglCetak' => $request->tglcetak,
                'pegawai' => $pegawai,
                'pemda' => PemdaModel::first(),
                'noImage' => false
            ])
            ->download(Uuid::uuid4() . '.pdf');
    }

    public function cetakVerifikasiSemuaData(Request $request)
    {
        $pelayanan = Pelayanan::when($request->tglPelayanan, function ($q) use ($request) {
            $tglPelayanan = explode(' - ', $request->tglPelayanan);

            return $q->whereBetween('t_pelayanan.t_tgl_pelayanan', [
                date('Y-m-d', strtotime($tglPelayanan[0])),
                date('Y-m-d', strtotime($tglPelayanan[1]))
            ]);
        })
            ->when($request->idJenisPelayanan, function ($q) use ($request) {
                $q->where('t_pelayanan.t_id_jenis_pelayanan', $request->idJenisPelayanan);
            })
            ->when($request->namaPemohon, function ($q) use ($request) {
                $q->where('t_pelayanan.t_nama_pemohon', $request->namaPemohon);
            })
            ->when($request->noPelayanan, function ($q) use ($request) {
                $q->where('t_pelayanan.t_no_pelayanan', 'like', "%{{$request->noPelayanan}}%");
            })
            ->has('pelayananAprove')
            ->orderBy('t_tgl_pelayanan')->get();

        $viewName = 'verifikasi.exports.pelayanan-data-verifikasi';
        $pegawai = Pegawai::find($request->ttd);

        if ($request->format == 'XLS') {
            return Excel::download(new VerifikasiExport($pelayanan, $viewName, $request->tglcetak, $pegawai), Uuid::uuid4() . '.xlsx');
        }

        return Pdf::setOption(['dpi' => 150, 'defaultFont' => 'Arial'])
            ->setPaper('a4', 'landscape')
            ->loadView($viewName, [
                'pelayanan' => $pelayanan,
                'tglCetak' => $request->tglcetak,
                'pegawai' => $pegawai,
                'pemda' => PemdaModel::first(),
                'noImage' => false
            ])
            ->download(Uuid::uuid4() . '.pdf');
    }
}
