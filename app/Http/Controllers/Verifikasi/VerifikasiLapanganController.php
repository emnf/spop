<?php

namespace App\Http\Controllers\Verifikasi;

use App\Exports\VerlapExport;
use App\Helpers\CountHelper;
use App\Helpers\DatagridHelpers;
use App\Helpers\PelayananHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\Verifikasi\VerifikasiLapanganTrait;
use App\Models\Hasil\HasilVerlapDetailBangunan;
use App\Models\Hasil\HasilVerlapOp;
use App\Models\Hasil\HasilVerlapWp;
use App\Models\Pbb\DatOpBumi;
use App\Models\Pbb\DatPetaBlok;
use App\Models\Pbb\DatPetaZnt;
use App\Models\Pbb\Kecamatan;
use App\Models\Pbb\Kelurahan;
use App\Models\Pbb\LookupItem;
use App\Models\Pbb\RefDati;
use App\Models\Pelayanan\DetailBangunan;
use App\Models\Pelayanan\Op;
use App\Models\Pelayanan\Pelayanan;
use App\Models\Pelayanan\Wp;
use App\Models\Setting\JenisPajak;
use App\Models\Setting\JenisPelayanan;
use App\Models\Setting\PemdaModel;
use App\Models\Validasi\Validasi;
use App\Service\Pbb\OpBumiService;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Ramsey\Uuid\Uuid;

class VerifikasiLapanganController extends Controller
{
    use VerifikasiLapanganTrait;

    const VERLAP_BELUM = 1;
    const VERLAP_SUDAH = 2;
    const VERLAP_DITOLAK = 3;

    public function index()
    {
        $param = (new CountHelper)->countPelayanan();
        $param['jenisPajak'] = JenisPajak::get();
        $param['jenisPelayanan'] = JenisPelayanan::isActive()->get();

        return view('verifikasi-lapangan.index', $param);
    }

    public function create(Pelayanan $pelayanan)
    {
        $pelayanan->load('jenisPajak')
            ->load('jenisPelayanan')
            ->load('objekPajak')
            ->load('wajibPajak')
            ->load('detailBangunan')
            ->load('OpLama');

        $tglSelesai = Carbon::createFromFormat('Y-m-d H:i:s', $pelayanan->pelayananAprove->t_tgl_perkiraan_selesai);
        $sisa = Carbon::parse(date('Y-m-d H:i:s'))->diffInDaysFiltered(function (Carbon $date) {
            return $date->isWeekday();
        }, $tglSelesai);

        $data = [
            'pelayanan' => $pelayanan,
            'jenisPajak' => $pelayanan->jenisPajak,
            'jenisLayanan' => $pelayanan->jenisPelayanan,
            'objekPajak' => $pelayanan->objekPajak,
            'wajibPajak' => $pelayanan->wajibPajak,
            'kecamatan' => Kecamatan::get(),
            'perkiraanSelesai' => PelayananHelper::formatTglPerkiraanSelesaai(date('Y-m-d H:i:s'), $sisa),
        ];

        if ($data['jenisPajak']->s_id_jenis_pajak == 10 && in_array($data['jenisLayanan']->s_id_jenis_pelayanan, [1, 2, 4, 3, 22])) {
            $data['lookupItem1'] = LookupItem::where('KD_LOOKUP_GROUP', PelayananHelper::JENIS_BUMI)->get();
            $data['lookupItem2'] = LookupItem::where('KD_LOOKUP_GROUP', PelayananHelper::KETERANGAN_OP)->get(); // rencana mau di comment
        }
        // dd($pelayanan);
        $znt = [];
        if ($pelayanan->t_id_jenis_pelayanan == 3) {
            $znt = DatPetaZnt::where('KD_KECAMATAN', $pelayanan->OpLama->kd_kecamatan)
                ->where('KD_KELURAHAN', $pelayanan->OpLama->kd_kelurahan)
                ->where('KD_BLOK', $pelayanan->OpLama->kd_blok)->get();
        }
        // dd($znt);

        $dati = RefDati::first();
        $data['kdKabkota'] = $dati->kode_kabkota;
        $data['znt'] = $znt;

        return view('verifikasi-lapangan.tambah', $data);
    }

    public function store(Request $request)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $pelayanan = Pelayanan::with(['jenisPelayanan', 'validasi'])->where('t_id_pelayanan',  $request->idPelayanan)->first();
            $rules = ['noBAHP' => 'required'];

            if ($pelayanan->t_id_jenis_pelayanan == 1) {
                $rules = [
                    'tglSHM' => 'required',
                    'noSHM' => 'required',
                    'noBAHP' => 'required',
                    'luasTanahSHM' => 'required',
                    // 'tempatPenelitian' => 'required',
                ];
            }

            $validasi = $request->validate($rules);
            $dati = RefDati::first();
            $kecamatan = Kecamatan::byKecamatanId($dati->kd_propinsi . $dati->kd_dati2 . $request->kodeKecamatan)->first();
            $kelurahan = Kelurahan::byKelurahanId($dati->kd_propinsi . $dati->kd_dati2 . $request->kodeKecamatan . $request->kodeKelurahan)->first();

            $verifikasiLapangan = $this->saveVerlap($request);

            $updateStatusVlidasi = Validasi::where('t_id_validasi', $pelayanan->validasi->t_id_validasi)
                ->update(['t_id_status_validasi' => 6]);

            $hasilVerlap = $this->saveHasilVerlap($request);

            if ($pelayanan->t_id_jenis_pelayanan == 1) {
                $hasilVerifikasiLapnganOpBaru = $this->saveHasilVerlapOpBaru($request);
            }

            if ($pelayanan->t_id_jenis_pelayanan == 3) {
                $this->saveDetailVerlapMutasiPecah($request, $pelayanan, $kecamatan, $kelurahan);
            } else {
                $this->saveDetailVerlap($request, $pelayanan, $kecamatan, $kelurahan);
            }

            DB::commit();
            return redirect('verifikasi-lapangan')->with('success', 'Verifikasi lapangan berasil disimpan');
        } catch (\Throwable $th) {
            DB::rollBack();
            report($th);
            dd($th);
            return redirect('verifikasi-lapangan')->with('error', 'Verifikasi lapangan gagal disimpan');
        }
    }

    public function findDataZnt(string $nop)
    {
        $dataOpBumi = DatOpBumi::findByNop(str_replace('.', '', $nop))->first();

        $response = [];
        if (!is_null($dataOpBumi)) {
            $daftarKodeBlok = DatPetaBlok::findByKdKecamtanKelurahan(
                $dataOpBumi['kd_kecamatan'],
                $dataOpBumi['kd_kelurahan']
            )->orderBy('kd_blok')->pluck('kd_blok');
            $daftarKodeZnt = DatPetaZnt::findByKdKecamtanKelurahan(
                $dataOpBumi['kd_kecamatan'],
                $dataOpBumi['kd_kelurahan'],
                $dataOpBumi['kd_blok']
            )->whereIn('KD_BLOK', $daftarKodeBlok)->pluck('kd_znt');
            // dd($daftarKodeZnt, $dataOpBumi);
            $response['kodeBlokList'] = $daftarKodeBlok;
            $response['kodeBlok'] = $dataOpBumi['kd_blok'];
            $response['kodeZNTList'] = $daftarKodeZnt;
            $response['kodeZNT'] = $dataOpBumi['kd_znt'];
        }

        return response()->json([
            'success' => !is_null($dataOpBumi) ? true : false,
            'data' => $response
        ], 200);
    }

    public function datagridBelumProses(Request $request)
    {
        try {
            $verlap = $this->dataBelumProses($request->filter, $request->pagination, $request->sorting);
            $dataArr = [];
            foreach ($verlap as $key => $v) {
                $dataArr[] = [
                    'jenisPajak' => $v->s_nama_singkat_pajak,
                    'jenisPelayanan' => $v->s_nama_jenis_pelayanan,
                    'tglPermohonan' => date('d-m-Y', strtotime($v->t_tgl_verifikasi)),
                    'noPelayanan' => PelayananHelper::formatNoPelayanan(date('Y', strtotime($v->t_tgl_pelayanan)), $v->t_id_jenis_pajak, $v->t_no_pelayanan),
                    'namaPemohon' => $v->t_nama_pemohon,
                    'tglVerifikasi' => date('d-m-Y', strtotime($v->t_tgl_verifikasi)),
                    'maxVerlap' => date('d-m-Y', strtotime($v->t_tgl_akhir_verlap)),
                    'keterangan' => $v->t_keterangan_validasi,
                    'actionList' => array(
                        [
                            'actionName' => 'tambah-verifikasi-lapangan',
                            'actionUrl' => "verifikasi-lapangan/tambah/" . $v->uuid,
                            'actionTitle' => 'Edit Pelayanan',
                            'actionActive' => true
                        ]
                    )
                ];
            }

            return (new DatagridHelpers)->getSuccessResponse($dataArr, $verlap);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }

    public function datagirdDitolak(Request $request)
    {
        try {
            $verlap = $this->datagridVerlap($request->filter, $request->pagination, $request->sorting, 3);
            $dataArr = [];
            foreach ($verlap as $key => $v) {
                $dataArr[] = [
                    'jenisPajak' => $v->s_nama_singkat_pajak,
                    'jenisPelayanan' => $v->s_nama_jenis_pelayanan,
                    'tglPermohonan' => date('d-m-Y', strtotime($v->t_tgl_verifikasi)),
                    'noPelayanan' => PelayananHelper::formatNoPelayanan(date('Y', strtotime($v->t_tgl_pelayanan)), $v->t_id_jenis_pajak, $v->t_no_pelayanan),
                    'namaPemohon' => $v->t_nama_pemohon,
                    'tglVerifikasi' => date('d-m-Y', strtotime($v->t_tgl_verifikasi_lapangan)),
                    'keterangan' => $v->t_keterangan_verifikasi_lapangan,
                    'actionList' => array(
                        [
                            'actionName' => 'tambah-verifikasi-lapangan',
                            'actionUrl' => "verifikasi-lapangan/tambah/" . $v->t_id_pelayanan,
                            'actionTitle' => 'Edit Pelayanan',
                            'actionActive' => true
                        ]
                    )
                ];
            }

            return (new DatagridHelpers)->getSuccessResponse($dataArr, $verlap);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }

    public function datagirdDiterima(Request $request)
    {
        try {
            $verlap = $this->datagridVerlap($request->filter, $request->pagination, $request->sorting, 2);
            $dataArr = [];
            foreach ($verlap as $key => $v) {
                $dataArr[] = [
                    'jenisPajak' => $v->s_nama_singkat_pajak,
                    'jenisPelayanan' => $v->s_nama_jenis_pelayanan,
                    'tglPermohonan' => date('d-m-Y', strtotime($v->t_tgl_verifikasi)),
                    'noPelayanan' => PelayananHelper::formatNoPelayanan(date('Y', strtotime($v->t_tgl_pelayanan)), $v->t_id_jenis_pajak, $v->t_no_pelayanan),
                    'namaPemohon' => $v->t_nama_pemohon,
                    'tglVerifikasi' => date('d-m-Y', strtotime($v->t_tgl_verifikasi_lapangan)),
                    'keterangan' => $v->t_keterangan_verifikasi_lapangan,
                    'actionList' => array(
                        [
                            'actionName' => 'tambah-verifikasi-lapangan',
                            'actionUrl' => "verifikasi-lapangan/tambah/" . $v->t_id_pelayanan,
                            'actionTitle' => 'Edit Pelayanan',
                            'actionActive' => true
                        ]
                    )
                ];
            }

            return (new DatagridHelpers)->getSuccessResponse($dataArr, $verlap);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }

    public function datagirdSemuaData(Request $request)
    {
        try {
            $verlap = $this->datagridVerlapSemua($request->filter, $request->pagination, $request->sorting);
            $dataArr = [];
            foreach ($verlap as $key => $v) {

                $dataArr[] = [
                    'jenisPajak' => $v->s_nama_singkat_pajak,
                    'jenisPelayanan' => $v->s_nama_jenis_pelayanan,
                    'tglPermohonan' => date('d-m-Y', strtotime($v->t_tgl_verifikasi)),
                    'noPelayanan' => PelayananHelper::formatNoPelayanan(date('Y', strtotime($v->t_tgl_pelayanan)), $v->t_id_jenis_pajak, $v->t_no_pelayanan),
                    'namaPemohon' => $v->t_nama_pemohon,
                    'statusVerlap' => $v->s_nama_status_verifikasi_lapangan ?? 'Belum',
                    'tglVerifikasi' => date('d-m-Y', strtotime($v->t_tgl_verifikasi_lapangan)),
                    'keterangan' => $v->t_keterangan_verifikasi_lapangan ?? '',
                    'actionList' => array(
                        [
                            'actionName' => 'tambah-verifikasi-lapangan',
                            'actionUrl' => "verifikasi-lapangan/tambah/" . $v->t_id_pelayanan,
                            'actionTitle' => 'Edit Pelayanan',
                            'actionActive' => is_null($v->t_id_verifikasi_lapangan) ?? true
                        ]
                    )
                ];
            }

            return (new DatagridHelpers)->getSuccessResponse($dataArr, $verlap);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }

    public function cetakVerlapBelum(Request $request)
    {
        $pelayanan = $this->getDataVerlap($request, 1);
        $viewName = 'verifikasi-lapangan.exports.cetak-belum';

        if ($request->format == 'XLS') {
            return Excel::download(new VerlapExport($pelayanan, $viewName), Uuid::uuid4() . '.xlsx');
        }

        return Pdf::setOption(['dpi' => 150, 'defaultFont' => 'Arial'])
            ->setPaper('a4', 'landscape')
            ->loadView($viewName, [
                'pelayanan' => $pelayanan,
                // 'tglCetak' => $request->tglcetak,
                // 'pegawai' => $pegawai,
                'pemda' => PemdaModel::first(),
                'noImage' => false
            ])
            ->download(Uuid::uuid4() . '.pdf');
    }

    public function cetakVerlapDitolak(Request $request)
    {
        $pelayanan = $this->getDataVerlap($request, 3);

        $viewName = 'verifikasi-lapangan.exports.cetak-belum';

        if ($request->format == 'XLS') {
            return Excel::download(new VerlapExport($pelayanan, $viewName), Uuid::uuid4() . '.xlsx');
        }

        return Pdf::setOption(['dpi' => 150, 'defaultFont' => 'Arial'])
            ->setPaper('a4', 'landscape')
            ->loadView($viewName, [
                'pelayanan' => $pelayanan,
                // 'tglCetak' => $request->tglcetak,
                // 'pegawai' => $pegawai,
                'pemda' => PemdaModel::first(),
                'noImage' => false
            ])
            ->download(Uuid::uuid4() . '.pdf');
    }

    public function cetakVerlapSudah(Request $request)
    {
        $pelayanan = $this->getDataVerlap($request, 2);

        $viewName = 'verifikasi-lapangan.exports.cetak-verlap';
        // $pegawai = Pegawai::find($request->ttd);

        if ($request->format == 'XLS') {
            return Excel::download(new VerlapExport($pelayanan, $viewName), Uuid::uuid4() . '.xlsx');
        }

        return Pdf::setOption(['dpi' => 150, 'defaultFont' => 'Arial'])
            ->setPaper('a4', 'landscape')
            ->loadView($viewName, [
                'pelayanan' => $pelayanan,
                // 'tglCetak' => $request->tglcetak,
                // 'pegawai' => $pegawai,
                'pemda' => PemdaModel::first(),
                'noImage' => false
            ])
            ->stream(Uuid::uuid4() . '.pdf');
    }

    public function cetakVerlapSemua(Request $request)
    {
        $pelayanan = $this->getDataVerlap($request, 0);

        $viewName = 'verifikasi-lapangan.exports.cetak-verlap';
        // $pegawai = Pegawai::find($request->ttd);

        if ($request->format == 'XLS') {
            return Excel::download(new VerlapExport($pelayanan, $viewName), Uuid::uuid4() . '.xlsx');
        }

        return Pdf::setOption(['dpi' => 150, 'defaultFont' => 'Arial'])
            ->setPaper('a4', 'landscape')
            ->loadView($viewName, [
                'pelayanan' => $pelayanan,
                // 'tglCetak' => $request->tglcetak,
                // 'pegawai' => $pegawai,
                'pemda' => PemdaModel::first(),
                'noImage' => false
            ])
            ->download(Uuid::uuid4() . '.pdf');
    }

    public function getKelasBumi(string $nop, string $kd_znt, $luas_tanah, OpBumiService $opBumiService)
    {
        try {
            $nilaiBumi = $opBumiService->penilaianBumi(str_replace('.', '', $nop), $kd_znt, $luas_tanah);
            $nilaiPermeter = ($nilaiBumi / $luas_tanah) * 1000; // ikut di store prosedur database pbb
            $kelasTanah = $opBumiService->penentuanKelsaTanah($nilaiPermeter, substr($nop, 3, 2));

            $response = [
                'success' => true,
                'message' => 'Proses berhasil',
                'data' => $kelasTanah
            ];

            return response()->json($response, 200);
        } catch (\Throwable $th) {
            report($th);
            return response()->json([
                'success' => false,
                'message' => 'Terjadi kesalahan di server'
            ], 200);
        }
    }
}
