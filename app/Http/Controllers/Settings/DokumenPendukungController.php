<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting\JenisPajak;
use App\Models\Setting\JenisPelayanan;
use App\Models\Setting\DokumenPendukung;
use App\Helpers\ComboHelper;
use App\Helpers\DatagridHelpers;
use App\Http\Requests\Setting\DokumenPendukungRequest;

class DokumenPendukungController extends Controller
{
    public function index()
    {
        return view('setting-dokumen-pendukung.index', [
            'jenisPelayanan' => JenisPelayanan::get(),
            'jenisPajak' => JenisPajak::get()
        ]);
    }

    public function create()
    {
        return view('setting-dokumen-pendukung.tambah', [
            'jenisPelayanan' => JenisPelayanan::get(),
            'jenisPajak' => JenisPajak::get()
        ]);
    }

    public function store(DokumenPendukungRequest $request)
    {
        try {
            DokumenPendukung::create($request->all());
            session()->flash('success', 'Data berhasil disimpan.');
        } catch (\Throwable $th) {
            report($th);
            session()->flash('error', 'Data gagal disimpan.');
        }
        return redirect('setting-dokumen-pendukung');
    }

    public function edit(DokumenPendukung $dokumenPendukung)
    {
        return view('setting-dokumen-pendukung.edit', [
            'jenisPelayanan' => JenisPelayanan::get(),
            'jenisPajak' => JenisPajak::get(),
            'data' => $dokumenPendukung
        ]);
    }

    public function update(DokumenPendukungRequest $request, DokumenPendukung $dokumenPendukung)
    {
        try {
            $dokumenPendukung->update($request->except('_token', '_method'));
            session()->flash('success', 'Data berhasil diupdate.');
        } catch (\Throwable $e) {
            report($e);
            session()->flash('error', 'Data gagal diupdate.');
        }
        return redirect('setting-dokumen-pendukung');
    }

    public function destroy(DokumenPendukung $dokumenPendukung)
    {
        try {
            $dokumenPendukung->delete();
            return response()->json('success');
        } catch (\Throwable $e) {
            report($e);
            return response()->json('error');
        }
    }

    public function datagrid(Request $request)
    {
        try {
            $response = (new DokumenPendukung())->datagrid($request);
            $dataArr = [];
            foreach ($response as $v) {
                $dataArr[] = [
                    's_id_jenis_pajak' => $v['dataJenisPajak']['s_nama_jenis_pajak'],
                    's_id_jenis_pelayanan' => $v['dataJenisPelayanan']['s_nama_jenis_pelayanan'],
                    's_nama_dokumen_pendukung' => $v['s_nama_dokumen_pendukung'],
                    'actionList' => [
                        [
                            'actionName' => 'edit',
                            'actionUrl' => '/setting-dokumen-pendukung/edit/' . $v['s_id_dokumen_pendukung'],
                            'actionActive' => true
                        ],
                        [
                            'actionName' => 'delete',
                            'actionUrl' => 'javascript:showDeleteDialog(' . $v['s_id_dokumen_pendukung'] . ')',
                            'actionActive' => true
                        ]
                    ]
                ];
            }

            return (new DatagridHelpers)->getSuccessResponse($dataArr, $response);
        } catch (\Throwable $e) {
            report($e);
        }
    }

    public function comboJenisPelayanan($idJenisPajak)
    {
        return response()->json(
            ComboHelper::getDataJenisPelayanan($idJenisPajak),
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }
}
