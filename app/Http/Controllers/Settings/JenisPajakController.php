<?php

namespace App\Http\Controllers\Settings;

use App\Helpers\DatagridHelpers;
use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\JenisPajakRequest;
use App\Models\Setting\JenisKategoriPajak;
use App\Models\Setting\JenisPajak;
use Illuminate\Http\Request;
use Throwable;

class JenisPajakController extends Controller
{
    public function index()
    {
        return view('setting-jenis-pajak.index', [
            'kategori' => JenisKategoriPajak::get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Setting\JenisPajak  $jenisPajak
     * @return \Illuminate\Http\Response
     */
    public function edit(JenisPajak $jenisPajak)
    {
        return view('setting-jenis-pajak.edit', [
            'jenisPajak' => $jenisPajak,
            'kategori' => JenisKategoriPajak::get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Setting\JenisPajak  $jenisPajak
     * @return \Illuminate\Http\Response
     */
    public function update(JenisPajakRequest $request, JenisPajak $jenisPajak)
    {
        try {
            $jenisPajak->update($request->except('_token', '_method'));
            session()->flash('success', 'Data berhasil diupdate.');
        } catch (Throwable $e) {
            report($e);
            session()->flash('error', 'Data gagal diupdate.');
        }
        return redirect('setting-jenis-pajak');
    }

    public function datagrid(Request $request)
    {
        try {
            $response = (new JenisPajak())->datagrid($request);
            $dataArr = [];
            foreach ($response as $v) {
                $dataArr[] = [
                    's_icon' => '<i class="' . $v['s_icon'] . '"></i>',
                    's_nama_jenis_pajak' => $v['s_nama_jenis_pajak'],
                    's_nama_singkat_pajak' => $v['s_nama_singkat_pajak'],
                    's_nama_kategori_pajak' => $v['dataKategoriPajak']['s_nama_kategori_pajak'],
                    's_active' => ($v['s_active']) ? 'Aktif' : 'Tidak Aktif',
                    'actionList' => [
                        [
                            'actionName' => 'edit',
                            'actionUrl' => '/setting-jenis-pajak/edit/' . $v['s_id_jenis_pajak'],
                            'actionActive' => true
                        ]
                    ]
                ];
            }
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $response);
        } catch (Throwable $e) {
            report($e);
        }
    }
}
