<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AboutController extends Controller
{
    public function index()
    {
        $param = DB::connection('pgsql')->table('s_about')->first();

        return view('setting-about.index', ['about' => $param]);
    }

    public function store(Request $request)
    {

        $id = $request->s_id ?? 1;
        DB::connection('pgsql')->table('s_about')->updateOrInsert(
            ['s_id' => $id],
            [
                's_keterangan_about' => $request->s_keterangan_about,
                'created_at' => now()->format('Y-m-d H:m:s'),
                'updated_at' => now()->format('Y-m-d H:m:s')
            ]
        );
        return redirect()->back()->with('success', 'Data berhasil disimpan.');
    }
}
