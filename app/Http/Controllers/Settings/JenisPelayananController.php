<?php

namespace App\Http\Controllers\Settings;

use App\Helpers\DatagridHelpers;
use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\JenisPelayananRequest;
use App\Models\Setting\JenisPajak;
use App\Models\Setting\JenisPelayanan;
use Illuminate\Http\Request;
use Throwable;

class JenisPelayananController extends Controller
{
    public function index()
    {
        return view('setting-jenis-pelayanan.index', [
            'jenisPajak' => JenisPajak::get(),
            'jenispelayanan' => JenisPelayanan::get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('setting-jenis-pelayanan.tambah', [
            'jenisPajak' => JenisPajak::get(),
            's_order' => JenisPelayanan::max('s_order') + 1
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\JenisPelayananRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JenisPelayananRequest $request)
    {
        try {
            JenisPelayanan::create($request->all());
            session()->flash('success', 'Data berhasil disimpan.');
        } catch (Throwable $th) {
            report($th);
            session()->flash('error', 'Data gagal disimpan.');
        }
        return redirect('setting-jenis-pelayanan');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Setting\JenisPelayanan  $jenisPelayanan
     * @return \Illuminate\Http\Response
     */
    public function edit(JenisPelayanan $jenisPelayanan)
    {
        return view('setting-jenis-pelayanan.edit', [
            'jenisPajak' => JenisPajak::get(),
            'data' => $jenisPelayanan
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\JenisPelayananRequest  $request
     * @param  \App\Models\Setting\JenisPelayanan  $jenisPelayanan
     * @return \Illuminate\Http\Response
     */
    public function update(JenisPelayananRequest $request, JenisPelayanan $jenisPelayanan)
    {
        try {
            $jenisPelayanan->where('s_id_jenis_pelayanan', $request['s_id_jenis_pelayanan'])->update($request->except('_token', '_method'));
            session()->flash('success', 'Data berhasil diupdate.');
        } catch (Throwable $e) {
            report($e);
            session()->flash('error', 'Data gagal diupdate.');
        }
        return redirect('setting-jenis-pelayanan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Setting\JenisPelayanan  $jenisPelayanan
     * @return \Illuminate\Http\Response
     */
    public function destroy(JenisPelayanan $jenisPelayanan)
    {
        try {
            $jenisPelayanan->where('s_id_jenis_pelayanan', $jenisPelayanan->s_id_jenis_pelayanan)->delete();
            return response()->json('success');
        } catch (Throwable $e) {
            report($e);
            return response()->json('error');
        }
    }

    public function datagrid(Request $request)
    {
        try {
            $response = (new JenisPelayanan())->datagrid($request);
            $dataArr = [];
            foreach ($response as $v) {
                $dataArr[] = [
                    's_id_jenis_pajak' => $v['dataJenisPajak']['s_nama_singkat_pajak'],
                    's_nama_jenis_pelayanan' => $v['s_nama_jenis_pelayanan'],
                    's_lunas_pbb' => ($v['s_lunas_pbb']) ? 'Aktif' : 'Tidak Aktif',
                    's_active' => ($v['s_active']) ? 'Aktif' : 'Tidak Aktif',
                    's_keterangan_jenis_pelayanan' => $v['s_keterangan_jenis_pelayanan'],
                    's_order' => $v['s_order'],
                    'actionList' => [
                        [
                            'actionName' => 'edit',
                            'actionUrl' => '/setting-jenis-pelayanan/edit/' . $v['s_id_jenis_pelayanan'],
                            'actionActive' => true
                        ],
                        [
                            'actionName' => 'delete',
                            'actionUrl' => 'javascript:showDeleteDialog(' . $v['s_id_jenis_pelayanan'] . ')',
                            'actionActive' => true
                        ]
                    ]
                ];
            }
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $response);
        } catch (Throwable $e) {
            report($e);
        }
    }
}
