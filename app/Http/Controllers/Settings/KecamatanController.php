<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Pbb\Kecamatan;
use Illuminate\Http\Request;

class KecamatanController extends Controller
{
    public function index()
    {
        return view('setting-kecamatan.index');
    }

    public function getKecamatan()
    {
        try {
            $kecamatan = Kecamatan::orderBy('kd_kecamatan', 'asc')->get();

            return response()->json([
                'success' => true,
                'data' => $kecamatan->toArray()
            ], 200);
        } catch (\Throwable $th) {
            report('ERROR : ' . $th);

            return response()->json([
                'success' => false,
                'message' => 'Terjadi Kesalahan'
            ], 200);
        }
    }

    public function getKecamatanById(string $kecamatanId)
    {
        return response()->json([
            'success' => true,
            'data' => Kecamatan::byKecamatanId($kecamatanId)->first()
        ], 200);
    }
}
