<?php

namespace App\Http\Controllers\Settings;

use App\Helpers\DatagridHelpers;
use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\ChangePasswordRequest;
use App\Http\Requests\Setting\UserRequest;
use App\Models\Setting\Pegawai;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{

    public function index()
    {
        return view('setting-user.index');
    }

    public function create()
    {
        return view('setting-user.tambah', [
            'user' => new User,
            'pegawais' => Pegawai::all(),
            'roles' => Role::where('name', '!=', 'Super Admin')->get()
        ]);
    }

    public function store(UserRequest $request, User $user)
    {
        $userData = [
            'name' => $request->name,
            'email' => $request->email,
            'nik' => $request->nik,
            'nohp' => $request->nohp,
            'username' => $request->username,
            'is_active' => $request->is_active,
            's_id_pegawai' => $request->s_id_pegawai,
            'email_verified_at' => date('Y-m-d H:i:s')

        ];

        if (!empty($request->password)) {
            $userData['password'] = Hash::make($request->password);
        }

        $data = $user = User::updateOrCreate(
            ['id' =>  $user->id],
            $userData
        );

        $user->syncRoles($request->role);

        return redirect('setting-user')->with('success', 'User berhasil ditambah');
    }

    public function edit(User $user)
    {
        return view('setting-user.edit', [
            'user' => $user,
            'pegawais' => Pegawai::all(),
            'roles' => Role::get()
        ]);
    }

    public function destroy($id)
    {
        //
    }

    public function datagrid(Request $request)
    {
        try {
            $users = User::when(empty($request->sorting), function ($q) {
                $q->orderBy('id');
            }, function ($q) use ($request) {
                $q->orderBy($request['sorting']['key'], $request['sorting']['order']);
            })
                ->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
            $dataArr = [];

            foreach ($users as $key => $user) {
                $dataArr[] = [
                    'userName' => $user->username,
                    'name' => $user->name,
                    'role' => count($user->getRoleNames()) > 0 ? implode(",", $user->getRoleNames()->toArray()) : '-',
                    'isActive' => $user->is_active ? 'Ya' : 'Tidak',
                    'foto' => '-',
                    'actionList' => array(
                        [
                            'actionName' => 'edit',
                            'actionUrl' => '/setting-user/' . $user->id . '/edit',
                            'actionActive' => true
                        ],
                        [
                            'actionName' => 'delete',
                            'actionUrl' => 'javascript:showDeleteDialog(' . $user->id . ')',
                            'actionActive' => true
                        ],
                    )
                ];
            }
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $users);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }

    public function changePassword()
    {
        return view('change-password.index');
    }

    public function savePassword(ChangePasswordRequest $request)
    {
        $user = auth()->user();

        if (Hash::check($request->old_password, $user->password)) {
            $user->update([
                'password' => bcrypt($request->new_password)
            ]);

            session()->flash('success', 'Password Berhasil di Perbarui');
        } else {
            session()->flash('error', 'Password baru gagal disimpan');
        }

        return redirect()->back();
    }
}
