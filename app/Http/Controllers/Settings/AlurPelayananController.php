<?php

namespace App\Http\Controllers\Settings;

use App\Helpers\DatagridHelpers;
use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\AlurPelayananRequest;
use App\Models\Setting\AlurPelayanan;
use Illuminate\Http\Request;

class AlurPelayananController extends Controller
{

    public function index()
    {
        return view('setting-alur-pelayanan.index');
    }

    public function create()
    {
        return view('setting-alur-pelayanan.tambah');
    }

    public function store(AlurPelayananRequest $request)
    {
        try {
            AlurPelayanan::updateOrcreate(
                ['s_id_alur' => $request->s_id_alur],
                $request->except('_token')
            );
            session()->flash('success', 'Data berhasil disimpan');
        } catch (\Throwable $th) {
            report($th);
            session()->flash('error', 'Data gagal disimpan.');
        }
        return redirect('setting-alur-pelayanan');
    }

    public function edit(AlurPelayanan $alurPelayanan)
    {
        $param = $alurPelayanan;
        return view('setting-alur-pelayanan.tambah', $param);
    }

    public function destroy(AlurPelayanan $alurPelayanan)
    {
        try {
            $alurPelayanan->delete();
            return response()->json('success');
        } catch (\Throwable $e) {
            report($e);
            return response()->json('error');
        }
    }

    public function datagrid(Request $request)
    {
        try {
            $response = (new AlurPelayanan())->datagrid($request);
            $dataArr = [];
            foreach ($response as $key => $v) {
                $dataArr[] = [
                    's_judul_alur' => $v->s_judul_alur,
                    's_deskripsi_alur' => $v->s_deskripsi_alur,
                    's_langkah_alur' => $v->s_langkah_alur,
                    's_user_alur' => $v->s_user_alur ?? '-',
                    's_icon_alur' => $v->s_icon_alur ?? '-',
                    'actionList' => array(
                        [
                            'actionName' => 'edit',
                            'actionUrl' => '/setting-alur-pelayanan/'. $v->s_id_alur .'/edit',
                            'actionActive' => true
                        ],
                        [
                            'actionName' => 'delete',
                            'actionUrl' => 'javascript:showDeleteDialog(' . $v->s_id_alur . ')',
                            'actionActive' => true
                        ],
                    )
                ];
            }
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $response);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }
}
