<?php

namespace App\Http\Controllers\Settings;

use App\Helpers\DatagridHelpers;
use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\FaqRequest;
use App\Models\Setting\Faq;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    public function index()
    {
        return view('setting-faq.index');
    }

    public function create()
    {
        return view('setting-faq.tambah');
    }

    public function store(FaqRequest $request)
    {
        // dd($request);
        try {
            Faq::updateOrCreate(
                ['s_id_faq' => $request->s_id_faq],
                $request->except('_token')
            );
            session()->flash('success', 'Data berhasil disimpan');
        } catch (\Throwable $th) {
            report($th);
            session()->flash('error', 'Data gagal disimpan.');
        }
        return redirect('setting-faq');
    }

    public function edit(Faq $faq)
    {
        $param = $faq;
        return view('setting-faq.tambah', $param);
    }

    public function destroy(Faq $faq)
    {
        try {
            $faq->delete();
            return response()->json('success');
        } catch (\Throwable $e) {
            report($e);
            return response()->json('error');
        }
    }

    public function datagrid(Request $request)
    {
        try {
            $response = (new Faq())->datagrid($request);
            $dataArr = [];
            foreach ($response as $key => $v) {
                $dataArr[] = [
                    's_pertanyaan' => $v->s_pertanyaan,
                    's_jawaban' => $v->s_jawaban,
                    'actionList' => array(
                        [
                            'actionName' => 'edit',
                            'actionUrl' => '/setting-faq/'. $v->s_id_faq .'/edit',
                            'actionActive' => true
                        ],
                        [
                            'actionName' => 'delete',
                            'actionUrl' => 'javascript:showDeleteDialog(' . $v->s_id_faq . ')',
                            'actionActive' => true
                        ],
                    )
                ];
            }
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $response);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }
}
