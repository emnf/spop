<?php

namespace App\Http\Controllers\Settings;

use App\Helpers\ComboHelper;
use App\Helpers\DatagridHelpers;
use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\JenisHasilPelayananRequest;
use App\Models\Setting\JenisHasilPelayanan;
use App\Models\Setting\JenisPajak;
use App\Models\Setting\JenisPelayanan;
use Illuminate\Http\Request;
use Throwable;

class JenisHasilPelayananController extends Controller
{
    public function index()
    {
        return view('setting-jenis-hasil-pelayanan.index', [
            'jenisPelayanan' => JenisPelayanan::get(),
            'jenisPajak' => JenisPajak::get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('setting-jenis-hasil-pelayanan.tambah', [
            'jenisPelayanan' => JenisPelayanan::get(),
            'jenisPajak' => JenisPajak::get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\JenisHasilPelayananRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JenisHasilPelayananRequest $request)
    {
        try {
            JenisHasilPelayanan::create($request->all());
            session()->flash('success', 'Data berhasil disimpan.');
        } catch (Throwable $th) {
            report($th);
            session()->flash('error', 'Data gagal disimpan.');
        }
        return to_route('setting-jenis-hasil-pelayanan');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Setting\JenisHasilPelayanan  $jenisHasilPelayanan
     * @return \Illuminate\Http\Response
     */
    public function edit(JenisHasilPelayanan $jenisHasilPelayanan)
    {
        return view('setting-jenis-hasil-pelayanan.edit', [
            'jenisPelayanan' => JenisPelayanan::get(),
            'jenisPajak' => JenisPajak::get(),
            'data' => $jenisHasilPelayanan
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\JenisHasilPelayananRequest  $request
     * @param  \App\Models\Setting\JenisHasilPelayanan  $jenisHasilPelayanan
     * @return \Illuminate\Http\Response
     */
    public function update(JenisHasilPelayananRequest $request, JenisHasilPelayanan $jenisHasilPelayanan)
    {
        try {
            $jenisHasilPelayanan->update($request->except('_token', '_method'));
            session()->flash('success', 'Data berhasil diupdate.');
        } catch (Throwable $e) {
            report($e);
            session()->flash('error', 'Data gagal diupdate.');
        }
        return redirect('setting-jenis-hasil-pelayanan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Setting\JenisHasilPelayanan  $jenisHasilPelayanan
     * @return \Illuminate\Http\Response
     */
    public function destroy(JenisHasilPelayanan $jenisHasilPelayanan)
    {
        try {
            $jenisHasilPelayanan->delete();
            return response()->json('success');
        } catch (Throwable $e) {
            report($e);
            return response()->json('error');
        }
    }

    public function datagrid(Request $request)
    {
        try {
            $response = (new JenisHasilPelayanan())->datagrid($request);
            $dataArr = [];
            foreach ($response as $v) {
                $dataArr[] = [
                    's_id_jenis_pajak' => $v['dataJenisPajak']['s_nama_jenis_pajak'],
                    's_id_jenis_pelayanan' => $v['dataJenisPelayanan']['s_nama_jenis_pelayanan'],
                    's_nama_hasil_pelayanan' => $v['s_nama_hasil_pelayanan'],
                    'actionList' => [
                        [
                            'actionName' => 'edit',
                            'actionUrl' => '/setting-jenis-hasil-pelayanan/edit/' . $v['s_id_hasil_pelayanan'],
                            'actionActive' => true
                        ],
                        [
                            'actionName' => 'delete',
                            'actionUrl' => 'javascript:showDeleteDialog(' . $v['s_id_hasil_pelayanan'] . ')',
                            'actionActive' => true
                        ]
                    ]
                ];
            }

            return (new DatagridHelpers)->getSuccessResponse($dataArr, $response);
        } catch (Throwable $e) {
            report($e);
        }
    }

    public function comboJenisPelayanan($idJenisPajak)
    {
        return response()->json(
            ComboHelper::getDataJenisPelayanan($idJenisPajak),
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }
}
