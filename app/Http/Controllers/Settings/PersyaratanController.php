<?php

namespace App\Http\Controllers\Settings;

use App\Helpers\DatagridHelpers;
use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\PersyaratanRequest;
use App\Models\Setting\JenisPelayanan;
use App\Models\Setting\Persyaratan;
use Illuminate\Http\Request;
use Throwable;

class PersyaratanController extends Controller
{
    public function index()
    {
        return view('setting-persyaratan.index', [
            'jenisPelayanan' => JenisPelayanan::get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('setting-persyaratan.tambah', [
            'jenisPelayanan' => JenisPelayanan::get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\PersyaratanRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PersyaratanRequest $request)
    {
        try {
            Persyaratan::create($request->all());
            session()->flash('success', 'Data berhasil disimpan.');
        } catch (Throwable $th) {
            report($th);
            session()->flash('error', 'Data gagal disimpan.');
        }
        return redirect('setting-persyaratan');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Setting\Persyaratan  $persyaratan
     * @return \Illuminate\Http\Response
     */
    public function edit(Persyaratan $persyaratan)
    {
        return view('setting-persyaratan.edit', [
            'jenisPelayanan' => JenisPelayanan::get(),
            'data' => $persyaratan
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\PersyaratanRequest  $request
     * @param  \App\Models\Setting\Persyaratan  $persyaratan
     * @return \Illuminate\Http\Response
     */
    public function update(PersyaratanRequest $request, Persyaratan $persyaratan)
    {
        try {
            $persyaratan->update($request->except('_token', '_method'));
            session()->flash('success', 'Data berhasil diupdate.');
        } catch (Throwable $e) {
            report($e);
            session()->flash('error', 'Data gagal diupdate.');
        }
        return redirect('setting-persyaratan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Setting\Persyaratan  $persyaratan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Persyaratan $persyaratan)
    {
        try {
            $persyaratan->delete();
            return response()->json('success');
        } catch (Throwable $e) {
            report($e);
            return response()->json('error');
        }
    }

    public function datagrid(Request $request)
    {
        try {
            $response = (new Persyaratan())->datagrid($request);
            $dataArr = [];
            foreach ($response as $v) {
                $dataArr[] = [
                    's_id_jenis_pelayanan' => $v['dataJenisPelayanan']['s_nama_jenis_pelayanan'],
                    's_nama_persyaratan' => $v['s_nama_persyaratan'],
                    's_is_optional' => ($v['s_is_optional']) ? 'Tidak Wajib' : 'Wajib',
                    'actionList' => [
                        [
                            'actionName' => 'edit',
                            'actionUrl' => '/setting-persyaratan/edit/' . $v['s_id_persyaratan'],
                            'actionActive' => true
                        ],
                        [
                            'actionName' => 'delete',
                            'actionUrl' => 'javascript:showDeleteDialog(' . $v['s_id_persyaratan'] . ')',
                            'actionActive' => true
                        ]
                    ]
                ];
            }
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $response);
        } catch (Throwable $e) {
            report($e);
        }
    }
}
