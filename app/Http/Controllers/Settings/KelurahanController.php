<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Pbb\Kelurahan;
use App\Models\Pbb\RefDati;
use Illuminate\Http\Request;

class KelurahanController extends Controller
{
    public function getKelurahanById(string $kelurahanId)
    {
        return response()->json([
            'success' => true,
            'data' => Kelurahan::byKelurahanId($kelurahanId)->first()
        ], 200);
    }

    public function getKelurahanByKecamatan(string $kecamatanId)
    {
        try {
            $dati = RefDati::first();
            $kelurahan = Kelurahan::byKecamatanId($dati->kd_propinsi . $dati->kd_dati2 . $kecamatanId)->get();
            return response()->json([
                'success' => true,
                'data' => $kelurahan
            ], 200);
        } catch (\Throwable $th) {
            report('Error : ' . $th);
            return response()->json([
                'success' => false,
                'error' => 'Terjadi kesalahan'
            ], 200);
        }
    }
}
