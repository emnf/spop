<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Setting\PemdaModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PemdaController extends Controller
{
    public function index()
    {
        return view('setting-pemda.index', [
            'pemda' => PemdaModel::first()
        ]);
    }

    public function update(Request $request, PemdaModel $pemda)
    {

        $data = $request->validate([
            's_nama_prov' => 'required',
            's_nama_kabkota' => 'required',
            's_nama_ibukota_kabkota' => 'required',
            's_kode_provinsi' => 'required',
            's_kode_kabkot' => 'required',
            's_nama_instansi' => 'required',
            's_nama_singkat_instansi' => 'required',
            's_alamat_instansi' => 'required',
            's_notelp_instansi' => 'required',
            's_kode_pos' => 'required',
            's_logo' => 'nullable'
        ]);


        if ($request->hasFile('s_logo')) {

            $request->validate([
                's_logo' => 'max:2048|mimes:jpg,png'
            ]);

            $file = $request->file('s_logo');
            $fileStored = Storage::putFile('/logo-pemda', $file);

            $data['s_logo'] = $fileStored;
        }

        if (PemdaModel::updateOrCreate(['s_id_pemda' => 1], $data)) {
            session()->flash('success', 'Data Pemda Behasil diupdate.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-pemda');
    }
}
