<?php

namespace App\Http\Controllers\Settings;

use App\Helpers\DatagridHelpers;
use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\UserManualRequest;
use App\Models\Setting\UserManual;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UserManualController extends Controller
{
    public function index()
    {
        return view('setting-user-manual.index');
    }

    public function create()
    {
        return view('setting-user-manual.tambah');
    }

    public function store(UserManualRequest $request)
    {
        try {
            $fileStored = '';
            if ($request->hasFile('s_lokasi_file')) {
                $request->validate([
                    's_lokasi_file' => 'max:2048|mimes:jpg,png,pdf,doc,docx'
                ]);
    
                $file = $request->file('s_lokasi_file');
                $fileStored = $file->store('public');
            }

            $request = $request->except('_token');
            if ($fileStored != '') {
                $request['s_lokasi_file'] = str_replace( 'public/','',$fileStored);
            }

            UserManual::updateOrcreate(
                ['s_id_usermanual' => $request['s_id_usermanual']],
                $request
            );
            session()->flash('success', 'Data berhasil disimpan');
        } catch (\Throwable $th) {
            report($th);
            session()->flash('error', 'Data gagal disimpan.');
        }
        return redirect('setting-user-manual');
    }

    public function edit(UserManual $userManual)
    {
        $param = $userManual;
        return view('setting-user-manual.tambah', $param);
    }

    public function destroy(UserManual $userManual)
    {
        try {
            if(Storage::exists('public/'.$userManual->s_lokasi_file)){
                Storage::delete('public/'.$userManual->s_lokasi_file);
            }
            $userManual->delete();
            return response()->json('success');
        } catch (\Throwable $e) {
            report($e);
            return response()->json('error');
        }
    }

    public function datagrid(Request $request)
    {
        try {
            $response = (new UserManual())->datagrid($request);
            $dataArr = [];
            foreach ($response as $key => $v) {
                $dataArr[] = [
                    's_nama_menu' => $v->s_nama_menu,
                    's_nama_file' => $v->s_nama_file,
                    's_lokasi_file' => "<a href='/storage/".$v->s_lokasi_file."' class='btn btn-success btn-xs  download' download=''><span class='fas fa-download'> Unduh</span></a>",
                    'actionList' => array(
                        [
                            'actionName' => 'edit',
                            'actionUrl' => '/setting-user-manual/'. $v->s_id_usermanual .'/edit',
                            'actionActive' => true
                        ],
                        [
                            'actionName' => 'delete',
                            'actionUrl' => 'javascript:showDeleteDialog(' . $v->s_id_usermanual . ')',
                            'actionActive' => true
                        ],
                    )
                ];
            }
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $response);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }
}
