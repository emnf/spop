<?php

namespace App\Http\Controllers\Settings;

use App\Helpers\DatagridHelpers;
use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\PegawaiRequest;
use App\Models\Setting\Pegawai;
use App\Models\Setting\PemdaModel;
use Illuminate\Http\Request;
use Throwable;

class PegawaiController extends Controller
{
    public function index()
    {
        return view('setting-pegawai.index', [
            'pemda' => PemdaModel::first()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('setting-pegawai.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Setting\PegawaiRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PegawaiRequest $request)
    {
        try {
            Pegawai::create($request->all());
            session()->flash('success', 'Data berhasil disimpan.');
        } catch (Throwable $th) {
            report($th);
            session()->flash('error', 'Data gagal disimpan.');
        }
        return redirect('setting-pegawai');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Setting\Pegawai  $pegawai
     * @return \Illuminate\Http\Response
     */
    public function edit(Pegawai $pegawai)
    {
        return view('setting-pegawai.edit', [
            'pegawai' => $pegawai
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Setting\PegawaiRequest  $request
     * @param  \App\Models\Setting\Pegawai  $pegawai
     * @return \Illuminate\Http\Response
     */
    public function update(PegawaiRequest $request, Pegawai $pegawai)
    {
        try {
            $pegawai->update($request->except('_token', '_method'));
            session()->flash('success', 'Data berhasil diupdate.');
        } catch (Throwable $e) {
            report($e);
            session()->flash('error', 'Data gagal diupdate.');
        }
        return redirect('setting-pegawai');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Setting\Pegawai  $pegawai
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pegawai $pegawai)
    {
        try {
            $pegawai->delete();
            return response()->json('success');
        } catch (Throwable $e) {
            report($e);
            return response()->json('error');
        }
    }

    public function datagrid(Request $request)
    {
        try {
            $response = (new Pegawai)->datagrid($request);
            $dataArr = [];
            foreach ($response as $v) {
                $dataArr[] = [
                    's_nama_pegawai' => $v['s_nama_pegawai'],
                    's_jabatan_pegawai' => $v['s_jabatan_pegawai'],
                    's_pangkat_pegawai' => $v['s_pangkat_pegawai'],
                    's_nip_pegawai' => $v['s_nip_pegawai'],
                    'actionList' => [
                        [
                            'actionName' => 'edit',
                            'actionUrl' => '/setting-pegawai/edit/' . $v['s_id_pegawai'],
                            'actionActive' => true
                        ],
                        [
                            'actionName' => 'delete',
                            'actionUrl' => 'javascript:showDeleteDialog(' . $v['s_id_pegawai'] . ')',
                            'actionActive' => true
                        ]
                    ]
                ];
            }
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $response);
        } catch (Throwable $e) {
            report($e);
        }
    }
}
