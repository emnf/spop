<?php

namespace App\Http\Controllers\Dokumen;

use App\Http\Controllers\Controller;
use App\Models\Esign\Esign;
use App\Models\Esign\Esign_penetapan;
use Illuminate\Http\Request;

class DokumenController extends Controller
{
    public function download($dokumen)
    {
        $res = Esign_penetapan::whereUuid($dokumen)->first();
        $pdfContent = base64_decode(str_replace('data:application/pdf;base64,', '', $res->signed));

        $fileName = date('d-m-Y H:is') . '.pdf';

        return response($pdfContent)
            ->header('Content-Type', 'application/pdf')
            ->header('Content-Disposition', 'attachment; filename="' . $fileName . '"');
    }
}
