<?php

namespace App\Http\Controllers\Esign;

use App\Helpers\DatagridHelpers;
use App\Helpers\PelayananHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\Esign\EsignTrait;
use App\Http\Traits\Esign\EsignTraitPenetapan;
use App\Http\Traits\Pelayanan\WpTrait;
use App\Http\Traits\Terbilang;
use App\Models\Esign\Esign;
use App\Models\Esign\Esign_penetapan;
use App\Models\Pbb\PegawaiPbb;
use App\Models\Pbb\RefUmum;
use App\Models\Pbb\Sppt;
use App\Models\Pelayanan\Op;
use App\Models\Pelayanan\Pelayanan;
use App\Models\Setting\Blanko;
use App\Models\Setting\JenisPelayanan;
use App\Models\Setting\Pegawai;
use App\Models\Setting\PemdaModel;
use App\Models\User;
use App\Models\Validasi\Validasi;
use App\Models\View\TunggakanView;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class EsignController extends Controller
{
    use EsignTraitPenetapan, WpTrait, Terbilang;
    public function index()
    {
        $user = Auth::user();
        $data['pegawais'] = Pegawai::where('s_id_pegawai', $user->s_id_pegawai)->get();
        $data['jenisPelayanan'] = JenisPelayanan::isActive()->get();

        return view('esign.index', $data);
    }

    public function datagridBelum(Request $request)
    {
        try {
            $helper = new PelayananHelper;
            $penetapan = $this->datagrid(
                $request->filter,
                $request->pagination,
                $request->sorting,
                $helper
            );
            $dataArr = [];
            foreach ($penetapan as $key => $v) {
                $nop = $v->kd_propinsi .
                    $v->kd_dati2 .
                    $v->kd_kecamatan .
                    $v->kd_kelurahan .
                    $v->kd_blok .
                    $v->no_urut .
                    $v->kd_jns_op;
                $formatNop = $helper->formatStringToNop($nop);
                $parsedNop = $helper->parseNop($formatNop);

                $sppt = (new Sppt())->getDataKetetapan($parsedNop, $v->thn_pajak_sppt);
                $user = User::find($v->created_by)->first();


                $dataArr[] = [
                    'click' => '<input type="checkbox" name="nop[]" id="nop" data-param="nop_' . $key . '" value="' . $formatNop . '.' . $v->thn_pajak_sppt . '" class="check-' . $key . '" style="transform: scale(1.3);">',
                    'tglpenetapan' => date('d-m-Y', strtotime($sppt->tgl_terbit_sppt)),
                    'nip_pencetak_sppt' => $sppt->nip_pencetak_sppt,
                    'nop' => $formatNop,
                    'tahun' => $sppt->thn_pajak_sppt,
                    'user_penetapan' => $user->name,
                    'actionList' => array()
                ];
            }
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $penetapan);
        } catch (\Throwable $th) {
            dd($th);
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }

    public function datagridBelumssss(Request $request)
    {
        try {
            $dataPerbiakan = $this->datagridBelumEsign(
                $request->filter,
                $request->pagination,
                $request->sorting,
                5
            );
            $dataArr = [];

            foreach ($dataPerbiakan as $key => $v) {
                $wajibPajak = $this->getNamaWp($v);
                // dd($v);
                $dataArr[] = [
                    'download' => '<input type="checkbox" name="t_id_pelayanan[]" id="t_id_pelayanan" data-param="t_id_pelayanan_' . $key . '" value="' . $v->t_id_pelayanan . '" class="check-' . $key . '" style="transform: scale(1.3);">',
                    'jenis_pelayanan' => $v->s_nama_jenis_pelayanan,
                    'tgl_permohonan' => date('d-m-Y', strtotime($v->t_tgl_pelayanan)),
                    'no_permohonan' => PelayananHelper::formatNoPelayanan(date('Y'), $v->t_id_jenis_pajak, $v->t_no_pelayanan),
                    'nama_pemohon' => $v->t_nama_pemohon
                ];
            }
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $dataPerbiakan);
        } catch (\Throwable $th) {
            dd($th);
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }

    public function datagridSudah(Request $request)
    {
        try {
            $dataPerbiakan = $this->datagridSudahEsign(
                $request->filter,
                $request->pagination,
                $request->sorting,
                5
            );
            $dataArr = [];

            foreach ($dataPerbiakan as $key => $v) {
                $parts = explode('.', $v->t_nop);
                $nop = implode('.', array_slice($parts, 0, 7));
                $pegawai = Pegawai::where('s_id_pegawai', $v->created_by)->first();

                $dataArr[] = [
                    'download' => '<a class="btn btn-sm btn-primary" href="/dokumen/download/' . $v->uuid . '" target="#">Download PDF</a>',
                    'tgl_penandatangan' => date('d-m-Y', strtotime($v->created_at)),
                    'nop' => $nop,
                    'tahun' => $v->thn_pajak_sppt,
                    'nama_pegawai' => $pegawai->s_nama_pegawai ?? ''
                ];
            }
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $dataPerbiakan);
        } catch (\Throwable $th) {
            dd($th);
            report($th);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }


    // public function create(Request $request)
    // {
    //     try {
    //         foreach ($request->t_id_pelayanan as $key => $value) {
    //             $pelayanan = Pelayanan::find($value);
    //             $ttd = Pegawai::find($request->s_id_pegawai);
    //             if ($pelayanan->t_id_jenis_pelayanan == 6) {
    //                 $objekArray = Op::selectRaw(
    //                     "t_op.*,
    //             t_detail_bangunan.t_id_detail_bangunan,
    //             t_detail_bangunan.t_jenis_penggunaan_bangunan,
    //             t_detail_bangunan.t_tahun_bangunan,
    //             t_detail_bangunan.t_tahun_renovasi,
    //             t_detail_bangunan.t_kondisi_bangunan,
    //             t_detail_bangunan.t_konstruksi,
    //             t_detail_bangunan.t_atap,
    //             t_detail_bangunan.t_dinding,
    //             t_detail_bangunan.t_lantai,
    //             t_detail_bangunan.t_langit_langit,
    //             t_detail_bangunan.t_ac_split,
    //             t_detail_bangunan.t_ac_window,
    //             t_detail_bangunan.t_panjang_pagar,
    //             t_detail_bangunan.t_bahan_pagar,
    //             t_detail_bangunan.t_jumlah_lantai,
    //             t_detail_bangunan.t_listrik,
    //             t_detail_bangunan.t_luas,
    //             t_detail_bangunan.t_no_urut_bangunan,
    //             t_detail_bangunan.created_by,
    //             t_detail_bangunan.created_at,
    //             t_detail_bangunan.updated_at,
    //             t_detail_bangunan.t_id_op"
    //                 )
    //                     ->Where('t_op.t_id_pelayanan', $pelayanan->t_id_pelayanan)->leftJoin('t_detail_bangunan', 't_op.t_id_pelayanan', '=', 't_detail_bangunan.t_id_pelayanan')->get();
    //             } else {
    //                 $objekArray = null;
    //             }
    //             $uuid = Uuid::uuid4();
    //             $urlQr = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . "/dokumen/informasi/" . $uuid;

    //             $imgQr = QrCode::format('png')
    //                 ->size(100)
    //                 ->margin(2)
    //                 ->generate($urlQr);

    //             $fileName = $uuid . '.png';
    //             $filePathQr = 'public/' . $fileName;
    //             Storage::disk('local')->put($filePathQr, $imgQr);
    //             $filePathQr2 = public_path() . '/storage/' . $fileName;

    //             $pdf = Pdf::setOption(['margin_top' => 5, 'margin_bottom' => 5, 'margin_left' => 5, 'margin_right' => 5])
    //                 ->setPaper('legal', 'potrait')
    //                 ->loadView('validasi.exports.hasil-validasi', [
    //                     'pemda' => PemdaModel::first(),
    //                     'pelayanan' => $pelayanan->load('jenisPajak')
    //                         ->load('jenisPelayanan')
    //                         ->load('wajibPajak')
    //                         ->load('objekPajak')
    //                         ->load('pelayananAprove'),
    //                     'validasi' => Validasi::with('statusValidasi')->where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first(),
    //                     'pejabat' => $ttd,
    //                     'tte' => 1,
    //                     'objekArray' => $objekArray

    //                 ]);

    //             $pdfName = $value . '.pdf';
    //             $pdf->save(storage_path('app/public/' . $pdfName));

    //             $fileoutput = 'app/public/' . $pdfName;

    //             $pdfSave = $uuid . '.pdf';
    //             $filesave = 'public/' . $pdfSave;

    //             $esign = $this->signed($fileoutput, $filesave, $ttd->s_nik, $request->passphrase, $urlQr, 80, 80, $filePathQr);
    //             Storage::put($filesave, $esign);

    //             $imgQrContent = file_get_contents($filePathQr2);
    //             $imgQr = base64_encode($imgQrContent);

    //             $filePath0 = public_path() . '/storage/' . $value . '.pdf';
    //             $pdfContent0 = file_get_contents($filePath0);
    //             $unsigned64 = base64_encode($pdfContent0);

    //             $filePath = public_path() . '/storage/' . $uuid . '.pdf';
    //             $pdfContent = file_get_contents($filePath);
    //             $signed64 = base64_encode($pdfContent);

    //             $savedEsign = Esign::where('t_id_pelayanan', $value)->first();
    //             $data['t_id_pelayanan'] = $value;
    //             $data['t_id_pejabat'] = $ttd->s_id_pegawai;
    //             $data['created_by'] = Auth::user()->id;
    //             $data['imgQr'] = 'data:image/png;base64,' . $imgQr;
    //             $data['file'] = 'data:application/pdf;base64,' . $unsigned64;
    //             $data['signed'] = 'data:application/pdf;base64,' . $signed64;

    //             if ($savedEsign) {
    //                 $savedEsign->update($data);
    //             } else {
    //                 $data['uuid'] = $uuid;
    //                 Esign::create($data);
    //             }

    //             if (file_exists($filePathQr2)) {
    //                 unlink($filePathQr2);
    //             }

    //             if (file_exists($filePath0)) {
    //                 unlink($filePath0);
    //             }

    //             if (file_exists($filePath)) {
    //                 unlink($filePath);
    //             }

    //             return redirect('esign-tte')->with('success', 'Data berhasil disimpan');
    //         }
    //     } catch (\Throwable $th) {
    //         dd($th);
    //         throw $th;
    //         return redirect('esign-tte')->with('errors', 'Terjadi Kesalahan');
    //     }
    // }

    public function create(Request $request)
    {
        try {
            if ($request->nop == null || $request->nop == 0) {
                return redirect('esign-tte')->with('errors', 'Pilih minimal 1 NOP');
            }
            foreach ($request->nop as $key => $value) {
                $ar_pemda = PemdaModel::orderBy('updated_at', 'desc')->first();
                $ar_blanko = Blanko::where('blanko', 1)->first();
                $nop = substr(str_replace('.', '', $value), 0, 18);
                $tahun = substr(str_replace('.', '', $value), -4);

                $data = (new TunggakanView())->getNopTahun($nop, $tahun);
                $tunggakan = (new TunggakanView())->getTunggakanNop($nop, $tahun);
                $pejabatTtd = (new PegawaiPbb())->getPejabat();
                $pegawai = Pegawai::where('s_id_pegawai', $request->s_id_pegawai)->first();

                $refUmum = RefUmum::where('KD_REF', 24)->first();
                $codingSppt = (new Sppt())->getCodingsppt($nop, $tahun, $data);
                $angkaKontrol = (new Sppt())->getAngkaKontrol($nop, $data->siklus_sppt, $tahun);

                $uuid = Uuid::uuid4();
                $urlQr = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . "/dokumen/informasi/" . $uuid;

                $imgQr = QrCode::format('png')
                    ->size(100)
                    ->margin(2)
                    ->generate($urlQr);

                $fileName = $uuid . '.png';
                $filePathQr = 'public/' . $fileName;
                Storage::disk('local')->put($filePathQr, $imgQr);
                $filePathQr2 = public_path() . '/storage/' . $fileName;

                $pdf = Pdf::loadView('penetapan.exports.cetaksppt', [
                    'data' => $data,
                    'pejabat' => $pejabatTtd,
                    'refUmum' => $refUmum,
                    'tunggakan' => $tunggakan,
                    'pemda' => $ar_pemda,
                    'nop' => $nop,
                    'blanko' => $ar_blanko,
                    'codingsppt' => $codingSppt[0]->codingsppt,
                    'angkakontrol' => $angkaKontrol[0]->angka_kontrol,
                    'terbilang' => $this->terbilang($data->pbb_yg_harus_dibayar_sppt),
                    'bulan_tempo' => $this->bulan(date('m', strtotime($data->tgl_jatuh_tempo_sppt))),
                    'bulan_terbit' => $this->bulan(date('m', strtotime($data->tgl_terbit_sppt)))
                    // 'pejabat' => $ttd,
                    // 'tte' => 1,
                    // 'objekArray' => $objekArray

                ]);
                // return $pdf->stream(Uuid::uuid4() . 'pdf');

                $pdfName = $value . '.pdf';
                $pdf->save(storage_path('app/public/' . $pdfName));

                $fileoutput = 'app/public/' . $pdfName;

                $pdfSave = $uuid . '.pdf';
                $filesave = 'public/' . $pdfSave;

                $esign = $this->signed($fileoutput, $filesave, $pegawai->s_nik, $request->passphrase, $urlQr, 80, 80, $filePathQr);
                Storage::put($filesave, $esign);

                $imgQrContent = file_get_contents($filePathQr2);
                $imgQr = base64_encode($imgQrContent);

                $filePath0 = public_path() . '/storage/' . $value . '.pdf';
                $pdfContent0 = file_get_contents($filePath0);
                $unsigned64 = base64_encode($pdfContent0);

                $filePath = public_path() . '/storage/' . $uuid . '.pdf';
                $pdfContent = file_get_contents($filePath);
                $signed64 = base64_encode($pdfContent);

                $savedEsign = Esign_penetapan::where('t_nop', $value)->first();
                $datapenetapan['t_nop'] = $value;
                $datapenetapan['t_id_pejabat'] = $pegawai->s_id_pegawai;
                $datapenetapan['created_by'] = Auth::user()->id;
                $datapenetapan['imgQr'] = 'data:image/png;base64,' . $imgQr;
                $datapenetapan['file'] = 'data:application/pdf;base64,' . $unsigned64;
                $datapenetapan['signed'] = 'data:application/pdf;base64,' . $signed64;

                if ($savedEsign) {
                    $savedEsign->update($datapenetapan);
                } else {
                    $datapenetapan['uuid'] = $uuid;
                    Esign_penetapan::create($datapenetapan);
                }

                if (file_exists($filePathQr2)) {
                    unlink($filePathQr2);
                }

                if (file_exists($filePath0)) {
                    unlink($filePath0);
                }

                if (file_exists($filePath)) {
                    unlink($filePath);
                }

                return redirect('esign-tte')->with('success', 'Data berhasil disimpan');
            }
        } catch (\Throwable $th) {
            dd($th);
            throw $th;
            return redirect('esign-tte')->with('errors', 'Terjadi Kesalahan');
        }
    }
}
