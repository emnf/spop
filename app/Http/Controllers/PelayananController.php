<?php

namespace App\Http\Controllers;

use App\Exports\PelayananExport;
use App\Models\Pbb\LookupItem;
use App\Models\Setting\PemdaModel;
use App\Models\Setting\JenisPajak;
use App\Models\Setting\JenisPelayanan;
use App\Models\Pelayanan\Pelayanan;
use App\Helpers\DatagridHelpers;
use App\Helpers\PelayananHelper;
use App\Http\Requests\Pelayanan\StoreKeberatanPajakRequest;
use App\Http\Traits\Pelayanan\DetailBangunanTrait;
use App\Http\Traits\Pelayanan\ObjekPajakTrait;
use App\Http\Traits\Pelayanan\ObjekPajakLamaTrait;
use App\Http\Traits\Pelayanan\PelayananApproveTrait;
use App\Http\Traits\Pelayanan\PelayananTrait;
use App\Http\Traits\Pelayanan\WajibPajakTrait;
use App\Http\Traits\Pelayanan\WajibPajakLamaTrait;
use App\Models\Pbb\DafnomOp;
use App\Models\Pbb\DatFasilitasBangunan;
use Throwable;

use App\Models\Pbb\DatObjekPajak;
use App\Models\Pbb\DatOpBangunan;
use App\Models\Pbb\DatOpBumi;
use App\Models\Pbb\DatSubjekPajak;
use App\Models\Pbb\Kecamatan;
use App\Models\Pbb\Kelurahan;
use App\Models\Pbb\PegawaiPbb;
use App\Models\Pbb\RefDati;
use App\Models\Pbb\RefJpb;
use App\Models\Pbb\Sppt;
use App\Models\Pelayanan\DetailBangunan;
use App\Models\Pelayanan\PersyaratanPelayanan;
use App\Models\Pelayanan\Wp;
use App\Models\Setting\Persyaratan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

use App\Models\Pelayanan\Op;
use App\Models\Pelayanan\OpLama;
use App\Models\Pelayanan\WpLama;
use App\Models\Setting\Blanko;
use App\Service\NotificationService;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Queue\NullQueue;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Ramsey\Uuid\Uuid;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class PelayananController extends Controller
{
    use PelayananTrait, WajibPajakTrait, ObjekPajakTrait, ObjekPajakLamaTrait, WajibPajakLamaTrait;
    use PelayananApproveTrait, DetailBangunanTrait;

    public function index(Request $request)
    {

        $success = '';
        $warning = '';

        return view('pelayanan.index', [
            'pemda' => PemdaModel::first(),
            'jenispelayanan' => JenisPelayanan::isActive()->get(),
            'success' => $success,
            'warning' => $warning,
            'dataPajak' => JenisPajak::orderBy('s_id_jenis_pajak', 'desc')->get(),
        ]);
    }

    public function jenisLayanan(JenisPajak $jenisPajak)
    {
        return view('pelayanan.jenislayanan', [
            'pemda' => PemdaModel::first(),
            'jenisPajak' => $jenisPajak,
            'jenisLayanan' => JenisPelayanan::where('s_id_jenis_pajak', $jenisPajak->s_id_jenis_pajak)
                ->where('s_active', true)
                ->orderBy('s_id_jenis_pelayanan', 'asc')->get(),
        ]);
    }

    public function create(JenisPajak $jenisPajak, JenisPelayanan $jenisPelayanan)
    {
        $param = [
            'jenisPajak' => $jenisPajak,
            'jenisLayanan' => $jenisPelayanan,
            'pemda' => PemdaModel::first(),
            'lookupItem1' => LookupItem::where('KD_LOOKUP_GROUP', PelayananHelper::JENIS_BUMI)->get()
        ];

        if ($jenisPajak->s_id_jenis_pajak == 10 && $jenisPelayanan->s_id_jenis_pelayanan == 1) {
            $param['lookupItem2'] = LookupItem::where('KD_LOOKUP_GROUP', PelayananHelper::KETERANGAN_OP)->get();
        }

        if ($jenisPajak->s_id_jenis_pajak == 10 && in_array($jenisPelayanan->s_id_jenis_pelayanan, [2, 6])) {
            $dataPelayanan = (new Pelayanan())->load('WpLama', 'OpLama');
            $param['pelayanan'] = $dataPelayanan;
        }

        if ($jenisPajak->s_id_jenis_pajak == 10 && in_array($jenisPelayanan->s_id_jenis_pelayanan, [3, 22])) {
            $param['kecamatan'] = Kecamatan::get()->toArray();
        }

        $dati = RefDati::first();
        $param['kdKabkota'] = $dati->kode_kabkota;

        return view('pelayanan.tambah', $param);
    }

    public function edit(Pelayanan $pelayanan)
    {
        $pelayanan->load('jenisPajak')->load('detailBangunan');

        $param = [
            'jenisPajak' => $pelayanan->jenisPajak,
            'jenisLayanan' => JenisPelayanan::where('s_id_jenis_pelayanan', $pelayanan->t_id_jenis_pelayanan)->first(),
            'pemda' => PemdaModel::first(),
            'pelayanan' => $pelayanan,
            'objek_pajak' => Op::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first(),
            'wajib_pajak' => Wp::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first(),
            'lookupItem1' => LookupItem::where('KD_LOOKUP_GROUP', PelayananHelper::JENIS_BUMI)->get()
        ];

        if ($pelayanan->t_id_jenis_pajak == 10 && $pelayanan->t_id_jenis_pelayanan == 1) {
            $param['lookupItem2'] = LookupItem::where('KD_LOOKUP_GROUP', PelayananHelper::KETERANGAN_OP)->get();
        }

        if ($pelayanan->t_id_jenis_pajak == 10 && in_array($pelayanan->t_id_jenis_pelayanan, [2, 4])) {
            $pelayanan->WpLama = WpLama::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first();
            $pelayanan->OPLama = OpLama::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first();
            $pelayanan->Wp = Wp::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first();
            $pelayanan->Op = Op::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first();
            $pelayanan->nop = (implode(".", array(
                $pelayanan->OPLama->kd_propinsi,
                $pelayanan->OPLama->kd_dati2,
                $pelayanan->OPLama->kd_kecamatan,
                $pelayanan->OPLama->kd_kelurahan,
                $pelayanan->OPLama->kd_blok,
                $pelayanan->OPLama->no_urut,
                $pelayanan->OPLama->kd_jns_op,
            )));
        }

        if ($pelayanan->t_id_jenis_pajak == 10 && $pelayanan->t_id_jenis_pelayanan == 5) {

            $pelayanan->WpLama = WpLama::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first();
            $pelayanan->OPLama = OpLama::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first();

            $pelayanan->nop = (implode(".", array(
                $pelayanan->OPLama->kd_propinsi,
                $pelayanan->OPLama->kd_dati2,
                $pelayanan->OPLama->kd_kecamatan,
                $pelayanan->OPLama->kd_kelurahan,
                $pelayanan->OPLama->kd_blok,
                $pelayanan->OPLama->no_urut,
                $pelayanan->OPLama->kd_jns_op,
            )));

            $pelayanan->t_tahun_nop = $pelayanan->t_tahun_pajak;
        }

        if ($pelayanan->t_id_jenis_pajak == 10 && $pelayanan->t_id_jenis_pelayanan == 9) {
            $pelayanan->load('wajibPajak')->load('objekPajak');
        }

        if ($pelayanan->t_id_jenis_pajak == 10 && in_array($pelayanan->t_id_jenis_pelayanan, [3, 22])) {
            $param['kecamatan'] = Kecamatan::get()->toArray();
            // $param['lookupItem1'] = LookupItem::where('KD_LOOKUP_GROUP', PelayananHelper::JENIS_BUMI)->get();
        }
        $dati = RefDati::first();
        $param['kdKabkota'] = $dati->kode_kabkota;

        return view('pelayanan.tambah', $param);
    }

    public function datagrid(Request $request)
    {
        try {
            $response = (new Pelayanan())->datagrid($request);
            $dataArr = [];

            foreach ($response as $v) {

                $btnPengajuan = $this->cekSatatusPersyaratanPelayanan($v->t_id_pelayanan, $v->t_id_jenis_pelayanan);

                $editBtnActive = !is_null($v->t_id_pelayanan_approve) ? false : true;
                $delBtnActive = !is_null($v->t_id_pelayanan_approve) ? false : true;
                $uploadBtnActive = is_null($v->t_id_pelayanan_approve) ? true : false;

                // $spptBtnActive = !is_null($v->t_id_pelayanan_approve) ? true : false;

                if ($v->t_id_status_verifikasi == 2) {
                    $editBtnActive = true;
                    $uploadBtnActive = true;
                    $delBtnActive = true;
                }

                $dataArr[] = [
                    't_jenispakjak' => '<strong class="text-blue text-bold">' . $v->nama_pajak . '</strong><br>',
                    't_jenislayanan' => '<strong class="text-red text-bold">' . $v->nama_jenis . '</strong><br>',
                    't_tgl_pendaftaran' => date('d-m-Y', strtotime($v->t_tgl_pelayanan)),
                    't_status' => $this->statusPelayanan($v),
                    't_nama_pemohon' => $v->t_nama_pemohon,
                    't_alasan' => $v->t_keterangan,
                    'pengajuan' => $v->t_kode_bayar_bphtb != null ? 'Dari BPHTB' : $v->name,
                    'actionList' => [
                        [
                            'actionName' => 'hapus-pelayanan',
                            'actionUrl' => "javascript:showModalHapusPelayanan('" . $v->t_id_pelayanan . "')",
                            'actionTitle' => 'Hapus Pelayanan',
                            'actionActive' => $delBtnActive
                        ],
                        [
                            'actionName' => 'edit-pelayanan',
                            'actionUrl' => "pelayanan/edit/" . $v->uuid,
                            'actionTitle' => 'Edit Pelayanan',
                            'actionActive' => $editBtnActive
                        ],
                        [
                            'actionName' => 'upload-pelayanan',
                            'actionUrl' => "pelayanan/upload/" . $v->uuid,
                            'actionTitle' => 'Upload File Persyaratan',
                            'actionActive' => $uploadBtnActive
                        ],
                        [
                            'actionName' => 'tanda-terima-pelayanan',
                            'actionUrl' => "javascript:showModalCetakTtp('" . $v->t_id_pelayanan . "')",
                            'actionTitle' => 'Cetak tanda terima Pelayanan',
                            'actionActive' => is_null($v->t_id_pelayanan_approve) ? false : true
                        ],
                        [
                            'actionName' => 'ajukan-pelayanan',
                            'actionUrl' => "pelayanan/pengajuan/" . $v->uuid,
                            'actionTitle' => 'Ajukan Pelayanan',
                            'actionActive' => !is_null($v->t_id_pelayanan_approve) ? false : $btnPengajuan
                        ],
                        [
                            'actionName' => 'cetak-spop',
                            'actionUrl' => "pelayanan/cetak-spop/" . $v->uuid,
                            'actionTitle' => 'Cetak Spop Lspop',
                            'actionActive' => ($v->t_id_jenis_pelayanan == 1) && ($v->t_id_status_validasi == 4) ? true : false
                        ],
                        [
                            'actionName' => 'cetak-sppt',
                            'actionUrl' => "pelayanan/sppt/" . $v->uuid,
                            'actionTitle' => 'Cetak sppt',
                            'actionActive' => is_null($v->t_id_pelayanan_approve) ? false : true
                        ],

                    ]
                ];
            }
            $totalPages = (int)ceil($response->total() / $response->perPage());
            // dd($totalPages);
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $response);
        } catch (Throwable $e) {
            report($e);
            return (new DatagridHelpers)->getErrorResponse();
        }
    }

    public function sppt($uuid)
    {
        try {
            $pelayanan = Pelayanan::whereUuid($uuid)->first();
            $op = Op::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->get();

            $sppt = [];
            foreach ($op as $key => $v) {
                // $datasppt = Sppt::where([
                //     'kd_propinsi' => $v->kd_propinsi,
                //     'kd_dati2' => $v->kd_dati2,
                //     'kd_kecamatan' => $v->kd_kecamatan,
                //     'kd_kelurahan' => $v->kd_kelurahan,
                //     'kd_blok' => $v->kd_blok,
                //     'no_urut' => $v->no_urut,
                //     'kd_jns_op' => $v->kd_jns_op,
                // ])->orderby('thn_pajak_sppt', 'desc')->first();

                $nop['KD_PROPINSI'] = $v->kd_propinsi;
                $nop['KD_DATI2'] = $v->kd_dati2;
                $nop['KD_KECAMATAN'] = $v->kd_kecamatan;
                $nop['KD_KELURAHAN'] = $v->kd_kelurahan;
                $nop['KD_BLOK'] = $v->kd_blok;
                $nop['NO_URUT'] = $v->no_urut;
                $nop['KD_JNS_OP'] = $v->kd_jns_op;
                $tahun = date('Y', strtotime($pelayanan->t_tgl_pelayanan));
                $datasppt = (new Sppt())->getDataKetetapan($nop, $tahun);


                $codingsppt = (new Sppt())->getCodingsppt2($nop, $tahun, $datasppt);

                $datasppt->codingsppt = $codingsppt->codingsppt;

                // dd(count($datasppt));
                // epbb.blorakab.go.id/tagihanpbb?nop=33.16.140.011.003.0198.0
                $datasppt->url = 'epbb.blorakab.go.id/tagihanpbb?nop=' . (new PelayananHelper())->formatStringToNop(
                    $v->kd_propinsi .
                        $v->kd_dati2 .
                        $v->kd_kecamatan .
                        $v->kd_kelurahan .
                        $v->kd_blok .
                        $v->no_urut .
                        $v->kd_jns_op
                ) ?? null;
                $datasppt->urlqrcode = QrCode::size(70)->color(18, 66, 64)->generate('epbb.blorakab.go.id/tagihanpbb?nop=' . (new PelayananHelper())->formatStringToNop(
                    $v->kd_propinsi .
                        $v->kd_dati2 .
                        $v->kd_kecamatan .
                        $v->kd_kelurahan .
                        $v->kd_blok .
                        $v->no_urut .
                        $v->kd_jns_op
                )) ?? null;;
                $datasppt->urlqrcode2 = 'data:image/png;base64,' . base64_encode(QrCode::format('png')->size(80)->generate($datasppt->url));
                $datasppt->nop
                    = (new PelayananHelper())->formatStringToNop(
                        $v->kd_propinsi .
                            $v->kd_dati2 .
                            $v->kd_kecamatan .
                            $v->kd_kelurahan .
                            $v->kd_blok .
                            $v->no_urut .
                            $v->kd_jns_op
                    ) ?? null;;
                $sppt[] = $datasppt;
            }

            $pegawai = PegawaiPbb::where('NIP', '060000013')->first();

            // return view('pelayanan.exports.sppt', [
            //     'pemda' => PemdaModel::first(),
            //     'data' => $sppt,
            //     'pegawai' => $pegawai
            // ]);


            $pdf = Pdf::setOptions([
                'dpi' => 150,
                'margin_top' => 50, // Adjust as needed
                'margin_right' => 30, // Adjust as needed
                'margin_bottom' => 20, // Adjust as needed
                'margin_left' => 20, // Adjust as needed
            ])
                ->setPaper('a4', 'potrait')
                // ->setPaper([0, 0, 340, 400], 'portrait')
                ->loadView('pelayanan.exports.sppt', [
                    'pemda' => PemdaModel::first(),
                    'data' => $sppt,
                    'pegawai' => $pegawai,
                    'blanko' => Blanko::where('id', 1)->first()
                ]);

            return $pdf->stream(Uuid::uuid4() . '.pdf');
        } catch (\Throwable $e) {
            // dd($e);
            // report($e);
            // return (new DatagridHelpers)->getErrorResponse();
            echo "SPPT BELUM DITETAPKAN";
            exit;
        }
    }

    public function storeObjekBaru(Request $request)
    {
        $pelayanan = $this->storePelayanan($request);
        $wp = $this->storeWp($request, $pelayanan);
        $op = $this->storeOp($request, $pelayanan);

        $pelayananId = is_null($request->t_id_pelayanan)
            ? $pelayanan->t_id_pelayanan
            : $request->t_id_pelayanan;

        if ($request->t_jenis_tanah == PelayananHelper::TANAH_BANGUNAN) {
            return redirect()->route('pelayanan.bangunan-op', ['pelayanan' => $pelayananId]);
        }

        return redirect()->route('pelayanan.upload-persyaratan', ['pelayanan' => $pelayanan->uuid]);
    }

    public function createBangunan(Pelayanan $pelayanan)
    {
        $pelayanan->load('detailBangunan');
        if (in_array($pelayanan->t_id_jenis_pelayanan, [3, 22])) {
            $datbumi = OpLama::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)
                ->whereNotNull('t_luas_bangunan')
                ->orderBy('no_urut', 'asc')->first();
            $nopbng =
                $datbumi->kd_propinsi .
                $datbumi->kd_dati2 .
                $datbumi->kd_kecamatan .
                $datbumi->kd_kelurahan .
                $datbumi->kd_blok .
                $datbumi->no_urut .
                $datbumi->kd_jns_op;
            // dd($nopbng);
            $dataPbbPull = $this->dataPbbPull2($nopbng);
            // $pelayanan->detailBangunans = $dataPbbPull['detailBangunan'];
            // dd($pelayanan);
            $jpb = RefJpb::get();
            return view('pelayanan.pbb.mutasi-pecah.bangunan', [
                'pelayanan' => $pelayanan,
                'dataPbb' => $dataPbbPull,
                'jpb' => $jpb
            ]);
        }

        if ($pelayanan->t_id_jenis_pelayanan == 2) {
            $dataPbbPull = $this->dataPbbPull2($pelayanan->t_nop);
            $jpb = RefJpb::get();

            // $pelayanan['detailBangunan'] = $dataPbbPull['detailBangunan'] ?? null;
            // dd($jpb);
            return view('pelayanan.pbb.mutasi.bangunan', [
                'pelayanan' => $pelayanan,
                'dataPbb' => $dataPbbPull,
                'jpb' => $jpb
            ]);
        }

        return view('pelayanan.pbb.op-baru.bangunan', [
            'pelayanan' => $pelayanan
        ]);
    }

    public function storeBangunan(Request $request)
    {
        try {
            $this->createOrUpdate($request);
            session()->flash('success', 'Data berhasil disimpan');

            return redirect('pelayanan/bangunan/' . $request->idPelayanan);
        } catch (\Throwable $th) {
            report($th);
            session()->flash('error', 'Data gagal disimapan');
            return redirect()->back();
        }
    }

    public function getDetileBangunan(int $bangunanId)
    {
        try {
            return response()->json(["success" => true, "data" => DetailBangunan::find($bangunanId)], 200);
        } catch (\Throwable $th) {
            report($th);
            return response()->json(["success" => false, "message" => 'Terjadi masalah diserver!'], 200);
        }
    }

    public function pengajuan(Pelayanan $pelayanan)
    {
        if (
            !$this->cekSatatusPersyaratanPelayanan(
                $pelayanan->t_id_pelayanan,
                $pelayanan->t_id_jenis_pelayanan
            )
        ) {
            return redirect()->route(
                'pelayanan.upload-persyaratan',
                ['pelayanan' => $pelayanan->uuid]
            );
        }

        if ($pelayanan->pelayananAprove) {
            return redirect('pelayanan');
        }

        $pelayanan->load('objekPajak')
            ->load('wajibPajak')
            ->load('OpLama')
            ->load('WpLama')
            ->load('detailBangunan');

        $jenisPelayanan = JenisPelayanan::where('s_id_jenis_pelayanan', $pelayanan->t_id_jenis_pelayanan)->first();

        return view('pelayanan.pengajuan', [
            'pelayanan' => $pelayanan,
            'jenisPajak' => JenisPajak::where('s_id_jenis_pajak', $pelayanan->t_id_jenis_pajak)->first(),
            'jenisLayanan' => $jenisPelayanan,
            'perkiraanSelesai' => PelayananHelper::formatTglPerkiraanSelesaai(date('Y-m-d H:i:s'), $jenisPelayanan->s_waktu_pelayanan)
        ]);
    }

    public function ajukanPelayanan(Request $request)
    {
        $noUrutPelayanan = (int) Pelayanan::getMaxNoUrut(date('Y'));
        $pelayananApprove = $this->savePelayananApprove($request);
        $notifikasi = new NotificationService;

        if (!$pelayananApprove) {
            return redirect('pelayanan')->with('error', 'Data gagal disimpan!');
        }

        $pelayanan = Pelayanan::updateOrCreate(
            ['t_id_pelayanan' => $request->t_id_pelayanan],
            ['t_no_pelayanan' => $noUrutPelayanan + 1]
        );

        $notifikasi->notifyPendaftaran($pelayanan);

        return redirect('pelayanan')->with('success', 'Data berhasil disimpan!');;
    }

    public function uploadPersyaratan(Pelayanan $pelayanan)
    {
        if (!$pelayanan->pelayananAprove()->get()->isEmpty()) {
            if ($pelayanan->verifikasi()->pluck('t_id_status_verifikasi')->toArray()[0] != 2) {
                return redirect('pelayanan');
            }
        }

        if (in_array($pelayanan->t_id_jenis_pelayanan, [1, 2, 3, 22])) {
            foreach ($pelayanan->objekPajaks as $key => $objekPajak) {
                if (
                    $objekPajak->t_jenis_tanah == PelayananHelper::TANAH_BANGUNAN
                    && count($objekPajak->detailBangunan) < 1
                ) {

                    return redirect()->route('pelayanan.bangunan-op', ['pelayanan' => $pelayanan->t_id_pelayanan]);
                }
            }
        }

        $noPelayanan = is_null($pelayanan->t_nourut)
            ? PelayananHelper::formatNoUrut($pelayanan->t_tgl_pelayanan, $pelayanan->t_id_jenis_pajak)
            : PelayananHelper::formatNoUrut($pelayanan->t_tgl_pelayanan, $pelayanan->t_id_jenis_pajak, $pelayanan->t_nourut);

        return view('pelayanan.upload', [
            'data' => $pelayanan,
            'data_pelayanan' => JenisPelayanan::find($pelayanan->t_id_jenis_pelayanan),
            'data_pajak' => JenisPajak::find($pelayanan->t_id_jenis_pajak),
            'no_pelayanan' => $noPelayanan,
            'persyaratan' => Persyaratan::where('s_id_jenis_pelayanan', $pelayanan->t_id_jenis_pelayanan)->get()
        ]);
    }

    public function getUploadPersyaratan(Pelayanan $pelayanan, Request $request)
    {
        try {
            // $data = PersyaratanPelayanan::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->get();
            $data = PersyaratanPelayanan::join('s_persyaratan', 't_persyaratan.s_id_persyaratan', '=', 's_persyaratan.s_id_persyaratan')->where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->get();

            return response()->json($data, 200);
        } catch (\Throwable $th) {
            report($th);
            return response()->json(['success' => false, 'message' => 'Terjadi kesalahan'], 200);
        }
    }

    public function cekUser(Request $request)
    {
        $roleid = auth()->user()['roles'][0]['id'];
        $uploaded = PersyaratanPelayanan::where('t_id_persyaratan', $request->idpelayanan)->count();
        $res = 1;
        if ($roleid == 6 && $uploaded == 0) {
            $res = 0;
        }
        return response()->json($res, 200);
    }

    public function uploadMultiFile(Request $request)
    {
        $request->validate([
            "idPersyaratan" => 'required',
            "idPelayanan" => 'required',
            "files" => 'required|max:512000|mimes:jpeg,png,pdf'
        ]);
        $persyaratanPelayanan = new PersyaratanPelayanan;
        $persyaratan = Persyaratan::where('s_id_persyaratan', $request->idPersyaratan)->first();

        if ($request->hasFile('files')) {
            $file = $request->file('files');
            $fileStored = Storage::putFile('/persyaratan/download', $file);

            $data = [
                't_lokasi_file' => $fileStored,
                't_nama_file' => 'pelayanan/' . $file->hashName(),
                'created_by' => Auth::user()->id,
            ];

            $data['s_id_persyaratan'] = $request->idPersyaratan;
            $data['t_id_pelayanan'] = $request->idPelayanan;
            $data['t_nama_persyaratan'] = $persyaratan->s_nama_persyaratan;

            $syarat = $persyaratanPelayanan->create($data);

            return response()->json($syarat, 200);
        }

        return response()->json(null, 200);
    }

    public function deleteUploadFile(Request $request)
    {
        $fileSyarat = PersyaratanPelayanan::find($request->idUpload);
        unlink($fileSyarat->t_lokasi_file);

        PersyaratanPelayanan::where('t_id_persyaratan', $fileSyarat->t_id_persyaratan)->delete();
        return response()->json(['success' => true, 'message' => 'Data dihapus!'], 200);
    }

    public function cekLengkapSyarat(Pelayanan $pelayanan)
    {
        return response()->json(
            $this->cekSatatusPersyaratanPelayanan(
                $pelayanan->t_id_pelayanan,
                $pelayanan->t_id_jenis_pelayanan
            ),
            200
        );
    }

    public function checkNik(Request $request)
    {
        try {
            $pelayanan = Pelayanan::where('t_nik_pemohon', $request->t_nik_pemohon)->first();
            if (!$pelayanan) {
                return response()->json(['pemohon' => null], 200);
            }

            $response = array(
                'pemohon' => $pelayanan
            );

            return response()->json($response, 200);
        } catch (Throwable $e) {
            report($e);
            Log::error("ERROR_MESSAGE: " . $e);
        }
    }

    public function getDataPbb(Request $request)
    {
        $data = '';
        try {
            $nop = PelayananHelper::parseNop($request->nop);
            $dafnomOp = (new DafnomOp())->whereNop($request->nop)->where('kategori_op', '=', '3')->first();

            if (!$dafnomOp) {
                $data = (new DatObjekPajak)->getDataOpWpKecKel($nop);
                if (!isset($request->tahunPajak) && $request->tahunPajak == "") {
                    $data->tahunPajak = (new Sppt)->getTahunPajakTerahir($request->nop);
                } else {
                    $data->ketetapan = (new Sppt)->getTahunPajakTerahir($request->nop, $request->tahunPajak);
                }
            }
            // DatOpBangunan::getByNop($nop)->get()->toArray();
        } catch (Throwable $e) {
            Log::error("ERROR_MESSAGE: " . $e);
        }

        return response()->json(
            $data,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
        );
    }

    public function getDataSppt(Request $request)
    {
        $data = '';
        try {
            $nop = PelayananHelper::parseNop($request->nop);
            $dafnomOp = (new DafnomOp())->whereNop($request->nop)->where('kategori_op', '=', '3')->first();

            if (!$dafnomOp) {
                $data = (isset($request->tahunPajak))
                    ? (new Sppt)->getDataKetetapan($nop, $request->tahunPajak)
                    : (new Sppt)->getDataKetetapanByNop($nop);
            }
        } catch (Throwable $e) {
            Log::error("ERROR_MESSAGE: " . $e);
        }
        return response()->json(
            $data,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
        );
    }

    public function storeMutasiWp(Request $request)
    {
        $lunasPbb = PelayananTrait::cekWajibLunasPbb($request->t_id_jenis_pelayanan);
        if ($lunasPbb) {
            $tunggakan = $this->cekStatusTunggakanSppt($request->t_nop);
            if (count($tunggakan) > 0) {
                session()->flash('error', 'Permohonan tidak bisa diproses, masih ada tunggakan SPPT');
                return redirect()->back();
            }
        }

        DB::beginTransaction();
        try {
            // $jenisPelayanan = $request->all()['t_id_jenis_pelayanan'];
            $pelayanan = $this->storePelayanan($request);
            $request['nop'] = $request['t_nop'];
            $datapbb = $this->getDataPbb($request);

            $nop = PelayananHelper::parseNop($request->t_nop);
            $request['kd_propinsi'] = $nop['KD_PROPINSI'];
            $request['kd_dati2'] = $nop['KD_DATI2'];
            $request['kd_kecamatan'] = $nop['KD_KECAMATAN'];
            $request['kd_kelurahan'] = $nop['KD_KELURAHAN'];
            $request['kd_blok'] = $nop['KD_BLOK'];
            $request['no_urut'] = $nop['NO_URUT'];
            $request['kd_jns_op'] = $nop['KD_JNS_OP'];
            $request['kd_blok_lama'] = $nop['KD_BLOK'];
            $request['no_urut_lama'] = $nop['NO_URUT'];
            $request['kd_jns_op_lama'] = $nop['KD_JNS_OP'];

            $pelayananId = is_null($request->t_id_pelayanan)
                ? $pelayanan->t_id_pelayanan
                : $request->t_id_pelayanan;

            $wp = $this->storeWp($request, $pelayanan);
            $op = $this->storeOp($request, $pelayanan);
            $wpLama = $this->storeWpLama($request, $pelayanan);
            $opLama = $this->storeOpLama($request, $pelayanan);

            DB::commit();

            // dd($request->all());
            if ($request->t_jenis_tanah == PelayananHelper::TANAH_BANGUNAN) {
                $datOp = DatOpBangunan::where([
                    'KD_PROPINSI' => $nop['KD_PROPINSI'],
                    'KD_DATI2' => $nop['KD_DATI2'],
                    'KD_KECAMATAN' => $nop['KD_KECAMATAN'],
                    'KD_KELURAHAN' => $nop['KD_KELURAHAN'],
                    'KD_BLOK' => $nop['KD_BLOK'],
                    'NO_URUT' => $nop['NO_URUT'],
                    'KD_JNS_OP' => $nop['KD_JNS_OP'],
                    'KD_BLOK' => $nop['KD_BLOK'],
                    'NO_URUT' => $nop['NO_URUT'],
                    'KD_JNS_OP' => $nop['KD_JNS_OP']
                ])->first();

                $datFasilitaslistrik = DatFasilitasBangunan::where([
                    'KD_PROPINSI' => $nop['KD_PROPINSI'],
                    'KD_DATI2' => $nop['KD_DATI2'],
                    'KD_KECAMATAN' => $nop['KD_KECAMATAN'],
                    'KD_KELURAHAN' => $nop['KD_KELURAHAN'],
                    'KD_BLOK' => $nop['KD_BLOK'],
                    'NO_URUT' => $nop['NO_URUT'],
                    'KD_JNS_OP' => $nop['KD_JNS_OP'],
                    'KD_BLOK' => $nop['KD_BLOK'],
                    'NO_URUT' => $nop['NO_URUT'],
                    'KD_JNS_OP' => $nop['KD_JNS_OP'],
                    'KD_FASILITAS' => 44
                ])->first();
                $datFasilitasAcSplit = DatFasilitasBangunan::where([
                    'KD_PROPINSI' => $nop['KD_PROPINSI'],
                    'KD_DATI2' => $nop['KD_DATI2'],
                    'KD_KECAMATAN' => $nop['KD_KECAMATAN'],
                    'KD_KELURAHAN' => $nop['KD_KELURAHAN'],
                    'KD_BLOK' => $nop['KD_BLOK'],
                    'NO_URUT' => $nop['NO_URUT'],
                    'KD_JNS_OP' => $nop['KD_JNS_OP'],
                    'KD_BLOK' => $nop['KD_BLOK'],
                    'NO_URUT' => $nop['NO_URUT'],
                    'KD_JNS_OP' => $nop['KD_JNS_OP'],
                    'KD_FASILITAS' => 01
                ])->first();
                $datFasilitasAcWindows = DatFasilitasBangunan::where([
                    'KD_PROPINSI' => $nop['KD_PROPINSI'],
                    'KD_DATI2' => $nop['KD_DATI2'],
                    'KD_KECAMATAN' => $nop['KD_KECAMATAN'],
                    'KD_KELURAHAN' => $nop['KD_KELURAHAN'],
                    'KD_BLOK' => $nop['KD_BLOK'],
                    'NO_URUT' => $nop['NO_URUT'],
                    'KD_JNS_OP' => $nop['KD_JNS_OP'],
                    'KD_BLOK' => $nop['KD_BLOK'],
                    'NO_URUT' => $nop['NO_URUT'],
                    'KD_JNS_OP' => $nop['KD_JNS_OP'],
                    'KD_FASILITAS' => 02
                ])->first();
                $jpbBng = RefJpb::where("KD_JPB", $datOp->kd_jpb)->first();
                $kondisiBng = LookupItem::whereRaw("KD_LOOKUP_GROUP=?", 21)->whereRaw("KD_LOOKUP_ITEM=?", [strtoupper($datOp->kondisi_bng)])->first();
                $konstruksiBng = LookupItem::whereRaw("KD_LOOKUP_GROUP=?", 22)->whereRaw("KD_LOOKUP_ITEM=?", [strtoupper($datOp->jns_konstruksi_bng)])->first();
                $atapBng = LookupItem::whereRaw("KD_LOOKUP_GROUP=?", 41)->whereRaw("KD_LOOKUP_ITEM=?", [strtoupper($datOp->jns_atap_bng)])->first();
                $dindingBng = LookupItem::whereRaw("KD_LOOKUP_GROUP=?", 42)->whereRaw("KD_LOOKUP_ITEM=?", [strtoupper($datOp->kd_dinding)])->first();
                $lantaiBng = LookupItem::whereRaw("KD_LOOKUP_GROUP=?", 43)->whereRaw("KD_LOOKUP_ITEM=?", [strtoupper($datOp->kd_lantai)])->first();
                $langitBng = LookupItem::whereRaw("KD_LOOKUP_GROUP=?", 44)->whereRaw("KD_LOOKUP_ITEM=?", [strtoupper($datOp->kd_langit_langit)])->first();


                $dataBangunan = [
                    't_id_pelayanan' => is_null($request->t_id_pelayanan)
                        ? $pelayanan->t_id_pelayanan
                        : $request->t_id_pelayanan,
                    't_jenis_penggunaan_bangunan' => $jpbBng->nm_jpb,
                    't_tahun_bangunan' => $datOp->thn_dibangun_bng,
                    't_tahun_renovasi' => $datOp->thn_renovasi_bng,
                    't_kondisi_bangunan' => $kondisiBng->nm_lookup_item ?? null,
                    't_konstruksi' => $konstruksiBng->nm_lookup_item ?? null,
                    't_atap' => $atapBng->nm_lookup_item ?? null,
                    't_dinding' => $dindingBng->nm_lookup_item ?? null,
                    't_lantai' => $lantaiBng->nm_lookup_item ?? null,
                    't_langit_langit' => $langitBng->nm_lookup_item ?? null,
                    't_ac_split' => $datFasilitasAcSplit->jml_satuan ?? null,
                    't_ac_window' => $datFasilitasAcWindows->jml_satuan ?? null,
                    't_panjang_pagar' => null,
                    't_bahan_pagar' => null,
                    't_jumlah_lantai' => $datOp->jml_lantai_bng,
                    't_listrik' => $datFasilitaslistrik->jml_satuan ?? null,
                    'created_by' => auth()->user()->id,
                    't_id_op' => $op->t_id_op ?? null,
                    't_no_urut_bangunan' => $datOp->no_bng,
                    't_luas' => $datOp->luas_bng
                ];
                // dd($dataBangunan);
                $upd = DetailBangunan::updateOrCreate([
                    't_id_pelayanan' => $dataBangunan['t_id_pelayanan'],
                    't_no_urut_bangunan' => $dataBangunan['t_no_urut_bangunan'],
                ], $dataBangunan);
                return redirect()->route('pelayanan.bangunan-op', ['pelayanan' => $pelayananId]);
            }
            session()->flash('success', 'Berhasil disimpan!');
            return redirect()->route('pelayanan.upload-persyaratan', ['pelayanan' => $pelayanan->uuid]);
        } catch (\Throwable $th) {
            DB::rollBack();
            report("ERROR: " . $th);
            session()->flash('error', 'Terjadi kesalahan!');
            dd($th);
            return redirect()->back();
        }
    }

    public function storePembatalanSppt(Request $request)
    {
        $lunasPbb = PelayananTrait::cekWajibLunasPbb($request->t_id_jenis_pelayanan);
        if ($lunasPbb) {
            $tunggakan = $this->cekStatusTunggakanSppt($request->t_nop);
            if (count($tunggakan) > 0) {
                session()->flash('error', 'Permohonan tidak bisa diproses, masih ada tunggakan SPPT');
                return redirect()->back();
            }
        }

        DB::beginTransaction();

        try {
            $request['t_tahun_pajak'] = $request['t_tahun_nop'];
            $pelayanan = $this->storePelayanan($request);
            $request['nop'] = $request['t_nop'];

            $nop = PelayananHelper::parseNop($request['t_nop']);
            $request['kd_propinsi'] = $nop['KD_PROPINSI'];
            $request['kd_dati2'] = $nop['KD_DATI2'];
            $request['kd_kecamatan'] = $nop['KD_KECAMATAN'];
            $request['kd_kelurahan'] = $nop['KD_KELURAHAN'];
            $request['kd_blok'] = $nop['KD_BLOK'];
            $request['no_urut'] = $nop['NO_URUT'];
            $request['kd_jns_op'] = $nop['KD_JNS_OP'];
            $request['kd_blok_lama'] = $nop['KD_BLOK'];
            $request['no_urut_lama'] = $nop['NO_URUT'];
            $request['kd_jns_op_lama'] = $nop['KD_JNS_OP'];

            $getDataSppt = (new Sppt)->getDataKetetapan($nop, $request['t_tahun_nop']);

            $pelayananId = is_null($request->t_id_pelayanan)
                ? $pelayanan->t_id_pelayanan
                : $request->t_id_pelayanan;

            $dataOpLama = [
                "t_id_pelayanan" => $pelayananId,
                "kd_propinsi" => $getDataSppt->kd_propinsi,
                "kd_dati2" => $getDataSppt->kd_dati2,
                "kd_kecamatan" => $getDataSppt->kd_kecamatan,
                "kd_kelurahan" => $getDataSppt->kd_kelurahan,
                "kd_blok" => $getDataSppt->kd_blok,
                "kd_jns_op" => $getDataSppt->kd_jns_op,
                "no_urut" => $getDataSppt->no_urut,
                "t_jalan_op" => $getDataSppt->jalan_op,
                "t_rt_op" => $getDataSppt->rt_op,
                "t_rw_op" => $getDataSppt->rw_op,
                "t_kelurahan_op" => $getDataSppt->nm_kelurahan,
                "t_kecamatan_op" => $getDataSppt->nm_kecamatan,
                "t_luas_tanah" => $getDataSppt->total_luas_bumi,
                "t_luas_bangunan" => $getDataSppt->total_luas_bng,
                "t_longitude" => $getDataSppt->longitude ?? null,
                "t_latitude" => $getDataSppt->latitude ?? null,
                "created_by" => Auth::user()->id,
                "t_blok_kav" => $getDataSppt->blok_kav_no_op,
            ];

            if ($request->fasumCheckbox == 1) {
                $dataOpLama = [
                    "t_id_pelayanan" => $pelayananId,
                    "kd_propinsi" => $getDataSppt->kd_propinsi,
                    "kd_dati2" => $getDataSppt->kd_dati2,
                    "kd_kecamatan" => $getDataSppt->kd_kecamatan,
                    "kd_kelurahan" => $getDataSppt->kd_kelurahan,
                    "kd_blok" => $getDataSppt->kd_blok,
                    "kd_jns_op" => $getDataSppt->kd_jns_op,
                    "no_urut" => $getDataSppt->no_urut,
                    "t_jalan_op" => $getDataSppt->jalan_op,
                    "t_rt_op" => $getDataSppt->rt_op,
                    "t_rw_op" => $getDataSppt->rw_op,
                    "t_kelurahan_op" => $getDataSppt->nm_kelurahan,
                    "t_kecamatan_op" => $getDataSppt->nm_kecamatan,
                    "t_luas_tanah" => $getDataSppt->total_luas_bumi,
                    "t_luas_bangunan" => $getDataSppt->total_luas_bng,
                    "t_longitude" => $getDataSppt->longitude ?? null,
                    "t_latitude" => $getDataSppt->latitude ?? null,
                    "created_by" => Auth::user()->id,
                    "t_blok_kav" => $getDataSppt->blok_kav_no_op,
                    "jns_bumi" => 4
                ];
            }

            $dataWpLama = [
                "t_id_pelayanan" => $pelayananId,
                "t_nama_wp" => $getDataSppt->nm_wp_sppt,
                // "t_nop" => isset($request->t_nop) ? str_replace(".", "", $request->t_nop) : null,
                "t_nik_wp" => null,
                "t_jalan_wp" => $getDataSppt->jln_wp_sppt,
                "t_rt_wp" => $getDataSppt->rt_wp_sppt,
                "t_rw_wp" => $getDataSppt->rw_wp_sppt,
                "t_kelurahan_wp" => $getDataSppt->kelurahan_wp_sppt,
                "t_kecamatan_wp" => null,
                "t_kabupaten_wp" => $getDataSppt->kota_wp_sppt,
                "t_no_hp_wp" => $getDataSppt->telp_wp,
                "created_by" => Auth::user()->id,
                "t_blok_kav_wp" => $getDataSppt->blok_kav_no_wp_sppt
            ];

            if (is_null($request->t_id_wp)) {
                WpLama::create($dataWpLama);
                OpLama::create($dataOpLama);
                Op::create($dataOpLama);
                Wp::create($dataWpLama);
            } else {
                Wp::where('t_id_wp', $request->t_id_wp)->update($dataWpLama);
                WpLama::where('t_id_wp_lama', $request->t_id_wp)->update($dataWpLama);
                Op::where('t_id_op', $request->t_id_op)->update($dataOpLama);

                unset($dataOpLama["t_longitude"]);
                unset($dataOpLama["t_latitude"]);
                OpLama::where('t_id_op_lama', $request->t_id_op)->update($dataOpLama);
            }

            DB::commit();
            session()->flash('success', 'Berhasil disimpan!');

            return redirect()->route('pelayanan.upload-persyaratan', ['pelayanan' => $pelayanan->uuid]);
        } catch (\Throwable $th) {
            DB::rollBack();
            report("ERROR: " . $th);
            session()->flash('error', 'Terjadi kesalahan!');
            return redirect()->back();
        }
    }

    public function storePembetulanSppt(Request $request)
    {
        $lunasPbb = PelayananTrait::cekWajibLunasPbb($request->t_id_jenis_pelayanan);
        if ($lunasPbb) {
            $tunggakan = $this->cekStatusTunggakanSppt($request->t_nop);
            if (count($tunggakan) > 0) {
                session()->flash('error', 'Permohonan tidak bisa diproses, masih ada tunggakan SPPT');
                return redirect()->back();
            }
        }

        DB::beginTransaction();
        try {
            $nop = PelayananHelper::parseNop($request['t_nop']);
            $request['kd_propinsi'] = $nop['KD_PROPINSI'];
            $request['kd_dati2'] = $nop['KD_DATI2'];
            $request['kd_kecamatan'] = $nop['KD_KECAMATAN'];
            $request['kd_kelurahan'] = $nop['KD_KELURAHAN'];
            $request['kd_blok'] = $nop['KD_BLOK'];
            $request['no_urut'] = $nop['NO_URUT'];
            $request['kd_jns_op'] = $nop['KD_JNS_OP'];
            $request['kd_blok_lama'] = $nop['KD_BLOK'];
            $request['no_urut_lama'] = $nop['NO_URUT'];
            $request['kd_jns_op_lama'] = $nop['KD_JNS_OP'];

            $pelayanan = $this->storePelayanan($request);


            $pelayananId = is_null($request->t_id_pelayanan)
                ? $pelayanan->t_id_pelayanan
                : $request->t_id_pelayanan;
            // dd($pelayanan);
            $opBaru = $this->storeOp($request, $pelayanan);
            $wpBaru = $this->storeWp($request, $pelayanan);

            $opLama = $this->storeOpLama($request, $pelayanan);
            $wpLama = $this->storeWpLama($request, $pelayanan);

            if ($request->nomorBangunans) {
                foreach ($request->nomorBangunans as $key => $noBng) {
                    $detailBangunan = [
                        't_id_pelayanan' => $pelayananId,
                        't_luas' => $request->luasOpBangunan[$key],
                        't_no_urut_bangunan' => $noBng,
                        'created_by' => auth()->user()->id
                    ];

                    if (isset($request->idDetailBangunans[$key])) {
                        DetailBangunan::where('t_id_detail_bangunan', $request->idDetailBangunans[$key])->update($detailBangunan);
                    } else {
                        DetailBangunan::create($detailBangunan);
                    }
                }
            }

            DB::commit();
            session()->flash('success', 'Berhasil disimpan!');

            return redirect()->route('pelayanan.upload-persyaratan', ['pelayanan' => $pelayanan->uuid]);
        } catch (\Throwable $th) {
            dd($th);
            DB::rollBack();
            report("ERROR: " . $th);
            session()->flash('error', 'Terjadi kesalahan!');
            return redirect()->back();
        }
    }

    // public function storeKeberatanPajak(StoreKeberatanPajakRequest $request)
    public function storeKeberatanPajak(Request $request)
    {
        $lunasPbb = PelayananTrait::cekWajibLunasPbb($request->t_id_jenis_pelayanan);
        if ($lunasPbb) {
            $tunggakan = $this->cekStatusTunggakanSppt($request->t_nop);
            if (count($tunggakan) > 0) {
                session()->flash('error', 'Permohonan tidak bisa diproses, masih ada tunggakan SPPT');
                return redirect()->back();
            }
        }

        DB::beginTransaction();
        try {
            $nop = PelayananHelper::parseNop($request['t_nop']);
            $getDataSppt = (new Sppt)->getDataKetetapan($nop, $request['t_tahun_nop']);

            $pelayanan = $this->storePelayanan($request);

            $pelayananId = is_null($request->t_id_pelayanan)
                ? $pelayanan->t_id_pelayanan
                : $request->t_id_pelayanan;

            $dataOp = [
                "t_id_pelayanan" => $pelayananId,
                "kd_propinsi" => $getDataSppt->kd_propinsi,
                "kd_dati2" => $getDataSppt->kd_dati2,
                "kd_kecamatan" => $getDataSppt->kd_kecamatan,
                "kd_kelurahan" => $getDataSppt->kd_kelurahan,
                "kd_blok" => $getDataSppt->kd_blok,
                "kd_jns_op" => $getDataSppt->kd_jns_op,
                "no_urut" => $getDataSppt->no_urut,
                "t_jalan_op" => $getDataSppt->jalan_op,
                "t_rt_op" => $getDataSppt->rt_op,
                "t_rw_op" => $getDataSppt->rw_op,
                "t_kelurahan_op" => $getDataSppt->nm_kelurahan,
                "t_kecamatan_op" => $getDataSppt->nm_kecamatan,
                "t_luas_tanah" => $getDataSppt->total_luas_bumi,
                "t_luas_bangunan" => $getDataSppt->total_luas_bng,
                // "t_longitude" => $getDataSppt->longitude ?? null,
                // "t_latitude" => $getDataSppt->latitude ?? null,
                "created_by" => Auth::user()->id,
                "t_blok_kav" => $getDataSppt->blok_kav_no_op,
            ];

            $dataWp = [
                "t_id_pelayanan" => $pelayananId,
                "t_nama_wp" => $getDataSppt->nm_wp_sppt,
                "t_nik_wp" => null,
                "t_jalan_wp" => $getDataSppt->jln_wp_sppt,
                "t_rt_wp" => $getDataSppt->rt_wp_sppt,
                "t_rw_wp" => $getDataSppt->rw_wp_sppt,
                "t_kelurahan_wp" => $getDataSppt->kelurahan_wp_sppt,
                "t_kecamatan_wp" => null,
                "t_kabupaten_wp" => $getDataSppt->kota_wp_sppt,
                "t_no_hp_wp" => $getDataSppt->telp_wp,
                "created_by" => Auth::user()->id,
                "t_blok_kav_wp" => $getDataSppt->blok_kav_no_wp_sppt
            ];

            if (is_null($request->t_id_wp)) {
                Op::create($dataOp);
                Wp::create($dataWp);
            } else {
                Wp::where('t_id_wp', $request->t_id_wp)->update($dataWp);
                Op::where('t_id_op', $request->t_id_op)->update($dataOp);
            }

            DB::commit();
            return redirect()->route('pelayanan.upload-persyaratan', ['pelayanan' => $pelayanan->uuid])->with('success', 'Data permohonan berhasil disimpan');
        } catch (\Throwable $th) {
            DB::rollBack();
            report($th);
            return redirect()->back()->with('error', 'Terjadi kesalahan!');
        }
    }

    public function storeSalinanSppt(Request $request)
    {
        $lunasPbb = PelayananTrait::cekWajibLunasPbb($request->t_id_jenis_pelayanan);
        if ($lunasPbb) {
            $tunggakan = $this->cekStatusTunggakanSppt($request->t_nop);
            if (count($tunggakan) > 0) {
                session()->flash('error', 'Permohonan tidak bisa diproses, masih ada tunggakan SPPT');
                return redirect()->back();
            }
        }

        DB::beginTransaction();
        try {
            $nop = PelayananHelper::parseNop($request['t_nop']);
            $request['kd_propinsi'] = $nop['KD_PROPINSI'];
            $request['kd_dati2'] = $nop['KD_DATI2'];
            $request['kd_kecamatan'] = $nop['KD_KECAMATAN'];
            $request['kd_kelurahan'] = $nop['KD_KELURAHAN'];
            $request['kd_blok'] = $nop['KD_BLOK'];
            $request['no_urut'] = $nop['NO_URUT'];
            $request['kd_jns_op'] = $nop['KD_JNS_OP'];

            $request['t_multiple_tahun'] = join(',', $request->tahunpajakBox);

            $getDataSppt = (new Sppt)->getDataKetetapanByNop($nop)->first();

            $pelayanan = $this->storePelayanan($request);

            $pelayananId = is_null($request->t_id_pelayanan)
                ? $pelayanan->t_id_pelayanan
                : $request->t_id_pelayanan;

            $dataOpLama = [
                "t_id_pelayanan" => $pelayananId,
                "kd_propinsi" => $getDataSppt->kd_propinsi,
                "kd_dati2" => $getDataSppt->kd_dati2,
                "kd_kecamatan" => $getDataSppt->kd_kecamatan,
                "kd_kelurahan" => $getDataSppt->kd_kelurahan,
                "kd_blok" => $getDataSppt->kd_blok,
                "kd_jns_op" => $getDataSppt->kd_jns_op,
                "no_urut" => $getDataSppt->no_urut,
                "t_jalan_op" => $getDataSppt->jalan_op,
                "t_rt_op" => $getDataSppt->rt_op ?? '000',
                "t_rw_op" => $getDataSppt->rw_op ?? '00',
                "t_kelurahan_op" => $getDataSppt->nm_kelurahan,
                "t_kecamatan_op" => $getDataSppt->nm_kecamatan,
                "t_luas_tanah" => $getDataSppt->total_luas_bumi,
                "t_luas_bangunan" => $getDataSppt->total_luas_bng,
                // "t_longitude" => $getDataSppt->longitude ?? null,
                // "t_latitude" => $getDataSppt->latitude ?? null,
                "created_by" => Auth::user()->id,
                "t_blok_kav" => $getDataSppt->blok_kav_no_op,
            ];

            $dataWpLama = [
                "t_id_pelayanan" => $pelayananId,
                "t_nama_wp" => $getDataSppt->nm_wp_sppt,
                // "t_nop" => isset($request->t_nop) ? str_replace(".", "", $request->t_nop) : null,
                "t_nik_wp" => null,
                "t_jalan_wp" => $getDataSppt->jln_wp_sppt,
                "t_rt_wp" => $getDataSppt->rt_wp_sppt ?? '000',
                "t_rw_wp" => $getDataSppt->rw_wp_sppt ?? '00',
                "t_kelurahan_wp" => $getDataSppt->kelurahan_wp_sppt,
                "t_kecamatan_wp" => null,
                "t_kabupaten_wp" => $getDataSppt->kota_wp_sppt,
                "t_no_hp_wp" => $getDataSppt->telp_wp,
                "created_by" => Auth::user()->id,
                "t_blok_kav_wp" => $getDataSppt->blok_kav_no_wp_sppt
            ];

            if (is_null($request->t_id_wp)) {
                WpLama::create($dataWpLama);
                OpLama::create($dataOpLama);
                Op::create($dataOpLama);
                Wp::create($dataWpLama);
            } else {
                Wp::where('t_id_wp', $request->t_id_wp)->update($dataWpLama);
                WpLama::where('t_id_wp_lama', $request->t_id_wp)->update($dataWpLama);
                Op::where('t_id_op', $request->t_id_op)->update($dataOpLama);

                unset($dataOpLama["t_longitude"]);
                unset($dataOpLama["t_latitude"]);
                OpLama::where('t_id_op_lama', $request->t_id_op)->update($dataOpLama);
            }

            DB::commit();
            session()->flash('success', 'Berhasil disimpan!');

            return redirect()->route('pelayanan.upload-persyaratan', ['pelayanan' => $pelayanan->uuid]);
        } catch (\Throwable $th) {
            DB::rollBack();
            report("ERROR: " . $th);
            session()->flash('error', 'Terjadi kesalahan!');
            return redirect()->back();
        }
    }

    public function storePenguranganKetetapan(Request $request)
    {
        $lunasPbb = PelayananTrait::cekWajibLunasPbb($request->t_id_jenis_pelayanan);
        if ($lunasPbb == 'true') {

            $tunggakan = $this->cekStatusTunggakanSppt($request->t_nop);
            if (count($tunggakan) > 0) {
                session()->flash('error', 'Permohonan tidak bisa diproses, masih ada tunggakan SPPT');
                return redirect()->back();
            }
        }
        DB::beginTransaction();
        try {
            $nop = PelayananHelper::parseNop($request['t_nop']);
            $getDataSppt = (new Sppt)->getDataKetetapan($nop, $request['t_tahun_nop']);

            $pelayanan = $this->storePelayanan($request);

            $pelayananId = is_null($request->t_id_pelayanan)
                ? $pelayanan->t_id_pelayanan
                : $request->t_id_pelayanan;

            $dataOp = [
                "t_id_pelayanan" => $pelayananId,
                "kd_propinsi" => $getDataSppt->kd_propinsi,
                "kd_dati2" => $getDataSppt->kd_dati2,
                "kd_kecamatan" => $getDataSppt->kd_kecamatan,
                "kd_kelurahan" => $getDataSppt->kd_kelurahan,
                "kd_blok" => $getDataSppt->kd_blok,
                "kd_jns_op" => $getDataSppt->kd_jns_op,
                "no_urut" => $getDataSppt->no_urut,
                "t_jalan_op" => $getDataSppt->jalan_op,
                "t_rt_op" => $getDataSppt->rt_op,
                "t_rw_op" => $getDataSppt->rw_op,
                "t_kelurahan_op" => $getDataSppt->nm_kelurahan,
                "t_kecamatan_op" => $getDataSppt->nm_kecamatan,
                "t_luas_tanah" => $getDataSppt->total_luas_bumi,
                "t_luas_bangunan" => $getDataSppt->total_luas_bng,
                // "t_longitude" => $getDataSppt->longitude ?? null,
                // "t_latitude" => $getDataSppt->latitude ?? null,
                "created_by" => Auth::user()->id,
                "t_blok_kav" => $getDataSppt->blok_kav_no_op,
            ];

            $dataWp = [
                "t_id_pelayanan" => $pelayananId,
                "t_nama_wp" => $getDataSppt->nm_wp_sppt,
                "t_nik_wp" => null,
                "t_jalan_wp" => $getDataSppt->jln_wp_sppt,
                "t_rt_wp" => $getDataSppt->rt_wp_sppt,
                "t_rw_wp" => $getDataSppt->rw_wp_sppt,
                "t_kelurahan_wp" => $getDataSppt->kelurahan_wp_sppt,
                "t_kecamatan_wp" => null,
                "t_kabupaten_wp" => $getDataSppt->kota_wp_sppt,
                "t_no_hp_wp" => $getDataSppt->telp_wp,
                "created_by" => Auth::user()->id,
                "t_blok_kav_wp" => $getDataSppt->blok_kav_no_wp_sppt
            ];

            if (is_null($request->t_id_wp)) {
                Op::create($dataOp);
                Wp::create($dataWp);
            } else {
                Wp::where('t_id_wp', $request->t_id_wp)->update($dataWp);
                Op::where('t_id_op', $request->t_id_op)->update($dataOp);
            }

            DB::commit();
            return redirect()->route('pelayanan.upload-persyaratan', ['pelayanan' => $pelayanan->uuid])->with('success', 'Data permohonan berhasil disimpan');
        } catch (\Throwable $th) {
            DB::rollBack();
            report($th);
            return redirect()->route('pelayanan')->with('error', 'Terjadi kesalahan!');
        }
    }

    public function storePenentuanJatuhTempo(Request $request)
    {
        $lunasPbb = PelayananTrait::cekWajibLunasPbb($request->t_id_jenis_pelayanan);
        if ($lunasPbb) {
            $tunggakan = $this->cekStatusTunggakanSppt($request->t_nop);
            if (count($tunggakan) > 0) {
                session()->flash('error', 'Permohonan tidak bisa diproses, masih ada tunggakan SPPT');
                return redirect()->back();
            }
        }

        DB::beginTransaction();
        try {
            $nop = PelayananHelper::parseNop($request['t_nop']);
            $getDataSppt = (new Sppt)->getDataKetetapan($nop, $request['t_tahun_nop']);

            $pelayanan = $this->storePelayanan($request);

            $pelayananId = is_null($request->t_id_pelayanan)
                ? $pelayanan->t_id_pelayanan
                : $request->t_id_pelayanan;

            $dataOp = [
                "t_id_pelayanan" => $pelayananId,
                "kd_propinsi" => $getDataSppt->kd_propinsi,
                "kd_dati2" => $getDataSppt->kd_dati2,
                "kd_kecamatan" => $getDataSppt->kd_kecamatan,
                "kd_kelurahan" => $getDataSppt->kd_kelurahan,
                "kd_blok" => $getDataSppt->kd_blok,
                "kd_jns_op" => $getDataSppt->kd_jns_op,
                "no_urut" => $getDataSppt->no_urut,
                "t_jalan_op" => $getDataSppt->jalan_op,
                "t_rt_op" => $getDataSppt->rt_op,
                "t_rw_op" => $getDataSppt->rw_op,
                "t_kelurahan_op" => $getDataSppt->nm_kelurahan,
                "t_kecamatan_op" => $getDataSppt->nm_kecamatan,
                "t_luas_tanah" => $getDataSppt->total_luas_bumi,
                "t_luas_bangunan" => $getDataSppt->total_luas_bng,
                // "t_longitude" => $getDataSppt->longitude ?? null,
                // "t_latitude" => $getDataSppt->latitude ?? null,
                "created_by" => Auth::user()->id,
                "t_blok_kav" => $getDataSppt->blok_kav_no_op,
            ];

            $dataWp = [
                "t_id_pelayanan" => $pelayananId,
                "t_nama_wp" => $getDataSppt->nm_wp_sppt,
                "t_nik_wp" => null,
                "t_jalan_wp" => $getDataSppt->jln_wp_sppt,
                "t_rt_wp" => $getDataSppt->rt_wp_sppt,
                "t_rw_wp" => $getDataSppt->rw_wp_sppt,
                "t_kelurahan_wp" => $getDataSppt->kelurahan_wp_sppt,
                "t_kecamatan_wp" => null,
                "t_kabupaten_wp" => $getDataSppt->kota_wp_sppt,
                "t_no_hp_wp" => $getDataSppt->telp_wp,
                "created_by" => Auth::user()->id,
                "t_blok_kav_wp" => $getDataSppt->blok_kav_no_wp_sppt
            ];

            if (is_null($request->t_id_wp)) {
                Op::create($dataOp);
                Wp::create($dataWp);
            } else {
                Wp::where('t_id_wp', $request->t_id_wp)->update($dataWp);
                Op::where('t_id_op', $request->t_id_op)->update($dataOp);
            }

            DB::commit();
            return redirect()->route('pelayanan.upload-persyaratan', ['pelayanan' => $pelayanan->uuid])->with('success', 'Data permohonan berhasil disimpan');
        } catch (\Throwable $th) {
            DB::rollBack();
            report($th);
            return redirect()->route('pelayanan')->with('error', 'Terjadi kesalahan!');
        }
    }

    public function storeMutasiPecah(Request $request)
    {
        // dd($request->all());
        if (count($request->nikWP) < 1) {
            return redirect()->back()->with('error', 'Data mutasi minimal dua data, satu data Induk dan berikutnya data pecahanya');
        }

        $lunasPbb = PelayananTrait::cekWajibLunasPbb($request->t_id_jenis_pelayanan);
        if ($lunasPbb) {
            $tunggakan = $this->cekStatusTunggakanSppt($request->nopCariLama);
            if (count($tunggakan) > 0) {
                session()->flash('error', 'Permohonan tidak bisa diproses, masih ada tunggakan SPPT');
                return redirect()->back();
            }
        }
        $luasPecah = array_sum($request->luasTanah);
        if ($request->luasTanahLama <= $luasPecah) {
            dd('error');
            return redirect()->back()->with('error', 'Data luas pecahan lebih dari induk');
        }
        DB::beginTransaction();
        try {
            $pelayanan = $this->storePelayanan($request);
            $pelayananId = is_null($request->t_id_pelayanan)
                ? $pelayanan->t_id_pelayanan
                : $request->t_id_pelayanan;

            $nopArr = explode('.', $request->nopCariLama);

            foreach ($request->idOP as $key => $idop) {
                // dd($request->all());
                $kecamatan = Kecamatan::byKecamatanId($nopArr[0] . $nopArr[1] . $request->kecamatanOP[$key])->first();
                $kelurahan = Kelurahan::byKelurahanId($nopArr[0] . $nopArr[1] . $request->kecamatanOP[$key] . $request->kelurahanOP[$key])->first();

                $dataOpBaru = [
                    "t_id_pelayanan" => $pelayananId,
                    "kd_kecamatan" => $request->kecamatanOP[$key],
                    "kd_kelurahan" => $request->kelurahanOP[$key],
                    "t_jalan_op" => $request->jalanOP[$key],
                    "t_rt_op" => $request->rtOP[$key],
                    "t_rw_op" => $request->rwOP[$key],
                    "t_kelurahan_op" => $kelurahan->nm_kelurahan,
                    "t_kecamatan_op" => $kecamatan->nm_kecamatan,
                    "t_luas_tanah" => $request->luasTanah[$key],
                    "t_jenis_tanah" => $request->jenisTanah[$key],
                    "created_by" => Auth::user()->id,
                    "t_blok_kav" => $request->blokNoOP[$key] ?? null
                ];
                $saveOp = Op::updateOrCreate(['t_id_op' => $request->idOP[$key]], $dataOpBaru);
                // dd($saveOp);
                $dataWpBaru = [
                    "t_id_pelayanan" => $pelayananId,
                    "t_nama_wp" => $request->namaWP[$key],
                    "t_nik_wp" => $request->nikWP[$key],
                    "t_jalan_wp" => $request->jalanWP[$key],
                    "t_rt_wp" => $request->rtWP[$key],
                    "t_rw_wp" => $request->rwWP[$key],
                    "t_kelurahan_wp" => $request->kelurahanWP[$key],
                    "t_kecamatan_wp" => $request->kecamatanWP[$key],
                    "t_kabupaten_wp" => $request->kabupatenWP[$key],
                    "t_no_hp_wp" => $request->telp_wp,
                    "created_by" => Auth::user()->id,
                    "t_blok_kav_wp" => $request->blokKavNoWP[$key] ?? null,
                    "t_id_op" => $saveOp->t_id_op,
                ];

                // dd($dataWpBaru);
                $saveWp = Wp::updateOrCreate(['t_id_wp' => $request->idWP[$key]], $dataWpBaru);
            }
            // die;

            $dataOpLama = [
                "t_id_pelayanan" => $pelayananId,
                "kd_propinsi" => $nopArr[0],
                "kd_dati2" => $nopArr[1],
                "kd_kecamatan" => $nopArr[2],
                "kd_kelurahan" => $nopArr[3],
                "kd_blok" => $nopArr[4],
                "no_urut" => $nopArr[5],
                "kd_jns_op" => $nopArr[6],
                "t_jalan_op" => $request->jalanOPLama,
                "t_rt_op" => $request->rtOPLama,
                "t_rw_op" => $request->rwOPLama,
                "t_kelurahan_op" => $request->kelurahanOPLama,
                "t_kecamatan_op" => $request->kecamatanOPLama,
                "t_luas_tanah" => $request->luasTanahLama,
                "t_luas_bangunan" => $request->luasBangunanLama,
                "created_by" => Auth::user()->id,
                "t_blok_kav" => $request->blokNoOPLama
            ];

            $dataWpLama = [
                "t_id_pelayanan" => $pelayananId,
                "t_nama_wp" => $request->namaWPLama,
                "t_nik_wp" => $request->nikWPLama,
                "t_jalan_wp" => $request->jalanWPLama,
                "t_rt_wp" => $request->rtWPLama,
                "t_rw_wp" => $request->rwWPLama,
                "t_kelurahan_wp" => $request->kelurahanWPLama,
                "t_kecamatan_wp" => $request->kecamatanWPLama,
                "t_kabupaten_wp" => $request->kabupatenWPLama,
                "created_by" => Auth::user()->id,
                "t_blok_kav_wp" => $request->blokKavWPLama ?? null
            ];

            // dd($dataWpLama);

            OpLama::updateOrCreate(['t_id_op_lama' => $request->idOPLama], $dataOpLama);
            WpLama::updateOrCreate(['t_id_wp_lama' => $request->idWPLama], $dataWpLama);

            DB::commit();

            if (in_array(PelayananHelper::TANAH_BANGUNAN, $request->jenisTanah)) {
                return redirect()->route('pelayanan.bangunan-op', ['pelayanan' => $pelayananId])->with('success', 'Data Pemohon Berhasil di Rekam, Silahkan melanjutkan mengisi data bangunan');
            }
            return redirect()->route('pelayanan.upload-persyaratan', ['pelayanan' => $pelayanan->uuid])->with('success', 'Data Pemohon Berhasil di Rekam, Silahkan Upload Persyaratan!');
        } catch (\Throwable $th) {
            report($th);
            DB::rollBack();
            dd($th);
            return redirect()->back()->with('error', 'Terjadi kesalahan!');
        }
    }

    public function storeMutasiGabung(Request $request)
    {
        if (count($request->idOPLama) <= 1) {
            return redirect()->back()->with('error', 'Data mutasi harus lebih dari satu');
        }

        $tempNop = '';
        foreach ($request->nopLamaCari as $key => $nop) {
            if ($tempNop == $nop) {
                return redirect()->back();
            } else {
                $tempNop = $nop;
            }

            $lunasPbb = PelayananTrait::cekWajibLunasPbb($request->t_id_jenis_pelayanan);
            if ($lunasPbb) {
                $tunggakan = $this->cekStatusTunggakanSppt($nop);
                if (count($tunggakan) > 0) {
                    return redirect()->back()->with('error', 'Permohonan tidak bisa diproses, masih ada tunggakan SPPT');
                }
            }
        }

        DB::beginTransaction();
        try {
            $pelayanan = $this->storePelayanan($request);
            $pelayananId = is_null($request->t_id_pelayanan)
                ? $pelayanan->t_id_pelayanan
                : $request->t_id_pelayanan;

            $nopArr = explode('.', $request->nopCari);
            $kecamatan = Kecamatan::byKecamatanId($nopArr[0] . $nopArr[1] . $nopArr[2])->first();
            $kelurahan = Kelurahan::byKelurahanId($nopArr[0] . $nopArr[1] . $nopArr[2] . $nopArr[3])->first();

            $dataOpBaru = [
                "t_id_pelayanan" => $pelayananId,
                "kd_propinsi" => $nopArr[0],
                "kd_dati2" => $nopArr[1],
                "kd_kecamatan" => $nopArr[2],
                "kd_kelurahan" => $nopArr[3],
                "kd_blok" => $nopArr[4],
                "no_urut" => $nopArr[5],
                "kd_jns_op" => $nopArr[6],
                "t_jalan_op" => $request->jalanOP,
                "t_rt_op" => $request->rtOP,
                "t_rw_op" => $request->rwOP,
                "t_kelurahan_op" => $kelurahan->nm_kelurahan,
                "t_kecamatan_op" => $kecamatan->nm_kecamatan,
                "t_luas_tanah" => $request->luasTanah,
                "t_jenis_tanah" => $request->jenisTanah,
                "t_luas_bangunan" => $request->luasBangunan,
                "created_by" => Auth::user()->id,
                "t_blok_kav" => $request->blokNoOP ?? null
            ];

            $dataWpBaru = [
                "t_id_pelayanan" => $pelayananId,
                "t_nama_wp" => $request->namaWP,
                "t_nik_wp" => $request->nikWP,
                "t_jalan_wp" => $request->jalanWP,
                "t_rt_wp" => $request->rtWP,
                "t_rw_wp" => $request->rwWP,
                "t_kelurahan_wp" => $request->kelurahanWP,
                "t_kecamatan_wp" => $request->kecamatanWP,
                "t_kabupaten_wp" => $request->kabupatenWP,
                "t_no_hp_wp" => $request->telp_wp,
                "created_by" => Auth::user()->id,
                "t_blok_kav_wp" => $request->blokKavWP ?? null
            ];

            Op::updateOrCreate(['t_id_op' => $request->idOP], $dataOpBaru);
            Wp::updateOrCreate(['t_id_wp' => $request->idWP], $dataWpBaru);

            foreach ($request->idOPLama as $key => $idop) {
                $nopArr = explode('.', $request->nopLamaCari[$key]);
                $kecamatan = Kecamatan::byKecamatanId($nopArr[0] . $nopArr[1] . $nopArr[2])->first();
                $kelurahan = Kelurahan::byKelurahanId($nopArr[0] . $nopArr[1] . $nopArr[2] . $nopArr[3])->first();

                $dataOpLama = [
                    "t_id_pelayanan" => $pelayananId,
                    "kd_propinsi" => $nopArr[0],
                    "kd_dati2" => $nopArr[1],
                    "kd_kecamatan" => $nopArr[2],
                    "kd_kelurahan" => $nopArr[3],
                    "kd_blok" => $nopArr[4],
                    "no_urut" => $nopArr[5],
                    "kd_jns_op" => $nopArr[6],
                    "t_jalan_op" => $request->jalanOPLama[$key],
                    "t_rt_op" => $request->rtOPLama[$key],
                    "t_rw_op" => $request->rwOPLama[$key],
                    "t_kelurahan_op" => $kelurahan->nm_kelurahan,
                    "t_kecamatan_op" => $kecamatan->nm_kecamatan,
                    "t_luas_tanah" => $request->luasTanahLama[$key],
                    "t_luas_bangunan" => $request->luasBangunanLama[$key],
                    "created_by" => Auth::user()->id,
                    "t_blok_kav" => $request->blokNoOPLama[$key] ?? null
                ];

                $dataWpLama = [
                    "t_id_pelayanan" => $pelayananId,
                    "t_nama_wp" => $request->namaWPLama[$key],
                    "t_nik_wp" => $request->nikWPLama[$key],
                    "t_jalan_wp" => $request->jalanWPLama[$key],
                    "t_rt_wp" => $request->rtWPLama[$key],
                    "t_rw_wp" => $request->rwWPLama[$key],
                    "t_kelurahan_wp" => $request->kelurahanWPLama[$key],
                    "t_kecamatan_wp" => $request->kecamatanWPLama[$key],
                    "t_kabupaten_wp" => $request->kabupatenWPLama[$key],
                    "created_by" => Auth::user()->id,
                    "t_blok_kav_wp" => $request->blokKavWpLama[$key] ?? null
                ];


                OpLama::updateOrCreate(['t_id_op_lama' => $request->idOPLama[$key]], $dataOpLama);
                WpLama::updateOrCreate(['t_id_wp_lama' => $request->idWPLama[$key]], $dataWpLama);
            }

            DB::commit();

            if (PelayananHelper::TANAH_BANGUNAN == $request->jenisTanah) {
                return redirect()->route('pelayanan.bangunan-op', ['pelayanan' => $pelayananId])->with('success', 'Data permohonan berhasil disimpan');
            }
            return redirect()->route('pelayanan.upload-persyaratan', ['pelayanan' => $pelayanan->uuid])->with('success', 'Data Pemohon Berhasil di Rekam, Silahkan Upload Persyaratan!');
        } catch (\Throwable $th) {
            report($th);
            DB::rollBack();
            return redirect()->back()->with('error', 'Terjadi kesalahan!');
        }
    }

    public function cetakTandaTerima(Pelayanan $pelayanan)
    {
        $persyaratan = PersyaratanPelayanan::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->get();

        $wpLama = WpLama::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first();
        $opLama = OpLama::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first();
        $wpBaru = Wp::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->get();
        // dd($wpBaru);
        if (in_array($pelayanan->t_id_jenis_pelayanan, [3])) {

            foreach ($wpBaru as $key => $v) {
                $opBaru = Op::where('t_id_op', $v->t_id_op)->first();
                // dd($opBaru);
                $databaru = [
                    't_id_wp' => $v->t_id_wp,
                    't_id_pelayanan' => $v->t_id_pelayanan,
                    't_nop' => $v->t_nop,
                    't_nama_wp' => $v->t_nama_wp,
                    't_nik_wp' => $v->t_nik_wp,
                    't_jalan_wp' => $v->t_jalan_wp,
                    't_rt_wp' => $v->t_rt_wp,
                    't_rw_wp' => $v->t_rw_wp,
                    't_kelurahan_wp' => $v->t_kelurahan_wp,
                    't_kecamatan_wp' => $v->t_kecamatan_wp,
                    't_kabupaten_wp' => $v->t_kabupaten_wp,
                    't_no_hp_wp' => $v->t_no_hp_wp,
                    't_npwpd' => $v->t_npwpd,
                    't_email' => $v->t_email,
                    'created_by' => $v->created_by,
                    't_blok_kav_wp' => $v->t_blok_kav_wp,
                    't_id_op' => $v->t_id_op,
                    'kd_propinsi' => $opBaru->kd_propinsi,
                    'kd_dati2' => $opBaru->kd_dati2,
                    'kd_kecamatan' => $opBaru->kd_kecamatan,
                    'kd_kelurahan' => $opBaru->kd_kelurahan,
                    'kd_blok' => $opBaru->kd_blok,
                    'no_urut' => $opBaru->no_urut,
                    'kd_jns_op' => $opBaru->kd_jns_op,
                    't_jalan_op' => $opBaru->t_jalan_op,
                    't_rt_op' => $opBaru->t_rt_op,
                    't_rw_op' => $opBaru->t_rw_op,
                    't_kelurahan_op' => $opBaru->t_kelurahan_op,
                    't_kecamatan_op' => $opBaru->t_kecamatan_op,
                    't_luas_tanah' => $opBaru->t_luas_tanah,
                    't_luas_bangunan' => $opBaru->t_luas_bangunan,
                    't_jenis_tanah' => $opBaru->t_jenis_tanah,
                    't_kode_lookup_item' => $opBaru->t_kode_lookup_item,
                    't_latitude' => $opBaru->t_latitude,
                    't_longitude' => $opBaru->t_longitude,
                    'created_by' => $opBaru->created_by,
                    'created_at' => $opBaru->created_at,
                    'updated_at' => $opBaru->updated_at,
                    't_blok_kav' => $opBaru->t_blok_kav,
                    't_kode_znt' => $opBaru->t_kode_znt,
                    't_nomor_op' => $opBaru->t_nomor_op,
                    't_nop_asal' => $opBaru->t_nop_asal,
                    'jns_bumi' => $opBaru->jns_bumi,
                ];
                $data[] = $databaru;
            }

            $dataLama = [
                't_id_wp_lama' => $wpLama->t_id_wp_lama,
                't_id_pelayanan' => $wpLama->t_id_pelayanan,
                't_nik_wp' => $wpLama->t_nik_wp,
                't_nama_wp' => $wpLama->t_nama_wp,
                't_jalan_wp' => $wpLama->t_jalan_wp,
                't_rt_wp' => $wpLama->t_rt_wp,
                't_rw_wp' => $wpLama->t_rw_wp,
                't_kelurahan_wp' => $wpLama->t_kelurahan_wp,
                't_kecamatan_wp' => $wpLama->t_kecamatan_wp,
                't_kabupaten_wp' => $wpLama->t_kabupaten_wp,
                't_no_hp_wp' => $wpLama->t_no_hp_wp,
                't_blok_kav_wp' => $wpLama->t_blok_kav_wp,
                'kd_propinsi' => $opLama->kd_propinsi,
                'kd_dati2' => $opLama->kd_dati2,
                'kd_kecamatan' => $opLama->kd_kecamatan,
                'kd_kelurahan' => $opLama->kd_kelurahan,
                'kd_blok' => $opLama->kd_blok,
                'no_urut' => $opLama->no_urut,
                'kd_jns_op' => $opLama->kd_jns_op,
                't_jalan_op' => $opLama->t_jalan_op,
                't_rt_op' => $opLama->t_rt_op,
                't_rw_op' => $opLama->t_rw_op,
                't_kelurahan_op' => $opLama->t_kelurahan_op,
                't_kecamatan_op' => $opLama->t_kecamatan_op,
                't_luas_tanah' => $opLama->t_luas_tanah,
                't_luas_bangunan' => $opLama->t_luas_bangunan,
                'created_by' => $opLama->created_by,
                't_blok_kav' => $opLama->t_blok_kav,
            ];
        } else {
            $dataLama = [
                't_id_wp_lama' => null,
                't_id_pelayanan' => null,
                't_nik_wp' => null,
                't_nama_wp' => null,
                't_jalan_wp' => null,
                't_rt_wp' => null,
                't_rw_wp' => null,
                't_kelurahan_wp' => null,
                't_kecamatan_wp' => null,
                't_kabupaten_wp' => null,
                't_no_hp_wp' => null,
                't_blok_kav_wp' => null,
                'kd_propinsi' => null,
                'kd_dati2' => null,
                'kd_kecamatan' => null,
                'kd_kelurahan' => null,
                'kd_blok' => null,
                'no_urut' => null,
                'kd_jns_op' => null,
                't_jalan_op' => null,
                't_rt_op' => null,
                't_rw_op' => null,
                't_kelurahan_op' => null,
                't_kecamatan_op' => null,
                't_luas_tanah' => null,
                't_luas_bangunan' => null,
                'created_by' => null,
                't_blok_kav' => null,
            ];
            $data = [[
                't_id_wp' => null,
                't_id_pelayanan' => null,
                't_nop' => null,
                't_nama_wp' => null,
                't_nik_wp' => null,
                't_jalan_wp' => null,
                't_rt_wp' => null,
                't_rw_wp' => null,
                't_kelurahan_wp' => null,
                't_kecamatan_wp' => null,
                't_kabupaten_wp' => null,
                't_no_hp_wp' => null,
                't_npwpd' => null,
                't_email' => null,
                'created_by' => null,
                't_blok_kav_wp' => null,
                't_id_op' => null,
                'kd_propinsi' => null,
                'kd_dati2' => null,
                'kd_kecamatan' => null,
                'kd_kelurahan' => null,
                'kd_blok' => null,
                'no_urut' => null,
                'kd_jns_op' => null,
                't_jalan_op' => null,
                't_rt_op' => null,
                't_rw_op' => null,
                't_kelurahan_op' => null,
                't_kecamatan_op' => null,
                't_luas_tanah' => null,
                't_luas_bangunan' => null,
                't_jenis_tanah' => null,
                't_kode_lookup_item' => null,
                't_latitude' => null,
                't_longitude' => null,
                'created_by' => null,
                'created_at' => null,
                'updated_at' => null,
                't_blok_kav' => null,
                't_kode_znt' => null,
                't_nomor_op' => null,
                't_nop_asal' => null,
                'jns_bumi' => null,
            ]];
        }
        // dd($wpBaru);
        $pdf = Pdf::setOption(['dpi' => 150, 'defaultFont' => 'Arial'])
            ->setPaper('a4', 'potrait')
            ->loadView('pelayanan.exports.cetak-tanda-terima', [
                'pemda' => PemdaModel::first(),
                'pelayanan' => $pelayanan->load('jenisPajak')->load('jenisPelayanan')->load('wajibPajak')->load('pelayananAprove')->load('OpLama'),
                'persyaratan' => $persyaratan,
                'dataLama' => $dataLama,
                'dataBaru' => $data,
                'wpLama' => $wpLama,
                'wpBaru' => $wpBaru,
                'noPelayanan' => PelayananHelper::formatNoPelayanan(
                    date('Y', strtotime($pelayanan->t_tgl_pelayanan)),
                    $pelayanan->t_id_jenis_pajak,
                    $pelayanan->t_no_pelayanan
                )
            ]);

        return $pdf->stream(Uuid::uuid4() . '.pdf');
        // return $pdf->download(Uuid::uuid4() . '.pdf');
    }

    public function cetakDaftarPelayanan(Request $request)
    {
        $pelayanan = Pelayanan::when($request->tglPealyanan, function ($q) use ($request) {
            $tglPelayanan = explode(' - ', $request->tglPealyanan);

            return $q->whereBetween('t_pelayanan.t_tgl_pelayanan', [
                date('Y-m-d', strtotime($tglPelayanan[0])),
                date('Y-m-d', strtotime($tglPelayanan[1]))
            ]);
        })->orderBy('t_tgl_pelayanan')->get();

        if ($request->format == 'XLS') {
            return Excel::download(new PelayananExport($pelayanan), Uuid::uuid4() . '.xlsx');
        }
        return Pdf::setOption(['dpi' => 150, 'defaultFont' => 'Arial'])
            ->setPaper('a4', 'landscape')
            ->loadView('pelayanan.exports.daftar-pelayanan', ['pelayanan' => $pelayanan])
            ->download(Uuid::uuid4() . '.pdf');
    }

    public function cekPelayananSebelumnyaOpBaru(Request $request)
    {
        $pelayanan = $this->checkPelayananOpBaruSebelumnya(
            $request->nikPemohon,
            $request->idJenisPelayanan,
            $request->idJenisPajak,
            $request->nikWp
        );

        $response = [
            'success' => false,
            'data' => null
        ];

        if ($pelayanan) {
            $response['success'] = true;
            $response['data'] = [
                'noPelayanan' => (new PelayananHelper())->formatNoUrut($pelayanan->t_tgl_pelayanan, $request->idJenisPajak, $pelayanan->t_no_pelayanan),
                'nikPemohon' => $pelayanan->t_nik_pemohon,
                'nikWp' => $pelayanan->wajibPajak->t_nik_wp,
                'namaWp' => $pelayanan->wajibPajak->t_nama_wp,
                'jenisPelayanan' => $pelayanan->jenisPelayanan->s_nama_jenis_pelayanan,
                'jenisPajak' => $pelayanan->jenisPajak->s_nama_jenis_pajak,
                'statusPermohonan' => 'Draft',
            ];
        }

        return response()->json([
            'status_permohonan' => true,
            'data' => $response
        ]);
    }

    public function cekPelayananSebelumnya(Request $request)
    {
        $pelayana = $this->cekPelayananSebelumnya(
            $request->t_nik_pemohon,
            $request->t_id_jenis_pelayanan,
            $request->t_id_jenis_pajak,
            $request->t_nop
        );

        return response()->json([
            'status_permohonan' => true
        ]);
    }

    public function cekDataMutas(Request $request)
    {
        $data = (new DafnomOp())->whereNop($request->nop)->first();

        return response()->json([
            'status' => $data ? true : false,
            'message' => $data ? 'Nop sudah dimutasi' : '',
        ], 200);
    }

    public function cetakSpop(Pelayanan $pelayanan)
    {
        // $pelayanan = Pelayanan::find(53);
        $datObjekPajak = DatObjekPajak::whereNop($pelayanan->objekPajak->nop)->first();
        $datObjekBumi = DatOpBumi::findByNop($pelayanan->objekPajak->nop)->first();
        $datSubjekPajak = DatSubjekPajak::whereSubjekPajakId(str_replace(' ', '', $datObjekPajak->subjek_pajak_id))->first();
        $datObjekBangunan = DatOpBangunan::with('jpb')->whereNop($pelayanan->objekPajak->nop)->get();
        $opBanguanan = [];
        foreach ($datObjekBangunan as $dob) {
            $bangunan = $dob->toArray();
            $bangunan['nop'] = $dob->nopFormated;
            $fasilitas = DatFasilitasBangunan::select('kd_fasilitas', 'jml_satuan')->with('fasilitas')->whereNopAndNoBng($dob->nop, $dob->no_bng)->get();
            foreach ($fasilitas as $value) {
                $bangunan[$value->kd_fasilitas] = [
                    "jml_satuan" => $value->jml_satuan,
                    "nm_fasilitas" => $value->fasilitas->nm_fasilitas,
                    "satuan_fasilitas" => $value->fasilitas->satuan_fasilitas,
                ];
            }
            $opBanguanan[] = $bangunan;
        }

        return Pdf::setOption(['dpi' => 150, 'defaultFont' => 'Arial'])
            ->setPaper('A4', 'potrait')
            ->loadView('pelayanan.exports.spop-lspop', [
                'pelayanan' => $pelayanan,
                'pemda' => PemdaModel::first(),
                'datObjekPajak' => $datObjekPajak,
                'datSubjekPajak' => $datSubjekPajak,
                'datObjekBangunan' => $opBanguanan,
                'datObjekBumi' => $datObjekBumi,
                // 'fasilitas' => $opBanguanan
            ])
            ->stream(Uuid::uuid4() . '.pdf');
    }

    public function cektunggakanpbb(Request $Request)
    {
        $dafnomOp = (new DafnomOp())->whereNop($Request->nop)->where('kategori_op', '=', '3')->first();
        if (!$dafnomOp) {

            $data_get = $Request->all();
            $html = '';
            $PBB_YG_HARUS_DIBAYAR_SPPT = 0;

            if (!empty($data_get['nop'])) {

                $cek_data_tunggakan = $this->cekStatusTunggakanSppt($data_get['nop']);

                $html = "<div class='row'>
                          <div class='col-md-12'>
                          <div class='panel panel-primary'>
                          <div class='panel-heading'><strong>Tunggakan SPPT-PBB</strong></div>
                          <table class='table table-striped table-bordered'>";
                $html .= "<tr>";
                $html .= "<th>No.</th>";
                $html .= "<th>Tahun</th>";
                $html .= "<th>Tunggakan (Rp.)</th>";
                $html .= "<th>Jatuh Tempo</th>";
                $html .= "<th>Denda (Rp.)</th>";
                $html .= "</tr>";
                $i = 1;
                $jumlahdenda = 0;
                $PBB_YG_HARUS_DIBAYAR_SPPT = 0;
                foreach ($cek_data_tunggakan as $row) {
                    $html .= "<tr>";
                    $html .= "<td style='text-align: center'> " . $i . " </td>";
                    $html .= "<td style='text-align: center'> " . $row['thn_pajak_sppt'] . " </td>";
                    $html .= "<td style='text-align: right'> " . number_format($row['pbb_yg_harus_dibayar_sppt'], 0, ',', '.') . " </td>";
                    $html .= "<td style='text-align: center'>  " . $row['jatuh_tempo'] . " </td>";
                    if (strtotime($row['jatuh_tempo']) < strtotime(date('Y-m-d'))) {
                        $cek_denda = $this->cek_jumlahbulandenda($row['jatuh_tempo'], $row['pbb_yg_harus_dibayar_sppt']);
                        $denda = $cek_denda['denda'];
                    } else {
                        $denda = 0;
                    }
                    $html .= "<td style='text-align: right'>  " . number_format($denda, 0, ',', '.') . " </td>";
                    $html .= "</tr>";
                    $i++;
                    $PBB_YG_HARUS_DIBAYAR_SPPT += $row['pbb_yg_harus_dibayar_sppt'];
                    $jumlahdenda = $jumlahdenda + $denda;
                }
                $html .= "<tr style='font-size:16px; font-weight:bold;'>";
                $html .= "<td colspan='2' style='text-align: right'>Jumlah Tunggakan</td>";
                $html .= "<td style='text-align: right'> " . number_format($PBB_YG_HARUS_DIBAYAR_SPPT, 0, ',', '.') . " </td>";
                $html .= "<td style='text-align: right'>Jumlah Denda</td>";
                $html .= "<td style='text-align: right'> " . number_format($jumlahdenda, 0, ',', '.') . " </td>";
                $html .= "</tr>";
                $html .= "<tr style='font-size:16px; font-weight:bold;'>";
                $html .= "<td colspan='3' style='text-align: right'>Jumlah Seluruh Tunggakan</td>";
                $html .= "<td colspan='2' style='text-align: right'> Rp. " . number_format($PBB_YG_HARUS_DIBAYAR_SPPT + $jumlahdenda, 0, ',', '.') . "</td>";
                $html .= "</tr>";
                $html .= "</table></div></div></div>";
            }
        } else {
            $html = 3;
            $PBB_YG_HARUS_DIBAYAR_SPPT = "";
        }
        return response()->json([
            'datatunggakan' => $html,
            'PBB_YG_HARUS_DIBAYAR_SPPT' => $PBB_YG_HARUS_DIBAYAR_SPPT,
        ]);
    }

    public function cek_jumlahbulandenda($tgl_tempo, $tagihan, $tglbayarnya = null)
    {
        if (!empty($tglbayarnya)) {
            $tgl_bayar = $tglbayarnya;
        } else {
            $tgl_bayar = date('Y-m-d');
        }
        $tgl_tempo = date('Y-m-d', strtotime($tgl_tempo));
        $tgl_bayar = explode("-", $tgl_bayar);
        $tgl_tempo = explode("-", $tgl_tempo);
        $tahun = $tgl_bayar[0] - $tgl_tempo[0];
        $bulan = $tgl_bayar[1] - $tgl_tempo[1];
        $hari = $tgl_bayar[2] - $tgl_tempo[2];
        if (($tahun == 0) || ($tahun < 1)) {
            if (($bulan == 0) || ($bulan < 1)) {
                if ($bulan < 0) {
                    $months = 0;
                } else {
                    if (($hari == 0) || ($hari < 1)) {
                        $months = 0;
                    } else {
                        $months = 1;
                    }
                }
            } else {
                if ($bulan == 1) {
                    if (($hari == 0) || ($hari < 1)) {
                        $months = $bulan;
                    } else {
                        $months = $bulan + 1;
                    }
                } else {
                    if (($hari == 0) || ($hari < 1)) {
                        $months = $bulan;
                    } else {
                        $months = $bulan + 1;
                    }
                }
            }
        } else {
            $jmltahun = $tahun * 12;
            if ($bulan == 0) {
                if (($hari == 0) || ($hari < 1)) {
                    $months = $jmltahun;
                } else {
                    $months = $jmltahun + 1;
                }
            } elseif ($bulan < 1) {
                if (($hari == 0) || ($hari < 1)) {
                    $months = $jmltahun + $bulan;
                } else {
                    $months = $jmltahun + $bulan + 1;
                }
            } else {
                if (($hari == 0) || ($hari < 1)) {
                    $months = $jmltahun + $bulan;
                } else {
                    $months = $jmltahun + $bulan + 1;
                }
            }
        }
        if ($months > 24)
            $months = 24;
        if ($months > 0)
            $jmldenda = $months * 2 / 100 * $tagihan;
        else
            $jmldenda = 0;
        $ubahstring = (string) ($jmldenda);
        $result['denda'] = ceil((float) $ubahstring);
        $result['jumlahbulandenda'] = $months;
        return $result;
    }

    public function dataPbbPull($nop)
    {
        $data = '';
        try {
            $nops = PelayananHelper::parseNop($nop);
            // $nop = '340101000700200330';
            $nope = [
                "KD_PROPINSI" => substr($nop, 0, 2),
                "KD_DATI2" => substr($nop, 2, 2),
                "KD_KECAMATAN" => substr($nop, 4, 3),
                "KD_KELURAHAN" => substr($nop, 7, 3),
                "KD_BLOK" => substr($nop, 10, 3),
                "NO_URUT" => substr($nop, 13, 4),
                "KD_JNS_OP" => substr($nop, 17, 1)
            ];

            $getDetailBangunan = (new DatOpBangunan())->detailBangunan($nope);
            $getFasilitasBangunan = (new DatOpBangunan())->detailFasilitas($nope);

            $data = [
                'detailBangunan' => $getDetailBangunan,
                'detailFasilitas' => $getFasilitasBangunan
            ];
            // dd($data);
        } catch (Throwable $e) {
            Log::error("ERROR_MESSAGE: " . $e);
        }
        return response()->json(
            $data,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
        );
    }

    public function dataPbbPull2($nop)
    {
        $data = '';
        try {
            $nops = PelayananHelper::parseNop($nop);
            // $nop = '340101000700200330';
            $nope = [
                "KD_PROPINSI" => substr($nop, 0, 2),
                "KD_DATI2" => substr($nop, 2, 2),
                "KD_KECAMATAN" => substr($nop, 4, 3),
                "KD_KELURAHAN" => substr($nop, 7, 3),
                "KD_BLOK" => substr($nop, 10, 3),
                "NO_URUT" => substr($nop, 13, 4),
                "KD_JNS_OP" => substr($nop, 17, 1)
            ];
            // dd($nope);
            $getDetailBangunan = (new DatOpBangunan())->detailBangunan($nope);
            $getFasilitasBangunan = (new DatOpBangunan())->detailFasilitas($nope);

            $data = [
                'detailBangunan' => $getDetailBangunan,
                'detailFasilitas' => $getFasilitasBangunan
            ];
            // dd($data);
        } catch (Throwable $e) {
            Log::error("ERROR_MESSAGE: " . $e);
        }
        return $data;
    }

    public function delete(Request $request)
    {
        try {
            PelayananTrait::deletePelayanan($request->id);
        } catch (\Throwable $th) {
            // DB::rollBack();
            report("ERROR: " . $th);
            session()->flash('error', 'Terjadi kesalahan!');
            dd($th);
            return redirect()->back();
        }
        return response()->json(
            'Berhasil Dihapus',
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
        );
    }
}
