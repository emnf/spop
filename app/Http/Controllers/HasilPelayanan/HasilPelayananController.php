<?php

namespace App\Http\Controllers\HasilPelayanan;

use App\Exports\HasilPelayananExport;
use App\Helpers\CountHelper;
use App\Helpers\DatagridHelpers;
use App\Helpers\PelayananHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\Validasi\ValidasiTrait;
use App\Models\Hasil\HasilPelayanan;
use App\Models\Pelayanan\Pelayanan;
use App\Models\Setting\JenisPajak;
use App\Models\Setting\JenisPelayanan;
use App\Models\Setting\PemdaModel;
use App\Service\NotificationService;
use App\Service\Pbb\SkPenguranganService;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Ramsey\Uuid\Uuid;

class HasilPelayananController extends Controller
{
    use ValidasiTrait;

    private $skPenguranganService;

    public function __construct(SkPenguranganService $skPenguranganService)
    {
        $this->skPenguranganService = $skPenguranganService;
    }

    public function index()
    {
        $param = (new CountHelper)->countPelayanan();
        $param['jenisPajak'] = JenisPajak::select('s_id_jenis_pajak', 's_nama_jenis_pajak')->get();
        $param['jenisPelayanan'] = JenisPelayanan::select('s_id_jenis_pelayanan', 's_nama_jenis_pelayanan')->get();
        return view('hasil-pelayanan.index', $param);
    }

    public function create(Pelayanan $pelayanan)
    {
        $pelayanan->load('validasi')
            ->load('pelayananAprove')
            ->load('jenisPelayanan')
            ->load('jenisPajak')
            ->load('wajibPajak')
            ->load('objekPajak');

        return view('hasil-pelayanan.tambah', [
            'pelayanan' => $pelayanan
        ]);
    }

    public function edit(Pelayanan $pelayanan)
    {
        return view('hasil-pelayanan.edit');
    }

    public function store(Request $request)
    {
        try {
            $pelayanan = Pelayanan::find($request->idPelayanan);

            if ($pelayanan->t_id_jenis_pelayanan == 8) {
                $this->skPenguranganService->simpanSkPenguranganPst($request->noSk, $request->tglSk, $pelayanan);
                $fileStored = null;
            } elseif (in_array($pelayanan->t_id_jenis_pelayanan, [4, 5, 7])) {
                $fileStored = null;
            } else {
                $request->validate([
                    "fileinput" => 'required|max:5120|mimes:jpeg,png,pdf'
                ]);

                if ($request->hasFile('fileinput')) {
                    $file = $request->file('fileinput');
                    $fileStored = Storage::putFile('/sk-pelayanan', $file);
                }

            }

            $dataPelayanan = [
                't_id_pelayanan' => $request->idPelayanan,
                't_id_validasi' => $request->idValidasi,
                't_tgl_sk' => date('Y-m-d', strtotime($request->tglSk)),
                't_no_sk' => $request->noSk,
                't_keterangan_sk' => $request->keteranganSk,
                't_lokasi_file' => $fileStored,
                'created_by' => auth()->user()->id,
                't_no_urut' => HasilPelayanan::maxNoUrut() + 1,
            ];
            $hasilPelayanan = HasilPelayanan::create($dataPelayanan);
            $pelayanan = Pelayanan::find($request->idPelayanan);

            $notifikasi = new NotificationService;
            $notifikasi->notifyHasilPelayanan($pelayanan);

            return redirect('hasil-pelayanan')->with('success', 'Hasil Pelayanan berhasil di simpan!');
        } catch (\Throwable $th) {
            report($th);

            return redirect()->back()->with('error', 'Hasil Pelayanan gagal di simpan!');
        }
    }

    public function datagridBelum(Request $request)
    {
        try {
            $pelayanan = (new HasilPelayanan())->daatgrid(
                $request->filter,
                $request->pagination,
                $request->sorting,
                HasilPelayanan::BELUM_UPLOAD
            );
            $dataArr = [];

            foreach ($pelayanan as $key => $v) {

                $dataArr[] = [
                    'jenisPajak' => $v->s_nama_singkat_pajak,
                    'jenisPelayanan' => $v->s_nama_jenis_pelayanan,
                    'tglPermohonan' => date('d-m-Y', strtotime($v->t_tgl_pelayanan)),
                    'noPermohonan' => PelayananHelper::formatNoPelayanan(
                        date('Y', strtotime($v->t_tgl_pelayanan)),
                        $v->t_id_jenis_pajak,
                        $v->t_no_pelayanan
                    ),
                    'nikPemohon' => $v->t_nik_pemohon,
                    'namaPemohon' => $v->t_nama_pemohon,
                    'actionList' => array(
                        [
                            'actionName' => 'sk-tambah',
                            'actionUrl' => "/hasil-pelayanan/tambah/" . $v->t_id_pelayanan,
                            'actionTitle' => 'Unggah Hasil Validasi',
                            'actionActive' => true
                        ]
                    )
                ];
            }

            return (new DatagridHelpers)->getSuccessResponse($dataArr, $pelayanan);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $pelayanan);
        }
    }

    public function datagridSudah(Request $request)
    {
        try {
            $pelayanan = (new HasilPelayanan())->daatgrid(
                $request->filter,
                $request->pagination,
                $request->sorting,
                HasilPelayanan::SUDAH_UPLOAD
            );
            $dataArr = [];

            foreach ($pelayanan as $key => $v) {
                if (in_array($v->t_id_jenis_pelayanan, [8, 4, 5, 7])) {
                    $btnDownloadSK = false;
                    $btnCetakSK = true;
                }  else {
                    $btnDownloadSK = true;
                    $btnCetakSK = false;
                }
                $dataArr[] = [
                    'jenisPajak' => $v->s_nama_singkat_pajak,
                    'jenisPelayanan' => $v->s_nama_jenis_pelayanan,
                    'tglPermohonan' => date('d-m-Y', strtotime($v->t_tgl_pelayanan)),
                    'noPermohonan' => PelayananHelper::formatNoPelayanan(
                        date('Y', strtotime($v->t_tgl_pelayanan)),
                        $v->t_id_jenis_pajak,
                        $v->t_no_pelayanan
                    ),
                    'nikPemohon' => $v->t_nik_pemohon,
                    'namaPemohon' => $v->t_nama_pemohon,
                    'noSK' => $v->t_no_sk,
                    'tglSK' => $v->t_tgl_sk,
                    'keteranganSK' => $v->t_keterangan_sk,
                    'actionList' => array(
                        [
                            'actionName' => 'sk-download',
                            'actionUrl' => "javascript:downloadsk('/" . $v->t_lokasi_file . "', 'SK')",
                            'actionTitle' => 'Cetak SK',
                            'actionActive' => $btnDownloadSK
                        ],
                        [
                            'actionName' => 'cetak-sk',
                            'actionUrl' => "javascript:cetaksk('" . $v->uuid . "')",
                            'actionTitle' => 'Cetak SK',
                            'actionActive' => $btnCetakSK
                        ],
                    )
                ];
            }

            return (new DatagridHelpers)->getSuccessResponse($dataArr, $pelayanan);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $pelayanan);
        }
    }

    public function datagridSemua(Request $request)
    {
        try {
            $pelayanan = (new HasilPelayanan())->daatgrid(
                $request->filter,
                $request->pagination,
                $request->sorting,
                HasilPelayanan::SEMUA_DATA
            );
            $dataArr = [];

            foreach ($pelayanan as $key => $v) {
                $dataArr[] = [
                    'jenisPajak' => $v->s_nama_singkat_pajak,
                    'jenisPelayanan' => $v->s_nama_jenis_pelayanan,
                    'tglPermohonan' => date('d-m-Y', strtotime($v->t_tgl_pelayanan)),
                    'noPermohonan' => PelayananHelper::formatNoPelayanan(
                        date('Y', strtotime($v->t_tgl_pelayanan)),
                        $v->t_id_jenis_pajak,
                        $v->t_no_pelayanan
                    ),
                    'nikPemohon' => $v->t_nik_pemohon,
                    'namaPemohon' => $v->t_nama_pemohon,
                    'status' => is_null($v->t_id_sk) ? 'Belum' : 'Sudah',
                    'noSK' => is_null($v->t_no_sk) ? '' : $v->t_no_sk,
                    'tglSK' => is_null($v->t_tgl_sk) ? '' : date('d-m-Y', strtotime($v->t_tgl_sk)),
                    'keteranganSK' => is_null($v->t_keterangan_sk) ? '' : $v->t_keterangan_sk,
                    'actionList' => array(
                        [
                            'actionName' => 'sk-download',
                            'actionUrl' => "javascript:downloadsk('/" . $v->t_lokasi_file . "', 'SK')",
                            'actionTitle' => 'Unduh Hasil Validasi',
                            'actionActive' => is_null($v->t_id_sk) ? false : true
                        ],
                        [
                            'actionName' => 'sk-tambah',
                            'actionUrl' => "/hasil-pelayanan/tambah/" . $v->t_id_pelayanan,
                            'actionTitle' => 'Unggah Hasil Validasi',
                            'actionActive' => true
                        ]
                    )
                ];
            }

            return (new DatagridHelpers)->getSuccessResponse($dataArr, $pelayanan);
        } catch (\Throwable $th) {
            report($th);
            return (new DatagridHelpers)->getSuccessResponse($dataArr, $pelayanan);
        }
    }

    public function cetakDaftarHasilPelayanan(Request $request)
    {
        $pelayanan = Pelayanan::when($request->tglPelayanan, function ($q) use ($request) {
            $tglPelayanan = explode(' - ', $request->tglPelayanan);

            return $q->whereBetween('t_pelayanan.t_tgl_pelayanan', [
                date('Y-m-d', strtotime($tglPelayanan[0])),
                date('Y-m-d', strtotime($tglPelayanan[1]))
            ]);
        })
            ->when($request->idJenisPelayanan, function ($q) use ($request) {
                $q->where('t_pelayanan.t_id_jenis_pelayanan', $request->idJenisPelayanan);
            })
            ->when($request->namaPemohon, function ($q) use ($request) {
                // $q->where('t_pelayanan.t_nama_pemohon', $request->namaPemohon);
                $q->where('t_pelayanan.t_nama_pemohon', 'ilike', "%{$request->namaPemohon}%");
            })
            ->when($request->noPelayanan, function ($q) use ($request) {
                $q->where('t_pelayanan.t_no_pelayanan', 'ilike', "%{$request->noPelayanan}%");
            })
            ->whereRelation('validasi', 't_id_status_validasi', '=', '4')
            ->orderBy('t_tgl_pelayanan')->get();

        $viewName = 'hasil-pelayanan.exports.daftar-hasil-pelayanan';

        if ($request->format == 'XLS') {
            return Excel::download(new HasilPelayananExport($pelayanan, $viewName), Uuid::uuid4() . '.xlsx');
        }

        return Pdf::setOption(['dpi' => 150, 'defaultFont' => 'Arial'])
            ->setPaper('a4', 'landscape')
            ->loadView($viewName, [
                'pelayanan' => $pelayanan,
                'pemda' => PemdaModel::first(),
                'noImage' => false
            ])
            ->download(Uuid::uuid4() . '.pdf');
    }

    public function cetakSkPenguranagan(Pelayanan $pelayanan)
    {
        $pdf = Pdf::setOption(['dpi' => 150, 'defaultFont' => 'Arial'])
            ->setPaper('a4', 'potrait')
            ->loadView('hasil-pelayanan.exports.cetak-sk-pengurangan', [
                'pemda' => PemdaModel::first(),
                'pelayanan' => $pelayanan
            ]);

        return $pdf->stream(Uuid::uuid4() . 'pdf');
    }

    public function cetakSK(Pelayanan $pelayanan)
    {
        $pdf = Pdf::setOption(['dpi' => 150, 'defaultFont' => 'Arial'])
            ->setPaper('a4', 'potrait')
            ->loadView('hasil-pelayanan.exports.cetak-sk', [
                'pemda' => PemdaModel::first(),
                'pelayanan' => $pelayanan
            ]);

        return $pdf->stream(Uuid::uuid4() . 'pdf');
    }
}
