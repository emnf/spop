<?php

namespace App\Http\Controllers\Pbb;

use App\Helpers\PelayananHelper;
use App\Http\Controllers\Controller;
use App\Models\Pbb\DatObjekPajak;
use App\Models\Pbb\DatOpBangunan;
use App\Models\Pbb\DatOpBumi;
use Illuminate\Http\Request;

class DatObjekPajakController extends Controller
{
    public function getBangunan(Request $request)
    {
        try {
            $nop = PelayananHelper::parseNop($request->nop);
            $bangunan = DatOpBangunan::getByNop($nop)->get()->toArray();

            return response()->json($bangunan, 200);
        } catch (\Throwable $th) {
            report($th);
            return response()->json($bangunan, 200);
        }
    }

    public function getObjekPajak(Request $request)
    {
        $arrNop = [
            "kd_propinsi" => substr($request->nop, 0, 2),
            "kd_dati2" => substr($request->nop, 3, 2),
            "kd_kecamatan" => substr($request->nop, 6, 3),
            "kd_kelurahan" => substr($request->nop, 10, 3),
            "kd_blok" => substr($request->nop, 14, 3),
            "no_urut" => substr($request->nop, 18, 4),
            "kd_jns_op" => substr($request->nop, 23, 1)
        ];

        $objekPajak = DatObjekPajak::with('subjekPajak')
            ->join('REF_KECAMATAN', function ($q) {
                $q->on('DAT_OBJEK_PAJAK.KD_PROPINSI', '=', 'REF_KECAMATAN.KD_PROPINSI')
                    ->on('DAT_OBJEK_PAJAK.KD_DATI2', '=', 'REF_KECAMATAN.KD_DATI2')
                    ->on('DAT_OBJEK_PAJAK.KD_KECAMATAN', '=', 'REF_KECAMATAN.KD_KECAMATAN');
            })
            ->join('REF_KELURAHAN', function ($q) {
                $q->on('DAT_OBJEK_PAJAK.KD_PROPINSI', '=', 'REF_KELURAHAN.KD_PROPINSI')
                    ->on('DAT_OBJEK_PAJAK.KD_DATI2', '=', 'REF_KELURAHAN.KD_DATI2')
                    ->on('DAT_OBJEK_PAJAK.KD_KECAMATAN', '=', 'REF_KELURAHAN.KD_KECAMATAN')
                    ->on('DAT_OBJEK_PAJAK.KD_KELURAHAN', '=', 'REF_KELURAHAN.KD_KELURAHAN');
            })
            ->whereNop($arrNop)->first();

        $datOpBumi = DatOpBumi::findByNop($arrNop)->first();

        $objekPajakArr = [];
        if (!is_null($objekPajak)) {
            $objekPajakArr = $objekPajak->toArray();
            $objekPajakArr['nop'] = $objekPajak->nop;
            $objekPajakArr['jenis_tanah'] = $datOpBumi->jns_bumi;
        }

        $objekPajakArr['rw_op'] = $objekPajakArr['rw_op'] == null ? '00' : $objekPajakArr['rw_op'];
        $objekPajakArr['rt_op'] = $objekPajakArr['rt_op'] == null ? '000' : $objekPajakArr['rt_op'];
        return response()->json($objekPajakArr);
    }
}
