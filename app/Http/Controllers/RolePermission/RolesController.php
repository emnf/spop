<?php

namespace App\Http\Controllers\RolePermission;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesController extends Controller
{
    public function index()
    {
        return view('setting-role-permission.index', [
            'roles' => Role::where('name', '!=', 'Super Admin')->get(),
            'permissionList' => Permission::select('id as persmissionId', 'name as permissionName')->get()
        ]);
    }

    public function getRole(Role $role)
    {
        $response = [
            'role' => $role,
            'permissions' => $role->getPermissionNames()
        ];
        return response()->json($response, 200);
    }

    public function syncPermission(Request $request)
    {
        $request->validate([
            'roleName' => 'required',
            'listPermission' => 'required|array',
        ]);
        $role = Role::findByName($request->roleName);
        $role->syncPermissions($request->listPermission);

        return redirect()->back()->with('success', 'Data berhasil disimpan');;
    }
}
