<?php

namespace App\Http\Controllers\LandingPage;

use App\Http\Controllers\Controller;
use App\Models\Setting\Faq;
use App\Models\Setting\JenisPajak;
use App\Models\Setting\JenisPelayanan;
use App\Models\Setting\PemdaModel;
use Illuminate\Http\Request;

class LandingpageController extends Controller
{
    // public function index()
    // {
    //     $param = [
    //         'dataPemda' => PemdaModel::get()->first(),
    //         'dataFaq' => Faq::get()
    //     ];

    //     return view('layouts.landing', $param);
    // }

    public function index()
    {
        $param = [
            'pemda' => PemdaModel::get()->first(),
        ];
        return view('auth.login', $param);
    }
    public function jenisPelayanan(JenisPajak $pajak)
    {
        $jenisPelayananPajak = JenisPelayanan::where('s_active', true)->where('s_id_jenis_pajak', $pajak->s_id_jenis_pajak)->get();
        $html = '';
        foreach ($jenisPelayananPajak as $jenisPelayanan) {
            $persyaratan = $jenisPelayanan->persyaratan->pluck('s_nama_persyaratan')->toArray();
            $html .= '<div class="col-lg-6 features-text wow fadeInDown"><small>Jenis Pajak
            <span class="label label-danger">' . $pajak->s_nama_singkat_pajak . '</span>
            </small>
            <h2>' . $jenisPelayanan->s_nama_jenis_pelayanan . '</h2>
            <p></p>
            <p>' . $jenisPelayanan->s_keterangan_jenis_pelayanan . '<br><br>
            <strong>Syarat - syarat untuk mengajukan pelayanan sebagai berikut :</strong><br>-
            ' . implode("<br>- ", $persyaratan) . '
            </p>
            </div>';
        }
        $dataPemda = PemdaModel::get()->first();

        return view('layouts.jenis-layanan', compact('html', 'dataPemda'));
    }
}
