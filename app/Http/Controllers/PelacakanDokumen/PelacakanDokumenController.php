<?php

namespace App\Http\Controllers\PelacakanDokumen;

use App\Helpers\DatagridHelpers;
use App\Helpers\PelayananHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\Pelayanan\PelayananTrait;
use App\Models\Pelayanan\Pelayanan;
use App\Models\Setting\JenisPajak;
use App\Models\Setting\JenisPelayanan;
use App\Models\Setting\Pegawai;
use App\Models\Status\StatusVerifikasi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use stdClass;

class PelacakanDokumenController extends Controller
{
    use PelayananTrait;

    public function index()
    {
        return view('pelacakan-dokumen.index', [
            'jenispajak' => JenisPajak::isActive()->get(),
            'jenispelayanan' => JenisPelayanan::get(),
            'datapegawai' => Pegawai::get(),
            'statusVerifikasi' => StatusVerifikasi::get()
        ]);
    }

    public function datagrid(Request $request)
    {
        $pelayanan = $this->datagridTrackingPelayanan($request->filter, $request->pagination, $request->sorting);
        $dataArr = [];

        foreach ($pelayanan as $v) {
            $dataArr[] = [
                'jenisPajak' => '<strong class="text-blue text-bold">' . $v->nama_pajak . '</strong><br>',
                'jenisLayanan' => '<strong class="text-red text-bold">' . $v->nama_jenis . '</strong><br>',
                'tglPermohonan' => date('d-m-Y', strtotime($v->t_tgl_pelayanan)),
                'noPelyanan' => PelayananHelper::formatNoPelayanan(date('Y', strtotime($v->t_tgl_pelayanan)), $v->t_id_jenis_pajak, $v->t_no_pelayanan),
                'nikPemohon' => $v->t_nik_pemohon,
                'namaPemohon' => $v->t_nama_pemohon,
                'status' => $this->statusPelayanan($v),
                'actionList' => [
                    [
                        'actionName' => 'tracking-dokumen',
                        // 'actionUrl' => "javascript:showTracking(" . $v->t_id_pelayanan . ")",
                        'actionUrl' => "tracking/lihat/" . $v->uuid,
                        'actionTitle' => 'Edit Pelayanan',
                        'actionActive' => true
                    ],
                ]
            ];
        }

        return (new DatagridHelpers)->getSuccessResponse($dataArr, $pelayanan);
    }

    public function lihat(Pelayanan $pelayanan)
    {
        $data = $this->dataTrackingPelayanan($pelayanan->t_id_pelayanan);
        $data->nop = (implode(".", array(
            $data->kd_propinsi,
            $data->kd_dati2,
            $data->kd_kecamatan,
            $data->kd_kelurahan,
            $data->kd_blok,
            $data->no_urut,
            $data->kd_jns_op
        )));
// // //perihal tolak persyaratan di verifikasi berkas.
        $persyaratan = $this->getAllIdSyaratUpload($pelayanan->t_id_pelayanan);
        $arr_ceklist = explode(',', $data->t_checklist_persyaratan);
        $checksyarat = is_null($data->t_checklist_persyaratan) ? $persyaratan->toArray() : array_diff($persyaratan->toArray(), $arr_ceklist);
        $getSyaratditolak = $this->getAllSyaratDitolak($checksyarat);

        $surat_keputusan =  $this->getAllHasilPelayananById($pelayanan->t_id_pelayanan);

        $tglPelayanan = Carbon::createFromFormat('Y-m-d', $data->t_tgl_pelayanan)->isoFormat('D MMMM Y');
        $tglSelesai = Carbon::createFromFormat('Y-m-d H:i:s', $data->t_tgl_perkiraan_selesai)->isoFormat('D MMMM Y');
        $data->t_tgl_pelayanan = $tglPelayanan;
        $data->t_tgl_perkiraan_selesai = $tglSelesai;
        $data->t_nop = (isset($data->t_nop)) ? PelayananHelper::formatStringToNop($data->t_nop) : '-';
        $data->alamat = $data->t_jalan_pemohon . ', Blok : ' . (!empty($data->t_blokkav) ? $data->t_blokkav : '-') . ', RT/RW : ' . $data->t_rt_pemohon . '/' . $data->t_rw_pemohon;
        $data->statusPermohonan = "Permohonan anda telah berhasil diajukan ke verifikator berkas.";
        $data->keteranganPermohonan = "Keterangan : -"; //". (!empty($data->t_keterangan) ? $data->t_keterangan : '-') ."
        $data->nopbaru = "Dengan Nop baru : ".$data->nop."";
        
        if ($data->t_no_pelayanan != null) {
            if ($data->t_id_verifikasi != null) {
                $statusverif = ($data->t_id_status_verifikasi != null) ? " dan " . strtolower($data->nama_verifikasi) . "" : ".";
                $data->header_verifikasi = "Permohonan anda telah diverifikasi" . $statusverif . "";
                $data->status_verifikasi = "Status : " . $data->nama_verifikasi . "";
                $data->keterangan_verifikasi = "Keterangan : " . $data->keterangan_verifikasi . " ";
                $data->created_verifikasi = $data->created_verifikasi;
                if ($data->t_id_validasi != null) {
                    $statusvalid = ($data->t_id_status_validasi != null) ? " dan " . $data->nama_status_validasi . "" : ".";
                    $data->header_validasi = "Permohonan anda telah divalidasi" . $statusvalid . "";
                    $data->status_validasi = "Status : " . $data->nama_status_validasi . "";
                    $data->t_keterangan_validasi = "Keterangan : " . $data->t_keterangan_validasi . "";
                    $data->created_validasi = $data->created_validasi;
                    $data->aktif_validasi = 1;
                    if($data->t_id_status_validasi == 3){
                        $data->aktif_hasil_pelayanan = "";
                    }else{
                        if($data->t_id_status_validasi ==  5){
                            if($data->t_id_status_verifikasi_lapangan = 1){
                                $data->aktif_verlap = 1;
                                $data->header_verlap = "Menunggu proses verifikasi lapangan...";
                                $data->t_keterangan_verlap = "";
                                $data->status_verlap = "";
                                $data->aktif_hasil_pelayanan = "";
                            // }else
                            // if($data->t_id_status_verifikasi_lapangan = 2){
                            //     $data->header_verlap = "Permohonan anda telah dilakukan Verifikasi Lapangan";
                            //     $data->status_verlap = "";
                            //     $data->keterangan_verlap = "";
                            //     $data->created_verlap = "";
                            //     $data->aktif_verlap = "1";
                            //     $data->header_validasi = "Menunggu proses validasi...";
                            //     $data->aktif_validasi = "1";
                            //     $data->aktif_hasil_pelayanan = "";
                            }else{
                                if(count($surat_keputusan) <= 0){
                                    $data->header_hasil_pelayanan = "Menunggu proses Upload Hasil Pelayanan...";
                                    $data->created_hasil_pelayanan = "";
                                    $data->aktif_hasil_pelayanan = 1;
                                }else{
                                    $data->header_hasil_pelayanan = "Selesai Pelayanan";
                                    $data->created_hasil_pelayanan = $surat_keputusan[0]->created_at;
                                    $data->link_hasil_pelayanan = "";
                                    $data->aktif_hasil_pelayanan = 1;
                                }
                            }
                        }
                        if($data->t_id_status_verifikasi_lapangan == 2 && $data->t_id_status_validasi == 6){
                            $data->header_verlap = "Permohonan anda telah dilakukan Verifikasi Lapangan";
                            $data->status_verlap = "Status : " . $data->nama_status_verlap." dan Menunggu Validasi Ulang.";
                            $data->keterangan_verlap = "Keterangan : " . $data->keterangan_verifikasi_lapangan . "";
                            $data->created_verlap = $data->created_verlap;
                            $data->aktif_verlap = "1";
                            $data->header_validasi = "Menunggu proses validasi ke ulang.";
                            $data->aktif_validasi = "1";
                            $data->aktif_hasil_pelayanan = "";
                        }else
                        if(count($surat_keputusan) <= 0){
                            if($data->t_id_status_validasi !==  5){
                                $data->header_hasil_pelayanan = "Menunggu proses Upload Hasil Pelayanan..";
                                $data->created_hasil_pelayanan = "";
                                $data->aktif_hasil_pelayanan = 1;
                            }
                        }else{
                            $data->header_hasil_pelayanan = "Selesai Pelayanan";
                            $data->created_hasil_pelayanan = $surat_keputusan[0]->created_at;
                            $data->link_hasil_pelayanan = "";
                            $data->aktif_hasil_pelayanan = 1;
                            $data->nopnya = ($data->t_id_jenis_pelayanan == 1) ? $data->nopbaru : '';
                        }
                    }
                }else{
                    if($data->t_id_status_verifikasi_lapangan == 3){
                        $data->aktif_verlap = "";
                        $data->aktif_validasi = "";
                    }else if($data->t_id_status_verifikasi == 5){
                        // for (UploadDokumenPendukung row : uploadDokumenPendukung) {
                        //     dokumenPendukung = row.getLokasiFile();
                        //     namaFilePendukung = "Nomor Dokumen: " + row.getNomorDokumen() + ", Tanggal : "
                        //             + FormatDate.byPatternID(row.getTanggalDokumen(), "dd MMMM yyyy");
                        // }
                        // $data->filePendukung = "Nomor Dokumen: " + + ", Tanggal : ";

                        if ($data->id_verlap != null) {
                            $data->header_verlap = "Permohonan anda telah dilakukan Verifikasi Lapangan";
                            $data->status_verlap = "";
                            $data->keterangan_verlap = "";
                            $data->created_verlap = "";
                            $data->aktif_verlap = "";
                            $data->header_validasi = "Menunggu proses validasi...";
                            $data->aktif_validasi = "";
                            $data->aktif_hasil_pelayanan = "";
                        } else if($data->t_id_status_validasi == 5){
                            $data->header_validasi = "Menunggu proses validasi...";
                            $data->header_verlap = "Menunggu proses verifikasi lapangan...";
                            $data->status_verlap = "";
                            $data->keterangan_verlap = "";
                            $data->created_verlap = "";
                            $data->aktif_verlap = 1;
                            $data->aktif_validasi = "";
                            $data->aktif_hasil_pelayanan = "";
                        }
                        else {
                            $data->header_validasi = "Menunggu proses validasi...";
                            $data->status_validasi = "";
                            $data->keterangan_validasi = "";
                            $data->created_validasi = "";
                            $data->aktif_verlap = "";
                            $data->aktif_validasi = 1;
                            $data->aktif_hasil_pelayanan = "";
                        }
                    }else if($data->t_id_status_verifikasi == 3) {
                        $syarattolak = $getSyaratditolak;
                        $data->alasan_ditolak = $syarattolak;
                    } else {
                        $data->header_validasi = "Menunggu proses validasi...";
                        $data->status_validasi = "";
                        $data->keterangan_validasi = "";
                        $data->created_validasi = "";
                        $data->aktif_verlap = "";
                        $data->aktif_validasi = 1;
                        $data->aktif_hasil_pelayanan = "";
                    }
                }
            } else {
                $data->header_verifikasi = "Menunggu proses verifikasi...";
                $data->status_verifikasi = "";
                $data->keterangan_verifikasi = "";
                $data->created_verifikasi = "";
                $data->aktif_verlap = "";
                $data->aktif_validasi = "";
                $data->aktif_hasil_pelayanan = "";
            }
        }

        return view('pelacakan-dokumen.lihat', [
            'data' => $data,
            'hasilPelayanan' => isset($surat_keputusan) ? $surat_keputusan : ''
        ]);
    }

    public function syaratUpload(Int $id)
    {

        $params = $this->getSyaratByIdPelayanan($id);

        return $params;
    }
}
