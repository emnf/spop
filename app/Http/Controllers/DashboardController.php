<?php

namespace App\Http\Controllers;

use App\Helpers\CountHelper;
use App\Models\Pbb\Sppt;
use App\Models\Pelayanan\Pelayanan;
use App\Models\Setting\PemdaModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        //  phpinfo(); die;
        $user = auth()->user();
        // $notification = $user->unreadNotifications;

        // foreach ($notification as $key => $value) {
        //     dump($value->data['message']);
        // }
        // dd();


        $param = array_merge(
            (new CountHelper)->countPelayanan(),
            (new CountHelper)->countPelayanPerTahun(),
            (new CountHelper)->countPerBulan()
        );
        $param['pemda'] = PemdaModel::first();

        return view('main.dashboard', $param);
    }
}
