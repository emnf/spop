<?php

namespace App\Http\Controllers\Notifikasi;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;

class NotifikasiController extends Controller
{
    public function getNotifikasi()
    {
        $user = Auth::user();

        $response = [];
        foreach ($user->unreadNotifications as $key => $notif) {

            $urlTarget = '/tracking';
            if ($notif->type == 'App\Notifications\PendaftaranNotify') {
                $urlTarget = '/verifikasi';
            }

            $response[] = [
                'messageId' => $notif->id,
                'urlTarget' => $urlTarget,
                'messageData' => [
                    'message' => $notif->data['message'],
                    'tanggal' => isset($notif->data['tanggal'])
                        ?  Carbon::parse($notif->data['tanggal'])->format('d-m-Y')
                        : ''
                ],
            ];
        }

        return response()->json($response);
    }

    public function markAsRead(Request $request)
    {
        $notif = auth()->user()->notifications()->where('id', $request->id)->first();

        if ($notif) {
            $notif->markAsRead();
        }

        return response()->json(['success' => true], 200);
    }
}
