<?php

namespace App\Http\Requests\Pelayanan;

use Illuminate\Foundation\Http\FormRequest;

class StoreKeberatanPajakRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "t_id_pelayanan" => "nullable",
            "t_id_jenis_pelayanan" => "nullable",
            "t_id_jenis_pajak" => "nullable",
            "t_nik_pemohon" => "required",
            "t_nama_pemohon" => "required",
            "t_jalan_pemohon" => "required",
            "t_rt_pemohon" => "required|numeric|digits:3",
            "t_rw_pemohon" => "required|numeric|digits_between:2,3",
            "t_kelurahan_pemohon" => "required",
            "t_kecamatan_pemohon" => "required",
            "t_kabupaten_pemohon" => "required",
            "t_kode_pos_pemohon" => "nullable|digits:5",
            "t_email_pemohon" => "nullable|email",
            "t_no_hp_pemohon" => "nullable|numeric|digits_between:5,13",
            "t_id_wp" => "nullable",
            "t_id_op" => "nullable",
            "t_nop" => "required",
            "t_tahun_nop" => "required|numeric|digits:4",
            "t_rt_op" => "required|numeric|digits:3",
            "t_rw_op" => "required|numeric|digits_between:2,3",
            "kd_blok_kav_op" => "nullable",
            "t_kelurahan_op" => "required",
            "t_kecamatan_op" => "required",
            "t_luas_tanah" => "required",
            "t_luas_bangunan" => "nullable",
            "t_nama_wp" => "required",
            "t_rt_wp" => "required|numeric|digits:3",
            "t_rw_wp" => "required|numeric|digits_between:2,3",
            "t_jalan_wp" => "required",
            "kd_blok_kav_wp" => "nullable",
            "t_kelurahan_wp" => "required",
            "t_kecamatan_wp" => "required",
            "t_kabupaten_wp" => "required",
            "t_tgl_pelayanan" => "required|date",
            "t_keterangan" => "required",
        ];
    }

    public function attributes()
    {
        return [
            "t_nik_pemohon" => "NIK Pemohon",
            "t_nama_pemohon" => "Nama Pemohon",
            "t_jalan_pemohon" => "Alamat Jalan",
            "t_rt_pemohon" => "RT Pemohon",
            "t_rw_pemohon" => "RW Pemohon",
            "t_kelurahan_pemohon" => "Kelurahan Pemohon",
            "t_kecamatan_pemohon" => "Kecamatan Pemohon",
            "t_kabupaten_pemohon" => "Kabupaten Pemohon",
            "t_kode_pos_pemohon" => "Kodepos",
            "t_email_pemohon" => "Email Pemohon",
            "t_no_hp_pemohon" => "No Hp Pemohon",
            "t_nop" => "NOP",
            "t_tahun_nop" => "Tahun Ketetapan",
            "t_tgl_pelayanan" => "Tanggal Pelayanan",
            "t_keterangan" => "Keterangan",
        ];
    }
}
