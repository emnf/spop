<?php

namespace App\Http\Requests\Pelayanan;

use Illuminate\Foundation\Http\FormRequest;

class StoreMutasiWpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            // pelayanan
            't_id_pelayanan' => '',
            't_no_pelayanan' => '',
            't_tgl_pelayanan' => '',
            't_id_jenis_pelayanan' => '',
            't_nop' => '',
            't_nama_pemohon' => '',
            't_blokkav' => '',
            't_nik_pemohon' => '',
            't_jalan_pemohon' => '',
            't_rt_pemohon' => '',
            't_rw_pemohon' => '',
            't_kelurahan_pemohon' => '',
            't_kecamatan_pemohon' => '',
            't_kabupaten_pemohon' => '',
            't_kode_pos_pemohon' => '',
            't_no_hp_pemohon' => '',
            't_email_pemohon' => '',
            't_keterangan' => '',
            't_id_jenis_pajak' => '',
            't_tahun_pajak' => '',
            't_nourut' => '',
            't_besar_pengurangan' => '',
            't_multiple_tahun' => '',
            't_tgl_permintaan' => '',
            't_jumlah_angsuran' => '',
            't_tahun_pajak_kompensa' => '',
            't_nop_kompensasi' => '',
            't_no_sk' => '',
            't_tgl_sk' => '',

            // wp lama
            't_id_wp_lama' => '',
            't_id_pelayanan' => '',
            't_nik_wp_lama'=> '',
            't_nama_wp_lama'=> '',
            't_jalan_wp_lama'=> '',
            't_rt_wp_lama'=> '',
            't_rw_wp_lama'=> '',
            't_kelurahan_wp_lama'=> '',
            't_kecamatan_wp_lama'=> '',
            't_kabupaten_wp_lama'=> '',
            't_no_hp_wp_lama'=> '',

            // wp baru
            't_id_wp'=> '',
            't_id_pelayanan'=> '',
            't_nop'=> '',
            't_nama_wp'=> '',
            't_nik_wp'=> '',
            't_jalan_wp'=> '',
            't_rt_wp'=> '',
            't_rw_wp'=> '',
            't_kelurahan_wp'=> '',
            't_kecamatan_wp'=> '',
            't_kabupaten_wp'=> '',
            't_no_hp_wp'=> ''

        ];
    }
}
