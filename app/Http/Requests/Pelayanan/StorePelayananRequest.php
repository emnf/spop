<?php

namespace App\Http\Requests\Pelayanan;

use Illuminate\Foundation\Http\FormRequest;

class StorePelayananRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            't_tgl_pelayanan' => ['required'],
            't_id_jenis_pelayanan' => ['required'],
            't_nop' => ['required'],
            't_nama_pemohon' => ['required'],
            't_blokkav' => ['required'],
            't_nik_pemohon' => ['required'],
            't_jalan_pemohon' => ['required'],
            't_rt_pemohon' => ['required'],
            't_rw_pemohon' => ['required'],
            't_kelurahan_pemohon' => ['required'],
            't_kecamatan_pemohon' => ['required'],
            't_kabupaten_pemohon' => ['required'],
            't_kode_pos_pemohon' => ['required'],
            't_no_hp_pemohon' => ['required'],
            't_email_pemohon' => ['required'],
            't_keterangan' => ['required'],
            't_id_jenis_pajak' => ['required'],
        ];

        return $rules;
    }
}
