<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class PegawaiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            's_nama_pegawai' => ['required', 'string'],
            's_jabatan_pegawai' => ['required', 'string'],
            's_pangkat_pegawai' => ['required', 'string'],
            's_nip_pegawai' => ['required', 'string']
        ];
    }
}