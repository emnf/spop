<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class JenisPajakRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            's_nama_jenis_pajak' => ['required', 'string'],
            's_nama_singkat_pajak' => ['required', 'string'],
            's_id_kategori_pajak' => ['required'],
            's_active' => ['required']
        ];
    }
}
