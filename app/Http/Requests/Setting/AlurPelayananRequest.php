<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class AlurPelayananRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            's_judul_alur' => ['required', 'string'],
            's_deskripsi_alur' => ['required', 'string'],
            's_langkah_alur' => ['required', 'string']
        ];
    }
}
