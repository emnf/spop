<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required',
            'new_password' => 'required|min:8|max:100',
            'confirm_password' => 'required|same:new_password',
        ];
    }

    public function messages()
    {
        return [
            'old_password.required' => 'Password Lama Harus diisi',
            'new_password.required' => 'Password Baru Harus diisi',
            'new_password.min' => 'Password Baru minimal 8 karakter',
            'confirm_password.required' => 'Konfirmasi Password Harus diisi',
            'confirm_password.same' => 'Konfirmasi Password tidak sama dengan password baru',
        ];
    }
}
