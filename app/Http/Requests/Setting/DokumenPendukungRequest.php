<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class DokumenPendukungRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            's_nama_dokumen_pendukung' => ['required', 'string'],
            's_id_kategori_pajak' => ['required'],
            's_id_jenis_pelayanan' => ['required']
        ];
    }
}
