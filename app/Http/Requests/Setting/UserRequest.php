<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'nohp' => 'nullable',
            'role' => 'required',
            'is_active' => 'required',
        ];

        if ($this->method() == 'POST') {
            $rules['password'] = 'required|confirmed';
            $rules['username'] = 'required|unique:users,username';
            $rules['email'] = 'required|email|unique:users,email';
        }

        if ($this->method() == 'PUT') {
            $rules['username'] = ['required', Rule::unique('users')->ignore($this->route('user')->id)];
            $rules['email'] = ['required', 'email', Rule::unique('users')->ignore($this->route('user')->id)];
        }

        if (!empty($this->input('password'))) {
            $rules['password'] = 'confirmed';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'Kolom harus diisi.',
            'username.required' => 'Kolom harus diisi.',
            'username.unique' => 'Username ini sudah digunakan',
            'email.required' => 'Kolom harus diisi.',
            'email.unique' => 'Email ini sudah digunakan',
            'password.required' => 'Kolom harus diisi.',
            'nohp.required' => 'Kolom harus diisi.',
            'role.required' => 'Kolom harus pilih.'
        ];
    }
}
