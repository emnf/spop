<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class UserManualRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            's_nama_menu' => ['required'],
            's_nama_file' => ['required'],
        ];
        
        if ($this->input('s_id_usermanual') == null) {
            $rule['s_lokasi_file'] = 'required';
        }

        return $rule;
    }
}
