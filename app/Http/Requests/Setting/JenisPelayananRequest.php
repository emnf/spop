<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class JenisPelayananRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            's_nama_jenis_pelayanan' => ['required', 'string'],
            's_id_jenis_pajak' => ['required'],
            's_active' => ['required'],
            's_keterangan_jenis_pelayanan' => ['required', 'string'],
            's_order' => ['required'],
            's_lunas_pbb' => ['required'],
        ];
    }
}
