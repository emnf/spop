<?php

namespace App\Http\Traits;

trait Terbilang
{
    function kekata($x)
    {
        $x = abs($x);
        $angka = array(
            "", "Satu", "Dua", "Tiga", "Empat", "Lima",
            "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas"
        );
        $temp = "";
        if ($x < 12) {
            $temp = " " . $angka[$x];
        } else if ($x < 20) {
            $temp = $this->kekata($x - 10) . " Belas";
        } else if ($x < 100) {
            $temp = $this->kekata($x / 10) . " Puluh" . $this->kekata($x % 10);
        } else if ($x < 200) {
            $temp = " Seratus" . $this->kekata($x - 100);
        } else if ($x < 1000) {
            $temp = $this->kekata($x / 100) . " Ratus" . $this->kekata($x % 100);
        } else if ($x < 2000) {
            $temp = " Seribu" . $this->kekata($x - 1000);
        } else if ($x < 1000000) {
            $temp = $this->kekata($x / 1000) . " Ribu" . $this->kekata($x % 1000);
        } else if ($x < 1000000000) {
            $temp = $this->kekata($x / 1000000) . " Juta" . $this->kekata($x % 1000000);
        } else if ($x < 1000000000000) {
            $temp = $this->kekata($x / 1000000000) . " Milyar" . $this->kekata(fmod($x, 1000000000));
        } else if ($x < 1000000000000000) {
            $temp = $this->kekata($x / 1000000000000) . " Trilyun" . $this->kekata(fmod($x, 1000000000000));
        }
        return $temp;
    }

    function terbilang($x, $style = 4)
    {
        if ($x < 0) {
            $hasil = "MINUS " . trim($this->kekata($x));
        } else {
            $hasil = trim($this->kekata($x));
        }
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }
        return $hasil;
    }

    public function bulan($a)
    {
        $abulan = [
            [
                'bulan' => '01',
                'nama' => 'Jan'
            ],
            [
                'bulan' => '02',
                'nama' => 'Feb'
            ],
            [
                'bulan' => '03',
                'nama' => 'Mar'
            ],
            [
                'bulan' => '04',
                'nama' => 'Apr'
            ],
            [
                'bulan' => '05',
                'nama' => 'Mei'
            ],
            [
                'bulan' => '06',
                'nama' => 'Jun'
            ],
            [
                'bulan' => '07',
                'nama' => 'Jul'
            ],
            [
                'bulan' => '08',
                'nama' => 'Agu'
            ],
            [
                'bulan' => '09',
                'nama' => 'Sep'
            ],
            [
                'bulan' => '10',
                'nama' => 'Okt'
            ],
            [
                'bulan' => '11',
                'nama' => 'Nov'
            ],
            [
                'bulan' => '12',
                'nama' => 'Des'
            ],
        ];
        $key = array_search($a, array_column($abulan, 'bulan'));
        return $abulan[$key];
    }
}
