<?php

namespace App\Http\Traits\Esign;

use App\Models\Esign\EsignConfig;
use App\Models\Pbb\PenetapanTerseleksi;
use App\Models\Pelayanan\Pelayanan;
use CURLFile;
use GuzzleHttp\Client;
use GuzzleHttp\Utils;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Psr7;
use Illuminate\Support\Facades\DB;

// use Illuminate\Support\Facades\Request;

trait EsignTraitPenetapan
{
    public function datagrid($filter, $pagination, $sorting, $helper)
    {

        $data = new PenetapanTerseleksi();
        $data = $data->leftjoin('users', 'users.id', 't_penetapan_terseleksi.created_by');
        // if ($filter['nip'] != null) {
        //     $data = $data->where('nip_perekam', $filter['nip']);
        // }

        // if ($filter['nop'] != null) {
        //     $nop = $helper->parseNop($filter['nop']);

        //     $data = $data->where([
        //         ['kd_propinsi', '=', $nop['KD_PROPINSI']],
        //         ['kd_dati2', '=', $nop['KD_DATI2']],
        //         ['kd_kecamatan', '=', $nop['KD_KECAMATAN']],
        //         ['kd_kelurahan', '=', $nop['KD_KELURAHAN']],
        //         ['kd_blok', '=', $nop['KD_BLOK']],
        //         ['no_urut', '=', $nop['NO_URUT']],
        //         ['kd_jns_op', '=', $nop['KD_JNS_OP']]
        //     ]);
        // }

        // if ($filter['tahun'] != null) {
        //     $data = $data->where('thn_pajak_sppt', $filter['tahun']);
        // }

        // if ($filter['user'] != null) {
        //     $data = $data->where('users.name', 'like', '%' . $filter['user'] . '%');
        // }
        $data = $data->paginate($pagination['pageSize'], ['*'], 'page', $pagination['pageNumber'] + 1);
        return $data;
    }

    public function datagridSudahEsign($filter, $pagination, $sorting, $status = null)
    {
        $data = PenetapanTerseleksi::leftjoin('t_esign_penetapan', function ($join) {
            $join->on(DB::raw("CONCAT(
                kd_propinsi,'.',
                kd_dati2,'.',
                kd_kecamatan,'.',
                kd_kelurahan,'.',
                kd_blok,'.',
                no_urut,'.',
                kd_jns_op,'.',
                thn_pajak_sppt
            )"), '=', 't_esign_penetapan.t_nop');
        })->whereNotNull('t_esign_penetapan.t_nop');

        return $data->paginate($pagination['pageSize'], ['*'], 'page', $pagination['pageNumber'] + 1);
    }

    public function signed($fileoutput, $filesave, $nik, $passphrase, $urlQr, $w, $h, $filePathQr)
    {
        $config = EsignConfig::where('is_aktif', 1)->first();
        $authorization = sprintf('Basic %s', base64_encode(sprintf('%s:%s', $config->username, $config->password)));

        $client = new Client();
        $headers = [
            'Accept' => 'multipart/form-data',
            'Authorization' => $authorization,
        ];
        $options = [
            'multipart' => [
                [
                    'name' => 'file',
                    'contents' => Psr7\Utils::streamFor(file_get_contents(storage_path($fileoutput))),
                    'filename' => $fileoutput,
                    'headers' => [
                        'Content-Type' => 'application/pdf',
                    ],
                ],
                [
                    'name' => 'imageTTD',
                    'contents' => Psr7\Utils::streamFor(file_get_contents(storage_path('app/' . $filePathQr))),
                    'filename' => $filePathQr,
                    'headers' => [
                        'Content-Type' => 'image/png',
                    ],
                ],
                [
                    'name' => 'nik',
                    'contents' => $nik,
                ],
                [
                    'name' => 'passphrase',
                    'contents' => $passphrase,
                ],
                [
                    'name' => 'tampilan',
                    'contents' => 'visible',
                ],
                [
                    'name' => 'image',
                    'contents' => true,
                ],
                [
                    'name' => 'width',
                    'contents' => $w,
                ],
                [
                    'name' => 'height',
                    'contents' => $h,
                ],
                [
                    'name' => 'tag_koordinat',
                    'contents' => '#'
                ],
                [
                    'name' => 'location',
                    'contents' => 'Blora',
                ],
            ],
        ];
        $request = new Request('POST', 'https://esign.blorakab.go.id/api/sign/pdf', $headers);
        $res = $client->sendAsync($request, $options)->wait();
        return $res->getBody();
    }
}
