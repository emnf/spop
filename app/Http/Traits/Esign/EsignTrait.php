<?php

namespace App\Http\Traits\Esign;

use App\Models\Esign\EsignConfig;
use App\Models\Pelayanan\Pelayanan;
use CURLFile;
use GuzzleHttp\Client;
use GuzzleHttp\Utils;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Psr7;
// use Illuminate\Support\Facades\Request;

trait EsignTrait
{
    public function datagridBelumEsign($filter, $pagination, $sorting, $status = null)
    {
        $data = (new Pelayanan())->select(
            't_pelayanan.*',
            's_jenis_pajak.s_nama_jenis_pajak',
            's_jenis_pelayanan.s_nama_jenis_pelayanan',
            't_verifikasi.t_tgl_verifikasi',
            't_verifikasi.t_no_verifikasi',
            't_verifikasi.t_keterangan_verifikasi',
            't_verifikasi.t_id_status_verifikasi',
            's_status_verifikasi.s_nama_status_verifikasi',
        )
            ->leftJoin('s_jenis_pelayanan', 's_jenis_pelayanan.s_id_jenis_pelayanan', '=', 't_pelayanan.t_id_jenis_pelayanan')
            ->leftJoin('s_jenis_pajak', 's_jenis_pajak.s_id_jenis_pajak', '=', 't_pelayanan.t_id_jenis_pajak')
            ->leftJoin('t_pelayanan_approve', 't_pelayanan_approve.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_verifikasi', 't_verifikasi.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('s_status_verifikasi', 's_status_verifikasi.s_id_status_verifikasi', '=', 't_verifikasi.t_id_status_verifikasi')
            ->leftJoin('t_esign', 't_pelayanan.t_id_pelayanan', 't_esign.t_id_pelayanan')
            ->when(empty($sorting), function ($query) {
                $query->orderBy('t_tgl_pelayanan', 'desc');
            }, function ($query) use ($sorting) {
                $query->orderBy($sorting['key'], $sorting['order']);
            })

            ->when(!is_null($status), function ($query) use ($status) {
                $query->where('t_id_status_verifikasi', $status);
            })

            ->when(!empty($filter['idJenisPajak']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_id_jenis_pajak', $filter['idJenisPajak']);
            })

            ->when(!empty($filter['idJenisPelayanan']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_id_jenis_pelayanan', $filter['idJenisPelayanan']);
            })

            ->when(!empty($filter['tglPelayanan']), function ($query) use ($filter) {
                $date = explode(' - ', $filter['tglPelayanan']);
                $startDate = date('Y-m-d', strtotime($date[0]));
                $endDate = date('Y-m-d', strtotime($date[1]));
                $query->whereBetween('t_pelayanan.t_tgl_pelayanan', [$startDate, $endDate]);
            })

            ->when(!empty($filter['tglVerifikasi']), function ($query) use ($filter) {
                $date = explode(' - ', $filter['tglVerifikasi']);
                $startDate = date('Y-m-d', strtotime($date[0]));
                $endDate = date('Y-m-d', strtotime($date[1]));
                $query->whereBetween('t_verifikasi.t_tgl_verifikasi', [$startDate, $endDate]);
            })

            ->when(!empty($filter['noPelayanan']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_no_pelayanan',  'like', "%" . $filter['noPelayanan'] . "%");
            })

            ->when(!empty($filter['namaPemohon']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_nama_pemohon',  'ilike', "%{$filter['namaPemohon']}%");
            })

            ->when(!empty($filter['namaWp']), function ($query) use ($filter) {
                $query->whereRelation('wajibPajak', 't_nama_wp',  'ilike', "%{$filter['namaWp']}%");
                $query->orWhereRelation('WpLama', 't_nama_wp',  'ilike', "%{$filter['namaWp']}%");
            })

            ->when(!empty($filter['idStatusVerifikasi']), function ($query) use ($filter) {
                $query->where('t_verifikasi.t_id_status_verifikasi', $filter['idStatusVerifikasi']);
            })

            ->whereNull('t_esign.id')
            ->whereNotNull('t_id_pelayanan_approve');

        return $data->paginate($pagination['pageSize'], ['*'], 'page', $pagination['pageNumber'] + 1);
    }

    public function datagridSudahEsign($filter, $pagination, $sorting, $status = null)
    {
        $data = (new Pelayanan())->select(
            't_pelayanan.*',
            's_jenis_pajak.s_nama_jenis_pajak',
            's_jenis_pelayanan.s_nama_jenis_pelayanan',
            't_verifikasi.t_tgl_verifikasi',
            't_verifikasi.t_no_verifikasi',
            't_verifikasi.t_keterangan_verifikasi',
            't_verifikasi.t_id_status_verifikasi',
            's_status_verifikasi.s_nama_status_verifikasi',
            't_esign.uuid as document'
        )
            ->leftJoin('s_jenis_pelayanan', 's_jenis_pelayanan.s_id_jenis_pelayanan', '=', 't_pelayanan.t_id_jenis_pelayanan')
            ->leftJoin('s_jenis_pajak', 's_jenis_pajak.s_id_jenis_pajak', '=', 't_pelayanan.t_id_jenis_pajak')
            ->leftJoin('t_pelayanan_approve', 't_pelayanan_approve.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_verifikasi', 't_verifikasi.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('s_status_verifikasi', 's_status_verifikasi.s_id_status_verifikasi', '=', 't_verifikasi.t_id_status_verifikasi')
            ->leftJoin('t_esign', 't_pelayanan.t_id_pelayanan', 't_esign.t_id_pelayanan')
            ->when(empty($sorting), function ($query) {
                $query->orderBy('t_tgl_pelayanan', 'desc');
            }, function ($query) use ($sorting) {
                $query->orderBy($sorting['key'], $sorting['order']);
            })

            ->when(!is_null($status), function ($query) use ($status) {
                $query->where('t_id_status_verifikasi', $status);
            })

            ->when(!empty($filter['idJenisPajak']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_id_jenis_pajak', $filter['idJenisPajak']);
            })

            ->when(!empty($filter['idJenisPelayanan']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_id_jenis_pelayanan', $filter['idJenisPelayanan']);
            })

            ->when(!empty($filter['tglPelayanan']), function ($query) use ($filter) {
                $date = explode(' - ', $filter['tglPelayanan']);
                $startDate = date('Y-m-d', strtotime($date[0]));
                $endDate = date('Y-m-d', strtotime($date[1]));
                $query->whereBetween('t_pelayanan.t_tgl_pelayanan', [$startDate, $endDate]);
            })

            ->when(!empty($filter['tglVerifikasi']), function ($query) use ($filter) {
                $date = explode(' - ', $filter['tglVerifikasi']);
                $startDate = date('Y-m-d', strtotime($date[0]));
                $endDate = date('Y-m-d', strtotime($date[1]));
                $query->whereBetween('t_verifikasi.t_tgl_verifikasi', [$startDate, $endDate]);
            })

            ->when(!empty($filter['noPelayanan']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_no_pelayanan',  'like', "%" . $filter['noPelayanan'] . "%");
            })

            ->when(!empty($filter['namaPemohon']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_nama_pemohon',  'ilike', "%{$filter['namaPemohon']}%");
            })

            ->when(!empty($filter['namaWp']), function ($query) use ($filter) {
                $query->whereRelation('wajibPajak', 't_nama_wp',  'ilike', "%{$filter['namaWp']}%");
                $query->orWhereRelation('WpLama', 't_nama_wp',  'ilike', "%{$filter['namaWp']}%");
            })

            ->when(!empty($filter['idStatusVerifikasi']), function ($query) use ($filter) {
                $query->where('t_verifikasi.t_id_status_verifikasi', $filter['idStatusVerifikasi']);
            })

            ->whereNotNull('t_esign.id')
            ->whereNotNull('t_id_pelayanan_approve');

        return $data->paginate($pagination['pageSize'], ['*'], 'page', $pagination['pageNumber'] + 1);
    }

    public function signed($fileoutput, $filesave, $nik, $passphrase, $urlQr, $w, $h, $filePathQr)
    {
        $config = EsignConfig::where('is_aktif', 1)->first();
        $authorization = sprintf('Basic %s', base64_encode(sprintf('%s:%s', $config->username, $config->password)));

        $client = new Client();
        $headers = [
            'Accept' => 'multipart/form-data',
            'Authorization' => $authorization,
        ];
        $options = [
            'multipart' => [
                [
                    'name' => 'file',
                    'contents' => Psr7\Utils::streamFor(file_get_contents(storage_path($fileoutput))),
                    'filename' => $fileoutput,
                    'headers' => [
                        'Content-Type' => 'application/pdf',
                    ],
                ],
                [
                    'name' => 'imageTTD',
                    'contents' => Psr7\Utils::streamFor(file_get_contents(storage_path('app/' . $filePathQr))),
                    'filename' => $filePathQr,
                    'headers' => [
                        'Content-Type' => 'image/png',
                    ],
                ],
                [
                    'name' => 'nik',
                    'contents' => $nik,
                ],
                [
                    'name' => 'passphrase',
                    'contents' => $passphrase,
                ],
                [
                    'name' => 'tampilan',
                    'contents' => 'visible',
                ],
                [
                    'name' => 'image',
                    'contents' => true,
                ],
                [
                    'name' => 'width',
                    'contents' => $w,
                ],
                [
                    'name' => 'height',
                    'contents' => $h,
                ],
                [
                    'name' => 'tag_koordinat',
                    'contents' => '#'
                ],
                [
                    'name' => 'location',
                    'contents' => 'Blora',
                ],
            ],
        ];
        $request = new Request('POST', 'https://esign.blorakab.go.id/api/sign/pdf', $headers);
        $res = $client->sendAsync($request, $options)->wait();
        return $res->getBody();
    }
}
