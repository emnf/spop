<?php

namespace App\Http\Traits\Pelayanan;

use App\Models\Hasil\HasilPelayanan;
use App\Models\Pbb\Sppt;
use App\Models\Pelayanan\Op;
use App\Models\Pelayanan\OpLama;
use App\Models\Pelayanan\Pelayanan;
use App\Models\Pelayanan\PersyaratanPelayanan;
use App\Models\Pelayanan\Wp;
use App\Models\Pelayanan\WpLama;
use App\Models\Setting\JenisPelayanan;
use App\Models\Setting\Persyaratan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

trait PelayananTrait
{
    public function storePelayanan($data)
    {
        $pelayanan = new Pelayanan;
        try {
            $dataPelayanan = [
                "t_tgl_pelayanan" => date('Y-m-d', strtotime($data->t_tgl_pelayanan)),
                "t_id_jenis_pelayanan" => $data->t_id_jenis_pelayanan,
                "t_nop" => isset($data->t_nop) ? str_replace(".", "", $data->t_nop) : null,
                "t_nama_pemohon" => $data->t_nama_pemohon,
                "t_nik_pemohon" => $data->t_nik_pemohon,
                "t_jalan_pemohon" => $data->t_jalan_pemohon,
                "t_rt_pemohon" => $data->t_rt_pemohon,
                "t_rw_pemohon" => $data->t_rw_pemohon,
                "t_kelurahan_pemohon" => $data->t_kelurahan_pemohon,
                "t_kecamatan_pemohon" => $data->t_kecamatan_pemohon,
                "t_kabupaten_pemohon" => $data->t_kabupaten_pemohon,
                "t_kode_pos_pemohon" => $data->t_kode_pos_pemohon,
                "t_no_hp_pemohon" => $data->t_no_hp_pemohon,
                "t_email_pemohon" => $data->t_email_pemohon ?? null,
                "t_keterangan" => $data->t_keterangan,
                "t_id_jenis_pajak" => $data->t_id_jenis_pajak,
                "t_tahun_pajak" => $data->t_tahun_pajak ?? $data->t_tahun_nop,
                "t_tgl_permintaan" => is_null($data->t_tgl_permintaan) ? null : date('Y-m-d', strtotime($data->t_tgl_permintaan)),
                "t_multiple_tahun" => isset($data->t_multiple_tahun) ? $data->t_multiple_tahun : null,
                "t_besar_pengurangan" => $data->t_besar_pengurangan ?? null,
                "created_by" => Auth::user()->id,
                "uuid" => Uuid::uuid4()->getHex()
            ];

            // $pelayanan->updateOrCreate(
            //     ['t_id_pelayanan' => $data->t_id_pelayanan],
            //     $dataPelayanan
            // );


            if (is_null($data->t_id_pelayanan)) {
                return $pelayanan->create($dataPelayanan);
            } else {
                $pelayanan->where('t_id_pelayanan', $data->t_id_pelayanan)->update($dataPelayanan);
                return $pelayanan->where('t_id_pelayanan', $data->t_id_pelayanan)->first();
            }
        } catch (\Throwable $th) {
            report($th);
            dd($th);
        }
    }

    public function cekSatatusPersyaratanPelayanan($idPelayanan, $idJenisPelayanan)
    {
        $syaratUpload = PersyaratanPelayanan::where('t_id_pelayanan', $idPelayanan)
            ->orderBy('t_id_persyaratan')->pluck('s_id_persyaratan');
        $persyaratan = Persyaratan::where('s_id_jenis_pelayanan', $idJenisPelayanan)
            ->where('s_is_optional', false)
            ->orderBy('s_id_jenis_pelayanan')->pluck('s_id_persyaratan');

        $result = array_diff($persyaratan->toArray(), $syaratUpload->toArray());

        return (count($result) > 0) ? false : true;
    }

    public function datagridTrackingPelayanan($filter, $pagination, $sorting)
    {
        $response = DB::connection('pgsql')->table('t_pelayanan')
            ->selectRaw('t_pelayanan.*,
                s_jenis_pelayanan.s_nama_jenis_pelayanan as nama_jenis,
                s_jenis_pajak.s_nama_singkat_pajak as nama_pajak,t_tgl_perkiraan_selesai,
                t_tgl_verifikasi,t_no_verifikasi,
                t_id_status_verifikasi,
                t_tgl_validasi,
                t_id_status_validasi,t_tgl_verifikasi_lapangan,
                t_id_status_verifikasi_lapangan,
                t_pelayanan_approve.t_id_pelayanan_approve')

            ->leftJoin('s_jenis_pelayanan', 's_jenis_pelayanan.s_id_jenis_pelayanan', '=', 't_pelayanan.t_id_jenis_pelayanan')
            ->leftJoin('s_jenis_pajak', 's_jenis_pajak.s_id_jenis_pajak', '=', 't_pelayanan.t_id_jenis_pajak')
            ->leftJoin('t_pelayanan_approve', 't_pelayanan_approve.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_verifikasi', 't_verifikasi.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_validasi', 't_validasi.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_verifikasi_lapangan', 't_verifikasi_lapangan.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_hasil_pelayanan', 't_hasil_pelayanan.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->whereNotNull('t_pelayanan_approve.t_id_pelayanan_approve')
            ->when(empty($sorting), function ($query) {
                $query->orderBy('t_tgl_pelayanan', 'desc');
            }, function ($query) use ($sorting) {
                $query->orderBy($sorting['key'], $sorting['order']);
            })
            ->when(!empty($filter['idJenisPelayanan']), function ($q) use ($filter) {
                $q->where('t_pelayanan.t_id_jenis_pelayanan', $filter['idJenisPelayanan']);
            })
            ->when(!empty($filter['idJenisPajak']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_id_jenis_pajak', $filter['idJenisPajak']);
            })
            ->when(!empty($filter['tglPelayanan']), function ($query) use ($filter) {
                $date = explode(' - ', $filter['tglPelayanan']);
                $startDate = date('Y-m-d', strtotime($date[0]));
                $endDate = date('Y-m-d', strtotime($date[1]));
                $query->whereBetween('t_pelayanan.t_tgl_pelayanan', [$startDate, $endDate]);
            })
            ->when(!empty($filter['noPelayanan']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_no_pelayanan',  'like', "%" . $filter['noPelayanan'] . "%");
            })
            ->when(!empty($filter['nikPemohon']), function ($q) use ($filter) {
                $q->where('t_pelayanan.t_nik_pemohon', 'like', "%" . $filter['nikPemohon'] . "%");
            })
            ->when(in_array('Wajib Pajak', auth()->user()->getRoleNames()->toArray()), function ($q) {
                $q->where('t_pelayanan.created_by', auth()->user()->id);
            })->when(!empty($filter['status']), function ($q) use ($filter) {
                if ($filter['status'] == 1) {
                    $q->whereNull('t_pelayanan_approve.t_id_pelayanan_approve');
                }

                if ($filter['status'] == 2) {
                    $q->whereNotNull('t_pelayanan_approve.t_id_pelayanan_approve');
                    // $q->where('t_verifikasi.t_id_status_verifikasi', '!=', 3);
                    // $q->whereNull('t_validasi');
                    // $q->whereNull('t_hasil_pelayanan.t_id_sk');
                    $q->whereRaw("t_pelayanan.t_id_pelayanan NOT IN ( SELECT t_id_pelayanan FROM t_verifikasi WHERE t_id_status_verifikasi = 3) AND t_pelayanan.t_id_pelayanan NOT IN ( SELECT t_id_pelayanan FROM t_validasi WHERE t_id_status_validasi = 4)");
                }

                if ($filter['status'] == 3) {
                    $q->where('t_verifikasi.t_id_status_verifikasi', 3);
                }

                if ($filter['status'] == 4) {
                    $q->where('t_validasi.t_id_status_validasi', 4);
                    $q->whereNull('t_hasil_pelayanan.t_id_sk');
                }

                if ($filter['status'] == 5) {
                    $q->whereNotNull('t_hasil_pelayanan.t_id_sk');
                }
            })
            // ->when(!empty($filter['tglValidasi']), function ($query) use ($filter) {
            //     $date = explode(' - ', $filter['tglValidasi']);
            //     $startDate = date('Y-m-d', strtotime($date[0]));
            //     $endDate = date('Y-m-d', strtotime($date[1]));
            //     $query->whereBetween('t_verifikasi.t_tgl_verifikasi', [$startDate, $endDate]);
            // })
            // ->when(!empty($filter['tglSelesaiVerlab']), function ($query) use ($filter) {
            //     $date = explode(' - ', $filter['tglSelesaiVerlab']);
            //     $startDate = date('Y-m-d', strtotime($date[0]));
            //     $endDate = date('Y-m-d', strtotime($date[1]));
            //     $query->whereBetween('t_verifikasi_lapangan.t_tgl_verifikasi_lapangan', [$startDate, $endDate]);
            // })
            ->when(!empty($filter['namaPemohon']), function ($q) use ($filter) {
                $q->where('t_pelayanan.t_nama_pemohon', 'ilike', "%{$filter['namaPemohon']}%");
            });


        return $response->paginate($pagination['pageSize'], ['*'], 'page', $pagination['pageNumber'] + 1);
    }

    public function statusPelayanan($pelayanan)
    {
        $hasilPelayanan = HasilPelayanan::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->first();

        $status = '<span class="btn btn-secondary btn-xs"><i class="fas fa-desktop">&nbsp;Proses</i></span>';

        if (is_null($pelayanan->t_id_pelayanan_approve)) {
            $status = '<span class="btn btn-secondary btn-xs"><i class="fas fa-file-alt">&nbsp;Draft</i></span>';
        }

        if (!is_null($pelayanan->t_id_status_verifikasi) && $pelayanan->t_id_status_verifikasi == 3) {
            $status = '<span class="btn btn-danger btn-xs"><i class="fas fa-ban">&nbsp;Ditolak</i></span>';
        }

        if (!is_null($pelayanan->t_id_status_validasi) && $pelayanan->t_id_status_validasi == 3) {
            $status = '<span class="btn btn-danger btn-xs"><i class="fas fa-ban">&nbsp;Ditolak</i></span>';
        }

        if (!is_null($pelayanan->t_id_status_validasi) && $pelayanan->t_id_status_validasi == 4) {
            $status = '<span class="btn btn-success btn-xs"><i class="fas fa-check">&nbsp;Diterima</i></span>';
        }

        if ($hasilPelayanan) {
            $status = '<span class="btn btn-primary btn-xs"><i class="fas fa-check-double">&nbsp;Selesai</i></span>';
        }

        return $status;
    }

    public function dataTrackingPelayanan($id)
    {
        $response = DB::connection('pgsql')->table('t_pelayanan')
            ->selectRaw('t_pelayanan.*,
                t_validasi.t_id_validasi,
                s_jenis_pelayanan.s_nama_jenis_pelayanan as nama_jenis,
                s_jenis_pajak.s_nama_singkat_pajak as nama_pajak,
                t_tgl_perkiraan_selesai,
                t_tgl_verifikasi,
                t_verifikasi.t_id_verifikasi,
                t_no_verifikasi,
                t_id_status_verifikasi,
                s_nama_status_verifikasi as nama_verifikasi,
                t_verifikasi.t_keterangan_verifikasi as keterangan_verifikasi,
                t_verifikasi.created_at as created_verifikasi,
                t_tgl_validasi,
                t_id_status_validasi,
                s_nama_status_validasi as nama_status_validasi,
                t_validasi.t_keterangan_validasi,
                t_validasi.created_at as created_validasi,
                t_tgl_verifikasi_lapangan,
                t_id_status_verifikasi_lapangan,
                s_nama_status_verifikasi_lapangan as nama_status_verlap,
                t_verifikasi_lapangan.t_id_verifikasi_lapangan as id_verlap,
                t_verifikasi_lapangan.t_keterangan_verifikasi_lapangan as keterangan_verifikasi_lapangan,
                t_verifikasi_lapangan.created_at as created_verlap,
                t_pelayanan_approve.t_id_pelayanan_approve,
                t_hasil_pelayanan.t_id_sk as surat_keputusanid,
                t_checklist_persyaratan,
                t_op.kd_propinsi,
                t_op.kd_dati2,
                t_op.kd_kecamatan,
                t_op.kd_kelurahan,
                t_op.kd_blok,
                t_op.no_urut,
                t_op.kd_jns_op
            ')

            ->leftJoin('s_jenis_pelayanan', 's_jenis_pelayanan.s_id_jenis_pelayanan', '=', 't_pelayanan.t_id_jenis_pelayanan')
            ->leftJoin('s_jenis_pajak', 's_jenis_pajak.s_id_jenis_pajak', '=', 't_pelayanan.t_id_jenis_pajak')
            ->leftJoin('t_pelayanan_approve', 't_pelayanan_approve.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_verifikasi', 't_verifikasi.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_validasi', 't_validasi.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_verifikasi_lapangan', 't_verifikasi_lapangan.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('s_status_verifikasi', 's_status_verifikasi.s_id_status_verifikasi', '=', 't_verifikasi.t_id_status_verifikasi')
            ->leftJoin('s_status_verifikasi_lapangan', 's_status_verifikasi_lapangan.s_id_status_verifikasi_lapangan', '=', 't_verifikasi_lapangan.t_id_status_verifikasi_lapangan')
            ->leftJoin('s_status_validasi', 's_status_validasi.s_id_status_validasi', '=', 't_validasi.t_id_status_validasi')
            ->leftJoin('t_hasil_pelayanan', 't_hasil_pelayanan.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_op', 't_op.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->where('t_pelayanan.t_id_pelayanan', $id)->first();

        return $response;
    }

    public function getSyaratByIdPelayanan($id)
    {
        $response = PersyaratanPelayanan::where('t_id_pelayanan', $id)
            ->orderBy('t_id_persyaratan')->get();

        return $response->toArray();
    }

    public function getAllIdSyaratUpload($id)
    {
        // $response = PersyaratanPelayanan::where('t_id_pelayanan', $id)->pluck('t_id_persyaratan');
        $response = PersyaratanPelayanan::join('s_persyaratan', 't_persyaratan.s_id_persyaratan', '=', 's_persyaratan.s_id_persyaratan')->where('s_persyaratan.s_is_optional', false)->where('t_id_pelayanan', $id)->pluck('t_id_persyaratan');
        return $response;
    }

    public function getAllSyaratDitolak($id)
    {
        $response = PersyaratanPelayanan::whereIn('t_id_persyaratan', $id)->pluck('t_nama_persyaratan');
        return $response->toArray();
    }

    public function getAllHasilPelayananById($id)
    {
        $data = HasilPelayanan::where('t_id_pelayanan', $id)->get();
        return $data;
    }

    public function checkPelayananOpBaruSebelumnya($nikPemohon, $idJenisPelayanan, $idJenisPajak, $nikWp)
    {


        return Pelayanan::with('wajibPajak')
            ->whereRelation('wajibPajak', 't_nik_wp', '=', $nikWp)
            ->where([
                ['t_nik_pemohon', '=', $nikPemohon],
                ['t_id_jenis_pelayanan', '=', $idJenisPelayanan],
                ['t_id_jenis_pajak', '=', $idJenisPajak],
            ])->first();
    }

    public function cekPelayananSebelumnya($nikPemohon, $idJenisPelayanan, $idJenisPajak, $nop)
    {
        return Pelayanan::where([
            ['t_nik_pemohon', '=', $nikPemohon],
            ['t_id_jenis_pelayanan', '=', $idJenisPelayanan],
            ['t_id_jenis_pajak', '=', $idJenisPajak],
            ['t_nop', '=', str_replace('.', '', $nop)],
        ])->first();
    }

    public function cekStatusTunggakanSppt($nop)
    {
        return Sppt::select(
            'SPPT.THN_PAJAK_SPPT',
            'SPPT.PBB_YG_HARUS_DIBAYAR_SPPT',
            DB::raw('TO_CHAR(SPPT.TGL_JATUH_TEMPO_SPPT,\'DD-MM-YYYY\') AS JATUH_TEMPO')
        )
            ->whereNop($nop)
            ->where('STATUS_PEMBAYARAN_SPPT', '=', '0')
            ->Where('THN_PAJAK_SPPT', '>=', 2016) // esppt kulonprogo dr 2016
            ->orderBy('SPPT.THN_PAJAK_SPPT', 'ASC')->get();
    }

    public function cekWajibLunasPbb($id)
    {
        return JenisPelayanan::where('s_id_jenis_pelayanan', '=', $id)->pluck('s_lunas_pbb')->first();
    }

    public function deletePelayanan($id)
    {
        OpLama::where('t_id_pelayanan', (int) $id)->delete();
        Op::where('t_id_pelayanan', (int) $id)->delete();
        Wp::where('t_id_pelayanan', (int) $id)->delete();
        WpLama::where('t_id_pelayanan', (int) $id)->delete();
        Pelayanan::where('t_id_pelayanan', (int) $id)->delete();
    }
}
