<?php

namespace App\Http\Traits\Pelayanan;

use App\Models\Pelayanan\Pelayanan;
use App\Models\Pelayanan\Wp;
use App\Models\Pelayanan\WpLama;

trait WpTrait
{
    private function getNamaWp(Pelayanan $pelayanan)
    {
        // dd($pelayanan);
        switch ($pelayanan->t_id_jenis_pelayanan) {
            case 2:
            case 22:
                $wajibPajak = $pelayanan->WpLamas[0]->t_nama_wp ?? WpLama::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->select('t_nama_wp')->first();
                break;
            case 1:
            case 3:
            case 4:
            case 6:
            case 7:
            case 8:
            case 9:
                $wajibPajak = $pelayanan->wajibPajaks[0]->t_nama_wp ?? Wp::where('t_id_pelayanan', $pelayanan->t_id_pelayanan)->select('t_nama_wp')->first();
                break;

            default:
                $wajibPajak = '-';
                break;
        }

        return $wajibPajak;
    }
}
