<?php

namespace App\Http\Traits\Pelayanan;

use App\Models\Pelayanan\Pelayanan;
use App\Models\Pelayanan\PelayananApprove;
use App\Models\User;
use App\Notifications\PendaftaranNotify;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Throwable;

trait PelayananApproveTrait
{
    public function savePelayananApprove($data)
    {
        try {
            $approve = [
                't_id_pelayanan' => $data->t_id_pelayanan,
                'created_by' => auth()->user()->id,
                't_tgl_perkiraan_selesai' => $data->tgl_perkiraan_selesai
            ];

            return PelayananApprove::create($approve);
        } catch (Throwable $th) {
            report($th);
            return false;
        }
    }
}
