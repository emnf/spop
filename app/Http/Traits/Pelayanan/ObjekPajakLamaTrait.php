<?php
namespace App\Http\Traits\Pelayanan;

use App\Models\Pelayanan\OpLama;
use Illuminate\Support\Facades\Auth;

trait ObjekPajakLamaTrait
{
    public function storeOpLama($data, $pelayanan)
    {
        try {
            $dataOpLama = [
                "t_id_pelayanan" => is_null($data->t_id_pelayanan) ? $pelayanan->t_id_pelayanan : $data->t_id_pelayanan,
                "kd_propinsi" => (!is_null($data->kd_propinsi)) ? $data->kd_propinsi : '',
                "kd_dati2" => (!is_null($data->kd_dati2)) ? $data->kd_dati2 : '',
                "kd_kecamatan" => $data->kd_kecamatan,
                "kd_kelurahan" => $data->kd_kelurahan,
                "kd_blok" => (!is_null($data->kd_blok_lama)) ? $data->kd_blok_lama : '',
                "kd_jns_op" => (!is_null($data->kd_jns_op_lama)) ? $data->kd_jns_op_lama : '',
                "no_urut" => (!is_null($data->no_urut_lama)) ? $data->no_urut_lama : '' ,
                "t_jalan_op" => $data->t_jalan_op_lama,
                "t_rt_op" => $data->t_rt_op_lama ?? '0',
                "t_rw_op" => $data->t_rw_op_lama ?? '0',
                "t_kelurahan_op" => $data->t_kelurahan_op_lama,
                "t_kecamatan_op" => $data->t_kecamatan_op_lama,
                "t_luas_tanah" => $data->t_luas_tanah_lama,
                "t_luas_bangunan" => $data->t_luas_bangunan_lama,
                "created_by" => Auth::user()->id,
                "t_blok_kav" => $data->t_blok_kav ?? $data->t_blokkav ?? null
            ];

            if (is_null($data->t_id_op_lama)) {
                return OpLama::create($dataOpLama);
            } else {
                return OpLama::where('t_id_op_lama', $data->t_id_op_lama)->update($dataOpLama);
            }
        } catch (\Throwable $th) {
            report($th);
        }
    }
}
