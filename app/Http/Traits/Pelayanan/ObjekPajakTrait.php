<?php

namespace App\Http\Traits\Pelayanan;

use App\Models\Pbb\Kecamatan;
use App\Models\Pbb\Kelurahan;
use App\Models\Pbb\RefDati;
use App\Models\Pelayanan\Op;
use App\Models\Pelayanan\Pelayanan;
use Illuminate\Support\Facades\Auth;

trait ObjekPajakTrait
{
    public function storeOp($data, $pelayanan)
    {
        $dati = RefDati::first();
        $kecamatan = Kecamatan::byKecamatanId($dati->kd_propinsi . $dati->kd_dati2 . $data->kd_kecamatan)->first();
        $kelurahan = Kelurahan::byKelurahanId($dati->kd_propinsi . $dati->kd_dati2 . $data->kd_kecamatan . $data->kd_kelurahan)->first();

        $dataOp = [
            "t_id_pelayanan" => is_null($data->t_id_pelayanan) ? $pelayanan->t_id_pelayanan : $data->t_id_pelayanan,
            "kd_propinsi" => (!is_null($data->kd_propinsi)) ? $data->kd_propinsi : '',
            "kd_dati2" => (!is_null($data->kd_dati2)) ? $data->kd_dati2 : '',
            "kd_kecamatan" => $data->kd_kecamatan,
            "kd_kelurahan" => $data->kd_kelurahan,
            "kd_blok" => (!is_null($data->kd_blok)) ? $data->kd_blok : '',
            "kd_jns_op" => (!is_null($data->kd_jns_op)) ? $data->kd_jns_op : '',
            "no_urut" => (!is_null($data->no_urut)) ? $data->no_urut : '',
            "t_jalan_op" => $data->t_jalan_op,
            "t_rt_op" => $data->t_rt_op,
            "t_rw_op" => $data->t_rw_op,
            "t_kelurahan_op" => $kelurahan->nm_kelurahan,
            "t_kecamatan_op" => $kecamatan->nm_kecamatan,
            "t_jenis_tanah" => $data->t_jenis_tanah,
            "t_kode_lookup_item" => $data->t_kode_lookup_item,
            "t_luas_tanah" => $data->t_luas_tanah,
            "t_luas_bangunan" => $data->t_luas_bangunan,
            "t_latitude" => $data->t_latitude ?? null,
            "t_longitude" => $data->t_longitude ?? null,
            "created_by" => Auth::user()->id,
            "t_blok_kav" => $data->t_blokkav ?? $data->t_blok_kav ?? null
        ];

        if (is_null($data->t_id_op)) {
            return Op::create($dataOp);
        } else {
            return Op::where('t_id_op', $data->t_id_op)->update($dataOp);
        }
    }
}
