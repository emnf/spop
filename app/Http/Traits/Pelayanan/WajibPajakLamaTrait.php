<?php

namespace App\Http\Traits\Pelayanan;

use App\Models\Pelayanan\WpLama;
use Illuminate\Support\Facades\Auth;

trait WajibPajakLamaTrait
{
    public function storeWpLama($data, $pelayanan)
    {
        try {
            $dataWp = [
                "t_id_pelayanan" => is_null($data->t_id_pelayanan) ? $pelayanan->t_id_pelayanan : $data->t_id_pelayanan,
                // "t_nop" => (!is_null($data->t_nop)) ? str_replace(".", "", $data->t_nop) : '',
                "t_nama_wp" => $data->t_nama_wp_lama,
                "t_nik_wp" => $data->t_nik_wp_lama,
                "t_jalan_wp" => $data->t_jalan_wp_lama,
                "t_rt_wp" => $data->t_rt_wp_lama,
                "t_rw_wp" => $data->t_rw_wp_lama,
                "t_kelurahan_wp" => $data->t_kelurahan_wp_lama,
                "t_kecamatan_wp" => $data->t_kecamatan_wp,
                "t_kabupaten_wp" => $data->t_kabupaten_wp_lama,
                "t_no_hp_wp" => $data->t_no_hp_wp_lama,
                "created_by" => Auth::user()->id,
                "t_blok_kav_wp" => $data->t_blok_kav_wp ?? null
            ];

            if (is_null($data->t_id_wp_lama)) {
                return WpLama::create($dataWp);
            } else {
                return WpLama::where('t_id_wp_lama', $data->t_id_wp_lama)->update($dataWp);
            }
        } catch (\Throwable $th) {
            report($th);
        }
    }
}
