<?php

namespace App\Http\Traits\Pelayanan;

use App\Models\Pelayanan\Wp;
use Illuminate\Support\Facades\Auth;

trait WajibPajakTrait
{
    public function storeWp($data, $pelayanan)
    {
        $dataWp = [
            "t_id_pelayanan" => is_null($data->t_id_pelayanan) ? $pelayanan->t_id_pelayanan : $data->t_id_pelayanan,
            "t_nop" => (!is_null($data->t_nop)) ? str_replace(".", "", $data->t_nop) : '',
            "t_nama_wp" => $data->t_nama_wp,
            "t_nik_wp" => $data->t_nik_wp,
            "t_jalan_wp" => $data->t_jalan_wp,
            "t_rt_wp" => $data->t_rt_wp,
            "t_rw_wp" => $data->t_rw_wp,
            "t_kelurahan_wp" => $data->t_kelurahan_wp,
            "t_kecamatan_wp" => $data->t_kecamatan_wp,
            "t_kabupaten_wp" => $data->t_kabupaten_wp,
            "t_no_hp_wp" => $data->t_no_hp_wp,
            "t_npwpd" => $data->t_npwpd,
            "t_email" => $data->t_email,
            "created_by" => Auth::user()->id,
            "t_blok_kav_wp" => $data->t_blok_kav_wp ?? null
        ];

        if (is_null($data->t_id_wp)) {
            return Wp::create($dataWp);
        } else {
            return Wp::where('t_id_wp', $data->t_id_wp)->update($dataWp);
        }
    }
}
