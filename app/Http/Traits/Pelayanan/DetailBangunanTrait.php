<?php

namespace App\Http\Traits\Pelayanan;

use App\Models\Pelayanan\DetailBangunan;
use App\Models\Pelayanan\Op;

trait DetailBangunanTrait
{
    public function createOrUpdate($data)
    {
        $dataBangunan = [
            't_id_pelayanan' => $data->idPelayanan,
            't_jenis_penggunaan_bangunan' => $data->jenisPenggunaanBangunan,
            't_tahun_bangunan' => $data->tahunBangunan,
            't_tahun_renovasi' => $data->tahunRenovasi,
            't_kondisi_bangunan' => $data->kondisiBangunan,
            't_konstruksi' => $data->konstruksi,
            't_atap' => $data->atap,
            't_dinding' => $data->dinding,
            't_lantai' => $data->lantai,
            't_langit_langit' => $data->langitLangit,
            't_ac_split' => $data->acSplit,
            't_ac_window' => $data->acWindow,
            't_panjang_pagar' => $data->panjangPagar,
            't_bahan_pagar' => $data->bahanPagar,
            't_jumlah_lantai' => $data->jumlahLantai,
            't_luas' => $data->luasBangunan,
            't_listrik' => $data->dayaListrik,
            't_no_urut_bangunan' => (new DetailBangunan())->getMaxNourut($data->idPelayanan),
            'created_by' => auth()->user()->id,
            't_id_op' => $data->idOp ?? null
        ];

        $dataBangunan = DetailBangunan::updateOrCreate([
            't_id_detail_bangunan' => $data->idDetailBangunan
        ], $dataBangunan);

        if (!is_null($data->idOp)) {
            $this->updateOpLuasBangunan($data->idOp);
        }

        return $dataBangunan;
    }

    public function updateOpLuasBangunan($objekPajakId)
    {
        $luasBangunan = DetailBangunan::where('t_id_op', $objekPajakId)->select('t_luas')->get();
        $totLuasBangunan = 0;
        foreach ($luasBangunan as $value) {
            $totLuasBangunan += $value->t_luas;
        }

        Op::where('t_id_op', $objekPajakId)->update(['t_luas_bangunan' => $totLuasBangunan]);
    }
}
