<?php

namespace App\Http\Traits\Verifikasi;

use App\Models\Pelayanan\Pelayanan;

trait VerifikasiTrait
{
    public function dataBelumProses($filter, $pagination, $sorting)
    {
        $data = (new Pelayanan)->select(
            't_pelayanan.*',
            's_jenis_pajak.s_nama_jenis_pajak',
            's_jenis_pelayanan.s_nama_jenis_pelayanan'
        )
            ->leftJoin('s_jenis_pelayanan', 's_jenis_pelayanan.s_id_jenis_pelayanan', '=', 't_pelayanan.t_id_jenis_pelayanan')
            ->leftJoin('s_jenis_pajak', 's_jenis_pajak.s_id_jenis_pajak', '=', 't_pelayanan.t_id_jenis_pajak')
            ->leftJoin('t_pelayanan_approve', 't_pelayanan_approve.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_verifikasi', 't_verifikasi.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')

            ->when(empty($sorting), function ($query) {
                $query->orderBy('t_tgl_pelayanan', 'desc');
            }, function ($query) use ($sorting) {
                $query->orderBy($sorting['key'], $sorting['order']);
            })

            ->when(!empty($filter['idJenisPajak']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_id_jenis_pajak', $filter['idJenisPajak']);
            })

            ->when(!empty($filter['idJenisPelayanan']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_id_jenis_pelayanan', $filter['idJenisPelayanan']);
            })

            ->when(!empty($filter['tglPelayanan']), function ($query) use ($filter) {
                $date = explode(' - ', $filter['tglPelayanan']);
                $startDate = date('Y-m-d', strtotime($date[0]));
                $endDate = date('Y-m-d', strtotime($date[1]));
                $query->whereBetween('t_pelayanan.t_tgl_pelayanan', [$startDate, $endDate]);
            })

            ->when(!empty($filter['noPelayanan']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_no_pelayanan',  'like', "%" . $filter['noPelayanan'] . "%");
            })

            ->when(!empty($filter['namaPemohon']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_nama_pemohon',  'ilike', "%" . $filter['namaPemohon'] . "%");
            })

            ->doesntHave('verifikasi')
            ->whereNotNull('t_id_pelayanan_approve');

        return $data->paginate($pagination['pageSize'], ['*'], 'page', $pagination['pageNumber'] + 1);
    }

    public function datagridVerifikasi($filter, $pagination, $sorting, $status = null)
    {
        $data = (new Pelayanan)->select(
            't_pelayanan.*',
            's_jenis_pajak.s_nama_jenis_pajak',
            's_jenis_pelayanan.s_nama_jenis_pelayanan',
            't_verifikasi.t_tgl_verifikasi',
            't_verifikasi.t_no_verifikasi',
            't_verifikasi.t_keterangan_verifikasi',
            't_verifikasi.t_id_status_verifikasi',
            's_status_verifikasi.s_nama_status_verifikasi',
        )
            ->leftJoin('s_jenis_pelayanan', 's_jenis_pelayanan.s_id_jenis_pelayanan', '=', 't_pelayanan.t_id_jenis_pelayanan')
            ->leftJoin('s_jenis_pajak', 's_jenis_pajak.s_id_jenis_pajak', '=', 't_pelayanan.t_id_jenis_pajak')
            ->leftJoin('t_pelayanan_approve', 't_pelayanan_approve.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_verifikasi', 't_verifikasi.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('s_status_verifikasi', 's_status_verifikasi.s_id_status_verifikasi', '=', 't_verifikasi.t_id_status_verifikasi')
            ->when(empty($sorting), function ($query) {
                $query->orderBy('t_tgl_pelayanan', 'desc');
            }, function ($query) use ($sorting) {
                $query->orderBy($sorting['key'], $sorting['order']);
            })

            ->when(!is_null($status), function($query) use ($status) {
                $query->where('t_id_status_verifikasi', $status);
            })

            ->when(!empty($filter['idJenisPajak']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_id_jenis_pajak', $filter['idJenisPajak']);
            })

            ->when(!empty($filter['idJenisPelayanan']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_id_jenis_pelayanan', $filter['idJenisPelayanan']);
            })

            ->when(!empty($filter['tglPelayanan']), function ($query) use ($filter) {
                $date = explode(' - ', $filter['tglPelayanan']);
                $startDate = date('Y-m-d', strtotime($date[0]));
                $endDate = date('Y-m-d', strtotime($date[1]));
                $query->whereBetween('t_pelayanan.t_tgl_pelayanan', [$startDate, $endDate]);
            })

            ->when(!empty($filter['tglVerifikasi']), function ($query) use ($filter) {
                $date = explode(' - ', $filter['tglVerifikasi']);
                $startDate = date('Y-m-d', strtotime($date[0]));
                $endDate = date('Y-m-d', strtotime($date[1]));
                $query->whereBetween('t_verifikasi.t_tgl_verifikasi', [$startDate, $endDate]);
            })

            ->when(!empty($filter['noPelayanan']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_no_pelayanan',  'like', "%" . $filter['noPelayanan'] . "%");
            })

            ->when(!empty($filter['namaPemohon']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_nama_pemohon',  'ilike', "%{$filter['namaPemohon']}%");
            })

            ->when(!empty($filter['namaWp']), function ($query) use ($filter) {
                $query->whereRelation('wajibPajak', 't_nama_wp',  'ilike', "%{$filter['namaWp']}%");
                $query->orWhereRelation('WpLama', 't_nama_wp',  'ilike', "%{$filter['namaWp']}%");
            })

            ->when(!empty($filter['idStatusVerifikasi']), function ($query) use ($filter) {
                $query->where('t_verifikasi.t_id_status_verifikasi', $filter['idStatusVerifikasi']);
            })

            ->whereNotNull('t_id_pelayanan_approve');

        return $data->paginate($pagination['pageSize'], ['*'], 'page', $pagination['pageNumber'] + 1);
    }

}
