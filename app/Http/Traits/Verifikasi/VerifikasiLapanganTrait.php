<?php

namespace App\Http\Traits\Verifikasi;

use App\Models\Hasil\HasilVerlap;
use App\Models\Hasil\HasilVerlapDetailBangunan;
use App\Models\Hasil\HasilVerlapOp;
use App\Models\Hasil\HasilVerlapOpBaru;
use App\Models\Hasil\HasilVerlapWp;
use App\Models\Pelayanan\Pelayanan;
use App\Models\Verifikasi\VerifikasiLapanagan;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Pelayanan\DetailBangunan;

trait VerifikasiLapanganTrait
{
    public function dataBelumProses($filter, $pagination, $sorting)
    {
        $data = (new Pelayanan)->select(
            't_pelayanan.*',
            's_jenis_pajak.s_nama_jenis_pajak',
            's_jenis_pajak.s_nama_singkat_pajak',
            's_jenis_pelayanan.s_nama_jenis_pelayanan',
            't_verifikasi.t_tgl_verifikasi',
            't_verifikasi.t_keterangan_verifikasi',
            't_validasi.t_tgl_akhir_verlap',
            't_validasi.t_keterangan_validasi'
        )
            ->leftJoin('s_jenis_pelayanan', 's_jenis_pelayanan.s_id_jenis_pelayanan', '=', 't_pelayanan.t_id_jenis_pelayanan')
            ->leftJoin('s_jenis_pajak', 's_jenis_pajak.s_id_jenis_pajak', '=', 't_pelayanan.t_id_jenis_pajak')
            ->leftJoin('t_pelayanan_approve', 't_pelayanan_approve.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_verifikasi', 't_verifikasi.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_validasi', 't_validasi.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->where('t_validasi.t_id_status_validasi', 5)
            ->doesntHave('verifikasiLapangan')
            ->when(empty($sorting), function ($query) {
                $query->orderBy('t_tgl_pelayanan', 'desc');
            }, function ($query) use ($sorting) {
                $query->orderBy($sorting['key'], $sorting['order']);
            })

            ->when(!empty($filter['idJenisPajak']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_id_jenis_pajak', $filter['idJenisPajak']);
            })

            ->when(!empty($filter['idJenisPelayanan']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_id_jenis_pelayanan', $filter['idJenisPelayanan']);
            })

            ->when(!empty($filter['tglPelayanan']), function ($query) use ($filter) {
                $date = explode(' - ', $filter['tglPelayanan']);
                $startDate = date('Y-m-d', strtotime($date[0]));
                $endDate = date('Y-m-d', strtotime($date[1]));
                $query->whereBetween('t_pelayanan.t_tgl_pelayanan', [$startDate, $endDate]);
            })

            ->when(!empty($filter['tglVerifikasi']), function ($query) use ($filter) {
                $date = explode(' - ', $filter['tglVerifikasi']);
                $startDate = date('Y-m-d', strtotime($date[0]));
                $endDate = date('Y-m-d', strtotime($date[1]));
                $query->whereBetween('t_verifikasi.t_tgl_verifikasi', [$startDate, $endDate]);
            })

            ->when(!empty($filter['noPelayanan']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_no_pelayanan',  'like', "%" . $filter['noPelayanan'] . "%");
            })

            ->when(!empty($filter['namaPemohon']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_nama_pemohon',  'ilike', "%" . $filter['namaPemohon'] . "%");
            });


        return $data->paginate($pagination['pageSize'], ['*'], 'page', $pagination['pageNumber'] + 1);
    }

    public function datagridVerlap($filter, $pagination, $sorting, $status = null)
    {
        $data = (new Pelayanan)->select(
            't_pelayanan.*',
            's_jenis_pajak.s_nama_jenis_pajak',
            's_jenis_pajak.s_nama_singkat_pajak',
            's_jenis_pelayanan.s_nama_jenis_pelayanan',
            's_status_verifikasi_lapangan.s_nama_status_verifikasi_lapangan',
            't_verifikasi.t_tgl_verifikasi',
            't_verifikasi.t_no_verifikasi',
            't_verifikasi.t_keterangan_verifikasi',
            't_verifikasi.t_id_status_verifikasi',
            't_validasi.t_keterangan_validasi',
            't_verifikasi_lapangan.*'
        )
            ->leftJoin('t_pelayanan_approve', 't_pelayanan_approve.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_verifikasi', 't_verifikasi.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_validasi', 't_validasi.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_verifikasi_lapangan', 't_verifikasi_lapangan.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('s_jenis_pelayanan', 's_jenis_pelayanan.s_id_jenis_pelayanan', '=', 't_pelayanan.t_id_jenis_pelayanan')
            ->leftJoin('s_jenis_pajak', 's_jenis_pajak.s_id_jenis_pajak', '=', 't_pelayanan.t_id_jenis_pajak')
            ->leftJoin('s_status_verifikasi_lapangan', 's_status_verifikasi_lapangan.s_id_status_verifikasi_lapangan', '=', 't_verifikasi_lapangan.t_id_status_verifikasi_lapangan')
            ->when(empty($sorting), function ($query) {
                $query->orderBy('t_tgl_pelayanan', 'desc');
            }, function ($query) use ($sorting) {
                $query->orderBy($sorting['key'], $sorting['order']);
            })

            ->when(!empty($filter['idJenisPajak']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_id_jenis_pajak', $filter['idJenisPajak']);
            })

            ->when(!empty($filter['idJenisPelayanan']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_id_jenis_pelayanan', $filter['idJenisPelayanan']);
            })

            ->when(!empty($filter['tglPelayanan']), function ($query) use ($filter) {
                $date = explode(' - ', $filter['tglPelayanan']);
                $startDate = date('Y-m-d', strtotime($date[0]));
                $endDate = date('Y-m-d', strtotime($date[1]));
                $query->whereBetween('t_pelayanan.t_tgl_pelayanan', [$startDate, $endDate]);
            })

            ->when(!empty($filter['tglVerifikasiLapangan']), function ($query) use ($filter) {
                $date = explode(' - ', $filter['tglVerifikasiLapangan']);
                $startDate = date('Y-m-d', strtotime($date[0]));
                $endDate = date('Y-m-d', strtotime($date[1]));
                $query->whereBetween('t_verifikasi_lapangan.t_tgl_verifikasi_lapangan', [$startDate, $endDate]);
            })

            ->when(!empty($filter['noPelayanan']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_no_pelayanan',  'like', "%" . $filter['noPelayanan'] . "%");
            })

            ->when(!empty($filter['namaPemohon']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_nama_pemohon',  'ilike', "%" . $filter['namaPemohon'] . "%");
            })

            ->when(!empty($filter['idStatusVerifikasi']), function ($query) use ($filter) {
                $query->where('t_verifikasi.t_id_status_verifikasi', $filter['idStatusVerifikasi']);
            })

            ->when(!is_null($status), function ($query) use ($status) {
                $query->where('t_id_status_verifikasi_lapangan', $status);
            })
            ->whereNotNull('t_id_pelayanan_approve');

        return $data->paginate($pagination['pageSize'], ['*'], 'page', $pagination['pageNumber'] + 1);
    }

    public function datagridVerlapSemua($filter, $pagination, $sorting)
    {
        $data = (new Pelayanan)->select(
            't_pelayanan.*',
            's_jenis_pajak.s_nama_jenis_pajak',
            's_jenis_pajak.s_nama_singkat_pajak',
            's_jenis_pelayanan.s_nama_jenis_pelayanan',
            't_verifikasi.t_tgl_verifikasi',
            't_verifikasi.t_no_verifikasi',
            't_verifikasi.t_keterangan_verifikasi',
            't_verifikasi.t_id_status_verifikasi',
            't_validasi.t_keterangan_validasi',
            's_status_verifikasi_lapangan.s_nama_status_verifikasi_lapangan',
            't_verifikasi_lapangan.*'
        )
            ->leftJoin('t_pelayanan_approve', 't_pelayanan_approve.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_verifikasi', 't_verifikasi.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_validasi', 't_validasi.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_verifikasi_lapangan', 't_verifikasi_lapangan.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('s_jenis_pelayanan', 's_jenis_pelayanan.s_id_jenis_pelayanan', '=', 't_pelayanan.t_id_jenis_pelayanan')
            ->leftJoin('s_jenis_pajak', 's_jenis_pajak.s_id_jenis_pajak', '=', 't_pelayanan.t_id_jenis_pajak')
            ->leftJoin('s_status_verifikasi_lapangan', 's_status_verifikasi_lapangan.s_id_status_verifikasi_lapangan', '=', 't_verifikasi_lapangan.t_id_status_verifikasi_lapangan')
            ->when(empty($sorting), function ($query) {
                $query->orderBy('t_tgl_pelayanan', 'desc');
            }, function ($query) use ($sorting) {
                $query->orderBy($sorting['key'], $sorting['order']);
            })

            ->when(!empty($filter['idJenisPajak']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_id_jenis_pajak', $filter['idJenisPajak']);
            })

            ->when(!empty($filter['idJenisPelayanan']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_id_jenis_pelayanan', $filter['idJenisPelayanan']);
            })

            ->when(!empty($filter['tglPelayanan']), function ($query) use ($filter) {
                $date = explode(' - ', $filter['tglPelayanan']);
                $startDate = date('Y-m-d', strtotime($date[0]));
                $endDate = date('Y-m-d', strtotime($date[1]));
                $query->whereBetween('t_pelayanan.t_tgl_pelayanan', [$startDate, $endDate]);
            })

            ->when(!empty($filter['noPelayanan']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_no_pelayanan',  'like', "%" . $filter['noPelayanan'] . "%");
            })

            ->when(!empty($filter['namaPemohon']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_nama_pemohon',  'ilike', "%" . $filter['namaPemohon'] . "%");
            })

            ->when(!empty($filter['idStatusVerifikasi']), function ($query) use ($filter) {
                $query->where('t_verifikasi.t_id_status_verifikasi', $filter['idStatusVerifikasi']);
            })
            ->whereRaw("t_validasi.t_id_status_validasi IN (5, 6) OR exists (select * from t_verifikasi_lapangan where t_pelayanan.t_id_pelayanan = t_verifikasi_lapangan.t_id_pelayanan)")
            // ->whereIn('t_id_status_validasi', [5, 6])
            ->whereNotNull('t_id_pelayanan_approve');

        return $data->paginate($pagination['pageSize'], ['*'], 'page', $pagination['pageNumber'] + 1);
    }

    public function saveHasilVerlapOpBaru($param)
    {
        $data = [
            't_id_pelayanan' => $param->idPelayanan,
            't_no_shm' => $param->noSHM,
            't_tgl_shm' => date('Y-m-d', strtotime($param->tglSHM)),
            't_luas_tanah_shm' => $param->luasTanahSHM,
            't_atas_nama_shm' => $param->atasNamaSHM,
            't_nop_terdekat' => $param->nopTerdekat,
            't_kode_blok' => $param->kodeBlok,
            't_kode_znt' => trim($param->kodeZNT),
            't_kelas_tanah' => $param->kelasTanah,
        ];

        return HasilVerlapOpBaru::updateOrCreate($data);
    }

    public function saveHasilVerlap($param)
    {
        $idHasilVerlap = HasilVerlap::where('t_id_pelayanan', $param->idPelayanan)->first();

        $data = [
            't_id_pelayanan' => $param->idPelayanan,
            't_no' => $param->noBAHP,
            't_tgl_penelitian' => date('Y-m-d', strtotime($param->tglVerifikasiLapangan)),
            't_tempat_penelitian' => $param->tempatPenelitian ?? '-'
        ];

        return HasilVerlap::updateOrCreate(
            ['t_id' => $idHasilVerlap],
            $data
        );
    }

    public function saveVerlap($param)
    {
        $data = [
            't_id_pelayanan' => $param->idPelayanan,
            't_tgl_verifikasi_lapangan' => date('Y-m-d', strtotime($param->tglVerifikasiLapangan)),
            't_no_verifikasi_lapangan' => VerifikasiLapanagan::maxNoUrut() + 1,
            't_id_status_verifikasi_lapangan' => $param->idStatusVerifikasiLapangan,
            't_keterangan_verifikasi_lapangan' => $param->keteranganVerifikasiLapangan,
            'created_by' => auth()->user()->id,
        ];

        return VerifikasiLapanagan::updateOrCreate(
            ['t_id_verifikasi_lapangan' => $param->idVerifikasiLapangan],
            $data
        );
    }

    public function getDataVerlap($request, $status)
    {
        $data = Pelayanan::when($request->tglPelayanan, function ($q) use ($request) {
            $tglPelayanan = explode(' - ', $request->tglPelayanan);

            return $q->whereBetween('t_pelayanan.t_tgl_pelayanan', [
                date('Y-m-d', strtotime($tglPelayanan[0])),
                date('Y-m-d', strtotime($tglPelayanan[1]))
            ]);
        })
            ->when($request->idJenisPelayanan, function ($q) use ($request) {
                $q->where('t_pelayanan.t_id_jenis_pelayanan', $request->idJenisPelayanan);
            })
            ->when($request->namaPemohon, function ($q) use ($request) {
                $q->where('t_pelayanan.t_nama_pemohon', 'ilike', "%{$request->namaPemohon}%");
            })
            ->when($request->noPelayanan, function ($q) use ($request) {
                $q->where('t_pelayanan.t_no_pelayanan', 'like', "%{$request->noPelayanan}%");
            })
            ->when($status == 1, function ($q) {
                $q->whereRelation('validasi', 't_id_status_validasi', '=', 5);
                $q->doesnthave('verifikasiLapangan');
            })
            ->when($status == 2, function ($q) {
                $q->whereRelation('verifikasiLapangan', 't_id_status_verifikasi_lapangan', '=', 2);
            })
            ->when($status == 3, function ($q) {
                $q->whereRelation('verifikasiLapangan', 't_id_status_verifikasi_lapangan', '=', 3);
            })
            ->when($status == 0, function ($q) {
                $q->whereRaw("exists (select * from t_validasi where t_pelayanan.t_id_pelayanan = t_validasi.t_id_pelayanan AND t_validasi.t_id_status_validasi IN (5,6)) OR exists (select * from t_verifikasi_lapangan where t_pelayanan.t_id_pelayanan = t_verifikasi_lapangan.t_id_pelayanan)");
            })
            ->orderBy('t_tgl_pelayanan')->get();

        return $data;
    }

    public function saveDetailVerlap($request, $pelayanan, $kecamatan, $kelurahan)
    {
        if (!is_null($request->idWP)) {
            $dataWp = [
                "t_id_pelayanan" => $request->idPelayanan,
                "t_nama_wp" => $request->namaWP,
                "t_nik_wp" => $request->nikWP,
                "t_jalan_wp" => $request->jalanWP,
                "t_rt_wp" => $request->rtWP,
                "t_rw_wp" => $request->rwWP,
                "t_kelurahan_wp" => $request->kelurahanWP,
                "t_kecamatan_wp" => $request->kecamatanWP,
                "t_kabupaten_wp" => $request->kabupatenWP,
                "t_no_hp_wp" => $request->noHpWP,
                "t_npwpd" => $request->npwpd,
                "t_email" => $request->email,
                "created_by" => auth()->user()->id
            ];

            HasilVerlapWp::create($dataWp);
        }

        if (!is_null($request->idOP)) {
            $t_luas_bangunan = null;

            if (is_array($request->luasBangunan)) {
                $t_luas_bangunan = array_sum($request->luasBangunan);
            } elseif (isset($request->luasBangunan)) {
                $t_luas_bangunan = $request->luasBangunan;
            }
            $dataOp = [
                "t_id_pelayanan" => $request->idPelayanan,
                'kd_propinsi' => $pelayanan->objekPajak->kd_propinsi ?? null,
                'kd_dati2' => $pelayanan->objekPajak->kd_dati2 ?? null,
                "kd_kecamatan" => $request->kodeKecamatan ?? $pelayanan->objekPajak->kd_kecamatan,
                "kd_kelurahan" => $request->kodeKelurahan ?? $pelayanan->objekPajak->kd_kelurahan,
                'kd_blok' => $pelayanan->objekPajak->kd_blok ?? null,
                'no_urut' => $pelayanan->objekPajak->no_urut ?? null,
                'kd_jns_op' => $pelayanan->objekPajak->kd_jns_op ?? null,
                "t_jalan_op" => $request->jalanOP,
                "t_rt_op" => $request->rtOP,
                "t_rw_op" => $request->rwOP,
                "t_kelurahan_op" => $kelurahan->nm_kelurahan ?? $pelayanan->objekPajak->t_kelurahan_op ?? null,
                "t_kecamatan_op" => $kecamatan->nm_kecamatan ?? $pelayanan->objekPajak->t_kecamatan_op ?? null,
                "t_jenis_tanah" => $request->jenisTanah,
                "t_kode_lookup_item" => $request->kodeLookUpItem,
                "t_luas_tanah" => $request->luasTanah,
                "t_luas_bangunan" => $t_luas_bangunan,
                "t_latitude" => $request->latitude,
                "t_longitude" => $request->longitude,
                "created_by" => auth()->user()->id
            ];
            // dd($dataOp);
            HasilVerlapOp::create($dataOp);
        }

        if ($request->idDetailBangunan) {
            foreach ($request->idDetailBangunan as $key => $bagunanId) {
                $detailBangunan = [
                    "t_id_pelayanan" => $request->idPelayanan,
                    't_jenis_penggunaan_bangunan' => $request->jenisPenggunaanBangunan[$key] ?? null,
                    't_tahun_bangunan' => $request->tahunBangunan[$key] ?? null,
                    't_tahun_renovasi' => $request->tahunRenovasi[$key] ?? null,
                    't_kondisi_bangunan' => $request->kondisiBangunan[$key] ?? null,
                    't_konstruksi' => $request->konstruksi[$key] ?? null,
                    't_atap' => $request->atap[$key] ?? null,
                    't_dinding' => $request->dinding[$key] ?? null,
                    't_lantai' => $request->lantai[$key] ?? null,
                    't_langit_langit' => $request->langitLangit[$key] ?? null,
                    't_ac_split' => $request->acSplit[$key] ?? null,
                    't_ac_window' => $request->acWindow[$key] ?? null,
                    't_panjang_pagar' => $request->panjangPagar[$key] ?? null,
                    't_bahan_pagar' => $request->bahanPagar[$key] ?? null,
                    't_jumlah_lantai' => $request->jumlahLantai[$key] ?? null,
                    't_luas' => $request->luasBangunan[$key],
                    't_no_urut_bangunan' => $request->nomorBangunan[$key],
                    't_listrik' => $request->dayaListrik[$key] ?? null,
                    'created_by' => auth()->user()->id
                ];

                HasilVerlapDetailBangunan::create($detailBangunan);
            }
        }
    }

    public function saveDetailVerlapMutasiPecah($request, $pelayanan, $kecamatan, $kelurahan)
    {
        $objekPajak = [];
        if (!is_null($request->idWP)) {
            foreach ($request->idWP as $key => $value) {
                $dataWp = [
                    "t_id_pelayanan" => $request->idPelayanan,
                    "t_nama_wp" => $request->namaWP[$key],
                    "t_nik_wp" => $request->nikWP[$key],
                    "t_jalan_wp" => $request->jalanWP[$key],
                    "t_rt_wp" => $request->rtWP[$key],
                    "t_rw_wp" => $request->rwWP[$key],
                    "t_kelurahan_wp" => $request->kelurahanWP[$key],
                    "t_kecamatan_wp" => $request->kecamatanWP[$key],
                    "t_kabupaten_wp" => $request->kabupatenWP[$key],
                    "t_no_hp_wp" => $request->noHpWP[$key] ?? null,
                    "t_email" => $request->email[$key] ?? null,
                    "created_by" => auth()->user()->id
                ];

                HasilVerlapWp::create($dataWp);
            }
        }

        if (!is_null($request->idOP)) {
            foreach ($request->idxObjek as $key => $value) {
                $dataOp = [
                    "t_id_pelayanan" => $request->idPelayanan,
                    'kd_propinsi' => $pelayanan->objekPajak->kd_propinsi[$key] ?? null,
                    'kd_dati2' => $pelayanan->objekPajak->kd_dati2[$key] ?? null,
                    "kd_kecamatan" => $request->kodeKecamatan ?? $pelayanan->objekPajaks[$key]->kd_kecamatan,
                    "kd_kelurahan" => $request->kodeKelurahan ?? $pelayanan->objekPajaks[$key]->kd_kelurahan,
                    'kd_blok' => $pelayanan->objekPajak->kd_blok[$key] ?? null,
                    'no_urut' => $pelayanan->objekPajak->no_urut[$key] ?? null,
                    'kd_jns_op' => $pelayanan->objekPajak->kd_jns_op[$key] ?? null,
                    "t_jalan_op" => $request->jalanOP[$key],
                    "t_rt_op" => $request->rtOP[$key],
                    "t_rw_op" => $request->rwOP[$key],
                    "t_kelurahan_op" => $kelurahan->nm_kelurahan ?? $pelayanan->objekPajaks[$key]->t_kelurahan_op ?? null,
                    "t_kecamatan_op" => $kecamatan->nm_kecamatan ?? $pelayanan->objekPajaks[$key]->t_kecamatan_op ?? null,
                    "t_jenis_tanah" => $request->jenisTanah[$key],
                    "t_luas_tanah" => $request->luasTanah[$key],
                    "t_luas_bangunan" => $request->luasBangunan[$key],
                    "t_latitude" => $request->latitude,
                    "t_longitude" => $request->longitude,
                    "created_by" => auth()->user()->id,
                    "t_id_objek_sebelumnya" => $value,
                    't_kode_znt' => $request->t_kode_znt[$key]
                ];
                // dd($dataOp);
                $verlapop = HasilVerlapOp::create($dataOp);

                $objekPajak[$value] = $verlapop->t_id_op;
            }
        }
        // $request->idDetailBangunan=[1,2];
        // dd($request->idDetailBangunan);
        if ($request->idDetailBangunan) {
            foreach ($request->idDetailBangunan as $key => $bagunanId) {
                // dd($bagunanId);
                $bangunan_lama = DetailBangunan::where('t_id_detail_bangunan', $bagunanId)->first();
                //  dd($bangunan_lama);
                $op_verlap = HasilVerlapOp::where('t_id_objek_sebelumnya', $bangunan_lama->t_id_op)->first();
                // dd($op_verlap);
                $detailBangunan = [
                    "t_id_pelayanan" => $request->idPelayanan,
                    't_jenis_penggunaan_bangunan' => $request->jenisPenggunaanBangunan[$key] ?? null,
                    't_tahun_bangunan' => $request->tahunBangunan[$key] ?? null,
                    't_tahun_renovasi' => $request->tahunRenovasi[$key] ?? null,
                    't_kondisi_bangunan' => $request->kondisiBangunan[$key] ?? null,
                    't_konstruksi' => $request->konstruksi[$key] ?? null,
                    't_atap' => $request->atap[$key] ?? null,
                    't_dinding' => $request->dinding[$key] ?? null,
                    't_lantai' => $request->lantai[$key] ?? null,
                    't_langit_langit' => $request->langitLangit[$key] ?? null,
                    't_ac_split' => $request->acSplit[$key] ?? null,
                    't_ac_window' => $request->acWindow[$key] ?? null,
                    't_panjang_pagar' => $request->panjangPagar[$key] ?? null,
                    't_bahan_pagar' => $request->bahanPagar[$key] ?? null,
                    't_jumlah_lantai' => $request->jumlahLantai[$key] ?? null,
                    't_luas' => $request->luasBangunans[$key],
                    't_no_urut_bangunan' => $request->nomorBangunan[$key],
                    't_listrik' => $request->dayaListrik[$key] ?? null,
                    'created_by' => auth()->user()->id,
                    't_id_objek' => $op_verlap->t_id_op
                    // $objekPajak[$request->idBangunanObjek[$key]]
                ];
                // dd($detailBangunan);

                HasilVerlapDetailBangunan::create($detailBangunan);
            }
        }
    }
}
