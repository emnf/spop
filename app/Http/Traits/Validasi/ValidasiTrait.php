<?php

namespace App\Http\Traits\Validasi;

use App\Models\Pelayanan\Pelayanan;

trait ValidasiTrait
{
    public function gridBelumProses($filter, $pagination, $sorting)
    {
        $data = Pelayanan::select(
            't_pelayanan.*',
            't_verifikasi.t_tgl_verifikasi',
            's_jenis_pajak.s_nama_jenis_pajak',
            's_jenis_pelayanan.s_nama_jenis_pelayanan'
        )
            ->leftJoin('s_jenis_pelayanan', 's_jenis_pelayanan.s_id_jenis_pelayanan', '=', 't_pelayanan.t_id_jenis_pelayanan')
            ->leftJoin('s_jenis_pajak', 's_jenis_pajak.s_id_jenis_pajak', '=', 't_pelayanan.t_id_jenis_pajak')
            ->join('t_verifikasi', 't_verifikasi.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->where('t_id_status_verifikasi', 5)
            ->doesntHave('validasi')
            ->when(empty($sorting), function ($query) {
                $query->orderBy('t_tgl_pelayanan', 'desc');
            }, function ($query) use ($sorting) {
                $query->orderBy($sorting['key'], $sorting['order']);
            })
            ->when(!empty($filter['idJenisPelayanan']), function ($q) use ($filter) {
                $q->where('t_pelayanan.t_id_jenis_pelayanan', $filter['idJenisPelayanan']);
            })
            ->when(!empty($filter['idJenisPajak']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_id_jenis_pajak', $filter['idJenisPajak']);
            })
            ->when(!empty($filter['tglVerifikasi']), function ($query) use ($filter) {
                $date = explode(' - ', $filter['tglVerifikasi']);
                $startDate = date('Y-m-d', strtotime($date[0]));
                $endDate = date('Y-m-d', strtotime($date[1]));
                $query->whereBetween('t_verifikasi.t_tgl_verifikasi', [$startDate, $endDate]);
            })
            ->when(!empty($filter['noPelayanan']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_no_pelayanan',  'like', "%" . $filter['noPelayanan'] . "%");
            })
            ->when(!empty($filter['nikPemohon']), function ($q) use ($filter) {
                $q->where('t_pelayanan.t_nik_pemohon', 'like', "%{$filter['nikPemohon']}%");
            })
            ->when(!empty($filter['namaPemohon']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_nama_pemohon',  'ilike', "%{$filter['namaPemohon']}%");
            })
            ->when(!empty($filter['namaWp']), function ($query) use ($filter) {
                $query->whereRelation('wajibPajak', 't_nama_wp',  'ilike', "%{$filter['namaWp']}%");
                $query->orWhereRelation('WpLama', 't_nama_wp',  'ilike', "%{$filter['namaWp']}%");
            })
            ->orderBy('t_pelayanan.t_id_pelayanan', 'desc');



        return $data->paginate(
            $pagination['pageSize'],
            ['*'],
            'page',
            $pagination['pageNumber'] + 1
        );
    }

    public function gridValidasi($filter, $pagination, $sorting, $status = null)
    {
        $data = Pelayanan::select(
            't_pelayanan.*',
            't_validasi.*',
            't_verifikasi.t_tgl_verifikasi',
            't_verifikasi.t_no_verifikasi',
            't_verifikasi.t_id_status_verifikasi',
            't_verifikasi.t_keterangan_verifikasi',
            't_verifikasi_lapangan.t_tgl_verifikasi_lapangan',
            's_jenis_pajak.s_nama_jenis_pajak',
            's_jenis_pelayanan.s_nama_jenis_pelayanan',
            's_status_validasi.s_id_status_validasi',
            's_status_validasi.s_nama_status_validasi'
        )
            ->leftJoin('t_pelayanan_approve', 't_pelayanan_approve.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_verifikasi', 't_verifikasi.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_validasi', 't_validasi.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('t_verifikasi_lapangan', 't_verifikasi_lapangan.t_id_pelayanan', '=', 't_pelayanan.t_id_pelayanan')
            ->leftJoin('s_jenis_pelayanan', 's_jenis_pelayanan.s_id_jenis_pelayanan', '=', 't_pelayanan.t_id_jenis_pelayanan')
            ->leftJoin('s_jenis_pajak', 's_jenis_pajak.s_id_jenis_pajak', '=', 't_pelayanan.t_id_jenis_pajak')
            ->join('s_status_validasi', 's_status_validasi.s_id_status_validasi', '=', 't_validasi.t_id_status_validasi')
            ->when(empty($sorting), function ($query) {
                $query->orderBy('t_tgl_validasi', 'desc');
            }, function ($query) use ($sorting) {
                $query->orderBy($sorting['key'], $sorting['order']);
            })
            ->when(!is_null($status), function ($q) use ($status) {
                $q->where('t_validasi.t_id_status_validasi', $status);
            })
            ->when(!empty($filter['idJenisPelayanan']), function ($q) use ($filter) {
                $q->where('t_pelayanan.t_id_jenis_pelayanan', $filter['idJenisPelayanan']);
            })
            ->when(!empty($filter['idJenisPajak']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_id_jenis_pajak', $filter['idJenisPajak']);
            })
            ->when(!empty($filter['tglValidasi']), function ($query) use ($filter) {
                $date = explode(' - ', $filter['tglValidasi']);
                $startDate = date('Y-m-d', strtotime($date[0]));
                $endDate = date('Y-m-d', strtotime($date[1]));
                $query->whereBetween('t_verifikasi.t_tgl_verifikasi', [$startDate, $endDate]);
            })
            ->when(!empty($filter['tglPelayanan']), function ($query) use ($filter) {
                $date = explode(' - ', $filter['tglPelayanan']);
                $startDate = date('Y-m-d', strtotime($date[0]));
                $endDate = date('Y-m-d', strtotime($date[1]));
                $query->whereBetween('t_pelayanan.t_tgl_pelayanan', [$startDate, $endDate]);
            })
            ->when(!empty($filter['tglSelesaiVerlab']), function ($query) use ($filter) {
                $date = explode(' - ', $filter['tglSelesaiVerlab']);
                $startDate = date('Y-m-d', strtotime($date[0]));
                $endDate = date('Y-m-d', strtotime($date[1]));
                $query->whereBetween('t_verifikasi_lapangan.t_tgl_verifikasi_lapangan', [$startDate, $endDate]);
            })
            ->when(!empty($filter['noPelayanan']), function ($query) use ($filter) {
                $query->where('t_pelayanan.t_no_pelayanan',  'like', "%" . $filter['noPelayanan'] . "%");
            })
            ->when(!empty($filter['nikPemohon']), function ($q) use ($filter) {
                $q->where('t_pelayanan.t_nik_pemohon', 'like', "%" . $filter['nikPemohon'] . "%");
            })
            ->when(!empty($filter['namaPemohon']), function ($q) use ($filter) {
                $q->where('t_pelayanan.t_nama_pemohon', 'ilike', "%{$filter['namaPemohon']}%");
            })
            ->when(!empty($filter['namaWp']), function ($query) use ($filter) {
                $query->whereRelation('wajibPajak', 't_nama_wp',  'ilike', "%{$filter['namaWp']}%");
                $query->orWhereRelation('WpLama', 't_nama_wp',  'ilike', "%{$filter['namaWp']}%");
            });


        return $data->paginate($pagination['pageSize'], ['*'], 'page', $pagination['pageNumber'] + 1);
    }
}
