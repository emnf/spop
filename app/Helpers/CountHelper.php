<?php

namespace App\Helpers;

use App\Models\Pelayanan\Pelayanan;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;

class CountHelper
{
    public function countPelayanan()
    {
        $pelayanan = new Pelayanan();

        return [
            'totalPelayanan' => $pelayanan->has('pelayananAprove')->count(),

            'totalBelumVerifikasi' => $pelayanan->has('pelayananAprove')->doesntHave('verifikasi')->count(),

            'totalVerifikasi' => $pelayanan->has('verifikasi')->count(),

            'totalBelumValidasi' => $pelayanan->whereHas('validasi', function (Builder $query) {
                $query->whereNotIn('t_validasi.t_id_status_validasi', [3, 4]);
            })->count(),

            'totalValidasi' => $pelayanan->WhereHas('validasi', function (Builder $query) {
                $query->whereIn('t_validasi.t_id_status_validasi', [3, 4]);
            })->count(),

            'totalPengajuanVerlap'  => $pelayanan->whereHas('validasi', function ($key) {
                $key->where('t_validasi.t_id_status_validasi', 5);
            })->count(),
            'totalSelesaiVerlap'  => $pelayanan->whereHas('validasi', function ($key) {
                $key->where('t_validasi.t_id_status_validasi', 6);
            })->count(),

            'totalBelumVerlap'  => $pelayanan->whereHas('validasi', function ($key) {
                $key->where('t_validasi.t_id_status_validasi', 5);
            })->count(),

            'totalTolakVerlap'  => $pelayanan->whereHas('verifikasiLapangan', function ($key) {
                $key->where('t_verifikasi_lapangan.t_id_status_verifikasi_lapangan', 3);
            })->count(),

            'totalVerlap'   => $pelayanan->whereHas('verifikasiLapangan', function ($key) {
                $key->where('t_verifikasi_lapangan.t_id_status_verifikasi_lapangan', 2);
            })->count(),

            'totalBelumDiUnggah' => $pelayanan->whereHas('validasi', function ($key) {
                $key->where('t_id_status_validasi', 4);
            })->doesntHave('hasilPelayanan')->count(),

            'totalSudahDiUnggah' => $pelayanan->whereHas('hasilPelayanan', function (Builder $query) {
                $query->whereNotNull('t_hasil_pelayanan.t_id_sk');
            })->count(),

            'totalHasilPelayanan' => $pelayanan->has('hasilPelayanan')->count()
        ];
    }

    public function countPerBulan()
    {

        $arrMonth = [
            array('key' => '01', 'name' => 'Januari'),
            array('key' => '02', 'name' => 'Februari'),
            array('key' => '03', 'name' => 'Maret'),
            array('key' => '04', 'name' => 'April'),
            array('key' => '05', 'name' => 'Mei'),
            array('key' => '06', 'name' => 'Juni'),
            array('key' => '07', 'name' => 'Juli'),
            array('key' => '08', 'name' => 'Agustus'),
            array('key' => '09', 'name' => 'September'),
            array('key' => '10', 'name' => 'Oktober'),
            array('key' => '11', 'name' => 'November'),
            array('key' => '12', 'name' => 'Desember'),
        ];
        $pelayanan = new pelayanan();
        $tampung = [];
        foreach ($arrMonth as $value) {

            $tampung[$value['name']] = [
                'totPengajuanPelayanan' => $pelayanan->whereHas('pelayananAprove', function (Builder $query) use ($value) {
                    $query->whereYear('t_pelayanan_approve.created_at', '=', Date('Y'));
                    $query->whereMonth('t_pelayanan_approve.created_at', '=', $value['key']);
                })->get()->count(),
                'totVerifikasiPelayanan' => $pelayanan->whereHas('verifikasi', function (Builder $query) use ($value) {
                    $query->whereYear('t_verifikasi.created_at', '=', Date('Y'));
                    $query->whereMonth('t_verifikasi.created_at', '=', $value['key']);
                })->get()->count(),
                'totValidasiPelayanan' => $pelayanan->whereHas('validasi', function (Builder $query) use ($value) {
                    $query->where('t_validasi.t_id_status_validasi', '=', 4);
                    $query->whereYear('t_validasi.created_at', '=', Date('Y'));
                    $query->whereMonth('t_validasi.created_at', '=', $value['key']);
                })->get()->count(),
                'totHasilPelayanan' => $pelayanan->whereHas('HasilPelayanan', function (Builder $query) use ($value) {
                    $query->whereYear('t_hasil_pelayanan.created_at', '=', Date('Y'));
                    $query->whereMonth('t_hasil_pelayanan.created_at', '=', $value['key']);
                })->get()->count()
            ];
        }

        return $tampung;
    }

    public function countPelayanPerTahun()
    {
        $pelayanan = new pelayanan();

        return [
            'totalAjuanPerTahun' => $pelayanan->whereHas('pelayananAprove', function (Builder $query) {
                $query->whereYear('t_pelayanan_approve.created_at', '=', Date('Y'));
            })->get()->count(),

            'totalVerifikasiPerTahun' => $pelayanan->whereHas('verifikasi', function (Builder $query) {
                $query->whereYear('t_verifikasi.created_at', '=', Date('Y'));
            })->get()->count(),

            'totalValidasiPerTahun' => $pelayanan->whereHas('validasi', function (Builder $query) {
                $query->whereYear('t_validasi.created_at', '=', Date('Y'));
                $query->where('t_validasi.t_id_status_validasi', '=', 4);
            })->get()->count(),

            'totalVerlapPerTahun' => $pelayanan->whereHas('VerifikasiLapangan', function (Builder $query) {
                $query->whereYear('t_verifikasi_lapangan.created_at', '=', Date('Y'));
            })->get()->count(),

            'totalHasilPelayananPerTahun' => $pelayanan->whereHas('HasilPelayanan', function (Builder $query) {
                $query->whereYear('t_hasil_pelayanan.created_at', '=', Date('Y'));
            })->get()->count()
        ];
    }
}
