<?php

namespace App\Helpers;

use App\Models\Setting\PemdaModel;
use Illuminate\Support\Carbon;

class PelayananHelper
{
    const JENIS_BUMI = "20";
    const KONDISI_BANGUNAN = "21";
    const KONSTRUKSI = "22";
    const ATAP = "41";
    const DINDING = "42";
    const LANTAI = "43";
    const LANGIT = "44";
    const KETERANGAN_OP = "84";

    const TANAH_BANGUNAN = 1;
    const KAVLING_SIAP_BANGUN = 2;
    const TANAH_KOSONG = 3;
    const FASILITAS_UMUM = 4;

    public static function formatNoUrut($tglPelayanan, $jenisPajak, $noUrut = 0)
    {
        return date('Y', strtotime($tglPelayanan)) . '.' . $jenisPajak . '.' . $noUrut;
    }

    public static function parseNop($nop)
    {
        $parsed = [
            "KD_PROPINSI" => substr($nop, 0, 2),
            "KD_DATI2" => substr($nop, 3, 2),
            "KD_KECAMATAN" => substr($nop, 6, 3),
            "KD_KELURAHAN" => substr($nop, 10, 3),
            "KD_BLOK" => substr($nop, 14, 3),
            "NO_URUT" => substr($nop, 18, 4),
            "KD_JNS_OP" => substr($nop, 23, 1)
        ];

        return $parsed;
    }

    public static function formatStringToNop($nop)
    {
        return substr($nop, 0, 2) . '.' . substr($nop, 2, 2) . '.' . substr($nop, 4, 3) . '.' . substr($nop, 7, 3) . '.' . substr($nop, 10, 3) . '.' . substr($nop, 13, 4) . '.' . substr($nop, 17, 1);
    }

    public static function trimNop($nop)
    {
        return str_replace(".", "", trim($nop));
    }

    public static function formatTglPerkiraanSelesaai($tglAwal, $estimasiHari, $tglSelesai = null)
    {
        // dd($tglAwal, $tglSelesai);
        $tglSelesai = Carbon::createFromFormat('Y-m-d H:i:s', $tglAwal)->addWeekdays($estimasiHari);
        $sisaHari = Carbon::createFromFormat('Y-m-d H:i:s', $tglAwal)->diffInDaysFiltered(function (Carbon $date) {
            return $date->isWeekday();
        }, $tglSelesai);
        // $sisaHari = $estimasiHari;

        if ($tglAwal < $tglSelesai) {

            $sisaHari = $sisaHari - ($sisaHari * 2);
            $warna = 'btn-success';
        } else {
            $warna = 'btn-danger';
        }

        return [
            'tglSelesai' => $tglSelesai,
            'sisaHari' => $sisaHari . ' Hari Kerja',
            'warna' => $warna
        ];
    }

    public static function formatNoPelayanan($tahunPelayanan, $idJenisPajak, $noUrut)
    {
        // 2022.10.000101
        return $tahunPelayanan . '.' . str_pad($idJenisPajak, 2, 0, STR_PAD_LEFT) . '.' . str_pad($noUrut, 6, 0, STR_PAD_LEFT);
    }

    public static function unFormatNoPelayanan($noPelayanan)
    {

        $parsed = [
            "tahunPelayanan" => substr($noPelayanan, 0, 4),
            "idjenisPajak" => substr($noPelayanan, 5, 2),
            "noUrut" => substr($noPelayanan, 8, 6),
        ];

        return $parsed;
    }

    public static function getDataPemda()
    {
        return PemdaModel::first();
    }

    public static function kekata($number)
    {
        $x = abs($number);
        $angka = array(
            "", "Satu", "Dua", "Tiga", "Empat", "Lima",
            "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas"
        );
        $temp = "";
        if ($x < 12) {
            $temp = " " . $angka[$x];
        } else if ($x < 20) {
            $temp = self::terbilang($x - 10) . " Belas ";
        } else if ($x < 100) {
            $temp = self::terbilang($x / 10) . " Puluh " . self::terbilang($x % 10);
        } else if ($x < 200) {
            $temp = " Seratus " . self::terbilang($x - 100);
        } else if ($x < 1000) {
            $temp = self::terbilang($x / 100) . " Ratus " . self::terbilang($x % 100);
        } else if ($x < 2000) {
            $temp = " Seribu " . self::terbilang($x - 1000);
        } else if ($x < 1000000) {
            $temp = self::terbilang($x / 1000) . " Ribu " . self::terbilang($x % 1000);
        } else if ($x < 1000000000) {
            $temp = self::terbilang($x / 1000000) . " Juta " . self::terbilang($x % 1000000);
        } else if ($x < 1000000000000) {
            $temp = self::terbilang($x / 1000000000) . " Milyar " . self::terbilang(fmod($x, 1000000000));
        } else if ($x < 1000000000000000) {
            $temp = self::terbilang($x / 1000000000000) . " Trilyun " . self::terbilang(fmod($x, 1000000000000));
        }

        return $temp;
    }

    public function terbilang($x, $style = 5)
    {
        if ($x < 0) {
            $hasil = "MINUS " . trim(self::kekata($x));
        } else {
            $hasil = trim(self::kekata($x));
        }

        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            case 4:
                $hasil = ucfirst($hasil);
                break;
            default:
                return $hasil;
                break;
        }
        return $hasil;
    }
}
