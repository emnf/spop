<?php

namespace App\Helpers;

use App\Models\Pbb\Kecamatan;
use App\Models\Pbb\Kelurahan;
use App\Models\Setting\JenisPelayanan;

class ComboHelper
{

    public static function getDataJenisPelayanan($idJenisPajak = '', $active = false, $idJenisPelayanan = '')
    {
        $sql = (new JenisPelayanan())->with('dataJenisPajak');
        $sql = ($idJenisPajak != '') ? $sql->where('s_id_jenis_pajak', $idJenisPajak) : $sql;
        $sql = ($idJenisPelayanan != '') ? $sql->where('s_id_jenis_pelayanan', $idJenisPelayanan) : $sql;
        $sql = ($active == true) ? $sql->where('s_active', true) : $sql;
        return $sql->get();
    }

    public static function getDataKecamatan($idKecamatan = '', $daerah = true)
    {
        $sql = new Kecamatan();
        $sql = ($idKecamatan != '') ? $sql->where('KD_KECAMATAN', $idKecamatan) : $sql;
        // luar daerah
        $sql = ($daerah) ? $sql->whereNotIn('KD_KECAMATAN', ['000', '00']) : $sql;
        return $sql->get();
    }

    public static function getDataKelurahan($idKecamatan = '', $idKelurahan = '')
    {
        $sql = new Kelurahan();
        $sql = ($idKecamatan != '') ? $sql->where('KD_KECAMATAN', $idKecamatan) : $sql;
        $sql = ($idKelurahan != '') ? $sql->where('KD_KELURAHAN', $idKelurahan) : $sql;
        return $sql->get();
    }
}
