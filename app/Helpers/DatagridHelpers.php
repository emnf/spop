<?php

namespace App\Helpers;

class DatagridHelpers
{
    public function getSuccessResponse($dataArr, $response)
    {
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => (($response->lastPage() <= 15) ? $response->lastPage() : 15) == $response->currentPage() ? true : false,
                'totalPages' => ($response->lastPage() <= 15) ? $response->lastPage() : 15,
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total(),
                'success' => true
            ]
        ];

        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function getErrorResponse(string $message = null)
    {
        return response()->json(
            [
                'data' => [
                    'success' => false,
                    'message' => is_null($message) ? 'Terjadi kesalahan server' : $message
                ]
            ],
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }
}
