<?php

namespace App\Notifications;

use App\Helpers\PelayananHelper;
use App\Models\Pelayanan\Pelayanan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class HasilPelayananPermohonanNotify extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Pelayanan $pelayanan, User $user)
    {
        $this->pelayanan = $pelayanan;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    public function toDatabase()
    {
        $this->pelayanan
            ->load('jenisPelayanan')
            ->load('pelayananAprove');
        $message = "Hasil Permohonan " . PelayananHelper::formatNoPelayanan(date('Y', strtotime($this->pelayanan->t_tgl_pelayanan)), $this->pelayanan->t_id_jenis_pajak, $this->pelayanan->t_no_pelayanan) . " sudah ada. ";

        return [
            'models' => [
                'Pelayanan' => $this->pelayanan->t_id_pelayanan,
                'User' => $this->user->id,
            ],
            'message' => $message,
            'tanggal' => date('Y-m-d H:i:s')
        ];
    }
}
