<?php

namespace App\Notifications;

use App\Models\Pelayanan\Pelayanan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PendaftaranNotify extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Pelayanan $pelayanan, User $user)
    {
        $this->pelayanan = $pelayanan;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    public function toDatabase()
    {
        $this->pelayanan
            ->load('jenisPelayanan')
            ->load('pelayananAprove');
        $message = "Permohonan " . $this->pelayanan->jenisPelayanan->s_nama_jenis_pelayanan . "di ajukan oleh " . $this->pelayanan->t_nama_pemohon;

        return [
            'models' => [
                'Pelayanan' => $this->pelayanan->t_id_pelayanan,
                'User' => $this->user->id,
            ],
            'message' => $message,
            'tanggal' => $this->pelayanan->pelayananAprove->created_at
        ];
    }
}
