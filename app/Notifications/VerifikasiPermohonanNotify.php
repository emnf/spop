<?php

namespace App\Notifications;

use App\Helpers\PelayananHelper;
use App\Models\Pelayanan\Pelayanan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class VerifikasiPermohonanNotify extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Pelayanan $pelayanan, User $user, string $message)
    {
        $this->pelayanan = $pelayanan;
        $this->user = $user;
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }


    public function toDatabase()
    {
        return [
            'models' => [
                'Pelayanan' => $this->pelayanan->t_id_pelayanan,
                'User' => $this->user->id,
            ],
            'message' => $this->message,
            'tanggal' => date('Y-m-d H:i:s')
        ];
    }
}
