$(function () {
    const markRead = function (key, link) {
        $.post("/notifikasi/mark-as-read",{
                id: key,
                _token: $('meta[name="csrf-token"]').attr("content"),
            }, (res) => {
                if (res.success) {
                    window.location.href = link;
                }
            }
        );
    };

    const notificatin = function () {
        $.get("/notifikasi", (response) => {
            let message = "";

            if (response.length > 0) {
                Toast.fire({
                    icon: "info",
                    title: ` Ada ${response.length}  Pemberitahuan belum dibaca.`,
                    position: 'top-end',
                    timer: 2000,
                });
            }

            response.map((msg) => {
                message += `<a href="#" class="dropdown-item notification-message" data-id="${msg.messageId}" data-link="${msg.urlTarget}">
                          <div class="media">
                          <div class="media-body">
                            <h3 class="dropdown-item-title"></h3>
                            <p class="text-sm">${msg.messageData.message}</p>
                            <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i>${msg.messageData.tanggal}</p>
                          </div>
                        </div></a>
                        <div class="dropdown-divider"></div>`;
            });
            $(".notification-count").text(response.length);
            $(".notification-items").append(message);

            $(".notification-message").click((event) => {
                markRead($(event.currentTarget).data("id"), $(event.currentTarget).data("link"));
            });
        });
    };

    const Toast = Swal.mixin({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000,
    });

    notificatin();
});
