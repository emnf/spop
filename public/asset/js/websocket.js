var WebSocket = function (options) {
    'use strict';

    var stompClient;
    var reconnectInterval;
    var socket;

    _connect();

    function _connect() {
        socket = new SockJS(options.socketUrl);
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function (frame) {
            options.subscribes(stompClient);
        });

        clearInterval(reconnectInterval);

        socket.onclose = function(){
            socket = null;
            reconnectInterval = setInterval(function(){
                _connect();
            }, 5000);
        }
    }

    function _disconnect() {
        if (stompClient !== null) {
            stompClient.disconnect();
        }
    }

    function sendMessage(endpoint, message) {
        stompClient.send(endpoint, {}, message);
    }

    return {
        sendMessage : sendMessage
    };
}