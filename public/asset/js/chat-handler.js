function toggleChat() {
    var formChat = $("#form-chat");
    var infoChat = $("#info-chat");
    if (formChat.hasClass("d-none")) {
      formChat.removeClass("d-none");
      infoChat.addClass("d-none");
    } else {
      formChat.addClass("d-none");
      infoChat.removeClass("d-none");
    }
    initSound();
}

function unreadCounter(){
    $.get("/message/unread-count/"+ $("#chat-user-id").val()).then(function(count){
        $("#float-chat-badge").text(count);
    })
}
function updateChatIllustration(count){
    if(count>0){
        $("#chat-conversation-box").removeClass("chat-no-messages");
    }else{
        $("#chat-conversation-box").addClass("chat-no-messages");
    }
}

function scrollToLatestChat(){
    $('#chat-conversation-box').scrollTop($('#chat-conversation-box')[0].scrollHeight);
}

function updateChat(){
    var userId = $("#chat-user-id").val();
    var history = $("#chat-history").val();
    var idPelayanan = $("#chat-id-pelayanan").val();
    var showHistory = history ? '/history' : '';
    var tagPelayanan = idPelayanan ? '/' + idPelayanan : '';
    $.get("/message/"+ userId + showHistory + tagPelayanan).then(function(response){
        var messages = history ? response : response.content;
        var conversation = '';

        if(messages){
            updateChatIllustration(messages.length);
            messages.forEach(message => {
                var user = (message.sentBy==userId)? 'Anda': message.sender.nama;
                var isRight = (message.sentBy!=userId);
                var classRight = isRight ? 'right' : '';
                var classFloatRight = isRight ? 'float-right': 'float-left';

                conversation += '<div class="direct-chat-msg '+classRight+'">';
                conversation +=   '<div class="direct-chat-infos clearfix">';
                conversation +=     '<span class="direct-chat-name '+classFloatRight+'">'+user+'</span>'
                conversation +=   '</div>';
                conversation +=   '<img class="direct-chat-img" src="/static/images/user-chat.png" alt="message user image">';
                conversation +=   '<div class="direct-chat-text">'+message.text+'</div>';
                conversation +=     '<div class="direct-chat-infos clearfix">';
                conversation +=     '<span class="direct-chat-timestamp '+classFloatRight+'" style="font-size: 9pt; margin-top: 0.5rem;"><i class="fa fa-clock">&nbsp;</i>'+message.sentAt+'</span>';
                conversation +=   '</div></div>';
            });
        }
        $("#chat-conversation-box").html(conversation);
        scrollToLatestChat();
    })
}

function sendingMessage(){
    $("#btnSendMessage").text("Mengirim...");
    $("#chat-message").attr("readonly", "readonly");
    $("#btnSendMessage").attr("disabled", "disabled");
}

function messageSent(){
    $("#btnSendMessage").text("Kirim");
    $("#btnSendMessage").removeAttr("disabled");
    $("#chat-message").val('');
    $("#chat-message").removeAttr("readonly");
}

function sendMessage(){
    var message = $("#chat-message").val();
    if(message.length>0){
        sendingMessage();
        $.ajax({
            type: "POST",
            url: "/message/send",
            data: JSON.stringify({
                sentBy: $("#chat-user-id").val(),
                text : message,
                recipient: $("#chat-recipient").val(),
                idPelayanan: $("#chat-id-pelayanan").val()
            }),
            processData: false,
            contentType: "application/json",
            cache: false,
            timeout: 600000,
            success: function (data) {
                updateChat();
                messageSent();
            },
            error: function (e) {
                messageSent();
            }
        });
    }
}

function initSound(){
    var audio = document.getElementById("chat-sound");
    audio.currentTime = 0;
    audio.play().then(()=>audio.pause());
}

function incomingMessage(){
    var audio = document.getElementById("chat-sound");
    audio.muted = false;
    audio.currentTime = 0;
    audio.play();
}

function refreshChat(){
    $.get("/message/user").then(function(users){

        var chatBox =  document.getElementById('chat-conversation-box');
        if (typeof(chatBox) != 'undefined' && chatBox != null)
        {
            $("#chat-user-id").val(users.idUser);
            unreadCounter();
            updateChat();
        }
    })
}

$(function(){
    var chat = WebSocket({
        socketUrl: '/elayanan-websocket',
        subscribes : function(socketClient){
            socketClient.subscribe('/chatting/new-message', function(response){
                updateChat();
                unreadCounter();
                if(response.body==$("#chat-user-id").val()){
                    incomingMessage();
                }
            })
        }
    });

    $("#btnSendMessage").click(function(){
        sendMessage();
    });

    $("#chat-message").on("keypress", function(e){
        if(e.which==13){
            sendMessage();
        }
    });

    $("body").mouseenter(function(){
        initSound();
    });

    refreshChat();
})