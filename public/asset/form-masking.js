$(document).ready(function () {
  $(".masked-nop").mask('99.99.999.999.999.9999.9');
  $(".masked-nop-tahun").mask('99.99.999.999.999.9999.9-9999');
  $(".masked-nik").mask('9999999999999999');
  $(".masked-npwpd").mask('P.99999.99.99');
  $(".masked-npwp").mask('99.999.999.9-999.999');
  $(".masked-number-2").mask('99');
  $(".masked-number-3").mask('999');
  $(".masked-number-4").mask('9999');
  $(".masked-number-5").mask('99999');
  $(".masked-hp").mask('999999999999999');
  $(".masked-nopsss").mask('999.9999.9');
});
