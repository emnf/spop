<?php
// Oracle database connection details
$oracleUsername = 'BLORA';
$oraclePassword = 'BLORA';
$oracleHostname = 'localhost';
$oracleServiceName = 'SIMPBB';

// Construct the Oracle connection string
$oracleConnectionString = "(DESCRIPTION=(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = $oracleHostname)(PORT = 1521)))(CONNECT_DATA=(SERVICE_NAME=$oracleServiceName)))";

// Attempt to establish a connection
$oracleConnection = oci_connect($oracleUsername, $oraclePassword, $oracleConnectionString);

// Check if the connection was successful
if (!$oracleConnection) {
    $error = oci_error(); // Fetch Oracle error information
    echo "Failed to connect to Oracle database: " . $error['message'];
} else {
    echo "Connected to Oracle database successfully!";
    // Perform database operations here...

    // Close the connection when done
    oci_close($oracleConnection);
}
