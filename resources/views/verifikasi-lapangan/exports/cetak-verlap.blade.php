@extends('layouts.print')

@section('body')
<table style="width: 100%; border-bottom: 5px double #000" cellspacing="0" cellpadding="0">
    <tr>
        <td style="text-align: center; width: 20%">
            <img style="width: 25%" title="LOGO" src="{{ $pemda->s_logo }}" />
        </td>
        <td style="text-align: center">
            <span style="font-size: 11pt;font-weight: bold">PEMERINTAH {{ strtoupper($pemda->s_nama_kabkota) }}</span><br>
            <span style="font-size: 14pt;font-weight: bold">{{ strtoupper($pemda->s_nama_instansi) }}</span><br>
            <span style="font-size: 9pt;">
                {{ ucwords(strtolower($pemda->s_alamat_instansi)) . ' Telp/Fax. ' . $pemda->s_notelp_instansi .', Kode Pos '. $pemda->s_kode_pos }}
            </span><br><br>
        </td>
        <td style="width: 20%">
            &nbsp;
        </td>
    </tr>
</table>
<table style="width: 100%" border="0">
    <tr>
        <td colspan="8" class="text-center">
            <h4>DATA VERIFIKASI LAPANGAN</h4>
        </td>
    </tr>
</table>

<table id="table" style="width: 100%; border-collapse: collapse;">
    <thead>
        <tr>
            <th class="border_kiri border_atas border_kanan border_bawah text-center">No.</th>
            <th class="border_kiri border_atas border_kanan border_bawah text-center">Jenis Pajak</th>
            <th class="border_kiri border_atas border_kanan border_bawah text-center">Nama Pelayanan</th>
            <th class="border_kiri border_atas border_kanan border_bawah text-center">No. Pelayanan</th>
            <th class="border_kiri border_atas border_kanan border_bawah text-center">Tanggal Pelayanan</th>
            <th class="border_kiri border_atas border_kanan border_bawah text-center">Nama Pemohon</th>
            <th class="border_kiri border_atas border_kanan border_bawah text-center">Tgl. Verlap</th>
            <th class="border_kiri border_atas border_kanan border_bawah text-center">Keterangan Verlap</th>
        </tr>
    </thead>
    <tbody>
        @if($pelayanan->count() > 0)
        @foreach ($pelayanan as $i => $row)
        <tr>
            <td class="border_kiri border_bawah text-center">{{ $i+1 }}</td>
            <td class="border_kiri border_bawah text-center">{{ $row->jenisPajak->s_nama_singkat_pajak }}</td>
            <td class="border_kiri border_bawah">{{ $row->jenisPelayanan->s_nama_jenis_pelayanan }}</td>
            <td class="border_kiri border_bawah">{{ App\Helpers\PelayananHelper::formatNoPelayanan(date('Y',
                strtotime($row->t_tgl_pelayanan)), $row->t_id_jenis_pajak, $row->t_no_pelayanan) }}</td>
            <td class="border_kiri border_bawah text-center">{{ date('d-m-Y', strtotime($row->t_tgl_pelayanan)) }}
            </td>
            <td class="border_kiri border_bawah border_kanan">{{ $row->t_nama_pemohon }}</td>
            <td class="border_kiri border_bawah border_kanan">{{ $row->verifikasiLapangan
                ? date('d-m-Y', strtotime($row->verifikasiLapangan->t_tgl_verifikasi_lapangan)) : '' }}</td>
            <td class="border_kiri border_bawah border_kanan">{{ $row->verifikasiLapangan
                ? $row->verifikasiLapangan->t_keterangan_verifikasi_lapangan : '' }}</td>
        </tr>
        @endforeach
        @else
        <tr>
            <td class="border_kiri border_bawah">&nbsp;</td>
            <td class="border_kiri border_bawah">&nbsp;</td>
            <td class="border_kiri border_bawah">&nbsp;</td>
            <td class="border_kiri border_bawah">&nbsp;</td>
            <td class="border_kiri border_bawah">&nbsp;</td>
            <td class="border_kiri border_bawah border_kanan">&nbsp;</td>
            <td class="border_kiri border_bawah border_kanan">&nbsp;</td>
            <td class="border_kiri border_bawah border_kanan">&nbsp;</td>
        </tr>
        @endif
    </tbody>
</table>

@endsection
