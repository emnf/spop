<div data-th-fragment="datagrid" class="tab-pane fade active show" id="navtab1" role="tabpanel">
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-sm-2">
            <button class="btn btn-danger btn-sm" onclick="printDataBelum('PDF')"><span class="fas fa-file-pdf"></span>
                &nbsp;PDF</button>
            <button class="btn btn-success btn-sm" onclick="printDataBelum('XLS')"><span
                    class="fas fa-file-excel"></span> &nbsp;XLS</button>
        </div>
    </div>
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-bordered table-striped table-hover" id="datagrid">
                <thead>
                    <tr>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Jenis Pajak</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pelayanan
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Permohonan
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No. Pelayanan
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pemohon
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Verifikasi
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Max Verlap</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Keterangan Validasi</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Action&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>
                            {{-- <select class="form-control form-control-sm " id="filter-idJenisPajak">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($jenisPajak as $item)
                                    <option value="{{ $item->s_id_jenis_pajak }}">{{ $item->s_nama_singkat_pajak }}</option>
                                @endforeach
                            </select> --}}
                        </th>
                        <th>
                            <select class="form-control form-control-sm " id="filter-idJenisPelayanan">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($jenisPelayanan as $item)
                                    <option value="{{ $item->s_id_jenis_pelayanan }}">{{ $item->s_nama_jenis_pelayanan }}</option>
                                @endforeach
                            </select>
                        </th>
                        <th><input class="form-control form-control-sm " type="text" id="filter-tglPelayanan"></th>
                        <th><input class="form-control form-control-sm " type="text" id="filter-noPelayanan"></th>
                        <th><input class="form-control form-control-sm " type="text" id="filter-namaPemohon"></th>
                        <th><input class="form-control form-control-sm " type="text" id="filter-tglVerifikasi"></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="9"> Tidak ada data.</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-12 pagination-footer"></div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    $('#filter-tglPelayanan, #filter-tglVerifikasi').daterangepicker({
        autoUpdateInput: false,
         locale: {
            cancelLabel: 'Clear'
         }
    });

    $('#filter-tglPelayanan, #filter-tglVerifikasi').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        search();
    });

    $('#filter-tglPelayanan, #filter-tglVerifikasi').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        search();
    });

    $("#filter-tglPelayanan, #filter-idJenisPajak, #filter-idJenisPelayanan").change(function(){
        search();
    });

    $("#filter-noPelayanan, #filter-namaPemohon, #filter-namaJenisPelayanan").keyup(function(){
        search();
    });

    var datatables = datagrid({
        url: '/verifikasi-lapangan/datagrid-belum-proses',
        table: "#datagrid",
        columns: [
            {class: ""},
            {class: ""},
            {class: "text-center"},
            {class: "text-center"},
            {class: ""},
            {class: "text-center"},
            {class: ""},
            {class: ""},
            {class: "text-center"},
        ],
        orders: [
            {sortable: false},
            {sortable: true, name: "idJenisPajak"},
            {sortable: true, name: "idJenisPelayanan"},
            {sortable: true, name: "tglPelayanan"},
            {sortable: true, name: "noPelayanan"},
            {sortable: true, name: "namaPemohon"},
            {sortable: true, name: "verifikasi.tglVerifikasi"},
            {sortable: false},
            {sortable: false},
            {sortable: false},
        ],
        action: [
            {
                name: 'tambah-verifikasi-lapangan',
                btnClass: 'btn btn-warning btn-xs',
                btnText: '<i class="fas fa-edit"> Verifikasi Lapangan</i>'
            },
        ]

    });

    function search(){
        datatables.setFilters({
            idJenisPajak: $("#filter-idJenisPajak").val(),
            tglPelayanan: $("#filter-tglPelayanan").val(),
            noPelayanan: $("#filter-noPelayanan").val(),
            idJenisPelayanan: $("#filter-idJenisPelayanan").val(),
            namaPemohon: $("#filter-namaPemohon").val(),
            tglVerifikasi: $("#filter-tglVerifikasi").val(),
        });
        datatables.reload();
    }

    search();

    function printDataBelum(type){
        window.open('/verifikasi-lapangan/cetak-verlap-belum-proses?format=' + type
        + '&idJenisPajak=' + $("#filter-idJenisPajak").val()
        + '&tglPelayanan=' + $("#filter-tglPelayanan").val()
        + '&noPelayanan=' + $("#filter-noPelayanan").val()
        + '&idJenisPelayanan=' + $("#filter-idJenisPelayanan").val()
        + '&namaPemohon=' + $("#filter-namaPemohon").val()
        + '&tglVerifikasi=' + $("#filter-tglVerifikasi").val()
        + '');
    }
</script>
@endpush
