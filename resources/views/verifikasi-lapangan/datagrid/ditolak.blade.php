<div data-th-fragment="datagrid" class="tab-pane fade" id="navtab2" role="tabpanel">
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-sm-2">
            <button class="btn btn-danger btn-sm" onclick="printDataDitolak('PDF')"><span
                    class="fas fa-file-pdf"></span> &nbsp;PDF</button>
            <button class="btn btn-success btn-sm" onclick="printDataDitolak('XLS')"><span
                    class="fas fa-file-excel"></span> &nbsp;XLS</button>
        </div>
    </div>
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-bordered table-striped table-hover" id="datagrid2">
                <thead>
                    <tr>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Jenis Pajak</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pelayanan
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Permohonan
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No. Pelayanan
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pemohon
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Verlap</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Keterangan Verlap</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>
                            {{-- <select class="form-control form-control-sm " id="filter-idJenisPajak2">
                                <option value="">Silahkan Pilih</option>
                                @foreach($jenisPajak as $item)
                                    <option value="{{ $item->s_id_jenis_pajak }}">{{ $item->s_nama_singkat_pajak }}</option>
                                @endforeach
                            </select> --}}
                        </th>
                        <th>
                            <select class="form-control form-control-sm " id="filter-idJenisPelayanan2">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($jenisPelayanan as $item)
                                    <option value="{{ $item->s_id_jenis_pelayanan }}">{{ $item->s_nama_jenis_pelayanan }}</option>
                                @endforeach
                            </select>
                        </th>
                        <th><input class="form-control form-control-sm " type="text" id="filter-tglPelayanan2"></th>
                        <th><input class="form-control form-control-sm " type="text" id="filter-noPelayanan2"></th>
                        <th><input class="form-control form-control-sm " type="text" id="filter-namaPemohon2"></th>
                        <th><input class="form-control form-control-sm " type="text" id="filter-tglVerifikasiLapangan2">
                        </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="9"> Tidak ada data.</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-12 pagination-footer"></div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    $('#filter-tglPelayanan2, #filter-tglVerifikasiLapangan2').daterangepicker({
        autoUpdateInput: false,
         locale: {
            cancelLabel: 'Clear'
         }
    });

    $('#filter-tglPelayanan2, #filter-tglVerifikasiLapangan2').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        search2();
    });

    $('#filter-tglPelayanan2, #filter-tglVerifikasiLapangan2').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        search2();
    });

    $("#filter-idJenisPajak2, #filter-idJenisPelayanan2").change(function(){
        search2();
    });

    $("#filter-namaJenisPelayanan3, #filter-noPelayanan2, #filter-namaPemohon2").keyup(function(){
        search2();
    });

    var datatables2 = datagrid({
        url: '/verifikasi-lapangan/datagrid-ditolak',
        table: "#datagrid2",
        columns: [
            {class: ""},
            {class: ""},
            {class: "text-center"},
            {class: "text-center"},
            {class: ""},
            {class: "text-center"},
            {class: ""},
        ],
        orders: [
            {sortable: false},
            {sortable: true, name: "idJenisPajak"},
            {sortable: true, name: "idJenisPelayanan"},
            {sortable: true, name: "tglPelayanan"},
            {sortable: true, name: "noPelayanan"},
            {sortable: true, name: "namaPemohon"},
            {sortable: true, name: "verifikasiLapangan.tglVerifikasiLapangan"},
            {sortable: false},
        ],
        action: [
        ]
    });

    function search2(){
        datatables2.setFilters({
            idJenisPajak: $("#filter-idJenisPajak2").val(),
            tglPelayanan: $("#filter-tglPelayanan2").val(),
            noPelayanan: $("#filter-noPelayanan2").val(),
            namaPemohon: $("#filter-namaPemohon2").val(),
            tglVerifikasiLapangan: $("#filter-tglVerifikasiLapangan2").val(),
            idJenisPelayanan: $("#filter-idJenisPelayanan2").val(),
        });
        datatables2.reload();
    }
    search2();

    function printDataDitolak(type){
        window.open('/verifikasi-lapangan/cetak-verlap-ditolak?format=' + type
        + '&idJenisPajak=' + $("#filter-idJenisPajak2").val()
        + '&tglPelayanan=' + $("#filter-tglPelayanan2").val()
        + '&noPelayanan=' + $("#filter-noPelayanan2").val()
        + '&namaPemohon=' + $("#filter-namaPemohon2").val()
        + '&tglVerifikasiLapangan=' + $("#filter-tglVerifikasiLapangan2").val()
        + '&idJenisPelayanan=' + $("#filter-idJenisPelayanan2").val()
        + '');
    }
</script>
@endpush
