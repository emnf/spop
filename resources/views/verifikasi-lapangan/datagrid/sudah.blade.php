<div data-th-fragment="datagrid" class="tab-pane fade" id="navtab3" role="tabpanel">
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-sm-2">
            <button class="btn btn-danger btn-sm" onclick="printDataSudah('PDF')"><span class="fas fa-file-pdf"></span>
                &nbsp;PDF</button>
            <button class="btn btn-success btn-sm" onclick="printDataSudah('XLS')"><span
                    class="fas fa-file-excel"></span> &nbsp;XLS</button>
        </div>
    </div>
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-bordered table-striped table-hover" id="datagrid3">
                <thead>
                    <tr>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Jenis Pajak</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pelayanan
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Permohonan
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No. Pelayanan
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pemohon
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Verlap</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Keterangan Verlap</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>
                            {{-- <select class="form-control form-control-sm " id="filter-idJenisPajak3">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($jenisPajak as $item)
                                    <option value="{{ $item->s_id_jenis_pajak }}">{{ $item->s_nama_singkat_pajak }}</option>
                                @endforeach
                            </select> --}}
                        </th>
                        <th>
                            <select class="form-control form-control-sm " id="filter-idJenisPelayanan3">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($jenisPelayanan as $item)
                                    <option value="{{ $item->s_id_jenis_pelayanan }}">{{ $item->s_nama_jenis_pelayanan }}</option>
                                @endforeach
                            </select>
                        </th>
                        <th><input class="form-control form-control-sm " type="text" id="filter-tglPelayanan3"></th>
                        <th><input class="form-control form-control-sm " type="text" id="filter-noPelayanan3"></th>
                        <th><input class="form-control form-control-sm " type="text" id="filter-namaPemohon3"></th>
                        <th><input class="form-control form-control-sm " type="text" id="filter-tglVerifikasiLapangan3">
                        </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="9"> Tidak ada data.</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-12 pagination-footer"></div>
    </div>
</div>

@push('scripts')
<script th:inline="javascript" data-th-fragment="js">
    $('#filter-tglPelayanan3, #filter-tglVerifikasiLapangan3').daterangepicker({
        autoUpdateInput: false,
         locale: {
            cancelLabel: 'Clear'
         }
    });

    $('#filter-tglPelayanan3, #filter-tglVerifikasiLapangan3').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        search3();
    });

    $('#filter-tglPelayanan3, #filter-tglVerifikasiLapangan3').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        search3();
    });

    $("#filter-idJenisPajak3, #filter-idJenisPelayanan3").change(function(){
        search3();
    });

    $("#filter-namaJenisPelayanan3, #filter-noPelayanan3, #filter-namaPemohon3").keyup(function(){
        search3();
    });

    var datatables3 = datagrid({
        url: '/verifikasi-lapangan/datagrid-sudah',
        table: "#datagrid3",
        columns: [
            {class: ""},
            {class: ""},
            {class: "text-center"},
            {class: "text-center"},
            {class: ""},
            {class: "text-center"},
            {class: ""},
        ],
        orders: [
            {sortable: false},
            {sortable: true, name: "idJenisPajak"},
            {sortable: true, name: "idJenisPelayanan"},
            {sortable: true, name: "tglPelayanan"},
            {sortable: true, name: "noPelayanan"},
            {sortable: true, name: "namaPemohon"},
            {sortable: true, name: "verifikasiLapangan.tglVerifikasiLapangan"},
            {sortable: false},
        ],
        action: [
        ]
    });

    function search3(){
        datatables3.setFilters({
            idJenisPajak: $("#filter-idJenisPajak3").val(),
            tglPelayanan: $("#filter-tglPelayanan3").val(),
            noPelayanan: $("#filter-noPelayanan3").val(),
            namaPemohon: $("#filter-namaPemohon3").val(),
            tglVerifikasiLapangan: $("#filter-tglVerifikasiLapangan3").val(),
            idJenisPelayanan: $("#filter-idJenisPelayanan3").val(),
        });
        datatables3.reload();
    }

    search3();

    function printDataSudah(type){
        window.open('/verifikasi-lapangan/cetak-verlap-sudah?format=' + type
        + '&idJenisPajak=' + $("#filter-idJenisPajak3").val()
        + '&tglPelayanan=' + $("#filter-tglPelayanan3").val()
        + '&noPelayanan=' + $("#filter-noPelayanan3").val()
        + '&namaPemohon=' + $("#filter-namaPemohon3").val()
        + '&idJenisPelayanan=' + $("#filter-idJenisPelayanan3").val()
        + '&tglVerifikasiLapangan=' + $("#filter-tglVerifikasiLapangan3").val()
        + '');
    }
</script>
@endpush
