<div data-th-fragment="datagrid" class="tab-pane fade" id="navtab4" role="tabpanel">
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-sm-2">
            <button class="btn btn-danger btn-sm" onclick="printDataSemua('PDF')"><span class="fas fa-file-pdf"></span>
                &nbsp;PDF</button>
            <button class="btn btn-success btn-sm" onclick="printDataSemua('XLS')"><span
                    class="fas fa-file-excel"></span> &nbsp;XLS</button>
        </div>
    </div>

    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-bordered table-striped table-hover" id="datagrid4">
                <thead>
                    <tr>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Jenis Pajak</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pelayanan
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Permohonan
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No. Pelayanan
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pemohon
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Status</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Verlap
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Keterangan Verlap</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Action</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>
                            {{-- <select class="form-control form-control-sm " id="filter-idJenisPajak4">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($jenisPajak as $item)
                                    <option value="{{ $item->s_id_jenis_pajak }}">{{ $item->s_nama_singkat_pajak }}</option>
                                @endforeach
                            </select> --}}
                        </th>
                        <th>
                            <select class="form-control form-control-sm " id="filter-idJenisPelayanan4">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($jenisPelayanan as $item)
                                    <option value="{{ $item->s_id_jenis_pelayanan }}">{{ $item->s_nama_jenis_pelayanan }}</option>
                                @endforeach
                            </select>
                        </th>
                        <th><input class="form-control form-control-sm " type="text" id="filter-tglPelayanan4"></th>
                        <th><input class="form-control form-control-sm " type="text" id="filter-noPelayanan4"></th>
                        <th><input class="form-control form-control-sm " type="text" id="filter-namaPemohon4"></th>
                        <th>
                            <select class="form-control form-control-sm " id="filter-idStatusVerifikasiLapangan4">
                                <option value="">Silahkan Pilih</option>
                                <option th:each="statusVerifikasiLapangan: ${statusVerifikasiLapangan}"
                                    th:text="${statusVerifikasiLapangan.namaStatusVerifikasiLapangan}"
                                    th:value="${statusVerifikasiLapangan.idStatusVerifikasiLapangan}">
                                </option>
                            </select>
                        </th>
                        <th><input class="form-control form-control-sm " type="text" id="filter-tglVerifikasiLapangan4">
                        </th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="10"> Tidak ada data.</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-12 pagination-footer"></div>
    </div>
</div>

@push('scripts')
<script>
    $('#filter-tglPelayanan4, #filter-tglVerifikasiLapangan4').daterangepicker({
        autoUpdateInput: false,
         locale: {
            cancelLabel: 'Clear'
         }
    });

    $('#filter-tglPelayanan4, #filter-tglVerifikasiLapangan4').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        search5();
    });

    $('#filter-tglPelayanan4, #filter-tglVerifikasiLapangan4').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        search5();
    });

    $("#filter-idJenisPajak4, #filter-idStatusVerifikasiLapangan4, #filter-idJenisPelayanan4").change(function(){
        search5();
    });

    $("#filter-noPelayanan4, #filter-namaPemohon4, #filter-namaJenisPelayanan5").keyup(function(){
        search5();
    });

    var datatables4 = datagrid({
        url: '/verifikasi-lapangan/datagrid-semua',
        table: "#datagrid4",
        columns: [
            {class: ""},
            {class: ""},
            {class: "text-center"},
            {class: "text-center"},
            {class: ""},
            {class: "text-center"},
            {class: "text-center"},
            {class: ""},
            {class: "text-center"},
        ],
        orders: [
            {sortable: false},
            {sortable: true, name: "idJenisPajak"},
            {sortable: true, name: "idJenisPelayanan"},
            {sortable: true, name: "tglPelayanan"},
            {sortable: true, name: "noPelayanan"},
            {sortable: true, name: "namaPemohon"},
            {sortable: true, name: "verifikasiLapangan.caseStatusVerifikasiLapangan"},
            {sortable: true, name: "verifikasiLapangan.tglVerifikasiLapangan"},
            {sortable: false},
            {sortable: false},
        ],
        action: [
            {
                name: 'tambah-verifikasi-lapangan',
                btnClass: 'btn btn-warning btn-xs',
                btnText: '<i class="fas fa-edit"> Verifikasi Lapangan</i>'
            },
        ]

    });

    function search5(){
        datatables4.setFilters({
            idJenisPajak: $("#filter-idJenisPajak4").val(),
            tglPelayanan: $("#filter-tglPelayanan4").val(),
            noPelayanan: $("#filter-noPelayanan4").val(),
            namaPemohon: $("#filter-namaPemohon4").val(),
            idStatusVerifikasiLapangan: $("#filter-idStatusVerifikasiLapangan4").val(),
            tglVerifikasiLapangan: $("#filter-tglVerifikasiLapangan4").val(),
            idJenisPelayanan: $("#filter-idJenisPelayanan4").val(),
        });
        datatables4.reload();
    }

    search5();

    function printDataSemua(type){
        window.open('/verifikasi-lapangan/cetak-verlap-semua?format=' + type
        + '&idJenisPajak=' + $("#filter-idJenisPajak4").val()
        + '&tglPelayanan=' + $("#filter-tglPelayanan4").val()
        + '&noPelayanan=' + $("#filter-noPelayanan4").val()
        + '&namaPemohon=' + $("#filter-namaPemohon4").val()
        + '&idJenisPelayanan=' + $("#filter-idJenisPelayanan4").val()
        + '&tglVerifikasiLapangan=' + $("#filter-tglVerifikasiLapangan4").val()
        + '&idStatusVerifikasiLapangan=' + $("#filter-idStatusVerifikasiLapangan4").val()
        + '');
    }
</script>
@endpush
