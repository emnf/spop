@extends('layouts.app')

@section('title')
    Kepoin || Verifikasi Lapangan
@endsection
@push('styles')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css"
        integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="" />
@endpush
@section('content')
    <section class="content" layout:fragment="content">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <section>
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Verifikasi Lapangan</li>
                            </ol>
                        </section>
                    </div>
                    <div class="col-12">
                        <div class="info-box callout callout-danger" id="background2">
                            <div class="col-sm-6">
                                <div class="" style="float: left;">
                                    <h5 class="">FORM VERIFIKASI LAPANGAN</h5>
                                    <span class="float">Periksalah dengan teliti dan benar.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <form class="form-validation" method="post" action="/verifikasi-lapangan/save" id="formverifikasi">
                    @csrf
                    @include('verifikasi-lapangan.form')
                </form>
            </div>
        </section>

        <div class="modal fade" id="modal-image">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h6 class="modal-title" id="modal-image-title">&nbsp;FILE PERSYARATAN</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center" id="imagePlaceholder"></div>
                    <div class="modal-footer justify-content-between"></div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-delete">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;HAPUS DATA ?</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Apakah anda yakin ingin menghapus data sebagai berikut?</p>
                        <div class="row">
                            <div class="col-sm-4">Dokumen Pendukung</div>
                            <div class="col-sm-8">
                                <input type="hidden" id="delete-idUploadDokumenPendukung">
                                <input type="hidden" id="delete-element-id">
                                <textarea type="text" class="form-control form-control-sm" id="delete-namaDokumenPendukung" readonly></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span
                                class="fas fa-times-circle"></span> Close
                        </button>
                        <button type="button" class="btn btn-danger btn-sm" disabled id="btnHapusDokumenPendukung"><span
                                class="fas fa-trash"></span> Hapus
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    {{-- <script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyq1l-IUYHhDWoWwF4HqTTVw-ixal-JzQ&callback=initMap&libraries=&v=weekly"
    async defer></script>
<script>
    function initMap() {
        var latitude    = $("#latitude").val()*1;
        var longitude   = $("#longitude").val()*1;

        // Create a map object and specify the DOM element for display.
        if (latitude == '' && longitude=='') {
            var latlng = {lat: -8.065143501839863, lng: 111.90310819998538};
        } else {
            var latlng = {lat: latitude, lng: longitude};
        }

        var map = new google.maps.Map(document.getElementById('map'), {
            center: latlng,
            scrollwheel: false,
            zoom: 17,
        });

        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            draggable: true,
            title : "Lokasi Objek Pajak"
        });

        google.maps.event.addListener(marker, 'dragend', function(a) {
            $("#longitude").val(a.latLng.lng().toFixed(4));
            $("#latitude").val(a.latLng.lat().toFixed(4));
        });
        google.maps.event.trigger(map, "resize");
    }

    function initMap2() {
        var latitude    = $("#latitude").val()*1;
        var longitude   = $("#longitude").val()*1;

        // Create a map object and specify the DOM element for display.
        if(latitude == '' && longitude=='') {
            var latlng = {lat: 4.306674, lng: 136.0884759};
        } else {
            var latlng = {lat: latitude, lng: longitude};
        }

        var map = new google.maps.Map(document.getElementById('map'), {
            center: latlng,
            scrollwheel: false,
            zoom: 17,
        });

        google.maps.event.trigger(map, "resize");
        var infoWindow = new google.maps.InfoWindow;

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                $("#latitude").val(position.coords.latitude.toFixed(4));
                $("#longitude").val(position.coords.longitude.toFixed(4));
                infoWindow.setPosition(pos);
                infoWindow.setContent('Lokasi Saya.');
                infoWindow.open(map);
                map.setCenter(pos);
            }, function(position) {
                handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
        }
    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
            'Error: The Geolocation service failed.' :
            'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
    }
</script> --}}
    <script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js"
        integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>
    <script>
        const map = new L.map('map').setView([-6.847947200071742, 107.92380809783937], 13);

        const tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        }).addTo(map);

        const marker = L.marker([-6.847947200071742, 107.92380809783937], {
            draggable: true
        }).addTo(map);

        marker.on('dragend', function(e) {
            document.getElementById('latitude').value = marker.getLatLng().lat;
            document.getElementById('longitude').value = marker.getLatLng().lng;
        });
    </script>

    <script>
        var no;
        var idpelayanan = $("#idPelayanan").val();

        $("#btnReject").click(function() {
            $("#idStatusVerifikasiLapangan").val(3);
        })

        $("#btnApprove").click(function() {
            $("#idStatusVerifikasiLapangan").val(2);
        })

        function poplateFileUpload() {
            $("#dataupload").html("");
            no = 1;
            $.get('/pelayanan/upload-file/' + idpelayanan).then(function(data) {
                var row = "";
                data.forEach(r => {
                    $wajib = (r.s_is_optional) ? '' : '<span class="text-danger">*</span>';
                    row = "<tr>" +
                        "<td style=\"text-align: center;\">" + no + "</td>" +
                        "<td>" + r.t_nama_persyaratan + ' ' + $wajib + "</td>" +
                        "<td>" +
                        "<a type=\"button\" class=\"text-primary\" link=\"/" + r.t_lokasi_file +
                        "\" name=\"" + r.t_nama_persyaratan + "\" onclick=\"showImage(this)\">" + r
                        .t_nama_persyaratan + "</a>" +
                        "</td>" +
                        "</tr>";
                    $("#dataupload").append(row);
                    no++;
                });
            });
        }

        function showImage(img) {
            var link = $(img).attr("link");
            var name = $(img).attr("name");
            $("#modal-image-title").text(name);
            var image = $('<img style="max-height:100%; max-width:100%;">');
            image.attr("src", link);
            $("#imagePlaceholder").html(image);
            $("#modal-image").modal('show');
            return false;
        }

        poplateFileUpload();

        $(function() {
            // updateUploadedFiles($("#idPelayanan").val());
            // updateUploadedDokumenPendukung();
        });

        function updateUploadedFiles(idPelayanan) {
            $.get("/upload-file/" + idPelayanan).then(function(data) {
                $("#uploadedFiles").html('');
                var no = 1;
                data.forEach(file => {
                    var tr = '<tr>' +
                        '<td style="text-align: center">' + no + '</td>' +
                        '<td>' + file.namaPersyaratan + '</td>' +
                        '<td><a href="javascript:void(0);" link="' + file.lokasiFile + '" name="' + file
                        .namaPersyaratan + '" onclick="showImage(this)">' + file.namaFile + '</a></td>' +
                        '</tr>';
                    $("#uploadedFiles").append(tr);
                    no++;
                })

            });
        }

        function updateUploadedDokumenPendukung() {
            var idPelayanan = $("#idPelayanan").val();
            $.get("/upload-dokumen-pendukung/" + idPelayanan).then(function(data) {
                data.forEach(file => {
                    var id = findIdDokumenPendukung(file.idDokumenPendukung);
                    if (id > 0) {
                        $("#idUploadDokumenPendukung" + id).val(file.idUploadDokumenPendukung);
                        $("#noDokumen" + id).val(file.nomorDokumen);
                        $("#noDokumen" + id).attr("readonly", "readonly");
                        $("#tglDokumen" + id).val(file.tanggalDokumen);
                        $("#keteranganDokumen" + id).text(file.keterangan);
                        $("#keteranganDokumen" + id).attr("readonly", "readonly");
                        $("#file" + id).addClass('d-none');
                        $("#btnDownload" + id).attr("href", file.lokasiFile);
                        $("#btnDownload" + id).removeClass('d-none');
                        $("#btnDeleteDokumenPendukung" + id).removeClass('d-none');
                        $("#btnSimpan" + id).addClass('d-none');
                    }
                })
            });
        }

        function findIdDokumenPendukung(idDokumenPendukung) {
            var id = 0;
            $(".id-dokumen-pendukung").each(function() {
                if ($(this).val() == idDokumenPendukung) {
                    id = $(this).data("id");
                }
            })
            return id;
        }

        $('.btnSimpan').each(function() {
            var id = $(this).data("id")
            $(this).click(function() {
                doUpload(id);
            })
        })

        function doUpload(id) {
            var data = new FormData();
            data.append("idUploadDokumenPendukung", $("#idUploadDokumenPendukung" + id).val());
            data.append("idDokumenPendukung", $("#idDokumenPendukung" + id).val());
            data.append("noDokumen", $("#noDokumen" + id).val());
            data.append("tglDokumen", $("#tglDokumen" + id).val());
            data.append("keterangan", $("#keteranganDokumen" + id).val());
            data.append("idPelayanan", $("#idPelayanan").val());

            var files = $("#file" + id)[0].files;
            $.each(files, function(i, file) {
                data.append("file", file);
            });

            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/upload-dokumen-pendukung",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function(data) {
                    console.log("SUCCESS : ", data);
                    updateUploadedDokumenPendukung();
                },
                error: function(e) {
                    console.log("ERROR : ", e);
                }
            });
        }

        $(".files").each(function() {
            $(this).change(function() {
                var id = $(this).data("id");
                var btnSimpan = $("#btnSimpan" + id);
                enableBtnSimpan(btnSimpan);
            })
        })

        function enableBtnSimpan(btn) {
            btn.removeClass("disabled");
            btn.attr("aria-disabled", "false");
        }

        $(".btnDeleteDokumenPendukung").each(function() {
            $(this).click(function() {
                var id = $(this).data("id");
                showDeleteDialog(id);
            });
        })

        function showDeleteDialog(id) {
            $("#modal-delete").modal('show');
            var idUpload = $("#idUploadDokumenPendukung" + id).val();
            $.get("/upload-dokumen-pendukung/by-id-upload/" + idUpload).then(function(data) {
                $("#btnHapusDokumenPendukung").removeAttr("disabled");
                $("#delete-idUploadDokumenPendukung").val(idUpload);
                $("#delete-element-id").val(id);
                $("#delete-namaDokumenPendukung").text(data.dokumenPendukung.namaDokumenPendukung);
            });
        }

        $("#btnHapusDokumenPendukung").click(function() {
            $.post('/upload-dokumen-pendukung/delete', {
                idUploadDokumenPendukung: $("#delete-idUploadDokumenPendukung").val()
            }).then(function() {
                var id = $("#delete-element-id").val();
                $("#idUploadDokumenPendukung" + id).val('');
                $("#noDokumen" + id).val('');
                $("#noDokumen" + id).removeAttr("readonly");
                $("#tglDokumen" + id).val('');
                $("#keteranganDokumen" + id).val('');
                $("#keteranganDokumen" + id).removeAttr("readonly");
                $("#file" + id).val('');
                $("#file" + id).removeClass('d-none');
                $("#btnSimpan" + id).addClass('disabled');
                $("#btnSimpan" + id).attr("aria-disabled", "true");
                $("#btnSimpan" + id).removeClass('d-none');
                $("#btnDownload" + id).addClass('d-none');
                $("#btnDeleteDokumenPendukung" + id).addClass('d-none');
                updateUploadedDokumenPendukung();
                $("#modal-delete").modal('hide');
            })
        })

        function panggilData() {
            $("#luasTanahSHM").val($("#luasTanah").val());
            $("#atasNamaSHM").val($("#namaWP").val());
        }
        panggilData();

        $("#luasTanah").on("change", panggilData);
        $("#namaWP").on("change", panggilData);
    </script>
@endpush
