<!DOCTYPE html>
<html lang="en" layout:decorate="~{layout/main}" xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout"
    xmlns:th="http://www.thymeleaf.org">

<head>
    <meta charset="UTF-8">
    <title>Kepoin || Verifikasi Pengajuan</title>
</head>

<body>
    <section class="content" layout:fragment="content">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <section>
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Verifikasi</li>
                            </ol>
                        </section>
                    </div>
                    <div class="col-12">
                        <div class="info-box callout callout-danger" id="background2"
                            style="background: url('/static/images/bgpelayanan.gif') #fff no-repeat center center;
                            -moz-background-size: cover;
                            -o-background-size: cover;
                            background-size: cover;">
                            <div class="col-sm-6">
                                <div class="" style="float: left;">
                                    <h5 class="">FORM VERIFIKASI PELAYANAN</h5>
                                    <span class="float">Periksalah dengan teliti dan benar.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <form class="form-validation" method="post" action="#" th:action="@{/verifikasi/save}"
                    id="formverifikasi">
                    <div class="row">
                        <div class="col-12">
                            <div class="card" style="border-top: 5px solid #bd2130;">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="invoice p-3 mb-3">
                                                <div class="row invoice-info">
                                                    <div class="col-sm-6 invoice-col border-right"
                                                        th:insert="pelayanan/common/data-pemohon :: data"></div>
                                                    <div class="col-sm-6 invoice-col">
                                                        <h5>Data Pelayanan</h5>
                                                        <hr>
                                                        <div
                                                            data-th-replace="pelayanan/common/data-pelayanan/__${pelayanan.jenisPelayanan.alias}__ :: data">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <th:block th:switch="${pelayanan.jenisPajak.idKategoriPajak}">
                            <th:block th:case="'2'">
                                <div
                                    data-th-replace="pelayanan/common/data-objek/__${pelayanan.jenisPelayanan.alias}__ :: data">
                                </div>
                            </th:block>
                        </th:block>
                    </div>
                    <div th:if="${#lists.isEmpty(databangunan)}">

                    </div>
                    <div th:unless="${#lists.isEmpty(databangunan)}">
                        <div th:if="${pelayanan.idJenisPelayanan==4}" class="row">
                            <div class="col-12">
                                <div class="invoice p-3 mb-3" style="border-top:3px solid #dc3545;">
                                    <div class="row invoice-info">
                                        <div class="col-sm-6 invoice-col border-right">
                                            <h5>DATA BANGUNAN</h5>
                                            <hr>
                                            <address th:each="v, p : ${databangunan}">
                                                <div class="row">
                                                    <div class="col-5" th:text="'Luas Bangunan Ke-'+${p.index+1}">
                                                    </div>
                                                    <div class="col-1">:</div>
                                                    <div class="col-6"><b th:text="${v.luasBangunan}"></b>
                                                        (M<sup>2</sup>)</div>
                                                </div>
                                            </address>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div th:unless="${pelayanan.idJenisPelayanan==4}" class="row">

                            <div class="col-12">
                                <div class="invoice p-3 mb-3" style="border-top:3px solid #dc3545;">
                                    <div class="row invoice-info">

                                        <div class="col-sm-6 invoice-col border-right">
                                            <h5>DATA BANGUNAN</h5>
                                            <hr>
                                            <address>
                                                <div class="row">
                                                    <div class="col-5">Jeni Penggunaan Bangunan</div>
                                                    <div class="col-1">:</div>
                                                    <div class="col-6"><b
                                                            th:text="${databangunan.jenisPenggunaanBangunan}"></b></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-5">Tahun Bibangunan</div>
                                                    <div class="col-1">:</div>
                                                    <div class="col-6"><b th:text="${databangunan.tahunBanguanan}"></b>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-5">Tahun Direnovasi</div>
                                                    <div class="col-1">:</div>
                                                    <div class="col-6"><b th:text="${databangunan.tahunRenovasi}"></b>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-5">Kondisi Pada Umumnya</div>
                                                    <div class="col-1">:</div>
                                                    <div class="col-6"><b
                                                            th:text="${databangunan.kondisiBangunan}"></b></div>
                                                    <div class="col-5">Konstruksi</div>
                                                    <div class="col-1">:</div>
                                                    <div class="col-6"><b th:text="${databangunan.Konstruksi}"></b>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-5">Atap</div>
                                                    <div class="col-1">:</div>
                                                    <div class="col-6"><b th:text="${databangunan.atap}"></b></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-5">Dinding</div>
                                                    <div class="col-1">:</div>
                                                    <div class="col-6"><b th:text="${databangunan.dinding}"></b>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-5">Lantai</div>
                                                    <div class="col-1">:</div>
                                                    <div class="col-6"><b th:text="${databangunan.lantai}"></b></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-5">LAngit-Langit</div>
                                                    <div class="col-1">:</div>
                                                    <div class="col-6"><b th:text="${databangunan.langitLangit}"></b>
                                                    </div>
                                                </div>

                                            </address>
                                        </div>
                                        <div class="col-sm-6 invoice-col border-right">
                                            <h5>FASILITAS</h5>
                                            <hr>
                                            <address>
                                                <div class="row">
                                                    <div class="col-5">AC Split</div>
                                                    <div class="col-1">:</div>
                                                    <div class="col-6"><b th:text="${databangunan.acSplit}"></b>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-5">AC Window</div>
                                                    <div class="col-1">:</div>
                                                    <div class="col-6"><b th:text="${databangunan.acWindow}"></b>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-5">Panjang Pagar (M)</div>
                                                    <div class="col-1">:</div>
                                                    <div class="col-6"><b th:text="${databangunan.panjangPagar}"></b>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-5">Bahan Pagar</div>
                                                    <div class="col-1">:</div>
                                                    <div class="col-6"><b th:text="${databangunan.bahanPagar}"></b>
                                                    </div>

                                                </div>
                                            </address>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="card" style="border-top: 5px solid #bd2130;">
                            <div class="card-header">
                                <b>Persyaratan Permohonan</b>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="width: 4rem;">No</th>
                                            <th>Nama Persyaratan</th>
                                            <th>File Persyaratan</th>
                                            <th>Persyaratan Sesuai</th>
                                        </tr>
                                    </thead>
                                    <tbody id="dataupload">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="card" style="border-top: 5px solid #bd2130;">
                            <div class="card-body">
                                <div class="row">
                                    <label class="col-sm-12">PERSETUJUAN VERIFIKASI</label>
                                </div>
                                <hr />
                                <div class="row">
                                    <div class="col-12" style="padding-bottom: 20px;">
                                        Berdasarkan informasi yang sudah diajukan diatas. Apakah verifikator menyetujui
                                        dengan permohonan tersebut?
                                        Apabila sudah, maka permohonan ini tidak akan bisa dilakukan perubahan.
                                        Apabila belum, maka bisa diberikan keterangan atau alasan kenapa permohonan ini
                                        dipending, ditolak, atau diajukan validator.
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="keteranganVerifikasi">Keterangan</label>
                                            <input type="hidden" name="idVerifikasi" id="idVerifikasi"
                                                th:value="${verifikasi.idVerifikasi}">
                                            <input type="hidden" name="idPelayanan" id="idPelayanan"
                                                th:value="${pelayanan.idPelayanan}">
                                            <input type="hidden" name="noVerifikasi" id="noVerifikasi"
                                                th:value="${verifikasi.noVerifikasi}">
                                            <input type="hidden" name="tglVerifikasi" id="tglVerifikasi"
                                                th:value="${#dates.format(#dates.createNow(), 'dd-MM-yyyy')}">
                                            <input type="hidden" name="idStatusVerifikasi" id="idStatusVerifikasi"
                                                th:value="${verifikasi.idStatusVerifikasi}">
                                            <textarea class="form-control" name="keteranganVerifikasi" id="keteranganVerifikasi"
                                                th:text="${verifikasi.keteranganVerifikasi}"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="col-12 d-flex justify-content-between">
                                    <button type="button" class="btn btn-danger" id="btnReject" data-toggle="modal"
                                        data-target="#modal-cancel">
                                        <i class="fas fa-times-circle"></i> Tolak Permohonan
                                    </button>
                                    <div class="modal fade" id="modal-cancel">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header bg-danger">
                                                    <h6 class="modal-title"><span class="fas fa-exclamation"></span>
                                                        &nbsp;TOLAK PENGAJUAN PERMOHONAN ?</h6>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Apakah anda yakin ingin menolak pengajuan data permohonan
                                                        tersebut?</p>
                                                </div>
                                                <div class="modal-footer justify-content-between">
                                                    <button type="button" class="btn btn-default btn-sm"
                                                        data-dismiss="modal"><span class="fas fa-times-circle"></span>
                                                        &nbsp;Close
                                                    </button>
                                                    <button type="submit" class="btn btn-danger btn-sm"><span
                                                            class="fas fa-exclamation"></span> &nbsp;IYA</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-primary float-right" id="btnApprove"
                                        style="margin-right: 5px;" data-toggle="modal" data-target="#modal-save">
                                        <i class="fas fa-check-circle"></i> Proses Permohonan</button>
                                    <div class="modal fade" id="modal-save">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header bg-primary">
                                                    <h6 class="modal-title"><span class="fas fa-send"></span>
                                                        &nbsp;Proses PERMOHONAN PELAYANAN</h6>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Apakah anda ingin memproses layanan ini?</p>
                                                </div>
                                                <div class="modal-footer justify-content-between">
                                                    <button type="button" class="btn btn-default btn-sm"
                                                        data-dismiss="modal"><span class="fas fa-times-circle"></span>
                                                        &nbsp;Close
                                                    </button>
                                                    <button type="submit" class="btn btn-primary btn-sm"><span
                                                            class="fas fa-check"></span> &nbsp;IYA
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </form>
            </div>
        </section>
        <div class="modal fade" id="modal-image">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h6 class="modal-title" id="modal-image-title">&nbsp;FILE PERSYARATAN</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center" id="imagePlaceholder"></div>
                    <div class="modal-footer justify-content-between"></div>
                </div>
            </div>
        </div>
    </section>
    <div layout:fragment="js">
        <script th:inline="javascript">
            var no;

            var idpelayanan = $("#idPelayanan").val();

            $("#btnReject").click(function() {
                $("#idStatusVerifikasi").val(3);
            });

            $("#btnApprove").click(function() {
                $("#idStatusVerifikasi").val(2);
            });

            function poplateFileUpload() {
                $("#dataupload").html("");
                no = 1;
                $.get('/upload-file/' + idpelayanan).then(function(data) {
                    data.forEach(r => {
                        row = "<tr>" +
                            "<td style=\"text-align: center;\">" + no + "</td>" +
                            "<td>" + r.namaPersyaratan + "</td>" +
                            "<td>" +
                            "<a href=\"#\" link=\"" + r.lokasiFile + "\" name=\"" + r.namaPersyaratan +
                            "\" onclick=\"showImage(this)\">" + r.namaPersyaratan + "</a>" +
                            "</td>" +
                            "<td style=\"text-align: center;\">" +
                            "<input type=\"checkbox\" id=\"checklistPersyaratan" + r.idUploadPersyaratan +
                            "\" name=\"checklistPersyaratan\" value=\"" + r.idUploadPersyaratan + "\">" +
                            "</td>" +
                            "</tr>";
                        $("#dataupload").append(row);
                        no++;
                    });
                    checkedUpload();

                });
            }

            function checkedUpload() {
                $.get('/verifikasi/checked-upload/' + idpelayanan).then(function(data) {
                    $("input[name='checklistPersyaratan']:checkbox").val(data.data).checked;
                });
            }

            function showImage(img) {
                var link = $(img).attr("link");
                var name = $(img).attr("name");
                $("#modal-image-title").text(name);
                var image = $('<img style="max-height:100%; max-width:100%;">');
                image.attr("src", link);
                $("#imagePlaceholder").html(image);
                $("#modal-image").modal('show');
                return false;
            }

            poplateFileUpload();
        </script>
        <th:block th:switch="${pelayanan.jenisPajak.idKategoriPajak}">
            <th:block th:case="'2'">
                <script data-th-replace="pelayanan/common/data-objek/__${pelayanan.jenisPelayanan.alias}__ :: js"></script>
            </th:block>
        </th:block>
    </div>
</body>

</html>
