<div class="row">
    @include('components.data-view.data-user')
    <div class="col-12">
        <div class="card" style="border-top: 5px solid #bd2130;">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="invoice p-3 mb-3">
                            <div class="row invoice-info">
                                <div class="col-sm-6 invoice-col border-right">
                                    <h5>Data Pemohon</h5>
                                    <hr>
                                    @include('pelayanan.pbb.components.data-pemohon')
                                </div>
                                <div class="col-sm-6 invoice-col">
                                    <h5>Data Pelayanan</h5>
                                    <hr>
                                    @include('pelayanan.pbb.components.data-view.data-pelayanan')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-12">
        <div class="card" style="border-top:5px solid #dc3545;">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="invoice p-3 mb-3">
                            <div class="row invoice-info">
                                <div class="col-sm-6 invoice-col border-right">
                                    @include('pelayanan.pbb.components.data-view.objek-pajak')
                                </div>
                                <div class="col-sm-6 invoice-col">
                                    @include('pelayanan.pbb.components.data-view.wajib-pajak')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if($pelayanan->jenisPelayanan->s_id_jenis_pelayanan == 1)
        @include('verifikasi-lapangan.partials.detail-op-baru')
    @endif

    @if($pelayanan->jenisPelayanan->s_id_jenis_pelayanan == 4)
        @include('verifikasi-lapangan.partials.pembetulan-sppt')
    @endif

    @if($pelayanan->jenisPelayanan->s_id_jenis_pelayanan == 2)
        @include('verifikasi-lapangan.partials.mutasi-subjek')
    @endif

    @if($pelayanan->jenisPelayanan->s_id_jenis_pelayanan == 3)
        @include('verifikasi-lapangan.partials.mutasi-pecah')
    @endif

    <div class="col-sm-12">
        {{-- @if(in_array($jenisLayanan->s_id_jenis_pelayanan, [1]) && ($pelayanan->detailBangunan->count() > 0)) --}}
        @if(in_array($jenisLayanan->s_id_jenis_pelayanan, [1]))
            @include('verifikasi-lapangan.partials.bangunan')
        @endif
    </div>

    @if($pelayanan->jenisPelayanan->s_id_jenis_pelayanan != 8)
    <div class="col-sm-12">
        <div class="card" style="border-top: 5px solid #bd2130;">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12"><b> LETAK OBJEK</b></div>
                    <div class="col-md-12">
                        <hr style="height:2px;color:red">
                    </div>
                    <div class="col-md-12 form-horizontal">
                        <div class="form-group-sm row">
                            <label class="col-sm-2 col-form-label-sm">Latitude <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control form-control-sm " id="latitude" name="latitude"
                                    value="{{ $objekPajak->t_latitude }}">
                            </div>
                            <div class="colsm-1"></div>
                            <label class="col-sm-2 col-form-label-sm">
                                Longitude <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control form-control-sm " id="longitude" name="longitude"
                                    required value="{{ $objekPajak->t_longitude }}">
                            </div>
                        </div>
                    </div>
                    <div class=" col-md-12">
                        {{-- <center>
                            <button type="button" onclick="initMap()" class="btn btn-primary">Lokasi
                                Pilih</button>
                            <button type="button" onclick="initMap2()" class="btn btn-primary">Lokasi
                                Saya</button>
                        </center> --}}
                    </div>
                    <div class="col-md-12">
                        <center>
                            <div align="center" id="map" style="width: 100%; height: 400px">
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<div class="col-12">
    <div class="card" style="border-top: 5px solid #bd2130;">
        <div class="card-header">
            <b>Persyaratan Permohonan</b>
        </div>
        <div class="card-body">
            <div class="row table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 4rem;">No</th>
                            <th>Nama Persyaratan</th>
                            <th>File Persyaratan</th>
                        </tr>
                    </thead>
                    <tbody id="dataupload">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="col-12">
    <div class="card" style="border-top: 5px solid #bd2130;">
        <div class="card-body">
            <div class="row">
                <label class="col-sm-12">PERSETUJUAN VERIFIKASI LAPANGAN</label>
            </div>
            <hr />
            <div class="row">
                <div class="col-12" style="padding-bottom: 20px;">
                    Data diatas sudah menampilkan data permohonan & data pelayanan, periksa data
                    tersebut dengan baik & teliti.
                    <br> Silahkan tulis keterangan pada hasil verifikasi lapangan dibawah ini :
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label>No. Berita Acara Verifikasi Lapangan<span class="text-danger">*</span></label>
                        <input type="text" class="form-control col-md-4" name="noBAHP" required>
                    </div>

                    @if($pelayanan->jenisPelayanan->s_id_jenis_pelayanan == 1)
                        @include('components.data-objek.persetujuan-verlap')
                    @endif

                    <div class="form-group" hidden>
                        <label>Tempat Penelitian</label>
                        <textarea class="form-control" name="tempatPenelitian">Lapangan</textarea>
                    </div>

                    <div class="form-group">
                        <label for="keteranganVerifikasiLapangan">Keterangan<span class="text-danger">*</span> :</label>
                        <input type="hidden" name="idVerifikasiLapangan" id="idVerifikasiLapangan">
                        <input type="hidden" name="idPelayanan" id="idPelayanan"
                            value="{{ $pelayanan->t_id_pelayanan }}">
                        <input type="hidden" name="tglVerifikasiLapangan" id="tglVerifikasiLapangan" value="{{ date('d-m-Y') }}">
                        <input type="hidden" name="noVerifikasiLapangan" id="noVerifikasiLapangan" value="">
                        <input type="hidden" name="idStatusVerifikasiLapangan" id="idStatusVerifikasiLapangan">

                        <textarea class="form-control" name="keteranganVerifikasiLapangan"
                            id="keteranganVerifikasiLapangan" required></textarea>
                    </div>

                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="col-12 d-flex justify-content-between">
                <button type="button" class="btn btn-danger" onclick="location.href='/verifikasi-lapangan'">
                    <i class="fas fa-arrow-circle-left"></i> Kembali
                </button>
                {{-- <button type="button" class="btn btn-danger" id="btnReject" data-toggle="modal"
                    data-target="#modal-cancel">
                    <i class="fas fa-times-circle"></i> Tolak Verifikasi Lapangan
                </button> --}}
                {{-- <div class="modal fade" id="modal-cancel">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bg-danger">
                                <h6 class="modal-title"><span class="fas fa-exclamation"></span>
                                    &nbsp;TOLAK PENGAJUAN PERMOHONAN ?</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Apakah anda yakin ingin menolak pengajuan verifikasi lapangan ?
                                </p>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span
                                        class="fas fa-times-circle"></span>
                                    &nbsp;Tutup
                                </button>
                                <button type="submit" class="btn btn-danger btn-sm"><span
                                        class="fas fa-exclamation"></span> &nbsp;Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <button type="button" class="btn btn-primary float-right" id="btnApprove" style="margin-right: 5px;"
                    data-toggle="modal" data-target="#modal-save">
                    <i class="fas fa-check-circle"></i> Simpan Verifikasi Lapangan
                </button>
                <div class="modal fade" id="modal-save">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bg-primary">
                                <h6 class="modal-title"><span class="fas fa-send"></span> &nbsp;PROSES
                                    VERIFIKASI LAPANGAN</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Apakah data ini sudah diperiksa & teliti dengan baik ?</p>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span
                                        class="fas fa-times-circle"></span>
                                    &nbsp;Tutup
                                </button>
                                <button type="submit" class="btn btn-primary btn-sm"><span class="fas fa-check"></span>
                                    &nbsp;Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
