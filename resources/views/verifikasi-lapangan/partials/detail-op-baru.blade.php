<div class="col-12">
    <div class="card" style="border-top: 5px solid #bd2130;">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <p style="font-weight: bold">DATA OBJEK PAJAK (HASIL PENELITIAN)</p>
                    <hr>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">NOP Terdekat</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm masked-nop" id="nopTerdekat"
                                name="nopTerdekat" value="" required>
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm"></label>
                        <div class="col-sm-8">
                            <b style="color: red;">*) Isi NOP terdekat untuk bisa memilih kode blok
                                dan kode znt</b>
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">RT</label>
                        <div class="col-sm-2">
                            <input type="hidden" id="idOP" name="idOP"
                                value="{{ $pelayanan->objekPajak->t_id_op }}">
                            <input type="text" class="form-control form-control-sm masked-number-3" id="rtOP"
                                name="rtOP" value="{{ $objekPajak->t_rt_op }}" maxlength="">
                        </div>
                        <label class="col-sm-2 col-form-label-sm">RW</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control form-control-sm masked-number-2" id="rwOP"
                                name="rwOP" value="{{ $objekPajak->t_rw_op }}">
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Jalan</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="jalanOP" name="jalanOP"
                                value="{{ $objekPajak->t_jalan_op }}">
                        </div>
                    </div>

                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Kecamatan</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="kodeKecamatan" name="kodeKecamatan"
                                onchange="kelurahanCombo(this.value)">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($kecamatan as $item)
                                    <option {{ $objekPajak->kd_kecamatan == $item->kd_kecamatan ? 'selected' : '' }}
                                        value="{{ $item->kd_kecamatan }}">
                                        {{ $item->kd_kecamatan . ' - ' . $item->nm_kecamatan }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="kodeKelurahan" name="kodeKelurahan">
                                <option value="">Silahkan Pilih</option>
                            </select>
                        </div>
                    </div>
                    @if (isset($lookupItem2))
                        <div class="form-group-sm row">
                            <label class="col-sm-4 col-form-label-sm">Keterangan Objek Pajak <span
                                    class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select class="form-control" name="kodeLookUpItem" id="kodeLookUpItem">
                                    <option value="">Silahkan Pilih</option>
                                    @foreach ($lookupItem2 as $item)
                                        <option
                                            {{ (int) $objekPajak->t_kode_lookup_item == (int) $item->kd_lookup_item ? 'selected' : '' }}
                                            value="{{ $item->kd_lookup_item }}">{{ $item->nm_lookup_item }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Kode Blok</label>
                        <div class="col-sm-2">
                            <select class="form-control" id="kodeBlok" name="kodeBlok" readonly required>
                            </select>
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Kode ZNT</label>
                        <div class="col-sm-2">
                            <select class="form-control" id="kodeZNT" name="kodeZNT" onchange="cariKelasTanah()"
                                required>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group-sm row">
                                <label class="col-sm-8 col-form-label-sm">Kelas Tanah</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control text-center" id="kelasTanah"
                                        name="kelasTanah" value="" required readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group-sm row">
                                <label class="col-sm-4 col-form-label-sm">NJOP/m<sup>2</sup></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control text-right" id="njopPermeter"
                                        name="njopPermeter" value="" required readonly>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Jenis Tanah <span
                                class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <select class="form-control" name="jenisTanah" id="jenisTanah">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($lookupItem1 as $item)
                                    <option
                                        {{ (int) $objekPajak->t_jenis_tanah == (int) $item->kd_lookup_item ? 'selected' : '' }}
                                        value="{{ $item->kd_lookup_item }}">{{ $item->nm_lookup_item }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Luas Tanah (M<sup>2</sup>) <span
                                class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="luasTanah"
                                name="luasTanah" required value="{{ $objekPajak->t_luas_tanah }}"
                                onchange="cariKelasTanah()">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <p style="font-weight: bold">DATA WAJIB PAJAK (HASIL PENELITIAN)</p>
                    <hr>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">NIK <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="hidden" id="idWP" name="idWP" value="{{ $wajibPajak->t_id_wp }}">
                            <input type="text" class="form-control form-control-sm masked-nik" id="nikWP"
                                name="nikWP" required value="{{ $wajibPajak->t_nik_wp }}">
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Nama <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="namaWP" name="namaWP"
                                required value="{{ $wajibPajak->t_nama_wp }}">
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">RT</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control form-control-sm masked-number-3" id="rtWP"
                                name="rtWP" value="{{ $wajibPajak->t_rt_wp }}" maxlength="3">
                        </div>
                        <label class="col-sm-2 col-form-label-sm">RW</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control form-control-sm masked-number-3" id="rwWP"
                                name="rwWP" value="{{ $wajibPajak->t_rw_wp }}" maxlength="2">
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Jalan</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="jalanWP" name="jalanWP"
                                value="{{ $wajibPajak->t_jalan_wp }}">
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="kelurahanWP"
                                name="kelurahanWP" value="{{ $wajibPajak->t_kelurahan_wp }}">
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Kecamatan</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="kecamatanWP"
                                name="kecamatanWP" value="{{ $wajibPajak->t_kecamatan_wp }}">
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Kabupaten/Kota</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="kabupatenWP"
                                name="kabupatenWP" value="{{ $wajibPajak->t_kabupaten_wp }}">
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">No. Telp<span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" required class="form-control form-control-sm" id="noHpWP"
                                name="noHpWP" value="{{ $wajibPajak->t_no_hp_wp }}" maxlength="15">
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">NPWPD</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm masked-npwpd" id="npwpd"
                                name="npwpd" value="{{ $wajibPajak->t_npwpd }}">
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Email</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="email" name="email"
                                value="{{ $wajibPajak->t_email }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $(document).ready(() => {
            if ($("#kodeKecamatan").val() != '') {
                kelurahanCombo($("#kodeKecamatan").val());
            }
        });

        const kelurahanCombo = function(idKec) {
            $.get('/kelurahan/getkelurahan-bykecamatan/' + idKec).then(function(res) {
                let comboKelurahan = document.querySelector("#kodeKelurahan");
                let kelurahaList = '<option value="">Silahkan Pilih</option>';
                let kd_kelurahan = '{{ $pelayanan->objekPajak->kd_kelurahan ?? '0' }}'

                if (res.success) {
                    res.data.map((kel, key) => {
                        kelurahaList +=
                            `<option value="${kel.kd_kelurahan}" ${kd_kelurahan == kel.kd_kelurahan ? 'selected' : ''} >${kel.kd_kelurahan + ' - ' + kel.nm_kelurahan}</option>`;
                    })
                }
                comboKelurahan.innerHTML = kelurahaList

                $("#nopTerdekat").val('{{ substr_replace($kdKabkota, '.', 2, 0) }}' + '.' + $("#kodeKecamatan")
                    .val() + '.' + $("#kodeKelurahan").val())
            })
        }

        $("#nopTerdekat").blur(function() {
            const nop = $("#nopTerdekat").val();
            let nopArr = nop.split('.');

            if (nopArr[2] != $("#kodeKecamatan").val()) {
                alert("NOP Terdekat bukan termasuk Kecamatan lokasi objek");
                return;
            }

            if (nopArr[3] != $("#kodeKelurahan").val()) {
                alert("NOP Terdekat bukan termasuk Kelurahan lokasi objek");
                return
            }

            if (nop.length == 24) {
                $.get('/verifikasi-lapangan/cari-data-znt/' + nop)
                    .then(function(response) {
                        $("#kodeBlok").find('option').remove();
                        $("#kodeZNT").html("");
                        if (!response.success) {
                            alert('NOP tidak ditemukan');
                            return;
                        }

                        if (response.success) {
                            console.log(response.data.kodeBlok);
                            response.data.kodeBlokList.forEach((blok) => {
                                let selected = (blok == response.data.kodeBlok) ?
                                    ' selected="selected" ' : '';
                                let disbaleOption = blok == nopArr[4] ? '' : 'disabled';
                                $("#kodeBlok").append(
                                    `<option ${selected} value="${blok}" ${disbaleOption}> ${blok} </option>`
                                );
                            })
                            console.log(response.data.kodeZNT);
                            response.data.kodeZNTList.forEach((znt) => {
                                let selected = (znt == response.data.kodeZNT) ?
                                    ' selected="selected" ' : '';
                                $("#kodeZNT").append(
                                    `<option ${selected} value="${znt}">${znt}</option>`);
                            })

                            cariKelasTanah();
                        }
                    });
            }
        })

        function cariKelasTanah() {
            let nop = $("#nopTerdekat").val();
            let kdZnt = $("#kodeZNT").val();
            let luasTanah = $("#luasTanah").val();

            $("#kelasTanah").val('');
            $("#njopPermeter").val('');

            $.ajax(`/verifikasi-lapangan/cari-kelas-tanah/${nop}/${kdZnt}/${luasTanah}`)
                .then(res => {
                    if (res.success) {
                        $("#kelasTanah").val(res.data.kdKelas)
                        $("#njopPermeter").val(formatMoney(res.data.njopPermeter))
                    }
                })
        }
    </script>
@endpush
