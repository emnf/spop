<div class="col-12">
    <div class="card" style="border-top: 5px solid #bd2130;">
        <div class="card-body">

            @foreach ($pelayanan->objekPajaks as $key => $objekPajak)
                <div class="row">
                    <div class="col-md-6">
                        <hr style="background-color: #28a745; height: 2px;">
                        <p style="font-weight: bold">DATA OBJEK PAJAK (BARU) No. {{ $key + 1 }}</p>
                        <hr>
                        <div class="form-group-sm row div-opbaru">
                            <label class="col-sm-4 col-form-label-sm">RT</label>
                            <div class="col-sm-2">
                                <input type="hidden" id="idOP{{ $key }}" name="idOP[]"
                                    value="{{ $objekPajak->t_id_op ?? '' }}">
                                <input type="hidden" id="nop{{ $key }}" name="nop[]"
                                    value="{{ $objekPajak->nop ?? '' }}" required>
                                <input type="hidden" name="idxObjek[]" value="{{ $objekPajak->t_id_op }}" />
                                <input type="text" class="form-control form-control-sm" id="rtOP{{ $key }}"
                                    name="rtOP[]" value="{{ $objekPajak->t_rt_op ?? '' }}"
                                    onkeypress="return onlyNumberKey(event)" readonly="">
                            </div>
                            <label class="col-sm-2 col-form-label-sm">RW</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control form-control-sm" id="rwOP{{ $key }}"
                                    name="rwOP[]" value="{{ $objekPajak->t_rw_op ?? '' }}"
                                    onkeypress="return onlyNumberKey(event)" readonly="">
                            </div>
                        </div>

                        <div class="form-group-sm row">
                            <label class="col-sm-4 col-form-label-sm">Jalan</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm"
                                    id="jalanOP{{ $key }}" name="jalanOP[]"
                                    value="{{ $objekPajak->t_jalan_op ?? '' }}" readonly="">
                            </div>
                        </div>

                        <div class="form-group-sm row">
                            <label class="col-sm-4 col-form-label-sm">Blok/No/Kav</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm"
                                    id="kdBlokKav{{ $key }}" name="kdBlokKav[]"
                                    value="{{ $objekPajak->kd_blok_kav ?? '' }}" readonly="">
                            </div>
                        </div>

                        <input type="hidden" id="no_urut{{ $key }}" name="no_urut[]"
                            value="{{ $objekPajak->no_urut ?? '' }}">
                        <input type="hidden" id="kd_jns_op{{ $key }}" name="kd_jns_op[]"
                            value="{{ $objekPajak->kd_jns_op ?? '' }}">

                        <div class="form-group-sm row">
                            <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm"
                                    id="t_kelurahan_op{{ $key }}" name="t_kelurahan_op[]"
                                    value="{{ $objekPajak->t_kelurahan_op ?? '' }}" readonly="">
                            </div>
                        </div>
                        <div class="form-group-sm row">
                            <label class="col-sm-4 col-form-label-sm">Kecamatan</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm"
                                    id="t_kecamatan_op{{ $key }}" name="t_kecamatan_op[]"
                                    value="{{ $objekPajak->t_kecamatan_op ?? '' }}" readonly="">
                            </div>
                        </div>

                        <div class="form-group-sm row">
                            <label class="col-sm-4 col-form-label-sm">Jenis Tanah <span
                                    class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select class="form-control form-control-sm" name="jenisTanah[]"
                                    id="jenisTanah{{ $key }}"
                                    onchange="tampilFormBangunan(event, {{ $objekPajak->t_id_op }})">
                                    <option value="">Silahkan Pilih</option>
                                    @foreach ($lookupItem1 as $item)
                                        <option value="{{ $item->kd_lookup_item }}"
                                            {{ (int) $objekPajak->t_jenis_tanah == (int) $item->kd_lookup_item ? 'selected' : '' }}>
                                            {{ $item->nm_lookup_item }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group-sm row">
                            <label class="col-sm-4 col-form-label-sm">Luas Tanah
                                (M<sup>2</sup>)</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm"
                                    id="luasTanah{{ $key }}" name="luasTanah[]"
                                    value="{{ $objekPajak->t_luas_tanah ?? '' }}"
                                    onkeypress="return onlyNumberKey(event)">
                            </div>
                        </div>

                        <div class="form-group-sm row">
                            <label class="col-sm-4 col-form-label-sm">Luas Bangunan
                                (M<sup>2</sup>)</label>
                            <div class="col-sm-8">
                                <input type="text"
                                    class="form-control form-control-sm luas-banguanan-op-{{ $objekPajak->t_id_op }}"
                                    id="luasBangunan{{ $key }}" name="luasBangunan[]"
                                    value="{{ $objekPajak->t_luas_bangunan ?? '' }}" readonly="">
                            </div>
                        </div>
                        <div class="form-group-sm row">
                            <label class="col-sm-4 col-form-label-sm">KODE ZNT <span
                                    class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select class="form-control form-control-sm" name="t_kode_znt[]"
                                    id="t_kode_znt{{ $key }}" required>
                                    <option value="">Silahkan Pilih</option>
                                    @foreach ($znt as $v)
                                        <option value="{{ $v->kd_znt }}">
                                            {{ $v->kd_znt }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-6">
                        <hr style="background-color: #28a745; height: 2px;">
                        <p style="font-weight: bold">DATA WAJIB PAJAK (BARU)</p>
                        <hr>
                        <div class="form-group-sm row">
                            <label class="col-sm-4 col-form-label-sm">NIK <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="hidden" id="idWP{{ $key }}" name="idWP[]"
                                    value="{{ $pelayanan->wajibPajaks[$key]->t_id_wp ?? '' }}">
                                <input type="text" class="form-control form-control-sm masked-nik"
                                    id="nikWP{{ $key }}" name="nikWP[]" maxlength="16"
                                    value="{{ $pelayanan->wajibPajaks[$key]->t_nik_wp ?? '' }}" required>
                            </div>
                        </div>

                        <div class="form-group-sm row">
                            <label class="col-sm-4 col-form-label-sm">Nama <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm"
                                    id="namaWP{{ $key }}" name="namaWP[]"
                                    value="{{ $pelayanan->wajibPajaks[$key]->t_nama_wp ?? '' }}" required>
                            </div>
                        </div>

                        <div class="form-group-sm row">
                            <label class="col-sm-4 col-form-label-sm">RT <span class="text-danger">*</span></label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control form-control-sm"
                                    id="rtWP{{ $key }}" name="rtWP[]"
                                    value="{{ $pelayanan->wajibPajaks[$key]->t_rt_wp ?? '' }}"
                                    onkeypress="return onlyNumberKey(event)" required>
                            </div>
                            <label class="col-sm-2 col-form-label-sm">RW <span class="text-danger">*</span></label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control form-control-sm"
                                    id="rwWP{{ $key }}" name="rwWP[]"
                                    value="{{ $pelayanan->wajibPajaks[$key]->t_rw_wp ?? '' }}"
                                    onkeypress="return onlyNumberKey(event)" required>
                            </div>
                        </div>

                        <div class="form-group-sm row">
                            <label class="col-sm-4 col-form-label-sm">Jalan <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm"
                                    id="jalanWP{{ $key }}" name="jalanWP[]"
                                    value="{{ $pelayanan->wajibPajaks[$key]->t_jalan_wp ?? '' }}" required>
                            </div>
                        </div>
                        <div class="form-group-sm row">
                            <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa <span
                                    class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm"
                                    id="kelurahanWP{{ $key }}" name="kelurahanWP[]"
                                    value="{{ $pelayanan->wajibPajaks[$key]->t_kelurahan_wp ?? '' }}" required>
                            </div>
                        </div>
                        <div class="form-group-sm row">
                            <label class="col-sm-4 col-form-label-sm">Kecamatan <span
                                    class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm"
                                    id="kecamatanWP{{ $key }}" name="kecamatanWP[]"
                                    value="{{ $pelayanan->wajibPajaks[$key]->t_kecamatan_wp ?? '' }}" required>
                            </div>
                        </div>
                        <div class="form-group-sm row">
                            <label class="col-sm-4 col-form-label-sm">Kabupaten/Kota <span
                                    class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm"
                                    id="kabupatenWP{{ $key }}" name="kabupatenWP[]"
                                    value="{{ $pelayanan->wajibPajaks[$key]->t_kabupaten_wp ?? '' }}" required>
                            </div>
                        </div>
                        <div class="form-group-sm row">
                            <label class="col-sm-4 col-form-label-sm">No. Telp<span
                                    class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" required class="form-control form-control-sm masked-hp"
                                    id="noHpWP{{ $key }}" name="noHpWP[]" maxlength="15"
                                    value="{{ old('t_no_hp_wp') ?? ($pelayanan->wajibPajaks[$key]->t_no_hp_wp ?? '') }}">
                            </div>
                        </div>
                        <div class="form-group-sm row">
                            <label class="col-sm-4 col-form-label-sm">Email</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm"
                                    id="email{{ $key }}" name="email[]"
                                    value="{{ old('t_email') ?? ($pelayanan->wajibPajaks[$key]->t_email ?? '') }}">
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</div>

@if ($pelayanan->detailBangunan->count() > 0)
    @foreach ($pelayanan->objekPajaks as $key => $objekPajak)
        <div class="col-12">
            <div class="card card-danger card-outline @if ($objekPajak->detailBangunan->count() <= 0) d-none @endif"
                id="formDetailBangunan{{ $objekPajak->t_id_op }}" style="border-top: 5px solid #bd2130;">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <div class="card-title">
                        <h5>DATA BANGUNAN OBJEK PAJAK {{ $key + 1 }}</h5>
                    </div>
                    <button type="button" class="btn btn-primary btn-sm ml-auto"
                        onclick="tambahBanguan({{ $objekPajak->t_id_op }})">Tambah</button>
                    <input type="hidden" name="idxBangunan" id="idxBangunan{{ $objekPajak->t_id_op }}"
                        value="{{ $objekPajak->detailBangunan->count() > 0 ? $objekPajak->detailBangunan->count() : '0' }}">
                </div>
                <div class="card-body" id="objek{{ $objekPajak->t_id_op }}">

                    @foreach ($objekPajak->detailBangunan as $i => $bangunan)
                        <div class="col-md-12"
                            id="divDetailBangunan{{ $bangunan->t_no_urut_bangunan }}_{{ $objekPajak->t_id_op }}">
                            <div class="card collapsed-card">
                                <div class="card-header">
                                    <h4 class="card-title">{{ 'Bangunan No. ' . $bangunan->t_no_urut_bangunan }}</h4>
                                    <div class="card-tools">
                                        <input type="hidden" name="nomorBangunan[{{ $i }}]"
                                            id="nomorBangunan" value="{{ $bangunan->t_no_urut_bangunan ?? '' }}">
                                        <button type="button" class="btn btn-danger btn-sm"
                                            onclick="hapusDetailBangunan('{{ $bangunan->t_no_urut_bangunan }}_{{ $objekPajak->t_id_op }}')">Hapus</button>
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                class="fas fa-plus"></i></button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p style="font-weight: bold">RINCIAN DATA BANGUNAN</p>
                                            <hr>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-5 col-form-label-sm">Jenis Penggunaan
                                                    Bangunan</label>
                                                <div class="col-sm-4 ">
                                                    <input type="hidden" name="idDetailBangunan[]"
                                                        value="{{ $bangunan->t_id_detail_bangunan }}">
                                                    <input type="hidden" name="t_id_op[]"
                                                        value="{{ $bangunan->t_id_detail_bangunan }}">
                                                    <input type="hidden" name="nomorBangunan[]"
                                                        value="{{ $bangunan->t_no_urut_bangunan }}">
                                                    <input type="hidden" name="idBangunanObjek[]"
                                                        value="{{ $objekPajak->t_id_op }}">
                                                    <select class="form-control form-control-sm"
                                                        name="jenisPenggunaanBangunan[]">
                                                        <option value=""> ---Silahkan Pilih---</option>
                                                        <option
                                                            {{ $bangunan->t_jenis_penggunaan_bangunan == 'Perumahan' ? 'selected' : '' }}
                                                            value="Perumahan"> Perumahan</option>
                                                        <option
                                                            {{ $bangunan->t_jenis_penggunaan_bangunan == 'Toko/Apotik/Pasar/Ruko' ? 'selected' : '' }}
                                                            value="Toko/Apotik/Pasar/Ruko">
                                                            Toko/Apotik/Pasar/Ruko
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-5 col-form-label-sm">Luas Bangunan <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-4">
                                                    <input type="text"
                                                        class="form-control form-control-sm luas-bangunan-{{ $objekPajak->t_id_op }}"
                                                        onchange="countBangunan(event, {{ $objekPajak->t_id_op }})"
                                                        onkeypress="return onlyNumberKey(event)"
                                                        name="luasBangunans[]" value="{{ $bangunan->t_luas }}">
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-5 col-form-label-sm">Jumlah Lantai <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-4">
                                                    <input type="text" maxlength="4"
                                                        class="form-control form-control-sm" name="jumlahLantai[]"
                                                        value="{{ $bangunan->t_jumlah_lantai }}">
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-5 col-form-label-sm">Tahun Dibangun <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-4">
                                                    <input type="text" maxlength="4"
                                                        class="form-control form-control-sm" name="tahunBangunan[]"
                                                        value="{{ $bangunan->t_tahun_bangunan }}">
                                                </div>

                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-5 col-form-label-sm">Tahun Direnovasi</label>
                                                <div class="col-sm-4">
                                                    <input type="text" maxlength="4"
                                                        class="form-control form-control-sm" name="tahunRenovasi[]"
                                                        value="{{ $bangunan->t_tahun_renovasi }}">
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-5 col-form-label-sm">Kondisi Pada Umumnya <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-4 ">
                                                    <select class="form-control form-control-sm"
                                                        name="kondisiBangunan[]">
                                                        <option value=""> ---Silahkan Pilih---</option>
                                                        <option
                                                            {{ $bangunan->t_kondisi_bangunan == 'SANGAT BAIK' ? 'selected' : '' }}
                                                            value="SANGAT BAIK">Sangat Baik</option>
                                                        <option
                                                            {{ $bangunan->t_kondisi_bangunan == 'BAIK' ? 'selected' : '' }}
                                                            value="BAIK">Baik</option>
                                                        <option
                                                            {{ $bangunan->t_kondisi_bangunan == 'SEDANG' ? 'selected' : '' }}
                                                            value="SEDANG">Sedang</option>
                                                        <option
                                                            {{ $bangunan->t_kondisi_bangunan == 'JELEK' ? 'selected' : '' }}
                                                            value="JELEK">Jelek</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-5 col-form-label-sm">Konstruksi <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-4 ">
                                                    <select class="form-control form-control-sm" name="konstruksi[]">
                                                        <option value=""> ---Silahkan Pilih---</option>
                                                        <option
                                                            {{ $bangunan->t_konstruksi == 'BAJA' ? 'selected' : '' }}
                                                            value="BAJA">Baja</option>
                                                        <option
                                                            {{ $bangunan->t_konstruksi == 'BETON' ? 'selected' : '' }}
                                                            value="BETON">Beton</option>
                                                        <option
                                                            {{ $bangunan->t_konstruksi == 'BATU BATA' ? 'selected' : '' }}
                                                            value="BATU BATA">Batu Bata</option>
                                                        <option
                                                            {{ $bangunan->t_konstruksi == 'KAYU' ? 'selected' : '' }}
                                                            value="KAYU">Kayu</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-5 col-form-label-sm">Atap <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-4 ">
                                                    <select class="form-control form-control-sm" name="atap[]">
                                                        <option value=""> ---Silahkan Pilih---</option>
                                                        <option
                                                            {{ $bangunan->t_atap == 'DECRABON/BETON/GTG GLAZUR' ? 'selected' : '' }}
                                                            value="DECRABON/BETON/GTG GLAZUR"> Decrabon/Beton Gtg
                                                            Glazur</option>
                                                        <option
                                                            {{ $bangunan->t_atap == 'GTG BETON/ALUMUNIUM' ? 'selected' : '' }}
                                                            value="GTG BETON/ALUMUNIUM"> Gtg Beton/Alumunium</option>
                                                        <option
                                                            {{ $bangunan->t_atap == 'GTG BIASA/SIRAP' ? 'selected' : '' }}
                                                            value="GTG BIASA/SIRAP"> Gtg Biasa/Sirap</option>
                                                        <option {{ $bangunan->t_atap == 'ASBES' ? 'selected' : '' }}
                                                            value="ASBES">
                                                            Asbes</option>
                                                        <option {{ $bangunan->t_atap == 'SENG' ? 'selected' : '' }}
                                                            value="SENG">
                                                            Seng
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-5 col-form-label-sm">Dinding <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-4 ">
                                                    <select class="form-control form-control-sm" name="dinding[]"
                                                        required>
                                                        <option value=""> ---Silahkan Pilih---</option>
                                                        <option
                                                            {{ $bangunan->t_dinding == 'KACA/ALUMUNIUM' ? 'selected' : '' }}
                                                            value="KACA/ALUMUNIUM"> Kaca/Alumunium</option>
                                                        <option {{ $bangunan->t_dinding == 'BETON' ? 'selected' : '' }}
                                                            value="BETON"> Beton</option>
                                                        <option
                                                            {{ $bangunan->t_dinding == 'BATU BATA/CONBLOK' ? 'selected' : '' }}
                                                            value="BATU BATA/CONBLOK"> Batu Bata/Conblok</option>
                                                        <option {{ $bangunan->t_dinding == 'KAYU' ? 'selected' : '' }}
                                                            value="KAYU"> Kayu</option>
                                                        <option {{ $bangunan->t_dinding == 'SENG' ? 'selected' : '' }}
                                                            value="SENG"> Seng</option>
                                                        <option
                                                            {{ $bangunan->t_dinding == 'TIDAK' ? 'selected' : '' }}
                                                            value="TIDAK ADA"> Tidak Ada</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group-sm row">
                                                <label class="col-sm-5 col-form-label-sm">Lantai <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-4 ">
                                                    <select class="form-control form-control-sm" name="lantai[]">
                                                        <option value=""> ---Silahkan Pilih---</option>
                                                        <option
                                                            {{ $bangunan->t_lantai == 'MARMER' ? 'selected' : '' }}
                                                            value="MARMER">
                                                            Marmer</option>
                                                        <option
                                                            {{ $bangunan->t_lantai == 'KERAMIK' ? 'selected' : '' }}
                                                            value="KERAMIK"> Keramik</option>
                                                        <option
                                                            {{ $bangunan->t_lantai == 'TERASO' ? 'selected' : '' }}
                                                            value="TERASO">
                                                            Teraso</option>
                                                        <option
                                                            {{ $bangunan->t_lantai == 'UBIN PC/PAPAN' ? 'selected' : '' }}
                                                            value="UBIN
                                                            PC/PAPAN">
                                                            Ubin PC/Papan</option>
                                                        <option {{ $bangunan->t_lantai == 'SEMEN' ? 'selected' : '' }}
                                                            value="SEMEN">
                                                            Semen</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-5 col-form-label-sm">Langit-Langit<span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-4 ">
                                                    <select class="form-control form-control-sm"
                                                        name="langitLangit[]">
                                                        <option value=""> ---Silahkan Pilih---</option>
                                                        <option
                                                            {{ $bangunan->t_langit_langit == 'AKUSTIK/JATI' ? 'selected' : '' }}
                                                            value="AKUSTIK/JATI"> Akustik/Jati</option>
                                                        <option
                                                            {{ $bangunan->t_langit_langit == 'TRIPLEK/ASBES BAMBU' ? 'selected' : '' }}
                                                            value="TRIPLEK/ASBES BAMBU"> Triplek/Asbes/Bambu</option>
                                                        <option
                                                            {{ $bangunan->t_langit_langit == 'TIDAK ADA' ? 'selected' : '' }}
                                                            value="TIDAK ADA"> Tidak Ada</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <p style="font-weight: bold">FASILITAS</p>
                                            <hr>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-5 col-form-label-sm">Jumlah AC SPlit</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control form-control-sm"
                                                        name="acSplit[]'}" onkeypress="return onlyNumberKey(event)"
                                                        value="{{ $bangunan->t_ac_split }}">
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-5 col-form-label-sm">Jumlah AC Window</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control form-control-sm"
                                                        id="acWindow" onkeypress="return onlyNumberKey(event)"
                                                        name="acWindow[]" value="{{ $bangunan->t_ac_window }}">
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-5 col-form-label-sm">Panjang Pagar (M)</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control form-control-sm"
                                                        name="panjangPagar[]" onkeypress="return onlyNumberKey(event)"
                                                        value="{{ $bangunan->t_panjang_pagar }}">
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-5 col-form-label-sm">Bahan Pagar</label>
                                                <div class="col-sm-3">
                                                    <select class="form-control form-control-sm" name="bahanPagar[]">
                                                        <option value=""> ---Silahkan Pilih---</option>
                                                        <option
                                                            {{ $bangunan->t_bahan_pagar == 'BAJA/BESI' ? 'selected' : '' }}
                                                            value="BAJA/BESI"> Baja/Besi</option>
                                                        <option
                                                            {{ $bangunan->t_bahan_pagar == 'BATA/BATAKO' ? 'selected' : '' }}
                                                            value="BATA/BATAKO"> Bata/Batako</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-5 col-form-label-sm">Daya Listrik(VA)</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control form-control-sm"
                                                        id="dayaListrik" onkeypress="return onlyNumberKey(event)"
                                                        name="dayaListrik[]"
                                                        value="{{ $bangunan->t_listrik ?? '' }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endforeach
@endif

@push('scripts')
    <script>
        // $('#jenisTanah').change(function() {
        // if ($(this).val()==1) {
        //     $('#formDetailBangunan').removeClass('d-none')
        // } else {
        //     $('#formDetailBangunan .card-body').html('')
        //     $('#formDetailBangunan').addClass('d-none')
        //     $('#idxBangunan').val(1)
        // }
        // })

        function tampilFormBangunan(e, a) {
            if (e.target.value == 1) {
                $('#formDetailBangunan' + a).removeClass('d-none')
            } else {
                $('#formDetailBangunan' + a).addClass('d-none')
                $('#formDetailBangunan' + a + ' .card-body').html('')
                $('#idxBangunan' + a).val(1)
            }
        }

        function tambahBanguan(a) {
            let no = parseInt($('#idxBangunan' + a).val());

            let html = `<div class="col-md-12" id="divDetailBangunan${no}_${a}">
            <div class="card collapsed-card">
                <div class="card-header">
                    <h4 class="card-title">Bangunan No. ${no+1} </h4>
                    <div class="card-tools">
                        <button type="button" class="btn btn-danger btn-sm" onclick="hapusDetailBangunan('${no}_${a}')">Hapus</button>
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-plus"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p style="font-weight: bold">RINCIAN DATA BANGUNAN</p>
                            <hr>
                            <div class="form-group-sm row">
                                <label class="col-sm-5 col-form-label-sm">Jenis Penggunaan
                                    Bangunan</label>
                                <div class="col-sm-4 ">
                                    <input type="hidden" name="idDetailBangunan[]">
                                    <input type="hidden" name="idBangunanObjek[]" value="${a}">
                                    <input type="hidden" name="nomorBangunan[]" value="${no}">
                                    <select class="form-control form-control-sm"
                                        name="jenisPenggunaanBangunan[]">
                                        <option value=""> ---Silahkan Pilih---</option>
                                        <option value="Perumahan"> Perumahan</option>
                                        <option value="Toko/Apotik/Pasar/Ruko"> Toko/Apotik/Pasar/Ruko
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-5 col-form-label-sm">Luas Bangunan <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control form-control-sm luas-bangunan-${a}"
                                        onkeypress="return onlyNumberKey(event)" onchange="countBangunan(event, ${a})" name="luasBangunans[]">
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-5 col-form-label-sm">Jumlah Lantai <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-4">
                                    <input type="text" maxlength="4" class="form-control form-control-sm"
                                        onkeypress="return onlyNumberKey(event)" name="jumlahLantai[]">
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-5 col-form-label-sm">Tahun Dibangun <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-4">
                                    <input type="text" maxlength="4" class="form-control form-control-sm"
                                        onkeypress="return onlyNumberKey(event)" name="tahunBangunan[]">
                                </div>

                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-5 col-form-label-sm">Tahun Direnovasi</label>
                                <div class="col-sm-4">
                                    <input type="text" maxlength="4" class="form-control form-control-sm"
                                        onkeypress="return onlyNumberKey(event)" name="tahunRenovasi[]">
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-5 col-form-label-sm">Kondisi Pada Umumnya <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-4 ">
                                    <select class="form-control form-control-sm" name="kondisiBangunan[]">
                                        <option value=""> ---Silahkan Pilih---</option>
                                        <option value="SANGAT BAIK">Sangat Baik</option>
                                        <option value="BAIK">Baik</option>
                                        <option value="SEDANG">Sedang</option>
                                        <option value="JELEK">Jelek</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-5 col-form-label-sm">Konstruksi <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-4 ">
                                    <select class="form-control form-control-sm" name="konstruksi[]">
                                        <option value=""> ---Silahkan Pilih---</option>
                                        <option value="BAJA">Baja</option>
                                        <option value="BETON">Beton</option>
                                        <option value="BATU BATA">Batu Bata</option>
                                        <option value="KAYU">Kayu</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-5 col-form-label-sm">Atap <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-4 ">
                                    <select class="form-control form-control-sm" name="atap[]">
                                        <option value=""> ---Silahkan Pilih---</option>
                                        <option value="DECRABON/BETON/GTG GLAZUR"> Decrabon/Beton Gtg Glazur
                                        </option>
                                        <option value="GTG BETON/ALUMUNIUM"> Gtg Beton/Alumunium</option>
                                        <option value="GTG BIASA/SIRAP"> Gtg Biasa/Sirap</option>
                                        <option value="ASBES">
                                            Asbes</option>
                                        <option value="SENG"> Seng
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-5 col-form-label-sm">Dinding <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-4 ">
                                    <select class="form-control form-control-sm" name="dinding[]" required>
                                        <option value=""> ---Silahkan Pilih---</option>
                                        <option value="KACA/ALUMUNIUM"> Kaca/Alumunium</option>
                                        <option value="BETON"> Beton</option>
                                        <option value="BATU BATA/CONBLOK"> Batu Bata/Conblok</option>
                                        <option value="KAYU"> Kayu</option>
                                        <option value="SENG"> Seng</option>
                                        <option value="TIDAK ADA"> Tidak Ada</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group-sm row">
                                <label class="col-sm-5 col-form-label-sm">Lantai <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-4 ">
                                    <select class="form-control form-control-sm" name="lantai[]">
                                        <option value=""> ---Silahkan Pilih---</option>
                                        <option value="MARMER">
                                            Marmer</option>
                                        <option value="KERAMIK"> Keramik</option>
                                        <option value="TERASO">
                                            Teraso</option>
                                        <option value="UBIN
                                        PC/PAPAN"> Ubin PC/Papan</option>
                                        <option value="SEMEN">
                                            Semen</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-5 col-form-label-sm">Langit-Langit<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-4 ">
                                    <select class="form-control form-control-sm" name="langitLangit[]">
                                        <option value=""> ---Silahkan Pilih---</option>
                                        <option value="AKUSTIK/JATI"> Akustik/Jati</option>
                                        <option value="TRIPLEK/ASBES BAMBU"> Triplek/Asbes/Bambu</option>
                                        <option value="TIDAK ADA"> Tidak Ada</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <p style="font-weight: bold">FASILITAS</p>
                            <hr>
                            <div class="form-group-sm row">
                                <label class="col-sm-4 col-form-label-sm">Jumlah AC SPlit</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control form-control-sm" name="acSplit[]'}"
                                        onkeypress="return onlyNumberKey(event)" value="{{ '' }}">
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-4 col-form-label-sm">Jumlah AC Window</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control form-control-sm" id="acWindow"
                                        onkeypress="return onlyNumberKey(event)" name="acWindow[]"
                                        value="{{ '' }}">
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-4 col-form-label-sm">Panjang Pagar (M)</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control form-control-sm"
                                        name="panjangPagar[]" onkeypress="return onlyNumberKey(event)"
                                        value="{{ '' }}">
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-4 col-form-label-sm">Bahan Pagar</label>
                                <div class="col-sm-4">
                                    <select class="form-control form-control-sm" name="bahanPagar[]">
                                        <option value=""> ---Silahkan Pilih---</option>
                                        <option value="BAJA/BESI"> Baja/Besi</option>
                                        <option value="BATA/BATAKO"> Bata/Batako</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-4 col-form-label-sm">Daya Listrik(VA)</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control form-control-sm" id="dayaListrik"
                                        onkeypress="return onlyNumberKey(event)" name="dayaListrik[]"
                                        value="{{ '' ?? '' }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>`;

            $('#objek' + a).append(html)
            no = no + 1;
            $('#idxBangunan' + a).val(no)
        }

        function hapusDetailBangunan(a) {
            $('#divDetailBangunan' + a).remove();
            let idx = a.split('_');
            console.log($('#idxBangunan' + idx[1]).val() - 1);
            $('#idxBangunan' + idx[1]).val($('#idxBangunan' + idx[1]).val() - 1)
        }

        function countBangunan(a, b) {
            let luasBng = document.querySelectorAll(".luas-bangunan-" + b);
            console.log(luasBng);
            let totLuasBng = 0;
            for (let i = 0; i < luasBng.length; i++) {
                console.log(luasBng[i].value);
                totLuasBng = parseInt(totLuasBng) + parseInt(luasBng[i].value);
                console.log(totLuasBng);
            }

            $(".luas-banguanan-op-" + b).val(totLuasBng);
        }
    </script>
@endpush
