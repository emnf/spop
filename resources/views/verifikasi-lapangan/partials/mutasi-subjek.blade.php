<div class="col-12">
    <div class="card" style="border-top: 5px solid #bd2130;">
        <div class="card-body">
            <div class="row">

                <div class="col-md-6">
                    <hr style="background-color: #28a745; height: 2px;">
                    <p style="font-weight: bold">DATA OBJEK PAJAK (BARU)</p>
                    <hr>
                    <div class="form-group-sm row div-opbaru">
                        <label class="col-sm-4 col-form-label-sm">RT</label>
                        <div class="col-sm-2">
                            <input type="hidden" id="idOP" name="idOP"
                                value="{{ $pelayanan->objekPajak->t_id_op ?? '' }}">
                            <input type="hidden" id="nop" name="nop"
                                value="{{ $pelayanan->objekPajak->nop ?? '' }}" required>
                            <input type="text" class="form-control form-control-sm" id="rtOP" name="rtOP"
                                value="{{ $pelayanan->objekPajak->t_rt_op ?? '' }}"
                                onkeypress="return onlyNumberKey(event)" readonly="">
                        </div>
                        <label class="col-sm-2 col-form-label-sm">RW</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control form-control-sm" id="rwOP" name="rwOP"
                                value="{{ $pelayanan->objekPajak->t_rw_op ?? '' }}"
                                onkeypress="return onlyNumberKey(event)" readonly="">
                        </div>
                    </div>

                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Jalan</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="jalanOP" name="jalanOP"
                                value="{{ $pelayanan->objekPajak->t_jalan_op ?? '' }}" readonly="">
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Blok/No/Kav</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="kdBlokKav" name="kdBlokKav"
                                value="{{ $pelayanan->objekPajak->kd_blok_kav ?? '' }}" readonly="">
                        </div>
                    </div>
                    <input type="hidden" id="no_urut" name="no_urut"
                        value="{{ $pelayanan->objekPajak->no_urut ?? '' }}">
                    <input type="hidden" id="kd_jns_op" name="kd_jns_op"
                        value="{{ $pelayanan->objekPajak->kd_jns_op ?? '' }}">
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="t_kelurahan_op"
                                name="t_kelurahan_op" value="{{ $pelayanan->objekPajak->t_kelurahan_op ?? '' }}"
                                readonly="">
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Kecamatan</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="t_kecamatan_op"
                                name="t_kecamatan_op" value="{{ $pelayanan->objekPajak->t_kecamatan_op ?? '' }}"
                                readonly="">
                        </div>
                    </div>
                    {{-- @dump($objekPajak) --}}
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Jenis Tanah <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <select class="form-control form-control-sm" name="jenisTanah" id="jenisTanah">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($lookupItem1 as $item)
                                    <option value="{{ $item->kd_lookup_item }}"
                                        {{ (int) $objekPajak->t_jenis_tanah == (int) $item->kd_lookup_item ? 'selected' : '' }}>
                                        {{ $item->nm_lookup_item }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Luas Tanah
                            (M<sup>2</sup>)</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="luasTanah" name="luasTanah"
                                value="{{ $pelayanan->objekPajak->t_luas_tanah ?? '' }}"
                                onkeypress="return onlyNumberKey(event)">
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Luas Bangunan
                            (M<sup>2</sup>)</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="luasBangunan"
                                name="luasBangunan" value="{{ $pelayanan->objekPajak->t_luas_bangunan ?? '' }}"
                                readonly="">
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <hr style="background-color: #28a745; height: 2px;">
                    <p style="font-weight: bold">DATA WAJIB PAJAK (BARU)</p>
                    <hr>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">NIK <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="hidden" id="idWP" name="idWP"
                                value="{{ $pelayanan->wajibPajak->t_id_wp ?? '' }}">
                            <input type="text" class="form-control form-control-sm masked-nik" id="nikWP"
                                name="nikWP" maxlength="16" value="{{ $pelayanan->wajibPajak->t_nik_wp ?? '' }}"
                                required>
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Nama <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="namaWP" name="namaWP"
                                value="{{ $pelayanan->wajibPajak->t_nama_wp ?? '' }}" required>
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">RT <span class="text-danger">*</span></label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control form-control-sm" id="rtWP" name="rtWP"
                                value="{{ $pelayanan->wajibPajak->t_rt_wp ?? '' }}"
                                onkeypress="return onlyNumberKey(event)" required>
                        </div>
                        <label class="col-sm-2 col-form-label-sm">RW <span class="text-danger">*</span></label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control form-control-sm" id="rwWP" name="rwWP"
                                value="{{ $pelayanan->wajibPajak->t_rw_wp ?? '' }}"
                                onkeypress="return onlyNumberKey(event)" required>
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Jalan <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="jalanWP" name="jalanWP"
                                value="{{ $pelayanan->wajibPajak->t_jalan_wp ?? '' }}" required>
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa <span
                                class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="kelurahanWP"
                                name="kelurahanWP" value="{{ $pelayanan->wajibPajak->t_kelurahan_wp ?? '' }}"
                                required>
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Kecamatan <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="kecamatanWP"
                                name="kecamatanWP" value="{{ $pelayanan->wajibPajak->t_kecamatan_wp ?? '' }}"
                                required>
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Kabupaten/Kota <span
                                class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="kabupatenWP"
                                name="kabupatenWP" value="{{ $pelayanan->wajibPajak->t_kabupaten_wp ?? '' }}"
                                required>
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">No. Telp<span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" required class="form-control form-control-sm masked-hp"
                                id="noHpWP" name="noHpWP" maxlength="15"
                                value="{{ old('t_no_hp_wp') ?? ($pelayanan->wajibPajak->t_no_hp_wp ?? '') }}">
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">NPWPD</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm masked-npwpd" id="npwpd"
                                name="npwpd" value="{{ old('t_npwpd') ?? ($pelayanan->wajibPajak->t_npwpd ?? '') }}">
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Email</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="email" name="email"
                                value="{{ old('t_email') ?? ($pelayanan->wajibPajak->t_email ?? '') }}">
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<div class="col-12">
    <div class="card card-danger card-outline d-none" id="formDetailBangunan" style="border-top: 5px solid #bd2130;">
        <div class="card-header d-flex justify-content-between align-items-center">
            <div class="card-title">
                <h5>DATA BANGUNAN (HASIL PENELITIAN)</h5>
            </div>
            <button type="button" class="btn btn-primary btn-sm ml-auto" onclick="tambahBanguan()">Tambah</button>
            <input type="hidden" name="idxBangunan" id="idxBangunan"
                value="{{ $pelayanan->detailBangunan->count() > 0 ? $pelayanan->detailBangunan->count() : '0' }}">
            @php
                $i = $pelayanan->detailBangunan->count();
            @endphp
        </div>
        <div class="card-body">
            @if ($pelayanan->detailBangunan->count() > 0)
                @foreach ($pelayanan->detailBangunan as $bangunan)
                    <div class="col-md-12">
                        <div class="card collapsed-card">
                            <div class="card-header">
                                <h4 class="card-title">{{ 'Bangunan No. ' . $bangunan->t_no_urut_bangunan }}</h4>
                                <div class="card-tools">
                                    <input type="hidden" name="nomorBangunan" id="nomorBangunan"
                                        value="{{ $bangunan->t_no_urut_bangunan ?? '' }}">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-plus"></i></button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p style="font-weight: bold">RINCIAN DATA BANGUNAN</p>
                                        <hr>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Jenis Penggunaan
                                                Bangunan</label>
                                            <div class="col-sm-4 ">
                                                <input type="hidden" name="idDetailBangunan[{{ $i }}]'}"
                                                    value="{{ $bangunan->t_id_detail_bangunan }}">
                                                <input type="hidden" name="nomorBangunan[{{ $i }}]'}"
                                                    value="{{ $bangunan->t_no_urut_bangunan }}">
                                                <select class="form-control form-control-sm"
                                                    name="jenisPenggunaanBangunan[{{ $i }}]">
                                                    <option value=""> ---Silahkan Pilih---</option>
                                                    <option
                                                        {{ $bangunan->t_jenis_penggunaan_bangunan == 'Perumahan' ? 'selected' : '' }}
                                                        value="Perumahan"> Perumahan</option>
                                                    <option
                                                        {{ $bangunan->t_jenis_penggunaan_bangunan == 'Toko/Apotik/Pasar/Ruko' ? 'selected' : '' }}
                                                        value="Toko/Apotik/Pasar/Ruko">
                                                        Toko/Apotik/Pasar/Ruko
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Luas Bangunan <span
                                                    class="text-danger">*</span></label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control form-control-sm"
                                                    onkeypress="return onlyNumberKey(event)"
                                                    name="luasBangunan[{{ $i }}]"
                                                    value="{{ $bangunan->t_luas }}">
                                            </div>
                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Jumlah Lantai <span
                                                    class="text-danger">*</span></label>
                                            <div class="col-sm-4">
                                                <input type="text" maxlength="4"
                                                    class="form-control form-control-sm"
                                                    name="jumlahLantai[{{ $i }}]"
                                                    value="{{ $bangunan->t_jumlah_lantai }}">
                                            </div>
                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Tahun Dibangun <span
                                                    class="text-danger">*</span></label>
                                            <div class="col-sm-4">
                                                <input type="text" maxlength="4"
                                                    class="form-control form-control-sm"
                                                    name="tahunBangunan[{{ $i }}]"
                                                    value="{{ $bangunan->t_tahun_bangunan }}">
                                            </div>

                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Tahun Direnovasi</label>
                                            <div class="col-sm-4">
                                                <input type="text" maxlength="4"
                                                    class="form-control form-control-sm"
                                                    name="tahunRenovasi[{{ $i }}]"
                                                    value="{{ $bangunan->t_tahun_renovasi }}">
                                            </div>
                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Kondisi Pada Umumnya <span
                                                    class="text-danger">*</span></label>
                                            <div class="col-sm-4 ">
                                                <select class="form-control form-control-sm"
                                                    name="kondisiBangunan[{{ $i }}]">
                                                    <option value=""> ---Silahkan Pilih---</option>
                                                    <option
                                                        {{ $bangunan->t_kondisi_bangunan == 'SANGAT BAIK' ? 'selected' : '' }}
                                                        value="SANGAT BAIK">Sangat Baik</option>
                                                    <option
                                                        {{ $bangunan->t_kondisi_bangunan == 'BAIK' ? 'selected' : '' }}
                                                        value="BAIK">Baik</option>
                                                    <option
                                                        {{ $bangunan->t_kondisi_bangunan == 'SEDANG' ? 'selected' : '' }}
                                                        value="SEDANG">Sedang</option>
                                                    <option
                                                        {{ $bangunan->t_kondisi_bangunan == 'JELEK' ? 'selected' : '' }}
                                                        value="JELEK">Jelek</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Konstruksi <span
                                                    class="text-danger">*</span></label>
                                            <div class="col-sm-4 ">
                                                <select class="form-control form-control-sm"
                                                    name="konstruksi[{{ $i }}]">
                                                    <option value=""> ---Silahkan Pilih---</option>
                                                    <option {{ $bangunan->t_konstruksi == 'BAJA' ? 'selected' : '' }}
                                                        value="BAJA">Baja</option>
                                                    <option {{ $bangunan->t_konstruksi == 'BETON' ? 'selected' : '' }}
                                                        value="BETON">Beton</option>
                                                    <option
                                                        {{ $bangunan->t_konstruksi == 'BATU BATA' ? 'selected' : '' }}
                                                        value="BATU BATA">Batu Bata</option>
                                                    <option {{ $bangunan->t_konstruksi == 'KAYU' ? 'selected' : '' }}
                                                        value="KAYU">Kayu</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Atap <span
                                                    class="text-danger">*</span></label>
                                            <div class="col-sm-4 ">
                                                <select class="form-control form-control-sm"
                                                    name="atap[{{ $i }}]">
                                                    <option value=""> ---Silahkan Pilih---</option>
                                                    <option
                                                        {{ $bangunan->t_atap == 'DECRABON/BETON/GTG GLAZUR' ? 'selected' : '' }}
                                                        value="DECRABON/BETON/GTG GLAZUR"> Decrabon/Beton Gtg Glazur
                                                    </option>
                                                    <option
                                                        {{ $bangunan->t_atap == 'GTG BETON/ALUMUNIUM' ? 'selected' : '' }}
                                                        value="GTG BETON/ALUMUNIUM"> Gtg Beton/Alumunium</option>
                                                    <option
                                                        {{ $bangunan->t_atap == 'GTG BIASA/SIRAP' ? 'selected' : '' }}
                                                        value="GTG BIASA/SIRAP"> Gtg Biasa/Sirap</option>
                                                    <option {{ $bangunan->t_atap == 'ASBES' ? 'selected' : '' }}
                                                        value="ASBES">
                                                        Asbes</option>
                                                    <option {{ $bangunan->t_atap == 'SENG' ? 'selected' : '' }}
                                                        value="SENG">
                                                        Seng
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Dinding <span
                                                    class="text-danger">*</span></label>
                                            <div class="col-sm-4 ">
                                                <select class="form-control form-control-sm"
                                                    name="dinding[{{ $i }}]" required>
                                                    <option value=""> ---Silahkan Pilih---</option>
                                                    <option
                                                        {{ $bangunan->t_dinding == 'KACA/ALUMUNIUM' ? 'selected' : '' }}
                                                        value="KACA/ALUMUNIUM"> Kaca/Alumunium</option>
                                                    <option {{ $bangunan->t_dinding == 'BETON' ? 'selected' : '' }}
                                                        value="BETON"> Beton</option>
                                                    <option
                                                        {{ $bangunan->t_dinding == 'BATU BATA/CONBLOK' ? 'selected' : '' }}
                                                        value="BATU BATA/CONBLOK"> Batu Bata/Conblok</option>
                                                    <option {{ $bangunan->t_dinding == 'KAYU' ? 'selected' : '' }}
                                                        value="KAYU"> Kayu</option>
                                                    <option {{ $bangunan->t_dinding == 'SENG' ? 'selected' : '' }}
                                                        value="SENG"> Seng</option>
                                                    <option {{ $bangunan->t_dinding == 'TIDAK' ? 'selected' : '' }}
                                                        value="TIDAK ADA"> Tidak Ada</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Lantai <span
                                                    class="text-danger">*</span></label>
                                            <div class="col-sm-4 ">
                                                <select class="form-control form-control-sm"
                                                    name="lantai[{{ $i }}]">
                                                    <option value=""> ---Silahkan Pilih---</option>
                                                    <option {{ $bangunan->t_lantai == 'MARMER' ? 'selected' : '' }}
                                                        value="MARMER">
                                                        Marmer</option>
                                                    <option {{ $bangunan->t_lantai == 'KERAMIK' ? 'selected' : '' }}
                                                        value="KERAMIK"> Keramik</option>
                                                    <option {{ $bangunan->t_lantai == 'TERASO' ? 'selected' : '' }}
                                                        value="TERASO">
                                                        Teraso</option>
                                                    <option
                                                        {{ $bangunan->t_lantai == 'UBIN PC/PAPAN' ? 'selected' : '' }}
                                                        value="UBIN
                                                    PC/PAPAN">
                                                        Ubin PC/Papan</option>
                                                    <option {{ $bangunan->t_lantai == 'SEMEN' ? 'selected' : '' }}
                                                        value="SEMEN">
                                                        Semen</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Langit-Langit<span
                                                    class="text-danger">*</span></label>
                                            <div class="col-sm-4 ">
                                                <select class="form-control form-control-sm"
                                                    name="langitLangit[{{ $i }}]">
                                                    <option value=""> ---Silahkan Pilih---</option>
                                                    <option
                                                        {{ $bangunan->t_langit_langit == 'AKUSTIK/JATI' ? 'selected' : '' }}
                                                        value="AKUSTIK/JATI"> Akustik/Jati</option>
                                                    <option
                                                        {{ $bangunan->t_langit_langit == 'TRIPLEK/ASBES BAMBU' ? 'selected' : '' }}
                                                        value="TRIPLEK/ASBES BAMBU"> Triplek/Asbes/Bambu</option>
                                                    <option
                                                        {{ $bangunan->t_langit_langit == 'TIDAK ADA' ? 'selected' : '' }}
                                                        value="TIDAK ADA"> Tidak Ada</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <p style="font-weight: bold">FASILITAS</p>
                                        <hr>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Jumlah AC SPlit</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control form-control-sm"
                                                    name="acSplit[{{ $i }}]'}"
                                                    onkeypress="return onlyNumberKey(event)"
                                                    value="{{ $bangunan->t_ac_split }}">
                                            </div>
                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Jumlah AC Window</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control form-control-sm"
                                                    id="acWindow" onkeypress="return onlyNumberKey(event)"
                                                    name="acWindow[{{ $i }}]"
                                                    value="{{ $bangunan->t_ac_window }}">
                                            </div>
                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Panjang Pagar (M)</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control form-control-sm"
                                                    name="panjangPagar[{{ $i }}]"
                                                    onkeypress="return onlyNumberKey(event)"
                                                    value="{{ $bangunan->t_panjang_pagar }}">
                                            </div>
                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Bahan Pagar</label>
                                            <div class="col-sm-3">
                                                <select class="form-control form-control-sm"
                                                    name="bahanPagar[{{ $i }}]">
                                                    <option value=""> ---Silahkan Pilih---</option>
                                                    <option
                                                        {{ $bangunan->t_bahan_pagar == 'BAJA/BESI' ? 'selected' : '' }}
                                                        value="BAJA/BESI"> Baja/Besi</option>
                                                    <option
                                                        {{ $bangunan->t_bahan_pagar == 'BATA/BATAKO' ? 'selected' : '' }}
                                                        value="BATA/BATAKO"> Bata/Batako</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Daya Listrik(VA)</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control form-control-sm"
                                                    id="dayaListrik" onkeypress="return onlyNumberKey(event)"
                                                    name="dayaListrik[{{ $i }}]"
                                                    value="{{ $bangunan->t_listrik ?? '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $('#jenisTanah').change(function() {
            if ($(this).val() == 1) {
                $('#formDetailBangunan').removeClass('d-none')
            } else {
                $('#formDetailBangunan .card-body').html('')
                $('#formDetailBangunan').addClass('d-none')
                $('#idxBangunan').val(1)
            }
        })

        function tambahBanguan() {
            let no = parseInt($('#idxBangunan').val());

            let html = `<div class="col-md-12" id="divDetailBangunan${no}">
                <div class="card collapsed-card">
                    <div class="card-header">
                        <h4 class="card-title">Bangunan No. ${no+1} </h4>
                        <div class="card-tools">
                            <button type="button" class="btn btn-danger btn-sm" onclick="hapusDetailBangunan(${no})" data-card-widget="collapse">Hapus</button>
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                    class="fas fa-plus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <p style="font-weight: bold">RINCIAN DATA BANGUNAN</p>
                                <hr>
                                <div class="form-group-sm row">
                                    <label class="col-sm-5 col-form-label-sm">Jenis Penggunaan
                                        Bangunan</label>
                                    <div class="col-sm-4 ">
                                        <input type="hidden" name="idDetailBangunan[${no}]">
                                        <input type="hidden" name="nomorBangunan[${no}]" value="${no}">
                                        <select class="form-control form-control-sm"
                                            name="jenisPenggunaanBangunan[${no}]">
                                            <option value=""> ---Silahkan Pilih---</option>
                                            <option value="Perumahan"> Perumahan</option>
                                            <option value="Toko/Apotik/Pasar/Ruko"> Toko/Apotik/Pasar/Ruko
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group-sm row">
                                    <label class="col-sm-5 col-form-label-sm">Luas Bangunan <span
                                            class="text-danger">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control form-control-sm"
                                            onkeypress="return onlyNumberKey(event)" name="luasBangunan[${no}]">
                                    </div>
                                </div>
                                <div class="form-group-sm row">
                                    <label class="col-sm-5 col-form-label-sm">Jumlah Lantai <span
                                            class="text-danger">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="text" maxlength="4" class="form-control form-control-sm"
                                            onkeypress="return onlyNumberKey(event)" name="jumlahLantai[${no}]">
                                    </div>
                                </div>
                                <div class="form-group-sm row">
                                    <label class="col-sm-5 col-form-label-sm">Tahun Dibangun <span
                                            class="text-danger">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="text" maxlength="4" class="form-control form-control-sm"
                                            onkeypress="return onlyNumberKey(event)" name="tahunBangunan[${no}]">
                                    </div>

                                </div>
                                <div class="form-group-sm row">
                                    <label class="col-sm-5 col-form-label-sm">Tahun Direnovasi</label>
                                    <div class="col-sm-4">
                                        <input type="text" maxlength="4" class="form-control form-control-sm"
                                            onkeypress="return onlyNumberKey(event)" name="tahunRenovasi[${no}]" value="{{ '' }}">
                                    </div>
                                </div>
                                <div class="form-group-sm row">
                                    <label class="col-sm-5 col-form-label-sm">Kondisi Pada Umumnya <span
                                            class="text-danger">*</span></label>
                                    <div class="col-sm-4 ">
                                        <select class="form-control form-control-sm" name="kondisiBangunan[${no}]">
                                            <option value=""> ---Silahkan Pilih---</option>
                                            <option value="SANGAT BAIK">Sangat Baik</option>
                                            <option value="BAIK">Baik</option>
                                            <option value="SEDANG">Sedang</option>
                                            <option value="JELEK">Jelek</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group-sm row">
                                    <label class="col-sm-5 col-form-label-sm">Konstruksi <span
                                            class="text-danger">*</span></label>
                                    <div class="col-sm-4 ">
                                        <select class="form-control form-control-sm" name="konstruksi[${no}]">
                                            <option value=""> ---Silahkan Pilih---</option>
                                            <option value="BAJA">Baja</option>
                                            <option value="BETON">Beton</option>
                                            <option value="BATU BATA">Batu Bata</option>
                                            <option value="KAYU">Kayu</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group-sm row">
                                    <label class="col-sm-5 col-form-label-sm">Atap <span
                                            class="text-danger">*</span></label>
                                    <div class="col-sm-4 ">
                                        <select class="form-control form-control-sm" name="atap[${no}]">
                                            <option value=""> ---Silahkan Pilih---</option>
                                            <option value="DECRABON/BETON/GTG GLAZUR"> Decrabon/Beton Gtg Glazur
                                            </option>
                                            <option value="GTG BETON/ALUMUNIUM"> Gtg Beton/Alumunium</option>
                                            <option value="GTG BIASA/SIRAP"> Gtg Biasa/Sirap</option>
                                            <option value="ASBES">
                                                Asbes</option>
                                            <option value="SENG"> Seng
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group-sm row">
                                    <label class="col-sm-5 col-form-label-sm">Dinding <span
                                            class="text-danger">*</span></label>
                                    <div class="col-sm-4 ">
                                        <select class="form-control form-control-sm" name="dinding[${no}]" required>
                                            <option value=""> ---Silahkan Pilih---</option>
                                            <option value="KACA/ALUMUNIUM"> Kaca/Alumunium</option>
                                            <option value="BETON"> Beton</option>
                                            <option value="BATU BATA/CONBLOK"> Batu Bata/Conblok</option>
                                            <option value="KAYU"> Kayu</option>
                                            <option value="SENG"> Seng</option>
                                            <option value="TIDAK ADA"> Tidak Ada</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group-sm row">
                                    <label class="col-sm-5 col-form-label-sm">Lantai <span
                                            class="text-danger">*</span></label>
                                    <div class="col-sm-4 ">
                                        <select class="form-control form-control-sm" name="lantai[${no}]">
                                            <option value=""> ---Silahkan Pilih---</option>
                                            <option value="MARMER">
                                                Marmer</option>
                                            <option value="KERAMIK"> Keramik</option>
                                            <option value="TERASO">
                                                Teraso</option>
                                            <option value="UBIN
                                            PC/PAPAN"> Ubin PC/Papan</option>
                                            <option value="SEMEN">
                                                Semen</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group-sm row">
                                    <label class="col-sm-5 col-form-label-sm">Langit-Langit<span
                                            class="text-danger">*</span></label>
                                    <div class="col-sm-4 ">
                                        <select class="form-control form-control-sm" name="langitLangit[${no}]">
                                            <option value=""> ---Silahkan Pilih---</option>
                                            <option value="AKUSTIK/JATI"> Akustik/Jati</option>
                                            <option value="TRIPLEK/ASBES BAMBU"> Triplek/Asbes/Bambu</option>
                                            <option value="TIDAK ADA"> Tidak Ada</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <p style="font-weight: bold">FASILITAS</p>
                                <hr>
                                <div class="form-group-sm row">
                                    <label class="col-sm-4 col-form-label-sm">Jumlah AC SPlit</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control form-control-sm" name="acSplit[${no}]'}"
                                            onkeypress="return onlyNumberKey(event)" value="{{ '' }}">
                                    </div>
                                </div>
                                <div class="form-group-sm row">
                                    <label class="col-sm-4 col-form-label-sm">Jumlah AC Window</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control form-control-sm" id="acWindow"
                                            onkeypress="return onlyNumberKey(event)" name="acWindow[${no}]"
                                            value="{{ '' }}">
                                    </div>
                                </div>
                                <div class="form-group-sm row">
                                    <label class="col-sm-4 col-form-label-sm">Panjang Pagar (M)</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control form-control-sm"
                                            name="panjangPagar[${no}]" onkeypress="return onlyNumberKey(event)"
                                            value="{{ '' }}">
                                    </div>
                                </div>
                                <div class="form-group-sm row">
                                    <label class="col-sm-4 col-form-label-sm">Bahan Pagar</label>
                                    <div class="col-sm-4">
                                        <select class="form-control form-control-sm" name="bahanPagar[${no}]">
                                            <option value=""> ---Silahkan Pilih---</option>
                                            <option value="BAJA/BESI"> Baja/Besi</option>
                                            <option value="BATA/BATAKO"> Bata/Batako</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group-sm row">
                                    <label class="col-sm-4 col-form-label-sm">Daya Listrik(VA)</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control form-control-sm" id="dayaListrik"
                                            onkeypress="return onlyNumberKey(event)" name="dayaListrik[${no}]"
                                            value="{{ '' ?? '' }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>`;

            $('#formDetailBangunan .card-body').append(html)
            no = no + 1;
            $('#idxBangunan').val(no)
        }

        function hapusDetailBangunan(a) {
            $('#divDetailBangunan' + a).remove();
        }
    </script>
@endpush
