<div class="col-12">
    <div class="card" style="border-top: 5px solid #bd2130;">
        <div class="card-body">
            <div class="row">

                <div class="col-md-6">
                    <hr style="background-color: #28a745; height: 2px;">
                    <p style="font-weight: bold">DATA OBJEK PAJAK (BARU)</p>
                    <hr>
                    <div class="form-group-sm row div-opbaru">
                        <label class="col-sm-4 col-form-label-sm">RT</label>
                        <div class="col-sm-2">
                            <input type="hidden" id="idOP" name="idOP"
                                value="{{ $pelayanan->objekPajak->t_id_op ?? '' }}">
                            <input type="hidden" id="nop" name="nop"
                                value="{{ $pelayanan->objekPajak->nop ?? '' }}" required>
                            <input type="text" class="form-control form-control-sm" id="rtOP" name="rtOP"
                                value="{{ $pelayanan->objekPajak->t_rt_op ?? '' }}"
                                onkeypress="return onlyNumberKey(event)" readonly="">
                        </div>
                        <label class="col-sm-2 col-form-label-sm">RW</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control form-control-sm" id="rwOP" name="rwOP"
                                value="{{ $pelayanan->objekPajak->t_rw_op ?? '' }}"
                                onkeypress="return onlyNumberKey(event)" readonly="">
                        </div>
                    </div>

                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Jalan</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="jalanOP" name="jalanOP"
                                value="{{ $pelayanan->objekPajak->t_jalan_op ?? '' }}" readonly="">
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Blok/No/Kav</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="kd_blok" name="kd_blok"
                                value="{{ $pelayanan->objekPajak->kd_blok ?? '' }}" readonly="">
                        </div>
                    </div>
                    <input type="hidden" id="no_urut" name="no_urut"
                        value="{{ $pelayanan->objekPajak->no_urut ?? '' }}">
                    <input type="hidden" id="kd_jns_op" name="kd_jns_op"
                        value="{{ $pelayanan->objekPajak->kd_jns_op ?? '' }}">
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="t_kelurahan_op"
                                name="t_kelurahan_op" value="{{ $pelayanan->objekPajak->t_kelurahan_op ?? '' }}"
                                readonly="">
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Kecamatan</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="t_kecamatan_op"
                                name="t_kecamatan_op" value="{{ $pelayanan->objekPajak->t_kecamatan_op ?? '' }}"
                                readonly="">
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Luas Tanah
                            (M<sup>2</sup>)</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="luasTanah" name="luasTanah"
                                value="{{ $pelayanan->objekPajak->t_luas_tanah ?? '' }}"
                                onkeypress="return onlyNumberKey(event)">
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Luas Bangunan
                            (M<sup>2</sup>)</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="luas_bangunan"
                                name="luas_bangunan" value="{{ $pelayanan->objekPajak->t_luas_bangunan ?? '' }}"
                                readonly="">
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <hr style="background-color: #28a745; height: 2px;">
                    <p style="font-weight: bold">DATA WAJIB PAJAK (BARU)</p>
                    <hr>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">NIK <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="hidden" id="idWP" name="idWP"
                                value="{{ $pelayanan->wajibPajak->t_id_wp ?? '' }}">
                            <input type="text" class="form-control form-control-sm masked-nik" id="nikWP"
                                name="nikWP" maxlength="16" value="{{ $pelayanan->wajibPajak->t_nik_wp ?? '' }}"
                                required>
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Nama <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="namaWP" name="namaWP"
                                value="{{ $pelayanan->wajibPajak->t_nama_wp ?? '' }}" required>
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">RT <span class="text-danger">*</span></label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control form-control-sm" id="rtWP" name="rtWP"
                                value="{{ $pelayanan->wajibPajak->t_rt_wp ?? '' }}"
                                onkeypress="return onlyNumberKey(event)" required>
                        </div>
                        <label class="col-sm-2 col-form-label-sm">RW <span class="text-danger">*</span></label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control form-control-sm" id="rwWP" name="rwWP"
                                value="{{ $pelayanan->wajibPajak->t_rw_wp ?? '' }}"
                                onkeypress="return onlyNumberKey(event)" required>
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Jalan <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="jalanWP" name="jalanWP"
                                value="{{ $pelayanan->wajibPajak->t_jalan_wp ?? '' }}" required>
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa <span
                                class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="kelurahanWP"
                                name="kelurahanWP" value="{{ $pelayanan->wajibPajak->t_kelurahan_wp ?? '' }}"
                                required>
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Kecamatan <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="kecamatanWP"
                                name="kecamatanWP" value="{{ $pelayanan->wajibPajak->t_kecamatan_wp ?? '' }}"
                                required>
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Kabupaten/Kota <span
                                class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="kabupatenWP"
                                name="kabupatenWP" value="{{ $pelayanan->wajibPajak->t_kabupaten_wp ?? '' }}"
                                required>
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">No. Telp<span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" required class="form-control form-control-sm masked-hp"
                                id="noHpWP" name="noHpWP" maxlength="15"
                                value="{{ old('t_no_hp_wp') ?? ($pelayanan->wajibPajak->t_no_hp_wp ?? '') }}">
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">NPWPD</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm masked-npwpd" id="npwpd"
                                name="npwpd" value="{{ old('t_npwpd') ?? ($pelayanan->wajibPajak->t_npwpd ?? '') }}">
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">Email</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="email" name="email"
                                value="{{ old('t_email') ?? ($pelayanan->wajibPajak->t_email ?? '') }}">
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<div class="col-12">
    <div class="card" style="border-top: 5px solid #bd2130;">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6" id="rinciLuasBangunan">
                    <hr style="background-color: #28a745; height: 2px;">
                    <p style="font-weight: bold">RINCIAN LUAS BANGUNAN</p>
                    <hr>
                    <div id="luasBangunan">
                        @if (isset($pelayanan->detailBangunan))
                            @foreach ($pelayanan->detailBangunan as $index => $item)
                                <div className="form-group-sm row">
                                    <input type="hidden" name="idDetailBangunan[{{ $index }}]"
                                        value="{{ $item->t_id_detail_bangunan }}">
                                    <input type="hidden" value="{{ $item->t_no_urut_bangunan }}"
                                        name="nomorBangunan[{{ $index }}]">
                                    <label className="col-sm-4 col-form-label-sm">Bangunan
                                        Ke-{{ $item->t_no_urut_bangunan }}</label>
                                    <div className="col-sm-4 ">
                                        <input type="text" class="form-control form-control-sm"
                                            value="{{ $item->t_luas }}" name="luasBangunan[{{ $index }}]"
                                            onChange="hitungLuasBangunan()" onkeypress="return onlyNumberKey(event)"
                                            onBlur="hitungLuasBangunan()">
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        function hitungLuasBangunan() {
            let total = 0;
            let values = $("input[name^='luasBangunan']")
                .map(function() {
                    return $(this).val();
                }).get();

            values.forEach((arr) => {
                total += parseFloat(arr);
            })

            $("#luas_bangunan").val(total);
        }
    </script>
@endpush
