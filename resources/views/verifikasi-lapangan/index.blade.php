@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Verifkasi Lapangan
@endsection

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('setting-pemda') }}">Setting</a></li>
                        <li class="breadcrumb-item active">Verifikasi Lapangan</li>
                    </ol>
                </div><!-- /.col -->
                {{-- <div class="col-12">
                    <div class="info-box" id="background2">
                        <div class="col-sm-3">
                            <div class="description-block" style="float: left;">
                                <h1 class="description-header">Verifikasi Lapangan</h1>
                            </div>
                            <!-- /.description-block -->
                        </div>
                    </div>
                </div> --}}
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <section class="content">
        <div class="container-fluid">
            @if (session('success'))
                <div class="alert alert-dismissible alert_customs_success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-check"></i> Berhasil !!</h5>
                    <span>{{ session('success') }}</span>
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-dismissible alert_customs_danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-exclamation-triangle"></i> Peringantan !!</h5>
                    <span>{{ session('error') }}</span>
                </div>
            @endif
            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-info elevation-1"><i class="fas fa-file"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Pengajuan</span>
                            <span class="info-box-number" id="jumlahPengajuan">{{ $totalPengajuanVerlap ?? 0 }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-book"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Belum</span>
                            <span class="info-box-number" id="jumlahVerlapBelum">{{ $totalBelumVerlap ?? 0 }}</span>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-archive"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Ditolak</span>
                        <span class="info-box-number" id="jumlahVerlapDitolak">{{ $totalTolakVerlap ?? 0 }}</span>
                    </div>
                </div>
            </div> --}}
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-success elevation-1"><i class="fas fa-thumbs-up"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Selesai</span>
                            <span class="info-box-number" id="jumlahSudahVerlap">{{ $totalVerlap ?? 0 }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card card-danger card-outline card-outline-tabs ">
                        <div class="card-header p-0 border-bottom-0">
                            <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="pill" href="#navtab1" role="tab"
                                        aria-controls="navtab1" aria-selected="true">Belum</a>
                                </li>
                                {{-- <li class="nav-item">
                                <a class="nav-link" data-toggle="pill" href="#navtab2" role="tab"
                                    aria-controls="navtab2" aria-selected="false">Ditolak</a>
                            </li> --}}
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="pill" href="#navtab3" role="tab"
                                        aria-controls="navtab3" aria-selected="false">Sudah</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="pill" href="#navtab4" role="tab"
                                        aria-controls="navtab4" aria-selected="false">Semua</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-three-tabContent">
                                @include('verifikasi-lapangan.datagrid.belum')
                                {{-- @include('verifikasi-lapangan.datagrid.ditolak') --}}
                                @include('verifikasi-lapangan.datagrid.sudah')
                                @include('verifikasi-lapangan.datagrid.semua')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
