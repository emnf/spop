@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Setting Alur Pelayanan
@endsection

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Setting About</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Setting</a></li>
                        <li class="breadcrumb-item active">About</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form class="card card-outline card-info form-validation" method="post"
                    action="{{ route('setting-about.save') }}">
                    @csrf
                    <div class="card-header">
                        <h3 class="card-title">
                            About
                        </h3>
                        <input type="hidden" id="s_id" name="s_id" value="{{ $about->s_id ?? '' }}">
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse"
                                data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            <button type="button" class="btn btn-tool btn-sm" data-card-widget="remove"
                                data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>
                        </div>
                    </div>
                    <div class="card-body pad">
                        <div class="mb-3">
                            <textarea class="textarea" placeholder="Place some text here" id="s_keterangan_about" name="s_keterangan_about"
                                style="width: 100%; height: 300px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                required>{{ $about->s_keterangan_about ?? '' }}</textarea>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-info btn-block" type="submit">SIMPAN</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(function() {
            // Summernote
            $('.textarea').summernote()
        })
    </script>
@endPush
