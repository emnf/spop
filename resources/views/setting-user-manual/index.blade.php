@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Setting User Manual
@endsection

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Setting</a></li>
                        <li class="breadcrumb-item active">Grid</li>
                    </ol>
                </div>
                <div class="col-12">
                    <div class="info-box" id="background2">
                        <div class="col-sm-3">
                            <div class="description-block" style="float: left;">
                                <h1 class="description-header">SETTING USER MANUAL</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="col-sm-12" style="margin-bottom: 10px;">
                                <a href="{{ route('setting-user-manual.tambah') }}" class="btn btn-info btn-sm"><span
                                        class="fas fa-plus-square"></span> Tambah</a>
                                {{-- <button class="btn btn-danger btn-sm" id="btnCetakPDF"><span class="fas fa-file-pdf"></span> &nbsp;PDF</button>
                            <button class="btn btn-success btn-sm" id="btnCetakExcel"><span class="fas fa-file-excel"></span> &nbsp;XLS</button> --}}
                            </div>
                        </div>
                        <div class="card-body table-responsive">
                            <table class="table table-bordered table-striped table-hover" id="datagrid-table">
                                <thead>
                                    <tr>
                                        <th style="background-color: #1fc8e3" class="text-center">No</th>
                                        <th style="background-color: #1fc8e3" class="text-center">Nama Menu</th>
                                        <th style="background-color: #1fc8e3" class="text-center">Nama File</th>
                                        <th style="background-color: #1fc8e3" class="text-center">File</th>
                                        <th style="background-color: #1fc8e3" class="text-center">Action</th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th><input class="form-control form-control-sm" id="filter-s_nama_menu"
                                                type="text" /></th>
                                        <th><input class="form-control form-control-sm" id="filter-s_nama_file"
                                                type="text" /></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center" colspan="5"> Tidak ada data.</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer clearfix pagination-footer">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        var datatables = datagrid({
            url: '/setting-user-manual/datagrid',
            table: "#datagrid-table",
            columns: [{
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
            ],
            orders: [{
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
            ],
            action: [{
                    name: 'edit',
                    btnClass: 'btn btn-outline-primary btn-sm mx-1',
                    btnText: '<i class="fas fa-pen"></i>'
                },
                {
                    name: 'delete',
                    btnClass: 'btn btn-outline-danger btn-sm mx-1',
                    btnText: '<i class="fas fa-trash"></i>'
                }
            ]

        });

        $("#filter-s_nama_menu", "filter-s_nama_file").keyup(function() {
            search();
        });

        function search() {
            datatables.setFilters({
                s_nama_menu: $("#filter-s_nama_menu").val(),
                s_nama_file: $("#filter-s_nama_file").val(),
            });
            datatables.reload();
        }
        search();

        function showDeleteDialog(a) {
            Swal.fire({
                title: 'Hapus Data',
                text: "Apa anda Yakin ingin menghapus ?",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Tidak',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '/setting-user-manual/delete/' + a,
                        type: 'delete',
                        dataType: "JSON",
                        data: {
                            s_id_alur: a
                        },
                        success: function(request, error) {
                            if (request == 'success') {
                                Toast.fire({
                                    icon: 'success',
                                    title: 'Data Berhasil Dihapus'
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: 'Data Gagal Dihapus'
                                });
                            }
                            datatables.reload();
                        }
                    });
                }
            })
        }

        $("#btnHapus").click(function() {
            $.ajax({
                url: '/setting-user-manual/delete',
                method: "POST",
                data: JSON.stringify({
                    idAlur: $("#idUserManual").val()
                }),
                contentType: 'application/json'
            }).then(function(data) {
                $("#modal-delete").modal("hide");
                datatables.reload();
            });
        });
    </script>
@endPush
