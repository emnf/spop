@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Setting User Manual
@endsection

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('setting-user-manual') }}">Setting</a></li>
                        <li class="breadcrumb-item active">Grid</li>
                    </ol>
                </div>
                <div class="col-12">
                    <div class="info-box" id="background2">
                        <div class="col-sm-3">
                            <div class="description-block" style="float: left;">
                                <h1 class="description-header">SETTING USER MANUAL</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="col-sm-12" style="margin-bottom: 10px;">
                                Form Setting User Manual [ {{ isset($s_id_usermanual) ? 'Edit' : 'Tambah' }} ]
                            </div>
                        </div>
                        <form enctype="multipart/form-data" class="form-horizontal form-validation" method="POST"
                            action="{{ route('setting-user-manual.store') }} ">
                            @csrf
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="s_nama_menu">Nama Menu</label>
                                    <div class="col-sm-4">
                                        <input id="s_id_usermanual" name="s_id_usermanual" type="hidden"
                                            value="{{ $s_id_usermanual ?? '' }}">
                                        <input class="form-control @error('s_nama_menu') is-invalid @enderror"
                                            name="s_nama_menu" id="s_nama_menu" value="{{ $s_nama_menu ?? '' }}" required>
                                        @error('s_nama_menu')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="s_nama_file">Nama File</label>
                                    <div class="col-sm-4">
                                        <input class="form-control @error('s_nama_file') is-invalid @enderror"
                                            id="s_nama_file" name="s_nama_file" value="{{ $s_nama_file ?? '' }}" required>
                                        @error('s_nama_file')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group-sm row">
                                    <label class="col-sm-2 col-form-label-sm" for="s_lokasi_file">File</label>
                                    <div class="col-sm-4">
                                        <div class="custom-file">
                                            <input type="file"
                                                class="custom-file-input @error('s_lokasi_file') is-invalid @enderror"
                                                id="s_lokasi_file" name="s_lokasi_file">
                                            <label class="custom-file-label" for="s_lokasi_file">Cari Berkas</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group-sm row">
                                    <label class="col-sm-2 col-form-label-sm" for="s_lokasi_file"></label>
                                    <div class="col-sm-4">
                                        <?php if(isset($s_lokasi_file) && explode('.',$s_lokasi_file)[1] == "pdf"){ ?>
                                        <iframe
                                            src="{{ !empty($s_lokasi_file) ? asset('/storage/' . $s_lokasi_file) : '' }}"
                                            style="width: 100%;height: 110%;border: none;"></iframe>
                                        <?php }else{ ?>
                                        <img id="s_lokasi_file" name="s_lokasi_file"
                                            src="{{ !empty($s_lokasi_file) ? asset('/storage/' . $s_lokasi_file) : '' }}"
                                            width="120"rt>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-info" type="submit">SIMPAN</button>
                                <a class="btn btn-danger btn-default float-right"
                                    href="{{ route('setting-user-manual') }}">BATAL</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript"></script>
@endPush
