@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Role & Permission
@endsection

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="">Setting</a></li>
                        <li class="breadcrumb-item active">Role & Permission</li>
                    </ol>
                </div><!-- /.col -->
                {{-- <div class="col-12">
                    <div class="info-box callout callout-danger" id="background2" style="">
                        <div class="col-sm-12">
                            <div class="" style="float: left;">
                                <h5 class="">ROLE & PERMISSION</h5>
                            </div>
                            <!-- /.description-block -->
                        </div>
                    </div>
                </div><!-- /.row --> --}}
            </div><!-- /.container-fluid -->
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="col-sm-12" style="margin-bottom: 10px;">
                                <a href="{{ url('setting-role/tambah') }}" class="btn btn-info">Tambah</a>
                            </div>
                        </div>

                        <div class="card-body table-responsive">
                            <table class="table table-bordered table-striped table-hover" id="datagrid-table">
                                <thead>
                                    <tr>
                                        <th style="background-color: #1fc8e3" class="text-center">No</th>
                                        <th style="background-color: #1fc8e3" class="text-center">Role</th>
                                        <th style="background-color: #1fc8e3" class="text-center">Permission</th>
                                        <th style="background-color: #1fc8e3" class="text-center">Action</th>
                                    </tr>
                                    {{-- <tr>
                                    <th></th>
                                    <th><input class="form-control form-control-sm" id="filter-s_username"
                                            type="text" /></th>
                                    <th></th>
                                </tr> --}}
                                </thead>
                                <tbody>
                                    @if ($roles->count() > 0)
                                        @foreach ($roles as $i => $item)
                                            <tr>
                                                <td class="text-center">{{ $i + 1 }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ implode(', ', $item->getPermissionNames()->toArray()) }}</td>
                                                <td class="text-center">
                                                    <button type="button" class="btn btn-sm btn-warning"><i
                                                            class="fas fa-edit"></i>
                                                        Edit</button>
                                                    <button type="button" class="btn btn-sm btn-secondary"
                                                        onclick="showModalPermission({{ $item->id }})"><i
                                                            class="fas fa-cogs"></i>
                                                        Permission</button>
                                                    <button type="button" class="btn btn-sm btn-danger"><i
                                                            class="fas fa-trash"></i>
                                                        Delete</button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="3"> Tidak ada data.</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer clearfix pagination-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="modal-permission">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h6 class="modal-title"><span class="fas fa-exclamation"></span>
                        &nbsp;PILIH PERMISSION</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('setting-roles.sync-permit') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Role Name</label>
                            <input type="text" class="form-control form-control-sm" id="roleName" name="roleName"
                                readonly>
                        </div>
                        <div class="form-group">
                            <label>Daftar Permission</label>
                            <select class="select2 form-select" id="listPermission" name="listPermission[]"
                                multiple="multiple" data-placeholder="Select a State" style="width: 100%;">
                                @foreach ($permissionList as $permit)
                                    <option value="{{ $permit->permissionName }}">{{ $permit->permissionName }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span
                                class="fas fa-times-circle"></span>
                            &nbsp;TUTUP
                        </button>
                        <button type="submit" class="btn btn-success btn-sm"><span class="fas fa-exclamation"></span>
                            &nbsp;SIMPAN</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(function() {
            $('.select2').select2({
                theme: "classic"
            })
        })

        const showModalPermission = function(a) {
            $.get('/setting-roles/' + a, (res) => {
                $("#roleName").val(res.role.name);
                $('#listPermission').val(res.permissions).trigger('change');
            })
            $("#modal-permission").modal('show');
        }
    </script>
@endpush
