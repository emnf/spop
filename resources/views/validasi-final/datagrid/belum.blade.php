<div data-th-fragment="tab-panel" class="tab-pane fade show active" id="datagrid-belum" role="tabpanel"
    aria-labelledby="datagrid-belum">
    <!--<div class="row mb-2">
        <div class="col-12 pl-0">
            <button class="btn btn-danger btn-sm" onclick="printValidasiBelumProses('PDF')"><span class="fas fa-file-pdf"></span> &nbsp;PDF</button>
            <button class="btn btn-success btn-sm" onclick="printValidasiBelumProses('XLS')"><span class="fas fa-file-excel"></span> &nbsp;XLS</button>
        </div>
    </div>-->
    <div class="row table-responsive">

        <table id="table-belum" class="table table-striped table-bordered" style="font-size: 12px; width: 100%">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Jenis Pajak</th>
                    <th>Jenis Pelayanan</th>
                    <th>Tgl. Verifikasi</th>
                    <th>No. Pelayanan</th>
                    <th>NIK</th>
                    <th>Nama</th>
                    <th style="width: 8rem;">#</th>
                </tr>
                <tr>
                    <th></th>
                    <th>
                        <select class="form-control form-control-sm dgbp-filter" id="dgbp-filter-id-jenis-pajak">
                            <option value="">Semua Jenis Pajak</option>
                            <option th:each="pajak: ${jenisPajak}" th:value="${pajak.idJenisPajak}"
                                th:text="${pajak.namaJenisPajak}"></option>
                        </select>
                    </th>
                    <th>
                        <select class="form-control form-control-sm dgbp-filter" id="dgbp-filter-id-jenis-pelayanan">
                            <option value="">Semua Jenis Pelayanan</option>
                            <option th:each="pelayanan: ${jenisPelayanan}" th:value="${pelayanan.idJenisPelayanan}"
                                th:text="${pelayanan.namaJenisPelayanan}"></option>
                        </select>
                    </th>
                    <th><input type="text" id="dgbp-filter-tgl-pelayanan"
                            class="form-control form-control-sm dgbp-filter" readonly></th>
                    <th><input type="text" id="dgbp-filter-no-pelayanan"
                            class="form-control form-control-sm dgbp-filter"></th>
                    <th><input type="text" id="dgbp-filter-nik-pemohon"
                            class="form-control form-control-sm dgbp-filter masked-nik"></th>
                    <th><input type="text" id="dgbp-filter-nama-pemohon"
                            class="form-control form-control-sm dgbp-filter"></th>
                    <th></th>

                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="card-footer pagination-footer">

    </div>
</div>

@push('scripts')
<script>
    $(function() {
            const dataGridBelumProses = datagrid({
                url: '/validasi-final/datagrid-belum',
                table: "#table-belum",
                columns: [{
                    class: ""
                }, {
                    class: ""
                }, {
                    class: "text-center"
                }, {
                    class: "text-center"
                }, {
                    class: ""
                }, {
                    class: "text-center"
                }, {
                    class: "text-center"
                }, {
                    class: "text-center"
                }, ],
                orders: [{
                        sortable: false
                    }, {
                        sortable: true,
                        name: "idJenisPajak"
                    }, {
                        sortable: true,
                        name: "idJenisPelayanan"
                    }, {
                        sortable: true,
                        name: "verifikasi.tglVerifikasi"
                    }, {
                        sortable: true,
                        name: "noPelayanan"
                    }, {
                        sortable: true,
                        name: "nikPemohon"
                    }, {
                        sortable: true,
                        name: "namaPemohon"
                    }, {
                        sortable: false
                    }

                ],
                action: [{
                    name: 'tambah',
                    btnClass: 'btn btn-warning btn-xs',
                    btnText: '<i class="fas fa-pencil-alt"> Validasi</i>'
                }]

            });

            function searchBelumProses() {
                dataGridBelumProses.setFilters({
                    idJenisPajak: $("#dgbp-filter-id-jenis-pajak").val(),
                    idJenisPelayanan: $("#dgbp-filter-id-jenis-pelayanan").val(),
                    tglPelayanan: $("#dgbp-filter-tgl-pelayanan").val(),
                    noPelayanan: $("#dgbp-filter-no-pelayanan").val(),
                    nikPemohon: $("#dgbp-filter-nik-pemohon").val(),
                    namaPemohon: $("#dgbp-filter-nama-pemohon").val()
                });
                dataGridBelumProses.reload();
            }

            $(".dgbp-filter").each(function() {
                $(this).change(function() {
                    searchBelumProses();
                });

                $(this).keyup(function() {
                    searchBelumProses();
                })
            });

            $("#dgbp-filter-tgl-pelayanan").daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $("#dgbp-filter-tgl-pelayanan").on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
                searchBelumProses();
            });

            $("#dgbp-filter-tgl-pelayanan").on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                searchBelumProses();
            });

            searchBelumProses();
        })
</script>
@endpush
