@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Setting Pemda
@endsection

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('setting-pemda') }}">Setting</a></li>
                        <li class="breadcrumb-item active">Data Pemda</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-12">
                    <div class="info-box" id="background2">
                        <div class="col-sm-3">
                            <div class="description-block" style="float: left;">
                                <h1 class="description-header">SETTING DATA PEMDA</h1>
                            </div>
                            <!-- /.description-block -->
                        </div>
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal form-validation" method="post"
                        action="{{ url('setting-pemda/update/1') }} " enctype="multipart/form-data">
                        @csrf
                        @if (session('success'))
                            <div class="alert alert-success alert-dismissible callout callout-success" id="alert">
                                <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-check"></i> Berhasil!</h5>
                                {{ session('success') }}
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger alert-dismissible callout callout-danger" id="alert">
                                <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i> Gagal!</h5>
                                {{ session('error') }}
                            </div>
                        @endif
                        <div class="card card-info" style="border-top:3px solid #138496;">
                            <div class="card-body">
                                <div class="form-group-sm row">
                                    <label class="col-sm-2 col-form-label-sm" for="s_kode_provinsi">Kode/Nama
                                        Provinsi</label>
                                    <div class="col-sm-1">
                                        <input name="s_id_pemda" type="hidden" value="{{ $pemda->s_id_pemda }}">
                                        <input class="form-control form-control-sm" id="s_kode_provinsi"
                                            name="s_kode_provinsi" type="text" value="{{ $pemda->s_kode_provinsi }}">
                                    </div>
                                    <div class="col-sm-3">
                                        <input class="form-control form-control-sm" id="s_nama_prov" name="s_nama_prov"
                                            required type="text" value="{{ $pemda->s_nama_prov }}">
                                    </div>
                                    <div class="col-sm-6"><img src="" width="100%" heigh="100%"></div>
                                </div>
                                <div class="form-group-sm row">
                                    <label class="col-sm-2 col-form-label-sm" for="s_kode_kabkot">Kode/Nama
                                        Kabupaten</label>
                                    <div class="col-sm-1">
                                        <input class="form-control form-control-sm" id="s_kode_kabkot" name="s_kode_kabkot"
                                            required type="text" value="{{ $pemda->s_kode_kabkot }}">
                                    </div>
                                    <div class="col-sm-3">
                                        <input class="form-control form-control-sm" id="s_nama_kabkota"
                                            name="s_nama_kabkota" required type="text"
                                            value="{{ $pemda->s_nama_kabkota }}">
                                    </div>
                                </div>
                                <div class="form-group-sm row">
                                    <label class="col-sm-2 col-form-label-sm" for="s_nama_ibukota_kabkota">Nama Ibu
                                        Kota</label>
                                    <div class="col-sm-4">
                                        <input class="form-control form-control-sm" id="s_nama_ibukota_kabkota"
                                            name="s_nama_ibukota_kabkota" required type="text"
                                            value="{{ $pemda->s_nama_ibukota_kabkota }}">
                                    </div>
                                </div>
                                <div class="form-group-sm row">
                                    <label class="col-sm-2 col-form-label-sm" for="s_nama_instansi">Nama Instansi</label>
                                    <div class="col-sm-4">
                                        <input class="form-control form-control-sm" id="s_nama_instansi"
                                            name="s_nama_instansi" required type="text"
                                            value="{{ $pemda->s_nama_instansi }}">
                                    </div>
                                </div>
                                <div class="form-group-sm row">
                                    <label class="col-sm-2 col-form-label-sm" for="s_nama_singkat_instansi">Nama Singkat
                                        Instansi</label>
                                    <div class="col-sm-4">
                                        <input class="form-control form-control-sm" id="s_nama_singkat_instansi"
                                            name="s_nama_singkat_instansi" required type="text"
                                            value="{{ $pemda->s_nama_singkat_instansi }}">
                                    </div>
                                </div>
                                <div class="form-group-sm row">
                                    <label class="col-sm-2 col-form-label-sm" for="s_alamat_instansi">Alamat
                                        Instansi</label>
                                    <div class="col-sm-4">
                                        <textarea class="form-control form-control-sm" id="s_alamat_instansi" name="s_alamat_instansi" required>{{ $pemda->s_alamat_instansi }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group-sm row">
                                    <label class="col-sm-2 col-form-label-sm" for="s_kode_pos">Kode Pos</label>
                                    <div class="col-sm-4">
                                        <input class="form-control form-control-sm" id="s_kode_pos" name="s_kode_pos"
                                            required type="text" value="{{ $pemda->s_kode_pos }}">
                                    </div>
                                </div>
                                <div class="form-group-sm row">
                                    <label class="col-sm-2 col-form-label-sm" for="s_notelp_instansi">No. Telp.</label>
                                    <div class="col-sm-4">
                                        <input class="form-control form-control-sm" id="s_notelp_instansi"
                                            name="s_notelp_instansi" required type="text"
                                            value="{{ $pemda->s_notelp_instansi }}">
                                    </div>
                                </div>
                                <div class="form-group-sm row">
                                    <label class="col-sm-2 col-form-label-sm" for="s_logo">Logo</label>
                                    <div class="col-sm-4">
                                        <!-- <input type="file" class="form-control" id="s_logo" name="s_logo" required> -->
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="s_logo"
                                                name="s_logo">
                                            <label class="custom-file-label" for="s_logo">Cari Berkas</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group-sm row">
                                    <label class="col-sm-2 col-form-label-sm" for="s_logo"></label>
                                    <div class="col-sm-4">
                                        <img src="{{ $pemda->s_logo ?? '' }}" width="120">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-info btn-block" type="submit">SIMPAN</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
