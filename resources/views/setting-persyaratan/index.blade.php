@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Setting Jenis Persyaratan
@endsection

@section('content')
    <section class="content">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Setting</a></li>
                            <li class="breadcrumb-item active">Grid</li>
                        </ol>
                    </div><!-- /.col -->
                    <div class="col-12">
                        <div class="info-box" id="background2">
                            <div class="col-sm-3">
                                <div class="description-block" style="float: left;">
                                    <h1 class="description-header">SETTING PERSYARATAN</h1>
                                </div>
                                <!-- /.description-block -->
                            </div>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-outline card-info">
                            <div class="card-header">
                                <div class="col-sm-12" style="margin-bottom: 10px;">
                                    <a href="{{ route('setting-persyaratan.tambah') }}" class="btn btn-info">Tambah</a>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive">
                                <table class="table table-bordered table-striped table-hover" id="datagrid-table">
                                    <thead>
                                        <tr>
                                            <th style="background-color: #1fc8e3" class="text-center">No</th>
                                            <th style="background-color: #1fc8e3" class="text-center">Nama Jenis Pelayanan
                                            </th>
                                            <th style="background-color: #1fc8e3" class="text-center">Nama Persyaratan</th>
                                            <th style="background-color: #1fc8e3" class="text-center">Status Persyaratan
                                            </th>
                                            <th style="background-color: #1fc8e3" class="text-center">Action</th>
                                        </tr>
                                        <tr>
                                            <th></th>
                                            <th>
                                                <select class="form-control form-control-sm"
                                                    id="filter-s_id_jenis_pelayanan" required>
                                                    <option value="">Silahkan Pilih</option>
                                                    @foreach ($jenisPelayanan as $row)
                                                        <option value="{{ $row['s_id_jenis_pelayanan'] }}">
                                                            {{ $row['s_nama_jenis_pelayanan'] }}</option>
                                                    @endforeach
                                                </select>
                                            </th>
                                            <th><input class="form-control form-control-sm" id="filter-s_nama_persyaratan"
                                                    type="text" /></th>
                                            <th>
                                                <select class="form-control form-control-sm" id="filter-s_is_optional">
                                                    <option value="">Silahkan Pilih</option>
                                                    <option value="false">Wajib</option>
                                                    <option value="true">Tidak Wajib</option>
                                                </select>
                                            </th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center" colspan="6"> Tidak ada data.</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer clearfix pagination-footer">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
@push('scripts')
    <script type="text/javascript">
        var datatables = datagrid({
            url: '/setting-persyaratan/datagrid',
            table: "#datagrid-table",
            serverSide: true,
            columns: [{
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
            ],
            orders: [{
                    sortable: false
                },
                {
                    sortable: true,
                    name: "s_id_jenis_pelayanan"
                },
                {
                    sortable: true,
                    name: "s_nama_persyaratan"
                },
                {
                    sortable: true,
                    name: "s_is_optional"
                },
                {
                    sortable: false
                }
            ],
            action: [{
                    name: 'edit',
                    btnClass: 'btn btn-outline-primary btn-sm mx-1',
                    btnText: '<i class="fas fa-pen"></i>'
                },
                {
                    name: 'delete',
                    btnClass: 'btn btn-outline-danger btn-sm mx-1',
                    btnText: '<i class="fas fa-trash"></i>'
                }
            ]

        });

        $("#filter-s_nama_persyaratan").keyup(function() {
            search();
        });

        $("#filter-s_id_jenis_pelayanan, #filter-s_is_optional").change(function() {
            search();
        });

        function search() {
            datatables.setpageNumber(0);
            datatables.setFilters({
                s_nama_persyaratan: $("#filter-s_nama_persyaratan").val(),
                s_id_jenis_pelayanan: $("#filter-s_id_jenis_pelayanan").val(),
                s_is_optional: $("#filter-s_is_optional").val()
            });
            datatables.reload();
        }
        search();

        function showDeleteDialog(a) {
            Swal.fire({
                title: 'Hapus Data',
                text: "Apa anda Yakin ingin menghapus ?",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Tidak',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '/setting-persyaratan/delete/' + a,
                        type: 'delete',
                        dataType: "JSON",
                        data: {
                            s_id_persyaratan: a
                        },
                        success: function(request, error) {
                            if (request == 'success') {
                                Toast.fire({
                                    icon: 'success',
                                    title: 'Data Berhasil Dihapus'
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: 'Data Gagal Dihapus'
                                });
                            }
                            datatables.reload();
                        }
                    });
                }
            })
        }

        function cetakData(a) {
            window.open(
                `{{ url('setting/pegawai/cetak-daftar') }}?type-cetak=${a}`
            );
        }
    </script>
@endPush
