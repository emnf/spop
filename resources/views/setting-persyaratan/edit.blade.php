@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Setting Pegawai
@endsection

@section('content')
    <section class="content">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Setting</a></li>
                            <li class="breadcrumb-item active">Grid</li>
                        </ol>
                    </div><!-- /.col -->
                    <div class="col-12">
                        <div class="info-box" id="background2"
                            style="background: url({{ asset('/asset/images/bgpelayanan.gif') }}) #fff no-repeat center center;
                            -moz-background-size: cover;
                            -o-background-size: cover;
                            background-size: cover;">
                            <div class="col-sm-3">
                                <div class="description-block" style="float: left;">
                                    <h1 class="description-header">SETTING PEGAWAI</h1>
                                </div>
                                <!-- /.description-block -->
                            </div>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-outline card-info">
                            <div class="card-header">
                                <div class="col-sm-12" style="margin-bottom: 10px;">
                                    Form Setting Pegawai [Edit]
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <form class="form-horizontal form-validation" method="post"
                                action="{{ route('setting-persyaratan.update', $data->s_id_persyaratan) }}">
                                @csrf
                                @method('PUT')
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_id_jenis_pelayanan">Nama Jenis
                                            Pelayanan</label>
                                        <div class="col-sm-4">
                                            <input id="s_id_persyaratan" name="s_id_persyaratan" type="hidden"
                                                value="{{ $data->s_id_persyaratan }}">
                                            <select class="form-control" id="s_id_jenis_pelayanan"
                                                name="s_id_jenis_pelayanan" required>
                                                <option value="">Silahkan Pilih</option>
                                                @foreach ($jenisPelayanan as $row)
                                                    <option
                                                        {{ $row['s_id_jenis_pelayanan'] == $data->s_id_jenis_pelayanan ? 'selected' : '' }}
                                                        value="{{ $row['s_id_jenis_pelayanan'] }}">
                                                        {{ $row['s_nama_jenis_pelayanan'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_nama_persyaratan">Nama
                                            Persyaratan</label>
                                        <div class="col-sm-4">
                                            <input class="form-control" id="s_nama_persyaratan" name="s_nama_persyaratan"
                                                type="text" value="{{ $data->s_nama_persyaratan }}" required>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_nama_persyaratan">Status
                                            Persyaratan</label>
                                        <div class="col-sm-4">
                                            <select class="form-control" id="s_is_optional" name="s_is_optional">
                                                <option {{ !$data->s_is_optional ? 'selected' : '' }} value="0">Wajib
                                                </option>
                                                <option {{ $data->s_is_optional ? 'selected' : '' }} value="1">Tidak
                                                    Wajib</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-info" type="submit">SIMPAN</button>
                                    <a class="btn btn-danger btn-default float-right"
                                        href="{{ route('setting-persyaratan') }}">KEMBALI</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
