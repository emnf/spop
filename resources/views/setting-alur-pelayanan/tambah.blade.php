@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Setting Alur Pelayanan
@endsection

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('setting-alur-pelayanan') }}">Setting</a></li>
                        <li class="breadcrumb-item active">Grid</li>
                    </ol>
                </div>
                <div class="col-12">
                    <div class="info-box" id="background2">
                        <div class="col-sm-3">
                            <div class="description-block" style="float: left;">
                                <h1 class="description-header">SETTING ALUR PELAYANAN</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="col-sm-12" style="margin-bottom: 10px;">
                                Form Setting Alur Pelayanan [ {{ isset($s_id_alur) ? 'Edit' : 'Tambah' }} ]
                            </div>
                        </div>
                        <form class="form-horizontal form-validation" method="POST"
                            action="{{ route('setting-alur-pelayanan.store') }} ">
                            @csrf
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="s_judul_alur">Judul</label>
                                    <div class="col-sm-4">
                                        <input id="s_id_alur" name="s_id_alur" type="hidden"
                                            value="{{ $s_id_alur ?? '' }}">
                                        <input class="form-control @error('s_judul_alur') is-invalid @enderror"
                                            name="s_judul_alur" id="s_judul_alur" value="{{ $s_judul_alur ?? '' }}"
                                            required>
                                        @error('s_judul_alur')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="s_deskripsi_alur">Deskripsi</label>
                                    <div class="col-sm-4">
                                        <textarea class="form-control @error('s_deskripsi_alur') is-invalid @enderror" id="s_deskripsi_alur"
                                            name="s_deskripsi_alur" required>{{ $s_deskripsi_alur ?? '' }}</textarea>
                                        @error('s_deskripsi_alur')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="s_langkah_alur">Langkah</label>
                                    <div class="col-sm-4">
                                        <input class="form-control @error('s_langkah_alur') is-invalid @enderror"
                                            name="s_langkah_alur" id="s_langkah_alur" value="{{ $s_langkah_alur ?? '' }}"
                                            required>
                                        @error('langks_langkah_alurahAlur')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="s_user_alur">User</label>
                                    <div class="col-sm-4">
                                        <input type="text"
                                            class="form-control @error('s_user_alur') is-invalid @enderror""
                                            name="s_user_alur" id="s_user_alur" value="{{ $s_user_alur ?? '' }}">
                                        @error('s_user_alur')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label @error('s_icon_alur') is-invalid @enderror"
                                        for="s_icon_alur">Icon</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="s_icon_alur" id="s_icon_alur"
                                            value="{{ $s_icon_alur ?? '' }}">
                                        @error('s_icon_alur')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-info" type="submit">SIMPAN</button>
                                <a class="btn btn-danger btn-default float-right"
                                    href="{{ route('setting-alur-pelayanan') }}">BATAL</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript"></script>
@endPush
