@component('mail::layout')
    {{-- Header --}}
    @slot('header')
    @component('mail::header', ['url' => config('app.url')])
        {{ config('app.name') }}
    @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                Kepala BAPENDA
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.name') }} <img src="https://mitraprimautama.id/frontend/img/logo.png" style="width: 20px; height: 15px;" class="logo" alt="Laravel Logo"> (mpu). @lang('All rights reserved.')
        @endcomponent
    @endslot
@endcomponent
