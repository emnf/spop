<tr>
    <td class="header">
        <a href="{{ $url }}" style="display: inline-block;">
        @if (trim($slot) === 'Laravel')
            <img src="{{ asset('asset/images/elayanan3.png')}}" class="logo" alt="Pemda Logo">
        @else
            {{ $slot }}
        @endif
        </a>
    </td>
</tr>
