<div class="col-12">
    <div class="card" style="border-top: 5px solid #bd2130;">
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <td colspan="5">
                            <h5>Informasi Akun</h5>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td rowspan="2" style="text-align: center; vertical-align: middle; width: 80px;">
                            <img style="width: 80px; height: 80px;" src="/asset/images/user.png">
                        </td>
                        <td style="width: 10px;"><i class="fas fa-user"></i></td>
                        <td style="width: 100px;">Nama Akun</td>
                        <td style="width: 5px; text-align: center;">:</td>
                        <td>{{ auth()->user()->username }}</td>
                    </tr>
                    <tr>
                        <td><i class="fas fa-address-card"></i></td>
                        <td>Nama</td>
                        <td>:</td>
                        <td>{{ auth()->user()->name }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
