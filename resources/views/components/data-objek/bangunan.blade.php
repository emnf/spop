<div class="card card-danger card-outline" style="border-top: 5px solid #bd2130;">
    <div class="card-header">
        <div class="card-title">
            <h5>DATA BANGUNAN (HASIL PENELITIAN)</h5>
        </div>
    </div>
    <div class="card-body">
        <div class="col-md-12">
            @foreach ($pelayanan->detailBangunan as $i => $bangunan)

            <div class="card collapsed-card">
                <div class="card-header">
                    <h4 class="card-title">{{ 'Bangunan No. ' . $bangunan->t_no_urut_bangunan }}</h4>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-plus"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p style="font-weight: bold">RINCIAN DATA BANGUNAN</p>
                            <hr>
                            <div class="form-group-sm row">
                                <label class="col-sm-4 col-form-label-sm">Jenis Penggunaan
                                    Bangunan</label>
                                <div class="col-sm-4 ">
                                    <input type="hidden" name="idDetailBangunan[{{ $i }}]'}"
                                        value="{{ $bangunan->t_id_detail_bangunan }}">
                                    <input type="hidden" name="nomorBangunan[{{ $i }}]'}"
                                        value="{{ $bangunan->t_no_urut_bangunan }}">
                                    <select class="form-control form-control-sm"
                                        name="jenisPenggunaanBangunan[{{ $i }}]">
                                        <option value=""> ---Silahkan Pilih---</option>
                                        <option {{ $bangunan->t_jenis_penggunaan_bangunan == 'Perumahan' ? 'selected' :
                                            '' }} value="Perumahan"> Perumahan</option>
                                        <option {{ $bangunan->t_jenis_penggunaan_bangunan == 'Toko/Apotik/Pasar/Ruko' ?
                                            'selected' : '' }} value="Toko/Apotik/Pasar/Ruko"> Toko/Apotik/Pasar/Ruko
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-4 col-form-label-sm">Luas Bangunan <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control form-control-sm" onkeypress="return onlyNumberKey(event)"
                                        name="luasBangunan[{{ $i }}]" value="{{ $bangunan->t_luas }}">
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-4 col-form-label-sm">Jumlah Lantai <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-4">
                                    <input type="text" maxlength="4" class="form-control form-control-sm"
                                        name="jumlahLantai[{{$i}}]" value="{{ $bangunan->t_jumlah_lantai }}">
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-4 col-form-label-sm">Tahun Dibangun <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-4">
                                    <input type="text" maxlength="4" class="form-control form-control-sm"
                                        name="tahunBangunan[{{ $i }}]" value="{{ $bangunan->t_tahun_bangunan }}">
                                </div>

                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-4 col-form-label-sm">Tahun Direnovasi</label>
                                <div class="col-sm-4">
                                    <input type="text" maxlength="4" class="form-control form-control-sm"
                                        name="tahunRenovasi[{{ $i }}]" value="{{ $bangunan->t_tahun_renovasi }}">
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-4 col-form-label-sm">Kondisi Pada Umumnya <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-4 ">
                                    <select class="form-control form-control-sm" name="kondisiBangunan[{{$i}}]">
                                        <option value=""> ---Silahkan Pilih---</option>
                                        <option {{ $bangunan->t_kondisi_bangunan == 'SANGAT BAIK' ? 'selected' : '' }}
                                            value="SANGAT BAIK">Sangat Baik</option>
                                        <option {{ $bangunan->t_kondisi_bangunan == 'BAIK' ? 'selected' : '' }}
                                            value="BAIK">Baik</option>
                                        <option {{ $bangunan->t_kondisi_bangunan == 'SEDANG' ? 'selected' : '' }}
                                            value="SEDANG">Sedang</option>
                                        <option {{ $bangunan->t_kondisi_bangunan == 'JELEK' ? 'selected' : '' }}
                                            value="JELEK">Jelek</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-4 col-form-label-sm">Konstruksi <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-4 ">
                                    <select class="form-control form-control-sm" name="konstruksi[{{$i}}]">
                                        <option value=""> ---Silahkan Pilih---</option>
                                        <option {{ $bangunan->t_konstruksi == 'BAJA' ? 'selected' : '' }}
                                            value="BAJA">Baja</option>
                                        <option {{ $bangunan->t_konstruksi == 'BETON' ? 'selected' : '' }}
                                            value="BETON">Beton</option>
                                        <option {{ $bangunan->t_konstruksi == 'BATU BATA' ? 'selected' : '' }}
                                            value="BATU BATA">Batu Bata</option>
                                        <option {{ $bangunan->t_konstruksi == 'KAYU' ? 'selected' : '' }}
                                            value="KAYU">Kayu</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-4 col-form-label-sm">Atap <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-4 ">
                                    <select class="form-control form-control-sm" name="atap[{{$i}}]">
                                        <option value=""> ---Silahkan Pilih---</option>
                                        <option {{ $bangunan->t_atap == 'DECRABON/BETON/GTG GLAZUR' ? 'selected' : '' }}
                                            value="DECRABON/BETON/GTG GLAZUR"> Decrabon/Beton Gtg Glazur</option>
                                        <option {{ $bangunan->t_atap == 'GTG BETON/ALUMUNIUM' ? 'selected' : '' }}
                                            value="GTG BETON/ALUMUNIUM"> Gtg Beton/Alumunium</option>
                                        <option {{ $bangunan->t_atap == 'GTG BIASA/SIRAP' ? 'selected' : '' }}
                                            value="GTG BIASA/SIRAP"> Gtg Biasa/Sirap</option>
                                        <option {{ $bangunan->t_atap == 'ASBES' ? 'selected' : '' }} value="ASBES">
                                            Asbes</option>
                                        <option {{ $bangunan->t_atap == 'SENG' ? 'selected' : '' }} value="SENG"> Seng
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-4 col-form-label-sm">Dinding <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-4 ">
                                    <select class="form-control form-control-sm" name="dinding[{{$i}}]" required>
                                        <option value=""> ---Silahkan Pilih---</option>
                                        <option {{ $bangunan->t_dinding == 'KACA/ALUMUNIUM' ? 'selected' : '' }}
                                            value="KACA/ALUMUNIUM"> Kaca/Alumunium</option>
                                        <option {{ $bangunan->t_dinding == 'BETON' ? 'selected' : '' }}
                                            value="BETON"> Beton</option>
                                        <option {{ $bangunan->t_dinding == 'BATU BATA/CONBLOK' ? 'selected' : '' }}
                                            value="BATU BATA/CONBLOK"> Batu Bata/Conblok</option>
                                        <option {{ $bangunan->t_dinding == 'KAYU' ? 'selected' : '' }}
                                            value="KAYU"> Kayu</option>
                                        <option {{ $bangunan->t_dinding == 'SENG' ? 'selected' : '' }}
                                            value="SENG"> Seng</option>
                                        <option {{ $bangunan->t_dinding == 'TIDAK' ? 'selected' : '' }}
                                            value="TIDAK ADA"> Tidak Ada</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group-sm row">
                                <label class="col-sm-4 col-form-label-sm">Lantai <span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-4 ">
                                    <select class="form-control form-control-sm" name="lantai[{{$i}}]">
                                        <option value=""> ---Silahkan Pilih---</option>
                                        <option {{ $bangunan->t_lantai == 'MARMER' ? 'selected' : '' }} value="MARMER">
                                            Marmer</option>
                                        <option {{ $bangunan->t_lantai == 'KERAMIK' ? 'selected' : '' }}
                                            value="KERAMIK"> Keramik</option>
                                        <option {{ $bangunan->t_lantai == 'TERASO' ? 'selected' : '' }} value="TERASO">
                                            Teraso</option>
                                        <option {{ $bangunan->t_lantai == 'UBIN PC/PAPAN' ? 'selected' : '' }} value="UBIN
                                            PC/PAPAN"> Ubin PC/Papan</option>
                                        <option {{ $bangunan->t_lantai == 'SEMEN' ? 'selected' : '' }} value="SEMEN">
                                            Semen</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-4 col-form-label-sm">Langit-Langit<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-4 ">
                                    <select class="form-control form-control-sm" name="langitLangit[{{$i}}]">
                                        <option value=""> ---Silahkan Pilih---</option>
                                        <option {{ $bangunan->t_langit_langit == 'AKUSTIK/JATI' ? 'selected' : '' }}
                                            value="AKUSTIK/JATI"> Akustik/Jati</option>
                                        <option {{ $bangunan->t_langit_langit == 'TRIPLEK/ASBES BAMBU' ? 'selected' : ''
                                            }} value="TRIPLEK/ASBES BAMBU"> Triplek/Asbes/Bambu</option>
                                        <option {{ $bangunan->t_langit_langit == 'TIDAK ADA' ? 'selected' : '' }}
                                            value="TIDAK ADA"> Tidak Ada</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <p style="font-weight: bold">FASILITAS</p>
                            <hr>
                            <div class="form-group-sm row">
                                <label class="col-sm-4 col-form-label-sm">Jumlah AC SPlit</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control form-control-sm" name="acSplit[{{$i}}]'}"
                                        onkeypress="return onlyNumberKey(event)" value="{{ $bangunan->t_ac_split }}">
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-4 col-form-label-sm">Jumlah AC Window</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control form-control-sm" id="acWindow"
                                        onkeypress="return onlyNumberKey(event)" name="acWindow[{{$i}}]" value="{{ $bangunan->t_ac_window }}">
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-4 col-form-label-sm">Panjang Pagar (M)</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control form-control-sm" name="panjangPagar[{{$i}}]"
                                        onkeypress="return onlyNumberKey(event)" value="{{ $bangunan->t_panjang_pagar }}">
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-4 col-form-label-sm">Bahan Pagar</label>
                                <div class="col-sm-3">
                                    <select class="form-control form-control-sm" name="bahanPagar[{{ $i }}]">
                                        <option value=""> ---Silahkan Pilih---</option>
                                        <option {{ $bangunan->t_bahan_pagar == 'BAJA/BESI' ? 'selected' : ''}}
                                            value="BAJA/BESI"> Baja/Besi</option>
                                        <option {{ $bangunan->t_bahan_pagar == 'BATA/BATAKO' ? 'selected' : ''}}
                                            value="BATA/BATAKO"> Bata/Batako</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group-sm row">
                                <label class="col-sm-4 col-form-label-sm">Daya Listrik(VA)</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control form-control-sm" id="dayaListrik"
                                        onkeypress="return onlyNumberKey(event)" name="dayaListrik[{{ $i }}]" value="{{ $bangunan->t_listrik ?? ''}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

    </div>
</div>
