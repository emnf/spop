<div class="form-group">
    <label>No. SHM/Surat Ukur/Dokumen Tanah<span class="text-danger">*</span></label>
    <input type="text" class="form-control col-md-4" name="noSHM" required>
</div>
<div class="form-group">
    <label>Tgl. SHM/Surat Ukur/Dokumen Tanah<span class="text-danger">*</span></label>
    <input type="text" class="form-control col-md-4 datepicker-date" name="tglSHM" required readonly>
</div>
<div class="form-group">
    <label>Luas Tanah di SHM/Surat Ukur/Dokumen Tanah</label>
    <input type="text" class="form-control col-md-4" id="luasTanahSHM" name="luasTanahSHM" required readonly>
</div>
<div class="form-group">
    <label>Atas Nama di SHM/Surat Ukur/Dokumen Tanah</label>
    <input type="text" class="form-control col-md-4" id="atasNamaSHM" name="atasNamaSHM" required readonly>
</div>

