@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Dashboard
@endsection

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>


    <div class="container-fluid">

        <div class="row">
            <div class="col-md-3 col-sm-6 col-12">
                <a href="#" class="text_black">
                    <div class="info-box">
                        <span class="info-box-icon bg-info elevation-1"><i class="fas fa-hands-helping"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Diajukan</span>
                            <span class="info-box-number">{{ $totalPelayanan }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </a>
                <!-- /.info-box -->
            </div>
            <div class="col-md-3 col-sm-6 col-12">
                <a href="#" class="text_black">
                    <div class="info-box">
                        <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-archive"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Diverifikasi</span>
                            <span class="info-box-number">{{ $totalVerifikasi }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </a>
                <!-- /.info-box -->
            </div>

            {{-- <div class="col-md-3 col-sm-6 col-12">
            <a href="#" class="text_black">
                <div class="info-box">
                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-search"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Diverifikasi Lapangan</span>
                        <span class="info-box-number">{{ $totalVerlap }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div> --}}
            <div class="col-md-3 col-sm-6 col-12">
                <a href="#" class="text_black">
                    <div class="info-box">
                        <span class="info-box-icon bg-success elevation-1"><i class="fas fa-clipboard-check"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Divalidasi</span>
                            <span class="info-box-number">{{ $totalValidasi }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </a>
                <!-- /.info-box -->
            </div>
            <div class="col-md-3 col-sm-6 col-12">
                <a href="#" class="text_black">
                    <div class="info-box">
                        <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-check-circle"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Pelayanan Selesai</span>
                            <span class="info-box-number">{{ $totalHasilPelayanan }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </a>
                <!-- /.info-box -->
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Data Grafik</h5>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            {{-- <div class="btn-group">
                            <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                                <i class="fas fa-wrench"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" role="menu">
                                <a href="#" class="dropdown-item">Action</a>
                                <a href="#" class="dropdown-item">Another action</a>
                                <a href="#" class="dropdown-item">Something else here</a>
                                <a class="dropdown-divider"></a>
                                <a href="#" class="dropdown-item">Separated link</a>
                            </div>
                        </div> --}}
                            {{-- <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button> --}}
                        </div>
                    </div>
                    {{-- <!-- /.card-header --> --}}
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <p class="text-center">
                                    <strong>1 Januari s/d 31 Desember <span>{{ date('Y') }}</span></strong>
                                </p>

                                <div class="chart">
                                    {{-- <!-- Sales Chart Canvas --> --}}
                                    <canvas id="areaChart" height="180" style="height: 180px;"></canvas>
                                </div>
                                {{-- <!-- /.chart-responsive --> --}}
                            </div>
                            {{-- <!-- /.col --> --}}
                            <div class="col-md-4">
                                <p class="text-center">
                                    <strong>Data Layanan Periode <span>{{ date('Y') }}</span></strong>
                                </p>

                                <div class="progress-group">
                                    <span class="progress-text">Diajukan</span>
                                    <span class="float-right"><b>{{ $totalAjuanPerTahun }}</b></span>
                                    <div class="progress progress-sm">
                                        <div class="progress-bar bg-primary" style="width: 100%"></div>
                                    </div>
                                </div>
                                <!-- /.progress-group -->

                                <div class="progress-group">
                                    <span class="progress-text">Diverifikasi</span>
                                    <span class="float-right"><b>{{ $totalVerifikasiPerTahun }}</b></span>
                                    <div class="progress progress-sm">
                                        <div class="progress-bar bg-danger" style="width: 100%"></div>
                                    </div>
                                </div>

                                <!-- /.progress-group -->
                                <div class="progress-group">
                                    <span class="progress-text">Divalidasi</span>
                                    <span class="float-right"><b>{{ $totalValidasiPerTahun }}</b></span>
                                    <div class="progress progress-sm">
                                        <div class="progress-bar bg-success" style="width: 100%"></div>
                                    </div>
                                </div>

                                <!-- /.progress-group -->
                                <div class="progress-group">
                                    <span class="progress-text">Pelayanan Selesai</span>
                                    <span class="float-right"><b>{{ $totalHasilPelayananPerTahun }}</b></span>
                                    <div class="progress progress-sm">
                                        <div class="progress-bar bg-warning" style="width: 100%"></div>
                                    </div>
                                </div>
                                <!-- /.progress-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- ./card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>

        <div hidden class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="car-header p-2">
                        Keterangan:
                        <span class="bg-info" style="padding:0px 10px 0px 10px;"></span> [ Diajukan ]
                        <span class="bg-danger" style="padding:0px 10px 0px 10px;"></span> [ Diverifikasi ]
                        <span class="bg-success" style="padding:0px 10px 0px 10px;"></span> [ Divalidasi ]
                        <span class="bg-warning" style="padding:0px 10px 0px 10px;"></span> [ Pelayanan Selesai ]
                    </div>
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover table-bordered table-striped text-nowrap"
                            style="font-size: 12px;">
                            <thead>
                                <tr>
                                    <th rowspan="2"
                                        style="vertical-align: middle; width: 10px; background-color: #1fc8e3; text-align: center">
                                        No.</th>
                                    <th rowspan="2"
                                        style="vertical-align: middle; background-color: #1fc8e3; text-align: center">Jenis
                                        Pajak</th>
                                    <th colspan="12"
                                        style="vertical-align: middle; background-color: #1fc8e3; text-align: center">Bulan
                                        Pelayanan Pajak Tahun <span></span></th>
                                    <th rowspan="2"
                                        style="vertical-align: middle; background-color: #1fc8e3; text-align: center; width: 200px">
                                        Jumlah</th>
                                </tr>
                                <tr>
                                    <th style="vertical-align: top; background-color: #1fc8e3; text-align: center">Januari
                                    </th>
                                    <th style="vertical-align: top; background-color: #1fc8e3; text-align: center">Februari
                                    </th>
                                    <th style="vertical-align: top; background-color: #1fc8e3; text-align: center">Maret
                                    </th>
                                    <th style="vertical-align: top; background-color: #1fc8e3; text-align: center">April
                                    </th>
                                    <th style="vertical-align: top; background-color: #1fc8e3; text-align: center">Mei</th>
                                    <th style="vertical-align: top; background-color: #1fc8e3; text-align: center">Juni
                                    </th>
                                    <th style="vertical-align: top; background-color: #1fc8e3; text-align: center">Juli
                                    </th>
                                    <th style="vertical-align: top; background-color: #1fc8e3; text-align: center">Agustus
                                    </th>
                                    <th style="vertical-align: top; background-color: #1fc8e3; text-align: center">
                                        September
                                    </th>
                                    <th style="vertical-align: top; background-color: #1fc8e3; text-align: center">Oktober
                                    </th>
                                    <th style="vertical-align: top; background-color: #1fc8e3; text-align: center">November
                                    </th>
                                    <th style="vertical-align: top; background-color: #1fc8e3; text-align: center">Desember
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="text-align: center"><span></span></td>
                                    <td>
                                        <span></span>
                                    </td>
                                    <td style="text-align: right"><span class="text-info"></span><br>
                                        <span class="text-danger"></span><br>
                                        <span class="text-success"></span><br>
                                        <span class="text-orange"></span>
                                    </td>
                                    <td style="text-align: right"><span class="text-info"></span><br>
                                        <span class="text-danger"></span><br>
                                        <span class="text-success"></span><br>
                                        <span class="text-orange"></span>
                                    </td>
                                    <td style="text-align: right"><span class="text-info"></span><br>
                                        <span class="text-danger"></span><br>
                                        <span class="text-success"></span><br>
                                        <span class="text-orange"></span>
                                    </td>
                                    <td style="text-align: right"><span class="text-info"></span><br>
                                        <span class="text-danger"></span><br>
                                        <span class="text-success"></span><br>
                                        <span class="text-orange"></span>
                                    </td>
                                    <td style="text-align: right"><span class="text-info"></span><br>
                                        <span class="text-danger"></span><br>
                                        <span class="text-success"></span><br>
                                        <span class="text-orange"></span>
                                    </td>
                                    <td style="text-align: right"><span class="text-info"></span><br>
                                        <span class="text-danger"></span><br>
                                        <span class="text-success"></span><br>
                                        <span class="text-orange"></span>
                                    </td>
                                    <td style="text-align: right"><span class="text-info"></span><br>
                                        <span class="text-danger"></span><br>
                                        <span class="text-success"></span><br>
                                        <span class="text-orange"></span>
                                    </td>
                                    <td style="text-align: right"><span class="text-info"></span><br>
                                        <span class="text-danger"></span><br>
                                        <span class="text-success"></span><br>
                                        <span class="text-orange"></span>
                                    </td>
                                    <td style="text-align: right"><span class="text-info"></span><br>
                                        <span class="text-danger"></span><br>
                                        <span class="text-success"></span><br>
                                        <span class="text-orange"></span>
                                    </td>
                                    <td style="text-align: right"><span class="text-info"></span><br>
                                        <span class="text-danger"></span><br>
                                        <span class="text-success"></span><br>
                                        <span class="text-orange"></span>
                                    </td>
                                    <td style="text-align: right"><span class="text-info"></span><br>
                                        <span class="text-danger"></span><br>
                                        <span class="text-success"></span><br>
                                        <span class="text-orange"></span>
                                    </td>
                                    <td style="text-align: right"><span class="text-info"></span><br>
                                        <span class="text-danger"></span><br>
                                        <span class="text-success"></span><br>
                                        <span class="text-orange"></span>
                                    </td>
                                    <td style="text-align: right"><strong class="text-info"></strong><br>
                                        <strong class="text-danger"></strong><br>
                                        <strong class="text-success"></strong><br>
                                        <span class="text-orange"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"><strong>TOTAL PELAYANAN</strong></td>
                                    <td style="text-align: right">
                                        <strong text="{{ $Januari['totPengajuanPelayanan'] }}"></strong>
                                        <input type="hidden" id="layananJan"
                                            value="{{ $Januari['totPengajuanPelayanan'] }}">
                                        <input type="hidden" id="verifikasiJan"
                                            value="{{ $Januari['totVerifikasiPelayanan'] }}">
                                        <input type="hidden" id="validasiJan"
                                            value="{{ $Januari['totValidasiPelayanan'] }}">
                                        <input type="hidden" id="selesaiJan"
                                            value="{{ $Januari['totHasilPelayanan'] }}">
                                    </td>
                                    <td style="text-align: right">
                                        <strong text="{{ $Februari['totPengajuanPelayanan'] }}"></strong>
                                        <input type="hidden" id="layananFeb"
                                            value="{{ $Februari['totPengajuanPelayanan'] }}">
                                        <input type="hidden" id="verifikasiFeb"
                                            value="{{ $Februari['totVerifikasiPelayanan'] }}">
                                        <input type="hidden" id="validasiFeb"
                                            value="{{ $Februari['totValidasiPelayanan'] }}">
                                        <input type="hidden" id="selesaiFeb"
                                            value="{{ $Februari['totHasilPelayanan'] }}">
                                    </td>
                                    <td style="text-align: right">
                                        <strong text="{{ $Maret['totPengajuanPelayanan'] }}"></strong>
                                        <input type="hidden" id="layananMar"
                                            value="{{ $Maret['totPengajuanPelayanan'] }}">
                                        <input type="hidden" id="verifikasiMar"
                                            value="{{ $Maret['totVerifikasiPelayanan'] }}">
                                        <input type="hidden" id="validasiMar"
                                            value="{{ $Maret['totValidasiPelayanan'] }}">
                                        <input type="hidden" id="selesaiMar" value="{{ $Maret['totHasilPelayanan'] }}">
                                    </td>
                                    <td style="text-align: right">
                                        <strong text="{{ $April['totPengajuanPelayanan'] }}"></strong>
                                        <input type="hidden" id="layananApr"
                                            value="{{ $April['totPengajuanPelayanan'] }}">
                                        <input type="hidden" id="verifikasiApr"
                                            value="{{ $April['totVerifikasiPelayanan'] }}">
                                        <input type="hidden" id="validasiApr"
                                            value="{{ $April['totValidasiPelayanan'] }}">
                                        <input type="hidden" id="selesaiApr" value="{{ $April['totHasilPelayanan'] }}">
                                    </td>
                                    <td style="text-align: right">
                                        <strong text="{{ $Mei['totPengajuanPelayanan'] }}"></strong>
                                        <input type="hidden" id="layananMei"
                                            value="{{ $Mei['totPengajuanPelayanan'] }}">
                                        <input type="hidden" id="verifikasiMei"
                                            value="{{ $Mei['totVerifikasiPelayanan'] }}">
                                        <input type="hidden" id="validasiMei"
                                            value="{{ $Mei['totValidasiPelayanan'] }}">
                                        <input type="hidden" id="selesaiMei" value="{{ $Mei['totHasilPelayanan'] }}">
                                    </td>
                                    <td style="text-align: right">
                                        <strong text="{{ $Juni['totPengajuanPelayanan'] }}"></strong>
                                        <input type="hidden" id="layananJun"
                                            value="{{ $Juni['totPengajuanPelayanan'] }}">
                                        <input type="hidden" id="verifikasiJun"
                                            value="{{ $Juni['totVerifikasiPelayanan'] }}">
                                        <input type="hidden" id="validasiJun"
                                            value="{{ $Juni['totValidasiPelayanan'] }}">
                                        <input type="hidden" id="selesaiJun" value="{{ $Juni['totHasilPelayanan'] }}">
                                    </td>
                                    <td style="text-align: right">
                                        <strong text="{{ $Juli['totPengajuanPelayanan'] }}"></strong>
                                        <input type="hidden" id="layananJul"
                                            value="{{ $Juli['totPengajuanPelayanan'] }}">
                                        <input type="hidden" id="verifikasiJul"
                                            value="{{ $Juli['totVerifikasiPelayanan'] }}">
                                        <input type="hidden" id="validasiJul"
                                            value="{{ $Juli['totValidasiPelayanan'] }}">
                                        <input type="hidden" id="selesaiJul" value="{{ $Juli['totHasilPelayanan'] }}">
                                    </td>
                                    <td style="text-align: right">
                                        <strong text="{{ $Agustus['totPengajuanPelayanan'] }}"></strong>
                                        <input type="hidden" id="layananAgus"
                                            value="{{ $Agustus['totPengajuanPelayanan'] }}">
                                        <input type="hidden" id="verifikasiAgus"
                                            value="{{ $Agustus['totVerifikasiPelayanan'] }}">
                                        <input type="hidden" id="validasiAgus"
                                            value="{{ $Agustus['totValidasiPelayanan'] }}">
                                        <input type="hidden" id="selesaiAgus"
                                            value="{{ $Agustus['totHasilPelayanan'] }}">
                                    </td>
                                    <td style="text-align: right">
                                        <strong text="{{ $September['totPengajuanPelayanan'] }}"></strong>
                                        <input type="hidden" id="layananSept"
                                            value="{{ $September['totPengajuanPelayanan'] }}">
                                        <input type="hidden" id="verifikasiSept"
                                            value="{{ $September['totVerifikasiPelayanan'] }}">
                                        <input type="hidden" id="validasiSept"
                                            value="{{ $September['totValidasiPelayanan'] }}">
                                        <input type="hidden" id="selesaiSept"
                                            value="{{ $September['totHasilPelayanan'] }}">
                                    </td>
                                    <td style="text-align: right">
                                        <strong text="{{ $Oktober['totPengajuanPelayanan'] }}"></strong>
                                        <input type="hidden" id="layananOkt"
                                            value="{{ $Oktober['totPengajuanPelayanan'] }}">
                                        <input type="hidden" id="verifikasiOkt"
                                            value="{{ $Oktober['totVerifikasiPelayanan'] }}">
                                        <input type="hidden" id="validasiOkt"
                                            value="{{ $Oktober['totValidasiPelayanan'] }}">
                                        <input type="hidden" id="selesaiOkt"
                                            value="{{ $Oktober['totHasilPelayanan'] }}">
                                    </td>
                                    <td style="text-align: right">
                                        <strong text="{{ $November['totPengajuanPelayanan'] }}"></strong>
                                        <input type="hidden" id="layananNov"
                                            value="{{ $November['totPengajuanPelayanan'] }}">
                                        <input type="hidden" id="verifikasiNov"
                                            value="{{ $November['totVerifikasiPelayanan'] }}">
                                        <input type="hidden" id="validasiNov"
                                            value="{{ $November['totValidasiPelayanan'] }}">
                                        <input type="hidden" id="selesaiNov"
                                            value="{{ $November['totHasilPelayanan'] }}">
                                    </td>
                                    <td style="text-align: right">
                                        <strong text="{{ $Desember['totPengajuanPelayanan'] }}"></strong>
                                        <input type="hidden" id="layananDes"
                                            value="{{ $Desember['totPengajuanPelayanan'] }}">
                                        <input type="hidden" id="verifikasiDes"
                                            value="{{ $Desember['totVerifikasiPelayanan'] }}">
                                        <input type="hidden" id="validasiDes"
                                            value="{{ $Desember['totValidasiPelayanan'] }}">
                                        <input type="hidden" id="selesaiDes"
                                            value="{{ $Desember['totHasilPelayanan'] }}">
                                    </td>
                                    <td style="text-align: right">
                                        <strong text="{{ $totalAjuanPerTahun }}"></strong>
                                        <input type="hidden" id="layananSatuTahun" value="{{ $totalAjuanPerTahun }}">
                                        <input type="hidden" id="verifikasiSatuTahun"
                                            value="{{ $totalVerifikasiPerTahun }}">
                                        <input type="hidden" id="validasiSatuTahun"
                                            value="{{ $totalValidasiPerTahun }}">
                                        <input type="hidden" id="selesaiSatuTahun"
                                            value="{{ $totalHasilPelayananPerTahun }}">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!--        AKSES WP-->
        {{-- <div th:if="${#strings.capitalize(#authentication.getPrincipal().getTipeUser())}==2">
        <div class="row">
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header p-2">
                        <ul class="nav nav-pills">
                            <li class="nav-item"><a class="nav-link active" href="#activity"
                                    data-toggle="tab">Informasi</a></li>
                            <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Hasil
                                    Pelayanan</a></li>
                        </ul>
                    </div><!-- /.card-header -->
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="active tab-pane" id="activity">
                                <!-- Post -->
                                <div class="post" th:each="infoArr, key: ${dataInformasiList}">
                                    <div class="user-block">
                                        <img class="img-circle img-bordered-sm"
                                            src="{{ asset('asset/images/elayanan3.png') }}" alt="user image">
                                        <span class="username">
                                            <a href="#"><span></span></a>
                                            <a href="#" class="float-right btn-tool"></a>
                                        </span>
                                        <span class="description">Minggu, 7 Juni 2020 - 7:30 PM</span>
                                    </div>
                                    <!-- /.user-block -->
                                    <p>
                                        <span th:text="${infoArr.pesan}"></span><br>
                                        <span th:text="${'No Pelayanan : ' + infoArr.noPelayanan}"></span><br>
                                        <span th:text="${'Keterangan : ' + infoArr.keterangan}"></span><br>
                                    </p>
                                </div>
                                <!-- /.post -->
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="timeline">
                                <!-- The timeline -->
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h3 class="card-title">Data Pengajuan</h3>

                                                <div class="card-tools">
                                                    <a href="javascript:printDaftarPengajuan('PDF')"
                                                        class="btn btn-danger btn-sm"><span
                                                            class="fas fa-file-pdf"></span> &nbsp;PDF</a>
                                                    <a href="javascript:printDaftarPengajuan('XLS')"
                                                        class="btn btn-success btn-sm"><span
                                                            class="fas fa-file-excel"></span> &nbsp;XLS</a>
                                                </div>
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body table-responsive p-0">
                                                <table
                                                    class="table table-hover table-bordered table-striped text-nowrap"
                                                    id="pelayanan-table" style="font-size: 10pt;">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 10px; background-color: #1fc8e3">No</th>
                                                            <th style="width: 10px; background-color: #1fc8e3">Jenis
                                                                Pajak</th>
                                                            <th style="width: 10px; background-color: #1fc8e3">Jenis
                                                                Layanan</th>
                                                            <th style="width: 10px; background-color: #1fc8e3">Tanggal
                                                            </th>
                                                            <th style="width: 10px; background-color: #1fc8e3">Status
                                                            </th>
                                                            <th style="width: 10px; background-color: #1fc8e3">Alasan
                                                            </th>
                                                            <th style="width: 10px; background-color: #1fc8e3">Action
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <th></th>
                                                            <th>
                                                                <select class="form-control form-control-sm"
                                                                    id="filter-idJenisPajak">
                                                                    <option value="">Silahkan Pilih</option>
                                                                    <option th:each="select: ${dataPajak}"
                                                                        th:text="${select.namaJenisPajak}"
                                                                        th:value="${select.idJenisPajak}"></option>
                                                                </select>
                                                            </th>
                                                            <th><input class="form-control form-control-sm"
                                                                    id="filter-namaJenisPelayanan" /></th>
                                                            <th><input class="form-control form-control-sm"
                                                                    id="filter-tglPelayanan" readonly="true" /></th>
                                                            <th>
                                                                <select class="form-control form-control-sm"
                                                                    id="filter-status">
                                                                    <option value="">Silahkan Pilih</option>
                                                                    <option value="1">Draft</option>
                                                                    <option value="2">Proses</option>
                                                                    <option value="3">Ditolak</option>
                                                                    <option value="4">Diterima</option>
                                                                </select>
                                                            </th>
                                                            <th><input class="form-control form-control-sm"
                                                                    id="filter-keterangan" /></th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="text-center" colspan="10"> Tidak ada data.</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.card-body -->
                                            <div class="card-footer clearfix pagination-footer">

                                            </div>
                                        </div>
                                        <!-- /.card -->
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-pane -->

                            <div class="tab-pane" id="settings">
                                <form class="form-horizontal">
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputName" placeholder="Name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputEmail"
                                                placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputName2" class="col-sm-2 col-form-label">Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputName2" placeholder="Name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputExperience" class="col-sm-2 col-form-label">Experience</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" id="inputExperience"
                                                placeholder="Experience"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputSkills" class="col-sm-2 col-form-label">Skills</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputSkills"
                                                placeholder="Skills">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="offset-sm-2 col-sm-10">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> I agree to the <a href="#">terms and
                                                        conditions</a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="offset-sm-2 col-sm-10">
                                            <button type="submit" class="btn btn-danger">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div><!-- /.card-body -->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->
            <div class="col-md-3" hidden>
                <!-- Profile Image -->
                <div class="card card-danger card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle" src="/static/images/user1-128x128.jpg"
                                alt="User profile picture">
                        </div>

                        <h3 class="profile-username text-center"><span
                                th:text="${#strings.capitalize(#authentication.getPrincipal().getNama())}"></span>
                            <br><span style="color:gray;font-size:9pt;"
                                th:text="${#strings.capitalize(#authentication.getPrincipal().getUsername())}">Web
                                Developer</span>
                        </h3>

                        <hr>

                        <strong><i class="fas fa-mail-bulk mr-1"></i> Email</strong>
                        <p class="text-muted">anonymous@gmail.com</p>
                        <hr>

                        <strong><i class="fas fa-map-marker-alt mr-1"></i>Alamat</strong>
                        <p class="text-muted">Jl Pahlawan No 28, Tangsel</p>
                        <hr>

                        <a href="#" class="btn btn-danger btn-block btn-sm"><b>Edit Profile</b></a>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
    </div> --}}


    </div>
@endsection
@push('styles')
    <style>
        .card-selamat {
            color: white;
            height: 150px;
            background: linear-gradient(to right, rgba(220, 53, 69, 0.9), rgba(209, 70, 70, 0.3)),
                /* url('/static/images/bg4.jpg') */
                no-repeat center center;
            min-width: 601px;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>
@endpush

@push('scripts')
    <script th:inline="javascript" th:if="${#strings.capitalize(#authentication.getPrincipal().getTipeUser())}!=2">
        $(function() {
            /* ChartJS
             * -------
             * Here we will create a few charts using ChartJS
             */

            //--------------
            //- AREA CHART -
            //--------------

            // Get context with jQuery - using jQuery's .get() method.
            var areaChartCanvas = $('#areaChart').get(0).getContext('2d')

            var areaChartData = {
                labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agust', 'Sept', 'Okt', 'Nov', 'Des'],
                datasets: [{
                        label: 'Pelayanan',
                        backgroundColor: 'rgba(60,141,188,0.9)',
                        borderColor: 'rgba(60,141,188,0.8)',
                        pointRadius: false,
                        pointColor: '#3b8bba',
                        pointStrokeColor: 'rgba(60,141,188,1)',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(60,141,188,1)',
                        data: [$('#layananJan').val(), $('#layananFeb').val(), $('#layananMar').val(), $(
                                '#layananApr').val(), $('#layananMei').val(), $('#layananJun').val(), $(
                                '#layananJul').val(), $('#layananAgus').val(), $('#layananSept').val(),
                            $('#layananOkt').val(), $('#layananNov').val(), $('#layananDes').val()
                        ]
                    },
                    {
                        label: 'Verifikasi',
                        backgroundColor: 'rgba(209,15,34,0.9)',
                        borderColor: 'rgba(209,15,34,0.8)',
                        pointRadius: false,
                        pointColor: '#d10f22',
                        pointStrokeColor: 'rgba(209,15,34,1)',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(209,15,34,1)',
                        data: [$('#verifikasiJan').val(), $('#verifikasiFeb').val(), $('#verifikasiMar')
                            .val(), $('#verifikasiApr').val(), $('#verifikasiMei').val(), $(
                                '#verifikasiJun').val(), $('#verifikasiJul').val(), $('#verifikasiAgus')
                            .val(), $('#verifikasiSept').val(), $('#verifikasiOkt').val(), $(
                                '#verifikasiNov').val(), $('#verifikasiDes').val()
                        ]
                    },
                    {
                        label: 'Validasi',
                        backgroundColor: 'rgba(40, 167, 69,0.9)',
                        borderColor: 'rgba(40, 167, 69,0.8)',
                        pointRadius: false,
                        pointColor: '#28a745',
                        pointStrokeColor: 'rgba(40, 167, 69,1)',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(40, 167, 69,1)',
                        data: [$('#validasiJan').val(), $('#validasiFeb').val(), $('#validasiMar').val(), $(
                                '#validasiApr').val(), $('#validasiMei').val(), $('#validasiJun').val(),
                            $('#validasiJul').val(), $('#validasiAgus').val(), $('#validasiSept').val(),
                            $('#validasiOkt').val(), $('#validasiNov').val(), $('#validasiDes').val()
                        ]
                    },
                    {
                        label: 'Selesai',
                        backgroundColor: 'rgba(255, 193, 7,0.9)',
                        borderColor: 'rgba(255, 193, 7,0.8)',
                        pointRadius: false,
                        pointColor: '#ffc107',
                        pointStrokeColor: 'rgba(255, 193, 7,1)',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(255, 193, 7,1)',
                        data: [$('#selesaiJan').val(), $('#selesaiFeb').val(), $('#selesaiMar').val(), $(
                                '#selesaiApr').val(), $('#selesaiMei').val(), $('#selesaiJun').val(), $(
                                '#selesaiJul').val(), $('#selesaiAgus').val(), $('#selesaiSept').val(),
                            $('#selesaiOkt').val(), $('#selesaiNov').val(), $('#selesaiDes').val()
                        ]
                    },
                ]
            }

            var areaChartOptions = {
                maintainAspectRatio: false,
                responsive: true,
                legend: {
                    display: false
                },
                scales: {
                    x: {
                        grid: {
                            offset: true
                        }
                    },
                    y: {
                        grid: {
                            offset: true
                        }
                    }
                }
            }

            // This will get the first returned node in the jQuery collection.
            var areaChart = new Chart(areaChartCanvas, {
                type: 'bar',
                data: areaChartData,
                options: areaChartOptions
            })

        })
    </script>
    <th:block th:if="${#strings.capitalize(#authentication.getPrincipal().getTipeUser())}==2">
        <script th:inline="javascript">
            $('#filter-tglPelayanan').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('#filter-tglPelayanan').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
                search();
            });

            $('#filter-tglPelayanan').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });

            $("#filter-idJenisPajak").change(function() {
                search();
            });

            $("#filter-status").change(function() {
                search();
            });

            $("#filter-namaJenisPelayanan").keyup(function() {
                search();
            });

            $("#filter-keterangan").keyup(function() {
                search();
            });

            // var datatables = datagrid({
            //     url: '/dashboard/datagrid',
            //     table: "#pelayanan-table",
            //     columns: [
            //         {class: ""},
            //         {class: ""},
            //         {class: "text-center"},
            //         {class: "text-center"},
            //         {class: ""},
            //         {class: "text-center"},
            //     ],
            //     orders: [
            //         {sortable: false},
            //         {sortable: true, name: "idJenisPajak"},
            //         {sortable: true, name: "namaJenisPelayanan"},
            //         {sortable: true, name: "tglPelayanan"},
            //         {sortable: false},
            //         {sortable: false},
            //         {sortable: false},
            //     ],
            //     action: [
            //         {
            //             name: 'download-sk',
            //             btnClass: 'btn btn-success btn-xs',
            //             btnText: '<i class="fas fa-download"> Hasil Pelayanan</i>'
            //         },
            //         {
            //             name: 'download-sk-disable',
            //             btnClass: 'btn btn-secondary btn-xs',
            //             btnText: '<i class="fas fa-download"> Hasil Pelayanan</i>'
            //         },
            //     ]

            // });

            // function search(){
            //     datatables.setFilters({
            //         idJenisPajak: $("#filter-idJenisPajak").val(),
            //         namaJenisPelayanan: $("#filter-namaJenisPelayanan").val(),
            //         keterangan: $("#filter-keterangan").val(),
            //         tglPelayanan: $("#filter-tglPelayanan").val(),
            //         status: $("#filter-status").val(),
            //     });
            //     datatables.reload();
            // }

            // search();

            function printDaftarPengajuan(type) {
                window.open('/report/printdaftarpengajuan' + type + '?idJenisPajak=' + $("#filter-idJenisPajak").val());
            }

            function printTandaTerima(a) {
                window.open('/report/printtandaterimapelayanan?idPelayanan=' + a);
            }

            function downloadsk(url, name) {
                var a = $("<a>").attr("href", url).attr("download", name).appendTo("body");
                a[0].click();
                a.remove();
            }
        </script>
    </th:block>
@endpush
