@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Ubah Password
@endsection

@section('content')
    <section class="content">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item active">Ubah Password</li>
                        </ol>
                    </div><!-- /.col -->
                    <div class="col-12">
                        <div class="info-box" id="background2">
                            <div class="col-sm-3">
                                <div class="description-block" style="float: left;">
                                    <h1 class="description-header">UBAH PASSWORD</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-outline card-info">
                            <form action="/ganti-password/save" method="POST">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="oldPassword">Password Lama</label>
                                        <input type="password" class="form-control" name="old_password" id="old_password" />
                                        @if ($errors->has('old_password'))
                                            <div class="text-danger mt-0 pl-1">
                                                {{ $errors->first('old_password') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="oldPassword">Password Baru</label>
                                        <input type="password" class="form-control" name="new_password" id="new_password" />
                                        @if ($errors->has('new_password'))
                                            <div class="text-danger mt-0 pl-1">
                                                {{ $errors->first('new_password') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="oldPassword">Password Baru Confirm</label>
                                        <input type="password" class="form-control" name="confirm_password"
                                            id="confirm_password" />
                                        @if ($errors->has('confirm_password'))
                                            <div class="text-danger mt-0 pl-1">
                                                {{ $errors->first('confirm_password') }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary float-right"><i class="fas fa-save"></i>
                                        Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
