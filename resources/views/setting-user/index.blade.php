@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Setting Pengguna
@endsection

@section('content')
    <section class="content">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Setting</a></li>
                            <li class="breadcrumb-item active">Grid</li>
                        </ol>
                    </div><!-- /.col -->
                    <div class="col-12">
                        <div class="info-box" id="background2">
                            <div class="col-sm-3">
                                <div class="description-block" style="float: left;">
                                    <h1 class="description-header">SETTING PENGGUNA</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-outline card-info">
                            <div class="card-header">
                                <div class="col-sm-12" style="margin-bottom: 10px;">
                                    <a href="{{ route('setting-user.tambah') }}" class="btn btn-info">Tambah</a>
                                </div>
                            </div>

                            <div class="card-body table-responsive">
                                <table class="table table-bordered table-striped table-hover" id="datagrid-table">
                                    <thead>
                                        <tr>
                                            <th style="background-color: #1fc8e3" class="text-center">No</th>
                                            <th style="background-color: #1fc8e3" class="text-center">Username</th>
                                            <th style="background-color: #1fc8e3" class="text-center">Nama</th>
                                            <th style="background-color: #1fc8e3" class="text-center">Tipe</th>
                                            <th style="background-color: #1fc8e3" class="text-center">Active</th>
                                            <th style="background-color: #1fc8e3" class="text-center">Foto</th>
                                            <th style="background-color: #1fc8e3" class="text-center">Action</th>
                                        </tr>
                                        <tr>
                                            <th></th>
                                            <th><input class="form-control form-control-sm" id="filter-s_username"
                                                    type="text" /></th>
                                            <th><input class="form-control form-control-sm" id="filter-s_nama"
                                                    type="text" /></th>
                                            <th>
                                                <select class="form-control form-control-sm" id="filter-s_tipe" required>
                                                    <option value="">Silahkan Pilih</option>
                                                    {{-- @foreach ($tipe as $row)
                                            <option value="{{ $row['s_id_jenis_pelayanan'] }}">{{ $row['s_nama_jenis_pelayanan'] }}</option>
                                        @endforeach --}}
                                                </select>
                                            </th>
                                            <th>
                                                <select class="form-control form-control-sm" id="filter-s_status" required>
                                                    <option value="">Silahkan Pilih</option>
                                                    <option value="ya">Ya</option>
                                                    <option value="tidak">Tidak</option>
                                                </select>
                                            </th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center" colspan="7"> Tidak ada data.</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer clearfix pagination-footer">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
@push('scripts')
    <script type="text/javascript">
        var datatables = datagrid({
            url: '/setting-user/datagrid',
            table: "#datagrid-table",
            serverSide: true,
            columns: [{
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
            ],
            orders: [{
                    sortable: false
                },
                {
                    sortable: true,
                    name: "s_username"
                },
                {
                    sortable: true,
                    name: "s_nama"
                },
                {
                    sortable: true,
                    name: "s_tipe"
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                }
            ],
            action: [{
                    name: 'edit',
                    btnClass: 'btn btn-outline-primary btn-sm mx-1',
                    btnText: '<i class="fas fa-pen"></i>'
                },
                {
                    name: 'delete',
                    btnClass: 'btn btn-outline-danger btn-sm mx-1',
                    btnText: '<i class="fas fa-trash"></i>'
                }
            ]

        });

        $("#filter-s_nama_persyaratan").keyup(function() {
            search();
        });

        $("#filter-s_id_jenis_pelayanan").change(function() {
            search();
        });

        function search() {
            datatables.setpageNumber(0);
            datatables.setFilters({
                s_nama_persyaratan: $("#filter-s_nama_persyaratan").val(),
                s_id_jenis_pelayanan: $("#filter-s_id_jenis_pelayanan").val()
            });
            datatables.reload();
        }
        search();

        function showDeleteDialog(a) {
            Swal.fire({
                title: 'Hapus Data',
                text: "Apa anda Yakin ingin menghapus ?",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Tidak',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '/setting-persyaratan/delete/' + a,
                        type: 'delete',
                        dataType: "JSON",
                        data: {
                            s_id_persyaratan: a
                        },
                        success: function(request, error) {
                            if (request == 'success') {
                                Toast.fire({
                                    icon: 'success',
                                    title: 'Data Berhasil Dihapus'
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: 'Data Gagal Dihapus'
                                });
                            }
                            datatables.reload();
                        }
                    });
                }
            })
        }

        function cetakData(a) {
            window.open(
                `{{ url('setting/pegawai/cetak-daftar') }}?type-cetak=${a}`
            );
        }
    </script>
@endPush
