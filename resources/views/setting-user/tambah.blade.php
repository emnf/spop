@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Setting Pengguna
@endsection

@section('content')
    <section class="content">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/setting-user">Setting</a></li>
                            <li class="breadcrumb-item active">Pengguna</li>
                        </ol>
                    </div><!-- /.col -->
                    <div class="col-12">
                        <div class="info-box" id="background2">
                            <div class="col-sm-3">
                                <div class="description-block" style="float: left;">
                                    <h1 class="description-header">TAMBAH PENGGUNA</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-outline card-info">
                            <form action="/setting-user/save" method="POST">
                                @csrf
                                <div class="card-body">
                                    @include('setting-user.form')
                                </div>
                                <div class="card-footer">
                                    <button type="button" class="btn btn-warning" onclick="history.back()"><i
                                            class="fas fa-ban"></i> Batal</button>
                                    <button type="submit" class="btn btn-primary float-right"><i class="fas fa-save"></i>
                                        Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
