<div class="form-group">
    <label for="">Nama</label>
    <input type="text" class="form-control" name="name" id="name" value="{{ old('name') ?? ($user->name ?? '') }}"
        required />
    @if ($errors->has('name'))
        <div class="text-danger mt-0 pl-1">
            {{ $errors->first('name') }}
        </div>
    @endif
</div>
<div class="form-group">
    <label for="">Username</label>
    <input type="text" class="form-control" name="username" id="username"
        value="{{ old('username') ?? ($user->username ?? '') }}" required />
    @if ($errors->has('username'))
        <div class="text-danger mt-0 pl-1">
            {{ $errors->first('username') }}
        </div>
    @endif
</div>
<div class="form-group">
    <label for="">NIK</label>
    <input type="text" class="form-control masked-nik" name="nik" id="nik"
        value="{{ old('nik') ?? ($user->nik ?? '') }}" required />
    @if ($errors->has('nik'))
        <div class="text-danger mt-0 pl-1">
            {{ $errors->first('nik') }}
        </div>
    @endif
</div>
<div class="form-group">
    <label for="">Password</label>
    <input type="password" class="form-control" name="password" id="password" />
    @if ($errors->has('password'))
        <div class="text-danger mt-0 pl-1">
            {{ $errors->first('password') }}
        </div>
    @endif
</div>
<div class="form-group">
    <label for="">Password Confirm</label>
    <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" />
</div>
<div class="form-group">
    <label for="">No. HP</label>
    <input type="text" class="form-control" name="nohp" id="nohp" onkeypress="return onlyNumberKey(event)"
        value="{{ old('nohp') ?? ($user->nohp ?? '') }}" />
    @if ($errors->has('nohp'))
        <div class="text-danger mt-0 pl-1">
            {{ $errors->first('nohp') }}
        </div>
    @endif
</div>
<div class="form-group">
    <label for="">Email</label>
    <input type="text" class="form-control" name="email" id="email"
        value="{{ old('email') ?? ($user->email ?? '') }}" />
    @if ($errors->has('email'))
        <div class="text-danger mt-0 pl-1">
            {{ $errors->first('email') }}
        </div>
    @endif
</div>
<div class="form-group">
    <label for="">Tipe Pengguna</label>
    <select name="role" id="role" class="form-control">
        <option value="">Pilih </option>
        @foreach ($roles as $role)
            <option {{ (is_null($user->id) ? '' : $user->roles->first()->id == $role->id) ? 'selected' : '' }}
                value="{{ $role->id }}">{{ $role->name }}</option>
        @endforeach
    </select>
    @if ($errors->has('role'))
        <div class="text-danger mt-0 pl-1">
            {{ $errors->first('role') }}
        </div>
    @endif
</div>
<div class="form-group">
    <label for="">Pejabat</label>
    <select name="s_id_pegawai" id="s_id_pegawai" class="form-control">
        <option value="">Pilih </option>
        @foreach ($pegawais as $pegawai)
            <option {{ (is_null($user->id) ? '' : $user->s_id_pegawai == $pegawai->s_id_pegawai) ? 'selected' : '' }}
                value="{{ $pegawai->s_id_pegawai }}">{{ $pegawai->s_nama_pegawai }}</option>
        @endforeach
    </select>
    @if ($errors->has('role'))
        <div class="text-danger mt-0 pl-1">
            {{ $errors->first('role') }}
        </div>
    @endif
</div>
{{-- <div class="form-group @if (is_null($user->id)) d-none @endif"> --}}
<div class="form-group">
    <label for="">Status Pengguna</label>
    <select name="is_active" id="is_active" class="form-control">
        <option {{ is_null($user->is_active) ? 'selected' : '' }} value="true">Ya </option>
        <option {{ (is_null($user->is_active) ? '' : $user->is_active) ? 'selected' : '' }} value="false">Tidak
        </option>
    </select>
</div>
