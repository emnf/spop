<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#">
                <i class="fas fa-bars"></i>
            </a>
        </li>

    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        {{-- <li class="nav-item dropdown border-right">
            <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
                <i class="far fa-bell"></i>
                <span class="badge badge-danger navbar-badge notification-count"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="left: inherit; right: 0px;">
                <span class="dropdown-item dropdown-header"><span class="notification-count"></span>
                    Notifications</span>
                <div class="dropdown-divider"></div>
                <div class="notification-items">
                </div>
            </div>
        </li> --}}
        <li class="nav-item dropdown">
            <div class="btn btn-sm w-auto btn-clean d-flex align-items-center px-2" data-toggle="dropdown">
                <span class="font-weight-bold font-size-base d-none d-md-inline mr-1">Hai,</span>
                <span
                    class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">{{ Auth::user()->username ?? 'Web Developer' }}</span>
                <img src="{{ asset('asset/images/user1-128x128.jpg') }}" class="img-rounded elevation-3"
                    style="height: 40px;width: 40px;" alt="User Image">
            </div>
            <div class="dropdown-menu dropdown-menu-xl dropdown-menu-right">
                <div class="dropdown-item elevation-3">
                    <div class="d-flex justify-content-start py-2">
                        <img src="{{ asset('asset/images/user1-128x128.jpg') }}" class="img-rounded elevation-3"
                            style="height: 90px;width: 90px;" alt="User Image">
                        <div class="px-3">
                            <h5 class="widget-user-desc">{{ Auth::user()->username ?? 'Web Developer' }}</h5>
                            <h6 class="text-muted text-sm">{{ Auth::user()->getRoleNames()->first() }}</h6>
                            <div>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                    @csrf
                                    <button class="btn btn-sm btn-outline-danger" href="{{ route('logout') }}"
                                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        Keluar
                                    </button>
                                </form>
                            </div>



                        </div>
                    </div>
                </div>

            </div>
            {{-- <a class="nav-link" data-toggle="dropdown" href="#">
                <img class="img-fluid img-circle img-sm" src="{{ asset('asset/images/user1-128x128.jpg') }}"
                    alt="Alt Text">
            </a>
            <div class="dropdown-menu dropdown-menu-xl dropdown-menu-right">
                <div class="card card-widget widget-user">
                    <div class="widget-user-header bg-danger">
                        <h3 class="widget-user-username">{{ Auth::user()->name ?? 'Anonymous' }}</h3>
                        <h5 class="widget-user-desc">{{ Auth::user()->username ?? 'Web Developer' }}</h5>
                    </div>
                    <div class="widget-user-image">
                        <img class="img-circle elevation-2" src="{{ asset('asset/images/user1-128x128.jpg') }}"
                            alt="User Avatar">
                    </div>
                    <div class="card-footer">

                    </div>
                </div>
                <form action="{{ url('logout') }}" method="post" class="col-12">
                    @csrf
                    <button class="btn btn-danger col-12" title="Logout">
                        Log Out
                    </button>
                </form> --}}
            {{-- </div> --}}
            {{-- </div> --}}
            </div>
        </li>
    </ul>
</nav>
