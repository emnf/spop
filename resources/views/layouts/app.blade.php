<!DOCTYPE html>
<html lang="en">

<head>
    @php
        $pemda = \App\Helpers\PelayananHelper::getDataPemda();
    @endphp
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <link rel="icon" type="image/png" href="{{ $pemda->s_logo }}">
    <title> @yield('title') </title>

    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet"
        href="{{ asset('plugins/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- JQVMap -->
    {{--
    <link rel="stylesheet" href="{{ asset('plugins/jqvmap/dist/jqvmap.min.css') }}"> --}}
    <!-- Theme style -->
    <link href="{{ asset('plugins/adminlte/dist/css/adminlte.min.css') }}" rel="stylesheet">
    <!-- overlayScrollbars -->
    <link href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}" rel="stylesheet">
    <!-- Daterange picker -->
    <link href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}" rel="stylesheet">
    <!-- ICheck Bootstrap -->
    <link href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}" rel="stylesheet">
    <!-- summernote -->
    <link href="{{ asset('plugins/summernote/summernote-bs4.css') }}" rel="stylesheet">
    <!-- Bootstrap Datepicker -->
    <link href="{{ asset('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <!-- Select2 -->
    <link href="{{ asset('plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet">
    {{--
    <link href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}" rel="stylesheet"> --}}
    <!-- pace-progress -->
    {{--
    <link rel="stylesheet" href="{{ asset('plugins/pace-progress/themes/black/pace-theme-flat-top.css') }}"> --}}
    <!-- Animation style -->
    <link href="{{ asset('plugins/animate.css/animate.css') }}" rel="stylesheet">
    <!--Sweet Alert-->
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}">

    <link href="{{ asset('asset/custom.css') }}" rel="stylesheet">
    {{--
    <link href="{{ asset('asset/css/chat.css') }}" rel="stylesheet"> --}}
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    {{--
    <link href="{{ asset('asset/leaflet/leaflet.css') }}" rel="stylesheet" crossorigin=""> --}}
    {{-- <script src="{{ asset('asset/leaflet/leaflet.js') }}" crossorigin=""></script> --}}
    <link href="{{ asset('datagrid/datagrid.css') }}" rel="stylesheet">

    <style>
        .custom-file-input:lang(en)~.custom-file-label::after {
            content: "Cari Berkas";
        }

        .os-theme-dark>.os-scrollbar,
        .os-theme-light>.os-scrollbar {
            padding: 2px;
            box-sizing: border-box;
            background-color: #c3bfbf;
        }

        .sidebar-dark-danger .nav-sidebar>.nav-item>.nav-link.active,
        .sidebar-light-danger .nav-sidebar>.nav-item>.nav-link.active {
            background-color: #6736da;
            color: #fff;
        }

        .page-header {
            background: url('{{ asset('asset/images/bgpelayanan.gif') }}') #fff no-repeat center center;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .home-header {
            background: url('{{ asset('asset/images/bg4.gif') }}') #fff no-repeat center center;
            min-width: 601px;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .navbar-white {
            background-color: #6736da;
            color: #fcfcfc;
        }

        .navbar-light .navbar-nav .nav-link {
            color: rgb(255, 255, 255);
        }

        /*
    </style>
    <style>
        */ .text_black {
            color: black;
        }

        a:hover div {
            background-color: #fbf5f5;
            color: black;
        }

        @media (max-width:600px) {
            #back-ground {
                display: none;
            }

            #back-groundwp {
                display: none;
            }

        }

        .dropdown-menu .notification-items {
            height: 250px;
            overflow-y: auto;
        }
    </style>
    @stack('styles')
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        @include('layouts.navigation')
        @include('layouts.sidebar')

        <div class="content-wrapper">
            @yield('content')
        </div>

        <div>
            <section data-th-replace="message/index::chat-box"></section>
        </div>

        <footer class="main-footer">
            <strong>Copyright © {{ date('Y') }} <a href="#" class="text-info"
                    target="_blank">{{ strtoupper($pemda->s_nama_singkat_instansi) }}
                    {{ strtoupper($pemda->s_nama_kabkota) }} </a>.
            </strong>
            All rights reserved.
            <div class="float-right d-none d-sm-inline-block">
                <b>Version</b>
                <span>2.0</span>
            </div>
        </footer>
    </div>

    {{-- <div id="loadingModal" class="loadingModal"> --}}
    <div class="modal fade bd-example-modal-lg" id="loadingModal" data-backdrop="static" data-keyboard="false"
        tabindex="-1">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="background-color: #fff0;
  border: 1px solid #fff0);">
                <div class="col-12 text-center">
                    <span class="spinner-grow text-muted"></span>
                    <span class="spinner-grow text-primary"></span>
                    <span class="spinner-grow text-success"></span>
                    <span class="spinner-grow text-info"></span>
                    <span class="spinner-grow text-warning"></span>
                    <span class="spinner-grow text-danger"></span>
                    <span class="spinner-grow text-dark"></span><br>
                    <small class="text-black">Mohon Menunggu...</small>
                </div>
            </div>
        </div>
    </div>
    {{-- </div> --}}

    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    {{-- <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script> --}}
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>
    <script src="{{ asset('plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script src="{{ asset('plugins/chart.js/dist/chart.min.js') }}"></script>
    <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/adminlte/dist/js/adminlte.min.js') }}"></script>
    <!-- Sparkline -->
    {{-- <script src="{{ asset('plugins/sparklines/sparkline.js') }}"></script> --}}
    <!-- JQVMap -->
    {{-- <script src="{{ asset('plugins/jqvmap/dist/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('plugins/jqvmap/dist/maps/jquery.vmap.usa.js') }}"></script> --}}
    <!-- jQuery Knob Chart -->
    {{-- <script src="{{ asset('plugins/jquery-knob/jquery.knob.min.js') }}"></script> --}}
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{ asset('plugins/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <!-- Summernote -->
    <script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
    <!-- overlayScrollbars -->
    {{-- <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script> --}}
    <!-- Select2 -->
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- SweetAlert2 -->
    <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.js') }}"></script>
    <!-- pace-progress -->
    {{-- <script src="{{ asset('plugins/pace-progress/pace.min.js') }}"></script> --}}
    <!-- Bootstrap Datepicker -->
    <script src="{{ asset('plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- websocket & sockJs -->
    {{-- <script src="/webjars/sockjs-client/1.1.2/sockjs.min.js') }}"></script> --}}
    {{-- <script src="/webjars/stomp-websocket/2.3.3-1/stomp.min.js') }}"></script> --}}

    <script src="{{ asset('asset/jquery.mask.min.js') }}"></script>
    <script src="{{ asset('asset/form-validation.js') }}"></script>
    <script src="{{ asset('asset/form-masking.js') }}"></script>
    <script src="{{ asset('datagrid/datagrid.min.js') }}"></script>
    {{-- <script src="{{ asset('plugins/inputmask/jquery.inputmask.min.js') }}"></script> --}}
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script> --}}

    <!-- Chatting -->
    {{-- <script src="{{ asset('asset/js/notification-handler.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/jfk.min.js') }}"></script> --}}

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        })

        var Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        $(document).ready(function() {
            @if (session('success'))
                Toast.fire({
                    icon: 'success',
                    title: '{{ session('success') }}'
                })
            @endif

            {{-- @if (session('error'))
                Toast.fire({
                    icon: 'error',
                    title: '{{ session('error') }}'
                })
            @endif --}}

            bsCustomFileInput.init();

            $('.datepicker-year').datepicker({
                autoclose: true,
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years"
            });

            $('.datepicker-date').datepicker({
                autoclose: true,
                format: "dd-mm-yyyy"
            });

            $('.datepicker-date-fixed').datepicker({
                autoclose: true,
                format: "dd-mm-yyyy",
                endDate: '+0d'
            })
        });

        function onlyNumberKey(evt) {

            var ASCIICode = (evt.which) ? evt.which : evt.keyCode
            if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57)) {
                return false;
            }
            return true;
        }

        function formatMoney(number) {
            return number.toLocaleString('id-ID');
        }

        $(".rupiah").on("keyup", function() {
            $(this).val(formatRupiah(this.value, null));
        });

        const formatRupiah = (number) => {
            return new Intl.NumberFormat("id-ID", {
                style: "currency",
                currency: "IDR",
                maximumFractionDigits: 0
            }).format(number);
        }
    </script>

    @stack('scripts')
</body>

</html>
