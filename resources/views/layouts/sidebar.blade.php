<section class="main-sidebar sidebar-light-danger elevation-4" data-th-fragment="sidebar">
    <!-- Brand Logo -->
    <a class="brand-link text-center" href="">
        <img alt="Kepoin Logo" class="brand-image text-center"
            src="{{ $pemda->s_logo ? asset($pemda->s_logo) : asset('asset/images/elayanan3.png') }}"
            style="float: revert !important;">
        <span class="brand-text font-weight-light mr-2">KEPOIN</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar user (optional) -->
        <div class="mt-3 pb-3 d-flex">
            <div class="image ml-3 mr-2">
                <img src="{{ asset('asset/images/user1-128x128.jpg') }}" style="height: 50px;" class="img-circle"
                    alt="User Image">
            </div>
            <div class="info">
                <span class="block m-t-xs">
                    <strong class="font-bold">{{ Auth::user()->name ?? 'Anonymous' }}</strong>
                </span>
                <br>
                {{-- <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"> --}}
                <span class="text-muted text-xs block">
                    {{ Auth::user()->username }}
                    <b class="caret"></b>
                </span>
                {{-- </a> --}}
                {{-- <div class="dropdown-menu">
                    <!--                            <a class="dropdown-item" href="#">Profile</a>-->
                    <a class="dropdown-item" th:href="@{/pelayanan}">Data Pelayanan</a>
                    <a class="dropdown-item" th:href="@{/message/document}">Info Berkas</a>
                    <a hidden class="dropdown-item" th:href="@{/message/history}">Obrolan</a>
                    <a class="dropdown-item" th:href="@{/logout}" title="Logout">
                        Log Out
                    </a>
                </div> --}}
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-accordion="false" data-widget="treeview"
                role="menu">
                <li class="nav-header">MENU INFORMASI</li>

                <li class="nav-item has-treeview">
                    <a class="nav-link {{ request()->is('home') ? 'active' : '' }}" href="{{ url('home') }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>

                @can('TrackingPelayanan')
                    <li class="nav-item has-treeview">
                        <a class="nav-link {{ request()->is('tracking') ? 'active' : '' }}" href="{{ url('tracking') }}">
                            <i class="nav-icon fas fa-link"></i>
                            <p>Pelacakan Dokumen</p>
                        </a>
                    </li>
                @endcan

                <li class="nav-header">MENU UTAMA</li>
                @can('Pelayanan')
                    <li class="nav-item has-treeview">
                        <a class="nav-link {{ request()->is('pelayanan') ? 'active' : '' }}" href="{{ url('pelayanan') }}"
                            data-toggle="tooltip" data-placement="top" {{-- title="Menu Pengajuan Pelayanan Baru" --}}>
                            <i class="nav-icon fas fa-hands-helping"></i>
                            <p>Pelayanan</p>
                        </a>
                    </li>
                @endcan

                @can('Verfikasi')
                    <li class="nav-item has-treeview">
                        <a class="nav-link {{ request()->is(['verifikasi', 'verifikasi/*']) ? 'active' : '' }}"
                            href="{{ url('verifikasi') }}" data-toggle="tooltip" data-placement="bottom"
                            {{--
                        title="Menu Untuk Verifikasi Persyaratan Permohonan" --}}>
                            <i class="nav-icon fas fa-archive"></i>
                            <p>Verifikasi</p>
                        </a>
                    </li>
                @endcan

                @can('Validasi')
                    <li class="nav-item has-treeview">
                        <a class="nav-link {{ request()->is(['validasi', 'validasi/*']) ? 'active' : '' }}"
                            href="{{ url('validasi') }}">
                            <i class="nav-icon fas fa-clipboard-check"></i>
                            <p>Validasi</p>
                        </a>
                    </li>
                @endcan

                @can('VerifiaksiLapangan')
                    <li class="nav-item has-treeview">
                        <a class="nav-link {{ request()->is(['verifikasi-lapangan', 'verifikasi-lapangan/*']) ? 'active' : '' }}"
                            href="{{ url('verifikasi-lapangan') }}">
                            <i class="nav-icon fas fa-copy"></i>
                            <p>Verifikasi Lapangan</p>
                        </a>
                    </li>
                @endcan
                {{-- <li class="nav-item has-treeview">
                    <a class="nav-link {{ request()->is(['validasi-final', 'validasi-final/*']) ? 'active' : '' }}"
                        href="{{ url('validasi-final') }}">
                        <i class="nav-icon fas fa-clipboard-check"></i>
                        <p>Validasi Final</p>
                    </a>
                </li> --}}

                @can('HasilPelayanan')
                    <li class="nav-item has-treeview">
                        <a class="nav-link {{ request()->is(['hasil-pelayanan', 'hasil-pelayanan/*']) ? 'active' : '' }}"
                            href="{{ url('hasil-pelayanan') }}">
                            <i class="nav-icon fas fa-file-archive"></i>
                            <p>Hasil Pelayanan</p>
                        </a>
                    </li>
                @endcan
                {{-- <li class="nav-header">PENETAPAN</li>
                @can('HasilPelayanan')
                    <li class="nav-item has-treeview">
                        <a class="nav-link {{ request()->is(['penetapan', 'penetapan/*']) ? 'active' : '' }}"
                            href="{{ url('penetapan') }}">
                            <i class="nav-icon fas fa-file-contract"></i>
                            <p>PENETAPAN</p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a class="nav-link {{ request()->is(['esign-tte', 'esign-tte/*']) ? 'active' : '' }}"
                            href="{{ url('esign-tte') }}">
                            <i class="nav-icon fas fa-signature"></i>
                            <p>Esign/TTE</p>
                        </a>
                    </li>
                @endcan --}}

                <li class="nav-header">SETTING</li>
                <li class="nav-item {{ request()->is('setting-*') ? 'menu-is-opening menu-open' : '' }}">
                    <a href="#" class="nav-link {{ request()->is('setting-*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Setting Aplikasi
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        @can('SettingPemda')
                            <li class="nav-item has-treeview">
                                <a class="nav-link {{ request()->is('setting-pemda') ? 'active' : '' }}"
                                    href="{{ url('setting-pemda') }}">
                                    &emsp;<i class="nav-icon fas fa-university"></i>
                                    <p>Data Pemda</p>
                                </a>
                            </li>
                        @endcan

                        @can('SettingPegawai')
                            <li class="nav-item has-treeview">
                                <a class="nav-link {{ request()->is(['setting-pegawai', 'setting-pegawai*']) ? 'active' : '' }}"
                                    href="{{ url('setting-pegawai') }}">
                                    &emsp;<i class="nav-icon fas fa-user-astronaut"></i>
                                    <p>Pegawai</p>
                                </a>
                            </li>
                        @endcan
                        {{-- <li class="nav-item has-treeview">
                            <a class="nav-link {{ request()->is(['setting-jenis-pajak', 'setting-jenis-pajak*']) ? 'active' : '' }}"
                                href="{{ url('setting-jenis-pajak') }}">
                                &emsp;<i class="nav-icon fas fa-puzzle-piece"></i>
                                <p>Jenis Pajak</p>
                            </a>
                        </li> --}}
                        {{-- @can('SettingJenisPelayanan')
                            <li class="nav-item has-treeview">
                                <a class="nav-link {{ request()->is(['setting-jenis-pelayanan', 'setting-jenis-pelayanan*']) ? 'active' : '' }}"
                                    href="{{ url('setting-jenis-pelayanan') }}">
                                    &emsp;<i class="nav-icon fas fa-puzzle-piece"></i>
                                    <p>Jenis Pelayanan</p>
                                </a>
                            </li>
                        @endcan

                        @can('SettingHasilPelayanan')
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is(['setting-jenis-hasil-pelayanan', 'setting-jenis-hasil-pelayanan*']) ? 'active' : '' }}"
                                    href="{{ url('setting-jenis-hasil-pelayanan') }}">
                                    &emsp;<i class="nav-icon fas fa-puzzle-piece"></i>
                                    <p>Jenis Hasil Pelayanan</p>
                                </a>
                            </li>
                        @endcan

                        @can('SettingPersyaratan')
                            <li class="nav-item has-treeview">
                                <a class="nav-link {{ request()->is(['setting-persyaratan', 'setting-persyaratan*']) ? 'active' : '' }}"
                                    href="{{ url('setting-persyaratan') }}">
                                    &emsp;<i class="nav-icon fas fa-user-lock"></i>
                                    <p>Persyaratan</p>
                                </a>
                            </li>
                        @endcan --}}

                        {{-- @can('SettingDokumenPendukung')
                            <li class="nav-item has-treeview">
                                <a class="nav-link {{ request()->is(['setting-dokumen-pendukung', 'setting-dokumen-pendukung*']) ? 'active' : '' }}"
                                    href="{{ url('setting-dokumen-pendukung') }}">
                                    &emsp;<i class="nav-icon fas fa-envelope-open-text"></i>
                                    <p>Dokumen Pendukung</p>
                                </a>
                            </li>
                        @endcan --}}

                        @can('SettingPengguna')
                            <li class="nav-item has-treeview">
                                <a class="nav-link {{ request()->is('setting-user') ? 'active' : '' }}"
                                    href="{{ url('setting-user') }}">
                                    &emsp;<i class="nav-icon fas fa-users"></i>
                                    <p>Pengguna</p>
                                </a>
                            </li>
                        @endcan

                        @if (Auth::user()->id == 1)
                            @can('RolePermission')
                                <li class="nav-item has-treeview">
                                    <a class="nav-link {{ request()->is('setting-roles') ? 'active' : '' }}"
                                        href="{{ url('setting-roles') }}">
                                        &emsp;<i class="nav-icon fas fa-lock"></i>
                                        <p>Role Permission</p>
                                    </a>
                                </li>
                            @endcan
                        @endif

                        @can('SettingUserManual')
                            <li class="nav-item has-treeview">
                                <a class="nav-link {{ request()->is('setting-user-manual') ? 'active' : '' }}"
                                    href="{{ url('setting-user-manual') }}">
                                    &emsp;<i class="nav-icon fas fa-book-medical"></i>
                                    <p>Panduan Aplikasi</p>
                                </a>
                            </li>
                        @endcan

                        @can('SettingAlurPelayanan')
                            <li class="nav-item has-treeview">
                                <a class="nav-link {{ request()->is(['setting-alur-pelayanan', 'setting-alur-pelayanan*']) ? 'active' : '' }}"
                                    href="{{ url('setting-alur-pelayanan') }}">
                                    &emsp;<i class="nav-icon fas fa-book-medical"></i>
                                    <p>Alur Pelayanan</p>
                                </a>
                            </li>
                        @endcan

                        @can('SettingFaq')
                            <li class="nav-item has-treeview">
                                <a class="nav-link {{ request()->is(['setting-faq', 'setting-faq*']) ? 'active' : '' }}"
                                    href="{{ url('setting-faq') }}">
                                    &emsp;<i class="nav-icon fas fa-info"></i>
                                    <p>FAQ</p>
                                </a>
                            </li>
                        @endcan

                        @can('SettingAbout')
                            <li class="nav-item has-treeview">
                                <a class="nav-link {{ request()->is(['setting-about', 'setting-about*']) ? 'active' : '' }}"
                                    href="{{ url('setting-about') }}">
                                    &emsp;<i class="nav-icon fas fa-question"></i>
                                    <p>Tentang</p>
                                </a>
                            </li>
                        @endcan


                        <li class="nav-item has-treeview">
                            <a class="nav-link {{ request()->is(['ganti-password', 'ganti-password*']) ? 'active' : '' }}"
                                href="{{ url('ganti-password') }}">
                                &emsp;<i class="nav-icon fas fa-key"></i>
                                <p>Ubah Password</p>
                            </a>
                        </li>
                    </ul>
                </li>

                {{-- <li class="nav-header">SETTING</li> --}}
            </ul>
        </nav>
    </div>
</section>
