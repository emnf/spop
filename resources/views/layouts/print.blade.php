<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" type="image/png" href="{{ asset('asset/images/elayanan3.png') }}">
    @yield('title')
    <style type="text/css">
        th,
        td {
            padding: 5px;
            vertical-align: top;
        }

        table {
            border-spacing: 0px;
            border-collapse: collapse;
        }

        .border_atas {
            border-top: 1px solid black;
            border-collapse: collapse;
        }

        .border_kiri {
            border-left: 1px solid black;
            border-collapse: collapse;
        }

        .border_kanan {
            border-right: 1px solid black;
            border-collapse: collapse;
        }

        .border_bawah {
            border-bottom: 1px solid black;
            border-collapse: collapse;
        }

        .border_bawah_tebal {
            border-bottom: 2px solid black;
            border-collapse: collapse;
        }

        .border_bawah_doble {
            border-bottom: 2px double black;
            border-collapse: collapse;
        }

        hr.doble_line {
            height: 6px;
            border-top: 2px solid black;
            border-bottom: 1px solid black;
        }

        .font_lima {
            font-size: 5pt;
        }

        .font_tujuh {
            font-size: 7pt;
        }

        .font_sepuluh {
            font-size: 9pt;
        }

        .font_sembilan {
            font-size: 9pt;
        }

        .font_sepuluh {
            font-size: 10pt;
        }

        .font_duabelas {
            font-size: 12pt;
        }

        .font_empatbelas {
            font-size: 14pt;
        }

        .font_enambelas {
            font-size: 16pt;
        }

        .kolom {
            display: inline-table !important;
        }

        .font_bold {
            font-weight: bold;
        }

        .text_tengah {
            text-align: center;
        }

        .text-center {
            text-align: center;
        }

        .text_kanan {
            text-align: right;
        }


        .div-table {
            display: table;
            width: auto;
            border-spacing: 5px;
            /* cellspacing:poor IE support for  this */
        }

        .div-table-row {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-col {
            float: left;
            /* fix for  buggy browsers */
            display: table-column;
            min-width: 200px;
            /* background-color: #ccc; */
        }

        @page {
            margin: 40px;
            margin-top: 30px;
            margin-bottom: 70px;
            footer: page-footer;
            header: page-header;
        }
    </style>
    @stack('styles')
</head>

<body>
    @yield('body')
</body>


</html>
