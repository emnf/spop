<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <title>KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }}</title>Landing Page
    </title>
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="{{ asset('asset/images/elayanan3.png') }}">
    <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('landing/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Animation CSS -->
    <link href="{{ asset('landing/css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('landing/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('landing/css/style.css') }}" rel="stylesheet">

</head>

<body id="page-top">
    <div class="navbar-wrapper">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Kepoin</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a class="page-scroll" href="/">Beranda</a></li>
                        <li class="active"><a class="page-scroll" href="#">Jenis
                                Pelayanan</a></li>
                        <li><a class="page-scroll" href="/#alur-pelayanan">Alur Pelayanan</a></li>
                        <li><a class="page-scroll" href="/#faq">Bantuan</a></li>
                        <li><a class="page-scroll" href="/#saran">Saran</a></li>
                        <li><a class="page-scroll" href="{{ url('/login') }}">Login</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <div id="inSlider" class="carousel carousel-fade" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#inSlider" data-slide-to="0" class="active"></li>
            <li data-target="#inSlider" data-slide-to="1"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="container">
                    <div class="carousel-caption wow fadeInLeft">
                        <h1><span>PEMERINTAH {{ $dataPemda->s_nama_kabkota ?? '(NAMA KABUPATEN)' }}</span><br />
                            <span>{{ $dataPemda->s_nama_instansi ?? 'SINGKATAN INSTANSI' }} (
                                {{ $dataPemda->s_nama_singkat_instansi ?? '' }} )</span>
                        </h1>
                        <p>Silahkan Login, Untuk Melakukan Pengajuan Layanan.</p>
                        <p>
                            <a class="btn btn-md btn-danger" href="{{ url('login') }}" role="button"><b>LOGIN</b></a>
                        </p>
                        <p>Silahkan Daftar akun, untuk yang belum memilik akun.</p>
                        <p>
                            <a class="btn btn-md btn-warning" href="{{ url('register') }}" role="button"><b>DAFTAR
                                    AKUN</b></a>
                        </p>
                    </div>
                    <div class="carousel-image wow zoomIn">
                        <!-- <img src="/elayanan/static/inspinia/img/bppkad.png" alt="laptop" /> -->
                    </div>
                </div>
                <!-- Set background for slide in css -->
                <div class="header-back one"></div>

            </div>
            <div class="item">
                <div class="container">
                    <div class="carousel-caption blank wow fadeInLeft">
                        <h1>NIKMATI KEMUDAHAN <br /> PENGAJUAN PELAYANAN PAJAK</h1>
                        <p>Silahkan Login, Untuk Melakukan Pengajuan Layanan.</p>
                        <p>
                            <a class="btn btn-md btn-danger" href="{{ url('login') }}" role="button"><b>LOGIN</b></a>
                        </p>
                        <p>Silahkan Daftar akun, untuk yang belum memilik akun.</p>
                        <p>
                            <a class="btn btn-md btn-warning" href="{{ url('register') }}" role="button"><b>DAFTAR
                                    AKUN</b></a>
                        </p>
                    </div>
                </div>
                <!-- Set background for slide in css -->
                <div class="header-back two"></div>
            </div>
        </div>
        <a class="left carousel-control" href="#inSlider" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#inSlider" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <section id="jenis-pajak" class="container features">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1><span class="navy">Jenis Pelayanan PBB</span></h1>
                <h2><span th:text="${pajak.namaJenisPajak}"></span></h2>
                <input type="hidden" id="idJenisPajak" value="10">
                <p>Berikut merupakan jenis pelayanan yang kami berikan untuk kemudahan anda dalam mengajukan permohonan.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="row features-block">
                <span>
                    {!! $html !!}
                </span>
            </div>
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 text-center ">
                    <a class="btn btn-lg btn-danger page-scroll" href="/" role="button">Kembali Ke Halaman
                        Utama</a>
                </div>
            </div>

    </section>

    <section id="contact" class="gray-section contact">
        <div class="container">
            <div class="row m-b-lg">
                <div class="col-lg-12 text-center">
                    <div class="navy-line"></div>
                    <h1 style="color:white">Kontak Kami</h1>
                    {{-- <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod.</p> --}}
                </div>
            </div>
            <div class="row m-b-lg">
                <div class="col-lg-3 col-lg-offset-3">
                    <address>
                        <strong><span class="navy">{{ $dataPemda->s_nama_instansi ?? '' }}</span></strong><br />
                        <span>{{ ucwords(strtolower($dataPemda->s_alamat_instansi)) ?? '' }}</span><br />
                        <span>Kecamatan {{ ucwords(strtolower($dataPemda->s_nama_ibukota_kabkota)) ?? '' }},
                            {{ ucwords(strtolower($dataPemda->s_nama_kabkota)) ?? '' }}, Kode Pos
                            {{ $dataPemda->s_kode_pos ?? '' }}</span><br />
                        <abbr title="Phone"><i class="fa fa-phone"></i> </abbr> :
                        <span>{{ $dataPemda->s_notelp_instansi ?? '' }}</span>
                    </address>
                </div>
                <div class="col-lg-4">
                    <p class="text-color">
                        Aplikasi Permohonan Pelayanan Pajak
                    </p>
                    <p class="text-color">
                        WA Center : 0812 3456 7890 (dum)
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p class="m-t-sm">
                        Silahakan Ikuti sosial media {{ $dataPemda->s_nama_singkat_instansi ?? '-' }}
                        {{ ucwords(strtolower($dataPemda->s_nama_kabkota)) ?? '-' }}
                    </p>
                    <ul class="list-inline social-icon">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a>
                        </li>
                        <li><a href="#"><i class="bi bi-whatsapp">
                                    <svg xmlns="{{ asset('asset/whatsapp.svg') }}" width="16" height="16"
                                        fill="currentColor" class="bi bi-whatsapp" viewBox="0 0 16 16">
                                        <path
                                            d="M13.601 2.326A7.854 7.854 0 0 0 7.994 0C3.627 0 .068 3.558.064 7.926c0 1.399.366 2.76 1.057 3.965L0 16l4.204-1.102a7.933 7.933 0 0 0 3.79.965h.004c4.368 0 7.926-3.558 7.93-7.93A7.898 7.898 0 0 0 13.6 2.326zM7.994 14.521a6.573 6.573 0 0 1-3.356-.92l-.24-.144-2.494.654.666-2.433-.156-.251a6.56 6.56 0 0 1-1.007-3.505c0-3.626 2.957-6.584 6.591-6.584a6.56 6.56 0 0 1 4.66 1.931 6.557 6.557 0 0 1 1.928 4.66c-.004 3.639-2.961 6.592-6.592 6.592zm3.615-4.934c-.197-.099-1.17-.578-1.353-.646-.182-.065-.315-.099-.445.099-.133.197-.513.646-.627.775-.114.133-.232.148-.43.05-.197-.1-.836-.308-1.592-.985-.59-.525-.985-1.175-1.103-1.372-.114-.198-.011-.304.088-.403.087-.088.197-.232.296-.346.1-.114.133-.198.198-.33.065-.134.034-.248-.015-.347-.05-.099-.445-1.076-.612-1.47-.16-.389-.323-.335-.445-.34-.114-.007-.247-.007-.38-.007a.729.729 0 0 0-.529.247c-.182.198-.691.677-.691 1.654 0 .977.71 1.916.81 2.049.098.133 1.394 2.132 3.383 2.992.47.205.84.326 1.129.418.475.152.904.129 1.246.08.38-.058 1.171-.48 1.338-.943.164-.464.164-.86.114-.943-.049-.084-.182-.133-.38-.232z" />
                                    </svg>
                                </i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg">
                    <p><strong>&copy; {{ env('APP_YEAR') ?? 2022 }} <a
                                href="#">{{ $dataPemda->s_nama_singkat_instansi ?? '-' }}
                                {{ $dataPemda->s_nama_kabkota ?? '-' }}</a></strong></p>
                </div>
            </div>
        </div>
    </section>

    <script src="{{ asset('landing/js/jquery-2.1.1.js') }}"></script>
    <script src="{{ asset('landing/js/pace.min.js') }}"></script>
    <script src="{{ asset('landing/js/bootstrap.min.js') }}"></script>
    <!--<script src="{{ asset('landing/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>-->
    <!--<script src="{{ asset('landing/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>-->
    <script src="{{ asset('landing/js/classie.js') }}"></script>
    <script src="{{ asset('landing/js/cbpAnimatedHeader.js') }}"></script>

    <script src="{{ asset('landing/js/wow.min.js') }}"></script>
    <script src="{{ asset('landing/js/inspinia.js') }}"></script>

</body>

</html>
