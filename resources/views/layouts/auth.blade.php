<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <title>KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link rel="icon" type="image/png" href="{{ $pemda->s_logo }}">
    <!-- Font Awesome -->
    <link href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
    <!-- icheck bootstrap -->
    <link href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('plugins/toastr/build/toastr.min.css') }}">
    <!-- Theme style -->
    <link href="{{ asset('plugins/adminlte/dist/css/adminlte.min.css') }}" rel="stylesheet">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <style>
        .panel-default {
            opacity: 0.9;
        }

        .form-group.last {
            margin-bottom: 0px;
        }

        .login-page {
            min-height: 100%;
            background: url("/asset/images/bg-login.jpg") no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            /* filter: brightness(0.8); */
            /* Example of adding a brightness filter */
            background-color: rgba(255, 165, 0, 0.5);
            /* Adjust the last value (alpha) to control the transparency */
        }

        .login-box {
            border-radius: 15px;
            /* Adjust the value to change the roundness of the corners */
            background-color: rgba(255, 252, 252, 0.5);
            /* Adjust the last value (alpha) to control the transparency */
        }

        .bd-example-modal-lg .modal-dialog {
            display: table;
            position: relative;
            margin: 0 auto;
            top: calc(50% - 24px);
        }

        .bd-example-modal-lg .modal-dialog .modal-content {
            background-color: transparent;
            border: none;
            box-shadow: none;
        }

        .mail-verified {
            padding-top: 2.5rem;
            max-width: 50%;
        }
    </style>

    @stack('styles')
</head>

<body class="login-page {{ request()->is('login') ? 'hold-transition' : '' }}">
    @yield('content')
    <!-- /.login-box -->

    <!-- jQuery -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('plugins/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <!-- Toastr -->
    <script src="{{ asset('plugins/toastr/build/toastr.min.js') }}"></script>
    <!-- <script src="{{ asset('asset/form-validation.js') }}"></script> -->
    @error('username')
        <script type="text/javascript">
            $(function() {
                toastr.error('{{ $message }}', 'Gagal!')
            });
        </script>
    @enderror
    @stack('scripts')

</body>

</html>
