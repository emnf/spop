@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Setting Pegawai
@endsection

@section('content')
    <section class="content" layout:fragment="content">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a th:href="@{/setting-pegawai}">Setting</a></li>
                            <li class="breadcrumb-item active">Grid</li>
                        </ol>
                    </div><!-- /.col -->
                    <div class="col-12">
                        <div class="info-box" id="background2"
                            style="background: url({{ asset('/asset/images/bgpelayanan.gif') }}) #fff no-repeat center center;
                            -moz-background-size: cover;
                            -o-background-size: cover;
                            background-size: cover;">
                            <div class="col-sm-3">
                                <div class="description-block" style="float: left;">
                                    <h1 class="description-header">SETTING PEGAWAI</h1>
                                </div>
                                <!-- /.description-block -->
                            </div>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-outline card-info">
                            <div class="card-header">
                                <div class="col-sm-12" style="margin-bottom: 10px;">
                                    Form Setting Pegawai [Tambah]
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <form class="form-horizontal form-validation" method="POST"
                                action="{{ route('setting-pegawai.store') }} ">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_nama_pegawai">Nama</label>
                                        <div class="col-sm-4">
                                            <input id="s_id_pegawai" name="s_id_pegawai" type="hidden">
                                            <input class="form-control @error('s_nama_pegawai') is-invalid @enderror"
                                                id="s_nama_pegawai" name="s_nama_pegawai" type="text" required>
                                            @error('s_nama_pegawai')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_jabatan_pegawai">Jabatan</label>
                                        <div class="col-sm-4">
                                            <input class="form-control @error('s_jabatan_pegawai') is-invalid @enderror"
                                                id="s_jabatan_pegawai" name="s_jabatan_pegawai" type="text">
                                            @error('s_jabatan_pegawai')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_pangkat_pegawai">Pangkat</label>
                                        <div class="col-sm-4">
                                            <input class="form-control @error('s_pangkat_pegawai') is-invalid @enderror"
                                                id="s_pangkat_pegawai" name="s_pangkat_pegawai" type="text">
                                            @error('s_pangkat_pegawai')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_nip_pegawai">NIP</label>
                                        <div class="col-sm-4">
                                            <input class="form-control @error('s_nip_pegawai') is-invalid @enderror"
                                                id="s_nip_pegawai" name="s_nip_pegawai" type="text">
                                            @error('s_nip_pegawai')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-info" type="submit">SIMPAN</button>
                                    <a class="btn btn-danger btn-default float-right"
                                        href="{{ route('setting-pegawai') }}">BATAL</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
