@extends('layouts.print')
@php
    // dd($arrWpOp);
@endphp

@section('title')
    <title>KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }}</title>
    </title>
@endsection

@push('styles')
    <style>
        body {
            font-size: 10pt;
        }

        @page {
            margin: 30px 100px;
        }

        div.breakNow {
            page-break-inside: avoid;
            page-break-after: always;
        }

        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;

        }
    </style>
@endpush

@section('body')
    <table style="width: 100%; border-bottom: 5px double #000" cellspacing="0" cellpadding="0">
        <tr>
            <td style="text-align: center; width: 20%">
                <img style="width: 45%" title="LOGO" src="{{ $pemda->s_logo }}" />
            </td>
            <td style="text-align: center">
                <span style="font-size: 11pt;font-weight: bold">PEMERINTAH
                    {{ strtoupper($pemda->s_nama_kabkota) }}</span><br>
                <span style="font-size: 14pt;font-weight: bold">{{ strtoupper($pemda->s_nama_instansi) }}</span><br>
                <span style="font-size: 9pt;">
                    {{ ucwords(strtolower($pemda->s_alamat_instansi)) . ' Telp/Fax. ' . $pemda->s_notelp_instansi . ', Kode Pos ' . $pemda->s_kode_pos }}
                </span><br><br>
            </td>
            <td style="width: 20%">
                &nbsp;
            </td>
        </tr>
    </table>

    <table style="width: 100%;" cellspacing="0" cellpadding="0">
        <tr>
            <td style="text-align: center;padding-top: 24px;">
                <span style="font-size: 12pt; text-align: center;font-weight: bold">SURAT KETERANGAN VALIDASI</span><br>
            </td>
        </tr>
    </table>

    <p style="font-weight: bold">Berkas permohonan atas nama: </p>
    <table style="width: 100%;" cellspacing="0" cellpadding="0">
        <tr>
            <td style="width: 30%">Nama Pemohon</td>
            <td style="width: 5px;">:</td>
            <td colspan="3">{{ $pelayanan->t_nama_pemohon }}</td>
        </tr>
        <tr>
            <td>NIK Pemohon</td>
            <td>:</td>
            <td colspan="3">{{ $pelayanan->t_nik_pemohon }}</td>
        </tr>
        <tr>
            <td>Alamat Pemohon</td>
            <td>:</td>
            <td colspan="3">
                {{ strtoupper($pelayanan->t_jalan_pemohon . ' Rt. ' . $pelayanan->t_rt_pemohon . ' Rw. ' . $pelayanan->t_rw_pemohon) }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td style="width: 200px;">Kelurahan / Desa</td>
            <td style="width: 5px;">:</td>
            <td>&nbsp;{{ strtoupper($pelayanan->t_kelurahan_pemohon) }}</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td style="width: 200px;">Kecamatan</td>
            <td style="width: 5px;">:</td>
            <td>&nbsp;{{ strtoupper($pelayanan->t_kecamatan_pemohon) }}</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td style="width: 200px;">Kabupaten / Kota</td>
            <td style="width: 5px;">:</td>
            <td>&nbsp;{{ strtoupper($pelayanan->t_kabupaten_pemohon) }}</td>
        </tr>
        <tr>
            <td>Jenis Pajak</td>
            <td>:</td>
            <td colspan="3">{{ $pelayanan->jenisPajak->s_nama_jenis_pajak }}</td>
        </tr>
        <tr>
            <td>Jenis Pelayanan</td>
            <td>:</td>
            <td colspan="3">{{ $pelayanan->jenisPelayanan->s_nama_jenis_pelayanan }}</td>
        </tr>
        <tr>
            <td>Tahun Pajak</td>
            <td>:</td>
            <td colspan="3">{{ $pelayanan->t_tahun_pajak }}</td>
        </tr>
        <tr>
            <td>Tanggal Permohonan</td>
            <td>:</td>
            <td colspan="3">{{ date('d-m-Y', strtotime($pelayanan->t_tgl_pelayanan)) }}</td>
        </tr>
        <tr>
            <td>Tanggal Pengajuan</td>
            <td>:</td>
            <td colspan="3">{{ date('d-m-Y', strtotime($pelayanan->pelayananAprove->created_at)) }}</td>
        </tr>
        <tr>
            <td>Tanggal Perkiraan Selesai</td>
            <td>:</td>
            <td colspan="3">{{ date('d-m-Y', strtotime($pelayanan->pelayananAprove->t_tgl_perkiraan_selesai)) }}</td>
        </tr>
        <tr>
            <td>Tanggal Selesai</td>
            <td>:</td>
            <td colspan="3">{{ date('d-m-Y', strtotime($validasi->t_tgl_validasi)) }}</td>
        </tr>
        <tr>
            <td>Status Validasi</td>
            <td>:</td>
            <td colspan="3">{{ $validasi->statusValidasi->s_nama_status_validasi }}</td>
        </tr>
        <tr>
            <td>Keterangan Validasi</td>
            <td>:</td>
            <td colspan="3">{{ $validasi->t_keterangan_validasi }}</td>
        </tr>
    </table>
    <hr style="border: 1px solid #000" />
    <p style="font-weight: bold">Data Terbaru: </p>

    @if ($opnyaCount != 0)
        @foreach ($arrWpOp as $key => $v)
            <p style="font-weight: bold">Data Subjek Pajak: </p>
            <table style="width: 100%;" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width: 30%">Nomor Pelayanan</td>
                    <td style="width: 5px;">:</td>
                    <td colspan="3">
                        {{ \App\Helpers\PelayananHelper::formatNoPelayanan(
                            date('Y', strtotime($pelayanan->t_tgl_pelayanan)),
                            $pelayanan->t_id_jenis_pajak,
                            $pelayanan->t_no_pelayanan,
                        ) }}
                    </td>
                </tr>
                <tr>
                    <td>Nama Wajib Pajak</td>
                    <td>:</td>
                    <td colspan="3">{{ $v['wpnya']['t_nama_wp'] }}</td>
                </tr>
                <tr>
                    <td>NIK</td>
                    <td>:</td>
                    <td colspan="3">{{ $v['wpnya']['t_nik_wp'] }}</td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td colspan="3">
                        {{ strtoupper($v['wpnya']['t_jalan_wp'] . ' Rt. ' . $v['wpnya']['t_rt_wp'] . ' Rw. ' . $v['wpnya']['t_rw_wp']) }}
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td style="width: 200px;">Kelurahan / Desa</td>
                    <td style="width: 5px;">:</td>
                    <td>&nbsp;{{ strtoupper($v['wpnya']['t_kelurahan_wp']) }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td style="width: 200px;">Kecamatan</td>
                    <td style="width: 5px;">:</td>
                    <td>&nbsp;{{ strtoupper($v['wpnya']['t_kecamatan_wp']) }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td style="width: 200px;">Kabupaten / Kota</td>
                    <td style="width: 5px;">:</td>
                    <td>&nbsp;{{ strtoupper($v['wpnya']['t_kabupaten_wp']) }}</td>
                </tr>
            </table>

            <p style="font-weight: bold">Data Objek Pajak: </p>
            <table style="width: 100%;" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width: 30%">NOP</td>
                    <td style="width: 5px;">:</td>
                    <td colspan="3">{{ $v['opnya']['nopnya'] }}</td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td colspan="3">{{ strtoupper($v['opnya']['t_jalan_op']) }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td style="width: 200px;">Kelurahan / Desa</td>
                    <td style="width: 5px;">:</td>
                    <td>&nbsp;{{ strtoupper($v['opnya']['t_kelurahan_op']) }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td style="width: 200px;">Kecamatan</td>
                    <td style="width: 5px;">:</td>
                    <td>&nbsp;{{ strtoupper($v['opnya']['t_kecamatan_op']) }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td style="width: 200px;">Kabupaten / Kota</td>
                    <td style="width: 5px;">:</td>
                    <td>&nbsp;{{ strtoupper($pemda->s_nama_kabkota) }}</td>
                </tr>
                <tr>
                    <td>Luas Tanah (M<sup>2</sup>)</td>
                    <td>:</td>
                    <td colspan="3">{{ $v['opnya']['t_luas_tanah'] }}</td>
                </tr>
                <tr>
                    <td>Luas Bangunan (M<sup>2</sup>)</td>
                    <td>:</td>
                    <td colspan="3">{{ $v['opnya']['t_luas_bangunan'] }}</td>
                </tr>
            </table>
            @if ($key == 0)
                <div style="page-break-before: always;"></div>
            @endif
            @if (($key + 1) % 3 == 0)
                <div style="page-break-after: before;"></div>
            @endif
        @endforeach
    @else
        <p style="font-weight: bold">Data Subjek Pajak Terbaru: </p>
        <table style="width: 100%;" cellspacing="0" cellpadding="0">
            <tr>
                <td style="width: 30%">Nomor Pelayanan</td>
                <td style="width: 5px;">:</td>
                <td colspan="3">
                    {{ \App\Helpers\PelayananHelper::formatNoPelayanan(
                        date('Y', strtotime($pelayanan->t_tgl_pelayanan)),
                        $pelayanan->t_id_jenis_pajak,
                        $pelayanan->t_no_pelayanan,
                    ) }}
                </td>
            </tr>
            <tr>
                <td>Nama Wajib Pajak</td>
                <td>:</td>
                <td colspan="3">{{ $pelayanan->wajibPajak->t_nama_wp }}</td>
            </tr>
            <tr>
                <td>NIK</td>
                <td>:</td>
                <td colspan="3">{{ $pelayanan->wajibPajak->t_nik_wp }}</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td colspan="3">
                    {{ strtoupper($pelayanan->wajibPajak->t_jalan_wp . ' Rt. ' . $pelayanan->wajibPajak->t_rt_wp . ' Rw. ' . $pelayanan->wajibPajak->t_rt_wp) }}
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td style="width: 200px;">Kelurahan / Desa</td>
                <td style="width: 5px;">:</td>
                <td>&nbsp;{{ strtoupper($pelayanan->wajibPajak->t_kelurahan_wp) }}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td style="width: 200px;">Kecamatan</td>
                <td style="width: 5px;">:</td>
                <td>&nbsp;{{ strtoupper($pelayanan->wajibPajak->t_kecamatan_wp) }}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td style="width: 200px;">Kabupaten / Kota</td>
                <td style="width: 5px;">:</td>
                <td>&nbsp;{{ strtoupper($pelayanan->wajibPajak->t_kabupaten_wp) }}</td>
            </tr>
        </table>

        <p style="font-weight: bold">Data Objek Pajak Terbaru: </p>
        <table style="width: 100%;" cellspacing="0" cellpadding="0">
            <tr>
                <td style="width: 30%">NOP</td>
                <td style="width: 5px;">:</td>
                <td colspan="3">{{ $pelayanan->t_nop ?? $pelayanan->objekPajak->nopFormated }}</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td colspan="3">{{ strtoupper($pelayanan->objekPajak->t_jalan_op) }}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td style="width: 200px;">Kelurahan / Desa</td>
                <td style="width: 5px;">:</td>
                <td>&nbsp;{{ strtoupper($pelayanan->objekPajak->t_kelurahan_op) }}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td style="width: 200px;">Kecamatan</td>
                <td style="width: 5px;">:</td>
                <td>&nbsp;{{ strtoupper($pelayanan->objekPajak->t_kecamatan_op) }}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td style="width: 200px;">Kabupaten / Kota</td>
                <td style="width: 5px;">:</td>
                <td>&nbsp;{{ strtoupper($pemda->s_nama_kabkota) }}</td>
            </tr>
            <tr>
                <td>Luas Tanah (M<sup>2</sup>)</td>
                <td>:</td>
                <td colspan="3">{{ $pelayanan->objekPajak->t_luas_tanah }}</td>
            </tr>
            <tr>
                <td>Luas Bangunan (M<sup>2</sup>)</td>
                <td>:</td>
                <td colspan="3">{{ $pelayanan->objekPajak->t_luas_bangunan }}</td>
            </tr>
        </table>
    @endif

    @if ($pelayanan->t_id_jenis_pelayanan == 8)
        <p class="spacing-1"><strong>NOP Dan Jumlah Pengurangan:</strong></p>
        <table>
            <tr>
                <td>NOP</td>
                <td>:</td>
                <td>{{ $pelayanan->objekPajak->nopFormated ?? ' ' }}</td>
            </tr>
            <td>Besar Pengurangan</td>
            <td>:</td>
            <td style="color:red">{{ $pelayanan->t_besar_pengurangan }}%</td>

        </table>
    @endif
    @if ($tte == 1)
        <div class="footer">
            <table style="width: 100%; border-collapse: collapse; font-size: 11px;" border="0">
                <tr>
                    <td colspan="3" style="border-top: 1px solid black;"></td>
                </tr>
                <tr>
                    <td style="width: 5px; vertical-align: top;">-</td>
                    <td style="vertical-align: top;">Dokumen ini ditandatangani secara elektronik menggunakan
                        Sertifikat Elektronik yang diterbitkan oleh Balai Sertifikasi Elektronik (BSrE), Badan Siber dan
                        Sandi
                        Negara (BSSN).</td>
                    <td style="vertical-align: top;">
                        <img src="{{ public_path() . '/bsre/' }}bsre.png" style="width: 70px;">
                    </td>
                </tr>
            </table>
        </div>
    @endif

    @if ($pelayanan->detailBangunan->count() > 0)
        <div class="breakNow"></div>
        <hr class="border">
        <p class="spacing-1"><strong>Rincian Data Bangunan Dan Fasilitas:</strong></p>
        <table>
            <tr>
                <td>Jenis Penggunaan Bangunan</td>
                <td>:</td>
                <td>{{ $pelayanan->detailBangunan[0]->t_jenis_penggunaan_bangunan }}</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Dinding</td>
                <td>:</td>
                <td>{{ $pelayanan->detailBangunan[0]->t_dinding }}</td>
            </tr>
            <td>Jumlah Lantai</td>
            <td>:</td>
            <td>{{ $pelayanan->detailBangunan[0]->t_jumlah_lantai }}</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Lantai</td>
            <td>:</td>
            <td>{{ $pelayanan->detailBangunan[0]->t_lantai }}</td>
            </tr>
            <tr>
                <td>Tahun Bangunan</td>
                <td>:</td>
                <td>{{ $pelayanan->detailBangunan[0]->t_tahun_bangunan }}</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Langit-Langit</td>
                <td>:</td>
                <td>{{ $pelayanan->detailBangunan[0]->t_langit_langit }}</td>
            </tr>
            <tr>
                <td>Tahun Renovasi</td>
                <td>:</td>
                <td>{{ $pelayanan->detailBangunan[0]->t_tahun_renovasi }}</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>AC Split</td>
                <td>:</td>
                <td>{{ $pelayanan->detailBangunan[0]->t_ac_split }}</td>
            </tr>
            <tr>
                <td>Kondisi Pada Umumnya</td>
                <td>:</td>
                <td>{{ $pelayanan->detailBangunan[0]->t_kondisi_bangunan }}</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>AC Window</td>
                <td>:</td>
                <td>{{ $pelayanan->detailBangunan[0]->t_ac_window }}</td>
            </tr>
            <tr>
                <td>Konstruksi</td>
                <td>:</td>
                <td>{{ $pelayanan->detailBangunan[0]->t_konstruksi }}</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Panjang Pagar (M)</td>
                <td>:</td>
                <td>{{ $pelayanan->detailBangunan[0]->t_panjang_pagar }}</td>
            </tr>
            <tr>
                <td>Atap</td>
                <td>:</td>
                <td>{{ $pelayanan->detailBangunan[0]->t_atap }}</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Bahan Pagar</td>
                <td>:</td>
                <td>{{ $pelayanan->detailBangunan[0]->t_bahan_pagar }}</td>
            </tr>
            <tr>
                <td>Daya Listrik (Watt)</td>
                <td>:</td>
                <td>{{ $pelayanan->detailBangunan[0]->t_listrik }}</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

        </table>
    @endif

    <table style="width: 100%">
        <tr>
            <td style="width: 50%">
                &nbsp;
            </td>
            <td style="text-align: center">
                {{ $pemda->s_nama_ibukota_kabkota }}, {{ date('d-m-Y') }}<br />
                {{ $pejabat->s_jabatan_pegawai }}
                <br />
                <br />
                @if ($tte == 1)
                    <br />
                    <br />
                    <span style="color: white;">#</span>
                    <br />
                    <br />
                    <br />
                    <br />
                    <img src="{{ public_path() . '/bsre/' }}visual-ttd.png" style="width: 100px;">
                    <br />
                @else
                    <br />
                    <br />
                    <br />
                @endif
                <span style="text-decoration: underline">{{ $pejabat->s_nama_pegawai }}</span><br />
                NIP. {{ $pejabat->s_nip_pegawai }}
            </td>
        </tr>
    </table>

    @if ($tte == 1)
        <div class="footer">
            <table style="width: 100%; border-collapse: collapse; font-size: 11px;" border="0">
                <tr>
                    <td colspan="3" style="border-top: 1px solid black;"></td>
                </tr>
                <tr>
                    <td style="width: 5px; vertical-align: top;">-</td>
                    <td style="vertical-align: top;">Dokumen ini ditandatangani secara elektronik menggunakan
                        Sertifikat Elektronik yang diterbitkan oleh Balai Sertifikasi Elektronik (BSrE), Badan Siber dan
                        Sandi
                        Negara (BSSN).</td>
                    <td style="vertical-align: top;">
                        <img src="{{ public_path() . '/bsre/' }}bsre.png" style="width: 70px;">
                    </td>
                </tr>
            </table>
        </div>
    @endif
@endsection
