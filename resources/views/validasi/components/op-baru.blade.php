<div class="form-group">
    <label>NOP Terdekat</label>
    <input type="text" class="form-control form-control-sm col-md-4" name="nopTerdekat" value="{{ $hasilVerlapOPBaru->t_nop_terdekat }}" readonly>
</div>
<div class="form-group">
    <label>No. SHM/Surat Ukur/Dokumen Tanah</label>
    <input type="text" class="form-control col-md-4" name="noSHM" value="{{ $hasilVerlapOPBaru->t_no_shm }}" readonly>
</div>
<div class="form-group">
    <label>Tgl. SHM/Surat Ukur/Dokumen Tanah</label>
    <input type="text" class="form-control col-md-4" name="tglSHM" value="{{ $hasilVerlapOPBaru->t_tgl_shm }}" readonly>
</div>
<div class="form-group">
    <label>Luas Tanah di SHM/Surat Ukur/Dokumen Tanah</label>
    <input type="text" class="form-control col-md-4" name="luasTanahSHM" value="{{ $hasilVerlapOPBaru->t_luas_tanah_shm }}" readonly>
</div>
<div class="form-group">
    <label>Atas Nama di SHM/Surat Ukur/Dokumen Tanah</label>
    <input type="text" class="form-control col-md-4" name="atasNamaSHM" value="{{ $hasilVerlapOPBaru->t_atas_nama_shm }}" readonly>
</div>
<div class="form-group">
    <label>Tempat Penelitian</label>
    <textarea class="form-control" name="tempatPenelitian" readonly>{{ $hasilVerlap->t_tempat_penelitian }}</textarea>
</div>
