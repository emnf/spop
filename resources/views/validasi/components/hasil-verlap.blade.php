<div class="row invoice-info pt-3">
    <div class="col-sm-6 invoice-col border-right">
        <h5>Data Objek Pajak (Hasil Verifikasi Lapang)</h5>
        @foreach ($pelayanan->hasilVerlapOp as $OpLama)
        <hr>
        <address>
            <div class="row">
                <div class="col-5">NOP</div>
                <div class="col-1">:</div>
                <div class="col-6"><b>{{ (empty($objekPajak->kd_propinsi)) ? '-' : $objekPajak->nopFormated}}</b></div>
            </div>
            <div class="row">
                <div class="col-5">Jalan</div>
                <div class="col-1">:</div>
                <div class="col-6"><b>{{ $OpLama->t_jalan_op ?? ''}}</b></div>
            </div>
            <div class="row">
                <div class="col-5">Rt</div>
                <div class="col-1">:</div>
                <div class="col-6"><b>{{ $OpLama->t_rt_op ?? ''}}</b></div>
            </div>
            <div class="row">
                <div class="col-5">Rw</div>
                <div class="col-1">:</div>
                <div class="col-6"><b>{{ $OpLama->t_rw_op ?? ''}}</b></div>
            </div>
            <div class="row">
                <div class="col-5">Blok/No/Kav</div>
                <div class="col-1">:</div>
                <div class="col-6"><b>-</b></div>
            </div>
            <div class="row">
                <div class="col-5">Kelurahan/Desa</div>
                <div class="col-1">:</div>
                <div class="col-6"><b>{{ $OpLama->t_kelurahan_op ?? ''}}</b></div>
            </div>
            <div class="row">
                <div class="col-5">Kecamatan</div>
                <div class="col-1">:</div>
                <div class="col-6"><b>{{ $OpLama->t_kecamatan_op ?? ''}}</b></div>
            </div>
            <div class="row">
                <div class="col-5">Luas Tanah (M<sup>2</sup>)</div>
                <div class="col-1">:</div>
                <div class="col-6"><b>{{ $OpLama->t_luas_tanah ?? ''}}</b></div>
            </div>
            <div class="row">
                <div class="col-5">Luas Bangunan (M<sup>2</sup>)</div>
                <div class="col-1">:</div>
                <div class="col-6"><b>{{ $OpLama->t_luas_bangunan ?? ''}}</b></div>
            </div>
        </address>
        @endforeach
    </div>
    <div class="col-sm-6 invoice-col">
        <h5>Data Wajib Pajak (Hasil Verifikasi Lapang)</h5>
        @foreach ($pelayanan->hasilVerlapWp as $WpLama)
        <hr>
        <address>
            <div class="row">
                <div class="col-5">NIK</div>
                <div class="col-1">:</div>
                <div class="col-6"><b>{{ $WpLama->t_nik_wp ?? '' }}</b></div>
            </div>
            <div class="row">
                <div class="col-5">Nama</div>
                <div class="col-1">:</div>
                <div class="col-6"><b>{{ $WpLama->t_nama_wp ?? '' }}</b></div>
            </div>
            <div class="row">
                <div class="col-5">Jalan</div>
                <div class="col-1">:</div>
                <div class="col-6"><b>{{ $WpLama->t_jalan_wp ?? '' }}</b></div>
            </div>
            <div class="row">
                <div class="col-5">Rt</div>
                <div class="col-1">:</div>
                <div class="col-6"><b>{{ $WpLama->t_rt_wp ?? '' }}</b></div>
            </div>
            <div class="row">
                <div class="col-5">Rw</div>
                <div class="col-1">:</div>
                <div class="col-6"><b>{{ $WpLama->t_rw_wp ?? '' }}</b></div>
            </div>
            <div class="row">
                <div class="col-5">Kelurahan/Desa</div>
                <div class="col-1">:</div>
                <div class="col-6"><b>{{ $WpLama->t_kelurahan_wp ?? '' }}</b></div>
            </div>
            <div class="row">
                <div class="col-5">Kecamatan</div>
                <div class="col-1">:</div>
                <div class="col-6"><b>{{ $WpLama->t_kecamatan_wp ?? '' }}</b></div>
            </div>
            <div class="row">
                <div class="col-5">Kabupaten/Kota</div>
                <div class="col-1">:</div>
                <div class="col-6"><b>{{ $WpLama->t_kabupaten_wp ?? '' }}</b></div>
            </div>
        </address>
        @endforeach
    </div>
</div>
