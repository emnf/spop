@push('styles')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css"
        integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="" />
@endpush
<div class="row">
    @include('components.data-view.data-user')
    <div class="col-12">
        <div class="invoice p-3 mb-3" style="border-top:3px solid #dc3545;">
            <div class="row invoice-info">
                <div class="col-sm-6 invoice-col border-right">
                    <h5>Data Pemohon</h5>
                    <hr>
                    @include('pelayanan.pbb.components.data-pemohon')
                </div>
                <div class="col-sm-6 invoice-col">
                    <h5>Data Pelayanan</h5>
                    <hr>
                    @include('pelayanan.pbb.components.data-view.data-pelayanan')
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card" style="border-top:5px solid #dc3545;">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="invoice p-3 mb-3">
                            <div class="row invoice-info">
                                <div class="col-sm-6 invoice-col border-right">
                                    @include('pelayanan.pbb.components.data-view.objek-pajak')
                                </div>
                                <div class="col-sm-6 invoice-col">
                                    @include('pelayanan.pbb.components.data-view.wajib-pajak')
                                </div>
                            </div>
                            @if (!in_array($pelayanan->t_id_jenis_pelayanan, [5, 6]))
                                @if (count($pelayanan->OpLamas) > 0)
                                    <div class="row invoice-info pt-3">
                                        <div class="col-sm-6 invoice-col border-right">
                                            @include('pelayanan.pbb.components.data-view.objek-pajak-lama')
                                        </div>
                                        <div class="col-sm-6 invoice-col">
                                            @include('pelayanan.pbb.components.data-view.wajib-pajak-lama')
                                        </div>
                                    </div>
                                @endif
                            @endif
                        </div>
                        @if (in_array($pelayanan->t_id_jenis_pelayanan, [1, 2, 3, 4]) && count($pelayanan->hasilVerlapOp) > 0)
                            <div class="invoice p-3 mb-3">
                                @include('validasi.components.hasil-verlap')
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if (
    !in_array($pelayanan->t_id_jenis_pelayanan, [3, 5, 6, 7, 8, 9]) &&
        (!is_null($pelayanan->objekPajak->t_latitude) || !is_null($pelayanan->objekPajak->t_longitude)))
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <b>LOKASI OBJEK</b>
                </div>
                <div class="card-body">
                    <div id="map" style="width: 100%; height: 400px">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@if (count($pelayanan->detailBangunan) > 0 && count($pelayanan->hasilVerlapOp) < 1)
    <div class="row">
        <div class="col-12">
            <div class="invoice p-3 mb-3" style="border-top:3px solid #dc3545;">
                <div class="row invoice-info">
                    <div class="col-sm-6 invoice-col border-right">
                        <h5>DATA BANGUNAN</h5>
                        <hr>
                        <address>
                            @foreach ($pelayanan->detailBangunan as $key => $item)
                                <div class="row">
                                    <div class="col-5">Luas Bangunan Ke-{{ $item->t_no_urut_bangunan }}</div>
                                    <div class="col-1">:</div>
                                    <div class="col-6"><b>{{ $item->t_luas }}</b> (M<sup>2</sup>)</div>
                                </div>
                            @endforeach
                        </address>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="card card-danger card-outline">
            <div class="card-header">
                <div class="card-title">
                    <h5>Persyaratan Permohonan Pelayanan</h5>
                </div>
            </div>
            <div class="card-body">
                <div class="row table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="width: 4rem;">No.</th>
                                <th>Jenis Persyaratan</th>
                                <th>File Persyaratan</th>
                                <th>Unduh File</th>
                            </tr>
                        </thead>
                        <tbody id="uploadedFiles">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@if (isset($verifikasiLapangan))
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <div class="card-title">
                        <h5>Hasil Verifikasi Lapangan</h5>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>No. Berita Acara Hasil Verifikasi Lapangan</label>
                                <input type="text" class="form-control col-md-4" name="noBAHP"
                                    value="{{ $hasilVerlap->t_no }}" readonly>
                            </div>
                            @if ($pelayanan->t_id_jenis_pelayanan == 1)
                                @include('validasi.components.op-baru')
                            @endif
                            <div class="form-group">
                                <label>Catatan Verifikasi Lapangan</label>
                                <textarea class="form-control" name="keteranganPenelitian" readonly>{{ $verifikasiLapangan->t_keterangan_verifikasi_lapangan }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif


<form class="form-horizontal form-validation" method="post" action="/validasi/save">
    @csrf
    <input type="hidden" name="idPelayanan" id="idPelayanan" value="{{ $pelayanan->t_id_pelayanan }}">
    <input type="hidden" name="idValidasi" id="idValidasi" value="{{ $validasi->t_id_validasi ?? '' }}">
    <input type="hidden" name="idStatusValidasi" id="idStatusValidasi"
        value="{{ $validasi->t_id_status_validasi ?? '' }}">
    <input type="hidden" id="idJenisPelayanan" name="idJenisPelayanan" value="{{ $pelayanan->t_id_jenis_pelayanan }}">
    <input type="hidden" id="idJenisPajak" name="idJenisPajak" value="{{ $pelayanan->t_id_jenis_pajak }}">
    <input type="hidden" id="nikPemohon" name="nikPemohon" value="{{ $pelayanan->t_nik_pemohon }}">
    <input type="hidden" id="nop" name="nop" value="{{ $pelayanan->t_nop }}">
    <input type="hidden" id="nikWP" name="nikWP" value="{{ $wajibPajak->t_nik_wp }}">

    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <div class="card-title">
                        <h5>Persetujuan Validasi</h5>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-justify">Berdasarkan informasi yang sudah diajukan diatas Apakah anda
                                selaku validator menyetujui permohonan pelayanan ini ? Jika anda sudah menyetujui
                                permohonan ini maka tidak dapat dilakukan perubahan lagi.</p>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12" style="text-align: center"><u>
                                                <h5>Keterangan Verifikasi</h5>
                                            </u>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="row">
                                        <div class="col-2">Kejelasan</div>
                                        <div class="col-1">:
                                            <label> {{ ($pelayanan->verifikasi->t_kejelasan == 1 ) ? "YA" : "TIDAK"
                                                }}</label>
                                        </div>

                                        <div class="row">
                                            <div class="col-2">Kebenaran</div>
                                            <div class="col-1">:
                                                <label> {{ ($pelayanan->verifikasi->t_kebenaran == 1 ) ? "YA" : "TIDAK"
                                                    }}</label>
                                            </div>
                                        </div>

                                    <div class="row">
                                        <div class="col-2">Keabsahan</div>
                                        <div class="col-1">:
                                            <label> {{ ($pelayanan->verifikasi->t_keabsahan == 1 ) ? "YA" : "TIDAK"
                                                }}</label>
                                        </div>
                                    </div> --}}
                                <div class="row">
                                    <div class="col-1" style="text-align: center">
                                        Catatan
                                    </div>
                                    <div class="col-10"> :
                                        <label> {{ $pelayanan->verifikasi->t_keterangan_verifikasi }} </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if ($pelayanan->t_id_jenis_pelayanan == 8)
                        <div class="raw">
                            <div class="col-md-12">
                                <h6>Besar Pengurangan Diminta (%) </h6>
                                <div class="row p-2">
                                    <input type="text" class="form-control form-control-sm col-md-2"
                                        name="t_besar_pengurangan" value="{{ $pelayanan->t_besar_pengurangan }}"
                                        readonly>
                                </div>
                            </div>
                        </div>
                        <div class="raw">
                            <div class="col-md-12">
                                <h6>Besar Pengurangan Disetujui (%)</h6>
                                <div class="row p-2">
                                    <input type="text" class="form-control form-control-sm col-md-2"
                                        name="pengurangan_disetujui">
                                </div>
                            </div>
                        </div>
                    @endif
                    @php
                        // if($pelayanan->t_id_jenis_pelayanan == 1){
                        //     if(isset($validasi)){
                        //        $a = 'Sudah dilakukan Verifikasi Lapangan';
                        //     }else{
                        //         $a = 'Op Baru Otomatis Verifikasi Lapangan';
                        //     }
                        // }
                        // ($pelayanan->t_id_jenis_pelayanan == 1) ? 'Op Baru Otomatis Verifikasi Lapangan' : ''
                    @endphp
                    <div class="row mt-4">
                        <div class="col-md-12">
                            <h6>Catatan<span class="text-danger">*</span> :
                                <br><small class="text-danger"> {{-- $a --}} </small>
                                {{-- <small>(Alasan kenapa permohonan Ditolak ?)</small> --}}
                            </h6>
                            <div class="row p-2">
                                <textarea class="form-control" name="keterangan" id="keterangan" th:text="${validasi.keterangan}" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4 @if (in_array($pelayanan->t_id_jenis_pelayanan, [1, 6]) || isset($verifikasiLapangan)) d-none @endif">
                        <div class="col-md-auto">
                            <h6>Pengajuan Verifikasi Lapangan
                                <small>(Apakah ini diajukan ke verifikasi lapangan?)</small>
                            </h6>
                            <div class="row p-2">
                                <select class="form-control" name="pengajuanVerlap" id="pengajuanVerlap" required>
                                    <option value="2">Diajukan</option>
                                    <option
                                        {{ isset($verifikasiLapangan) || $pelayanan->t_id_jenis_pelayanan == 6 ? 'selected' : '' }}
                                        value="1" @if ($pelayanan->t_id_jenis_pelayanan == 1) disabled @endif>Tidak</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="col-12 d-flex justify-content-between">
                        <button type="button" class="btn btn-danger" id="btnReject" data-toggle="modal"
                            data-target="#modal-cancel">
                            <i class="fas fa-times-circle"></i> Tolak Permohonan
                        </button>
                        <div class="modal fade" id="modal-cancel">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header bg-danger">
                                        <h6 class="modal-title"><span class="fas fa-exclamation"></span> &nbsp;TOLAK
                                            PENGAJUAN PERMOHONAN ?</h6>
                                        <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Apakah anda yakin ingin menolak pengajuan data permohonan tersebut?
                                        </p>
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-default btn-sm"
                                            data-dismiss="modal"><span class="fas fa-times-circle"></span>
                                            &nbsp;Tutup
                                        </button>
                                        <button type="submit" class="btn btn-danger btn-sm"><span
                                                class="fas fa-exclamation"></span> &nbsp;IYA
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="button" class="btn btn-primary float-right" id="btnApprove"
                            style="margin-right: 5px;" data-toggle="modal" data-target="#modal-save">
                            <i class="fas fa-check-circle"></i> Terima Permohonan
                        </button>
                        <div class="modal fade" id="modal-save">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header bg-primary">
                                        <h6 class="modal-title"><span class="fas fa-send"></span> &nbsp;TERIMA
                                            PERMOHONAN PELAYANAN</h6>
                                        <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Apakah anda ingin menerima permohonan layanan ini?</p>
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-default btn-sm"
                                            data-dismiss="modal"><span class="fas fa-times-circle"></span>
                                            &nbsp;Tutup
                                        </button>
                                        <button type="submit" class="btn btn-primary btn-sm"><span
                                                class="fas fa-check"></span> &nbsp;IYA
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<div class="modal fade" id="modal-image">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h6 class="modal-title" id="modal-image-title">&nbsp;FILE PERSYARATAN</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center" id="imagePlaceholder">

            </div>
            <div class="modal-footer justify-content-between">

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;HAPUS DATA ?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus data sebagai berikut?</p>
                <div class="row">
                    <div class="col-sm-4">Dokumen Pendukung</div>
                    <div class="col-sm-8">
                        <input type="hidden" id="delete-idUploadDokumenPendukung">
                        <input type="hidden" id="delete-element-id">
                        <textarea type="text" class="form-control form-control-sm" id="delete-namaDokumenPendukung" readonly></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span
                        class="fas fa-times-circle"></span> Tutup
                </button>
                <button type="button" class="btn btn-danger btn-sm" disabled id="btnHapusDokumenPendukung"><span
                        class="fas fa-trash"></span> Hapus
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-SudahAjukan">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-exclamation"></span> &nbsp;PEMBERITAHUAN
                </h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="tbodyMessages">
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-info btn-sm" data-dismiss="modal"><span
                        class="fas fa-check"></span>
                    &nbsp;OK
                </button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    {{-- <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyq1l-IUYHhDWoWwF4HqTTVw-ixal-JzQ&callback=initMap&libraries=&v=weekly"
        async defer>
    </script> --}}
    {{-- <script>
        $(function () {
            initMap();
        });
        function initMap() {
            let latitude = {{ $objekPajak->t_latitude ?? 0 }}*1;
            let longitude = {{ $objekPajak->t_longitude ?? 0 }}*1;

            if(latitude == '' && longitude=='')
            {
                var latlng = {lat: -3.4278, lng: 115.9978};
            }
            else
            {
                var latlng = {lat: latitude, lng: longitude};
            }

            var map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                scrollwheel: false,
                zoom: 17,
            });
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                draggable: false,
                title : "Lokasi Objek Pajak"
            });

            google.maps.event.trigger(map, "resize");
        }
    </script> --}}
    <script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js"
        integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>
    <script>
        let latitude = {{ $objekPajak->t_latitude ?? 0 }};
        let longitude = {{ $objekPajak->t_longitude ?? 0 }};
        const map = new L.map('map').setView([latitude, longitude], 13);

        const tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        }).addTo(map);

        const marker = L.marker([latitude, longitude], {
            draggable: true
        }).addTo(map);

        marker.on('dragend', function(e) {
            document.getElementById('t_latitude').value = marker.getLatLng().lat;
            document.getElementById('t_longitude').value = marker.getLatLng().lng;
        });
    </script>

    <script type="text/javascript">
        $(function() {
            updateUploadedFiles($("#idPelayanan").val());
            // updateUploadedDokumenPendukung();
        })

        $("#btnReject").click(function() {
            $("#idStatusValidasi").val(3);
        })

        $("#btnApprove").click(function() {
            $("#idStatusValidasi").val(4);
        })

        function showImage(img) {
            var link = $(img).attr("link");
            var name = $(img).attr("name");
            $("#modal-image-title").text(name);
            var image = $('<img style="max-height:100%; max-width:100%;">');
            image.attr("src", link);
            $("#imagePlaceholder").html(image);
            $("#modal-image").modal('show');
            return false;
        }

        function findIdDokumenPendukung(idDokumenPendukung) {
            var id = 0;
            $(".id-dokumen-pendukung").each(function() {
                if ($(this).val() == idDokumenPendukung) {
                    id = $(this).data("id");
                }
            })
            return id;
        }

        function showDeleteDialog(id) {
            $("#modal-delete").modal('show');
            var idUpload = $("#idUploadDokumenPendukung" + id).val();
            $.get("/upload-dokumen-pendukung/by-id-upload/" + idUpload).then(function(data) {
                $("#btnHapusDokumenPendukung").removeAttr("disabled");
                $("#delete-idUploadDokumenPendukung").val(idUpload);
                $("#delete-element-id").val(id);
                $("#delete-namaDokumenPendukung").text(data.dokumenPendukung.namaDokumenPendukung);
            });
        }

        function updateUploadedDokumenPendukung() {
            var idPelayanan = $("#idPelayanan").val();
            $.get("/upload-dokumen-pendukung/" + idPelayanan).then(function(data) {
                data.forEach(file => {
                    var id = findIdDokumenPendukung(file.idDokumenPendukung);
                    if (id > 0) {
                        $("#idUploadDokumenPendukung" + id).val(file.idUploadDokumenPendukung);
                        $("#noDokumen" + id).val(file.nomorDokumen);
                        $("#noDokumen" + id).attr("readonly", "readonly");
                        $("#tglDokumen" + id).val(file.tanggalDokumen);
                        $("#keteranganDokumen" + id).text(file.keterangan);
                        $("#keteranganDokumen" + id).attr("readonly", "readonly");
                        $("#file" + id).addClass('d-none');
                        $("#btnDownload" + id).attr("href", file.lokasiFile);
                        $("#btnDownload" + id).removeClass('d-none');
                        $("#btnDeleteDokumenPendukung" + id).removeClass('d-none');
                        $("#btnSimpan" + id).addClass('d-none');
                    }
                })
            });
        }

        function updateUploadedFiles(idPelayanan) {
            $.get("/validasi/upload-file/" + idPelayanan).then(function(data) {
                $("#uploadedFiles").html('');
                var no = 1;
                data.forEach(file => {
                    var tr = '<tr>' +
                        '<td style="text-align: center">' + no + '</td>' +
                        '<td>' + file.t_nama_persyaratan + '</td>' +
                        '<td><a type="button" class="text-primary" link="/' + file.t_lokasi_file +
                        '" name="' + file.t_nama_persyaratan + '" onclick="showImage(this)">' + file
                        .t_nama_persyaratan + '</a></td>' +
                        '<td><a href="/' + file.t_lokasi_file + '" target="_blank" download>' + "Unduh" +
                        '</td>' +
                        '</tr>';
                    $("#uploadedFiles").append(tr);
                    no++;
                })

            });
        }

        function doUpload(id) {
            var data = new FormData();
            data.append("idUploadDokumenPendukung", $("#idUploadDokumenPendukung" + id).val());
            data.append("idDokumenPendukung", $("#idDokumenPendukung" + id).val());
            data.append("noDokumen", $("#noDokumen" + id).val());
            data.append("tglDokumen", $("#tglDokumen" + id).val());
            data.append("keterangan", $("#keteranganDokumen" + id).val());
            data.append("idPelayanan", $("#idPelayanan").val());

            var files = $("#file" + id)[0].files;
            $.each(files, function(i, file) {
                data.append("file", file);
            });

            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/upload-dokumen-pendukung",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function(data) {
                    updateUploadedDokumenPendukung();
                },
                error: function(e) {
                    console.log("ERROR : ", e);
                }
            });
        }

        var jenisPelayanan = $("#idJenisPelayanan").val();
        if (jenisPelayanan == 1) {
            // cekPermohonanSebelumnyaOPbaru();
        } else {
            // cekPermohonanSebelumnya();
        }

        function cekPermohonanSebelumnya() {
            $.get("/pelayanan/pengajuan-sebelumnya/" + $("#nikPemohon").val() + "/" + $("#idJenisPelayanan").val() + "/" +
                    $("#idJenisPajak").val() + "/" + $("#nop").val())
                .then(function(res) {
                    var data = res.data;
                    var tbodyMessages = "";
                    $("#tbodyMessages").html("");

                    if (data.idPelayanan != $("#idPelayanan").val()) {

                        tbodyMessages = '<div class="col-12">' +
                            '<div class="alert alert-info alert-dismissible">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
                            'Permohonan ini sudah pernah direkam / diajukan sebelumnya, dengan data sebagai berikut:' +
                            '</div>' +
                            '<div class="form-group-sm row">' +
                            '<label class="col-sm-4 col-form-label-sm">No. Pelayanan</label>' +
                            '<div class="col-sm-8"> : <strong class="text-blue">' + data.noPelayanan +
                            '</strong> </div>' +
                            '</div>' +
                            '<div class="form-group-sm row">' +
                            '<label class="col-sm-4 col-form-label-sm">NIK Pemohon</label>' +
                            '<div class="col-sm-8"> : ' + data.nikPemohon + ' </div>' +
                            '</div>' +
                            '<div class="form-group-sm row">' +
                            '<label class="col-sm-4 col-form-label-sm">Nama Pemohon</label>' +
                            '<div class="col-sm-8"> : ' + data.namaPemohon + ' </div>' +
                            '</div>' +
                            '<div class="form-group-sm row">' +
                            '<label class="col-sm-4 col-form-label-sm">NOP</label>' +
                            '<div class="col-sm-8"> : ' + data.nop + ' </div>' +
                            '</div>' +
                            '<div class="form-group-sm row">' +
                            '<label class="col-sm-4 col-form-label-sm">Jenis Pelayanan</label>' +
                            '<div class="col-sm-8"> : ' + data.jenisPelayanan + ' </div>' +
                            '</div>' +
                            '<div class="form-group-sm row">' +
                            '<label class="col-sm-4 col-form-label-sm">Jenis Pajak</label>' +
                            '<div class="col-sm-8"> : ' + data.jenisPajak + ' </div>' +
                            '</div>' +
                            '<div class="form-group-sm row">' +
                            '<label class="col-sm-4 col-form-label-sm">Status Permohonan</label>' +
                            '<div class="col-sm-8"> : <span class="btn btn-sm bg-warning">' + data.statusPermohonan +
                            '</span> </div>' +
                            '</div>' +
                            '</div>';

                        $("#modal-SudahAjukan").modal("show");

                    }
                    $("#tbodyMessages").append(tbodyMessages);
                });
        }

        function cekPermohonanSebelumnyaOPbaru() {
            $.get("/pelayanan/pengajuan-sebelumnya-opbaru/" + $("#nikPemohon").val() + "/" + $("#idJenisPelayanan").val() +
                    "/" + $("#idJenisPajak").val() + "/" + $("#nikWP").val())
                .then(function(res) {
                    var data = res.data;
                    var tbodyMessages = "";
                    $("#tbodyMessages").html("");

                    if (data.idPelayanan != $("#idPelayanan").val()) {
                        tbodyMessages = '<div class="col-12">' +
                            '<div class="alert alert-info alert-dismissible">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
                            'Permohonan ini sudah pernah direkam / diajukan sebelumnya, dengan data sebagai berikut:' +
                            '</div>' +
                            '<div class="form-group-sm row">' +
                            '<label class="col-sm-4 col-form-label-sm">No. Pelayanan</label>' +
                            '<div class="col-sm-8"> : <strong class="text-blue">' + data.noPelayanan +
                            '</strong> </div>' +
                            '</div>' +
                            '<div class="form-group-sm row">' +
                            '<label class="col-sm-4 col-form-label-sm">NIK Pemohon</label>' +
                            '<div class="col-sm-8"> : ' + data.nikPemohon + ' </div>' +
                            '</div>' +
                            '<div class="form-group-sm row">' +
                            '<label class="col-sm-4 col-form-label-sm">Nama Pemohon</label>' +
                            '<div class="col-sm-8"> : ' + data.namaPemohon + ' </div>' +
                            '</div>' +
                            '<div class="form-group-sm row">' +
                            '<label class="col-sm-4 col-form-label-sm">NIK Wajib Pajak <small class="text-blue">(Baru)</small></label>' +
                            '<div class="col-sm-8"> : ' + data.nikWp + ' </div>' +
                            '</div>' +
                            '<div class="form-group-sm row">' +
                            '<label class="col-sm-4 col-form-label-sm">Nama WP <small class="text-blue">(Baru)</small></label>' +
                            '<div class="col-sm-8"> : ' + data.namaWp + ' </div>' +
                            '</div>' +
                            '<div class="form-group-sm row">' +
                            '<label class="col-sm-4 col-form-label-sm">Jenis Pelayanan</label>' +
                            '<div class="col-sm-8"> : ' + data.jenisPelayanan + ' </div>' +
                            '</div>' +
                            '<div class="form-group-sm row">' +
                            '<label class="col-sm-4 col-form-label-sm">Jenis Pajak</label>' +
                            '<div class="col-sm-8"> : ' + data.jenisPajak + ' </div>' +
                            '</div>' +
                            '<div class="form-group-sm row">' +
                            '<label class="col-sm-4 col-form-label-sm">Status Permohonan</label>' +
                            '<div class="col-sm-8"> : <span class="btn btn-sm bg-warning">' + data.statusPermohonan +
                            '</span> </div>' +
                            '</div>' +
                            '</div>';

                        $("#modal-SudahAjukan").modal("show");
                    }
                    $("#tbodyMessages").append(tbodyMessages);
                });

            $("input:checkbox").on('click', function() {
                var $box = $(this);
                if ($box.is(":checked")) {
                    var group = "input:checkbox[name='" + $box.attr("name") + "']";
                    $(group).prop("checked", false);
                    $box.prop("checked", true);
                } else {
                    $box.prop("checked", false);
                }
            });
        }
    </script>
    {{-- // <th:block th:switch="${pelayanan.jenisPajak.idKategoriPajak}">
        // <th:block th:case="'2'">
            // <script data-th-replace="pelayanan/common/data-objek/__${pelayanan.jenisPelayanan.alias}__ :: js">
            </script>
            // </th:block>
        // </th:block> --}}
@endpush
