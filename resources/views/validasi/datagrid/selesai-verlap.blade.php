<div data-th-fragment="tab-panel" class="tab-pane fade" id="datagrid-tab-selesai-verlap" role="tabpanel"
    aria-labelledby="datagrid-tab-selesai-verlap">
    <div class="row table-responsive">
        <table id="table-selesai-verlap" class="table table-striped table-bordered"
            style="font-size: 12px; width: 100%">
            <thead>
                <tr>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No.</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Jenis Pajak</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Jenis Pelayanan</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Permohonan</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No. Pelayanan</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Selesai Verlap</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">NIK Pemohon</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pemohon</th>
                    <th style="background-color: #1fc8e3; vertical-align: top; width: 8rem;" class="text-center">Action</th>
                </tr>
                <tr>
                    <th></th>
                    <th>
                        {{-- <select class="form-control form-control-sm selesaiVerlap-filter" id="selesaiVerlap-filter-idJenisPajak">
                            <option value="">Semua Jenis Pajak</option>
                            @foreach ($jenispajak as $jp)
                                <option value="{{ $jp->s_id_jenis_pajak }}">{{ $jp->s_nama_jenis_pajak }}</option>
                            @endforeach
                        </select> --}}
                    </th>
                    <th>
                        <select class="form-control form-control-sm selesaiVerlap-filter" id="selesaiVerlap-filter-idJenisPelayanan">
                            <option value="">Semua Jenis Pelayanan</option>
                            @foreach ($jenispelayanan as $jp)
                                <option value="{{ $jp->s_id_jenis_pelayanan }}">{{ $jp->s_nama_jenis_pelayanan }}</option>
                            @endforeach
                        </select>
                    </th>
                    <th><input class="form-control form-control-sm "
                        type="text" id="selesaiVerlap-filter-tglPemohon" readonly></th>
                    <th><input class="form-control form-control-sm selesaiVerlap-filter"
                        type="text" id="selesaiVerlap-filter-noPelayanan"></th>
                    <th><input class="form-control form-control-sm"
                        type="text" id="selesaiVerlap-filter-tglSelesaiVerlab" readonly></th>
                    <th><input class="form-control form-control-sm selesaiVerlap-filter masked-nik"
                        type="text" id="selesaiVerlap-filter-nikPemohon"></th>
                    <th><input class="form-control form-control-sm selesaiVerlap-filter"
                        type="text" id="selesaiVerlap-filter-namaPemohon"></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="card-footer pagination-footer"></div>
</div>

@push('scripts')
<script>
    $(function(){
        var dataGridSelesaiVerlap = datagrid({
                url: '/validasi/datagrid-sudah-verlap',
                table: "#table-selesai-verlap",
                columns: [
                    {class: ""},
                    {class: ""},
                    {class: "text-center"},
                    {class: "text-center"},
                    {class: "text-center"},
                    {class: "text-center"},
                    {class: "text-center"},
                    {class: ""}
                ],
                orders: [
                    {sortable: false},
                    {sortable: true, name: "idJenisPajak"},
                    {sortable: true, name: "idJenisPelayanan"},
                    {sortable: true, name: "tglPelayanan"},
                    {sortable: true, name: "noPelayanan"},
                    {sortable: true, name: "validasi.tglValidasi"},
                    {sortable: true, name: "nikPemohon"},
                    {sortable: true, name: "namaPemohon"},
                    {sortable: false}

                ],
                action: [
                    {
                        name: 'edit-validasi',
                        btnClass: 'btn btn-warning btn-xs',
                        btnText: '<i class="fas fa-edit"> Periksa Hasil Verlap</i>'
                    }
                ]
            });

            function searchSelesaiVerlap(){
                dataGridSelesaiVerlap.setFilters({
                    idJenisPajak: $("#selesaiVerlap-filter-idJenisPajak").val(),
                    idJenisPelayanan: $("#selesaiVerlap-filter-idJenisPelayanan").val(),
                    tglPelayanan: $("#selesaiVerlap-filter-tglPemohon").val(),
                    noPelayanan: $("#selesaiVerlap-filter-noPelayanan").val(),
                    tglSelesaiVerlab: $("#selesaiVerlap-filter-tglSelesaiVerlab").val(),
                    nikPemohon: $("#selesaiVerlap-filter-nikPemohon").val(),
                    namaPemohon: $("#selesaiVerlap-filter-namaPemohon").val()
                });
                dataGridSelesaiVerlap.reload();
            }

            $(".selesaiVerlap-filter").each(function(){
                $(this).change(function(){
                    searchSelesaiVerlap();
                });

                $(this).keyup(function(){
                    searchSelesaiVerlap();
                })
            });

            $("#selesaiVerlap-filter-tglPemohon").daterangepicker({
                autoUpdateInput: false,
                 locale: {
                    cancelLabel: 'Clear'
                 }
             });

            $("#selesaiVerlap-filter-tglPemohon").on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
                searchSelesaiVerlap();
            });

            $("#selesaiVerlap-filter-tglPemohon").on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                searchSelesaiVerlap();
            });

            $("#selesaiVerlap-filter-tglSelesaiVerlab").daterangepicker({
                autoUpdateInput: false,
                 locale: {
                    cancelLabel: 'Clear'
                 }
             });

            $("#selesaiVerlap-filter-tglSelesaiVerlab").on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
                searchSelesaiVerlap();
            });

            $("#selesaiVerlap-filter-tglSelesaiVerlab").on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                searchSelesaiVerlap();
            });

            searchSelesaiVerlap();
    })

    function printHasilValidasi(idPelayanan){
        window.open('/report/validasi/hasil/mutasi/' + idPelayanan);
    }



</script>
@endpush
