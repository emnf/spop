<div class="tab-pane fade show active" id="datagrid-tab-belum-proses"
     role="tabpanel" aria-labelledby="datagrid-tab-belum-proses">
    <!--<div class="row mb-2">
        <div class="col-12 pl-0">
            <button class="btn btn-danger btn-sm" onclick="printValidasiBelumProses('PDF')"><span class="fas fa-file-pdf"></span> &nbsp;PDF</button>
            <button class="btn btn-success btn-sm" onclick="printValidasiBelumProses('XLS')"><span class="fas fa-file-excel"></span> &nbsp;XLS</button>
        </div>
    </div>-->
    <div class="row table-responsive">

        <table id="table-belum-proses" class="table table-striped table-bordered"
               style="font-size: 12px; width: 100%">
            <thead>
                <tr>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No.</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Jenis Pajak</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Jenis Pelayanan</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Verifikasi</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No. Pelayanan</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">NIK</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Wajib Pajak</th>
                    <th style="background-color: #1fc8e3; vertical-align: top; width: 8rem;" class="text-center">Action</th>
                </tr>
                <tr>
                    <th></th>
                    <th>
                        {{-- <select class="form-control form-control-sm" id="filter-idJenisPajak">
                            <option value="">Semua Jenis Pajak</option>
                            @foreach ($jenispajak as $jp)
                                <option value="{{ $jp->s_id_jenis_pajak }}">{{ $jp->s_nama_jenis_pajak }}</option>
                            @endforeach
                        </select> --}}
                    </th>
                    <th>
                        <select class="form-control form-control-sm" id="filter-idJenisPelayanan">
                            <option value="">Semua Jenis Pelayanan</option>
                            @foreach ($jenispelayanan as $jp)
                                <option value="{{ $jp->s_id_jenis_pelayanan }}">{{ $jp->s_nama_jenis_pelayanan }}</option>
                            @endforeach
                        </select>
                    </th>
                    <th><input class="form-control form-control-sm" type="text" id="filter-tglVerifikasi" readonly></th>
                    <th><input class="form-control form-control-sm" type="text" id="filter-noPelayanan"></th>
                    <th><input class="form-control form-control-sm masked-nik" type="text" id="filter-nikPemohon"></th>
                    <th><input class="form-control form-control-sm" type="text" id="filter-namaPemohon"></th>
                    <th><input class="form-control form-control-sm" type="text" id="filter-namaWp"></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="card-footer pagination-footer">

    </div>
</div>

@push('scripts')
<script>
    const dataGridBelumProses = datagrid({
        url: '/validasi/datagrid-belum-proses',
        table: "#table-belum-proses",
        columns: [
            {class: ""},
            {class: ""},
            {class: "text-center"},
            {class: "text-center"},
            {class: ""},
            {class: ""},
            {class: "text-center"},
        ],
        orders: [
            {sortable: false},
            {sortable: true, name: "t_id_jenis_pajak"},
            {sortable: true, name: "t_id_jenis_pelayanan"},
            {sortable: true, name: "t_verifikasi.t_tgl_verifikasi"},
            {sortable: true, name: "t_no_pelayanan"},
            {sortable: true, name: "t_nik_pemohon"},
            {sortable: true, name: "t_nama_pemohon"},
            {sortable: false},
            {sortable: false}
        ],
        action: [
            {
                name: 'input-validasi',
                btnClass: 'btn btn-warning btn-xs',
                btnText: '<i class="fas fa-pencil-alt"> Validasi</i>'
            }
        ]

    });

    function searchBelumProses(){
        dataGridBelumProses.setFilters({
            idJenisPajak: $("#filter-idJenisPajak").val(),
            idJenisPelayanan: $("#filter-idJenisPelayanan").val(),
            tglVerifikasi: $("#filter-tglVerifikasi").val(),
            noPelayanan: $("#filter-noPelayanan").val(),
            nikPemohon: $("#filter-nikPemohon").val(),
            namaPemohon: $("#filter-namaPemohon").val(),
            namaWp: $("#filter-namaWp").val()
        });
        dataGridBelumProses.reload();
    }

    $("#filter-tglVerifikasi, #filter-idJenisPajak, #filter-idJenisPelayanan").change(function(){
        searchBelumProses();
    });

    $("#filter-nikPemohon, #filter-noPelayanan, #filter-namaPemohon").keyup(function(){
        searchBelumProses();
    })

    $("#filter-namaPemohon, #filter-namaWp").keyup(function(){
        if ($(this).val().length >= 3 || $(this).val().length == 0) {
            searchBelumProses();
        }
    })

        $("#filter-tglVerifikasi").daterangepicker({
        autoUpdateInput: false,
            locale: {
            cancelLabel: 'Clear'
            }
        });

    $("#filter-tglVerifikasi").on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        searchBelumProses();
    });

    $("#filter-tglVerifikasi").on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        searchBelumProses();
    });

    searchBelumProses();
</script>
@endpush
