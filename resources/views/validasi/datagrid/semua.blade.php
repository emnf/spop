<div class="tab-pane fade" id="datagrid-tab-semua" role="tabpanel"
     aria-labelledby="datagrid-tab-semua">
    <div class="row table-responsive">
        <table id="table-all" class="table table-striped table-bordered"
               style="font-size: 12px; width: 100%">
            <thead>
            <tr>
                <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No.</th>
                <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Jenis Pajak</th>
                <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Jenis Pelayanan</th>
                <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Permohonan</th>
                <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No. Pelayanan</th>
                <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Validasi</th>
                <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pemohon</th>
                <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Wajib Pajak</th>
                <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Status</th>
            </tr>
            <tr>
                <th></th>
                <th>
                    {{-- <select class="form-control form-control-sm semua-filter" id="semua-filter-idJenisPajak">
                        <option value="">Semua Jenis Pajak</option>
                        @foreach ($jenispajak as $jp)
                            <option value="{{ $jp->s_id_jenis_pajak }}">{{ $jp->s_nama_jenis_pajak }}</option>
                        @endforeach
                    </select> --}}
                </th>
                <th>
                    <select class="form-control form-control-sm semua-filter" id="semua-filter-idJenisPelayanan">
                        <option value="">Semua Jenis Pelayanan</option>
                        @foreach ($jenispelayanan as $jp)
                            <option value="{{ $jp->s_id_jenis_pelayanan }}">{{ $jp->s_nama_jenis_pelayanan }}</option>
                        @endforeach
                    </select>
                </th>
                <th><input class="form-control form-control-sm" type="text" id="semua-filter-tglPelayanan" readonly></th>
                <th><input class="form-control form-control-sm semua-filter" type="text" id="semua-filter-noPelayanan"></th>
                <th><input class="form-control form-control-sm" type="text" id="semua-filter-tglValidasi" readonly></th>
                <th><input class="form-control form-control-sm semua-filter" type="text" id="semua-filter-namaPemohon"></th>
                <th><input class="form-control form-control-sm semua-filter" type="text" id="semua-filter-namaWp"></th>
                <th>
                    <select class="form-control form-control-sm semua-filter" id="semua-filter-statusValidasi">
                        <option value="">Semua Status</option>
                        <option value="1">Belum Proses</option>
                        <option value="2">Pending</option>
                        <option value="3">Ditolak</option>
                        <option value="4">Diterima</option>
                        <option value="5">Diajukan Verlap</option>
                        <option value="6">Selesai Verlap</option>
                    </select>
                </th>

            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="card-footer pagination-footer"></div>
</div>

@push('scripts')
<script>
    $(function(){
        var dataGrid = datagrid({
            url: '/validasi/datagrid-validasi',
            table: "#table-all",
            columns: [
                {class: ""},
                {class: ""},
                {class: "text-center"},
                {class: "text-center"},
                {class: ""},
                {class: "text-center"},
                {class: ""}
            ],
            orders: [
                {sortable: false},
                {sortable: true, name: "idJenisPajak"},
                {sortable: true, name: "idJenisPelayanan"},
                {sortable: true, name: "tglPelayanan"},
                {sortable: true, name: "noPelayanan"},
                {sortable: true, name: "validasi.tglValidasi"},
                {sortable: true, name: "namaPemohon"},
                {sortable: true, name: "namaWp"},
                {sortable: false}

            ],
            action: [
                {
                    name: 'status-belum-proses',
                    btnClass: 'btn btn-default btn-xs dg-status',
                    btnText: 'Belum diproses'
                },
                {
                    name: 'status-pending',
                    btnClass: 'btn btn-secondary btn-xs dg-status',
                    btnText: 'Pending'
                },
                {
                    name: 'status-ditolak',
                    btnClass: 'btn btn-danger btn-xs dg-status',
                    btnText: 'Ditolak'
                },
                {
                    name: 'status-diterima',
                    btnClass: 'btn btn-primary btn-xs dg-status',
                    btnText: 'Diterima'
                },
                {
                    name: 'status-diajukan-verlap',
                    btnClass: 'btn btn-warning btn-xs dg-status',
                    btnText: 'Diajukan Verlap'
                },
                {
                    name: 'status-selesai-verlap',
                    btnClass: 'btn btn-warning btn-xs dg-status',
                    btnText: 'Selesai Verlap'
                }
            ]
        });

        function searchAll(){
            dataGrid.setFilters({
                idJenisPajak: $("#semua-filter-idJenisPajak").val(),
                idJenisPelayanan: $("#semua-filter-idJenisPelayanan").val(),
                tglPelayanan: $("#semua-filter-tglPelayanan").val(),
                tglValidasi: $("#semua-filter-tglValidasi").val(),
                noPelayanan: $("#semua-filter-noPelayanan").val(),
                namaPemohon: $("#semua-filter-namaPemohon").val(),
                namaWp: $("#semua-filter-namaWp").val(),
                idStatusValidasi: $("#semua-filter-statusValidasi").val()
            });
            dataGrid.reload();
        }

        $(".semua-filter").each(function(){
            $(this).keyup(function(){
                if ($(this).val().length >= 1 || $(this).val().length <= 0) {
                    searchAll();
                }
            })
        });

        $("#semua-filter-tglPelayanan").daterangepicker({
            autoUpdateInput: false,
             locale: {
                cancelLabel: 'Clear'
             }
         });

        $("#semua-filter-tglPelayanan").on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            searchAll();
        });

        $("#semua-filter-tglPelayanan").on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            searchAll();
        });

        $("#semua-filter-tglValidasi").daterangepicker({
            autoUpdateInput: false,
             locale: {
                cancelLabel: 'Clear'
             }
         });

        $("#semua-filter-tglValidasi").on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            searchAll();
        });

        $("#semua-filter-tglValidasi").on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            searchAll();
        });

        $("#semua-filter-idJenisPelayanan").change(function(){
            searchAll();
        });

        searchAll();
    })
</script>
@endpush
