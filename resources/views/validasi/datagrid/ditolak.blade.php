<div class="tab-pane fade" id="datagrid-tab-ditolak" role="tabpanel"
    aria-labelledby="datagrid-tab-ditolak">
    <div class="row table-responsive">
        <table id="table-rejected" class="table table-striped table-bordered" style="font-size: 12px; width: 100%">
            <thead>
                <tr>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No.</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Jenis Pajak</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Jenis Pelayanan</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Permohonan</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No. Pelayanan</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Ditolak</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">NIK Pemohon</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Keterangan</th>
                    <th style="background-color: #1fc8e3; vertical-align: top; width: 8rem;" class="text-center">Action</th>

                </tr>
                <tr>
                    <th></th>
                    <th>
                        {{-- <select class="form-control form-control-sm ditolak-filter" id="ditolak-filter-idJenisPajak">
                            <option value="">Semua Jenis Pajak</option>
                            @foreach ($jenispajak as $jp)
                                <option value="{{ $jp->s_id_jenis_pajak }}">{{ $jp->s_nama_jenis_pajak }}</option>
                            @endforeach
                        </select> --}}
                    </th>
                    <th>
                        <select class="form-control form-control-sm ditolak-filter" id="ditolak-filter-idJenisPelayanan">
                            <option value="">Semua Jenis Pelayanan</option>
                            @foreach ($jenispelayanan as $jp)
                                <option value="{{ $jp->s_id_jenis_pelayanan }}">{{ $jp->s_nama_jenis_pelayanan }}</option>
                            @endforeach
                        </select>
                    </th>
                    <th><input class="form-control form-control-sm" type="text" id="ditolak-filter-tglPelayanan" readonly></th>
                    <th><input class="form-control form-control-sm ditolak-filter" type="text" id="ditolak-filter-noPelayanan"></th>
                    <th><input class="form-control form-control-sm" type="text" id="ditolak-filter-tglValidasi" readonly></th>
                    <th><input class="form-control form-control-sm ditolak-filter masked-nik" type="text" id="ditolak-filter-nikPemohon"></th>
                    <th></th>
                    <th></th>

                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="card-footer pagination-footer">
    </div>
</div>

@push('scripts')
<script th:inline="javascript" data-th-fragment="js">
    const dataGridRejected = datagrid({
        url: '/validasi/datagrid-ditolak',
        table: "#table-rejected",
        columns: [
            {class: ""},
            {class: ""},
            {class: "text-center"},
            {class: "text-center"},
            {class: ""},
            {class: "text-center"},
            {class: ""}
        ],
        orders: [
            {sortable: false},
            {sortable: true, name: "idJenisPajak"},
            {sortable: true, name: "idJenisPelayanan"},
            {sortable: true, name: "tglPelayanan"},
            {sortable: true, name: "noPelayanan"},
            {sortable: true, name: "validasi.tglValidasi"},
            {sortable: true, name: "nikPemohon"},
            {sortable: false},
            {sortable: false}

        ],
        action: [
            {
                name: 'hasil-validasi',
                btnClass: 'btn btn-danger btn-xs',
                btnText: '<i class="fas fa-print"> Hasil Validasi</i>'
            },
            {
                name: 'batal-ditolak',
                btnClass: 'btn btn-warning btn-xs',
                btnText: '<i class="fas fa-times"> Batal Ditolak</i>'
            }
        ]
    });

    function searchRejected(){
        dataGridRejected.setFilters({
            idJenisPajak: $("#ditolak-filter-idJenisPajak").val(),
            idJenisPelayanan: $("#ditolak-filter-idJenisPelayanan").val(),
            tglPelayanan: $("#ditolak-filter-tglPelayanan").val(),
            tglValidasi: $("#ditolak-filter-tglValidasi").val(),
            noPelayanan: $("#ditolak-filter-noPelayanan").val(),
            nikPemohon: $("#ditolak-filter-nikPemohon").val()
        });
        dataGridRejected.reload();
    }

    $(".ditolak-filter").each(function(){
        $(this).change(function(){
            searchRejected();
        });

        $(this).keyup(function(){
            searchRejected();
        })
    });

    $("#ditolak-filter-tglPelayanan").daterangepicker({
        autoUpdateInput: false,
            locale: {
            cancelLabel: 'Clear'
            }
        });

    $("#ditolak-filter-tglPelayanan").on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        searchRejected();
    });

    $("#ditolak-filter-tglPelayanan").on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        searchRejected();
    });

    $("#ditolak-filter-tglValidasi").daterangepicker({
        autoUpdateInput: false,
            locale: {
            cancelLabel: 'Clear'
            }
        });

    $("#ditolak-filter-tglValidasi").on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        searchRejected();
    });

    $("#ditolak-filter-tglValidasi").on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        searchRejected();
    });

    searchRejected();

    function showBatalDitolak(a) {
        $.get(`/validasi/data-pengajuan/${a}`)
            .then((res) => {
                $("#batalDitolakId").val(res.id)
                $("#batalDitolakNoPelayanan").val(res.namaPemohon)
                $("#batalDitolakNamaPemohon").val(res.noPelayanan)
                $("#modal-batal-ditolak").modal('show');
            });
    }

    function batalDitolak() {
        $.post('/validasi/batal-verlap', {
            pelayanan: $("#batalDitolakId").val()
        }).then((res) => {
            $("#modal-batal-ditolak").modal('hide');
            searchRejected();
            searchBelumProses();
        })
    }

</script>
@endpush
