<div class="tab-pane fade" id="datagrid-tab-diajukan-verlap" role="tabpanel"
    aria-labelledby="datagrid-tab-diajukanverlap">
    <div class="row table-responsive">
        <table id="table-diajukan-verlap" class="table table-striped table-bordered"
            style="font-size: 12px; width: 100%">
            <thead>
                <tr>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No.</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Jenis Pajak</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Jenis Pelayanan</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Permohonan</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No. Pelayanan</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Diajukan Verlap</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pemohon</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Wajib Pajak</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Aksi</th>
                </tr>
                <tr>
                    <th></th>
                    <th>
                        {{-- <select class="form-control form-control-sm diajukan-filter" id="diajukan-filter-idJenisPajak">
                            <option value="">Semua Jenis Pajak</option>
                            @foreach ($jenispajak as $jp)
                                <option value="{{ $jp->s_id_jenis_pajak }}">{{ $jp->s_nama_jenis_pajak }}</option>
                            @endforeach
                        </select> --}}
                    </th>
                    <th>
                        <select class="form-control form-control-sm diajukan-filter" id="diajukan-filter-idJenisPelayanan">
                            <option value="">Semua Jenis Pelayanan</option>
                            @foreach ($jenispelayanan as $jp)
                                <option value="{{ $jp->s_id_jenis_pelayanan }}">{{ $jp->s_nama_jenis_pelayanan }}</option>
                            @endforeach
                        </select>
                    </th>
                    <th><input class="form-control form-control-sm" type="text" id="diajukan-filter-tglPemohon" readonly></th>
                    <th><input class="form-control form-control-sm diajukan-filter" type="text" id="diajukan-filter-noPelayanan" ></th>
                    <th><input class="form-control form-control-sm" type="text" id="diajukan-filter-tglValidasi" readonly></th>
                    <th><input class="form-control form-control-sm diajukan-filter" type="text" id="diajukan-filter-namaPemohon" ></th>
                    <th><input class="form-control form-control-sm diajukan-filter" type="text" id="diajukan-filter-namaWp" ></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="card-footer pagination-footer"></div>
</div>

@push('scripts')
<script>
const dataGridDiajukanVerlap = datagrid({
        url: '/validasi/datagrid-diajukan-verlap',
        table: "#table-diajukan-verlap",
        columns: [
            {class: ""},
            {class: ""},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: ""},
        ],
        orders: [
            {sortable: false},
            {sortable: true, name: "idJenisPajak"},
            {sortable: true, name: "idJenisPelayanan"},
            {sortable: true, name: "tglPemohon"},
            {sortable: true, name: "noPelayanan"},
            {sortable: true, name: "validasi.tglValidasi"},
            {sortable: true, name: "namaPemohon"},
            {sortable: true, name: "namaWp"},
            {sortable: false},
            {sortable: false},
        ],
        action: [{
            name: 'batal-verlap',
            btnClass: 'btn btn-warning btn-xs',
            btnText: '<i class="fas fa-times"> Batal Ajukan</i>'
        }]
    });

    function searchDiajukanVerlap(){
        dataGridDiajukanVerlap.setFilters({
            idJenisPajak: $("#diajukan-filter-idJenisPajak").val(),
            idJenisPelayanan: $("#diajukan-filter-idJenisPelayanan").val(),
            tglValidasi: $("#diajukan-filter-tglValidasi").val(),
            noPelayanan: $("#diajukan-filter-noPelayanan").val(),
            tglPelayanan: $("#diajukan-filter-tglPemohon").val(),
            namaPemohon: $("#diajukan-filter-namaPemohon").val(),
            namaWp: $("#diajukan-filter-namaWp").val(),
        });
        dataGridDiajukanVerlap.reload();
    }

    $(".diajukan-filter").each(function(){
        $(this).change(function(){
            searchDiajukanVerlap();
        });

        $(this).keyup(function(){
            searchDiajukanVerlap();
        })
    });

    $("#diajukan-filter-tglPemohon").daterangepicker({
        autoUpdateInput: false,
            locale: {
            cancelLabel: 'Clear'
            }
        });

    $("#diajukan-filter-tglPemohon").on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        searchDiajukanVerlap();
    });

    $("#diajukan-filter-tglPemohon").on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        searchDiajukanVerlap();
    });

    $("#diajukan-filter-tglValidasi").daterangepicker({
        autoUpdateInput: false,
            locale: {
            cancelLabel: 'Clear'
            }
        });

    $("#diajukan-filter-tglValidasi").on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        searchDiajukanVerlap();
    });

    $("#diajukan-filter-tglValidasi").on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        searchDiajukanVerlap();
    });

    searchDiajukanVerlap();

    function printHasilValidasi(idPelayanan){
        window.open('/report/validasi/hasil/mutasi/' + idPelayanan);
    }

    function showBatalVerlap(a) {
        $.get(`/validasi/data-pengajuan/${a}`)
            .then((res) => {
                $("#batalVerlapId").val(res.id)
                $("#batalVerlapNoPelayanan").val(res.namaPemohon)
                $("#batalVerlapNamaPemohon").val(res.noPelayanan)
                $("#modal-batal-verlap").modal('show');
            });
    }

    function batalPengajuanVerlap() {
        $.post('/validasi/batal-verlap', {
            pelayanan: $("#batalVerlapId").val()
        }).then((res) => {
            $("#modal-batal-verlap").modal('hide');
            searchDiajukanVerlap();
            searchBelumProses();
        })
    }
</script>
@endpush
