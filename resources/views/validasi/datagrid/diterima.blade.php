<div class="tab-pane fade" id="datagrid-tab-diterima" role="tabpanel" aria-labelledby="datagrid-tab-diterima">
    <div class="row table-responsive">
        <table id="table-approved" class="table table-striped table-bordered" style="font-size: 12px; width: 100%">
            <thead>
                <tr>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No.</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Jenis Pajak</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Jenis Pelayanan</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Permohonan</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No. Pelayanan</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Diterima</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">NIK Pemohon</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pemohon</th>
                    <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Wajib Pajak
                    </th>
                    <th style="background-color: #1fc8e3; vertical-align: top; width: 8rem;" class="text-center">Action
                    </th>
                </tr>
                <tr>
                    <th></th>
                    <th>
                        {{-- <select class="form-control form-control-sm diterima-filter"
                            id="diterima-filter-idJenisPajak">
                            <option value="">Semua Jenis Pajak</option>
                            @foreach ($jenispajak as $jp)
                            <option value="{{ $jp->s_id_jenis_pajak }}">{{ $jp->s_nama_jenis_pajak }}</option>
                            @endforeach
                        </select> --}}
                    </th>
                    <th>
                        <select class="form-control form-control-sm diterima-filter"
                            id="diterima-filter-idJenisPelayanan">
                            <option value="">Semua Jenis Pelayanan</option>
                            @foreach ($jenispelayanan as $jp)
                            <option value="{{ $jp->s_id_jenis_pelayanan }}">{{ $jp->s_nama_jenis_pelayanan }}</option>
                            @endforeach
                        </select>
                    </th>
                    <th><input class="form-control form-control-sm" type="text" id="diterima-filter-tglPelayanan"
                            readonly></th>
                    <th><input class="form-control form-control-sm diterima-filter" type="text"
                            id="diterima-filter-noPelayanan"></th>
                    <th><input class="form-control form-control-sm" type="text" id="diterima-filter-tglValidasi"
                            readonly></th>
                    <th><input class="form-control form-control-sm diterima-filter masked-nik" type="text"
                            id="diterima-filter-nikPemohon"></th>
                    <th><input class="form-control form-control-sm diterima-filter" type="text"
                            id="diterima-filter-namaPemohon"></th>
                    <th><input class="form-control form-control-sm diterima-filter" type="text"
                            id="diterima-filter-namaWp"></th>
                    <th></th>

                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="card-footer pagination-footer"></div>
</div>

@push('scripts')
<script th:inline="javascript" data-th-fragment="js">
    $(function() {
        var dataGridApproved = datagrid({
            url: '/validasi/datagrid-diterima',
            table: "#table-approved",
            columns: [{
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: "text-center"
                },
                {
                    class: "text-center"
                },
                {
                    class: ""
                },
                {
                    class: "text-center"
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                }
            ],
            orders: [
                {sortable: false},
                {sortable: true, name: "idJenisPajak"},
                {sortable: true, name: "idJenisPelayanan"},
                {sortable: true, name: "tglPelayanan"},
                {sortable: true, name: "noPelayanan"},
                {sortable: true, name: "validasi.tglValidasi"},
                {sortable: true, name: "nikPemohon"},
                {sortable: false},
                {sortable: false},
                {sortable: false}

            ],
            action: [{
                    name: 'hasil-validasi',
                    btnClass: 'btn btn-danger btn-xs',
                    btnText: '<i class="fas fa-print"> Hasil Validasi</i>'
                },
                // {
                //     name: 'berkas-validasi',
                //     btnClass: 'btn btn-info btn-xs',
                //     btnText: '<i class="fas fa-print"> Berkas </i>'
                // }
            ]
        });

        function searchApproved() {
            dataGridApproved.setFilters({
                idJenisPajak: $("#diterima-filter-idJenisPajak").val(),
                idJenisPelayanan: $("#diterima-filter-idJenisPelayanan").val(),
                tglPelayanan: $("#diterima-filter-tglPelayanan").val(),
                noPelayanan: $("#diterima-filter-noPelayanan").val(),
                tglValidasi: $("#diterima-filter-tglValidasi").val(),
                nikPemohon: $("#diterima-filter-nikPemohon").val(),
                namaPemohon: $("#diterima-filter-namaPemohon").val(),
                namaWp: $("#diterima-filter-namaWp").val()
            });
            dataGridApproved.reload();
        }

        $(".diterima-filter").each(function() {
            $(this).keyup(function() {
                if ($(this).val().length >= 3 || $(this).val().length == 0 ) {
                    searchApproved();
                }
            })
        });

        $("#diterima-filter-tglPelayanan").daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $("#diterima-filter-tglPelayanan").on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            searchApproved();
        });

        $("#diterima-filter-tglPelayanan").on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            searchApproved();
        });

        $("#diterima-filter-tglValidasi").daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $("#diterima-filter-tglValidasi").on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            searchApproved();
        });

        $("#diterima-filter-tglValidasi").on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            searchApproved();
        });

        searchApproved();
    })

    function printHasilValidasi(idPelayanan) {
        $("#idPelayanan").val(idPelayanan);

        $("#modal-cetak").modal('show');

    }

    function cetak() {
        window.open("{{ url('validasi/cetak-hasil-validasi') }}/" + $("#idPelayanan").val() + '/' + $("#ttd").val(), '_blank');

    }

    function printBerkasValidasi(a) {
        window.open('/report/validasi/hasil/berkas/' + a);
    }
</script>

@endpush
