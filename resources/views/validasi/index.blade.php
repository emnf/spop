@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Validasi
@endsection

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('setting-pemda') }}">Setting</a></li>
                        <li class="breadcrumb-item active">Validasi</li>
                    </ol>
                </div><!-- /.col -->
                {{-- <div class="col-12">
                    <div class="info-box callout callout-danger" id="background2">
                        <div class="col-sm-3">
                            <div class="description-block" style="float: left;">
                                <h1 class="description-header">Validasi</h1>
                            </div>
                            <!-- /.description-block -->
                        </div>
                    </div>
                </div> --}}
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- Info boxes -->
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-hands-helping"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Pengajuan</span>
                                    <span class="info-box-number">{{ $totalPelayanan ?? 0 }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="info-box mb-3">
                                <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-archive"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Diverifikasi</span>
                                    <span class="info-box-number">{{ $totalVerifikasi ?? 0 }}</span>
                                </div>
                            </div>
                        </div>

                        <!-- fix for small devices only -->
                        <div class="clearfix hidden-md-up"></div>

                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="info-box mb-3">
                                <span class="info-box-icon bg-success elevation-1"><i
                                        class="fas fa-clipboard-check"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Belum Divalidasi</span>
                                    <span class="info-box-number">{{ $totalBelumValidasi ?? 0 }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="info-box mb-3">
                                <span class="info-box-icon bg-warning elevation-1"><i
                                        class="fas fa-check-circle"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Divalidasi</span>
                                    <span class="info-box-number">{{ $totalValidasi ?? 0 }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="info-box mb-3">
                                <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-check-circle"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Selesai Verlap</span>
                                    <span class="info-box-number">{{ $totalSelesaiVerlap ?? 0 }}</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if (session('success'))
                        <div class="alert alert-dismissible alert_customs_success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h5><i class="icon fas fa-check"></i> Success!</h5>
                            <span>{{ session('success') }}</span>
                        </div>
                    @endif

                    @if (session('error'))
                        <div class="alert alert-dismissible alert_customs_danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h5><i class="icon fas fa-exclamation-triangle"></i> Gagal!</h5>
                            <span>{{ session('error') }}</span>
                        </div>
                    @endif

                    <div class="card card-danger card-outline card-outline-tabs">
                        <div class="card-header p-0 border-bottom-0">

                            <ul class="nav nav-tabs" id="datagrid-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="datagrid-belum-proses-tab" data-toggle="pill"
                                        href="#datagrid-tab-belum-proses" role="tab"
                                        aria-controls="datagrid-tab-belum-proses" aria-selected="true">Belum
                                        Diproses</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="datagrid-diajukan-verlap-tab" data-toggle="pill"
                                        href="#datagrid-tab-diajukan-verlap" role="tab"
                                        aria-controls="datagrid-tab-diajukan-verlap" aria-selected="false">Diajukan
                                        Verlap</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="datagrid-selesai-verlap-tab" data-toggle="pill"
                                        href="#datagrid-tab-selesai-verlap" role="tab"
                                        aria-controls="datagrid-tab-selesai-verlap" aria-selected="false">Selesai Verlap</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="datagrid-diterima-tab" data-toggle="pill"
                                        href="#datagrid-tab-diterima" role="tab" aria-controls="datagrid-tab-diterima"
                                        aria-selected="false">Diterima</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="datagrid-ditolak-tab" data-toggle="pill"
                                        href="#datagrid-tab-ditolak" role="tab" aria-controls="datagrid-tab-ditolak"
                                        aria-selected="false">Ditolak</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="datagrid-semua-tab" data-toggle="pill"
                                        href="#datagrid-tab-semua" role="tab" aria-controls="datagrid-tab-semua"
                                        aria-selected="false">Semua
                                        Data</a>
                                </li>
                            </ul>

                        </div>

                        <div class="card-body">
                            <div class="tab-content" id="datagrid-tab-content">
                                @include('validasi.datagrid.belum-proses')
                                @include('validasi.datagrid.diajukan-verlap')
                                @include('validasi.datagrid.selesai-verlap')
                                @include('validasi.datagrid.diterima')
                                @include('validasi.datagrid.ditolak')
                                @include('validasi.datagrid.semua')
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-cetak">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-danger">
                            <h6 class="modal-title"><span class=""></span> &nbsp;Pilih Pejabat Mengetahui</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <p>Pilih Pejabat Mengetahui</p>

                            <div class="row">
                                <input type="hidden" class="form-control form-control-sm" id="idPelayanan" readonly>
                                <div class="col-sm-4">Mengetahui</div>
                                <div class="col-sm-8">
                                    <select class="form-control form-control-sm" id="ttd">
                                        {{-- <option th:each="datapegawai: ${datapegawai}"
                                        th:text="${datapegawai.namaPegawai + ' - ' + datapegawai.jabatanPegawai}"
                                        th:value="${datapegawai.idPegawai}"></option> --}}
                                        @foreach ($datapegawai as $item)
                                            <option value="{{ $item->s_id_pegawai }}">{{ $item->s_nama_pegawai }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span
                                    class="fas fa-times-circle"></span> batal</button>
                            <button type="button" class="btn btn-danger btn-sm" onclick="cetak()"><span
                                    class="fas fa-print"></span> Cetak</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-batal-verlap">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-danger">
                            <h6 class="modal-title"><span class=""></span> &nbsp;Batal Diajukan Verifikasi Lapangan?
                            </h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <input type="hidden" name="batalVerlapId" id="batalVerlapId">
                            <div class="row">
                                <div class="col-sm-4">No</div>
                                <div class="col-sm-8">
                                    <input class="form-control form-control-sm" id="batalVerlapNoPelayanan" readonly />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">Nama Pemohon</div>
                                <div class="col-sm-8">
                                    <input class="form-control form-control-sm" id="batalVerlapNamaPemohon" readonly />
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span
                                    class="fas fa-times-circle"></span> batal</button>
                            <button type="button" class="btn btn-danger btn-sm" onclick="batalPengajuanVerlap()"><span
                                    class="fas fa-times"></span> Proses</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-batal-ditolak">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-danger">
                            <h6 class="modal-title"><span class=""></span> &nbsp;Batal Ditolak Pelayanan?</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <input type="hidden" name="batalDitolakId" id="batalDitolakId">
                            <div class="row">
                                <div class="col-sm-4">No</div>
                                <div class="col-sm-8">
                                    <input class="form-control form-control-sm" id="batalDitolakNoPelayanan" readonly />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">Nama Pemohon</div>
                                <div class="col-sm-8">
                                    <input class="form-control form-control-sm" id="batalDitolakNamaPemohon" readonly />
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span
                                    class="fas fa-times-circle"></span> batal</button>
                            <button type="button" class="btn btn-danger btn-sm" onclick="batalDitolak()"><span
                                    class="fas fa-times"></span> Proses</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@push('scripts')
    <script></script>
@endpush
