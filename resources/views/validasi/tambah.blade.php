@extends('layouts.app')

@section('title')
    Kepoin || Tambah Validasi
@endsection

@section('content')
    <section class="content">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <section>
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a th:href="@{/pelayanan}">Home</a></li>
                                <li class="breadcrumb-item active">Validasi</li>
                            </ol>
                        </section>
                    </div>
                    <div class="col-12">
                        <div class="info-box callout callout-danger" id="background2">
                            <div class="col-sm-12">
                                <div class="" style="float: left;">
                                    <h5 class="">FORM VALIDASI <span></span></h5>
                                    <span class="float">Silahkan untuk melakukan validasi terhadap permohonan yang
                                        sudah diajukan.</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /.container-fluid -->
            <div class="container-fluid">
                <div class="row" th:if="${errorValidasi}">
                    <div class="col-md-12">
                        <p th:text="${errorValidasi}"></p>
                    </div>
                </div>

                @include('validasi.form', [
                    'jenisPajak' => $pelayanan->jenisPajak,
                    'jenisLayanan' => $pelayanan->jenisPelayanan,
                    'objekPajak' => $pelayanan->objekPajak,
                    'wajibPajak' => $pelayanan->wajibPajak,
                ])
            </div>
        </div>
    </section>
@endsection
