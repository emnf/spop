<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">

<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <link rel="icon" type="image/png" href="/asset/images/elayanan3.png">
    <title>KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }}</title>Registrasi
    Akun</title>
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="plugins/toastr/build/toastr.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="plugins/adminlte/dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- Animation CSS -->
    <link href="/landing/css/animate.min.css" rel="stylesheet">

    <link href="/signup/fonts/material-icon/css/material-design-iconic-font.min.css" rel="stylesheet">
    <link href="/signup/css/style.css" rel="stylesheet">
    <style>
        .bd-example-modal-lg .modal-dialog {
            display: table;
            position: relative;
            margin: 0 auto;
            top: calc(50% - 24px);
        }

        .bd-example-modal-lg .modal-dialog .modal-content {
            background-color: transparent;
            border: none;
            box-shadow: none;
        }
    </style>
</head>

<body>
    <div class="main">
        <!-- Sign up form -->
        <section class="signup animated fadeInDown">
            <div class="container">
                <div class="signup-content">
                    <div class="signup-form">
                        <span style="color:#c4ac0e" th:text="${notif != null} ? ${notif} : ''"></span>
                        <h2 class="form-title">Registrasi Akun</h2>
                        <form method="POST" onsubmit="return checkForm();" action="register"
                            class="register-form form-validation" id="register-form">
                            @csrf
                            <div class="form-group">
                                <label for="nik"><i class="zmdi zmdi-card"></i></label>
                                <input type="text" placeholder="NIK" style="margin-bottom: 10px" name="nik"
                                    id="nik"
                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                    required maxlength="16" minlength="16">
                                @if ($errors->has('nim'))
                                    <span class="text-danger">NIK ini tidak bisa digunakan</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                                <input type="hidden" name="idRegisterUser" id="idRegisterUser">
                                <input type="hidden" name="tglRegisterUser" id="tglRegisterUser">
                                <input type="text" name="name" id="name" placeholder="Nama Lengkap"
                                    value="{{ old('name') }}" required />
                                @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="email"><i class="zmdi zmdi-email"></i></label>
                                <input type="text" name="email" id="email" placeholder="Email"
                                    value="{{ old('email') }}" required />
                                @if ($errors->has('email'))
                                    <span class="text-danger">Email ini tidak bisa digunakan</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="username"><i class="zmdi zmdi-account material-icons-name"></i></label>
                                <input type="text" name="username" id="username" value="{{ old('username') }}"
                                    placeholder="Nama Akun" required />
                                @if ($errors->has('username'))
                                    <span class="text-danger">Username ini tidak bisa digunakan</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="noHpRegisterUser"><i class="zmdi zmdi-smartphone-info"></i></label>
                                <input type="text" name="noHpRegisterUser" id="noHpRegisterUser" placeholder="No HP"
                                    value="{{ old('noHpRegisterUser') }}"
                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                    maxlength="15" />
                                @if ($errors->has('noHpRegisterUser'))
                                    <span class="text-danger">Masukan No HP anda</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="password"><i class="zmdi zmdi-lock"></i></label>
                                <input type="password" name="password" id="password" placeholder="Kode Sandi" />
                                @if ($errors->has('password'))
                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="password_confirmation"><i class="zmdi zmdi-lock-outline"></i></label>
                                <input type="password" name="password_confirmation" id="password_confirmation"
                                    placeholder="Ulangi Kode Sandi" />
                                @if ($errors->has('password_confirmation'))
                                    <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="agreeTermRegisterUser" id="agreeTermRegisterUser"
                                    required />
                                <label for="agreeTermRegisterUser" class="label-agree-term"><span><span></span></span>
                                    Saya menyetujui semua pernyataan sesuai
                                    <a href="javascript:void(0)">
                                        <u>Ketentuan Layanan</u>
                                    </a>
                                </label>
                            </div>
                            <div class="form-group form-button">
                                <input type="submit" name="signup" id="signup" class="form-submit"
                                    value="Daftar User" />
                            </div>
                        </form>
                    </div>
                    <div class="signup-image">
                        <figure><img src="landing/img/laptop2.png" alt="sing up image"></figure>
                        <a href="/login" class="signup-image-link"><u>Saya sudah memiliki user</u></a>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="modal fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="col-12">
                    <span class="spinner-grow text-muted"></span>
                    <span class="spinner-grow text-primary"></span>
                    <span class="spinner-grow text-success"></span>
                    <span class="spinner-grow text-info"></span>
                    <span class="spinner-grow text-warning"></span>
                    <span class="spinner-grow text-danger"></span>
                    <span class="spinner-grow text-secondary"></span>
                    <span class="spinner-grow text-dark"></span>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <script src="plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="plugins/jquery-validation/additional-methods.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Toastr -->
    <script src="plugins/toastr/build/toastr.min.js"></script>
    <!-- AdminLTE App -->
    <script src="plugins/adminlte/dist/js/adminlte.min.js"></script>
    <script type="text/javascript">
        $(function() {
            $('.form-validation').validate({
                rules: {
                    passwordRegisterUser: {
                        minlength: 6
                    },
                    passwordRegisterUser_confirm: {
                        minlength: 6,
                        equalTo: "#passwordRegisterUser"
                    }
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.addClass('invalid-feedback');
                    $(error).insertAfter(element.closest('input,select,textarea'));
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        })

        function checkForm() {
            $('.modal').modal('show');
            var confirm = $('#agreeTermRegisterUser');
            if (confirm.is(":checked") == false) {
                setTimeout(function() {
                    $('.modal').modal('hide');
                }, 1000);
                confirm.focus();
                $(document).Toasts('create', {
                    body: 'Isi formulir & centang persetujuan.',
                    title: 'Pemberitahuan',
                    subtitle: '',
                    icon: 'fas fa-envelope fa-lg',
                });
                return false;
            } else {
                setTimeout(function() {
                    $('.modal').modal('hide');
                }, 1000);
                return true;
            }

        }

        // $("#usernameRegisterUser").keyup(function(){
        //     checkUsername();
        // });

        function checkUsername() {
            $.get("/register-user/check-username/" + $("#usernameRegisterUser").val())
                .then(function(res) {
                    var data = res.data;
                    var code = res.code;
                    if (code == '200') {
                        $(document).Toasts('create', {
                            class: 'bg-danger',
                            title: 'Pemberitahuan',
                            subtitle: '',
                            body: 'Username sudah ada, mohon gunakan user lain.'
                        });
                        $("#usernameRegisterUser").val('')
                        $("#usernameRegisterUser").focus()
                    }
                });
        }
    </script>
    {{-- <th:block th:switch="${alert}">
    <script th:case="'success'" type="text/javascript">
        $(function() {
            $(document).Toasts('create', {
                class: 'bg-success',
                title: 'Pemberitahuan',
                subtitle: '',
                body: 'Registrasi berhasil, silahkan cek email anda.'
            })
        });
    </script>
    <script th:case="'active-account'" type="text/javascript">
        $(function() {
            $(document).Toasts('create', {
                class: 'bg-success',
                title: 'Pemberitahuan',
                subtitle: '',
                body: 'Akun anda sudah aktif, bisa masuk aplikasi dengan username & password.'
            })
        });
    </script>
    <script th:case="'error-active-account'" type="text/javascript">
        $(function() {
            $(document).Toasts('create', {
                class: 'bg-danger',
                title: 'Pemberitahuan',
                subtitle: '',
                body: 'Data tidak ditemukan'
            })
        });
    </script>
    <script th:case="'error'" type="text/javascript">
        $(function() {
             $(document).Toasts('create', {
                class: 'bg-danger',
                title: 'Pemberitahuan',
                subtitle: '',
                body: 'Registrasi gagal, silahkan coba lagi.'
              });
        });
    </script>
</th:block> --}}
</body>

</html>
