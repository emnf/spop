@extends('layouts.auth')

@section('content')
    @if (session('success'))
        <div class="alert alert-success alert-dismissible callout callout-success" id="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h5><i class="icon fas fa-check"></i> Berhasil!</h5>
            {{ session('success') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible callout callout-danger" id="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h5><i class="icon fas fa-ban"></i> Gagal!</h5>
            {{ session('error') }}
        </div>
    @endif
    <div class="login-box">
        <div class="login-logo" style="margin-top: 20px">
            <img src="{{ $pemda->s_logo }}" width="110"><br>
            <a href="#"><b style="font-size:16pt;">KEPOIN</span></b></a>
            <a href="#">
                <p style="font-size:12pt;">(KEMUDAHAN PELAYANAN PBB-P2 ONLINE TERINTEGRASI)</span></p>
            </a>
        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body" style="border-top:2px solid #dc3545;">
                <form method="POST" class="form-validation" id="formLogin" action="{{ route('login') }}">
                    @csrf

                    <div class="form-group">
                        <input autocomplete="off" class="form-control" id="username" name="username"
                            value="{{ old('username') ?? '' }}" placeholder="Username" required type="username"
                            autofocus="true">
                    </div>
                    <div class="form-group">
                        <input autocomplete="off" class="form-control" id="password" name="password" placeholder="Password"
                            required type="password">
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <div class="icheck-danger">
                                <input id="remember" name="remember_me" type="checkbox">
                                {{-- <input id="remember_me" type="checkbox" name="remember" value=""> --}}

                                <label for="remember">
                                    {{ __('Remember me') }}
                                </label>
                            </div>
                        </div>

                        <!-- /.col -->
                        <div class="col-4">
                            <button class="btn btn-danger btn-block" type="submit" id="btnLogin">
                                {{ __('Log in') }}
                            </button>
                        </div>
                        <!-- /.col -->
                    </div>

                </form>
                <p class="mb-3"></p>
                {{-- <p class="mb-0">
                    <small>Belum memiliki akun? <a href="{{ url('register') }}">Daftar Akun baru</a></small>
                </p> --}}

                <center><img src="bsre/bsre.png" style="width: 130px;"></center>
            </div>
            <!-- /.login-card-body -->
            <div class="card-footer text-center">
                {{-- <small>{{ env('APP_YEAR') ?? 2022 }} &copy; <a href="" target="_blank">{{ $nama_instansi ?? 'Mitra Prima
                    Utama'}}</a></small> --}}
                <small>{{ env('APP_YEAR') ?? 2022 }} &copy; <a href=""
                        target="_blank">{{ $pemda->s_nama_singkat_instansi }}
                        {{ ucwords(strtolower($pemda->s_nama_kabkota)) }}</a></small>
            </div>
        </div>
    </div>
@endsection
