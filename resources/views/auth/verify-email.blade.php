@extends('layouts.auth')

@section('content')
<div class="mail-verified">
    <div class="login-logo">
        <img src="{{ asset('asset/images/elayanan3.png') }}" width="100"><br>
        <a href="#"><b style="font-size:16pt;">Selamat Datang Di <span> Elayanan Pajak</span></b></a>
    </div>
    @if (session('status') == 'verification-link-sent')
    <div class="alert alert-success alert-dismissible callout callout-success" id="alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h5><i class="icon fas fa-check"></i> Berhasil!</h5>
        {{ session('success') }}
    </div>
    @endif
    <div class="card">
        <div class="card-body " style="border-top:2px solid #dc3545;">
            <p class="login-box-msg"> {{ __('Terimakasih sudah mendaftar, mohon periksa email anda.')}}<br>{{ __('Kami sudah mengirim
                link verifikasi untuk aktivasi akun anda.')}}<br>{{ __(' Jika anda belum menerimanya silahkan klik link di bawah') }}</p>
            <div class="row login-box-msg">
                <form method="POST" action="{{ route('verification.send') }}">
                    @csrf
                    <button type="submit" class="btn btn-link">
                        {{ __('Kirim ulang Email Verifikasi') }}
                    </button>
                </form>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf

                    <button type="submit" class="btn btn-link">
                        {{ __('Log Out') }}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
