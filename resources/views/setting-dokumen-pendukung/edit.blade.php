@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Setting Dokumen Pendukung
@endsection

@section('content')
    <section class="content">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Setting</a></li>
                            <li class="breadcrumb-item active">Grid</li>
                        </ol>
                    </div>
                    <div class="col-12">
                        <div class="info-box" id="background2">
                            <div class="col-sm-3">
                                <div class="description-block" style="float: left;">
                                    <h1 class="description-header">SETTING DOKUMEN PENDUKUNG</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-outline card-info">
                            <div class="card-header">
                                <div class="col-sm-12" style="margin-bottom: 10px;">
                                    Form Setting Dokumen Pendukung [EDIT]
                                </div>
                            </div>
                            <form class="form-horizontal form-validation" method="POST"
                                action="{{ route('setting-dokumen-pendukung.update', $data->s_id_dokumen_pendukung) }}">
                                @csrf
                                @method('PUT')

                                <div class="card-body">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_id_kategori_pajak">Kategori
                                            Pajak</label>
                                        <div class="col-sm-4">
                                            <input id="s_id_hasil_pelayanan" name="s_id_hasil_pelayanan" type="hidden"
                                                value="{{ $data->s_id_hasil_pelayanan }}">
                                            <select class="form-control" id="s_id_kategori_pajak" name="s_id_kategori_pajak"
                                                onchange="comboJenisPelayanan(this.value)" required>
                                                <option value="">Silahkan Pilih</option>
                                                @foreach ($jenisPajak as $row)
                                                    <option
                                                        {{ $data->s_id_kategori_pajak == $row['s_id_jenis_pajak'] ? 'selected' : '' }}
                                                        value="{{ $row['s_id_jenis_pajak'] }}">
                                                        {{ $row['s_nama_jenis_pajak'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_id_jenis_pelayanan">Jenis
                                            Pelayanan</label>
                                        <div class="col-sm-4">
                                            <select class="form-control" name="s_id_jenis_pelayanan"
                                                id="s_id_jenis_pelayanan" required>
                                                <option value="">Silahkan Pilih</option>
                                                @foreach ($jenisPelayanan as $row)
                                                    <option
                                                        {{ $data->s_id_jenis_pelayanan == $row['s_id_jenis_pelayanan'] ? 'selected' : '' }}
                                                        value="{{ $row['s_id_jenis_pelayanan'] }}">
                                                        {{ $row['s_nama_jenis_pelayanan'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_nama_dokumen_pendukung">Nama Dokumen
                                            Pendukung</label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name="s_nama_dokumen_pendukung"
                                                id="s_nama_dokumen_pendukung" value="{{ $data->s_nama_dokumen_pendukung }}"
                                                required>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-info" type="submit">SIMPAN</button>
                                    <a class="btn btn-danger btn-default float-right"
                                        href="{{ route('setting-jenis-hasil-pelayanan') }}">BATAL</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
    <script type="text/javascript">
        function comboJenisPelayanan(id) {
            $.ajax({
                url: '/setting-dokumen-pendukung/combo-jenis-pelayanan/' + id,
                type: 'POST',
                data: {
                    s_id_kategori_pajak: id
                }
            }).then(function(data) {
                $("#s_id_jenis_pelayanan option[value!='']").remove();
                $.each(data, function(key, val) {
                    $('#s_id_jenis_pelayanan').append(
                        `<option value="${val.s_id_jenis_pelayanan}">${val.s_nama_jenis_pelayanan}</option>`
                    );
                });
            });
        }
    </script>
@endpush
