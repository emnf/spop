@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Setting Dokumen Pendukung
@endsection

@section('content')
    <section class="content" layout:fragment="content">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">Home</li>
                            <li class="breadcrumb-item active"><a href="#">Grid</a></li>
                        </ol>
                    </div>
                    <div class="col-12">
                        <div class="info-box" id="background2">
                            <div class="col-sm-3">
                                <div class="description-block" style="float: left;">
                                    <h1 class="description-header">SETTING DOKUMEN PENDUKUNG</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-outline card-info">
                            <div class="card-header">
                                <div class="col-sm-12" style="margin-bottom: 10px;">
                                    <a href="{{ route('setting-dokumen-pendukung.tambah') }}"
                                        class="btn btn-info btn-sm"><span class="fas fa-plus-square"></span> Tambah</a>
                                    <button class="btn btn-danger btn-sm" onclick="printdaftar('PDF')"><span
                                            class="fas fa-file-pdf"></span> &nbsp;PDF</button>
                                    <button class="btn btn-success btn-sm" onclick="printdaftar('XLS')"><span
                                            class="fas fa-file-excel"></span> &nbsp;XLS</button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive">
                                <table class="table table-bordered table-striped table-hover" id="datagrid-table">
                                    <thead>
                                        <tr>
                                            <th style="background-color: #1fc8e3" class="text-center">No</th>
                                            <th style="background-color: #1fc8e3" class="text-center">Kategori Pajak</th>
                                            <th style="background-color: #1fc8e3" class="text-center">Jenis Pelayanan</th>
                                            <th style="background-color: #1fc8e3" class="text-center">Dokumen Pendukung</th>
                                            <th style="background-color: #1fc8e3" class="text-center">Action</th>
                                        </tr>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th>
                                                <select class="form-control form-control-sm"
                                                    id="filter-s_id_jenis_pelayanan">
                                                    <option value="">Silahkan Pilih</option>
                                                    @foreach ($jenisPelayanan as $row)
                                                        <option value="{{ $row['s_id_jenis_pelayanan'] }}">
                                                            {{ $row['s_nama_jenis_pelayanan'] }}</option>
                                                    @endforeach
                                                </select>
                                            </th>
                                            <th><input type="text" class="form-control form-control-sm"
                                                    id="filter-s_nama_dokumen_pendukung"></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center" colspan="4"> Tidak ada data.</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer clearfix pagination-footer">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
@push('scripts')
    <script type="text/javascript">
        var datatables = datagrid({
            url: '/setting-dokumen-pendukung/datagrid',
            table: "#datagrid-table",
            serverSide: true,
            columns: [{
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
            ],
            orders: [{
                    sortable: false
                },
                {
                    sortable: true,
                    name: "s_id_kategori_pajak"
                },
                {
                    sortable: true,
                    name: "s_id_jenis_pelayanan"
                },
                {
                    sortable: true,
                    name: "s_nama_dokumen_pendukung"
                },
                {
                    sortable: false
                }
            ],
            action: [{
                    name: 'edit',
                    btnClass: 'btn btn-outline-primary btn-sm mx-1',
                    btnText: '<i class="fas fa-pen"></i>'
                },
                {
                    name: 'delete',
                    btnClass: 'btn btn-outline-danger btn-sm mx-1',
                    btnText: '<i class="fas fa-trash"></i>'
                }
            ]

        });

        $("#filter-s_nama_dokumen_pendukung").keyup(function() {
            search();
        });

        $("#filter-s_id_jenis_pelayanan, #filter-s_id_kategori_pajak").change(function() {
            search();
        });

        function search() {
            datatables.setpageNumber(0);
            datatables.setFilters({
                s_nama_dokumen_pendukung: $("#filter-s_nama_dokumen_pendukung").val(),
                s_id_jenis_pelayanan: $("#filter-s_id_jenis_pelayanan").val(),
                s_id_kategori_pajak: $("#filter-s_id_kategori_pajak").val()
            });
            datatables.reload();
        }
        search();

        function showDeleteDialog(a) {
            Swal.fire({
                title: 'Hapus Data',
                text: "Apa anda Yakin ingin menghapus ?",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Tidak',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '/setting-dokumen-pendukung/delete/' + a,
                        type: 'delete',
                        dataType: "JSON",
                        data: {
                            s_id_dokumen_pendukung: a
                        },
                        success: function(request, error) {
                            if (request == 'success') {
                                Toast.fire({
                                    icon: 'success',
                                    title: 'Data Berhasil Dihapus'
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: 'Data Gagal Dihapus'
                                });
                            }
                            datatables.reload();
                        }
                    });
                }
            })
        }

        function cetakData(a) {
            window.open(
                `{{ url('setting/dokumen-pendukung/cetak-daftar') }}?type-cetak=${a}`
            );
        }
    </script>
@endPush
