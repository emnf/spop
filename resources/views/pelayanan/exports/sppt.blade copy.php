    @php
        error_reporting(0);
        ini_set('pcre.backtrack_limit', 10000000);
        // if ($v->njop_sppt < $v->NJOP_MAX) {
        //     $v_tarif_dasar = '0,11%';
        // } elseif ($v->njop_sppt > $v->NJOP_MAX) {
        //     $v_tarif_dasar = '0,11%';
        // }

        // function hitungDenda($tgltempo, $jmlhpajak)
        // {
        //     $tgljatuhtempo = date('Y-m-d', strtotime($tgltempo));
        //     $tglsekarang = date('Y-m-d');
        //     $ts1 = strtotime($tgljatuhtempo);
        //     $ts2 = strtotime($tglsekarang);

        //     $year1 = date('Y', $ts1);
        //     $year2 = date('Y', $ts2);

        //     $month1 = date('m', $ts1);
        //     $month2 = date('m', $ts2);

        //     $day1 = date('d', $ts1);
        //     $day2 = date('d', $ts2);
        //     if ($day1 < $day2) {
        //         $tambahanbulan = 1;
        //     } else {
        //         $tambahanbulan = 0;
        //     }

        //     $jmlbulan = ($year2 - $year1) * 12 + ($month2 - $month1 + $tambahanbulan);
        //     if ($jmlbulan > 24) {
        //         $jmlbulan = 24;
        //         $jmldenda = (($jmlbulan * 2) / 100) * $jmlhpajak;
        //     } elseif ($jmlbulan <= 0) {
        //         $jmlbulan = 0;
        //         $jmldenda = (($jmlbulan * 2) / 100) * $jmlhpajak;
        //     } else {
        //         $jmldenda = (($jmlbulan * 2) / 100) * $jmlhpajak;
        //     }
        //     return round($jmldenda);
        // }
    @endphp
    <style>
        h3 {
            text-align: center;
        }

        .background-gray-50 {
            background: rgb(226, 225, 225);
        }

        .background-black {
            background: black;
            color: white;
        }

        .text-right {
            text-align: right;
        }

        .text-center {
            text-align: center;
        }

        .border-left {
            border-left: 1px solid #000;
        }

        .border-right {
            border-right: 1px solid #000;
        }

        .border-top {
            border-top: 1px solid #000;
        }

        .border-bottom {
            border-bottom: 1px solid #000;
        }

        .posisi {
            position: absolute;
            font-size: 9pt;
        }

        @page {
            margin: 0;
        }

        .image {

            position: absolute;

            width: 100%;

            height: 100%;

            background-image: url({{ $blanko->thumbnail }});

            background-size: cover;

            background-color: purple;

            /* padding-bottom:153%; */

        }
    </style>
    @foreach ($data as $v)
        <div>

            <div class="image"></div>
            {{-- <div
    style="background: url('{{ $blanko->thumbnail }}') no-repeat fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    padding-bottom:153%;
    ">
</div> --}}


            <div class="posisi" style="top:3.9em;right:3em;">{{ $v->nm_sektor == 'PERKOTAAN' ? '411312' : '411311' }}
            </div>
            <div class="posisi" style="top:4.9em;right:3em;">{{ $v->nm_sektor }}</div>
            <div class="posisi" style="top:5.9em;right:2.5em;">#{{ $v->codingsppt . '/' . $v->siklus_sppt }}#</div>
            <div class="posisi" style="top:4.9em;right:10em;font-size:10pt;"><strong>{{ $v->thn_pajak_sppt }}</strong>
            </div>
            <div class="posisi" style="top:5.9em;left:4em;">{{ $nop }}</div>

            {{-- Letak Objek Pajak --}}
            <div class="posisi" style="top:8em;left:3em;width;">{{ $v->jalan_op . ' ' . $v->blok_kav_no_op }}</div>
            <div class="posisi" style="top:9em;left:3em;">{{ ' RT.' . $v->rt_op . ' RW.' . $v->rw_op }}</div>
            <div class="posisi" style="top:10em;left:3em;">{{ $v->nm_kelurahan }}</div>
            <div class="posisi" style="top:11em;left:3em;">{{ $v->nm_kecamatan }}</div>

            {{-- Nama Alamat Wajib Pajak --}}
            <div class="posisi" style="top:8em;left:23em;width:180px;">{{ $v->nm_wp_sppt }}</div>
            <div class="posisi" style="top:9em;left:23em;">{{ $v->jln_wp_sppt . ' ' . $v->blok_kav_no_sppt }}</div>
            <div class="posisi" style="top:10em;left:23em;">{{ 'RT.' . $v->rt_wp_sppt . ' RW.' . $v->rw_wp_sppt }}
            </div>
            <div class="posisi" style="top:11em;left:23em;">{{ $v->kelurahan_wp_sppt }}</div>
            <div class="posisi" style="top:12em;left:23em;">{{ $v->kota_wp_sppt }}</div>
            <div class="posisi" style="top:13em;left:23em;">{{ $v->npwp_sppt }}</div>

            {{-- Detail Objek Pajak --}}
            <div class="posisi" style="top:15.5em;left:3em;">BUMI</div>
            <div class="posisi" style="top:15.5em;right:28.5em;width:70px;text-align:right;">{{ $v->luas_bumi_sppt }}
            </div>
            <div class="posisi" style="top:15.5em;right:25em;width:30px;text-align:right;">{{ $v->kd_kls_tanah }}</div>
            <div class="posisi" style="top:15.5em;right:14em;width:130px;text-align:right;">
                {{ number_format($v->luas_bumi_sppt > 0 ? $v->njop_bumi_sppt / $v->luas_bumi_sppt : 0, 0, ',', '.') }}
            </div>
            <div class="posisi" style="top:15.5em;right:2.5em;width:130px;text-align:right;">
                {{ number_format($v->njop_bumi_sppt, 0, ',', '.') }}</div>

            <div class="posisi" style="top:16.5em;left:3em;">BANGUNAN</div>
            <div class="posisi" style="top:16.5em;right:28.5em;width:70px;text-align:right;">{{ $v->luas_bng_sppt }}
            </div>
            <div class="posisi" style="top:16.5em;right:25em;width:30px;text-align:right;">{{ $v->kd_kls_bng }}</div>
            <div class="posisi" style="top:16.5em;right:14em;width:130px;text-align:right;">
                {{ number_format($v->luas_bng_sppt > 0 ? $v->njop_bng_sppt / $v->luas_bng_sppt : 0, 0, ',', '.') }}
            </div>
            <div class="posisi" style="top:16.5em;right:2.5em;width:130px;text-align:right;">
                {{ number_format($v->njop_bng_sppt, 0, ',', '.') }}</div>

            <div class="posisi" style="top:19.8em;right:2.5em;">{{ number_format($v->njop_sppt, 0, ',', '.') }}</div>
            <div class="posisi" style="top:20.8em;right:2.5em;">{{ number_format($v->njoptkp_sppt, 0, ',', '.') }}
            </div>
            <div class="posisi" style="top:21.8em;right:2.5em;">
                {{ number_format($v->njop_sppt - $v->njoptkp_sppt, 0, ',', '.') }}</div>
            <div class="posisi" style="top:22.4em;left:15em;">
                {{ $v_tarif_dasar . ' x ' . number_format($v->njop_sppt - $v->njoptkp_sppt, 0, ',', '.') }}</div>
            <div class="posisi" style="top:22.6em;right:2.5em;">
                {{ number_format($v->pbb_terhutang_sppt, 0, ',', '.') }}
            </div>

            @if ($v->faktor_pengurangan_sppt > 0)
                <div class="posisi" style="top:23.5em;left:2.1em;font-size: 8pt;">Faktor Pengurang/Pengaturan Pengenaan
                </div>
                <div class="posisi" style="top:23.3em;right:2.5em;">
                    {{ number_format($v->faktor_pengurangan_sppt, 0, ',', '.') }}
                </div>
            @endif

            <div class="posisi" style="top:24.8em;right:2.5em;">
                {{ number_format($v->pbb_yg_hrs_dibayar_sppt, 0, ',', '.') }}
            </div>
            <div class="posisi" style="top:27em;left:3em;"><i>{{ strtoupper($terbilang) }} RUPIAH </i></div>



            <div class="posisi" style="top:29em;left:11em;">
                {{ strtoupper(date('d M Y', strtotime($v->tgl_jatuh_tempo_sppt))) }}</div>
            <div class="posisi" style="top:31em;left:2.5em;text-align:center;width:180px;">{{ $v->nm_tp }}
            </div>

            <div class="posisi" style="top:29.2em;right:7em;">
                {{ strtoupper(date('d M Y', strtotime($v->tgl_terbut_sppt))) }}</div>

            {{-- QRCODE --}}
            <div class="posisi" style="top:33em;left:23em;">
                <table class="" style="font-size: 9pt;width:90%;">
                    <tr>
                        <td rowspan="2" class="" style="padding: 9px;width:25%;">
                            {{-- {!! str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $qrcode) !!} --}}
                        </td>
                        <td class="text-center">
                            {{-- Ditandatangani secara elektronik oleh:<br> --}}
                            {{-- {{ $pejabat->NM_JABATAN }} {{ strtoupper($pemda->s_namainstansi) }} --}}
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                            {{ $pejabat->nm_pegawai }}<br>
                            {{-- {{ $refUmum->NM_REF }}<br> --}}
                            NIP. {{ $pejabat->nip_baru }}
                        </td>
                    </tr>
                </table>
            </div>


            {{-- LEMBAR BANK --}}
            <div class="posisi" style="top:37.9em;left:9em;font-size:8pt;width:180px;">{{ $v->nm_wp_sppt }}</div>
            <div class="posisi" style="top:38.9em;left:11.5em;font-size:8pt;">{{ $v->nm_kecamatan }}</div>
            <div class="posisi" style="top:39.9em;left:15.5em;font-size:8pt;">{{ $v->kelurahan_wp_sppt }}</div>
            <div class="posisi" style="top:41em;left:9em;font-size:8pt;">{{ $nop }}</div>
            <div class="posisi" style="top:41.9em;left:9em;font-size:8pt;">
                {{ $v->thn_pajak_sppt . $angkakontrol . ' / Rp. ' . number_format($v->pbb_yg_hrs_dibayar_sppt, 0, ',', '.') }}
            </div>
        </div>
    @endforeach
