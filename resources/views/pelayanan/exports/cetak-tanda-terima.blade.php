@php
    // dd($pelayanan);
@endphp
@extends('layouts.print')

@section('title')
    <title>Cetak Tanda Terima</title>
@endsection

@push('styles')
    <style>
        @page {
            margin: 30px 100px;
        }
    </style>
@endpush

@section('body')

    <table style="width: 100%; border-bottom: 5px double #000" cellspacing="0" cellpadding="0">
        <tr>
            <td style="text-align: center; width: 20%">
                <img style="width: 45%" title="LOGO" src="{{ $pemda->s_logo }}" />
            </td>
            <td style="text-align: center">
                <span style="font-size: 11pt;font-weight: bold">PEMERINTAH
                    {{ strtoupper($pemda->s_nama_kabkota) }}</span><br>
                <span style="font-size: 14pt;font-weight: bold">{{ strtoupper($pemda->s_nama_instansi) }}</span><br>
                <span style="font-size: 9pt;">
                    {{ ucwords(strtolower($pemda->s_alamat_instansi)) . ' Telp/Fax. ' . $pemda->s_notelp_instansi . ', Kode Pos ' . $pemda->s_kode_pos }}
                </span><br><br>
            </td>
            <td style="width: 20%">
                &nbsp;
            </td>
        </tr>
    </table>
    @if (in_array($pelayanan->t_id_jenis_pelayanan, [3]))
        <table style="width: 100%;" cellspacing="0" cellpadding="0">
            <tr>
                <td style="text-align: center;padding-top: 24px;">
                    <span style="font-size: 12pt; text-align: center;font-weight: bold">TANDA TERIMA</span><br>
                    <span style="text-align: center">TELAH DILAKUKAN PERUBAHAN DATA SPPT PBB DARI</span><br>
                    <span style="text-align: center">DATA LAMA</span>
                </td>
            </tr>
        </table>
        <table style="border-bottom: 1px solid #000;width: 100%" cellspacing="0" cellpadding="0">
            <tr>
                <td style="width: 30%">Nama</td>
                <td style="width: 15px;">:</td>
                <td colspan="3">{{ $dataLama['t_nama_wp'] }}</td>
            </tr>
            <tr>
                <td>Alamat Subjek Pajak</td>
                <td>:</td>
                <td colspan="3">{{ $dataLama['t_jalan_wp'] }}, RT. {{ $dataLama['t_rt_wp'] }}, RW.
                    {{ $dataLama['t_rw_wp'] }}
                </td>
            </tr>
            <tr>
                <td>Alamat Objek Pajak</td>
                <td>:</td>
                <td colspan="3">
                    {{ $dataLama['t_jalan_op'] . ' RT. ' . $dataLama['t_rt_op'] . ' RW. ' . $dataLama['t_rw_op'] }}
                </td>
            </tr>
            <tr>
                <td>Luas Bumi</td>
                <td>:</td>
                <td colspan="3">
                    {{ $dataLama['t_luas_tanah'] }}
                </td>
            </tr>
            <tr>
                <td>Luas Bangunan</td>
                <td>:</td>
                <td colspan="3">
                    {{ $dataLama['t_luas_bangunan'] }}
                </td>
            </tr>
        </table>

        <table style="width: 100%;" cellspacing="0" cellpadding="0">
            <tr>
                <td style="text-align: center;padding-top: 24px;">
                    <span style="text-align: center">DATA BARU</span>
                </td>
            </tr>
        </table>

        @foreach ($dataBaru as $k => $v)
            <table style="border: 1px solid #000;width: 100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width: 30%">Nama</td>
                    <td style="width: 15px;">:</td>
                    <td colspan="3">{{ $v['t_nama_wp'] }}</td>
                </tr>
                <tr>
                    <td>Alamat Subjek Pajak</td>
                    <td>:</td>
                    <td colspan="3">{{ $v['t_jalan_wp'] }}, RT. {{ $v['t_rt_wp'] }}, RW.
                        {{ $v['t_rw_wp'] }}
                    </td>
                </tr>
                <tr>
                    <td>Alamat Objek Pajak</td>
                    <td>:</td>
                    <td colspan="3">
                        {{ $v['t_jalan_op'] . ' RT. ' . $v['t_rt_op'] . ' RW. ' . $v['t_rw_op'] }}
                    </td>
                </tr>
                <tr>
                    <td>Luas Bumi</td>
                    <td>:</td>
                    <td colspan="3">
                        {{ $v['t_luas_tanah'] }}
                    </td>
                </tr>
                <tr>
                    <td>Luas Bangunan</td>
                    <td>:</td>
                    <td colspan="3">
                        {{ $v['t_luas_bangunan'] == null ? 0 : $v['t_luas_bangunan'] }}
                    </td>
                </tr>
            </table>
        @endforeach
    @elseif (in_array($pelayanan->t_id_jenis_pelayanan, [2]))
        <table style="width: 100%;" cellspacing="0" cellpadding="0">
            <tr>
                <td style="text-align: center;padding-top: 24px;">
                    <span style="font-size: 12pt; text-align: center;font-weight: bold">TANDA TERIMA</span><br>
                    <span style="text-align: center">TELAH DILAKUKAN PERUBAHAN DATA SPPT PBB DARI</span><br>
                    <span style="text-align: center">DATA LAMA</span>
                </td>
            </tr>
        </table>
        <table style="border-bottom: 1px solid #000;width: 100%" cellspacing="0" cellpadding="0">
            <tr>
                <td style="width: 30%">Nama</td>
                <td style="width: 15px;">:</td>
                <td colspan="3">{{ $wpLama['t_nama_wp'] }}</td>
            </tr>
            <tr>
                <td>Alamat Subjek Pajak</td>
                <td>:</td>
                <td colspan="3">{{ $wpLama['t_jalan_wp'] }}, RT. {{ $wpLama['t_rt_wp'] }}, RW.
                    {{ $wpLama['t_rw_wp'] }}
                </td>
            </tr>
            <tr>
                <td>Alamat Objek Pajak</td>
                <td>:</td>
                <td colspan="3">
                    {{ $wpLama['t_jalan_op'] . ' RT. ' . $wpLama['t_rt_op'] . ' RW. ' . $wpLama['t_rw_op'] }}
                </td>
            </tr>
            <tr>
                <td>Luas Bumi</td>
                <td>:</td>
                <td colspan="3">
                    {{ $pelayanan['OpLama']['t_luas_tanah'] }}
                </td>
            </tr>
            <tr>
                <td>Luas Bangunan</td>
                <td>:</td>
                <td colspan="3">
                    {{ $pelayanan['OpLama']['t_luas_bangunan'] == null ? 0 : $pelayanan['OpLama']['t_luas_bangunan'] }}
                </td>
            </tr>
        </table>

        <table style="width: 100%;" cellspacing="0" cellpadding="0">
            <tr>
                <td style="text-align: center;padding-top: 24px;">
                    <span style="text-align: center">DATA BARU</span>
                </td>
            </tr>
        </table>

        @foreach ($wpBaru as $k => $v)
            <table style="border: 1px solid #000;width: 100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width: 30%">Nama</td>
                    <td style="width: 15px;">:</td>
                    <td colspan="3">{{ $v['t_nama_wp'] }}</td>
                </tr>
                <tr>
                    <td>Alamat Subjek Pajak</td>
                    <td>:</td>
                    <td colspan="3">{{ $v['t_jalan_wp'] }}, RT. {{ $v['t_rt_wp'] }}, RW.
                        {{ $v['t_rw_wp'] }}
                    </td>
                </tr>
                <tr>
                    <td>Alamat Objek Pajak</td>
                    <td>:</td>
                    <td colspan="3">
                        {{ $v['t_jalan_op'] . ' RT. ' . $v['t_rt_op'] . ' RW. ' . $v['t_rw_op'] }}
                    </td>
                </tr>
                <tr>
                    <td>Luas Bumi</td>
                    <td>:</td>
                    <td colspan="3">
                        {{ $pelayanan['OpLama']['t_luas_tanah'] }}
                    </td>
                </tr>
                <tr>
                    <td>Luas Bangunan</td>
                    <td>:</td>
                    <td colspan="3">
                        {{ $pelayanan['OpLama']['t_luas_bangunan'] == null ? 0 : $pelayanan['OpLama']['t_luas_bangunan'] }}
                    </td>
                </tr>
            </table>
        @endforeach
        @php
            // dd($pelayanan['OpLama']['t_luas_tanah']);
        @endphp
    @else
        <table style="width: 100%;" cellspacing="0" cellpadding="0">
            <tr>
                <td style="text-align: center;padding-top: 24px;">
                    <span style="font-size: 12pt; text-align: center;font-weight: bold">TANDA TERIMA BERKAS
                        PERMOHONAN</span><br>
                    <span style="text-align: center">Nomor Pelayanan: <span
                            style="font-weight: bold">{{ $noPelayanan }}</span></span>
                </td>
            </tr>
        </table>

        <div style="padding-top: 24px;">Telah diterima berkas permohonan dari: </div>

        <table style="border-bottom: 1px solid #000;width: 100%" cellspacing="0" cellpadding="0">
            <tr>
                <td style="width: 30%">Nama</td>
                <td style="width: 15px;">:</td>
                <td colspan="3">{{ $pelayanan->t_nama_pemohon }}</td>
            </tr>
            <tr>
                <td>NIK</td>
                <td>:</td>
                <td colspan="3">{{ $pelayanan->t_nik_pemohon }}</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td colspan="3">
                    {{ $pelayanan->t_jalan_pemohon . ' RT. ' . $pelayanan->t_rt_pemohon . ' RW. ' . $pelayanan->t_rw_pemohon }}
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td style="width: 200px;">Desa/Kelurahan</td>
                <td style="width: 15px;">:</td>
                <td>{{ $pelayanan->t_kelurahan_pemohon }}</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Kecamatan</td>
                <td>:</td>
                <td>{{ $pelayanan->t_kecamatan_pemohon }}</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Kabupaten/Kota</td>
                <td>:</td>
                <td>{{ $pelayanan->t_kabupaten_pemohon }}</td>
            </tr>
            <tr>
                <td>Jenis Pajak</td>
                <td>:</td>
                <td colspan="3">{{ $pelayanan->jenisPajak->s_nama_jenis_pajak }}</td>
            </tr>
            <tr>
                <td>Jenis Pelayanan</td>
                <td>:</td>
                <td colspan="3">{{ $pelayanan->jenisPelayanan->s_nama_jenis_pelayanan }}</td>
            </tr>
            <tr>
                <td>Nama Wajib Pajak</td>
                <td>:</td>
                <td colspan="3">{{ $pelayanan->wajibPajak->t_nama_wp ?? '' }}</td>
            </tr>
            <tr>
                <td>NOP</td>
                <td>: </td>
                <td colspan="3">
                    @if (!is_null($pelayanan->nop))
                        {{ \App\Helpers\PelayananHelper::formatStringToNop($pelayanan->t_nop) }}
                    @else
                        @if (!is_null($pelayanan->OpLama))
                            {{ $pelayanan->OpLama->kd_propinsi .
                                '.' .
                                $pelayanan->OpLama->kd_dati2 .
                                '.' .
                                $pelayanan->OpLama->kd_kecamatan .
                                '.' .
                                $pelayanan->OpLama->kd_kelurahan .
                                '.' .
                                $pelayanan->OpLama->kd_blok .
                                '.' .
                                $pelayanan->OpLama->no_urut .
                                '.' .
                                $pelayanan->OpLama->kd_jns_op }}
                        @else
                            -
                        @endif
                    @endif

                </td>
            </tr>
            <tr>
                <td>Tanggal Permohonan</td>
                <td>:</td>
                <td colspan="3">
                    {{ \Carbon\Carbon::createFromDate($pelayanan->t_tgl_pelayanan)->translatedFormat('d F Y') }}
                </td>
            </tr>
            <tr>
                <td>Tanggal Estimasi Selesai</td>
                <td>:</td>
                <td colspan="3">
                    {{ \Carbon\Carbon::createFromDate($pelayanan->pelayananAprove->t_tgl_perkiraan_selesai)->translatedFormat('d F Y') }}
                </td>
            </tr>
        </table>

        <div style="padding-bottom: 15px; padding-top: 5px;">
            Dokumen yang dilampirkan seperti tersebut dibawah ini:
        </div>

        <table style="width: 100%" class="border_atas border_kanan border_bawah border_kiri">
            <tr>
                <td class="border_kanan border_atas" style="text-align: center">No.</td>
                <td class="border_kanan border_atas" style="padding-left: 10px;">Dokumen</td>
                <td class="border_kanan border_atas" style="text-align: center">Terlampir</td>
            </tr>
            @foreach ($persyaratan as $key => $persyaratan)
                <tr>
                    <td class="border_kanan border_atas" style="text-align: center">{{ $key + 1 }}</td>
                    <td class="border_kanan border_atas">{{ $persyaratan->t_nama_persyaratan }}</td>
                    <td class="border_kanan border_atas" style="text-align: center"><img style="width: 100px"
                            src="{{ $persyaratan->t_lokasi_file }}"></td>
                </tr>
            @endforeach
        </table>
    @endif
    <table style="width: 100%;" cellspacing="0" cellpadding="0">
        <tr>
            <td style="text-align: center;padding-top: 24px;">
                <span style="text-align: center">DATA SPPT TERBARU DAPAT DITERIMA DI TAHUN BERIKUTNYA.</span>
            </td>
        </tr>
    </table>
    {{-- {{ dd() }} --}}
@endsection
