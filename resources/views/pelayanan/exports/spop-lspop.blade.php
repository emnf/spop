<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <style>
        table,
        tr,
        td {
            padding-top: 5px;
            padding-bottom: 5px;
        }
    </style>
    <title>SPOP</title>
</head>

@php
    $pekerjaan = ["1" => "PNS", "2" => "ABRI", "3" => "Pensiunan", "4" => "Badan", "5" => "Lainnya"];
    $jenisBumi = ["1" => "TANAH & BANGUNAN", "2" => "KAVLING SIAP BANGUN", "3" => "TANAH KOSONG", "4" => "FASILITAS UMUM"];
    $kondisiBangunan = ["1" => "SANGAT BAIK", "2" => "BAIK", "3" => "SEDANG", "4" => "JELEK"];
    $atap = ["1" => "DECRABON/BETON/GTG GLAZUR", "2" => "GTG BETON/ALUMUNIUM", "3" => "GTG BIASA/SIRAP", "4" => "ASBES", "5" => "SENG"];
    $dinding = ["1" => "KACA/ALUMUNIUM", "2" => "BETON", "3" => "BATU BATA/CONBLOK", "4" => "KAYU", "5" => "SENG", "6" => "TIDAK ADA"];
    $lantai = ["1" => "MARMER", "2" => "KERAMIK", "3" => "TRASO", "4" => "UBIN PC/PAPAN", "5" => "SEMEN"];
    $langit = ["1" => "AKUSTIK/JATI", "2" => "TRIPLEK/ASBES BAMBU", "3" => "TIDAK ADA"];
    $konstruksi = ["1" => "BAJA", "2" => "BETON", "3" => "BATU BATA", "4" => "KAYU"];
@endphp

<body>
    <table border="0" style="width: 100%; font-size: 12px; border: 1px solid; border-collapse: collapse;">
        <tr>
            <td style="width: 10%; padding: 10px;" rowspan="2">
                <img width="60" height="70" src="{{ $pemda->s_logo }}">
            </td>
            <td style="width: 50%; vertical-align: middle; text-align: center; vertical-align: top; border-bottom: 1px solid; border-collapse: collapse;">
                <b>
                        <span th:text="${#strings.toUpperCase(pemda.namaKabkota)}" style="font-size: 11px;"> PEMERINTAH {{ $pemda->s_nama_kabkota }}</span>
                        <br>
                        <span th:text="${#strings.toUpperCase(pemda.namaInstansi)}" style="font-size: 14px;">{{ $pemda->s_nama_instansi }}</span>
                        <br>
                        <span th:text="${#strings.toUpperCase(pemda.alamatInstansi)}" style="font-size: 9px;">{{ $pemda->s_alamat_instansi . ' Telp/Fax. ' . $pemda->s_notelp_instansi .', Kode Pos '. $pemda->s_kode_pos }}</span>
                    </b>
            </td>
            <td style="width: 20px;"></td>
            <td style="vertical-align: top; border: 1px solid; border-collapse: collapse; padding: 10px;">
                No. Formulir:
                <b>{{ $datObjekPajak->no_formulir_spop }}</b>
            </td>
        </tr>
        <tr>
            <td style="text-align: center; vertical-align: top;">
                <b>SURAT PEMBERITAHUAN OBJEK PAJAK ( S P O P )</b>
            </td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <table border="0" style="width: 100%; font-size: 12px; border: 1px solid; border-collapse: collapse;">
        <tr>
            <td style="width: 50%; padding: 5px; border-top: 1px solid;">I. Diisi oleh Wajib Pajak, kecuali bagian yang diarsir</td>
            <td style="padding: 5px; border-top: 1px solid;">II. Beri tanda centang (V) pada kolom yang sesuai</td>
        </tr>
    </table>
    <table style="width: 100%; border: 1px solid; font-size: 12px; border-collapse: collapse;">
        <tr>
            <td style="width: 3%; border-bottom: 1px solid; border-collapse: collapse; padding-left: 5px">1.</td>
            <td style="width: 20%; border-bottom: 1px solid; border-collapse: collapse;">JENIS TRANSAKSI</td>
            <td style="width: 32%; border-bottom: 1px solid; border-collapse: collapse;">
                <label th:text="${dataOpWp.namaJenisTransaksiOP}">{{ $datObjekPajak->jns_transaksi_op == 1 ? 'PEREKAMAN DATA' : 'PEMUTAKHIRAN DATA' }}</label>
            </td>
            <td style="width: 2%; border-bottom: 1px solid; border-collapse: collapse;"></td>
            <td style="width: 20%; border-bottom: 1px solid; border-collapse: collapse;"></td>
            <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
        </tr>
        <tr>
            <td style="padding-left: 5px">2.</td>
            <td>NOP</td>
            <td colspan="4">
                <label th:text="${dataOpWp.nop}">{{ $datObjekPajak->nopFormated }}</label>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 5px">3.</td>
            <td>NOP BERSAMA</td>
            <td colspan="4"></td>
        </tr>
        <tr>
            <td colspan="6" style="height: 10px; border: 1px solid; border-collapse: collapse; text-align: center;  background-color: black; padding: 5px 0 5px 0;">
                <b style="color: white">A. INFORMASI TAMBAHAN UNTUK DATA BARU</b>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 5px">4.</td>
            <td>NOP ASAL</td>
            <td colspan="4"></td>
        </tr>
        <tr>
            <td style="padding-left: 5px">5.</td>
            <td>NO. SPPT LAMA</td>
            <td colspan="4">
                <label th:text="${dataOpWp.nomorSertifikat}">{{ $datObjekPajak->no_persil }}</label>
            </td>
        </tr>
        <tr>
            <td colspan="6" style="height: 10px; border: 1px solid; border-collapse: collapse; text-align: center; background-color: black; padding: 5px 0 5px 0;">
                <b style="color: white">B. DATA LETAK OBJEK PAJAK</b>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 5px">6.</td>
            <td>NAMA JALAN</td>
            <td></td>
            <td>7.</td>
            <td>BLOK/KAV/NOMOR</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">
                <label th:text="${dataOpWp.jalanOP}">{{ $datObjekPajak->jalan_op }}</label>
            </td>
            <td></td>
            <td colspan="2">
                <label th:text="${dataOpWp.blokOP}">{{ $datObjekPajak->blok_kav_no_op }}</label>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 5px">8.</td>
            <td>KELURAHAN/DESA</td>
            <td></td>
            <td>9.</td>
            <td>RT &nbsp;&nbsp;&nbsp;&nbsp;10. RW</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">
                <label th:text="${kelurahanOP}">{{ $pelayanan->objekPajak->t_kelurahan_op }}</label>
            </td>
            <td></td>
            <td colspan="2">
                <label th:text="${dataOpWp.rtOP}">{{ $datObjekPajak->rt_op }}</label> &nbsp;&nbsp;&nbsp;&nbsp;
                <label th:text="${dataOpWp.rwOP}">{{ $datObjekPajak->rw_op }}</label>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 5px">11.</td>
            <td>KECAMATAN</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">
                <label th:text="${kecamatanOP}">{{ $pelayanan->objekPajak->t_kecamatan_op }}</label>
            </td>
            <td></td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="6" style="height: 10px; border: 1px solid; border-collapse: collapse; text-align: center; background-color: black; padding: 5px 0 5px 0;">
                <b style="color: white">C. DATA SUBJEK PAJAK</b>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 5px">11.</td>
            <td>STATUS</td>
            <td colspan="4">
                <label th:text="${dataOpWp.namaJenisKepemilikan}"></label>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 5px">12.</td>
            <td>PEKERJAAN</td>
            <td colspan="4">
                <label th:text="${dataOpWp.namaStatusPekerjaanWP}">{{ $pekerjaan[$datSubjekPajak->status_pekerjaan_wp] }}</label>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 5px">13.</td>
            <td>NAMA SUBJEK PAJAK</td>
            <td></td>
            <td>14.</td>
            <td>NPWP</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">
                <label th:text="${dataOpWp.namaWP}">{{ $datSubjekPajak->nm_wp }}</label>
            </td>
            <td></td>
            <td colspan="2">
                <label th:text="${dataOpWp.npwp}">{{ $datSubjekPajak->npwp }}</label>
            </td>
            <tr>
                <td style="padding-left: 5px">15.</td>
                <td>NAMA JALAN</td>
                <td></td>
                <td>16.</td>
                <td>BLOK/KAV/NOMOR</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">
                    <label th:text="${dataOpWp.jalanWP}">{{ $datSubjekPajak->jalan_wp }}</label>
                </td>
                <td></td>
                <td colspan="2">
                    <label th:text="${dataOpWp.blokWP}">{{ $datSubjekPajak->blok_kav_no_wp }}</label>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 5px">17.</td>
                <td>KELURAHAN/DESA</td>
                <td></td>
                <td>18.</td>
                <td>RT &nbsp;&nbsp;&nbsp;&nbsp;19. RW</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">
                    <label th:text="${dataOpWp.kelurahanWP}">{{ $datSubjekPajak->kelurahan_wp }}</label>
                </td>
                <td></td>
                <td colspan="2">
                    <label th:text="${dataOpWp.rtWP}">{{ $datSubjekPajak->rt_wp }}</label> &nbsp;&nbsp;&nbsp;&nbsp;
                    <label th:text="${dataOpWp.rwWP}">{{ $datSubjekPajak->rw_wp }}</label>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 5px">20.</td>
                <td colspan="2">KABUPATEN/KOTAMADYA</td>
                <td></td>
                <td colspan="2">KODE POS</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">
                    <label th:text="${dataOpWp.kabupatenWP}">{{ $datSubjekPajak->kota_wp }}</label>
                </td>
                <td></td>
                <td colspan="2">
                    <label th:text="${dataOpWp.kodePosWP}">{{ $datSubjekPajak->kd_pos_wp }}</label>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 5px">21.</td>
                <td>NOMOR KTP</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">
                    <label th:text="${dataOpWp.nikWP}">{{ $pelayanan->wajibPajak->t_nik_wp  }}</label>
                </td>
                <td></td>
                <td colspan="2"></td>
            </tr>
    </table>
    <table style="width: 100%; font-size: 12px; border: 1px solid; border-collapse: collapse;">
        <tr>
            <td colspan="3" style="height: 10px; border: 1px solid; border-collapse: collapse; text-align: center; background-color: black;">
                <b style="color: white">D. DATA TANAH</b>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="border-left: 1px solid; border-right: 1px solid; border-collapse: collapse; padding: 0;">
                <table style="width: 100%; padding: 0;">
                    <tr>
                        <td style="width: 70%; padding: 0;">
                            <table style="width: 100%; padding: 0;">
                                <tr>
                                    <td style="width: 30%">22. LUAS TANAH</td>
                                    <td style="width: 3%">:</td>
                                    <td>
                                        <label th:text="${dataOpWp.luasTanah}">{{ $datObjekPajak->total_luas_bumi }}</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>24. JENIS TANAH</td>
                                    <td>:</td>
                                    <td>
                                        <label th:text="${dataOpWp.namaJenisTanah}">{{ $jenisBumi[$datObjekBumi->jns_bumi] }}</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        Catatan: *) yang penghasilannya semata-mata berasal dari gaji atau uang pensiunan
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="border-left: 1px solid; border-collapse: collapse; ">
                            <table>
                                <tr>
                                    <td>
                                        23. ZONA NILAI TANAH :
                                        <label th:text="${dataOpWp.kodeZNT}">{{ $datObjekBumi->kd_znt }}</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div style="page-break-before:always">&nbsp;</div>
    <table style="width: 100%; font-size: 12px; border: 1px solid; border-collapse: collapse;">
        <tr>
            <td colspan="3" style="height: 10px; border: 1px solid; border-collapse: collapse; text-align: center; background-color: black;">
                <b style="color: white">E. DATA BANGUNAN</b>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="border-left: 1px solid; border-right: 1px solid; border-collapse: collapse; padding-left: 5px;">
                25. JUMLAH BANGUNAN &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                <label th:text="${dataOpWp.jumlahBangunan}">{{ count($datObjekBangunan) }}</label>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: 10px; border: 1px solid; border-collapse: collapse; text-align: center; background-color: black;">
                <b style="color: white">F. PERNYATAAN SUBJEK PAJAK</b>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="border-left: 1px solid; border-right: 1px solid; border-collapse: collapse; ">
                Saya menyatakan bahwa informasi yang telah saya berikan dalam formulir ini termasuk lampirannya adalah benar, jelas dan lengkap menurut keadaan yang sebenarnya, sesuai dengan Peraturan Daerah yang berlaku.
            </td>
        </tr>
        <tr>
            <td colspan="3" style="border-left: 1px solid; border-right: 1px solid; border-collapse: collapse; ">
                <table style="width: 100%">
                    <tr>
                        <td style="width:20%"></td>
                        <td style="width:30%">26. TANGGAL/BULAN/TAHUN</td>
                        <td>:</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>27. TANDA TANGAN</td>
                        <td>:</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>28. NAMA LENGKAP</td>
                        <td>:</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="border-left: 1px solid; border-right: 1px solid; border-collapse: collapse; ">
                - Dalam hal bertindak selaku kuasa, Surat Kuasa harap dilampirkan
                <br> - Dalam hal Subjek Pajak mendaftarkan sendiri Objek Pajak, supaya menggambarkan Sket/ Denah Lokasi Objek Pajak
                <br> - Batas waktu pengembalian SPOP 30 (tiga puluh) hari sejak diterima oleh Subjek Pajak sesuai Peraturan Daerah yang berlaku.
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: 10px; border: 1px solid; border-collapse: collapse; text-align: center; background-color: black;">
                <b style="color: white">G. IDENTITAS PENDATA/PEJABAT YANG BERWENANG</b>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="border-left: 1px solid; border-right: 1px solid; border-collapse: collapse; padding: 0px;">
                <table style="width: 100%; border-right: 1px solid; border-collapse: collapse;">
                    <tr>
                        <td style="border-right: 1px solid; border-collapse: collapse; width:50%; text-align: center">
                            PETUGAS PENDATA
                        </td>
                        <td style="text-align: center">MENGETAHUI PEJABAT YANG BERWENANG :</td>
                    </tr>
                    <tr>
                        <td style="border-right: 1px solid; border-collapse: collapse;">29. TANGGAL (TGL/BLN/THN)</td>
                        <td>33. TANGGAL (TGL/BLN/THN)</td>
                    </tr>
                    <tr>
                        <td style="border-right: 1px solid; border-collapse: collapse;">30. TANDA TANGAN</td>
                        <td>34. TANDA TANGAN</td>
                    </tr>
                    <tr>
                        <td style="border-right: 1px solid; border-collapse: collapse;">31. NAMA JELAS</td>
                        <td>35. NAMA JELAS</td>
                    </tr>
                    <tr>
                        <td style="border-right: 1px solid; border-collapse: collapse;">32. NIP</td>
                        <td>36. NIP</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br>
    <p style="text-align: center">
        <u>
                        <b>SKET / DENAH LOKASI OBJEK PAJAK</b>
                    </u>
    </p>
    <table style="width: 100%;">
        <tr>
            <td style="height: 300px; border: 1px solid; border-collapse: collapse;">&nbsp;</td>
        </tr>
    </table>
    <table style="width: 100%;">
        <tr>
            <td style="width: 50%; text-align: center">
                KETERANGAN :
            </td>
            <td style="text-align: center">
                Contoh Penggambaran
            </td>
        </tr>
        <tr>
            <td style="font-size: 12px">
                <ul>
                    <li>
                        Gambarkan sket/ denah lokasi objek pajak (tanpa skala), yang dihubungkan dengan jalan raya/jalan protokol, jalan lingkungan dan lain-lain, yang mudah diketahui oleh umum
                    </li>
                    <li>Sebutkan batas-batas pemilikan sebelah utara, selatan, timur dan barat</li>
                </ul>
            </td>
            <td></td>
        </tr>
    </table>

    @if (count($datObjekBangunan) > 0)
        <div style="page-break-before:always">&nbsp;</div>
        @foreach ($datObjekBangunan as $bangunan)
            <table style="width: 100%; font-size: 12px; border: 1px solid; border-collapse: collapse;">
                <tr>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;" colspan="5">
                        <b style="font-size: 14px">LAMPIRAN SURAT PEMBERITAHUAN OBJEK PAJAK ( LSPOP )</b>
                    </td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">
                        No. Formulir :
                        <span th:each="dataBangunan: ${listBangunan}" th:if="${dataBangunan.nomorBangunan==lspop.nomorBangunan}">
                            <b th:text="${dataBangunan.noFormulirLSPOP}">{{ $bangunan['no_formulir_lspop'] }}</b>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td style="width: 2%; border-bottom: 1px solid; border-collapse: collapse;">1.</td>
                    <td style="width: 18%; border-bottom: 1px solid; border-collapse: collapse;">JENIS TRANSAKSI</td>
                    <td style="width: 30%; border-bottom: 1px solid; border-collapse: collapse;">
                        :
                        <label th:text="${lspop.jenisBangunan}">{{ $bangunan['jns_transaksi_bng'] == 1 ? "PEREKAMAN DATA" : "PEMUTAKHIRAN DATA" }}</label>
                    </td>
                    <td style="width: 2%; border-bottom: 1px solid; border-collapse: collapse;"></td>
                    <td style="width: 18%; border-bottom: 1px solid; border-collapse: collapse;"></td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td>NOP</td>
                    <td>
                        :
                        <label th:text="${dataOpWp.nop}">{{ $bangunan['nop'] }}</label>
                    </td>
                    <td>3.</td>
                    <td>JUMLAH BANGUNAN</td>
                    <td>
                        :
                        <label></label>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">4.</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">BANGUNAN KE</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">
                        :
                        <label th:text="${lspop.nomorBangunan}">{{ $bangunan['no_bng'] }}</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="height: 10px; border: 1px solid; border-collapse: collapse; text-align: center; background-color: black;">
                        <b style="color: white">A. RINCIAN DATA BANGUNAN</b>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">5.</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">JNS PENGGUNAAN BANGUNAN</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">
                        :
                        <label th:text="${lspop.jenisBangunan}">{{ $bangunan['jpb']['nm_jpb'] }}</label>
                    </td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                </tr>
                <tr>
                    <td>6.</td>
                    <td>LUAS BANGUNAN</td>
                    <td>
                        :
                        <label th:text="${lspop.luasBangunan}">{{ $bangunan['luas_bng'] }}</label>
                    </td>
                    <td>7.</td>
                    <td>JUMLAH LANTAI</td>
                    <td>
                        :
                        <label th:text="${lspop.jumlahLantai}">{{ $bangunan['jml_lantai_bng'] }}</label>
                    </td>
                </tr>
                <tr>
                    <td>8.</td>
                    <td>THN DIBANGUN</td>
                    <td>
                        :
                        <label th:text="${lspop.tahunDibangun}">{{ $bangunan['thn_dibangun_bng'] }}</label>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="vertical-align: top">9.</td>
                    <td style="vertical-align: top">THN DIRENOVASI</td>
                    <td style="vertical-align: top">
                        :
                        <label th:text="${lspop.tahunDirenovasi}">{{ $bangunan['thn_renovasi_bng'] }}</label>
                    </td>
                    <td style="vertical-align: top">10.</td>
                    <td style="vertical-align: top">DAYA LISTRIK TERPASANG (WATT)</td>
                    <td style="vertical-align: top">
                        :
                        <label>{{ isset($bangunan[44]) ? $bangunan[44]['jml_satuan'] : '' }}</label>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top">11.</td>
                    <td style="vertical-align: top">KONDISI PADA UMUMNYA</td>
                    <td style="vertical-align: top">
                        :
                        <label th:text="${lspop.kondisiBangunan}">{{ $kondisiBangunan[$bangunan['kondisi_bng']] }}</label>
                    </td>
                    <td style="vertical-align: top"></td>
                    <td style="vertical-align: top"></td>
                    <td style="vertical-align: top"></td>
                </tr>
                <tr>
                    <td>12.</td>
                    <td>KONSTRUKSI</td>
                    <td>
                        :
                        <label th:text="${lspop.konstruksi}">{{ $konstruksi[$bangunan['jns_konstruksi_bng']] }}</label>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>13.</td>
                    <td>ATAP</td>
                    <td>
                        :
                        <label th:text="${lspop.atap}">{{ $atap[$bangunan['jns_atap_bng']] }}</label>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>14.</td>
                    <td>DINDING</td>
                    <td>
                        :
                        <label th:text="${lspop.dinding}">{{ $dinding[$bangunan['kd_dinding']] }}</label>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>15.</td>
                    <td>LANTAI</td>
                    <td>
                        :
                        <label th:text="${lspop.lantai}">{{ $lantai[$bangunan['kd_lantai']] }}</label>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">16.</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">LANGIT-LANGIT</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">
                        :
                        <label th:text="${lspop.langit}">{{ $langit[$bangunan['kd_langit_langit']] }}</label>
                    </td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                </tr>
                <tr>
                    <td colspan="6" style="height: 10px; border: 1px solid; border-collapse: collapse; text-align: center; background-color: black;">
                        <b style="color: white">B. FASILITAS</b>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">17.</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">JUMLAH AC</td>
                    <td style="border-bottom: 1px solid; border-right: 1px solid; border-collapse: collapse;">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%">
                                    <label>{{ $bangunan['01']['jml_satuan'] ?? '' }}  {{ $bangunan['01']['satuan_fasilitas'] ?? '' }}</label> AC SPLIT
                                </td>
                                <td>
                                    <label>{{ $bangunan['02']['jml_satuan'] ?? '' }}  {{ $bangunan['02']['satuan_fasilitas'] ?? '' }}</label> AC WINDOW
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">18.</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">AC CENTRAL</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">
                        <table style="width: 100%; padding: 0px;">
                            <tr>
                                <td style="width:10px; border:1px solid #000; height: 5px; line-height: 0px; font-size: 12px;"></td>
                                <td style="height: 5px; line-height: 0px; font-size: 10px;">1. Ada</td>
                                <td style="width:10px; border:1px solid #000; height: 5px; line-height: 0px; font-size: 12px;"></td>
                                <td style="height: 5px; line-height: 0px; font-size: 10px;">2. Tidak Ada</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top">19.</td>
                    <td>LUAS KOLAM RENANG</td>
                    <td style="border-right: 1px solid; border-collapse: collapse; vertical-align: top;">
                        :
                        <label></label>
                    </td>
                    <td style="vertical-align: top;" colspan="3">
                        20. LUAS PERKERASAN HALAMAN :
                        <label></label>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top"></td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                    <td style="border-bottom: 1px solid; border-right: 1px solid; border-collapse: collapse; vertical-align: top;">
                        <label></label>
                    </td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top;" colspan="3"></td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">21.</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">JUMLAH LAPANGAN TENIS</td>
                    <td style="border-bottom: 1px solid; border-right: 1px solid; border-collapse: collapse;">:</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">22.</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">JUMLAH LIFT</td>
                    <td style="border-bottom: 1px solid; border-right: 1px solid; border-collapse: collapse;">
                        23. JUMLAH TANGGA BERJALAN
                    </td>
                </tr>
                <tr>
                    <td style="border-collapse: collapse;">24.</td>
                    <td style="border-collapse: collapse;">PANJANG PAGAR</td>
                    <td style="border-right: 1px solid; border-collapse: collapse; vertical-align: top;">
                        :
                        <label>{{ $bangunan[36]['jml_satuan'] ?? '' }} {{ $bangunan[36]['satuan_fasilitas'] ?? '' }}</label>
                    </td>
                    <td style="border-collapse: collapse; vertical-align: top;">25.</td>
                    <td style="border-collapse: collapse; vertical-align: top;">PEMADAM KEBAKARAN</td>
                    <td style="border-collapse: collapse; vertical-align: top;"></td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">BAHAN PAGAR</td>
                    <td style="border-bottom: 1px solid; border-right: 1px solid; border-collapse: collapse; vertical-align: top;">
                        :
                        <label>{{ $bangunan[36]['nm_fasilitas'] ?? '' }}</label>
                    </td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top;"></td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top;"></td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top;"></td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">26.</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">JML. SALURAN PES. PABX</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top;">:</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top;">27.</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top;">
                        KEDALAMAN SUMUR ARTETIS (M)
                    </td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top;">
                        <label></label>
                    </td>
                </tr>
            </table>
            <div style="page-break-before:always">&nbsp;</div>
            <table style="width: 100%; font-size: 12px; border: 1px solid; border-collapse: collapse;">
                <tr>
                    <td colspan="6" style="height: 10px; border: 1px solid; border-collapse: collapse; text-align: center; background-color: black;">
                        <b style="color: white">C. DATA TAMBAHAN UNTUK JPB = 3 / 8</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table style="width: 100%; padding: 0px;">
                            <tr>
                                <td style="width:10px; border:1px solid #000; height: 5px; line-height: 0px; font-size: 10px;"></td>
                                <td style="height: 5px; line-height: 0px; font-size: 12px;">
                                    PABRIK/BENGKEL/GUDANG/PERTANIAN (JPB=3/8)
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top">28.</td>
                    <td style="vertical-align: top">TINGGI KOLOM (M)</td>
                    <td style="vertical-align: top">:</td>
                    <td style="vertical-align: top">29.</td>
                    <td>LEBAR BENTANG (M2)</td>
                    <td></td>
                </tr>
                <tr>
                    <td style="vertical-align: top">30.</td>
                    <td>DAYA DUKUNG LANTAI (Kg/M2)</td>
                    <td>:</td>
                    <td style="vertical-align: top">31.</td>
                    <td>KELILING DINDING (M)</td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="vertical-align: top">32.</td>
                    <td>LUAS MEZZANINE (M2)</td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="6" style="height: 10px; border: 1px solid; border-collapse: collapse; text-align: center; background-color: black;">
                        <b style="color: white">D. DATA TAMBAHAN UNTUK BANGUNAN NON-STANDARD</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table style="width: 100%; padding: 0px;">
                            <tr>
                                <td style="width:10px; border:1px solid #000; height: 5px; line-height: 0px; font-size: 10px;"></td>
                                <td style="height: 5px; line-height: 0px; font-size: 12px;">
                                    PERKANTORAN SWASTA / GEDUNG PEMERINTAH (JPB=2/9)
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">33.</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">KELAS BANGUNAN</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">:</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table style="width: 100%; padding: 0px;">
                            <tr>
                                <td style="width:10px; border:1px solid #000; height: 5px; line-height: 0px; font-size: 10px;"></td>
                                <td style="height: 5px; line-height: 0px; font-size: 12px;">
                                    TOKO/APOTIK/PASAR/RUKO (JPB=4)
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">34.</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">KELAS BANGUNAN</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">:</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table style="width: 100%; padding: 0px;">
                            <tr>
                                <td style="width:10px; border:1px solid #000; height: 5px; line-height: 0px; font-size: 10px;"></td>
                                <td style="height: 5px; line-height: 0px; font-size: 12px;">
                                    RUMAH SAKIT / KLINIK (JPB=5)
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>35.</td>
                    <td>KELAS BANGUNAN</td>
                    <td>:</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">36.</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">
                        LUAS KAMAR DG AC SENTRAL (M2)
                    </td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">:</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">37.</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">
                        LUAS RUANG LAIN AC SENTRAL (M2)
                    </td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top"></td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table style="width: 100%; padding: 0px;">
                            <tr>
                                <td style="width:10px; border:1px solid #000; height: 5px; line-height: 0px; font-size: 10px;"></td>
                                <td style="height: 5px; line-height: 0px; font-size: 12px;">OLAHRAGA / REKREASI (JPB=6)</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">38.</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">KELAS BANGUNAN</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">:</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table style="width: 100%; padding: 0px;">
                            <tr>
                                <td style="width:10px; border:1px solid #000; height: 5px; line-height: 0px; font-size: 10px;"></td>
                                <td style="height: 5px; line-height: 0px; font-size: 12px;">HOTEL / WISMA (JPB=7)</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>39.</td>
                    <td>JENIS HOTEL</td>
                    <td colspan="4">:</td>
                </tr>
                <tr>
                    <td>40.</td>
                    <td>JML BINTANG</td>
                    <td colspan="4">:</td>
                </tr>
                <tr>
                    <td>41.</td>
                    <td>JUMLAH KAMAR</td>
                    <td>:</td>
                    <td style="vertical-align: top">42.</td>
                    <td style="vertical-align: top">LS KAMAR DGN AC SENTRAL (M2)</td>
                    <td style="vertical-align: top"></td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">43.</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">
                        LS RUANG LAIN DGN AC SENTRAL (M2)
                    </td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top" colspan="4">
                        :
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table style="width: 100%; padding: 0px;">
                            <tr>
                                <td style="width:10px; border:1px solid #000; height: 5px; line-height: 0px; font-size: 10px;"></td>
                                <td style="height: 5px; line-height: 0px; font-size: 12px;">BANGUNAN PARKIR (JPB=12)</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">44.</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">TIPE BANGUNAN</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">:</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;"></td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table style="width: 100%; padding: 0px;">
                            <tr>
                                <td style="width:10px; border:1px solid #000; height: 5px; line-height: 0px; font-size: 10px;"></td>
                                <td style="height: 5px; line-height: 0px; font-size: 12px;">APARTEMEN (JPB=13)</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>45.</td>
                    <td>KELAS BANGUNAN</td>
                    <td>:</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>46.</td>
                    <td>JML APARTEMEN</td>
                    <td>:</td>
                    <td style="vertical-align: top">47.</td>
                    <td style="vertical-align: top">LUAS APT DGN AC SENTRAL (M2)</td>
                    <td style="vertical-align: top"></td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">48.</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">
                        LUAS RUANG LAIN DGN AC SENTRAL (M2)
                    </td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top" colspan="4">
                        :
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table style="width: 100%; padding: 0px;">
                            <tr>
                                <td style="width:10px; border:1px solid #000; height: 5px; line-height: 0px; font-size: 10px;"></td>
                                <td style="height: 5px; line-height: 0px; font-size: 12px;">TANGKI MINYAK (JPB=15)</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">49.</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">
                        KAPASITAS TANGKI (M3)
                    </td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">:</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">50.</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">
                        LETAK TANGKI
                    </td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top"></td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table style="width: 100%; padding: 0px;">
                            <tr>
                                <td style="width:10px; border:1px solid #000; height: 5px; line-height: 0px; font-size: 10px;"></td>
                                <td style="height: 5px; line-height: 0px; font-size: 12px;">GEDUNG SEKOLAH (JPB=16)</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">51.</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;">KELAS BANGUNAN</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse;" colspan="4">:</td>
                </tr>
                <tr>
                    <td colspan="6" style="height: 10px; border: 1px solid; border-collapse: collapse; text-align: center; background-color: black;">
                        <b style="color: white">E. PENILAIAN INDIVIDUAL ( x 1000 Rp)</b>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">52.</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">
                        NILAI SISTEM
                    </td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">:</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">53.</td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top">
                        NILAI INDIVIDUAL
                    </td>
                    <td style="border-bottom: 1px solid; border-collapse: collapse; vertical-align: top"></td>
                </tr>
                <tr>
                    <td colspan="6" style="height: 10px; border: 1px solid; border-collapse: collapse; text-align: center; background-color: black;">
                        <b style="color: white">F. IDENTITAS PENDATA / PEJABAT YANG BERWENANG</b>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center; border-right: 1px solid; border-collapse: collapse;" colspan="3">
                        PETUGAS PENDATA
                    </td>
                    <td style="text-align: center" colspan="3">MENGETAHUI PEJABAT YANG BERWENANG</td>
                </tr>
                <tr>
                    <td style="vertical-align: top">54.</td>
                    <td style="vertical-align: top">TGL. KUNJUNG KEMBALI</td>
                    <td style="border-right: 1px solid; border-collapse: collapse; vertical-align: top">:</td>
                    <td style="vertical-align: top" colspan="3"></td>
                </tr>
                <tr>
                    <td style="vertical-align: top">55.</td>
                    <td style="vertical-align: top">TGL. PENDATAAN</td>
                    <td style="border-right: 1px solid; border-collapse: collapse; vertical-align: top">:</td>
                    <td style="vertical-align: top">59.</td>
                    <td style="vertical-align: top">TGL. PENELITIAN</td>
                    <td style="vertical-align: top"></td>
                </tr>
                <tr>
                    <td style="vertical-align: top">56.</td>
                    <td style="vertical-align: top">TANDA TANGAN</td>
                    <td style="border-right: 1px solid; border-collapse: collapse; vertical-align: top">:</td>
                    <td style="vertical-align: top">60.</td>
                    <td style="vertical-align: top">TANDA TANGAN</td>
                    <td style="vertical-align: top"></td>
                </tr>
                <tr>
                    <td style="vertical-align: top">57.</td>
                    <td style="vertical-align: top">NAMA JELAS</td>
                    <td style="border-right: 1px solid; border-collapse: collapse; vertical-align: top">:</td>
                    <td style="vertical-align: top">61.</td>
                    <td style="vertical-align: top">NAMA JELAS</td>
                    <td style="vertical-align: top"></td>
                </tr>
                <tr>
                    <td style="vertical-align: top">58.</td>
                    <td style="vertical-align: top">NIP</td>
                    <td style="border-right: 1px solid; border-collapse: collapse; vertical-align: top">:</td>
                    <td style="vertical-align: top">62.</td>
                    <td style="vertical-align: top">NIP</td>
                    <td style="vertical-align: top"></td>
                </tr>
            </table>
        @endforeach
    @endif
</body>

</html>
