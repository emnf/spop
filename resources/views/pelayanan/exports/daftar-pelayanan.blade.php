@extends('layouts.print')

@section('body')

<table style="width: 100%" border="0">
<tr>
    <td colspan="9" class="text-center">
        <h4>DATA PENGAJUAN PELAYANAN</h4>
    </td>
</tr>
</table>

<table id="table" style="width: 100%; border-collapse: collapse;">
    <thead>
        <tr>
            <th class="border_kiri border_atas border_kanan border_bawah text-center" style="vertical-align: middle">No.
            </th>
            <th class="border_kiri border_atas border_kanan border_bawah text-center" style="vertical-align: middle">
                Jenis Pajak</th>
            <th class="border_kiri border_atas border_kanan border_bawah text-center" style="vertical-align: middle">No.
                Pelayanan</th>
            <th class="border_kiri border_atas border_kanan border_bawah text-center" style="vertical-align: middle">
                Tanggal Pelayanan</th>
            <th class="border_kiri border_atas border_kanan border_bawah text-center" style="vertical-align: middle">
                Jenis Pelayanan</th>
            <th class="border_kiri border_atas border_kanan border_bawah text-center" style="vertical-align: middle">
                Nama Pemohon</th>
            <th class="border_kiri border_atas border_kanan border_bawah text-center" style="vertical-align: middle">
                Alamat Pemohon</th>
            <th class="border_kiri border_atas border_kanan border_bawah text-center" style="vertical-align: middle">NOP
            </th>
            <th class="border_kiri border_atas border_kanan border_bawah text-center" style="vertical-align: middle">
                Keterangan</th>
        </tr>
    </thead>
    <tbody>
        @foreach($pelayanan as $i => $row)
        <tr>
            <td class="border_kiri border_bawah text-center">{{ $i+1 }}</td>
            <td class="border_kiri border_bawah text-center">{{ $row->jenisPajak->s_nama_singkat_pajak }}</td>
            <td class="border_kiri border_bawah ">{{ App\Helpers\PelayananHelper::formatNoPelayanan(date('Y',
                strtotime($row->t_tgl_pelayanan)), $row->t_id_jenis_pajak, $row->t_no_pelayanan) }}</td>
            <td class="border_kiri border_bawah text-center">{{ date('d-m-Y', strtotime($row->t_tgl_pelayanan)) }}
            </td>
            <td class="border_kiri border_bawah">{{ $row->jenisPelayanan->s_nama_jenis_pelayanan }}</td>
            <td class="border_kiri border_bawah">{{ $row->t_nama_pemohon }}</td>
            <td class="border_kiri border_bawah">{{ $row->t_jalan_pemohon }}</td>
            <td class="border_kiri border_bawah">{{ is_null($row->objekPajak->no_urut) || empty($row->objekPajak->no_urut) ? '' : $row->
                    objekPajak->nopFormated }}</td>
            <td class="border_kiri border_kanan border_bawah">{{ $row->t_keterangan }}</td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection
