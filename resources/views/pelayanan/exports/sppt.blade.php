    @php
        error_reporting(0);
        ini_set('pcre.backtrack_limit', 10000000);
        // if ($v->njop_sppt < $v->NJOP_MAX) {
        //     $v_tarif_dasar = '0,11%';
        // } elseif ($v->njop_sppt > $v->NJOP_MAX) {
        //     $v_tarif_dasar = '0,11%';
        // }

        // function hitungDenda($tgltempo, $jmlhpajak)
        // {
        //     $tgljatuhtempo = date('Y-m-d', strtotime($tgltempo));
        //     $tglsekarang = date('Y-m-d');
        //     $ts1 = strtotime($tgljatuhtempo);
        //     $ts2 = strtotime($tglsekarang);

        //     $year1 = date('Y', $ts1);
        //     $year2 = date('Y', $ts2);

        //     $month1 = date('m', $ts1);
        //     $month2 = date('m', $ts2);

        //     $day1 = date('d', $ts1);
        //     $day2 = date('d', $ts2);
        //     if ($day1 < $day2) {
        //         $tambahanbulan = 1;
        //     } else {
        //         $tambahanbulan = 0;
        //     }

        //     $jmlbulan = ($year2 - $year1) * 12 + ($month2 - $month1 + $tambahanbulan);
        //     if ($jmlbulan > 24) {
        //         $jmlbulan = 24;
        //         $jmldenda = (($jmlbulan * 2) / 100) * $jmlhpajak;
        //     } elseif ($jmlbulan <= 0) {
        //         $jmlbulan = 0;
        //         $jmldenda = (($jmlbulan * 2) / 100) * $jmlhpajak;
        //     } else {
        //         $jmldenda = (($jmlbulan * 2) / 100) * $jmlhpajak;
        //     }
        //     return round($jmldenda);
        // }

        function bulan($a)
        {
            $referensi = [
                '01' => 'JAN',
                '02' => 'FEB',
                '03' => 'MAR',
                '04' => 'APR',
                '05' => 'MEI',
                '06' => 'JUN',
                '07' => 'JUL',
                '08' => 'AGU',
                '09' => 'SEP',
                '10' => 'OKT',
                '11' => 'NOV',
                '12' => 'DES',
            ];
            return $referensi[$a];
        }

        function kekata($x)
        {
            $x = abs($x);
            $angka = [
                '',
                'Satu',
                'Dua',
                'Tiga',
                'Empat',
                'Lima',
                'Enam',
                'Tujuh',
                'Delapan',
                'Sembilan',
                'Sepuluh',
                'Sebelas',
            ];
            $temp = '';
            if ($x < 12) {
                $temp = ' ' . $angka[$x];
            } elseif ($x < 20) {
                $temp = kekata($x - 10) . ' Belas';
            } elseif ($x < 100) {
                $temp = kekata($x / 10) . ' Puluh' . kekata($x % 10);
            } elseif ($x < 200) {
                $temp = ' Seratus' . kekata($x - 100);
            } elseif ($x < 1000) {
                $temp = kekata($x / 100) . ' Ratus' . kekata($x % 100);
            } elseif ($x < 2000) {
                $temp = ' Seribu' . kekata($x - 1000);
            } elseif ($x < 1000000) {
                $temp = kekata($x / 1000) . ' Ribu' . kekata($x % 1000);
            } elseif ($x < 1000000000) {
                $temp = kekata($x / 1000000) . ' Juta' . kekata($x % 1000000);
            } elseif ($x < 1000000000000) {
                $temp = kekata($x / 1000000000) . ' Milyar' . kekata(fmod($x, 1000000000));
            } elseif ($x < 1000000000000000) {
                $temp = kekata($x / 1000000000000) . ' Trilyun' . kekata(fmod($x, 1000000000000));
            }
            return $temp;
        }

        function terbilang($x, $style = 4)
        {
            if ($x < 0) {
                $hasil = 'MINUS ' . trim(kekata($x));
            } else {
                $hasil = trim(kekata($x));
            }
            switch ($style) {
                case 1:
                    $hasil = strtoupper($hasil);
                    break;
                case 2:
                    $hasil = strtolower($hasil);
                    break;
                case 3:
                    $hasil = ucwords($hasil);
                    break;
                default:
                    $hasil = ucfirst($hasil);
                    break;
            }
            return $hasil . ' Rupiah';
        }
    @endphp
    <style>
        /* body { */
        /* background-image: url('<?php echo $blanko->thumbnail; ?>'); */
        /* background-size: cover; */
        /* background-repeat: no-repeat; */
        /* } */


        /* @page {
            size: auto;
            margin: 20px;
            padding: 20px;
        } */


        body {
            font-family: Arial, Helvetica, sans-serif;
            color: #787A91;
            font-size: 10pt;
        }

        /* .halaman_1 {
            font-family: monospace;
            font-size: 14px;
            color: #848587;
            background-image: url('{{ asset('img/sppt2.png') }}');
            background-size: cover;
            background-repeat: no-repeat;
            width: 100%;
        } */
    </style>
    @foreach ($data as $index => $v)
        <div style="background: url('<?php echo $blanko->thumbnail; ?>')'); background-repeat: no-repeat;">
            <br>
            <table style="width: 100%; border-collapse: collapse;" border="0">
                <tr>
                    <td style="width: 71%;"></td>
                    <td style="padding: 29px;">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>{{ $v->thn_pajak_sppt }} &nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $v->nm_sektor }}</td>
                </tr>
            </table>
            <br>
            <table style="width: 100%; border-collapse: collapse;" border="0">
                <tr>
                    <td style="width: 8%;"></td>
                    <td>{{ $v->nop }}</td>
                    <td style="width: 30%;"></td>
                    <td>#{{ $v->codingsppt }}/{{ str_pad($v->siklus_sppt, 2, '0', STR_PAD_LEFT) }}#</td>
                </tr>
            </table>
            <br>
            <table style="width: 100%; border-collapse: collapse;" border="0">

                <tr>

                    <td style="padding-top: 5px;padding-left:30px;width:50%;">
                        {{ $v->jalan_op }},{{ $v->blok_kav_no_op }}<BR>
                        RT : {{ $v->rt_op }} RW : {{ $v->rw_op }}<BR>
                        {{ $v->nm_kelurahan }}<BR>
                        {{ $v->nm_kecamatan }}<BR>
                        BLORA<BR></td>
                    <td style="padding-top: 15px;padding-left:40px;width:50%;">
                        {{ $v->nm_wp_sppt }} <BR>
                        {{ $v->jln_wp_sppt }} {{ $v->blok_kav_no_wp_sppt }}<BR>
                        RT {{ $v->rt_wp_sppt }}, RW {{ $v->rw_wp_sppt }}<BR>
                        {{ $v->kelurahan_wp_sppt }}<BR>
                        {{ $v->kota_wp_sppt }}<BR><BR><BR></td>
                </tr>
            </table>
            <table
                style="border-collapse: collapse; width: 80%; padding-bottom: 40px; margin-top: 25px; margin-left: 28px;  border-space: 0;"
                border="0">
                <tr>

                    <td width="30%">BUMI</td>
                    <td width="10%" align="right" style="padding-left:29px;">
                        {{ number_format($v->luas_bumi_sppt, 0, ',', '.') }}</td>
                    <td width="10%" align="right">{{ $v->kd_kls_tanah }}</td>
                    <td width="28%" align="right">
                        {{ number_format($v->njop_bumi_sppt / $v->luas_bumi_sppt, 0, ',', '.') }}
                    </td>
                    <td align="right">{{ number_format($v->njop_bumi_sppt, 0, ',', '.') }}</td>
                </tr>
                <tr>
                    <td>BANGUNAN</td>
                    <td align="right" style="padding-left:25px;">{{ number_format($v->luas_bng_sppt, 0, ',', '.') }}
                    </td>
                    <td align="right">{{ $v->kd_kls_bng }}</td>
                    <td align="right">
                        {{ number_format($v->luas_bng_sppt > 0 ? $v->njop_bng_sppt / $v->luas_bng_sppt : 0, 0, ',', '.') }}
                    </td>
                    <td align="right">{{ number_format($v->njop_bng_sppt, 0, ',', '.') }}</td>
                </tr>
            </table>
            <table
                style=" border-collapse: collapse; width: 80%; padding-bottom: 20px; margin-top: 5px; margin-left: 18px; margin-right: 40px; border-space: 0;"
                border="0">
                <tr>
                    <td></td>
                    <td align="right" style="padding-right:10px;"></td>
                    <td align="center"></td>
                    <td align="right" style="padding-right:10px;"></td>
                    <td align="right" style="padding-top:23.4px; border-space: 0;">
                        {{ number_format($v->njop_sppt, 0, ',', '.') }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td align="right" style="padding-right:10px;"></td>
                    <td align="center"></td>
                    <td align="right" style="padding-right:10px;"></td>
                    <td align="right" style="border-space: 0;">{{ number_format($v->njoptkp_sppt, 0, ',', '.') }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td align="right" style="padding-right:10px;"></td>
                    <td align="center"></td>
                    <td align="right" style="padding-right:10px;"></td>
                    <td align="right">{{ number_format($v->njop_sppt - $v->njoptkp_sppt, 0, ',', '.') }}
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td align="right" style="padding-right:10px;"></td>
                    <td align="center"></td>
                    <td align="right" style="padding-right:10px;"></td>
                    <td align="right">{{ number_format($v->hasil_njkp_sppt, 0, ',', '.') }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td align="right" style="padding-right:10px;"></td>
                    <td align="center"></td>
                    <td align="right" style="padding-right:10px;"></td>
                    <td align="right">{{ number_format($v->mytarif, 2, ',', '.') }}%</td>
                </tr>
                <tr>
                    <td></td>
                    <td align="right" style="padding-right:10px;"></td>
                    <td align="center"></td>
                    <td align="right" style="padding-right:10px;"></td>
                    <td align="right">{{ number_format($v->pbb_terhutang_sppt, 0, ',', '.') }}</td>
                </tr>
                <tr>
                    <td style="padding-left: 11px;"> STIMULUS/PENGURANGAN</td>
                    <td align="right" style="padding-right:10px;"></td>
                    <td align="center"></td>
                    <td align="right" style="padding-right:10px;"></td>
                    <td align="right">{{ number_format($v->faktor_pengurang_sppt, 0, ',', '.') }}</td>
                </tr>

            </table>
            <table
                style=" border-collapse: collapse; width: 80%; padding-bottom: 20px; margin-top: 5px; margin-left: 18px; margin-right: 40px; border-space: 0;"
                border="0">
                <tr>
                    <td></td>
                    <td align="right" style="padding-right:10px;"></td>
                    <td align="center"></td>
                    <td align="right" style="padding-right:10px;"></td>
                    <td align="right">{{ number_format($v->pbb_yg_harus_dibayar_sppt, 0, ',', '.') }}</td>
                </tr>
                <tr>
                    <td colspan="5" style="padding-left: 10px;">
                        {{ strtoupper(terbilang($v->pbb_yg_harus_dibayar_sppt)) }}</td>
                </tr>
            </table>
            {{-- <br> --}}
            <table
                style=" border-collapse: collapse; width: 100%; padding-bottom: 20px; margin-top: 5px; margin-left: 18px; margin-right: 40px; border-space: 0;"
                border="0">
                <tr>
                    <td colspan="2" style="padding: 14px 0px 0px 150px;text-align: right;">
                        {{ date('d', strtotime($v->tgl_jatuh_tempo_sppt)) }}
                        {{ bulan(date('m', strtotime($v->tgl_jatuh_tempo_sppt))) }}
                        {{ date('Y', strtotime($v->tgl_jatuh_tempo_sppt)) }}</td>
                    <td></td>
                    <td colspan="2" style="padding: 14px 0px 0px 120px;">BLORA,
                        {{ date('d', strtotime($v->tgl_terbit_sppt)) }}
                        {{ bulan(date('m', strtotime($v->tgl_terbit_sppt))) }}
                        {{ date('Y', strtotime($v->tgl_terbit_sppt)) }}
                    </td>
                </tr>
            </table>
            <br>
            <table style=" border-collapse: collapse; width: 100%; border-space: 0; margin-top: 20px;" border="0">
                <tr>
                    <td style="width: 18%; text-align: right;vertical-align: top;">
                        <img src="{{ $v->urlqrcode2 }}" alt="QR Code">
                    <td style="width: 32%; text-align: justify;"></td>
                    <td style="width: 8%;"></td>
                    <td style="padding-top: 57px; padding-left: 20px;">{{ $pegawai->nm_pegawai }}<br>NIP.
                        {{ $pegawai->nip_baru }}</td>
                </tr>
            </table>
            <br>
            <br>
            <table style=" border-collapse: collapse; width: 100%; border-space: 0; margin-top: 9px;" border="0">
                <tr>
                    <td style="width: 21%;"></td>
                    <td>{{ $v->nm_wp_sppt }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>{{ $v->nm_kecamatan }}<br>{{ $v->nm_kelurahan }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>{{ $v->nop }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>{{ $v->thn_pajak_sppt }}/
                        {{ number_format($v->pbb_yg_harus_dibayar_sppt, 0, ',', '.') }}</td>
                </tr>
            </table>
            <br>
            {{-- @if ($index < $totalItems - 1) --}}
            <div style="page-break-after: always;"></div>
            {{-- @endif --}}
    @endforeach
