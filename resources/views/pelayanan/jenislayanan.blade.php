@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Dashboard
@endsection

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="">Home</a></li>
                        <li class="breadcrumb-item active">Pelayanan</li>
                    </ol>
                </div><!-- /.col -->
                {{-- <div class="col-12">
                    <div class="info-box callout callout-danger" id="background2"
                        style="background: url('{{ asset('asset/images/bgpelayanan.gif') }}') #fff no-repeat center center;
                            -moz-background-size: cover;
                            -o-background-size: cover;
                            background-size: cover;">
                        <div class="col-sm-12">
                            <div class="" style="float: left;">
                                <h5 class="">PELAYANAN <span>{{ $jenisPajak['s_nama_singkat_pajak'] }}</span></h5>
                                ( <span class="float">{{ $jenisPajak['s_nama_jenis_pajak'] }}</span> )
                            </div>
                            <!-- /.description-block -->
                        </div>
                    </div>
                </div><!-- /.row --> --}}
            </div><!-- /.container-fluid -->
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <h6 class="mt-4 mb-2">Pilih Jenis Pelayanan</h6>
                        </div>
                        <div class="col-md-6">
                            <a href="{{ url('pelayanan') }}" class="mt-4 mb-2 btn btn-danger btn-sm float-right"><span
                                    class="fas fa-minus-circle"></span>&nbsp; Kembali</a>
                        </div>
                    </div>
                </div>
                @empty(!$jenisLayanan)
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($jenisLayanan as $val)
                        <div class="col-12">
                            <a href="{{ url('pelayanan/tambah/' . $val['s_id_jenis_pajak'] . '/' . $val['s_id_jenis_pelayanan']) }}"
                                style="color:black;width:100%;">
                                <div class="callout callout-info"
                                    style="background: url('{{ asset('asset/images/bg-info.gif') }}') #fff no-repeat center center;
                            -moz-background-size: cover;
                            -o-background-size: cover;
                            background-size: cover;">
                                    <div class="row">
                                        <div class="col-sm-2 border-right">
                                            <div class="description-block">
                                                <div class="icon text-center">
                                                    <h1><span class="badge badge-secondary">{{ $no++ }}</span></h1>
                                                </div>
                                            </div>
                                            <!-- /.description-block -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-sm-10">
                                            <h5>{{ $val['s_nama_jenis_pelayanan'] }}</h5>
                                            <hr style="border:2px solid #fcf8f7;" />
                                            <span>{{ $val['s_keterangan_jenis_pelayanan'] }}</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                @endempty

            </div>
        </div>
    </div>
@endsection
