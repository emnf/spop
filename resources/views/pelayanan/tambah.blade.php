@extends('layouts.app')

@section('title')
    Kepoin || {{ $jenisLayanan['s_nama_jenis_pelayanan'] }}
@endsection

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/pelayanan">Home</a></li>
                        <li class="breadcrumb-item active">Pelayanan</li>
                    </ol>
                </div>
                {{-- <div class="col-12">
                    <div class="info-box callout callout-danger" id="background2">
                        <div class="col-sm-12">
                            <div class="" style="float: left;">
                                <h5 class="">FORM
                                    <span> {{ strtoupper($jenisLayanan['s_nama_jenis_pelayanan']) }}
                                        {{ strtoupper($jenisPajak['s_nama_singkat_pajak']) }}</span>
                                </h5>

                            </div>

                        </div>
                    </div>
                </div> --}}
            </div>
        </div>

        <div class="container-fluid">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        @if (session('error'))
                            <div class="alert alert-dismissible alert_customs_danger">
                                <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-exclamation-triangle"></i> Gagal!</h5>
                                <span>{{ session('error') }}</span>
                            </div>
                        @endif

                        <h4 class="">Data Pemohon</h4>
                        <div class="card card-danger card-outline" style="border-top:3px solid #dc3545;">
                            <div class="card-body">

                                @if ($jenisLayanan['s_id_jenis_pelayanan'] == 1)
                                    <form method="POST" id="formpelayanan" action="{{ url('pelayanan/save-op-baru') }}"
                                        id="frmTambahPelayanan">
                                        @csrf
                                        @include('pelayanan.pbb.components.data-input.data-pemohon')
                                        @include('pelayanan.pbb.op-baru.form')
                                    @elseif($jenisLayanan['s_id_jenis_pelayanan'] == 2)
                                        <form method="POST" id="formpelayanan"
                                            action="{{ url('pelayanan/save-mutasi-wp') }}" id="frmTambahMutasiWp">
                                            @csrf
                                            @include('pelayanan.pbb.components.data-input.data-pemohon')
                                            @include('pelayanan.pbb.mutasi.form')
                                        @elseif($jenisLayanan['s_id_jenis_pelayanan'] == 3)
                                            <form method="POST" id="formpelayanan"
                                                action="{{ url('pelayanan/save-mutasi-pecah') }}" id="frmTambahMutasiPecah">
                                                @csrf
                                                @include('pelayanan.pbb.components.data-input.data-pemohon')
                                                @include('pelayanan.pbb.mutasi-pecah.form')
                                            @elseif($jenisLayanan['s_id_jenis_pelayanan'] == 4)
                                                <form method="POST" id="formpelayanan"
                                                    action="{{ url('pelayanan/save-pembetulan') }}"
                                                    id="frmTambahPembetulan">
                                                    @csrf
                                                    @include('pelayanan.pbb.components.data-input.data-pemohon')
                                                    @include('pelayanan.pbb.pembetulan.form')
                                                @elseif($jenisLayanan['s_id_jenis_pelayanan'] == 5)
                                                    <form method="POST" id="formpelayanan"
                                                        action="{{ url('pelayanan/save-pembatalan') }}"
                                                        id="frmTambahPembatalan">
                                                        @csrf
                                                        @include('pelayanan.pbb.components.data-input.data-pemohon')
                                                        @include('pelayanan.pbb.pembatalan-sppt.form')
                                                    @elseif($jenisLayanan['s_id_jenis_pelayanan'] == 6)
                                                        <form method="POST" id="formpelayanan"
                                                            action="{{ url('pelayanan/save-salinan-sppt') }}"
                                                            id="frmTambahSalinanSppt">
                                                            @csrf
                                                            @include('pelayanan.pbb.components.data-input.data-pemohon')
                                                            @include('pelayanan.pbb.salinan-sppt.form')
                                                        @elseif($jenisLayanan['s_id_jenis_pelayanan'] == 7)
                                                            <form method="POST" id="formpelayanan"
                                                                action="{{ url('pelayanan/save-keberatan-pajak') }}"
                                                                id="frmTambahKeberatan">
                                                                @csrf
                                                                @include('pelayanan.pbb.components.data-input.data-pemohon')
                                                                @include('pelayanan.pbb.keberatan-pajak.form')
                                                            @elseif($jenisLayanan['s_id_jenis_pelayanan'] == 8)
                                                                <form method="POST" id="formpelayanan"
                                                                    action="{{ url('pelayanan/save-pengurangan-ketetapan') }}"
                                                                    id="frmTambah">
                                                                    @csrf
                                                                    @include('pelayanan.pbb.components.data-input.data-pemohon')
                                                                    @include('pelayanan.pbb.pengurangan-ketetapan.form')
                                                                @elseif($jenisLayanan['s_id_jenis_pelayanan'] == 9)
                                                                    <form method="POST" id="formpelayanan"
                                                                        action="{{ url('pelayanan/save-penentuan-jatuh-tempo') }}"
                                                                        id="frmTambah">
                                                                        @csrf
                                                                        @include('pelayanan.pbb.components.data-input.data-pemohon')
                                                                        @include('pelayanan.pbb.penentuan-jatuh-tempo.form')
                                                                    @elseif($jenisLayanan['s_id_jenis_pelayanan'] == 22)
                                                                        <form method="POST" id="formpelayanan"
                                                                            action="{{ url('pelayanan/save-mutasi-gabung') }}"
                                                                            id="frmMutasiGabung">
                                                                            @csrf
                                                                            @include('pelayanan.pbb.components.data-input.data-pemohon')
                                                                            @include('pelayanan.pbb.mutasi-gabung.form')
                                @endif
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('pelayanan.pbb.components.modal-sudah-ajukan')
    @endsection

    @push('scripts')
        <script>
            $('.datepicker-date').datepicker({
                format: "dd-mm-yyyy",
                autoclose: true
            })

            function copyPemohon(wrapperFrm) {
                let pemohonObj = {
                    'nik': '',
                    'nama': '',
                    'jalan': '',
                    'rt': '',
                    'rw': '',
                    'kecamatan': '',
                    'kelurahan': '',
                    'kabupaten': '',
                    'kabupaten': '',
                    'email': '',
                    'no_hp': '',
                }

                Object.entries(pemohonObj).map(item => {
                    let value = $("#div-readonly input[name*='" + item[0] + "']").val();
                    $("#" + wrapperFrm + " input[name*='" + item[0] + "']").val(value)
                })
                @if ($jenisLayanan['s_id_jenis_pelayanan'] == 1)
                    sudahAjukanPermohonanOpBaru()
                @endif
            }

            document.addEventListener("DOMContentLoaded", function() {
                const form = document.getElementById('formpelayanan');
                const loadingModal = document.getElementById('loadingModal');

                form.addEventListener('submit', function(event) {
                    jQuery('#modal-save').modal('hide');
                    jQuery('#loadingModal').modal('show');
                });
            });
        </script>
    @endpush
