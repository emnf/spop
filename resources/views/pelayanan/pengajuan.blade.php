@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Pengajuan Pelayanan
@endsection

@section('content')
    <div class="content-header pl-3 pb-0">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <section>
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="pelayanan">Home</a></li>
                            <li class="breadcrumb-item">Pelayanan</li>
                            <li class="breadcrumb-item active">Pengajuan</li>
                        </ol>
                    </section>
                </div>
                {{-- <div class="col-12">
                    <div class="info-box callout callout-danger" id="background2">
                        <div class="col-sm-12">
                            <div class="" style="float: left;">
                                <h5 class="">FORM PENGAJUAN PELAYANAN <span
                                        class="text-uppercase">{{ $jenisLayanan->s_nama_jenis_pelayanan }}</span>
                                </h5>
                                <span class="float">Telitilah data yang akan anda ajukan dengan baik dan benar.
                                </span>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="invoice p-3 mb-3" style="border-top:3px solid #dc3545;">
                        <div class="row invoice-info">
                            <div class="col-sm-5 invoice-col border-right">
                                <h5>DATA PEMOHON </h5>
                                <hr>
                                @include('pelayanan.pbb.components.data-pemohon')
                            </div>
                            <div class="col-sm-7 invoice-col">
                                <h5>DATA PELAYANAN</h5>
                                <hr>
                                @include('pelayanan.pbb.components.data-view.data-pelayanan')
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- @if (in_array($pelayanan->t_id_jenis_pelayanan, [1, 2, 3, 4, 5, 6, 7, 9])) --}}
            @if ($pelayanan->objekPajaks->count() > 0)
                <div class="row">
                    <div class="col-12">
                        <div class="invoice p-3 mb-3" style="border-top:3px solid #dc3545;">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row invoice-info">
                                        <div class="col-sm-6 invoice-col border-right">
                                            @include('pelayanan.pbb.components.data-view.objek-pajak')
                                        </div>
                                        <div class="col-sm-6 invoice-col">
                                            @include('pelayanan.pbb.components.data-view.wajib-pajak')
                                        </div>
                                    </div>

                                    @if (!in_array($pelayanan->t_id_jenis_pelayanan, [5, 6]))
                                        @if (!is_null($pelayanan->WpLama) && !is_null($pelayanan->OpLama))
                                            <div class="row invoice-info pt-3">
                                                <div class="col-sm-6 invoice-col border-right">
                                                    @include('pelayanan.pbb.components.data-view.objek-pajak-lama')
                                                </div>
                                                <div class="col-sm-6 invoice-col">
                                                    @include('pelayanan.pbb.components.data-view.wajib-pajak-lama')
                                                </div>
                                            </div>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        </div>

        @if ($pelayanan->detailBangunan->count() > 0)
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="invoice p-3 mb-3" style="border-top:3px solid #dc3545;">
                            <div class="row invoice-info">
                                <div class="col-sm-6 invoice-col border-right">
                                    <h5>DATA BANGUNAN</h5>
                                    <hr>
                                    <address>
                                        @foreach ($pelayanan->detailBangunan as $index => $item)
                                            <div class="row">
                                                <div class="col-4">Luas Bangunan Ke-{{ $item->t_no_urut_bangunan }}</div>
                                                <div class="col-1 text-right">:</div>
                                                <div class="col-6"><b>{{ $item->t_luas }}</b> (M<sup>2</sup>)</div>
                                            </div>
                                        @endforeach
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        {{-- <div class="row"> --}}
        <div class="col-md-12">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <div class="card-title">
                        <h5>Persyaratan Permohonan Pelayanan</h5>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row table-responsive">
                        <input type="hidden" name="idPelayanan" id="idPelayanan" value="{{ $pelayanan->t_id_pelayanan }}">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="width: 4rem;">No.</th>
                                    <th>Jenis Persyaratan</th>
                                    <th>File Persyaratan</th>
                                    {{-- <th>Unduh File</th> --}}
                                </tr>
                            </thead>
                            <tbody id="uploadedFiles">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{-- </div> --}}
        <div class="col-12">
            <div class="card card-danger card-outline alert alert-default bg-white elevation-1">
                <h5 class="text-primary">
                    <i class="icon fas fa-exclamation-triangle"></i>
                    Persetujuan atas data yang di input!
                </h5>
                @csrf
                <input type="hidden" name="id" id="id" value="">
                {{-- <label class="text-primary">
                <i>Data yang di input adalah data yang sebenarnya dan bisa di pertanggung jawabkan ke
                    asliannya.</i>
            </label><br>
            <label class="text-primary">
                <input id="t_idpersetujuan" name="t_idpersetujuan"
                       type="checkbox" > Saya Setuju Dengan Persyaratan Di Atas
            </label> --}}
                <label class="text-primary">
                    <input id="t_idpersetujuan" name="t_idpersetujuan" type="checkbox"> <i> Dengan ini saya menyatakan bahwa
                        data yang saya masukkan di aplikasi ini adalah benar dan bisa di pertanggung jawabkan ke
                        asliannya.</i>
                </label>
            </div>
        </div>
    </div>

    <form id="form-pengajuan" method="post" action="{{ url('pelayanan/ajukan') }}" class="px-4 pb-3 pt-0">
        @csrf
        <div class="col-12 d-flex justify-content-between ">
            <input type="hidden" id="idPelayananApprove" name="idPelayananApprove" value="">
            <input type="hidden" id="t_id_pelayanan" name="t_id_pelayanan" value="{{ $pelayanan->t_id_pelayanan }}">
            <input type="hidden" id="tgl_perkiraan_selesai" name="tgl_perkiraan_selesai"
                value="{{ $perkiraanSelesai['tglSelesai'] }}">
            <input type="hidden" id="noUrut" name="noUrut" value="" readonly>
            <input type="hidden" id="t_no_pelayanan" name="t_no_pelayanan" value="{{ $pelayanan->t_no_pelayanan ?? '0' }}">
            <input type="hidden" id="s_waktu_pelayanan" name="s_waktu_pelayanan"
                value="{{ $jenisLayanan->s_waktu_pelayanan }}">
            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-cancel">
                <i class="fas fa-backspace"></i> Kembali</button>
            <div class="modal fade" id="modal-cancel">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-danger">
                            <h6 class="modal-title"><span class="fas fa-exclamation"></span> &nbsp;BATALKAN PENGAJUAN
                                PERMOHONAN ?</h6>
                            {{-- <h6 class="modal-title"><span class="fas fa-exclamation"></span> &nbsp;KEMBALI KE PENGAJUAN PERMOHONAN SEBELUMNYA ?</h6> --}}
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {{-- <p>Apakah anda yakin ingin membatalkan pengajuan data permohonan tersebut?</p> --}}
                            <p>Apakah anda yakin ingin kembali ke data permohonan sebelumnya?</p>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span
                                    class="fas fa-times-circle"></span>
                                &nbsp;TUTUP
                            </button>
                            <a href="{{ redirect()->back()->getTargetUrl() }}" class="btn btn-success btn-sm"><span
                                    class="fas fa-exclamation"></span> &nbsp;YA</a>
                        </div>
                    </div>
                </div>
            </div>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-save" id="ajukan"
                name="ajukan" disabled="true">
                <i class="fas fa-upload"></i> Ajukan</button>
            <div class="modal fade" id="modal-save">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-primary">
                            <h6 class="modal-title"><span class="fas fa-send"></span> &nbsp;PENGAJUAN PELAYANAN</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Pastikan data yang anda isi sudah benar. Apakah anda ingin mengajukan layanan ini?
                            </p>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span
                                    class="fas fa-times-circle"></span>
                                &nbsp;TUTUP
                            </button>
                            <button type="submit" class="btn btn-success btn-sm"><span class="fas fa-check"></span>
                                &nbsp;YA
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="modal fade" id="modal-image">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h6 class="modal-title" id="modal-image-title">&nbsp;FILE PERSYARATAN</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center" id="imagePlaceholder">

                </div>
                <div class="modal-footer justify-content-between">

                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(function() {
            $('#t_idpersetujuan').click(function() {
                if ($('#ajukan').attr('disabled')) {
                    $('#ajukan').attr('disabled', false);
                } else {
                    $('#ajukan').attr('disabled', true);
                }
            });
        });

        $(document).ready(function() {
            var quantityItems = $("#quantity").val();

            if (quantityItems > 0) {
                $("#ShowHide").show();
            } else {
                $("#ShowHide").hide();
            }

            @if (!$pelayanan->wajibPajak)
                if ($('#nopPelayanan').val() !== '') {
                    getDataSppt();
                }
            @endif
        });

        function getDataSppt(params) {
            $.get('/pelayanan/getDataSppt', {
                nop: $('#nopPelayanan').val(),
                tahunPajak: $('#tahunPajakPelayanan').val(),
            }, (res) => {
                $("#op_nomor").html($('#nopPelayanan').val());
                $("#op_jalan").html(res.jalan_op);
                $("#op_rt").html(res.rt_op);
                $("#op_rw").html(res.rw_op);
                $("#op_block").html(res.blok_kav_no_op);
                $("#op_desa").html(res.nm_kelurahan);
                $("#op_kecamatan").html(res.nm_kecamatan);
                $("#op_luas_tanah").html(res.luas_bumi_sppt);
                $("#op_luas_bangunan").html(res.luas_bng_sppt);
                $("#op_nomor").html($('#nopPelayanan').val());

                // $("#wp_nik").html(res.jalan_op);
                $("#wp_nama").html(res.nm_wp);
                $("#wp_jalan").html(res.jalan_op);
                $("#wp_rt").html(res.rt_op);
                $("#wp_rw").html(res.rw_op);
                $("#wp_desa").html(res.nm_kelurahan);
                $("#wp_kecamatan").html(res.nm_kecamatan);
                $("#wp_kab_kota").html(res.t_kabupaten_wp);
                $("#wp_telp").html(res.t_no_hp_wp);
                $("#wp_email").html(res.t_email);
                $("#npwpd").html(res.t_npwpd);

            })
        }
    </script>
    <script type="text/javascript">
        $(function() {
            updateUploadedFiles($("#idPelayanan").val());
        })

        function showImage(img) {
            var link = $(img).attr("link");
            var name = $(img).attr("name");
            $("#modal-image-title").text(name);
            var image = $('<img style="max-height:100%; max-width:100%;">');
            image.attr("src", link);
            $("#imagePlaceholder").html(image);
            $("#modal-image").modal('show');
            return false;
        }

        function updateUploadedFiles(idPelayanan) {
            $.get("/pelayanan/upload-file/" + idPelayanan).then(function(data) {
                $("#uploadedFiles").html('');
                var no = 1;
                data.forEach(file => {
                    var tr = '<tr>' +
                        '<td style="text-align: center">' + no + '</td>' +
                        '<td>' + file.t_nama_persyaratan + '</td>' +
                        '<td><a type="button" class="text-primary" link="/' + file.t_lokasi_file +
                        '" name="' + file.t_nama_persyaratan + '" onclick="showImage(this)">' + file
                        .t_nama_persyaratan + '</a></td>' +
                        // '<td><a href="/' + file.t_lokasi_file + '" target="_blank" download>' + "Unduh" + '</td>' +
                        '</tr>';
                    $("#uploadedFiles").append(tr);
                    no++;
                })

            });
        }

        document.addEventListener("DOMContentLoaded", function() {
            const form = document.getElementById('form-pengajuan');
            const loadingModal = document.getElementById('loadingModal');

            form.addEventListener('submit', function(event) {
                jQuery('#modal-save').modal('hide');
                jQuery('#loadingModal').modal('show');
            });
        });
    </script>
@endpush
