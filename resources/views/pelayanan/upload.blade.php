@extends('layouts.app')

@section('title')
    Kepoin || Data Bangunan
@endsection

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <section>
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('pelayanan') }}">Home</a></li>
                            <li class="breadcrumb-item">Pelayanan</li>
                            <li class="breadcrumb-item active">Unggah Syarat Pelayanan</li>
                        </ol>
                    </section>
                </div>
                {{-- <div class="col-12">
                    <div class="info-box callout callout-danger" id="background2">
                        <div class="" style="float: left;">
                            <h5 class="">UNGGAH PERSYARATAN PELAYANAN <span
                                    class="text-uppercase">{{ $data_pelayanan->s_nama_jenis_pelayanan }}</span>
                            </h5>
                            <span class="float">{{ $data_pajak->s_nama_jenis_pajak }}</span>
                        </div>
                        <!-- /.description-block -->
                    </div>
                </div> --}}
            </div>

        </div>

        @if (session('success'))
            <div class="alert alert-dismissible alert_customs_success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-check"></i> Success!</h5>
                <span>{{ session('success') }}</span>
            </div>
        @endif

        @if (session('error'))
            <div class="alert alert-dismissible alert_customs_danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-exclamation-triangle"></i> Gagal!</h5>
                <span>{{ session('error') }}"></span>
            </div>
        @endif
    </div>
    <!-- /.container-fluid -->
    <div class="container-fluid pl-3">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-widget widget-user-2" style="border-top:3px solid #dc3545;">
                            <!-- Add the bg color to the header using any of the bg-* classes -->
                            <div class="widget-user-header">
                                <h5><b>Data Pemohon</b></h5>
                                <hr>
                                <input type="hidden" id="t_id_pelayanan" name="t_id_pelayanan"
                                    value="{{ $data->t_id_pelayanan }}">

                                <address>
                                    <strong>{{ $data->t_nama_pemohon }}</strong><br>
                                    <span>{{ $data->t_jalan_pemohon }}</span>
                                    RT <span>{{ $data->t_rt_pemohon }}</span>/
                                    RW <span>{{ $data->t_rw_pemohon }}</span>,
                                    <span>{{ $data->t_kelurahan_pemohon }}</span>,
                                    <span>{{ $data->t_kecamatan_pemohon }}</span><br>
                                    <span>{{ $data->t_kabupaten_pemohon }}</span>,
                                    <span>{{ $data->t_kode_pos_pemohon ?? '' }}</span><br>
                                    No. HP : <span>{{ $data->t_no_hp_pemohon ?? '-' }}</span><br>
                                    Email : <span>{{ $data->t_email_pemohon ?? '-' }}</span><br>
                                </address>
                            </div>
                            <div class="widget-user-header">
                                <h5><b>Ketentuan Unggah Persyaratan</b></h5>
                                <hr>
                                <address>
                                    <ol>
                                        <li>File unggah hanya berupa gambar
                                            (<code>.jpg, .jpeg, .png dan .bpm</code>)</li>
                                        <li>Ukuran maksimal 1 file unggah <code>5 MB</code></li>
                                        <li>Jika terdapat persyaratan yang membutuhkan lebih dari
                                            <code>1 berkas unggah</code>, maka hal itu diperbolehkan.
                                        </li>
                                    </ol>
                                </address>
                            </div>
                            <div class="widget-user-header bg-danger">
                                <div class="row">
                                    <div class="col-sm-6 border-right text-center">
                                        <h6 class="">Jenis Pajak</h6>
                                        <h5 class="">{{ $data_pajak->s_nama_jenis_pajak }}</h5>
                                    </div>
                                    <div class="col-sm-6 text-center">
                                        <h6 class="">Jenis Pelayanan</h6>
                                        <h5 class="">{{ $data_pelayanan->s_nama_jenis_pelayanan }}</h5>
                                    </div>
                                </div>
                                <!-- /.widget-user-image -->

                            </div>
                            <div class="card-body p-2">
                                <h5><b>DAFTAR PERSYARATAN YANG PERLU DILENGKAPI</b></h5>
                                <hr style="background-color: red; height: 1px">
                                <div class="row">
                                    <div class="col-sm-5 border-right">
                                        <ul class="nav flex-column">
                                            @foreach ($persyaratan as $i => $syarat)
                                                <li class="nav-item">
                                                    <div class="col-12">
                                                        <a href="#" class="nav-link"></a>
                                                        <strong>{{ $i + 1 . '. ' . $syarat->s_nama_persyaratan }}
                                                            {{ $syarat->s_is_optional ? '' : '(Wajib)' }}</strong>
                                                        <input type="hidden" name="idPersyaratan{{ $i + 1 }}"
                                                            id="idPersyaratan{{ $i + 1 }}"
                                                            value="{{ $syarat->s_id_persyaratan }}">
                                                        <input type="hidden" name="idPelayanan{{ $i + 1 }}"
                                                            id="idPelayanan{{ $i + 1 }}"
                                                            value="{{ $data->t_id_pelayanan }}">
                                                        <input type="hidden" name="isOptional"
                                                            id="isOptional{{ $i + 1 }}" value="">
                                                        <div class="input-group">
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input"
                                                                    id="files{{ $i + 1 }}"
                                                                    accept="image/jpeg, image/png" multiple>
                                                                <label class="custom-file-label">Pilih file</label>
                                                            </div>
                                                            <div class="input-group-append">
                                                                <input type="button" class="btn btn-success btnUpload"
                                                                    data-id="{{ $i + 1 }}" value="Unggah">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>

                                    <div class="col-sm-7 text-center">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table-striped table-bordered table-hovered"
                                                    style="font-size: 12pt; width: 100%" cellpadding="3" cellspacing="3">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="4" class="text-green">BERKAS YANG BERHASIL
                                                                DIUNGGAH</th>
                                                        </tr>
                                                        <tr class="bg-success">
                                                            <th
                                                                style="vertical-align: top; width: 10px; text-align: center">
                                                                No.
                                                            </th>
                                                            <th style="vertical-align: top; text-align: center">
                                                                Persyaratan
                                                            </th>
                                                            <th style="vertical-align: top; text-align: center">
                                                                Berkas File
                                                            </th>
                                                            <th style="vertical-align: top; text-align: center">
                                                                DEL
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="uploadedFiles">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer">
                                <button type="button" id="btnPengajuan"
                                    class="btn btn-primary float-right swalDefaultSimpan" onclick="cekUser()">
                                    <i class="fas fa-check"></i> Lanjutkan ke Pengajuan</button>
                                <div class="modal fade" id="modal-save">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header bg-primary">
                                                <h6 class="modal-title"><span class="fas fa-check"></span>
                                                    &nbsp;PENGAJUAN PELAYANAN</h6>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Apakah anda ingin melanjutkan ke pengajuan layanan ini?</p>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <button type="button" class="btn btn-danger btn-sm"
                                                    data-dismiss="modal"><span class="fas fa-times-circle"></span>
                                                    &nbsp;TUTUP
                                                </button>
                                                <a href="{{ url('pelayanan/pengajuan') . '/' . $data->uuid }}"
                                                    class="btn btn-success btn-sm"><span class="fas fa-check"></span>
                                                    &nbsp;YA
                                                </a>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <button type="button" class="btn btn-danger float-left" data-toggle="modal"
                                    data-target="#modal-cancel">
                                    <i class="fas fa-backspace"></i> Kembali
                                </button>
                                <div class="modal fade" id="modal-cancel">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header bg-danger">
                                                <h6 class="modal-title"><span class="fas fa-exclamation"></span>
                                                    &nbsp;KEMBALI KE HALAMAN PELAYANAN ?</h6>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Apakah anda yakin ingin kembali ke halaman pelayanan?</p>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <button type="button" class="btn btn-danger btn-sm"
                                                    data-dismiss="modal"><span class="fas fa-times-circle"></span>
                                                    &nbsp;TUTUP
                                                </button>
                                                <a href="{{ url('pelayanan/edit') . '/' . $data->uuid }}"
                                                    class="btn btn-success btn-sm"><span
                                                        class="fas fa-exclamation"></span>
                                                    &nbsp;YA</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.widget-user -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-image">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h6 class="modal-title" id="modal-image-title">&nbsp;FILE PERSYARATAN</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center" id="imagePlaceholder">

                </div>
                <div class="modal-footer justify-content-between">

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal-delete">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;HAPUS FILE ?</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="file-id-delete">
                    <p>Apakah anda yakin akan menghapus file ini <span id="file-name-delete"></span> ?</p>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span
                            class="fas fa-times-circle"></span> Close
                    </button>
                    <button type="button" class="btn btn-danger btn-sm" onclick="deleteFile()"><span
                            class="fas fa-trash"></span> Hapus
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {

            updateUploadedFiles($("#t_id_pelayanan").val());

            $(".btnUpload").each(function() {
                $(this).click(function(event) {
                    event.preventDefault();
                    doUpload($(this).data('id'), $(this));
                });
            });

            $("input[type=file]").each(function() {
                $(this).change(function() {
                    $(".btnUpload").each(function() {
                        $(this).removeClass("btn-danger");
                        $(this).removeClass("btn-warning");
                        $(this).addClass("btn-success");
                        $(this).val("Upload");
                        $(this).prop("disabled", false);
                    })
                })
            })
        });

        function resetFormUpload() {
            $("input[type=file]").each(function() {
                $(this).val("");
            })
            $(".custom-file-label").each(function() {
                $(this).text("Pilih file");
            })
        }

        function lengkapSyarat(idPelayanan) {
            $.get("/pelayanan/lengkapsyarat/" + idPelayanan).then(function(res) {
                var statuspersyaratan = res;
                if (statuspersyaratan == 1) {
                    $("#btnPengajuan").removeAttr('disabled');
                } else {
                    $("#btnPengajuan").attr('disabled', 'disabled');
                }
            })
        }

        function deleteFile() {
            $.post('/pelayanan/delete-upload-file', {
                idUpload: $("#file-id-delete").val(),
                '_token': $('meta[name="csrf-token"]').attr('content')
            }).then(function(res) {
                updateUploadedFiles($("#t_id_pelayanan").val());
                $("#modal-delete").modal('hide');
            })
        }

        function showDeleteDialog(btn) {
            var fileName = $(btn).data("name");
            var id = $(btn).data("id");
            $("#file-name-delete").text(fileName);
            $("#file-id-delete").val(id);
            $("#modal-delete").modal('show');
        }

        function showImage(img) {
            var link = $(img).attr("link");
            var name = $(img).attr("name");

            $("#modal-image-title").text(name);
            var image = $('<img style="max-height:100%; max-width:100%;">');
            image.attr("src", link);
            $("#imagePlaceholder").html(image);
            $("#modal-image").modal('show');
            return false;
        }

        function doUpload(id, btn) {
            var data = new FormData();
            data.append("idPersyaratan", $("#idPersyaratan" + id).val());
            data.append("idPelayanan", $("#idPelayanan" + id).val());
            var files = $("#files" + id)[0].files;
            $.each(files, function(i, file) {
                data.append("files", file);
            });

            $(btn).prop("disabled", true);

            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/pelayanan/upload-file/multiple",
                data: data,
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function(data) {
                    updateUploadedFiles($("#idPelayanan" + id).val());
                    $(btn).val("Upload Berhasil");
                    $(btn).removeClass("btn-success").addClass("btn-primary");
                    resetFormUpload();
                },
                error: function(e) {
                    console.log("ERROR : ", e);
                    $(btn).val("Upload Gagal");
                    $(btn).removeClass("btn-success").addClass("btn-danger");
                    resetFormUpload();
                }
            });


        }

        function updateUploadedFiles(idPelayanan) {
            $.get("/pelayanan/upload-file/" + idPelayanan).then(function(data) {
                $("#uploadedFiles").html('');
                var no = 1;
                data.forEach(file => {
                    var tr = '<tr>' +
                        '<td style="text-align: center">' + no + '</td>' +
                        '<td style="text-align: left">' + file.t_nama_persyaratan + '</td>' +
                        '<td><a href="#" link="/' + file.t_lokasi_file + '" name="' + file
                        .t_nama_persyaratan + '" onclick="showImage(this)">' + file.t_nama_persyaratan +
                        '</a></td>' +
                        '<td style="text-align: center">' +
                        '<button type="button" data-id="' + file.t_id_persyaratan + '" data-name="' + file
                        .t_nama_persyaratan +
                        '" onclick="showDeleteDialog(this)" class="btn btn-danger btn-sm">' +
                        '<span class="fas fa-trash"></span>' +
                        '</button>' +
                        '</td>' +
                        '</tr>';
                    $("#uploadedFiles").append(tr);
                    no++;
                })
                lengkapSyarat(idPelayanan);
            });
        }

        function cekUser() {
            var idpers = $("#t_id_pelayanan").val();
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/pelayanan/upload-file/cekuser",
                data: {
                    idpelayanan: idpers
                },
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    if (data == 1) {
                        $('#modal-save').modal('show');
                    } else {
                        alert('Mohon Lengkapi Dokumen!');
                        return false
                    }
                },
                error: function(e) {
                    alert('Terjadi Kesalahan!');
                    return false
                }
            });
        }
    </script>
@endpush
