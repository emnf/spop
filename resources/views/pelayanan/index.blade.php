@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Pelayanan
@endsection

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="">Home</a></li>
                        <li class="breadcrumb-item active">Pelayanan</li>
                    </ol>
                </div><!-- /.col -->

            </div><!-- /.container-fluid -->
            @empty(isset($success))
                <div class="alert alert-dismissible alert_customs_success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-check"></i> Success!</h5>
                    <span>{{ $success }}</span>
                </div>
            @endempty

            @empty(isset($warning))
                <div class="alert alert-dismissible alert_customs_danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-exclamation-triangle"></i> Gagal!</h5>
                    <span>{{ $warning }}</span>
                </div>
            @endempty
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="{{ url('pelayanan/jenis-layanan/10') }}" class="mt-4 mb-2 btn btn-danger btn-sm"
                                data-toggle="tooltip" data-placement="right" {{-- title="Untuk menambahkan data pelayanan baru" --}}><span
                                    class="fas fa-file"></span>&nbsp; Pilih
                                Jenis Pelayanan PBB</a>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>

                    <div class="row" id="data-pengajuan">
                        <div class="col-12">
                            <div class="card card-danger card-outline" style="border-top:3px solid #dc3545;">
                                <div class="card-header">
                                    <h3 class="card-title">Data Pengajuan</h3>
                                    <div class="card-tools">
                                        <a href="javascript:printDaftarPengajuan('PDF')" class="btn btn-danger btn-sm"><span
                                                class="fas fa-file-pdf"></span> &nbsp;PDF</a>
                                        <a href="javascript:printDaftarPengajuan('XLS')"
                                            class="btn btn-success btn-sm"><span class="fas fa-file-excel"></span>
                                            &nbsp;XLS</a>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body table-responsive p-2">
                                    <table class="table table-hover table-bordered table-striped text-nowrap"
                                        id="pelayanan-table" style="font-size: 10pt;">
                                        <thead>
                                            <tr>
                                                <th style="width: 10px; background-color: #1fc8e3">No</th>
                                                <th style="width: 10px; background-color: #1fc8e3">Jenis Pajak</th>
                                                <th style="width: 10px; background-color: #1fc8e3">Jenis Layanan</th>
                                                <th style="width: 10px; background-color: #1fc8e3">Tanggal</th>
                                                <th style="width: 10px; background-color: #1fc8e3">Status</th>
                                                <th style="width: 10px; background-color: #1fc8e3">Pemohon</th>
                                                <th style="width: 10px; background-color: #1fc8e3">Alasan</th>
                                                <th style="width: 10px; background-color: #1fc8e3">User Pemohon</th>
                                                <th style="width: 10px; background-color: #1fc8e3">Action</th>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <th>
                                                    {{-- <select class="form-control form-control-sm"
                                                    id="filter-idJenisPajak">
                                                    <option value="">Silahkan Pilih</option>
                                                    @foreach ($dataPajak as $item)
                                                    <option value="{{ $item->s_id_jenis_pajak  }}">{{
                                                        $item->s_nama_singkat_pajak }}</option>
                                                    @endforeach
                                                </select> --}}
                                                </th>
                                                {{-- <th><input class="form-control form-control-sm"
                                                    id="filter-namaJenisPelayanan" /></th> --}}
                                                <th>
                                                    <select class="form-control form-control-sm"
                                                        id="filter-idJenisPelayanan">
                                                        <option value="">Semua Jenis Pelayanan</option>
                                                        @foreach ($jenispelayanan as $jp)
                                                            <option value="{{ $jp->s_id_jenis_pelayanan }}">
                                                                {{ $jp->s_nama_jenis_pelayanan }}</option>
                                                        @endforeach
                                                    </select>
                                                </th>
                                                <th><input class="form-control form-control-sm" id="filter-tglPelayanan"
                                                        readonly="true" /></th>
                                                <th>
                                                    <select class="form-control form-control-sm" id="filter-status">
                                                        <option value="">Silahkan Pilih</option>
                                                        <option value="1">Draft</option>
                                                        <option value="2">Proses</option>
                                                        <option value="3">Ditolak</option>
                                                        <option value="4">Diterima</option>
                                                        <option value="5">Selesai</option>
                                                    </select>
                                                </th>
                                                <th><input class="form-control form-control-sm" id="filter-pemohon" />
                                                </th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="text-center" colspan="9"> Tidak ada data.</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer clearfix pagination-footer">
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        var datatableObjek = datagrid({
            url: "/pelayanan/datagrid",
            table: "#pelayanan-table",
            columns: [{
                    class: "text-center"
                },
                {
                    class: "",
                },
                {
                    class: "text-center"
                },
                {
                    class: "text-center"
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                }, {
                    class: ""
                }
            ],
            orders: [{
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
            ],
            action: [{
                    name: 'hapus-pelayanan',
                    btnClass: 'btn btn-danger btn-sm mr-1',
                    btnText: '<i class="fas fa-trash"></i> Hapus',
                    btnTitle: 'Hapus Data Pelayanan',
                },
                {
                    name: 'edit-pelayanan',
                    btnClass: 'btn btn-warning btn-sm mr-1',
                    btnText: '<i class="fas fa-edit"></i> Edit',
                    btnTitle: 'Edit Data Pelayanan',
                },
                {
                    name: 'upload-pelayanan',
                    btnClass: 'btn btn-primary btn-sm mr-1',
                    btnText: '<i class="fas fa-file-pdf"></i> Unggah Berkas',
                    btnTitle: 'Unggah Berkas Persyaratan Pelayanan',
                },
                {
                    name: 'tanda-terima-pelayanan',
                    btnClass: 'btn btn-danger btn-sm mr-1',
                    btnText: '<i class="fas fa-hand-point-up"></i> Tanda Terima',
                    btnTitle: 'Cetak Tanda Terima Permohonan Pelayanan',
                },
                {
                    name: 'cetak-spop',
                    btnClass: 'btn btn-primary btn-sm mr-1',
                    btnText: '<i class="fas fa-print"></i> Cetak SPOP-LSPOP',
                    btnTitle: 'Cetak Tanda Terima Permohonan Pelayanan',
                    target: '_balnk'
                },
                {
                    name: 'ajukan-pelayanan',
                    btnClass: 'btn btn-primary btn-sm mr-1',
                    btnText: '<i class="fas fa-file"></i> Ajukan',
                    btnTitle: 'Ajukan Permohonan Pelayanan',
                },
                {
                    name: 'cetak-sppt',
                    btnClass: 'btn btn-success btn-sm mr-1',
                    btnText: '<i class="fas fa-print"></i> SPPT',
                    btnTitle: 'SPPT',
                    target: '_balnk'
                }
            ]

        });


        $("#filter-namaJenisPelayanan, #filter-t_nama_kelurahan_objek, #filter-t_nama_kecamatan_objek,#filter-pemohon")
            .keyup(() => searchObjek());

        $("#filter-status, #filter-idJenisPelayanan, #filter-idJenisPajak")
            .change(() => searchObjek())

        $('#filter-tglPelayanan').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-tglPelayanan').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            searchObjek();
        });

        $('#filter-tglPelayanan').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            searchObjek();
        });

        $("#filter-tglPelayanan, #filter-idJenisPajak, #filter-idJenisPelayanan").change(function() {
            searchObjek();
        });

        function searchObjek() {
            datatableObjek.setFilters({
                jenis_pajak: $("#filter-idJenisPajak").val(),
                nama_jenis_pelayanan: $("#filter-namaJenisPelayanan").val(),
                id_jenis_pelayanan: $("#filter-idJenisPelayanan").val(),
                tgl_pelayanan: $("#filter-tglPelayanan").val(),
                nama_pemohon: $("#filter-pemohon").val(),
                status: $("#filter-status").val(),
            });
            datatableObjek.reload();
        }
        searchObjek();

        function openPelaporan(a) {
            var idJenisObjek = $("#t_id_jenis_objek").val();
            window.open('/sptpd/tambah/' + a);
        }

        function showModalCetakTtp(a) {
            window.open(`{{ url('pelayanan') }}/${a}/cetak-tanda-terima`, '_blank')
        }

        function showModalHapusPelayanan(a) {
            var konfirmasi = confirm("Data Pelayanan akan dihapus?");
            if (konfirmasi) {
                $.ajax({
                    url: '/pelayanan/delete',
                    type: 'POST',
                    data: {
                        id: a
                    },
                    success: function(response) {
                        // console.log(response);
                        searchObjek();
                    },
                    error: function(xhr, status, error) {
                        console.error(xhr.responseText);
                    }
                });
            } else {
                return false();
            }
        }


        function printDaftarPengajuan(a) {
            let tglPealyanan = $('#filter-tglPelayanan').val();
            let jenisPelayanan = $("#filter-namaJenisPelayanan").val();
            let status = $("#filter-status").val();

            window.open(
                `{{ url('pelayanan/cetak-daftar-pelayanan') }}?format=${a}&tglPealyanan=${tglPealyanan}&jenisPelayanan${jenisPelayanan}&status=${status}`,
                '_blank')
        }
    </script>
@endpush
