<div class="modal fade" id="modal-sudah-ajukan">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-exclamation"></span>
                    &nbsp;PEMBERITAHUAN</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span
                        class="fas fa-times-circle"></span>
                    &nbsp;TUTUP
                </button>
                <a th:href="@{/pelayanan}" class="btn btn-success btn-sm"><span
                        class="fas fa-exclamation"></span> &nbsp;YA</a>
            </div>
        </div>
    </div>
</div>
