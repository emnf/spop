<div class="col-12 pb-3" id="div-readonly">
    <div class="form-group-sm row">
        <label class="col-sm-2 col-form-label-sm">NIK <span class="text-danger">*</span></label>
        <div class="col-sm-6">
            <input type="hidden" class="form-control" id="t_id_pelayanan" name="t_id_pelayanan"
                value="{{ isset($pelayanan->t_id_pelayanan) ? $pelayanan->t_id_pelayanan : '' }}">
            <input type="hidden" class="form-control" id="lunasPbb" name="lunasPbb"
                value="{{ isset($jenisLayanan->s_lunas_pbb) ? $jenisLayanan->s_lunas_pbb : '' }}">
            <input type="hidden" class="form-control" id="t_id_jenis_pelayanan" name="t_id_jenis_pelayanan"
                value="{{ isset($pelayanan->t_id_jenis_pelayanan) ? $pelayanan->t_id_jenis_pelayanan : $jenisLayanan->s_id_jenis_pelayanan }}">
            <input type="hidden" class="form-control" id="t_id_jenis_pajak" name="t_id_jenis_pajak"
                value="{{ isset($pelayanan->t_id_jenis_pajak) ? $pelayanan->t_id_jenis_pajak : $jenisPajak->s_id_jenis_pajak }}">
            <input type="text" value="{{ isset($pelayanan->t_nik_pemohon) ? $pelayanan->t_nik_pemohon : '' }}"
                class="form-control form-control-sm masked-nik" id="t_nik_pemohon" name="t_nik_pemohon" maxlength="18"
                placeholder="" required>
        </div>
    </div>
    <div class="form-group-sm row">
        <label class="col-sm-2 col-form-label-sm">Nama <span class="text-danger">*</span></label>
        <div class="col-sm-6">
            <input type="text"
                value="{{ old('t_nama_pemohon') ?? isset($pelayanan->t_nama_pemohon) ? $pelayanan->t_nama_pemohon : '' }}"
                class="form-control form-control-sm" id="t_nama_pemohon" name="t_nama_pemohon" placeholder="" required
                readonly>
        </div>
    </div>
    <div class="form-group-sm row">
        <label class="col-sm-2 col-form-label-sm">Jalan <span class="text-danger">*</span></label>
        <div class="col-sm-6">
            <input type="text"
                value="{{ old('t_jalan_pemohon') ?? isset($pelayanan->t_jalan_pemohon) ? $pelayanan->t_jalan_pemohon : '' }}"
                class="form-control form-control-sm" id="t_jalan_pemohon" name="t_jalan_pemohon" placeholder="" required
                readonly>
        </div>
    </div>
    <div class="form-group-sm row">
        <label class="col-sm-2 col-form-label-sm">RT <span class="text-danger">*</span></label>
        <div class="col-sm-2">
            <input type="text"
                value="{{ old('t_rt_pemohon') ?? isset($pelayanan->t_rt_pemohon) ? $pelayanan->t_rt_pemohon : '' }}"
                class="form-control form-control-sm masked-number-3" id="t_rt_pemohon" name="t_rt_pemohon"
                maxlength="3" placeholder="" required readonly>
        </div>
        <label class="col-sm-2 col-form-label-sm">RW <span class="text-danger">*</span></label>
        <div class="col-sm-2">
            <input type="text"
                value="{{ old('t_rw_pemohon') ?? isset($pelayanan->t_rw_pemohon) ? $pelayanan->t_rw_pemohon : '' }}"
                class="form-control form-control-sm masked-number-2" id="t_rw_pemohon" name="t_rw_pemohon"
                maxlength="2" placeholder="" required readonly>
        </div>
    </div>
    <div class="form-group-sm row">
        <label class="col-sm-2 col-form-label-sm">Kelurahan <span class="text-danger">*</span></label>
        <div class="col-sm-6">
            <input type="text"
                value="{{ old('t_kelurahan_pemohon') ?? isset($pelayanan->t_kelurahan_pemohon) ? $pelayanan->t_kelurahan_pemohon : '' }}"
                class="form-control form-control-sm" id="t_kelurahan_pemohon" name="t_kelurahan_pemohon" placeholder=""
                required readonly>
        </div>
    </div>
    <div class="form-group-sm row">
        <label class="col-sm-2 col-form-label-sm">Kecamatan <span class="text-danger">*</span></label>
        <div class="col-sm-6">
            <input type="text"
                value="{{ old('t_kecamatan_pemohon') ?? isset($pelayanan->t_kecamatan_pemohon) ? $pelayanan->t_kecamatan_pemohon : '' }}"
                class="form-control form-control-sm" id="t_kecamatan_pemohon" name="t_kecamatan_pemohon" placeholder=""
                required readonly>
        </div>
    </div>
    <div class="form-group-sm row">
        <label class="col-sm-2 col-form-label-sm">Kabupaten/Kota <span class="text-danger">*</span></label>
        <div class="col-sm-6">
            <input type="text"
                value="{{ old('t_kabupaten_pemohon') ?? isset($pelayanan->t_kabupaten_pemohon) ? $pelayanan->t_kabupaten_pemohon : '' }}"
                class="form-control form-control-sm" id="t_kabupaten_pemohon" name="t_kabupaten_pemohon"
                placeholder="" required readonly>
        </div>
    </div>
    <div class="form-group-sm row">
        <label class="col-sm-2 col-form-label-sm">Kode Pos</label>
        <div class="col-sm-5">
            <input type="text"
                value="{{ old('t_kode_pos_pemohon') ?? isset($pelayanan->t_kode_pos_pemohon) ? $pelayanan->t_kode_pos_pemohon : '' }}"
                class="form-control form-control-sm masked-number-5" id="t_kode_pos_pemohon"
                name="t_kode_pos_pemohon" maxlength="5" placeholder="" readonly>
        </div>
    </div>
    <div class="form-group-sm row">
        <label class="col-sm-2 col-form-label-sm">Email</label>
        <div class="col-sm-6">
            <input type="text"
                value="{{ old('t_email_pemohon') ?? isset($pelayanan->t_email_pemohon) ? $pelayanan->t_email_pemohon : '' }}"
                class="form-control form-control-sm" id="t_email_pemohon" name="t_email_pemohon" placeholder=""
                readonly>
        </div>
    </div>
    <div class="form-group-sm row">
        <label class="col-sm-2 col-form-label-sm">No. HP</label>
        <div class="col-sm-6">
            <input type="text"
                value="{{ old('t_no_hp_pemohon') ?? isset($pelayanan->t_no_hp_pemohon) ? $pelayanan->t_no_hp_pemohon : '' }}"
                class="form-control form-control-sm masked-hp" id="t_no_hp_pemohon" name="t_no_hp_pemohon"
                placeholder="" readonly>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $(function() {
            $("#t_nik_pemohon").change(function() {
                $.ajax({
                    url: '/pelayanan/check-nik',
                    type: 'GET',
                    data: {
                        t_nik_pemohon: $("#t_nik_pemohon").val(),
                    }
                }).then(function(response) {
                    let data = response.pemohon;
                    let readOnlyDiv = $("#div-readonly input");
                    // alert(data);
                    if (data == null) {
                        readOnlyDiv.map((key, inputRO) => {
                            $("#" + inputRO.id).removeAttr('readonly');
                            if ($("#lunasPbb").val() !== '1') {
                                $("#btnSimpanDraft").attr('disabled', false);
                            }
                        })
                    } else {
                        $("#t_nama_pemohon").val(data.t_nama_pemohon);
                        $("#t_jalan_pemohon").val(data.t_jalan_pemohon);
                        $("#t_rt_pemohon").val(data.t_rt_pemohon);
                        $("#t_rw_pemohon").val(data.t_rw_pemohon);
                        $("#t_kelurahan_pemohon").val(data.t_kelurahan_pemohon);
                        $("#t_kecamatan_pemohon").val(data.t_kecamatan_pemohon);
                        $("#t_kabupaten_pemohon").val(data.t_kabupaten_pemohon);
                        $("#t_kode_pos_pemohon").val(data.t_kode_pos_pemohon);
                        $("#t_no_hp_pemohon").val(data.t_no_hp_pemohon);
                        $("#t_email_pemohon").val(data.t_email_pemohon);
                        if ($("#lunasPbb").val() !== '1') {
                            $("#btnSimpanDraft").attr('disabled', false);
                        }
                        // sudahAjukanPermohonan()
                        @if ($jenisLayanan['s_id_jenis_pelayanan'] == 1)
                            sudahAjukanPermohonanOpBaru()
                        @endif
                    }

                });
            });
        })

        function sudahAjukanPermohonan(nopPbb = null) {
            if (nopPbb == null) {
                nopPbb = $("#nop").val()
            }

            $.get("/pelayanan/pengajuan-sebelumnya/" + $("#nikPemohon").val() + "/" + $("#idJenisPelayanan")
                    .val() + "/" + $("#idJenisPajak").val() + "/" + nopPbb)
                .then(function(res) {
                    var data = res.data;
                    var tbodyMessages = "";
                    $("#modal-sudah-ajukan > .modal-body").html("");

                    if (data.idPelayanan === null) {
                        if ($("#lunasPbb").val() !== '1') {
                            $("#btnSimpanDraft").removeAttr('disabled');
                        }
                    } else {
                        tbodyMessages = '<div class="col-12">' +
                            '<div class="alert alert-info alert-dismissible">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
                            'Permohonan anda sudah pernah direkam / diajukan sebelumnya, dengan data sebagai berikut:' +
                            '</div>' +
                            '<div class="form-group-sm row">' +
                            '<label class="col-sm-4 col-form-label-sm">No. Pelayanan</label>' +
                            '<div class="col-sm-8"> : <strong class="text-blue">' + data.noPelayanan +
                            '</strong> </div>' +
                            '</div>' +
                            '<div class="form-group-sm row">' +
                            '<label class="col-sm-4 col-form-label-sm">NIK Pemohon</label>' +
                            '<div class="col-sm-8"> : ' + data.nikPemohon + ' </div>' +
                            '</div>' +
                            '<div class="form-group-sm row">' +
                            '<label class="col-sm-4 col-form-label-sm">Nama Pemohon</label>' +
                            '<div class="col-sm-8"> : ' + data.namaPemohon + ' </div>' +
                            '</div>' +
                            '<div class="form-group-sm row">' +
                            '<label class="col-sm-4 col-form-label-sm">NOP</label>' +
                            '<div class="col-sm-8"> : ' + data.nop + ' </div>' +
                            '</div>' +
                            '<div class="form-group-sm row">' +
                            '<label class="col-sm-4 col-form-label-sm">Jenis Pelayanan</label>' +
                            '<div class="col-sm-8"> : ' + data.jenisPelayanan + ' </div>' +
                            '</div>' +
                            '<div class="form-group-sm row">' +
                            '<label class="col-sm-4 col-form-label-sm">Jenis Pajak</label>' +
                            '<div class="col-sm-8"> : ' + data.jenisPajak + ' </div>' +
                            '</div>' +
                            '<div class="form-group-sm row">' +
                            '<label class="col-sm-4 col-form-label-sm">Status Permohonan</label>' +
                            '<div class="col-sm-8"> : <span class="btn btn-sm bg-warning">' + data
                            .statusPermohonan + '</span> </div>' +
                            '</div>' +
                            '</div>';

                        $("#modal-sudah-ajukan").modal("show");
                        if ($("#lunasPbb").val() !== '1') {
                            $("#btnSimpanDraft").removeAttr('disabled');
                        }
                    }
                    $("#modal-sudah-ajukan > .modal-body").append('tbodyMessages');
                });
        }
    </script>
@endpush
