<div class="row">
    <div class="col-md-6">
        <p style="font-weight: bold">DATA OBJEK PAJAK (LAMA)</p>
        <hr>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">NOP Asal/Lama</label>
            <div class="col-sm-6">
                <input type="hidden" id="t_id_op_lama" name="t_id_op_lama" value="{{ $pelayanan->OpLama->t_id_op_lama ?? '' }}">

                <input type="text" class="form-control form-control-sm masked-nop" id="t_nop" name="t_nop" value="{{ $pelayanan->nop ?? '' }}" placeholder="" required>

                <input type="hidden" id="nopLama" name="nopLama" value="{{ $pelayanan->nop ?? '' }}" required>
            </div>
            <div class="col-sm-2">
                <button type="button" class="btn btn-sm btn-primary" id="btnCariNOP"><span class="fas fa-search"></span>
                    CARI
                    {{-- <samp class="submitspinner"></samp> --}}
                    <samp class="spinner-border-sm"></samp>
                </button>
            </div>
        </div>

        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">RT</label>
            <div class="col-sm-2">
                <input type="text" class="form-control form-control-sm masked-number-3" id="t_rt_op_lama" name="t_rt_op_lama" value="{{ $pelayanan->OpLama->t_rt_op ?? '' }}" readonly="">
            </div>
            <label class="col-sm-2 col-form-label-sm">RW</label>
            <div class="col-sm-2">
                <input type="text" class="form-control form-control-sm masked-number-2" id="t_rw_op_lama" name="t_rw_op_lama" value="{{ $pelayanan->OpLama->t_rw_op ?? '' }}" readonly="">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Jalan</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_jalan_op_lama" name="t_jalan_op_lama" value="{{ $pelayanan->OpLama->t_jalan_op ?? '' }}" readonly="">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Blok/No/Kav</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_blok_kav"
                    name="t_blok_kav" value="{{ $pelayanan->Op->t_blok_kav ?? '-' }}" readonly="">
            </div>
        </div>
        <input type="hidden" id="no_urut_lama" name="no_urut_lama">
        <input type="hidden" id="kd_jns_op_lama" name="kd_jns_op_lama">
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_kelurahan_op_lama" name="t_kelurahan_op_lama" value="{{ $pelayanan->OpLama->t_kelurahan_op ?? '' }} " readonly="">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Kecamatan</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_kecamatan_op_lama" name="t_kecamatan_op_lama" value="{{ $pelayanan->OpLama->t_kecamatan_op ?? '' }}" readonly="">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Luas Tanah
                (M<sup>2</sup>)</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_luas_tanah_lama" name="t_luas_tanah_lama" value="{{ $pelayanan->OpLama->t_luas_tanah ?? '' }}" readonly="" required>
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Luas Bangunan
                (M<sup>2</sup>)</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_luas_bangunan_lama" name="t_luas_bangunan_lama" value="{{ $pelayanan->OpLama->t_luas_bangunan ?? '' }}" readonly="" required>
            </div>
        </div>
    </div>


    <div class="col-md-6">
        <p style="font-weight: bold">DATA WAJIB PAJAK (LAMA)</p>
        <hr>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">NIK</label>
            <div class="col-sm-8">
                <input type="hidden" id="t_id_wp_lama" name="t_id_wp_lama" value="{{ $pelayanan->WpLama->t_id_wp_lama  ?? '' }}">
                <input type="text" class="form-control form-control-sm masked-nik" id="t_nik_wp_lama" name="t_nik_wp_lama" value="{{ $pelayanan->WpLama->t_nik_wp ?? '' }}" readonly="" required>
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Nama</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_nama_wp_lama" name="t_nama_wp_lama" value="{{ $pelayanan->WpLama->t_nama_wp ?? '' }}" readonly="" required>
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">RT</label>
            <div class="col-sm-2">
                <input type="text" class="form-control form-control-sm masked-number-3" id="t_rt_wp_lama" name="t_rt_wp_lama" value="{{ $pelayanan->WpLama->t_rt_wp ?? '' }}" readonly="">
            </div>
            <label class="col-sm-2 col-form-label-sm">RW</label>
            <div class="col-sm-2">
                <input type="text" class="form-control form-control-sm masked-number-2" id="t_rw_wp_lama" name="t_rw_wp_lama" value="{{ $pelayanan->WpLama->t_rw_wp ?? '' }}" readonly="">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Jalan</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_jalan_wp_lama" name="t_jalan_wp_lama" value="{{ $pelayanan->WpLama->t_jalan_wp ?? '' }}" readonly="">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Blok/No/Kav</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_blok_kav_wp_lama" name="t_blok_kav_wp_lama"
                    value="{{ $pelayanan->WpLama->t_blok_kav_wp ?? '' }}" readonly="">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_kelurahan_wp_lama" name="t_kelurahan_wp_lama" value="{{ $pelayanan->WpLama->t_kelurahan_wp ?? '' }}" readonly="">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Kecamatan</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_kecamatan_wp_lama" name="t_kecamatan_wp_lama" value="{{ $pelayanan->WpLama->t_kecamatan_wp ?? '' }}" readonly="">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Kabupaten/Kota</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_kabupaten_wp_lama" name="t_kabupaten_wp_lama" value="{{ $pelayanan->WpLama->t_kabupaten_wp ?? '' }}" readonly="">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">No. Telp</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm masked-hp" onkeypress="" id="t_no_hp_wp_lama" name="t_no_hp_wp_lama" value="{{ $pelayanan->WpLama->t_no_hp_wp ?? '' }}" readonly="">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalTunggakanPBB" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title">Daftar Tunggakan PBB {{ ($jenisLayanan->s_lunas_pbb == true) ? 'Yang Wajib Di Lunasi' : ''; }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="datatunggakan"></div>
            </div>
            <div class="modal-footer right">
                <button type="button" class="btn btn-outline-light" data-dismiss="modal"
                    onclick="tutupmodaltunggakan()">Tutup</button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    // cek nop
    $(document).ready(() => {
        setDati();
        @if(isset($pelayanan) && !is_null($pelayanan->opLama))
        $('#t_nop').val('{{$pelayanan->opLama->nopFormated}}')
        @endif
    })

    function setDati() {
        let dati = '{{ substr($kdKabkota, 0, 2) .'.'. substr($kdKabkota, 2) }}';
        $('#t_nop').val(dati)
    }

    function getToday() {
        var d = new Date();
        var strDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
        return strDate;
    }

    function getDataPbb() {
        @if(request()->segment(2) == 'edit')
            let currentUrl = "/pelayanan/{{ request()->segment(2) }}/"+ $('#t_id_jenis_pelayanan').val() +"/get-data-pbb"
        @else
            let currentUrl = "/pelayanan/{{ request()->segment(2) }}/"+ $('#t_id_jenis_pajak').val() +"/"+$('#t_id_jenis_pelayanan').val() +"/get-data-pbb"
        @endif

        $.ajax({
            url: currentUrl,
            type: 'GET',
            data: {
                nop: $('#t_nop').val()
            }
        }).then(function(response) {
            var aa = response;

            if (aa == '') {
                alert('NOP tidak ditemukan');
            }

            $("#t_rt_op_lama").val(aa.rt_op);
            $("#t_rw_op_lama").val(aa.rw_op);
            $("#t_jalan_op_lama").val(aa.jalan_op);
            $("#kd_blok_lama").val(aa.kd_blok);
            $("#t_kecamatan_op_lama").val(aa.nm_kecamatan);
            $("#t_kelurahan_op_lama").val(aa.nm_kelurahan);
            $("#t_luas_tanah_lama").val(aa.total_luas_bumi);
            $("#t_luas_bangunan_lama").val(aa.total_luas_bng);
            $("#no_urut_lama").val(aa.no_urut);
            $("#kd_jns_op_lama").val(aa.kd_jns_op);

            $("#t_nik_wp_lama").val(aa.subjek_pajak_id);
            $("#t_nama_wp_lama").val(aa.nm_wp);
            $("#t_rt_wp_lama").val(aa.rt_wp);
            $("#t_rw_wp_lama").val(aa.rw_wp);
            $("#t_jalan_wp_lama").val(aa.jalan_wp);
            $("#t_kelurahan_wp_lama").val(aa.kelurahan_wp);
            $("#t_kecamatan_wp_lama").val(aa.kecamatan_wp);
            $("#t_kabupaten_wp_lama").val(aa.kota_wp);
            $("#t_no_hp_wp_lama").val(aa.telp_wp);
            $("#t_blok_kav_wp_lama").val(aa.blok_kav_no_wp);

            $("#t_luas_bangunan").val(aa.total_luas_bng).attr('readonly', 'readonly');
            $("#no_urut").val(aa.no_urut);
            $("#kd_jns_op").val(aa.kd_jns_op);

            $("#t_luas_tanah").val(aa.total_luas_bumi).attr('readonly', 'readonly');
            $("#t_jalan_op").val(aa.jalan_op).attr('readonly', 'readonly');
            $("#t_rt_op").val(aa.rt_op).attr('readonly', 'readonly');
            $("#t_rw_op").val(aa.rw_op).attr('readonly', 'readonly');
            $("#kd_blok").val(aa.kd_blok).attr('readonly', 'readonly');
            $("#t_kecamatan_op").val(aa.nm_kecamatan).attr('readonly', 'readonly');
            $("#t_kelurahan_op").val(aa.nm_kelurahan).attr('readonly', 'readonly');
            $("#t_jenis_tanah_value").val(aa.jns_bumi).attr('disabled', true);
            $("#t_jenis_tanah").val(aa.jns_bumi).attr('readonly', true);

            // WpBaru
            // $("#t_nik_wp").val($("#t_nik_pemohon").val());
            // $("#t_nama_wp").val($("#t_nama_pemohon").val());
            // $("#t_jalan_wp").val($("#t_jalan_pemohon").val());
            // $("#t_rt_wp").val($("#t_rt_pemohon").val());
            // $("#t_rw_wp").val($("#t_rw_pemohon").val());
            // $("#t_kelurahan_wp").val($("#t_kelurahan_pemohon").val());
            // $("#t_kecamatan_wp").val($("#t_kecamatan_pemohon").val());
            // $("#t_kabupaten_wp").val($("#t_kabupaten_pemohon").val());
            // $("#t_kode_pos_wp").val($("#t_kode_pos_pemohon").val());
            // $("#t_no_hp_wp").val($("#t_no_hp_pemohon").val());
            // $("#t_email").val($("#t_email_pemohon").val());

            $("#t_tahun_pajak").val(aa.tahunPajak);
            $("#t_tgl_pelayanan").val(getToday());

            if ($("#rinciLuasBangunan").length > 0) {
                $("#t_luas_tanah").removeAttr('readonly')
                $("#t_jalan_op").removeAttr('readonly')
                $("#t_rt_op").removeAttr('readonly')
                $("#t_rw_op").removeAttr('readonly')
                $("#kd_blok").removeAttr('readonly')
                getOpBangunan1();
                getOpBangunan();
            }
        });
    }

    function getTunggakanSppt(){
        let currentUrl = "/pelayanan/cektunggakanpbb"

        $.ajax({
            url: currentUrl,
            type: 'POST',
            data: {
                nop: $('#t_nop').val()
            }
        }).then(function(response) {
            var aa = response;
            if (aa.PBB_YG_HARUS_DIBAYAR_SPPT != '' && aa.datatunggakan != 3) {
                jQuery('#ModalTunggakanPBB').modal('show');
                $('#datatunggakan').html(aa.datatunggakan);
                // $('#btnSimpanDraft').attr('disabled','disabled');
            } else if(aa.PBB_YG_HARUS_DIBAYAR_SPPT == '' && aa.datatunggakan != 3){
                alert('Tidak Ada Tunggakan');
                $("#btnSimpanDraft").removeAttr('disabled');
            }
        });
    }

    function tutupmodaltunggakan() {
        jQuery('#ModalTunggakanPBB').modal('hide');
    }

    $('#btnCariNOP').click(function() {
        getDataPbb();
        getTunggakanSppt();
    });

    function getOpBangunan1() {
        $.get("/dat-objek-pajak/bangunan", {
                'nop': $('#t_nop').val()
            })
            .then(function(res) {
                let aa = res;
                let arr = [];
                aa.forEach((entry, index) => {

                    arr.push(
                        '<div className="form-group-sm row">' +
                        '<input type="hidden" value="' + entry.no_bng + '" name="nomorBangunans[' + index + ']">' +
                        '<label className="col-sm-4 col-form-label-sm">Bangunan Ke-' + entry.no_bng +
                        '</label>' +
                        '<div className="col-sm-4 ">' +
                        '<input type="text" class="form-control form-control-sm"' +
                        'value="' + entry.luas_bng + '" name="luasOpBangunan[' + index +
                        ']" onChange="hitungLuasBangunan()" onkeypress="return onlyNumberKey(event)" onBlur="hitungLuasBangunan()">' +
                        '</div></div>');
                });

                $("#luasBangunan").html(arr);
            });
    }

    function hitungLuasBangunan() {
        let total = 0;
        let values = $("input[name^='luasOpBangunan']")
            .map(function() {
                return $(this).val();
            }).get();

        values.forEach((arr) => {
            total += parseFloat(arr);
        })
        $("#t_luas_bangunan").val(total);
    }
</script>
@endpush
