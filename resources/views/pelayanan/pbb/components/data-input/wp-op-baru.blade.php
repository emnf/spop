<div class="row">

    <div class="col-md-6">
        <hr style="background-color: #28a745; height: 2px;">
        <p style="font-weight: bold">DATA OBJEK PAJAK (BARU)</p>
        <hr>
        <div class="form-group-sm row div-opbaru">
            <label class="col-sm-4 col-form-label-sm">RT</label>
            <div class="col-sm-2">
                <input type="hidden" id="t_id_op" name="t_id_op" value="{{ $pelayanan->Op->t_id_op ?? '' }}">
                <input type="text" class="form-control form-control-sm masked-number-3" id="t_rt_op" name="t_rt_op"
                    value="{{ $pelayanan->Op->t_rt_op ?? '' }}" onkeypress="return onlyNumberKey(event)" readonly="">
            </div>
            <label class="col-sm-2 col-form-label-sm">RW</label>
            <div class="col-sm-2">
                <input type="text" class="form-control form-control-sm masked-number-2" id="t_rw_op" name="t_rw_op"
                    value="{{ $pelayanan->Op->t_rw_op ?? '' }}" onkeypress="return onlyNumberKey(event)" readonly="">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Jalan</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_jalan_op" name="t_jalan_op"
                    value="{{ $pelayanan->Op->t_jalan_op ?? '' }}" readonly="">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Blok/No/Kav</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_blok_kav" name="t_blok_kav"
                    value="{{ $pelayanan->Op->t_blok_kav ?? '' }}">
            </div>
        </div>
        <input type="hidden" id="no_urut" name="no_urut" value="{{ $pelayanan->Op->no_urut ?? '' }}">
        <input type="hidden" id="kd_jns_op" name="kd_jns_op" value="{{ $pelayanan->Op->kd_jns_op ?? '' }}">
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_kelurahan_op" name="t_kelurahan_op"
                    value="{{ $pelayanan->Op->t_kelurahan_op ?? '' }}" readonly="">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Kecamatan</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_kecamatan_op" name="t_kecamatan_op"
                    value="{{ $pelayanan->Op->t_kecamatan_op ?? '' }}" readonly="">
            </div>
        </div>
        @if ($jenisLayanan['s_id_jenis_pelayanan'] == 2)
            <div class="form-group-sm row">
                <label class="col-sm-4 col-form-label-sm">Jenis Tanah <span class="text-danger">*</span></label>
                <div class="col-sm-8">
                    <select class="form-control form-control-sm" name="t_jenis_tanah_value" id="t_jenis_tanah_value">
                        <option value="">Silahkan Pilih</option>
                        @foreach ($lookupItem1 as $item)
                            <option value="{{ $item->kd_lookup_item }}"
                                {{ (!isset($objek_pajak) ? '' : (int) $objek_pajak->t_jenis_tanah == (int) $item->kd_lookup_item) ? 'selected' : '' }}
                                readonly>
                                {{ $item->nm_lookup_item }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group-sm row" hidden>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-sm" name="t_jenis_tanah" id="t_jenis_tanah"
                        value="{{ $objekPajak->t_jenis_tanah ?? '' }}" readonly>
                </div>
            </div>
        @endif
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Luas Tanah
                (M<sup>2</sup>)</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_luas_tanah" name="t_luas_tanah"
                    value="{{ $pelayanan->Op->t_luas_tanah ?? '' }}" onkeypress="return onlyNumberKey(event)"
                    @if ($jenisLayanan['s_id_jenis_pelayanan'] != 2) readonly @endif>
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Luas Bangunan
                (M<sup>2</sup>)</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_luas_bangunan" name="t_luas_bangunan"
                    value="{{ $pelayanan->Op->t_luas_bangunan ?? '' }}" readonly="">
            </div>
        </div>
    </div>
    <div class="col-md-6" id="div-wpbaru">
        <hr style="background-color: #28a745; height: 2px;">
        <p style="font-weight: bold">DATA WAJIB PAJAK (BARU)</p>
        <hr>
        <button type="button" class="btn btn-primary btn-sm" onclick="copyPemohon('div-wpbaru')">copy data
            pemohon</button>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">NIK <span class="text-danger">*</span></label>
            <div class="col-sm-8">
                <input type="hidden" id="t_id_wp" name="t_id_wp" value="{{ $pelayanan->Wp->t_id_wp ?? '' }}">
                <input type="text" class="form-control form-control-sm masked-nik" id="t_nik_wp" name="t_nik_wp"
                    maxlength="16" value="{{ $pelayanan->Wp->t_nik_wp ?? '' }}" required>
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Nama <span class="text-danger">*</span></label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_nama_wp" name="t_nama_wp"
                    value="{{ $pelayanan->Wp->t_nama_wp ?? '' }}" required>
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">RT <span class="text-danger">*</span></label>
            <div class="col-sm-2">
                <input type="text" class="form-control form-control-sm masked-number-3" id="t_rt_wp"
                    name="t_rt_wp" value="{{ $pelayanan->Wp->t_rt_wp ?? '' }}"
                    onkeypress="return onlyNumberKey(event)" required>
            </div>
            <label class="col-sm-2 col-form-label-sm">RW <span class="text-danger">*</span></label>
            <div class="col-sm-2">
                <input type="text" class="form-control form-control-sm masked-number-2" id="t_rw_wp"
                    name="t_rw_wp" value="{{ $pelayanan->Wp->t_rw_wp ?? '' }}"
                    onkeypress="return onlyNumberKey(event)" required>
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Jalan <span class="text-danger">*</span></label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_jalan_wp" name="t_jalan_wp"
                    value="{{ $pelayanan->Wp->t_jalan_wp ?? '' }}" required>
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Blok/No/Kav</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_blok_kav_wp" name="t_blok_kav_wp"
                    value="{{ $pelayanan->Wp->t_blok_kav_wp ?? '' }}">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa <span class="text-danger">*</span></label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_kelurahan_wp" name="t_kelurahan_wp"
                    value="{{ $pelayanan->Wp->t_kelurahan_wp ?? '' }}" required>
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Kecamatan <span class="text-danger">*</span></label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_kecamatan_wp" name="t_kecamatan_wp"
                    value="{{ $pelayanan->Wp->t_kecamatan_wp ?? '' }}" required>
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Kabupaten/Kota <span class="text-danger">*</span></label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_kabupaten_wp" name="t_kabupaten_wp"
                    value="{{ $pelayanan->Wp->t_kabupaten_wp ?? '' }}" required>
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">No. Telp<span class="text-danger">*</span></label>
            <div class="col-sm-8">
                <input type="text" required class="form-control form-control-sm masked-hp" id="t_no_hp_wp"
                    name="t_no_hp_wp" maxlength="15"
                    value="{{ old('t_no_hp_wp') ?? ($wajib_pajak->t_no_hp_wp ?? '') }}">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">NPWPD</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm masked-npwpd" id="t_npwpd"
                    name="t_npwpd" value="{{ old('t_npwpd') ?? ($wajib_pajak->t_npwpd ?? '') }}">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Email</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_email" name="t_email"
                    value="{{ old('t_email') ?? ($wajib_pajak->t_email ?? '') }}">
            </div>
        </div>

    </div>

</div>
