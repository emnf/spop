<h4 class="mt-4 mb-2">Data Pelayanan</h4>
<div class="card card-danger card-outline" style="border-top:3px solid #dc3545;">
    <div class="card-body">
        <div class="col-md-12">
            <p style="font-weight: bold">Jenis Pelayanan : <span>{{ $jenisLayanan['s_nama_jenis_pelayanan'] }}</span>
            </p>
            <hr>
            @if(!in_array($jenisLayanan['s_id_jenis_pelayanan'], [1, 5, 6, 7, 8, 9]))
                <div class="form-group-sm row">
                    <label class="col-sm-3 col-form-label-sm">Tahun Pajak</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm" id="t_tahun_pajak" name="t_tahun_pajak"
                            value="{{ $pelayanan->t_tahun_pajak ?? date('Y') }}"
                            @if($jenisLayanan['s_id_jenis_pelayanan'] != 3) readonly @endif>
                    </div>
                </div>
            @endif
            @if($jenisLayanan['s_id_jenis_pelayanan'] == 6)
                <div class="form-group-sm row">
                    <label class="col-sm-3 col-form-label-sm">Tahun Pajak</label>
                    <div class="col-sm-8">
                        <input type="hidden" class="form-control form-control-sm" id="t_tahun_pajak" name="t_tahun_pajak"
                            value="{{ $pelayanan->t_tahun_pajak ?? date('Y') }}"
                            readonly>
                        <input type="hidden" id="noUrut" name="noUrut" value="" readonly>
                        <div class="form-group">
                        <div id="tahunPajakBox">
                        </div>
                        </div>
                    </div>
                </div>
            @endif

            @if($jenisLayanan['s_id_jenis_pelayanan'] == 9)
                <div class="form-group-sm row">
                    <label class="col-sm-3 col-form-label-sm">Tgl. Permintaan Jatuh Tempo</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm datepicker-date"
                            id="t_tgl_permintaan" name="t_tgl_permintaan"
                            value="{{ !isset($pelayanan) ? date('d-m-Y') : date('d-m-Y', strtotime($pelayanan->t_tgl_permintaan)) }}" readonly>
                    </div>
                </div>
            @endif

            <div class="form-group-sm row">
                <input type="hidden" id="noUrut" name="noUrut" value="" readonly>
                <label class="col-sm-3 col-form-label-sm">Tanggal Permohonan</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control form-control-sm" id="t_tgl_pelayanan" name="t_tgl_pelayanan"
                        value="{{ !isset($pelayanan) ? '' : isset($pelayanan->t_tgl_pelayanan) ? date('d-m-Y', strtotime($pelayanan->t_tgl_pelayanan)) : date('d-m-Y') }}"
                        readonly>
                </div>
            </div>
            @if($jenisLayanan['s_id_jenis_pelayanan'] == 8)
                <div class="form-group-sm row">
                    <label class="col-sm-3 col-form-label-sm">Besarnya Pengurangan (%) <span class="text-danger">*</span></label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm masked-number-3" id="t_besar_pengurangan" name="t_besar_pengurangan"
                            value="{{ old('t_besar_pengurangan') ?? $pelayanan->t_besar_pengurangan ?? '' }}" required>
                    </div>
                    <div class="col-sm-8">
                        <span style="color: red; font-weight:bold; font-size:11px">*isilah besaran presentase pengurangan sesuai yang anda inginkan. maksimal pengurangan adalah 100%.</span>
                    </div>
                </div>
            @endif

            <div class="form-group-sm row">
                <label class="col-sm-3 col-form-label-sm">Keterangan<span class="text-danger">*</span></label>
                <div class="col-sm-8">
                    <textarea class="form-control" id="t_keterangan" name="t_keterangan">{{ old('t_keterangan') ?? $pelayanan->t_keterangan ?? '' }}</textarea>
                </div>
            </div>
        </div>

    </div>

    <div class="card-footer">
        <button type="button" id="btnSimpanDraft" class="btn btn-primary float-left swalDefaultSimpan"
            data-toggle="modal" data-target="#modal-save" {{ ($jenisLayanan->s_lunas_pbb == 'true') ? 'disabled = disabled' : '' }}><span class="fas fa-save"></span>
            Simpan Sebagai Draft</button>
        <div class="modal fade" id="modal-save">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h6 class="modal-title"><span class="fas fa-save"></span> &nbsp;SIMPAN
                            PERMOHONAN ?</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Apakah anda ingin menyimpan data permohonan tersebut?</p>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span
                                class="fas fa-times-circle"></span>
                            &nbsp;TUTUP
                        </button>
                        <button type="submit" class="btn btn-success btn-sm"><span class="fas fa-save"></span>
                            &nbsp;YA
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <button type="button" class="btn btn-danger float-right" data-toggle="modal" data-target="#modal-cancel">
            <span class="fas fa-times"></span> &nbsp;BATAL
        </button>
        <div class="modal fade" id="modal-cancel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h6 class="modal-title"><span class="fas fa-exclamation"></span> &nbsp;BATALKAN
                            PERMOHONAN ?</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Apakah anda yakin ingin membatalkan data permohonan tersebut?</p>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span
                                class="fas fa-times-circle"></span>
                            &nbsp;TUTUP
                        </button>
                        <a href="{{ url('pelayanan/jenis-layanan') .'/'. $jenisPajak->s_id_jenis_pajak }}"
                            class="btn btn-success btn-sm"><span class="fas fa-exclamation"></span>
                            &nbsp;YA</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>