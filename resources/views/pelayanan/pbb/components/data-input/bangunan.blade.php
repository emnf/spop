<div class="row">
    <div class="col-md-6" id="rinciLuasBangunan">
        <hr style="background-color: #28a745; height: 2px;">
        <p style="font-weight: bold">RINCIAN LUAS BANGUNAN</p>
        <hr>
        <div id="luasBangunan">
            @if(isset($pelayanan->detailBangunan))
                @foreach ($pelayanan->detailBangunan as $index => $item)
                <div className="form-group-sm row">
                    <input type="hidden" name="idDetailBangunans[{{ $index }}]" value="{{ $item->t_id_detail_bangunan }}">
                    <input type="hidden" value="{{ $item->t_no_urut_bangunan }}" name="nomorBangunans[{{ $index }}]">
                    <label className="col-sm-4 col-form-label-sm">Bangunan Ke-{{ $item->t_no_urut_bangunan }}</label>
                    <div className="col-sm-4 ">
                        <input type="text" class="form-control form-control-sm" value="{{ $item->t_luas }}"
                            name="luasOpBangunan[{{ $index }}]" onChange="hitungLuasBangunan()"
                            onkeypress="return onlyNumberKey(event)" onBlur="hitungLuasBangunan()">
                    </div>
                </div>
                @endforeach
            @endif
        </div>
    </div>
</div>

@push('scripts')
    <script type="text/javascript">
        if($("#t_luas_bangunan").val() > 0 ){
            $("#opBanguanan2").show();
        }
        function getOpBangunan() {
            $.get("/data-objek-pajak/bangunan/" + $("#nop").val())
                .then(function(res) {
                    let aa = res;
                    let arr = [];
                    aa.forEach((entry, index) => {
                        arr.push(
                            '<div className="form-group-sm row">' +
                            '<input type="hidden" value="' + entry.noBng + '" name="nomorBangunans[' + index + ']">' +
                            '<label className="col-sm-4 col-form-label-sm">Bangunan Ke-' + entry.noBng + '</label>' +
                            '<div className="col-sm-4 ">' +
                            '<input type="text" class="form-control form-control-sm"' +
                            'value="' + entry.luasBng + '" name="luasOpBangunan[' + index + ']" onChange="hitungLuasBangunan()" onBlur="hitungLuasBangunan()">' +
                            '</div></div>');

                    });
                    $("#opBanguanan").html(arr);
                });
        }

        function hitungLuasBangunan() {
            let total = 0;
            let values = $("input[name^='luasOpBangunan']")
                .map(function() {
                    return $(this).val();
                }).get();

            values.forEach((arr) => {
                total += parseFloat(arr);
            })

            $("#luasBangunan").val(total);
            $("#t_luas_bangunan").val(total);
        }
    </script>
@endpush
