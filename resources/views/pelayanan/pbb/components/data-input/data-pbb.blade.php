<h4 class="mt-4 mb-2">Data PBB</h4>
<div class="card card-danger card-outline" style="border-top:3px solid #dc3545;">
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group-sm row">
                    <label class="col-sm-2 col-form-label-sm">NOP</label>
                    <div class="col-sm-3">
                        <input type="hidden" class="form-control form-control-sm masked-nop" id="t_id_wp"
                            name="t_id_wp" placeholder="" required
                            value="{{ $pelayanan->WpLama->t_id_wp_lama ?? ($pelayanan->wajibPajak->t_id_wp ?? '') }}">
                        <input type="hidden" class="form-control form-control-sm masked-nop" id="t_id_op"
                            name="t_id_op" placeholder="" required
                            value="{{ $pelayanan->OpLama->t_id_op_lama ?? ($pelayanan->objekPajak->t_id_op ?? '') }}">
                        <input type="text" class="form-control form-control-sm masked-nop" id="t_nop"
                            name="t_nop" placeholder="" required value="{{ $pelayanan->t_nop ?? '' }}">
                    </div>
                    <label class="col-sm-1 col-form-label-sm">Tahun Pajak</label>
                    <div class="col-sm-1">
                        <input type="text" class="form-control form-control-sm masked-number-4 datasppt"
                            id="t_tahun_nop" name="t_tahun_nop" value="{{ $pelayanan->t_tahun_pajak ?? '' }}">
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="btn btn-sm btn-primary" id="searchNop"><span
                                class="fas fa-search"></span>
                            CARI</button>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="1" id="fasumCheckbox"
                                name="fasumCheckbox">
                            <label class="form-check-label" for="fasumCheckbox">
                                Fasum
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <hr style="background-color: #dc3545; height: 2px;">
                <p style="font-weight: bold">DATA OBJEK PAJAK (Lama)</p>
                <hr>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">RT<span class="float-right">:</span></label>
                    <div class="col-sm-2">
                        <div class="datasppt" id="t_rt_op_lama" name="t_rt_op_lama"></div>
                    </div>
                    <label class="col-sm-2 col-form-label-sm">RW<span class="float-right">:</span></label>
                    <div class="col-sm-2">
                        <div class="datasppt" id="t_rw_op_lama" name="t_rw_op_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Jalan<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <div class="datasppt" id="t_jalan_op_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Blok/No/Kav<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <div class="datasppt" id="kd_blok_kav_lama" name="kd_blok_kav_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <div class="datasppt" id="t_kelurahan_op_lama" name="t_kelurahan_op_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kecamatan<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <div class="datasppt" id="t_kecamatan_op_lama" name="t_kecamatan_op_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Luas Tanah
                        (M<sup>2</sup>)<span class="float-right">:</span></label>
                    <div class="col-sm-2">
                        <div class="datasppt" id="t_luas_tanah_lama"></div>
                    </div>
                    <label class="col-sm-3 col-form-label-sm">Luas Bng. (M<sup>2</sup>)<span
                            class="float-right">:</span></label>
                    <div class="col-sm-3">
                        <div class="datasppt" id="t_luas_bangunan_lama" name="t_luas_bangunan_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Pajak<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <div class="datasppt" id="t_pajak_pbb" name="t_pajak_pbb"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <hr style="background-color: #dc3545; height: 2px;">
                <p style="font-weight: bold">DATA WAJIB PAJAK (Lama)</p>
                <hr>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">NIK<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <div class="datasppt" id="t_nik_wp_lama" nama="t_nik_wp_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Nama<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <div class="datasppt" id="t_nama_wp_lama" name="t_nama_wp_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">RT<span class="float-right">:</span></label>
                    <div class="col-sm-2">
                        <div class="datasppt" id="t_rt_wp_lama" name="t_rt_wp_lama"></div>
                    </div>
                    <label class="col-sm-2 col-form-label-sm">RW<span class="float-right">:</span></label>
                    <div class="col-sm-2">
                        <div class="datasppt" id="t_rw_wp_lama" name="t_rw_wp_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Jalan<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <div class="datasppt" id="t_jalan_wp_lama" name="t_jalan_wp_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <div class="datasppt" id="t_kelurahan_wp_lama" name="t_kelurahan_wp_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kecamatan<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <div class="datasppt" id="t_kecamatan_wp_lama" name="t_kecamatan_wp_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kabupaten/Kota<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <div class="datasppt" id="t_kabupaten_wp_lama" name="t_kabupaten_wp_lama"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<div class="modal fade" id="modal-not-found-nop">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title">Peringatan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">NOP<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <span id="nopModal"></span>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Tahun Pajak<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <span id="tahunModal"></span>
                    </div>
                </div>
                <p>Data tersebut tidak ditemukan.</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-payment">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title">Peringatan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">NOP<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <span id="nopModalpay"></span>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Tahun Pajak<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <span id="tahunModalpay"></span>
                    </div>
                </div>
                <p>NOP tersebut Sudah Lunas Pembayaran.</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ModalTunggakanPBB" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title">Daftar Tunggakan
                    PBB{{ $jenisLayanan->s_lunas_pbb == true ? ' Yang Wajib Di Lunasi.' : '.' }}</h4>
                <input type="hidden" id="lunasPbb" name="lunasPbb" value="{{ $jenisLayanan->s_lunas_pbb }}">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="datatunggakan"></div>
            </div>
            <div class="modal-footer right">
                <button type="button" class="btn btn-outline-light" data-dismiss="modal"
                    onclick="tutupmodaltunggakan()">Tutup</button>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script type="text/javascript">
        $(document).ready(() => {
            // if ($('#t_id_pelayanan').val() != '') getDataSppt();
            if ($('#t_id_pelayanan').val() != '') {
                getTunggakanSppt();
                getDataSppt();
            }

            if ($('#t_id_pelayanan').val() == '') {
                setDati()
            }
        });

        function getToday() {
            var d = new Date();
            var strDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
            return strDate;
        }

        function setDati() {
            let dati = '{{ substr($kdKabkota, 0, 2) . '.' . substr($kdKabkota, 2) }}';
            $('#t_nop').val(dati)
        }

        function getDataSppt() {
            @if (request()->segment(2) == 'edit')
                let currentUrl = "/pelayanan/{{ request()->segment(2) }}/" + $('#t_id_jenis_pelayanan').val() +
                    "/get-data-sppt"
            @else
                let currentUrl = "/pelayanan/{{ request()->segment(2) }}/" + $('#t_id_jenis_pajak').val() + "/" + $(
                    '#t_id_jenis_pelayanan').val() + "/get-data-sppt"
            @endif

            $.ajax({
                url: currentUrl,
                type: 'GET',
                data: {
                    nop: $('#t_nop').val(),
                    tahunPajak: $('#t_tahun_nop').val()
                }
            }).then(function(response) {
                var aa = response;
                const bb = document.querySelectorAll('.datasppt');

                bb.forEach(element => {
                    $(`#${element.id}`).html('');
                });

                if (aa.length < 1) {
                    $('#nopModal').html($('#t_nop').val());
                    $('#tahunModal').html($("#t_tahun_nop").val());
                    $('#modal-not-found-nop').modal('show');
                } else {
                    $("#t_rt_op_lama").html(aa.rt_op);
                    $("#t_rw_op_lama").html(aa.rw_op);
                    $("#t_jalan_op_lama").html(aa.jalan_op);
                    $("#kd_blok_lama").html(aa.kd_blok);
                    $("#t_kecamatan_op_lama").html(aa.nm_kecamatan);
                    $("#t_kelurahan_op_lama").html(aa.nm_kelurahan);
                    $("#t_luas_tanah_lama").html(aa.total_luas_bumi);
                    $("#t_luas_bangunan_lama").html(aa.total_luas_bng);
                    $("#no_urut_lama").html(aa.no_urut);
                    $("#kd_jns_op_lama").html(aa.kd_jns_op);
                    var fasumCheckbox = document.getElementById("fasumCheckbox");
                    if (aa.kd_jns_op == 4) {
                        fasumCheckbox.checked = true;
                    } else {
                        fasumCheckbox.checked = false;
                    }
                    $("#t_nik_wp_lama").html(aa.subjek_pajak_id);
                    $("#t_nama_wp_lama").html(aa.nm_wp);
                    $("#t_rt_wp_lama").html(aa.rt_wp);
                    $("#t_rw_wp_lama").html(aa.rw_wp);
                    $("#t_jalan_wp_lama").html(aa.jalan_wp);
                    $("#t_kelurahan_wp_lama").html(aa.kelurahan_wp);
                    $("#t_kecamatan_wp_lama").html(aa.kecamatan_wp);
                    $("#t_kabupaten_wp_lama").html(aa.kota_wp);
                    $("#t_no_hp_wp_lama").html(aa.telp_wp);

                    $("#t_pajak_pbb").html(aa.pbb_yg_harus_dibayar_sppt);

                    $("#t_tahun_pajak").html(aa.thn_pajak_sppt);
                    $("#t_tgl_pelayanan").val(getToday());
                }
            });
        }

        function getTunggakanSppt() {
            let currentUrl = "/pelayanan/cektunggakanpbb"

            $.ajax({
                url: currentUrl,
                type: 'POST',
                data: {
                    nop: $('#t_nop').val()
                }
            }).then(function(response) {
                var aa = response;

                if (aa.PBB_YG_HARUS_DIBAYAR_SPPT != '' && aa.datatunggakan != 3) {
                    jQuery('#ModalTunggakanPBB').modal('show');
                    $('#datatunggakan').html(aa.datatunggakan);
                    if ($("#lunasPbb").val() == 1) {
                        $('#btnSimpanDraft').attr('disabled', 'disabled');
                        // getDataSppt();
                    } else {
                        // getDataSppt();
                    }
                } else if (aa.PBB_YG_HARUS_DIBAYAR_SPPT == '' && aa.datatunggakan != 3) {
                    alert('Tidak Ada Tunggakan');
                    // getDataSppt();
                    $("#btnSimpanDraft").removeAttr('disabled');
                }
            });
        }

        function tutupmodaltunggakan() {
            jQuery('#ModalTunggakanPBB').modal('hide');
        }

        $('#searchNop').click(() => [getDataSppt(), getTunggakanSppt()]);
    </script>
@endpush
