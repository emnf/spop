@push('styles')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css"
        integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="" />
@endpush
<div class="row">
    <div class="col-md-12"><b> LETAK OBJEK</b></div>
    <div class="col-md-12">
        <hr style="height:2px;color:red">
    </div>
    <div class="col-md-12 form-horizontal">
        <div class="form-group-sm row">
            <label class="col-sm-2 col-form-label-sm">Latitude <span class="text-danger">*</span>
            </label>
            <div class="col-sm-4">
                <input type="text" class="form-control form-control-sm " id="t_latitude" name="t_latitude"
                    value="{{ old('t_latitude') ?? ($objek_pajak->t_latitude ?? '') }}">
            </div>
            <div class="colsm-1"></div>
            <label class="col-sm-2 col-form-label-sm">Longitude <span class="text-danger">*</span>
            </label>
            <div class="col-sm-4">
                <input type="text" class="form-control form-control-sm " id="t_longitude" name="t_longitude"
                    value="{{ old('t_longitude') ?? ($objek_pajak->t_longitude ?? '') }}">
            </div>
        </div>
    </div>
    <div class=" col-md-12">
        {{-- <center>
            <button type="button" onclick="initMap()" class="btn btn-primary">Lokasi
                Pilih</button>
            <button type="button" onclick="initMap2()" class="btn btn-primary">Lokasi
                Saya</button>
        </center> --}}
    </div>
    <div class="col-md-12">
        <center>
            <div align="center" id="map" style="width: 100%; height: 400px"></div>
        </center>
    </div>
</div>

@push('scripts')
    {{-- <script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyq1l-IUYHhDWoWwF4HqTTVw-ixal-JzQ&callback=initMap&libraries=&v=weekly"
    async defer></script>
<script>
    function initMap() {
        var latitude = $("#t_latitude").val() * 1;
        var longitude = $("#t_longitude").val() * 1;

        // Create a map object and specify the DOM element for display.
        if (latitude <= 0 && longitude <= 0) {
            var latlng = {
                lat: <?= $pemda->s_latitude ?>,
                lng: <?= $pemda->s_longitude ?>
            };
        } else {
            var latlng = {
                lat: latitude,
                lng: longitude
            };
        }

        var map = new google.maps.Map(document.getElementById('map'), {
            center: latlng,
            scrollwheel: false,
            zoom: 15,
        });
        // buat tampungan marker
        let marker = '';

        // baca kalo ada klik di map
        google.maps.event.addListener(map, 'click', function (e) {
            var location = e.latLng;

            // kalo ada markernya hapus
            if (marker != '') {
                marker.setMap(null);
            }

            // isi makerek dengan titik baru
            marker = new google.maps.Marker({
                position: location,
                map: map,
                draggable: true
            });

            $("#t_longitude").val(location.lng().toFixed(4));
            $("#t_latitude").val(location.lat().toFixed(4));

            // hiasan
            google.maps.event.addListener(marker, "click", function (e) {
                var infoWindow = new google.maps.InfoWindow({
                    content: 'Lokasi Objek Pajak'
                });
                infoWindow.open(map, marker);
            });

            // update form latlang kalo marker di geser
            google.maps.event.addListener(marker, 'dragend', function (a) {
                $("#t_longitude").val(a.latLng.lng().toFixed(4));
                $("#t_latitude").val(a.latLng.lat().toFixed(4));
            });
        })
        google.maps.event.trigger(map, "resize");
    }

    function initMap2() {
        var latitude = $("#latitude").val() * 1;
        var longitude = $("#longitude").val() * 1;
        // Create a map object and specify the DOM element for display.
        if (isNaN(latitude) && isNaN(longitude)) {
            var latlng = {
                lat: -7.866666599999999,
                lng: 110.1791475
            };
        } else {
            var latlng = {
                lat: latitude,
                lng: longitude
            };
        }
        var map = new google.maps.Map(document.getElementById('map'), {
            center: latlng,
            scrollwheel: false,
            zoom: 17,
        });

         // buat tampungan marker
         let marker = '';

        // baca kalo ada klik di map
        google.maps.event.addListener(map, 'click', function (e) {
            var location = e.latLng;

            // kalo ada markernya hapus
            if (marker != '') {
                marker.setMap(null);
            }

            // isi makerek dengan titik baru
            marker = new google.maps.Marker({
                position: location,
                map: map,
                draggable: true
            });

            $("#t_longitude").val(location.lng().toFixed(4));
            $("#t_latitude").val(location.lat().toFixed(4));

            // hiasan
            google.maps.event.addListener(marker, "click", function (e) {
                var infoWindow = new google.maps.InfoWindow({
                    content: 'Lokasi Objek Pajak'
                });
                infoWindow.open(map, marker);
            });

            // update form latlang kalo marker di geser
            google.maps.event.addListener(marker, 'dragend', function (a) {
                $("#t_longitude").val(a.latLng.lng().toFixed(4));
                $("#t_latitude").val(a.latLng.lat().toFixed(4));
            });
        })

        google.maps.event.trigger(map, "resize");
        var infoWindow = new google.maps.InfoWindow;

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                marker = new google.maps.Marker({
                    position: pos,
                    map: map,
                    draggable: true
                });
                console.log(marker);
                $("#t_latitude").val(position.coords.latitude.toFixed(4));
                $("#t_longitude").val(position.coords.longitude.toFixed(4));
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent('Lokasi Objek');
                    infoWindow.open(map, marker);
                });
                map.setCenter(pos);
            }, function (e) {
                handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
        }

    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
            'Error: The Geolocation service failed.' :
            'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
    }

</script> --}}
    <script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js"
        integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>
    <script>
        const map = new L.map('map').setView([-6.9611527, 111.4007443], 13);

        const tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        }).addTo(map);

        const marker = L.marker([-6.9611527, 111.4007443], {
            draggable: true
        }).addTo(map);

        marker.on('dragend', function(e) {
            document.getElementById('t_latitude').value = marker.getLatLng().lat;
            document.getElementById('t_longitude').value = marker.getLatLng().lng;
        });
    </script>
@endpush
