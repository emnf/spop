<address>
    <strong class="pr-1">{{ $pelayanan->t_nama_pemohon }}</strong>(<small>{{ $pelayanan->t_nik_pemohon ?? '-' }}</small>)<br>
    <span></span>{{ $pelayanan->t_jalan_pemohon }}</span>
    RT <span>{{ $pelayanan->t_rt_pemohon }}</span>/
    RW <span>{{ $pelayanan->t_rw_pemohon }}</span>,
    <span>{{ $pelayanan->t_kelurahan_pemohon }}</span>,
    <span>{{ $pelayanan->t_kecamatan_pemohon }}</span><br>
    <span>{{ $pelayanan->t_kabupaten_pemohon }}</span>,
    <span>{{ $pelayanan->t_kode_pos_pemohon ?? '-' }}</span><br>
    No. HP : <span>{{ $pelayanan->t_no_hp_pemohon ?? '-' }}</span><br>
    Email : <span>{{ $pelayanan->t_email_pemohon ?? '-' }}</span><br>
</address>
