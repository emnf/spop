<div class="card card-danger" style="border-top:3px solid #dc3545;">
    <div class="card-body">
        <div class="row">
            <div class="col-md-12"><b> LETAK OBJEK</b></div>
            <div class="col-md-12">
                <hr style="height:2px;color:red">
            </div>
            <div class="col-md-12 form-horizontal">
                <div class="form-group-sm row">
                    <label class="col-sm-2 col-form-label-sm">Latitude <span class="text-danger">*</span>
                    </label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm " id="latitude" name="latitude"
                            value="">
                    </div>
                    <div class="colsm-1"></div>
                    <label class="col-sm-2 col-form-label-sm">Longitude <span class="text-danger">*</span>
                    </label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm " id="longitude" name="longitude"
                            value="">
                    </div>
                </div>
            </div>

            <div class=" col-md-12">
                <center>
                    <button type="button" onclick="initMap()" class="btn btn-primary">Lokasi
                        Pilih</button>
                    <button type="button" onclick="initMap2()" class="btn btn-primary">Lokasi
                        Saya</button>
                </center>
            </div>
            <div class="col-md-12">
                <center>
                    <div align="center" id="map" style="width: 100%; height: 400px"></div>
                </center>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyq1l-IUYHhDWoWwF4HqTTVw-ixal-JzQ&callback=initMap&libraries=&v=weekly"
        async defer></script>
    <script>
        function initMap() {
            let latitude = $("#latitude").val() * 1;
            let longitude = $("#longitude").val() * 1;
            let latitudedef = {{ $pemda->s_latitude ?? -6.2949369 }} * 1;
            let longitudedef = {{ $pemda->s_longitude ?? 106.7137581 }} * 1;
            // Create a map object and specify the DOM element for display.
            if (latitude == '' && longitude == '') {
                var latlng = {
                    lat: latitudedef,
                    lng: longitudedef
                };
            } else {
                var latlng = {
                    lat: latitude,
                    lng: longitude
                };
            }
            var map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                scrollwheel: false,
                zoom: 17,
            });
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                draggable: true,
                title: "Lokasi Objek Pajak"
            });
            google.maps.event.addListener(marker, 'dragend', function(a) {
                $("#longitude").val(a.latLng.lng().toFixed(4));
                $("#latitude").val(a.latLng.lat().toFixed(4));
            });
            google.maps.event.trigger(map, "resize");
        }

        function initMap2() {
            var latitude = $("#latitude").val() * 1;
            var longitude = $("#longitude").val() * 1;
            // Create a map object and specify the DOM element for display.
            if (latitude == '' && longitude == '') {
                var latlng = {
                    lat: 4.306674,
                    lng: 136.0884759
                };
            } else {
                var latlng = {
                    lat: latitude,
                    lng: longitude
                };
            }
            var map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                scrollwheel: false,
                zoom: 17,
            });
            google.maps.event.trigger(map, "resize");
            var infoWindow = new google.maps.InfoWindow;

            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    $("#latitude").val(position.coords.latitude.toFixed(4));
                    $("#longitude").val(position.coords.longitude.toFixed(4));
                    infoWindow.setPosition(pos);
                    infoWindow.setContent('Location found.');
                    infoWindow.open(map);
                    map.setCenter(pos);
                }, function() {
                    handleLocationError(true, infoWindow, map.getCenter());
                });
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());
            }

        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
        }
    </script>
@endpush
