<h4 class="mt-0 mb-2">&nbsp;Data Pelayanan</h4>
<div class="card card-danger card-outline" style="border-top:3px solid #dc3545;">
    <div class="card-body">
        <div class="col-md-12">
            <p style="font-weight: bold">Jenis Pelayanan : <span>{{
                    $jenisLayanan->s_nama_jenis_pelayanan }}</span></p>
            <hr>
            <div class="form-group-sm row">
                <label class="col-sm-2 col-form-label-sm">Tanggal Permohonan</label>
                <div class="col-sm-2">
                    <input type="hidden" class="form-control form-control-sm" id="tahunPajak" name="tahunPajak"
                        value="{{ date('Y') }}" readonly>
                    <input type="text" class="form-control form-control-sm" id="tglPelayanan" name="tglPelayanan"
                        value="{{ date('d-m-Y') }}" readonly>
                </div>
            </div>
            <div class="form-group-sm row">
                <label class="col-sm-2 col-form-label-sm">Keterangan</label>
                <div class="col-sm-8">
                    <textarea class="form-control" id="keterangan" name="keterangan"
                        th:text="${(data.idPelayanan != null) ? data.keterangan : ''}"></textarea>
                </div>
            </div>
        </div>

    </div>

    <div class="card-footer">
        <button type="button" class="btn btn-primary float-left swalDefaultSimpan" data-toggle="modal"
            data-target="#modal-save"><span class="fas fa-save"></span>
            Simpan Sebagai
            Draft
        </button>
        <div class="modal fade" id="modal-save">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h6 class="modal-title"><span class="fas fa-save"></span> &nbsp;SIMPAN
                            PERMOHONAN ?</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Apakah anda ingin menyimpan data permohonan tersebut?</p>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span
                                class="fas fa-times-circle"></span>
                            &nbsp;TUTUP
                        </button>
                        <button type="submit" class="btn btn-success btn-sm"><span class="fas fa-save"></span> &nbsp;YA
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <button type="button" class="btn btn-danger float-right" data-toggle="modal" data-target="#modal-cancel">
            <span class="fas fa-times"></span> &nbsp;BATAL
        </button>
        <div class="modal fade" id="modal-cancel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h6 class="modal-title"><span class="fas fa-exclamation"></span>
                            &nbsp;BATALKAN PERMOHONAN ?</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Apakah anda yakin ingin membatalkan data permohonan tersebut?
                        </p>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span
                                class="fas fa-times-circle"></span>
                            &nbsp;TUTUP
                        </button>
                        <a th:href="@{/pelayanan}" class="btn btn-success btn-sm"><span
                                class="fas fa-exclamation"></span> &nbsp;YA</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
