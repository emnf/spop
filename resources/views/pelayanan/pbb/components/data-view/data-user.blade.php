<!DOCTYPE html>
<html lang="en"
      layout:decorate="~{layout/main}"
      xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout"
      xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<th:block data-th-fragment="data">
    <div class="col-12">
        <div class="card" style="border-top: 5px solid #bd2130;">
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <td colspan="5"><h5>Informasi Akun</h5></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td rowspan="2" style="text-align: center; vertical-align: middle; width: 80px;">
                                <img style="width: 80px; height: 80px;" th:src="@{/static/images/elayanan3.png}">
                            </td>
                            <td style="width: 10px;"><i class="fas fa-user"></i></td>
                            <td style="width: 100px;">Nama Akun</td>
                            <td style="width: 5px; text-align: center;">:</td>
                            <td th:text="${userInfo.username}"></td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-address-card"></i></td>
                            <td>Nama</td>
                            <td>:</td>
                            <td th:text="${userInfo.nama}"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</th:block>
</body>
</html>