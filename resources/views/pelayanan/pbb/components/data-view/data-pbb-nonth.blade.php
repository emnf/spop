<h4 class="mt-4 mb-2">Data PBB</h4>
<div class="card card-danger card-outline" style="border-top:3px solid #dc3545;">
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group-sm row">
                    <label class="col-sm-2 col-form-label-sm">NOP</label>
                    <div class="col-sm-3">
                        <input type="hidden" class="form-control form-control-sm masked-nop" id="t_id_wp" name="t_id_wp" placeholder="" required
                        value="{{ $pelayanan->WpLama->t_id_wp_lama ?? '' }}">
                        <input type="hidden" class="form-control form-control-sm masked-nop" id="t_id_op" name="t_id_op" placeholder="" required
                        value="{{ $pelayanan->OpLama->t_id_op_lama ?? '' }}">
                        <input type="text" class="form-control form-control-sm masked-nop" id="t_nop" name="t_nop" placeholder="" required
                        value="{{ $pelayanan->t_nop ?? '' }}">
                    </div>
                    {{-- <label class="col-sm-1 col-form-label-sm">Tahun Pajak</label>
                    <div class="col-sm-1">
                        <input type="text" class="form-control form-control-sm masked-number-4" id="t_tahun_nop" name="t_tahun_nop"
                        value="{{ $pelayanan->t_tahun_nop ?? '' }}">
                    </div> --}}
                    <div class="col-sm-1">
                        <button type="button" class="btn btn-sm btn-primary" id="searchNop"><span
                                class="fas fa-search"></span>
                            CARI</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <hr style="background-color: #dc3545; height: 2px;">
                <p style="font-weight: bold">DATA OBJEK PAJAK</p>
                <hr>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">RT<span
                            class="float-right">:</span></label>
                    <div class="col-sm-2">
                        <div class="datasppt" id="t_rt_op_lama" name="t_rt_op_lama"></div>
                    </div>
                    <label class="col-sm-2 col-form-label-sm">RW<span
                            class="float-right">:</span></label>
                    <div class="col-sm-2">
                        <div class="datasppt" id="t_rw_op_lama" name="t_rw_op_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Jalan<span
                            class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <div class="datasppt" id="t_jalan_op_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Blok/No/Kav<span
                            class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <div class="datasppt" id="kd_blok_lama" name="kd_blok_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa<span
                            class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <div class="datasppt" id="t_kelurahan_op_lama" name="t_kelurahan_op_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kecamatan<span
                            class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <div class="datasppt" id="t_kecamatan_op_lama" name="t_kecamatan_op_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Luas Tanah
                        (M<sup>2</sup>)<span class="float-right">:</span></label>
                    <div class="col-sm-2">
                        <div class="datasppt" id="t_luas_tanah_lama"></div>
                    </div>
                    <label class="col-sm-3 col-form-label-sm">Luas Bng. (M<sup>2</sup>)<span
                            class="float-right">:</span></label>
                    <div class="col-sm-3">
                        <div class="datasppt" id="t_luas_bangunan_lama" name="t_luas_bangunan_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Pajak<span
                            class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <div class="datasppt" id="t_pajak_pbb" name="t_pajak_pbb"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <hr style="background-color: #dc3545; height: 2px;">
                <p style="font-weight: bold">DATA WAJIB PAJAK</p>
                <hr>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">NIK<span
                            class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <div class="datasppt" id="t_nik_wp_lama" nama="t_nik_wp_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Nama<span
                            class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <div class="datasppt" id="t_nama_wp_lama" name="t_nama_wp_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">RT<span
                            class="float-right">:</span></label>
                    <div class="col-sm-2">
                        <div class="datasppt" id="t_rt_wp_lama" name="t_rt_wp_lama"></div>
                    </div>
                    <label class="col-sm-2 col-form-label-sm">RW<span
                            class="float-right">:</span></label>
                    <div class="col-sm-2">
                        <div class="datasppt" id="t_rw_wp_lama" name="t_rw_wp_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Jalan<span
                            class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <div class="datasppt" id="t_jalan_wp_lama" name="t_jalan_wp_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa<span
                            class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <div class="datasppt" id="t_kelurahan_wp_lama" name="t_kelurahan_wp_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kecamatan<span
                            class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <div class="datasppt" id="t_kecamatan_wp_lama"  name="t_kecamatan_wp_lama"></div>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kabupaten/Kota<span
                            class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <div class="datasppt" id="t_kabupaten_wp_lama" name="t_kabupaten_wp_lama"></div>
                    </div>
                </div>
            </div>
        </div>
     </div>

     <div class="modal fade" id="modal-not-found-nop">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title">Peringatan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group-sm row">
                        <label class="col-sm-4 col-form-label-sm">NOP<span class="float-right">:</span></label>
                        <div class="col-sm-8">
                            <span id="nopModal"></span>
                        </div>
                    </div>
                    <p>Data tersebut tidak ditemukan.</p>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    {{-- {{ dd($jenisLayanan) }} --}}
    <div class="modal fade" id="ModalTunggakanPBB" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <input type="hidden" id="lunasPbb" name="lunasPbb" value="{{ $jenisLayanan->s_lunas_pbb }}">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title">Daftar Tunggakan PBB</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="datatunggakan"></div>
                </div>
                <div class="modal-footer right">
                    <button type="button" class="btn btn-outline-light" data-dismiss="modal"
                        onclick="tutupmodaltunggakan()">Tutup</button>
                </div>
            </div>
        </div>
    </div>

</div>

@push('scripts')
<script type="text/javascript">
        $(document).ready(() => {
            if ($('#t_id_pelayanan').val() != '') {
                getDataSppt();
                pelayanan = @json($pelayanan);
                thnChecked = pelayanan.t_multiple_tahun.split(",")
            }

            setDati();
        });

        let pelayanan = ''
        let thnChecked = ''

        function setDati() {
            let dati = '{{ substr($kdKabkota, 0, 2) .'.'. substr($kdKabkota, 2) }}';
            $('#t_nop').val(dati)
        }

        function getToday() {
            var d = new Date();
            var strDate = d.getDate() + "-" + (d.getMonth()+1) + "-" + d.getFullYear();
            return strDate;
        }

        function getDataSppt() {
            @if(request()->segment(2) == 'edit')
                let currentUrl = "/pelayanan/{{ request()->segment(2) }}/"+ $('#t_id_jenis_pelayanan').val() +"/get-data-sppt"
            @else
                let currentUrl = "/pelayanan/{{ request()->segment(2) }}/"+ $('#t_id_jenis_pajak').val() +"/"+$('#t_id_jenis_pelayanan').val() +"/get-data-sppt"
            @endif

            $.ajax({
                url: currentUrl,
                type: 'GET',
                data: {
                    nop: $('#t_nop').val(),
                }
            }).then(function(response) {
                var aa = response;
                const bb = document.querySelectorAll('.datasppt');
                bb.forEach(element => {
                    $(`#${element.id}`).html('');
                });

                if (aa.length < 1) {
                    $('#nopModal').html($('#t_nop').val());
                    $('#modal-not-found-nop').modal('show');
                } else {
                    $("#t_rt_op_lama").html(aa[0].rt_op);
                    $("#t_rw_op_lama").html(aa[0].rw_op);
                    $("#t_jalan_op_lama").html(aa[0].jalan_op);
                    $("#kd_blok_lama").html(aa[0].blok_kav_no_op);
                    $("#t_kecamatan_op_lama").html(aa[0].nm_kecamatan);
                    $("#t_kelurahan_op_lama").html(aa[0].nm_kelurahan);
                    $("#t_luas_tanah_lama").html(aa[0].luas_bumi_sppt);
                    $("#t_luas_bangunan_lama").html(aa[0].luas_bng_sppt);
                    $("#no_urut_lama").html(aa[0].no_urut);
                    $("#kd_jns_op_lama").html(aa[0].kd_jns_op);

                    $("#t_nik_wp_lama").html(aa[0].subjek_pajak_id);
                    $("#t_nama_wp_lama").html(aa[0].nm_wp_sppt);
                    $("#t_rt_wp_lama").html(aa[0].rt_wp_sppt);
                    $("#t_rw_wp_lama").html(aa[0].rw_wp_sppt);
                    $("#t_jalan_wp_lama").html(aa[0].jln_wp_sppt);
                    $("#t_kelurahan_wp_lama").html(aa[0].kelurahan_wp_sppt);
                    $("#t_kecamatan_wp_lama").html(aa[0].kecamatan_wp);
                    $("#t_kabupaten_wp_lama").html(aa[0].kota_wp_sppt);
                    $("#t_no_hp_wp_lama").html(aa[0].telp_wp);

                    $("#t_pajak_pbb").html(aa[0].pbb_yg_harus_dibayar_sppt);

                    let tahunPajak = '';
                    aa.map(sppt => {
                        tahunPajak += `<div class="form-check"><input type="checkbox" id="tahunpajakBox" name="tahunpajakBox[]" value="${sppt.thn_pajak_sppt}" ${thnChecked.indexOf(sppt.thn_pajak_sppt) !== -1 ? 'checked' : ''}></input><label class="ml-2">${sppt.thn_pajak_sppt}</label><br></div>`;
                    })

                    $("#tahunPajakBox").html(tahunPajak);

                    $( "#t_tgl_pelayanan" ).val(getToday());
                }
            });
        }

        function getTunggakanSppt(){
        let currentUrl = "/pelayanan/cektunggakanpbb"
        
        $.ajax({
            url: currentUrl,
            type: 'POST',
            data: {
                nop: $('#t_nop').val()
            }
        }).then(function(response) {
            var aa = response;

            if (aa.PBB_YG_HARUS_DIBAYAR_SPPT != '' && aa.datatunggakan != 3) {
                jQuery('#ModalTunggakanPBB').modal('show');
                $('#datatunggakan').html(aa.datatunggakan);
                if($("#lunasPbb").val() == 1){
                    $('#btnSimpanDraft').attr('disabled','disabled');
                    getDataSppt();
                }else{
                    getDataSppt();
                }
            } else if(aa.PBB_YG_HARUS_DIBAYAR_SPPT == '' && aa.datatunggakan != 3){
                alert('Tidak Ada Tunggakan');
                getDataSppt();
                $("#btnSimpanDraft").removeAttr('disabled');
            } 
        });
    }

    function tutupmodaltunggakan() {
        jQuery('#ModalTunggakanPBB').modal('hide');
    }

        $('#searchNop').click(function() {
            getTunggakanSppt();
            // getDataSppt();
        });
</script>
@endpush
