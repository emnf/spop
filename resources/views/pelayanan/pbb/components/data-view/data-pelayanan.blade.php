<address>
    <div class="row">
        <div class="col-5">Tanggal Permohonan</div>
        <div class="col-1">:</div>
        <div class="col-6"><b>{{ $pelayanan->t_tgl_pelayanan }}</b></div>
    </div>
    <div class="row">
        <div class="col-5">No. Pelayanan</div>
        <div class="col-1">:</div>
        <div class="col-6"><a href="javascript:void(0)"><b>{{ $pelayanan->t_no_pelayanan }}</b></a></div>
    </div>
    <div class="row">
        <div class="col-5">Jenis Pajak</div>
        <div class="col-1">:</div>
        <div class="col-6"><b>{{ $jenisPajak->s_nama_jenis_pajak }}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Jenis Pelayanan</div>
        <div class="col-1">:</div>
        <div class="col-6"><b>{{ $jenisLayanan->s_nama_jenis_pelayanan }}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Tanggal Perkiraan Selesai</div>
        <div class="col-1">:</div>
        <div class="col-6">
            <b>{{ \Illuminate\Support\Carbon::parse($perkiraanSelesai['tglSelesai'])->format('d F Y H:i:s') }}</b>&nbsp;&nbsp;
            <a href="javascript:void(0)" class="btn btn-xs {{ $perkiraanSelesai['warna'] }}"><i
                    class="fas fa-clock"></i>&nbsp;&nbsp;<b>{{ $perkiraanSelesai['sisaHari'] }}</b>
            </a>
        </div>
    </div>
    @if(!is_null($pelayanan->t_nop))
    <div class="row">
        <div class="col-5">NOP</div>
        <div class="col-1">:</div>
        <div class="col-6"><b>{{ \App\Helpers\PelayananHelper::formatStringToNop($pelayanan->t_nop) }}</b></div>
        <input type="hidden" id="tahunPajakPelayanan" value="{{ $pelayanan->t_tahun_pajak }}">
    </div>
    @endif
    <div class="row">
        {{-- <input type="hidden" id="nopPelayanan" name="nopPelayanan" value="{{ $pelayanan->t_no_pelayanan }}"> --}}
        <div class="col-5">Keterangan</div>
        <div class="col-1">:</div>
        <div class="col-6"><b>{{ $pelayanan->t_keterangan }}</b></div>
    </div>
    @if(!is_null($pelayanan->t_besar_pengurangan))
    <div class="row">
        <div class="col-5">Besar Pengurangan Diajukan</div>
        <div class="col-1">:</div>
        <div class="col-6">
            <a href="javascript:void(0)" class="btn btn-xs btn-warning">
                <b> {{ $pelayanan->t_besar_pengurangan }} %</b>
            </a>
        </div>
    </div>
    @endif
</address>
