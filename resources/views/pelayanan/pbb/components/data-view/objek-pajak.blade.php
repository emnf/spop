<h5>Data Objek Pajak
    @php
        use App\Models\Pelayanan\Op;
        $opbaru = Op::where('t_id_pelayanan', $pelayanan['t_id_pelayanan'])->first();
    @endphp
    @if ($opbaru)
        @if ($opbaru->jns_bumi == 4)
            (FASUM)
        @endif
    @endif

</h5>
@foreach ($pelayanan->objekPajaks as $objekPajak)
    <hr>
    <address>
        <div class="row">
            <div class="col-5">NOP</div>
            <div class="col-1">:</div>
            <div class="col-6"><b
                    id="op_nomor">{{ empty($objekPajak->kd_propinsi) ? '-' : $objekPajak->nopFormated }}</b></div>
        </div>
        <div class="row">
            <div class="col-5">Jalan</div>
            <div class="col-1">:</div>
            <div class="col-6"><b id="op_jalan">{{ $objekPajak->t_jalan_op ?? '' }}</b></div>
        </div>
        <div class="row">
            <div class="col-5">Rt</div>
            <div class="col-1">:</div>
            <div class="col-6"><b id="op_rt">{{ $objekPajak->t_rt_op ?? '' }}</b></div>
        </div>
        <div class="row">
            <div class="col-5">Rw</div>
            <div class="col-1">:</div>
            <div class="col-6"><b id="op_rw">{{ $objekPajak->t_rw_op ?? '' }}</b></div>
        </div>
        <div class="row">
            <div class="col-5">Blok/No/Kav</div>
            <div class="col-1">:</div>
            <div class="col-6"><b id="op_blok">{{ $objekPajak->t_blok_kav ?? '-' }}</b></div>
        </div>
        <div class="row">
            <div class="col-5">Kelurahan/Desa</div>
            <div class="col-1">:</div>
            <div class="col-6"><b id="op_desa">{{ $objekPajak->t_kelurahan_op ?? '' }}</b></div>
        </div>
        <div class="row">
            <div class="col-5">Kecamatan</div>
            <div class="col-1">:</div>
            <div class="col-6"><b id="op_kecamatan">{{ $objekPajak->t_kecamatan_op ?? '' }}</b></div>
        </div>
        <div class="row">
            <div class="col-5">Luas Tanah (M<sup>2</sup>)</div>
            <div class="col-1">:</div>
            <div class="col-6"><b id="op_luas_tanah">{{ $objekPajak->t_luas_tanah ?? '' }}</b></div>
        </div>
        <div class="row">
            <div class="col-5">Luas Bangunan (M<sup>2</sup>)</div>
            <div class="col-1">:</div>
            <div class="col-6"><b id="op_luas_banguan">{{ $objekPajak->t_luas_bangunan ?? '-' }}</b></div>
        </div>
        <div class="row">
            <div class="col-5">Latitude</div>
            <div class="col-1">:</div>
            <div class="col-6"><b id="op_lat">{{ $objekPajak->t_latitude ?? '-' }}</b></div>
        </div>
        <div class="row">
            <div class="col-5">Longitude</div>
            <div class="col-1">:</div>
            <div class="col-6"><b id="op_long">{{ $objekPajak->t_longitude ?? '-' }}</b></div>
        </div>
    </address>
@endforeach
