<h5>Data Objek Pajak Lama</h5>
@foreach ($pelayanan->OpLamas as $OpLama)
<hr>
<address>
    <div class="row">
        <div class="col-5">NOP</div>
        <div class="col-1">:</div>
        <div class="col-6"><b>{{ App\Helpers\PelayananHelper::formatStringToNop($OpLama->nop) ?? ''}}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Jalan</div>
        <div class="col-1">:</div>
        <div class="col-6"><b>{{ $OpLama->t_jalan_op ?? ''}}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Rt</div>
        <div class="col-1">:</div>
        <div class="col-6"><b>{{ $OpLama->t_rt_op ?? ''}}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Rw</div>
        <div class="col-1">:</div>
        <div class="col-6"><b>{{ $OpLama->t_rw_op ?? ''}}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Blok/No/Kav</div>
        <div class="col-1">:</div>
        <div class="col-6"><b>-</b></div>
    </div>
    <div class="row">
        <div class="col-5">Kelurahan/Desa</div>
        <div class="col-1">:</div>
        <div class="col-6"><b>{{ $OpLama->t_kelurahan_op ?? ''}}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Kecamatan</div>
        <div class="col-1">:</div>
        <div class="col-6"><b>{{ $OpLama->t_kecamatan_op ?? ''}}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Luas Tanah (M<sup>2</sup>)</div>
        <div class="col-1">:</div>
        <div class="col-6"><b>{{ $OpLama->t_luas_tanah ?? ''}}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Luas Bangunan (M<sup>2</sup>)</div>
        <div class="col-1">:</div>
        <div class="col-6"><b>{{ $OpLama->t_luas_bangunan ?? ''}}</b></div>
    </div>
</address>
@endforeach
