<h5>Data Wajib Pajak Lama</h5>
@foreach ($pelayanan->WpLamas as $WpLama)
<hr>
<address>
    <div class="row">
        <div class="col-5">NIK</div>
        <div class="col-1">:</div>
        <div class="col-6"><b>{{ $WpLama->t_nik_wp ?? '' }}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Nama</div>
        <div class="col-1">:</div>
        <div class="col-6"><b>{{ $WpLama->t_nama_wp ?? '' }}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Jalan</div>
        <div class="col-1">:</div>
        <div class="col-6"><b>{{ $WpLama->t_jalan_wp ?? '' }}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Rt</div>
        <div class="col-1">:</div>
        <div class="col-6"><b>{{ $WpLama->t_rt_wp ?? '' }}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Rw</div>
        <div class="col-1">:</div>
        <div class="col-6"><b>{{ $WpLama->t_rw_wp ?? '' }}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Kelurahan/Desa</div>
        <div class="col-1">:</div>
        <div class="col-6"><b>{{ $WpLama->t_kelurahan_wp ?? '' }}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Kecamatan</div>
        <div class="col-1">:</div>
        <div class="col-6"><b>{{ $WpLama->t_kecamatan_wp ?? '' }}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Kabupaten/Kota</div>
        <div class="col-1">:</div>
        <div class="col-6"><b>{{ $WpLama->t_kabupaten_wp ?? '' }}</b></div>
    </div>
</address>
@endforeach
