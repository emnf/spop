<h5>Data Wajib Pajak</h5>
@foreach ($pelayanan->wajibPajaks as $wajibPajak)
<hr>
<address>
    <div class="row">
        <div class="col-5">NIK</div>
        <div class="col-1">:</div>
        <div class="col-6"><b id="wp_nik">{{ $wajibPajak->t_nik_wp ?? '-' }}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Nama</div>
        <div class="col-1">:</div>
        <div class="col-6"><b id="wp_nama">{{ $wajibPajak->t_nama_wp ?? '' }}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Jalan</div>
        <div class="col-1">:</div>
        <div class="col-6"><b id="wp_jalan">{{ $wajibPajak->t_jalan_wp ?? '' }}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Rt</div>
        <div class="col-1">:</div>
        <div class="col-6"><b id="wp_rt">{{ $wajibPajak->t_rt_wp ?? '' }}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Rw</div>
        <div class="col-1">:</div>
        <div class="col-6"><b id="wp_rw">{{ $wajibPajak->t_rw_wp ?? '' }}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Kelurahan/Desa</div>
        <div class="col-1">:</div>
        <div class="col-6"><b id="wp_desa">{{ $wajibPajak->t_kelurahan_wp ?? '' }}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Kecamatan</div>
        <div class="col-1">:</div>
        <div class="col-6"><b id="wp_kecamatan">{{ $wajibPajak->t_kecamatan_wp ?? '' }}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Kabupaten/Kota</div>
        <div class="col-1">:</div>
        <div class="col-6"><b id="wp_kab_kota">{{ $wajibPajak->t_kabupaten_wp ?? '' }}</b></div>
    </div>
    <div class="row">
        <div class="col-5">No. Telp</div>
        <div class="col-1">:</div>
        <div class="col-6"><b id="wp_telp">{{ $wajibPajak->t_no_hp_wp ?? '-' }}</b></div>
    </div>
    <div class="row">
        <div class="col-5">NPWPD</div>
        <div class="col-1">:</div>
        <div class="col-6"><b id="npwpd">{{ $wajibPajak->t_npwpd ?? '-' }}</b></div>
    </div>
    <div class="row">
        <div class="col-5">Email</div>
        <div class="col-1">:</div>
        <div class="col-6"><b id="wp_email">{{ $wajibPajak->t_email ?? '-' }}</b></div>
    </div>
</address>
@endforeach
