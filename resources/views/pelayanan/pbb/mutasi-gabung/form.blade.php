<h4 class="mt-4 mb-2">Data Nop Akan Digabung</h4>
<div class="card card-danger card-outline" style="border-top:3px solid #dc3545;">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <button type="button" onclick="tambahNopLama()" class="btn btn-sm btn-info">Tambah</button>
            </div>
        </div>
        <div class="row mt-4">
            <input type="hidden" id="counterNopLama" value="{{ isset($pelayanan) ? (count($pelayanan->OpLamas) +1) : 1 }}">
            <div class="col-sm-12" id="dataNopAkanGabung">
                @if(isset($pelayanan) && count($pelayanan->OpLamas) > 0 )
                    @foreach($pelayanan->OpLamas as $key => $opLama)
                    <div
                        id="divNopLama{{ $key+1 }}"
                        class="card card-danger card-outline" style="border-top:3px solid #dc3545;">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group-sm row">
                                        <label class="col-sm-2 col-form-label-sm">
                                            NOP
                                            <span class="text-danger">
                                                *
                                            </span>
                                        </label>
                                        <div class="col-sm-4">
                                            <input type="hidden" id="idOPLama{{ $key+1 }}" name="idOPLama[]"
                                                value="{{ $opLama->t_id_op_lama ?? '' }}">
                                            <input type="text" class="form-control form-control-sm masked-nop"
                                                id="nopLamaCari{{ $key+1 }}"
                                                value="{{ $opLama->nop ?? '' }}" name="nopLamaCari[]"
                                                placeholder="xx.xx.xxx.xxx.xxx.xxxx.x" required="" maxlength="24">
                                            <input type="hidden" id="nopLama{{ $key+1 }}"
                                                value="{{ $opLama->nop ?? ''}}" name="nopLama[]" required="">
                                        </div>
                                        <div class="col-sm-4">
                                            <button type="button" class="btn btn-sm btn-primary"
                                                onclick="cariNopLama('{{ $key+1 }}')">
                                                <span class="fas fa-search"></span>
                                                CARI
                                            </button> &nbsp;&nbsp;
                                            <button type="button" class="btn btn-sm btn-danger"
                                                onclick="hapusDivNopLama('{{ $key+1 }}')">Hapus</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <hr style="background-color: #dc3545; height: 2px;">
                                    <p style="font-weight: bold">DATA OBJEK PAJAK (LAMA)</p>
                                    <hr>
                                    <div class="form-group-sm row">
                                        <label class="col-sm-4 col-form-label-sm">RT</label>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control form-control-sm"
                                                id="rtOPLama{{ $key+1 }}" value="{{ $opLama->t_rt_op }}"
                                                name="rtOPLama" readonly="">
                                        </div>
                                        <label class="col-sm-2 col-form-label-sm">RW</label>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control form-control-sm"
                                                id="rwOPLama{{ $key+1 }}" value="{{  $opLama->t_rw_op }}"
                                                name="rwOPLama[]" readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group-sm row">
                                        <label class="col-sm-4 col-form-label-sm">Jalan</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control form-control-sm"
                                                id="jalanOPLama{{ $key+1 }}"
                                                value="{{  $opLama->t_jalan_op }}" name="jalanOPLama[]"
                                                readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group-sm row">
                                        <label class="col-sm-4 col-form-label-sm">Blok/No/Kav</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control form-control-sm"
                                                id="blokNoOPLama{{ $key+1 }}" name="blokNoOPLama[]" value="{{ $opLama->t_blok_kav }}" readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group-sm row">
                                        <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control form-control-sm"
                                                id="kelurahanOPLama{{ $key+1 }}"
                                                value="{{ $opLama->t_kelurahan_op }}" name="kelurahanOPLama[]"
                                                readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group-sm row">
                                        <label class="col-sm-4 col-form-label-sm">Kecamatan</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control form-control-sm"
                                                id="kecamatanOPLama{{ $key+1 }}"
                                                value="{{ $opLama->t_kecamatan_op }}" name="kecamatanOPLama[]"
                                                readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group-sm row">
                                        <label class="col-sm-4 col-form-label-sm">
                                            Luas Tanah
                                            (M
                                            <sup>2</sup>
                                            )
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control form-control-sm"
                                                id="luasTanahLama{{ $key+1 }}"
                                                value="{{ $opLama->t_luas_tanah }}" name="luasTanahLama[]"
                                                readonly="" required="">
                                        </div>
                                    </div>
                                    <div class="form-group-sm row">
                                        <label class="col-sm-4 col-form-label-sm">
                                            Luas Bangunan
                                            (M
                                            <sup>2</sup>
                                            )
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control form-control-sm"
                                                id="luasBangunanLama{{ $key+1 }}"
                                                value="{{ $opLama->t_luas_bangunan }}" name="luasBangunanLama[]"
                                                readonly="" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <hr style="background-color: #dc3545; height: 2px;">
                                    <p style="font-weight: bold">DATA WAJIB PAJAK (LAMA)</p>
                                    <hr>
                                    <div class="form-group-sm row">
                                        <label class="col-sm-4 col-form-label-sm">NIK</label>
                                        <div class="col-sm-8">
                                            <input type="hidden" id="idWPLama{{ $key+1 }}"
                                                value="{{ $pelayanan->wpLamas[$key]->t_id_wp_lama }}" name="idWPLama[]">
                                            <input type="text" class="form-control form-control-sm masked-nik"
                                                id="nikWPLama{{ $key+1 }}"
                                                value="{{ $pelayanan->wpLamas[$key]->t_nik_wp }}" name="nikWPLama[]" readonly=""
                                                required="">
                                        </div>
                                    </div>
                                    <div class="form-group-sm row">
                                        <label class="col-sm-4 col-form-label-sm">Nama</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control form-control-sm"
                                                id="namaWPLama{{ $key+1 }}"
                                                value="{{ $pelayanan->wpLamas[$key]->t_nama_wp }}" name="namaWPLama[]" readonly=""
                                                required="">
                                        </div>
                                    </div>
                                    <div class="form-group-sm row">
                                        <label class="col-sm-4 col-form-label-sm">RT</label>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control form-control-sm"
                                                id="rtWPLama{{ $key+1 }}" value="{{ $pelayanan->wpLamas[$key]->t_rt_wp }}"
                                                name="rtWPLama[]" readonly="">
                                        </div>
                                        <label class="col-sm-2 col-form-label-sm">RW</label>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control form-control-sm"
                                                id="rwWPLama{{ $key+1 }}" value="{{ $pelayanan->wpLamas[$key]->t_rw_wp }}"
                                                name="rwWPLama[]" readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group-sm row">
                                        <label class="col-sm-4 col-form-label-sm">Jalan</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control form-control-sm"
                                                id="jalanWPLama{{ $key+1 }}"
                                                value="{{ $pelayanan->wpLamas[$key]->t_jalan_wp }}" name="jalanWPLama[]"
                                                readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group-sm row">
                                        <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control form-control-sm"
                                                id="kelurahanWPLama{{ $key+1 }}"
                                                value="{{ $pelayanan->wpLamas[$key]->t_kelurahan_wp }}" name="kelurahanWPLama[]"
                                                readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group-sm row">
                                        <label class="col-sm-4 col-form-label-sm">Kecamatan</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control form-control-sm"
                                                id="kecamatanWPLama{{ $key+1 }}"
                                                value="{{ $pelayanan->wpLamas[$key]->t_kecamatan_wp }}" name="kecamatanWPLama[]"
                                                readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group-sm row">
                                        <label class="col-sm-4 col-form-label-sm">Kabupaten/Kota</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control form-control-sm"
                                                id="kabupatenWPLama{{ $key+1 }}"
                                                value="{{ $pelayanan->wpLamas[$key]->t_kabupaten_wp }}" name="kabupatenWPLama[]"
                                                readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>

<h4 class="mt-4 mb-2">Data Nop Tujuan</h4>
<div class="card card-danger card-outline" style="border-top:3px solid #dc3545;">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group-sm row">
                    <label class="col-sm-2 col-form-label-sm">
                        NOP
                        <span class="text-danger">*</span>
                    </label>
                    <div class="col-sm-4">
                        <input type="hidden" id="idOP" name="idOP" value="{{ old('idOP') ?? $pelayanan->objekPajak->t_id_op ?? '' }}">
                        <input type="text" class="form-control form-control-sm masked-nop" id="nopCari" name="nopCari"
                            value="{{ old('nopCari') ?? $pelayanan->objekPajak->nopFormated ?? '' }}"
                            required>
                        <input type="hidden" id="nop" name="nop"
                            value="{{ old('nop') ?? $pelayanan->objekPajak->nop ?? ''}}" required>
                    </div>
                    <div class="col-sm-2">
                        <button type="button" class="btn btn-sm btn-primary" onclick="cariNop()">
                            <span class="fas fa-search"></span>
                            CARI
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <hr style="background-color: #dc3545; height: 2px;">
                <p style="font-weight: bold">DATA OBJEK PAJAK (BARU)</p>
                <hr>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">RT</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm masked-number-3" id="rtOP" name="rtOP"
                            value="{{ old('rtOP') ?? $pelayanan->objekPajak->t_rt_op ?? '' }}" readonly="">
                    </div>
                    <label class="col-sm-2 col-form-label-sm">RW</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm masked-number-2" id="rwOP" name="rwOP"
                            value="{{ old('rwOP') ?? $pelayanan->objekPajak->t_rw_op ?? ''  }}" readonly="">
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Jalan</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="jalanOP" name="jalanOP"
                            value="{{ old('jalanOP') ?? $pelayanan->objekPajak->t_jalan_op ?? '' }}" readonly="">
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Blok/No/Kav</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="blokNoOP" name="blokNoOP"
                            value="{{ old('blokNoOP') ?? $pelayanan->objekPajak->t_blok_kav ?? '' }}" readonly="">
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="kelurahanOP" name="kelurahanOP"
                            value="{{ old('kelurahanOP') ?? $pelayanan->objekPajak->t_kelurahan_op ?? ''}}" readonly="">
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kecamatan</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="kecamatanOP" name="kecamatanOP"
                            value="{{ old('kecamatanOP') ?? $pelayanan->objekPajak->t_kecamatan_op ?? '' }}" readonly="">
                    </div>
                </div>

                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">
                        Jenis Tanah
                        <span class="text-danger">*</span>
                    </label>
                    <div class="col-sm-8">
                        <select class="form-control form-control-sm" name="jenisTanah" id="jenisTanah">
                            <option value="">Silahkan Pilih</option>
                            @foreach ($lookupItem1 as $item)
                                <option {{ !isset($pelayanan) ? '' : (int)$pelayanan->objekPajak->t_jenis_tanah == (int)$item->kd_lookup_item ? 'selected' : '' }} value="{{ $item->kd_lookup_item }}">{{ $item->nm_lookup_item }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">
                        Luas Tanah
                        (M
                        <sup>2</sup>
                        )
                    </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="luasTanah" name="luasTanah"
                            value="{{ old('luasTanah') ?? $pelayanan->objekPajak->t_luas_tanah ?? '' }}" required>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">
                        Luas Bangunan
                        (M
                        <sup>2</sup>
                        )
                    </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="luasBangunan" name="luasBangunan"
                            value="{{ old('luasBangunan') ?? $pelayanan->objekPajak->t_luas_bangunan ?? '' }}" required>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <hr style="background-color: #dc3545; height: 2px;">
                <p style="font-weight: bold">DATA WAJIB PAJAK (BARU)</p>
                <hr>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">NIK</label>
                    <div class="col-sm-8">
                        <input type="hidden" id="idWP" name="idWP"
                            value="{{ old('idWP') ?? $pelayanan->wajibPajak->t_id_wp ?? ''}}">
                        <input type="text" class="form-control form-control-sm masked-nik" id="nikWP" name="nikWP"
                            value="{{ old('nikWP') ?? $pelayanan->wajibPajak->t_nik_wp ?? ''}}" required>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Nama</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="namaWP" name="namaWP"
                            value="{{ old('namaWP') ?? $pelayanan->wajibPajak->t_nama_wp ?? '' }}" required>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">RT</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm masked-number-3" id="rtWP" name="rtWP"
                            value="{{ old('rtWP') ?? $pelayanan->wajibPajak->t_rt_wp ?? '' }}">
                    </div>
                    <label class="col-sm-2 col-form-label-sm">RW</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm masked-number-2" id="rwWP" name="rwWP"
                            value="{{ old('rwWP') ?? $pelayanan->wajibPajak->t_rw_wp ?? ''}}">
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Jalan</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="jalanWP" name="jalanWP"
                            value="{{ old('jalanWP') ?? $pelayanan->wajibPajak->t_jalan_wp ?? '' }}">
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="kelurahanWP" name="kelurahanWP"
                            value="{{ old('kelurahanWP') ?? $pelayanan->wajibPajak->t_kelurahan_wp ?? '' }}">
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kecamatan</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="kecamatanWP" name="kecamatanWP"
                            value="{{ old('kecamatanWP') ?? $pelayanan->wajibPajak->t_kecamatan_wp ?? '' }}">
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kabupaten/Kota</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="kabupatenWP" name="kabupatenWP"
                            value="{{ old('kabupatenWP') ?? $pelayanan->wajibPajak->t_kabupaten_wp ?? ''}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ModalTunggakanPBB" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title">Daftar Tunggakan PBB {{ ($jenisLayanan->s_lunas_pbb == true) ? 'Yang Wajib Di Lunasi' : ''; }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="datatunggakan"></div>
            </div>
            <div class="modal-footer right">
                <button type="button" class="btn btn-outline-light" data-dismiss="modal"
                    onclick="tutupmodaltunggakan()">Tutup</button>
            </div>
        </div>
    </div>
</div>
@include('pelayanan.pbb.components.data-input.keterangan-pemohon')

@push('scripts')
<script>
    var kodePropinsiKab = '{{ isset($pelayanan) ? $pelayanan->objekPajak->nop : $kdKabkota  }}';

    const kodeKecamatan = JSON.parse('@json($kecamatan)');
    const jenisTanahList = JSON.parse('@json($lookupItem1)');

    function tambahNopLama() {
        var html = '';
        var no = parseInt($("#counterNopLama").val());
        html += '<div id="divNopLama' + no + '" class="card card-danger card-outline" style="border-top:3px solid #dc3545;">' +
            '<div class="card-body">' +
            '<div class="row">' +
            '<div class="col-sm-12">' +
            '<div class="form-group-sm row">' +
            '<label class="col-sm-2 col-form-label-sm">NOP <spanclass="text-danger">*</span></label>' +
            '<div class="col-sm-4">' +
            '<input type="hidden" id="idOPLama' + no + '" name="idOPLama[]">' +
            '<input type="text" class="form-control form-control-sm masked-nop" id="nopLamaCari' + no + '" name="nopLamaCari[]" placeholder="xx.xx.xxx.xxx.xxx.xxxx.x" value="' + kodePropinsiKab + '"  required>' +
            '<input type="hidden" id="nopLama' + no + '" name="nopLama[]" required>' +
            '</div>' +
            '<div class="col-sm-4">' +
            '<button type="button" class="btn btn-sm btn-primary" onclick="cariNopLama(' + no + '); getTunggakanSppt('+ no +')"><span class="fas fa-search"></span>CARI</button>' +
            '&nbsp;&nbsp;<button type="button" class="btn btn-sm btn-danger" onclick="hapusDivNopLama(' + no + ')">Hapus</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-sm-6">' +
            '<hr style="background-color: #dc3545; height: 2px;">' +
            '<p style="font-weight: bold">DATA OBJEK PAJAK (LAMA)</p>' +
            '<hr>' +
            '<div class="form-group-sm row">' +
            '<label class="col-sm-4 col-form-label-sm">RT</label>' +
            '<div class="col-sm-2">' +
            '<input type="text" class="form-control form-control-sm" id="rtOPLama' + no + '" name="rtOPLama[]" readonly="">' +
            '</div>' +
            '<label class="col-sm-2 col-form-label-sm">RW</label>' +
            '<div class="col-sm-2">' +
            '<input type="text" class="form-control form-control-sm" id="rwOPLama' + no + '" name="rwOPLama[]" readonly="">' +
            '</div>' +
            '</div>' +
            '<div class="form-group-sm row">' +
            '<label class="col-sm-4 col-form-label-sm">Jalan</label>' +
            '<div class="col-sm-8">' +
            '<input type="text" class="form-control form-control-sm" id="jalanOPLama' + no + '" name="jalanOPLama[]" readonly="">' +
            '</div>' +
            '</div>' +
            '<div class="form-group-sm row">' +
            '<label class="col-sm-4 col-form-label-sm">Blok/No/Kav</label>' +
            '<div class="col-sm-8">' +
            '<input type="text" class="form-control form-control-sm" id="blokNoOPLama' + no + '" name="blokNoOPLama[]" readonly="">' +
            '</div>' +
            '</div>' +
            '<div class="form-group-sm row">' +
            '<label class="col-sm-4 col-form-label-sm">Kelurahan/Desa</label>' +
            '<div class="col-sm-8">' +
            '<input type="text" class="form-control form-control-sm" id="kelurahanOPLama' + no + '" name="kelurahanOPLama[]" readonly="">' +
            '</div>' +
            '</div>' +
            '<div class="form-group-sm row">' +
            '<label class="col-sm-4 col-form-label-sm">Kecamatan</label>' +
            '<div class="col-sm-8">' +
            '<input type="text" class="form-control form-control-sm" id="kecamatanOPLama' + no + '" name="kecamatanOPLama[]" readonly="">' +
            '</div>' +
            '</div>' +
            '<div class="form-group-sm row">' +
            '<label class="col-sm-4 col-form-label-sm">Luas Tanah (M<sup>2</sup>)</label>' +
            '<div class="col-sm-8">' +
            '<input type="text" class="form-control form-control-sm" id="luasTanahLama' + no + '" name="luasTanahLama[]" readonly="" required>' +
            '</div>' +
            '</div>' +
            '<div class="form-group-sm row">' +
            '<label class="col-sm-4 col-form-label-sm">Luas Bangunan (M<sup>2</sup>)</label>' +
            '<div class="col-sm-8">' +
            '<input type="text" class="form-control form-control-sm" id="luasBangunanLama' + no + '" name="luasBangunanLama[]" readonly="" required>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-sm-6">' +
            '<hr style="background-color: #dc3545; height: 2px;">' +
            '<p style="font-weight: bold">DATA WAJIB PAJAK (LAMA)</p>' +
            '<hr>' +
            '<div class="form-group-sm row">' +
            '<label class="col-sm-4 col-form-label-sm">NIK</label>' +
            '<div class="col-sm-8">' +
            '<input type="hidden" id="idWPLama' + no + '" name="idWPLama[]">' +
            '<input type="text" class="form-control form-control-sm masked-nik" id="nikWPLama' + no + '" name="nikWPLama[]" readonly="" required>' +
            '</div>' +
            '</div>' +
            '<div class="form-group-sm row">' +
            '<label class="col-sm-4 col-form-label-sm">Nama</label>' +
            '<div class="col-sm-8">' +
            '<input type="text" class="form-control form-control-sm" id="namaWPLama' + no + '" name="namaWPLama[]" readonly="" required>' +
            '</div>' +
            '</div>' +
            '<div class="form-group-sm row">' +
            '<label class="col-sm-4 col-form-label-sm">RT</label>' +
            '<div class="col-sm-2">' +
            '<input type="text" class="form-control form-control-sm" id="rtWPLama' + no + '" name="rtWPLama[]" readonly="">' +
            '</div>' +
            '<label class="col-sm-2 col-form-label-sm">RW</label>' +
            '<div class="col-sm-2">' +
            '<input type="text" class="form-control form-control-sm" id="rwWPLama' + no + '" name="rwWPLama[]" readonly="">' +
            '</div>' +
            '</div>' +
            '<div class="form-group-sm row">' +
            '<label class="col-sm-4 col-form-label-sm">Jalan</label>' +
            '<div class="col-sm-8">' +
            '<input type="text" class="form-control form-control-sm" id="jalanWPLama' + no + '" name="jalanWPLama[]" readonly="">' +
            '</div>' +
            '</div>' +
            '<div class="form-group-sm row">' +
            '<label class="col-sm-4 col-form-label-sm">Kelurahan/Desa</label>' +
            '<div class="col-sm-8">' +
            '<input type="text" class="form-control form-control-sm" id="kelurahanWPLama' + no + '" name="kelurahanWPLama[]" readonly="">' +
            '</div>' +
            '</div>' +
            '<div class="form-group-sm row">' +
            '<label class="col-sm-4 col-form-label-sm">Kecamatan</label>' +
            '<div class="col-sm-8">' +
            '<input type="text" class="form-control form-control-sm" id="kecamatanWPLama' + no + '" name="kecamatanWPLama[]" readonly="">' +
            '</div>' +
            '</div>' +
            '<div class="form-group-sm row">' +
            '<label class="col-sm-4 col-form-label-sm">Kabupaten/Kota</label>' +
            '<div class="col-sm-8">' +
            '<input type="text" class="form-control form-control-sm" id="kabupatenWPLama' + no + '" name="kabupatenWPLama[]" readonly="">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';

        html += '<script>$(".masked-nop").mask(\'99.99.999.999.999.9999.9\')<' + '/script>';

        $("#dataNopAkanGabung").append(html);
        no = no + 1;
        $("#counterNopLama").val(no)
    }

    function hapusDivNopLama(a) {
        $("#divNopLama" + a).remove();
    }

    function setNop() {
        $('#nopCari').val(kodePropinsiKab);
    }
    setNop();

    function cariNopLama(a) {
        $("#nopLama" + a).val('');
        $("#rtOPLama" + a).val('');
        $("#rwOPLama" + a).val('');
        $("#jalanOPLama" + a).val('');
        $("#blokNoOPLama" + a).val('');
        $("#kecamatanOPLama" + a).val('');
        $("#kelurahanOPLama" + a).val('');
        $("#luasTanahLama" + a).val('');
        $("#luasBangunanLama" + a).val('');
        $("#nikWPLama" + a).val('');
        $("#namaWPLama" + a).val('');
        $("#rtWPLama" + a).val('');
        $("#rwWPLama" + a).val('');
        $("#jalanWPLama" + a).val('');
        $("#kelurahanWPLama" + a).val('');
        $("#kecamatanWPLama" + a).val('');
        $("#kabupatenWPLama" + a).val('');
        $.get("/dat-objek-pajak", {
            nop: $("#nopLamaCari" + a).val()
        })
        .then(function(data) {
            if (data.nop === undefined) {
                alert("NOP " + $("#nopLamaCari" + a).val() + " Tidak ditemukan!");

            } else {
                $("#nopLama" + a).val(data.nop);
                $("#rtOPLama" + a).val(data.rt_op);
                $("#rwOPLama" + a).val(data.rw_op);
                $("#jalanOPLama" + a).val(data.jalan_op);
                $("#blokNoOPLama" + a).val(data.blok_kav_no_op);
                $("#kecamatanOPLama" + a).val(data.nm_kecamatan);
                $("#kelurahanOPLama" + a).val(data.nm_kelurahan);
                $("#luasTanahLama" + a).val(data.total_luas_bumi);
                $("#luasBangunanLama" + a).val(data.total_luas_bng);
                $("#nikWPLama" + a).val(data.subjek_pajak.subjek_pajak_id);
                $("#namaWPLama" + a).val(data.subjek_pajak.nm_wp);
                $("#rtWPLama" + a).val(data.subjek_pajak.rt_wp);
                $("#rwWPLama" + a).val(data.subjek_pajak.rw_wp);
                $("#jalanWPLama" + a).val(data.subjek_pajak.jalan_wp);
                $("#kelurahanWPLama" + a).val(data.subjek_pajak.kelurahan_wp);
                $("#kecamatanWPLama" + a).val(data.subjek_pajak.kecamatanWP);
                $("#kabupatenWPLama" + a).val(data.subjek_pajak.kota_wp);

                // sudahAjukanPermohonan();
                // cek();
            }
        })
    }

    function cariNop() {
        $("#nop").val('');
        $("#rtOP").val('');
        $("#rwOP").val('');
        $("#jalanOP").val('');
        $("#blokNoOP").val('');
        $("#kecamatanOP").val('');
        $("#kelurahanOP").val('');
        $("#luasTanah").val('');
        $("#luasBangunan").val('');
        $("#nikWP").val('');
        $("#namaWP").val('');
        $("#rtWP").val('');
        $("#rwWP").val('');
        $("#jalanWP").val('');
        $("#kelurahanWP").val('');
        $("#kecamatanWP").val('');
        $("#kabupatenWP").val('');

        var arrNOPLama = [];
        $("input[name='nopLama[]']").each(function() {
            arrNOPLama.push($(this).val());
        });

        var nop = $("#nopCari").val();
        if (arrNOPLama.includes(nop.replaceAll('.', '')) == false) {
            alert("NOP Gabungan tidak terdapat pada nop lama");
        } else {
            $.get("/dat-objek-pajak", {
                nop: $("#nopCari").val()
            })
            .then(function(data) {
                if (data.nop === undefined) {
                    alert("NOP " + $("#nopCari").val() + " Tidak ditemukan!");

                } else {
                    $("#nop").val(data.nop);
                    $("#rtOP").val(data.rt_op);
                    $("#rwOP").val(data.rw_op);
                    $("#jalanOP").val(data.jalan_op);
                    $("#blokNoOP").val(data.blok_kav_no_op);
                    $("#kecamatanOP").val(data.nm_kecamatan);
                    $("#kelurahanOP").val(data.nm_kelurahan);
                    $("#luasTanah").val(data.total_luas_bumi);
                    $("#luasBangunan").val(data.total_luas_bng);
                    $("#jenisTanah").val($("#t_nik_pemohon").val());

                    // $("#nikWP").val(data.subjek_pajak.subjek_pajak_id);
                    // $("#namaWP").val(data.subjek_pajak.nm_wp);
                    // $("#rtWP").val(data.subjek_pajak.rt_wp);
                    // $("#rwWP").val(data.subjek_pajak.rw_wp);
                    // $("#jalanWP").val(data.subjek_pajak.jalan_wp);
                    // $("#kelurahanWP").val(data.subjek_pajak.kelurahan_wp);
                    // $("#kecamatanWP").val(data.subjek_pajak.kecamatanWP);
                    // $("#kabupatenWP").val(data.subjek_pajak.kota_wp);
                    // $("#jenisTanah").val(data.jenis_tanah);

                    $("#nikWP").val($("#t_nik_pemohon").val());
                    $("#namaWP").val($("#t_nama_pemohon").val());
                    $("#rtWP").val($("#t_rt_pemohon").val());
                    $("#rwWP").val($("#t_rw_pemohon").val());
                    $("#jalanWP").val($("#t_jalan_pemohon").val());
                    $("#kelurahanWP").val($("#t_kelurahan_pemohon").val());
                    $("#kecamatanWP").val($("#t_kecamatan_pemohon").val());
                    $("#kabupatenWP").val($("#t_kabupaten_pemohon").val());

                    // sudahAjukanPermohonan();
                    getTunggakanSppt2(nop);
                }
            })
        }
    }

    // $("#frmMutasiGabung").submit(function(e) {
    //     e.preventDefault();
    //     if ($("input[name='nopLamaCari[]']").length < 2) {
    //         $("#modal-save").modal('hide')
    //         alert("Data mutasi minimal 2 objek")
    //         return false;
    //     }
    // })

    function getTunggakanSppt(a){
        let currentUrl = "/pelayanan/cektunggakanpbb"
        $.ajax({
            url: currentUrl,
            type: 'POST',
            data: {
                nop: $("#nopLamaCari" + a).val()
            }
        }).then(function(response) {
            var aa = response;
            if (aa.PBB_YG_HARUS_DIBAYAR_SPPT != '' && aa.datatunggakan != 3) {
                jQuery('#ModalTunggakanPBB').modal('show');
                $('#datatunggakan').html(aa.datatunggakan);
            } else if(aa.PBB_YG_HARUS_DIBAYAR_SPPT == '' && aa.datatunggakan != 3){
                alert('Tidak Ada Tunggakan');
            } 
        });
    }

    function getTunggakanSppt2(nop){
        // console.log(nop);
        let currentUrl = "/pelayanan/cektunggakanpbb"
        $.ajax({
            url: currentUrl,
            type: 'POST',
            data: {
                nop: nop
            }
        }).then(function(response) {
            var aa = response;
            if (aa.PBB_YG_HARUS_DIBAYAR_SPPT != '' && aa.datatunggakan != 3) {
                jQuery('#ModalTunggakanPBB').modal('show');
                $('#datatunggakan').html(aa.datatunggakan);
            } else if(aa.PBB_YG_HARUS_DIBAYAR_SPPT == '' && aa.datatunggakan != 3){
                alert('Tidak Ada Tunggakan');
                $("#btnSimpanDraft").removeAttr('disabled');
            } 
        });
    }

    function tutupmodaltunggakan() {
        jQuery('#ModalTunggakanPBB').modal('hide');
    }

    // function cek(){
    //     let a = document.querySelectorAll("input[name^='nopLamaCari[']");
    //     for (ab of a){
    //         if(!a.includes(val)){
    //             a.push(val)
    //         }else{
    //             alert("Duplicate entry");
    //         }
    //         console.log(ab.value);
    //     }
    // }

</script>
@endpush
