@extends('layouts.app')

@section('title')
    Kepoin || Bangunan
@endsection

@section('content')
    <section class="content">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <section>
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a th:href="@{/pelayanan}">Home</a></li>
                                <li class="breadcrumb-item active">Pelayanan</li>
                            </ol>
                        </section>
                    </div>
                    <div class="col-12">
                        <div class="info-box callout callout-danger" id="background2">
                            <div class="col-sm-12">
                                <div class="" style="float: left;">
                                    <h5 class="">FORM INPUT DATA BANGUNAN</h5>
                                    <span class="float">Isilah data dengan teliti dan benar.</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                        </div>
                    </div>

                </div>
            </div><!-- /.container-fluid -->
            <div class="container-fluid">
                <div class="row">

                    <div class="col-12">
                        @if (session('success'))
                            <div class="alert alert-dismissible alert_customs_success">
                                <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-check"></i> Success!</h5>
                                <span>{{ session('success') }}</span>
                                <a href="/pelayanan/upload/{{ $pelayanan->uuid }}" class="btn btn-sm btn-primary"> Selesai
                                    Input Data Bangunan dan Lanjutkan Permohonan</a>
                            </div>
                        @endif
                        <div
                            class="alert alert-dismissible alert_customs_danger @if (!session()->get('error')) d-none @endif">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h5><i class="icon fas fa-exclamation-triangle"></i> Gagal!</h5>
                            <span th:text="${error}"></span>
                        </div>
                        <!-- =========================================================== -->
                        <form class="form-horizontal form-validation" method="post"
                            action="{{ url('pelayanan/bangunan/' . $pelayanan->t_id_pelayanan . '/save') }}">
                            @csrf
                            <div class="card card-danger card-outline" style="border-top:3px solid #dc3545;">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-condensed">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Id Objek</th>
                                                        <th>Lokasi Objek</th>
                                                        <th>Nama WP</th>
                                                        <th>Status</th>
                                                        <th>Input Baru</th>
                                                        <th>Data Bangunan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($pelayanan->objekPajaks as $i => $objekPajak)
                                                        @php
                                                            if ($objekPajak->t_jenis_tanah != 1) {
                                                                continue;
                                                            }
                                                        @endphp
                                                        <tr>
                                                            <td class="text-center">{{ $i + 1 }}</td>
                                                            <td class="text-center text-bold">{{ $objekPajak->t_id_op }}
                                                            </td>
                                                            <td class="text-bold">{{ $objekPajak->t_jalan_op }}</td>
                                                            <td class="text-bold">
                                                                {{ $pelayanan->wajibPajaks[$i]->t_nama_wp }}</td>
                                                            <td class="text-center"><b class="op-label-act"
                                                                    id="opActive_{{ $objekPajak->t_id_op }}">Active</b></td>
                                                            <td class="text-center">
                                                                <button type="button" class="btn btn-primary btn-sm"
                                                                    onclick="switchObjek({{ $objekPajak->t_id_op }})">Input
                                                                    Baru</button>
                                                            </td>
                                                            <td class="text-bold text-center">
                                                                {{ $objekPajak->detailBangunan->count() }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-5">
                                            <p style="font-weight: bold">RINCIAN DATA BANGUNAN</p>
                                            <hr>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-6 col-form-label-sm">Jenis Penggunaan Bangunan</label>
                                                <div class="col-sm-4 ">
                                                    <input type="hidden" name="idPelayanan" id="idPelayanan"
                                                        value="{{ $pelayanan->t_id_pelayanan }}">
                                                    <input type="hidden" name="idOp" id="idOp"
                                                        value="{{ $pelayanan->objekPajaks[0]->t_id_op }}">
                                                    <input type="hidden" name="idDetailBangunan" id="idDetailBangunan"
                                                        value="{{ $idDetailBangunan ?? '' }}">
                                                    <input type="hidden" name="nomorBangunan" id="nomorBangunan"
                                                        value="{{ $nomorBangunan ?? '' }}">
                                                    <select class="form-control form-control-sm"
                                                        name="jenisPenggunaanBangunan" id="jenisPenggunaanBangunan">
                                                        <option value=""> ---Silahkan Pilih---</option>
                                                        <option value="Perumahan"> Perumahan</option>
                                                        <option value="Toko/Apotik/Pasar/Ruko"> Toko/Apotik/Pasar/Ruko
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-6 col-form-label-sm">Luas Bangunan <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control form-control-sm"
                                                        onkeypress="return onlyNumberKey(event)" id="luasBangunan"
                                                        name="luasBangunan"
                                                        value="{{ $detailBangunan->luasBangunan ?? '' }}">
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-6 col-form-label-sm">Jumlah Lantai <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-4">
                                                    <input type="text" maxlength="3"
                                                        class="form-control form-control-sm" id="jumlahLantai"
                                                        name="jumlahLantai"
                                                        value="{{ $detailBangunan->jumlahLantai ?? '' }}">
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-6 col-form-label-sm">Tahun Dibangun <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-4">
                                                    <input type="text" pattern="[0-9.]+" maxlength="4"
                                                        class="form-control form-control-sm" id="tahunBangunan"
                                                        name="tahunBangunan"
                                                        value="{{ $detailBangunan->tahunBangunan ?? '' }}">
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-6 col-form-label-sm">Tahun Direnovasi<sup>
                                                        <i style="color:red">isi tahun/Kosongkan</i></sup>
                                                </label>
                                                <div class="col-sm-4">
                                                    <input type="text" pattern="[0-9.]+" maxlength="4"
                                                        class="form-control form-control-sm" id="tahunRenovasi"
                                                        name="tahunRenovasi"
                                                        value="{{ $detailBangunan->tahunRenovasi ?? '' }}">
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-6 col-form-label-sm">Kondisi Pada Umumnya <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-4 ">
                                                    <select class="form-control form-control-sm" name="kondisiBangunan"
                                                        id="kondisiBangunan">
                                                        <option value=""> ---Silahkan Pilih---</option>
                                                        <option value="SANGAT BAIK"> Sangat Baik
                                                        </option>
                                                        <option value="BAIK"> Baik
                                                        </option>
                                                        <option value="SEDANG"> Sedang
                                                        </option>
                                                        <option value="JELEK"> Jelek
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-6 col-form-label-sm">Konstruksi <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-4 ">
                                                    <select class="form-control form-control-sm" name="konstruksi"
                                                        id="konstruksi">
                                                        <option value=""> ---Silahkan Pilih---</option>
                                                        <option value="BAJA"> Baja
                                                        </option>
                                                        <option value="BETON"> Beton
                                                        </option>
                                                        <option value="BATU BATA"> Batu Bata
                                                        </option>
                                                        <option value="KAYU"> Kayu
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-6 col-form-label-sm">Atap <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-4 ">
                                                    <select class="form-control form-control-sm" name="atap">
                                                        <option value=""> ---Silahkan Pilih---</option>
                                                        <option value="DECRABON/BETON/GTG GLAZUR"> Decrabon/Beton Gtg
                                                            Glazur
                                                        </option>
                                                        <option value="GTG BETON/ALUMUNIUM"> Gtg Beton/Alumunium
                                                        </option>
                                                        <option value="GTG BIASA/SIRAP"> Gtg Biasa/Sirap
                                                        </option>
                                                        <option value="ASBES"> Asbes
                                                        </option>
                                                        <option value="SENG">
                                                            Seng
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-6 col-form-label-sm">Dinding <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-4 ">
                                                    <select class="form-control form-control-sm" name="dinding" required>
                                                        <option value=""> ---Silahkan Pilih---</option>
                                                        <option value="KACA/ALUMUNIUM"> Kaca/Alumunium
                                                        </option>
                                                        <option value="BETON"> Beton
                                                        </option>
                                                        <option value="BATU BATA/CONBLOK"> Batu Bata/Conblok
                                                        </option>
                                                        <option value="KAYU"> Kayu
                                                        </option>
                                                        <option value="SENG"> Seng
                                                        </option>
                                                        <option value="TIDAK ADA"> Tidak Ada
                                                        </option>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-6 col-form-label-sm">Lantai <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-4 ">
                                                    <select class="form-control form-control-sm" name="lantai">
                                                        <option value=""> ---Silahkan Pilih---</option>
                                                        <option value="MARMER"> Marmer
                                                        </option>
                                                        <option value="KERAMIK"> Keramik
                                                        </option>
                                                        <option value="TERASO"> Teraso
                                                        </option>
                                                        <option value="UBIN PC/PAPAN"> Ubin PC/Papan
                                                        </option>
                                                        <option value="SEMEN"> Semen
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-6 col-form-label-sm">Langit-Langit<span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-4 ">
                                                    <select class="form-control form-control-sm" name="langitLangit">
                                                        <option value=""> ---Silahkan Pilih---</option>
                                                        <option value="AKUSTIK/JATI"> Akustik/Jati
                                                        </option>
                                                        <option value="TRIPLEK/ASBES BAMBU"> Triplek/Asbes/Bambu
                                                        </option>
                                                        <option value="TIDAK ADA"> Tidak Ada
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-7">
                                            <p style="font-weight: bold">FASILITAS</p>
                                            <hr>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-4 col-form-label-sm">Jumlah AC SPlit</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control form-control-sm"
                                                        id="acSplit" onkeypress="return onlyNumberKey(event)"
                                                        name="acSplit" value="{{ $detailBangunan->acSplit ?? '' }}">
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-4 col-form-label-sm">Jumlah AC Window</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control form-control-sm"
                                                        id="acWindow" onkeypress="return onlyNumberKey(event)"
                                                        name="acWindow" value="{{ $detailBangunan->acWindow ?? '' }}">
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-4 col-form-label-sm">Panjang Pagar (M)</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control form-control-sm"
                                                        id="panjangPagar" name="panjangPagar"
                                                        onkeypress="return onlyNumberKey(event)"
                                                        value="{{ $detailBangunan->panjangPagar ?? '' }}">
                                                </div>
                                                <label class="col-sm-2 col-form-label-sm">Bahan Pagar</label>
                                                <div class="col-sm-3">
                                                    <select class="form-control form-control-sm" name="bahanPagar">
                                                        <option value=""> ---Silahkan Pilih---</option>
                                                        <option value="BAJA/BESI"> Baja/Besi
                                                        </option>
                                                        <option value="BATA/BATAKO"> Bata/Batako
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group-sm row">
                                                <label class="col-sm-4 col-form-label-sm">Daya Listrik(VA)</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control form-control-sm"
                                                        id="dayaListrik" onkeypress="return onlyNumberKey(event)"
                                                        name="dayaListrik"
                                                        value="{{ $detailBangunan->dayaListrik ?? '' }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="button" id="tombolSimpan"
                                    class="btn btn-primary float-left swalDefaultSimpan" data-toggle="modal"
                                    data-target="#modal-save"><span class="fas fa-save"></span> Simpan
                                </button>
                                <div class="modal fade" id="modal-save">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header bg-primary">
                                                <h6 class="modal-title"><span class="fas fa-save"></span> &nbsp;SIMPAN
                                                    DATA BANGUNAN ?</h6>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Apakah anda ingin menyimpan data bangunan tersebut?</p>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <button type="button" class="btn btn-danger btn-sm"
                                                    data-dismiss="modal"><span class="fas fa-times-circle"></span>
                                                    &nbsp;TUTUP
                                                </button>
                                                <button type="submit" class="btn btn-success btn-sm"><span
                                                        class="fas fa-save"></span> &nbsp;YA
                                                </button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <button type="button" class="btn btn-primary float-right" data-toggle="modal"
                                    data-target="#modal-cancel">
                                    <span class="fas fa-check"></span> &nbsp;Lanjutkan Permohonan
                                </button>
                                <div class="modal fade" id="modal-cancel">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header bg-danger">
                                                <h6 class="modal-title"><span class="fas fa-exclamation"></span>
                                                    &nbsp;ANDA YAKIN MELANJUTKAN PERMOHONAN ?</h6>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Apakah anda yakin dengan inputan data bangunan tersebut?</p>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <button type="button" class="btn btn-danger btn-sm"
                                                    data-dismiss="modal"><span class="fas fa-times-circle"></span>
                                                    &nbsp;TUTUP
                                                </button>
                                                <a href="/pelayanan/upload/{{ $pelayanan->uuid }}"
                                                    class="btn btn-success btn-sm"><span
                                                        class="fas fa-exclamation"></span>
                                                    &nbsp;YA</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.card -->

            </div>
        </div>
        <!-- /.row -->


    </section>
@endsection

@push('scripts')
    <script>
        function getDetailBangunan(bngId) {
            $.get(`/pelayanan/detailBangunan/${bngId}`, (res) => {
                $("#jenisPenggunaanBangunan").val(res.data.t_jenis_penggunaan_bangunan);
                $("#luasBangunan").val(res.data.t_luas);
                $("#jumlahLantai").val(res.data.t_jumlah_lanati);
                $("#tahunBangunan").val(res.data.t_tahun_bangunan);
                $("#tahunRenovasi").val(res.data.t_tahun_renovasi);
                $("#kondisiBangunan").val(res.data.t_kondisi_bangunan);
                $("#konstruksi").val(res.data.t_konstruksi);
                $("#idDetailBangunan").val(res.data.t_id_detail_bangunan);
                $("#acSplit").val(res.data.t_ac_split);
                $("#acWindow").val(res.data.t_ac_window);
                $("#panjangPagar").val(res.data.t_panjang_pagar);
                $("#dayaListrik").val(res.data.t_listrik);
                $("#idDetailBangunan").val(res.data.t_id_detail_bangunan);
                $("#nomorBangunan").val(res.data.t_no_urut_bangunan);

                $("select[name='atap']").val(res.data.t_atap);
                $("select[name='dinding']").val(res.data.t_dinding);
                $("select[name='lantai']").val(res.data.t_lantai);
                $("select[name='langitLangit']").val(res.data.t_langit_langit);
                $("select[name='bahanPagar']").val(res.data.t_bahan_pagar);
            })
        }

        function switchObjek(op) {
            $("#idOp").val(op);
            const opLables = document.querySelectorAll('.op-label-act');
            const activeOp = 'opActive_' + op;
            opLables.forEach(element => {
                if (element.id === activeOp) {
                    element.innerText = 'Active'
                } else {
                    element.innerText = 'Tidak Active'
                }
            });
        }
    </script>
@endpush
