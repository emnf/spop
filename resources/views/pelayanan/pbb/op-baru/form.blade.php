<h4 class="mt-4 mb-2">Data Objek</h4>
<div class="col-12">
    <hr style="background-color: #28a745; height: 2px;">
</div>
<div class="row">
    <div class="col-md-6">
        <p style="font-weight: bold">DATA OBJEK PAJAK (BARU)</p>
        <hr>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">RT</label>
            <div class="col-sm-2">
                <input type="hidden" id="t_id_op" name="t_id_op" value="{{ $objek_pajak->t_id_op ?? '' }}">
                <input type="text" class="form-control form-control-sm masked-number-3" id="t_rt_op" name="t_rt_op"
                    value="{{ old('t_rt_op') ?? $objek_pajak->t_rt_op ?? '' }}">
            </div>
            <label class="col-sm-2 col-form-label-sm">RW</label>
            <div class="col-sm-2">
                <input type="text" class="form-control form-control-sm masked-number-2" id="t_rw_op" name="t_rw_op"
                    value="{{ old('t_rw_op') ?? $objek_pajak->t_rw_op ?? '' }}">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Jalan</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_jalan_op" name="t_jalan_op"
                    value="{{ old('t_jalan_op') ?? $objek_pajak->t_jalan_op ?? '' }}">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Blok/Kav</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_blok_kav" name="t_blok_kav"
                    value="{{ old('t_blok_kav') ?? $objek_pajak->t_blok_kav ?? '' }}">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Kecamatan</label>
            <div class="col-sm-8">
                <select class="form-control" id="kd_kecamatan" name="kd_kecamatan">
                    <option value="">Silahkan Pilih</option>
                </select>
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa</label>
            <div class="col-sm-8">
                <select class="form-control" id="kd_kelurahan" name="kd_kelurahan">
                    <option value="">Silahkan Pilih</option>
                    </option>
                </select>
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Jenis Tanah <span class="text-danger">*</span></label>
            <div class="col-sm-8">
                <select class="form-control" name="t_jenis_tanah" id="t_jenis_tanah">
                    <option value="">Silahkan Pilih</option>
                    @foreach($lookupItem1 as $item)
                    <option value="{{ $item->kd_lookup_item }}"
                        {{ !isset($objek_pajak) ? '' : ((int)$objek_pajak->t_jenis_tanah == (int)$item->kd_lookup_item) ? 'selected' : ''}}>
                        {{ $item->nm_lookup_item }}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Keterangan Objek Pajak <span class="text-danger">*</span></label>
            <div class="col-sm-8">
                <select class="form-control" name="t_kode_lookup_item" id="t_kode_lookup_item">
                    <option value="">Silahkan Pilih</option>
                    @foreach($lookupItem2 as $item)
                    <option value="{{ $item->kd_lookup_item }}"
                        {{ !isset($objek_pajak) ? '' : ((int) $objek_pajak->t_kode_lookup_item == (int)$item->kd_lookup_item) ? 'selected' : ''}}>
                        {{ $item->nm_lookup_item }}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Luas Tanah (M<sup>2</sup>)
                <span class="text-danger">*</span></label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_luas_tanah" name="t_luas_tanah"
                    value="{{ old('t_luas_tanah') ?? $objek_pajak->t_luas_tanah ?? '' }}" required>
            </div>
        </div>
    </div>
    <div class="col-md-6" id="div-wpbaru">
        <p style="font-weight: bold">DATA WAJIB PAJAK (BARU)</p>
        <hr>
        <button type="button" class="btn btn-primary btn-sm" onclick="copyPemohon('div-wpbaru')">copy data pemohon</button>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">NIK <span class="text-danger">*</span></label>
            <div class="col-sm-8">
                <input type="hidden" id="t_id_wp" name="t_id_wp" value="{{ $wajib_pajak->t_id_wp ?? '' }}">
                <input type="text" class="form-control form-control-sm masked-nik" id="t_nik_wp" name="t_nik_wp"
                    value="{{ old('t_nik_wp') ?? $wajib_pajak->t_nik_wp ?? '' }}" maxlength="16" required>
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Nama <span class="text-danger">*</span></label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_nama_wp" name="t_nama_wp"
                    value="{{ old('t_nama_wp') ?? $wajib_pajak->t_nama_wp ?? '' }}" required>
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">RT<span class="text-danger">*</span></label>
            <div class="col-sm-2">
                <input type="text" required class="form-control form-control-sm masked-number-3" id="t_rt_wp"
                    name="t_rt_wp" value="{{ old('t_rt_wp') ?? $wajib_pajak->t_rt_wp ?? '' }}">
            </div>
            <label class="col-sm-2 col-form-label-sm">RW<span class="text-danger">*</span></label>
            <div class="col-sm-2">
                <input type="text" required class="form-control form-control-sm masked-number-2" id="t_rw_wp"
                    name="t_rw_wp" value="{{ old('t_rw_wp') ?? $wajib_pajak->t_rw_wp ?? '' }}">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Jalan<span class="text-danger">*</span></label>
            <div class="col-sm-8">
                <input required type="text" class="form-control form-control-sm" id="t_jalan_wp" name="t_jalan_wp"
                    value="{{ old('t_jalan_wp') ?? $wajib_pajak->t_jalan_wp ?? '' }}">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Blok/Kav</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_blok_kav_wp" name="t_blok_kav_wp"
                    value="{{ old('t_blok_kav_wp') ?? $wajib_pajak->t_blok_kav_wp ?? '' }}">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa<span class="text-danger">*</span></label>
            <div class="col-sm-8">
                <input type="text" required class="form-control form-control-sm" id="t_kelurahan_wp"
                    name="t_kelurahan_wp" value="{{ old('t_kelurahan_wp') ?? $wajib_pajak->t_kelurahan_wp ?? '' }}">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Kecamatan<span class="text-danger">*</span></label>
            <div class="col-sm-8">
                <input type="text" required class="form-control form-control-sm" id="t_kecamatan_wp"
                    name="t_kecamatan_wp" value="{{ old('t_kecamatan_wp') ?? $wajib_pajak->t_kecamatan_wp ?? '' }}">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Kabupaten/Kota<span class="text-danger">*</span></label>
            <div class="col-sm-8">
                <input type="text" required class="form-control form-control-sm" id="t_kabupaten_wp"
                    name="t_kabupaten_wp" value="{{ old('t_kabupaten_wp') ?? $wajib_pajak->t_kabupaten_wp ?? '' }}">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">No. Telp<span class="text-danger">*</span></label>
            <div class="col-sm-8">
                <input type="text" required class="form-control form-control-sm masked-hp" id="t_no_hp_wp"
                    name="t_no_hp_wp" value="{{ old('t_no_hp_wp') ?? $wajib_pajak->t_no_hp_wp ?? '' }}">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">NPWPD</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm masked-npwpd" id="t_npwpd" name="t_npwpd"
                    value="{{ old('t_npwpd') ?? $wajib_pajak->t_npwpd ?? '' }}">
            </div>
        </div>
        <div class="form-group-sm row">
            <label class="col-sm-4 col-form-label-sm">Email</label>
            <div class="col-sm-8">
                <input type="text" class="form-control form-control-sm" id="t_email" name="t_email"
                    value="{{ old('t_email') ?? $wajib_pajak->t_email ?? '' }}">
            </div>
        </div>
    </div>
</div>



@include('pelayanan.pbb.components.data-input.peta-op')
@include('pelayanan.pbb.components.data-input.keterangan-pemohon')

@push('scripts')
<script>
    $(function() {
        $('#kd_kecamatan').change(function() {
            kelurahanCombo($(this).val())
        })
    })

    const kelurahanCombo = function(idKec) {
        $.get('/kelurahan/getkelurahan-bykecamatan/' + idKec).then(function(res) {
                let comboKelurahan = document.querySelector("#kd_kelurahan");
                let kelurahaList = '<option value="">Silahkan Pilih</option>';
                let kd_kelurahan = '{{ $objek_pajak->kd_kelurahan ?? '0' }}'

                if (res.success) {
                    res.data.map((kel, key) => {
                        kelurahaList += `<option value="${kel.kd_kelurahan}" ${kd_kelurahan == kel.kd_kelurahan ? 'selected' : ''} >${kel.kd_kelurahan + ' - ' + kel.nm_kelurahan}</option>`;
                    })
                }
                comboKelurahan.innerHTML = kelurahaList
            })
    }

    const kecamatanCombo = function() {
        $.get('/kecamatan/getkecamatan-all', function(res) {
            let comboKecamatan = document.querySelector("#kd_kecamatan");
            let kecList = '<option value="">Silahkan Pilih</option>';
            let kd_kecamatan = '{{ $objek_pajak->kd_kecamatan ?? 0}}';

            if (res.success) {
                res.data.map((kec, key) => {
                    kecList += `<option value="${kec.kd_kecamatan}" ${kd_kecamatan == kec.kd_kecamatan ? 'selected' : ''} >${kec.kd_kecamatan +' - '+ kec.nm_kecamatan}</option>`
                });
            }
            comboKecamatan.innerHTML = kecList;

            if (kd_kecamatan !== '0') {
                kelurahanCombo(kd_kecamatan);
            }
        })
    }
    kecamatanCombo();

    function sudahAjukanPermohonanOpBaru() {
        $("#modal-sudah-ajukan .modal-body").html('');

        $.post('/pelayanan/pengajuan-sebelumnya-opbaru', {
            nikPemohon: $('#t_nik_pemohon').val(),
            idJenisPelayanan: $('#t_id_jenis_pelayanan').val(),
            idJenisPajak: $('#t_id_jenis_pajak').val(),
            nikWp: $('#t_nik_wp').val(),
        }, function(res) {
            let data = res.data.data
console.log(data);
            if (data != null) {
                tbodyMessages = '<div class="col-12">' +
                    '<div class="alert alert-info alert-dismissible">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
                    'Permohonan anda sudah pernah direkam / diajukan sebelumnya, dengan data sebagai berikut:' +
                    '</div>' +
                    '<div class="form-group-sm row">' +
                    '<label class="col-sm-4 col-form-label-sm">No. Pelayanan</label>' +
                    '<div class="col-sm-8"> : <strong class="text-blue">' + data.noPelayanan + '</strong> </div>' +
                    '</div>' +
                    '<div class="form-group-sm row">' +
                    '<label class="col-sm-4 col-form-label-sm">NIK Pemohon</label>' +
                    '<div class="col-sm-8"> : ' + data.nikPemohon + ' </div>' +
                    '</div>' +
                    '<div class="form-group-sm row">' +
                    '<label class="col-sm-4 col-form-label-sm">Nama Pemohon</label>' +
                    '<div class="col-sm-8"> : ' + data.namaPemohon + ' </div>' +
                    '</div>' +
                    '<div class="form-group-sm row">' +
                    '<label class="col-sm-4 col-form-label-sm">NIK Wajib Pajak <small class="text-blue">(Baru)</small></label>' +
                    '<div class="col-sm-8"> : ' + data.nikWp + ' </div>' +
                    '</div>' +
                    '<div class="form-group-sm row">' +
                    '<label class="col-sm-4 col-form-label-sm">Nama WP <small class="text-blue">(Baru)</small></label>' +
                    '<div class="col-sm-8"> : ' + data.namaWp + ' </div>' +
                    '</div>' +
                    '<div class="form-group-sm row">' +
                    '<label class="col-sm-4 col-form-label-sm">Jenis Pelayanan</label>' +
                    '<div class="col-sm-8"> : ' + data.jenisPelayanan + ' </div>' +
                    '</div>' +
                    '<div class="form-group-sm row">' +
                    '<label class="col-sm-4 col-form-label-sm">Jenis Pajak</label>' +
                    '<div class="col-sm-8"> : ' + data.jenisPajak + ' </div>' +
                    '</div>' +
                    '<div class="form-group-sm row">' +
                    '<label class="col-sm-4 col-form-label-sm">Status Permohonan</label>' +
                    '<div class="col-sm-8"> : <span class="btn btn-sm bg-warning">' + data.statusPermohonan + '</span> </div>' +
                    '</div>' +
                    '</div>';

                $("#modal-sudah-ajukan").modal("show");
                $("#btnSimpanDraft").removeAttr('disabled');
                $("#modal-sudah-ajukan .modal-body").html(tbodyMessages);
            }
        })
    }

</script>
@endpush
