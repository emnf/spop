<h4 class="mt-4 mb-2">Data Nop Induk</h4>
<div class="card card-danger card-outline" style="border-top:3px solid #dc3545;">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group-sm row">
                    <label class="col-sm-2 col-form-label-sm">
                        NOP
                        <span class="text-danger">*</span>
                    </label>
                    <div class="col-sm-4">
                        <input type="hidden" id="idOPLama" name="idOPLama"
                            value="{{ old('idOPLama') ?? ($pelayanan->opLama->t_id_op_lama ?? '') }}">
                        <input type="text" class="form-control form-control-sm masked-nop" id="nopCariLama"
                            name="nopCariLama" value="{{ old('nopCariLama') ?? ($pelayanan->opLama->nop ?? '') }}"
                            placeholder="" required>
                        <input type="hidden" id="nopLama" name="nopLama"
                            value="{{ old('nopLama') ?? ($pelayanan->opLama->nop ?? '') }}" required>
                    </div>
                    <div class="col-sm-4">
                        <button type="button" id="cariNopLama" class="btn btn-sm btn-primary">
                            <span class="fas fa-search"></span>
                            CARI
                        </button>
                        <button type="button" id="editSubjeklama" class="btn btn-sm btn-success"
                            onclick="openSubjekLama()">
                            <span class="fas fa-edit"></span>
                            Update Data
                        </button>
                        <input type="hidden" id="statusLama" name="statusLama">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <hr style="background-color: #dc3545; height: 2px;">
                <p style="font-weight: bold">DATA OBJEK PAJAK (LAMA)</p>
                <hr>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">RT</label>
                    <div class="col-sm-2">
                        <input type="hidden" id="idOPLama" name="idOPLama"
                            value="{{ old('idOPLama') ?? ($pelayanan->OpLama->t_id_op_lama ?? '') }}">
                        <input type="text" class="form-control form-control-sm masked-number-3" id="rtOPLama"
                            name="rtOPLama" value="{{ old('rtOPLama') ?? ($pelayanan->opLama->t_rt_op ?? '') }}"
                            readonly="">
                    </div>
                    <label class="col-sm-2 col-form-label-sm">RW</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm masked-number-2" id="rwOPLama"
                            name="rwOPLama" value="{{ old('rwOPLama') ?? ($pelayanan->opLama->t_rw_op ?? '') }}"
                            readonly="">
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Jalan</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="jalanOPLama" name="jalanOPLama"
                            value="{{ old('jalanOPLama') ?? ($pelayanan->opLama->t_jalan_op ?? '') }}" readonly="">
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Blok/No/Kav</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="blokNoOPLama" name="blokNoOPLama"
                            value="{{ old('blokNoOPLama') ?? ($pelayanan->opLama->t_kelurahan_op ?? '') }}"
                            readonly="">
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kecamatan</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="kecamatanOPLama"
                            name="kecamatanOPLama"
                            value="{{ old('kecamatanOPLama') ?? ($pelayanan->opLama->t_kecamatan_op ?? '') }}"
                            readonly="">
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="kelurahanOPLama"
                            name="kelurahanOPLama"
                            value="{{ old('kelurahanOPLama') ?? ($pelayanan->opLama->t_kelurahan_op ?? '') }}"
                            readonly="">
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">
                        Luas Tanah
                        (M
                        <sup>2</sup>
                        )
                    </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="luasTanahLama"
                            name="luasTanahLama"
                            value="{{ old('luasTanahLama') ?? ($pelayanan->opLama->t_luas_tanah ?? '') }}"
                            readonly="" required>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">
                        Luas Bangunan
                        (M
                        <sup>2</sup>
                        )
                    </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="luasBangunanLama"
                            name="luasBangunanLama"
                            value="{{ old('luasBangunanLama') ?? ($pelayanan->opLama->t_luas_bangunan ?? '') }}"
                            readonly="" required>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <hr style="background-color: #dc3545; height: 2px;">
                <p style="font-weight: bold">DATA WAJIB PAJAK (LAMA)</p>
                <hr>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">NIK</label>
                    <div class="col-sm-8">
                        <input type="hidden" id="idWPLama" name="idWPLama"
                            value="{{ old('idWPLama') ?? ($pelayanan->WpLama->t_id_wp_lama ?? '') }}">
                        <input type="text" class="form-control form-control-sm masked-nik" id="nikWPLama"
                            name="nikWPLama" value="{{ old('nikWPLama') ?? ($pelayanan->WpLama->t_nik_wp ?? '') }}"
                            readonly="" required>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Nama</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="namaWPLama" name="namaWPLama"
                            value="{{ old('namaWPLama') ?? ($pelayanan->WpLama->t_nama_wp ?? '') }}" readonly=""
                            required>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">RT</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm masked-number-3" id="rtWPLama"
                            name="rtWPLama" value="{{ old('rtWPLama') ?? ($pelayanan->WpLama->t_rt_wp ?? '') }}"
                            readonly="">
                    </div>
                    <label class="col-sm-2 col-form-label-sm">RW</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm masked-number-2" id="rwWPLama"
                            name="rwWPLama" value="{{ old('rwWPLama') ?? ($pelayanan->WpLama->t_rw_wp ?? '') }}"
                            readonly="">
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Jalan</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="jalanWPLama"
                            name="jalanWPLama"
                            value="{{ old('jalanWPLama') ?? ($pelayanan->WpLama->t_jalan_wp ?? '') }}"
                            readonly="">
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="kelurahanWPLama"
                            name="kelurahanWPLama"
                            value="{{ old('kelurahanWPLama') ?? ($pelayanan->WpLama->t_kelurahan_wp ?? '') }}"
                            readonly="">
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kecamatan</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="kecamatanWPLama"
                            name="kecamatanWPLama"
                            value="{{ old('kecamatanWPLama') ?? ($pelayanan->WpLama->t_kecamatan_wp ?? '') }}"
                            readonly="">
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kabupaten/Kota</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="kabupatenWPLama"
                            name="kabupatenWPLama"
                            value="{{ old('kabupatenWPLama') ?? ($pelayanan->WpLama->t_kabupaten_wp ?? '') }}"
                            readonly="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<h4 class="mt-4 mb-2">Data Mutasi</h4>
<div class="card card-danger card-outline" style="border-top:3px solid #dc3545;">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <button type="button" onclick="tambahNopBaru()" class="btn btn-sm btn-info">Tambah</button>
            </div>
        </div>
        <div class="row mt-4">
            <input type="hidden" id="counterNopBaru"
                value="{{ isset($pelayanan) ? count($pelayanan->objekPajaks) + 1 : 1 }}">
            <div class="col-sm-12" id="dataNopAkanPecah">

                @if (isset($pelayanan) && count($pelayanan->objekPajaks) > 0)
                    @foreach ($pelayanan->objekPajaks as $key => $objekPajak)
                        <div id="divNopBaru{{ $key + 1 }}" class="card card-danger card-outline"
                            style="border-top:3px solid #dc3545;">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group-sm row">
                                            <div class="col-sm-4">
                                                <button type="button" class="btn btn-sm btn-danger"
                                                    onclick="hapusDivNopBaru('{{ $key + 1 }}')">Hapus</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <hr style="background-color: #dc3545; height: 2px;">
                                        <p style="font-weight: bold">DATA OBJEK PAJAK (BARU)</p>
                                        <hr>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">RT</label>
                                            <div class="col-sm-2">
                                                <input type="hidden" id="idOP{{ $key + 1 }}" name="idOP[]"
                                                    value="{{ $objekPajak->t_id_op ?? '' }}">
                                                <input type="text" class="form-control form-control-sm"
                                                    id="rtOP{{ $key + 1 }}"
                                                    value="{{ $objekPajak->t_rt_op ?? '' }}" name="rtOP[]">
                                            </div>
                                            <label class="col-sm-2 col-form-label-sm">RW</label>
                                            <div class="col-sm-2">
                                                <input type="text" class="form-control form-control-sm"
                                                    id="rwOP{{ $key + 1 }}"
                                                    value="{{ $objekPajak->t_rw_op ?? '' }}" name="rwOP[]">
                                            </div>
                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Jalan</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control form-control-sm"
                                                    id="jalanOP{{ $key + 1 }}"
                                                    value="{{ $objekPajak->t_jalan_op ?? '' }}" name="jalanOP[]">
                                            </div>
                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Blok/No/Kav</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control form-control-sm"
                                                    id="blokNoOP{{ $key + 1 }}"
                                                    value="{{ isset($objekPajak->t_blok_kav) ?? '' }}"
                                                    name="blokNoOP[]">
                                            </div>
                                        </div>

                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Kecamatan</label>
                                            <div class="col-sm-8">
                                                <select class="form-control" id="kecamatanOP{{ $key + 1 }}"
                                                    name="kecamatanOP[]"
                                                    onChange="kelurahanCari('{{ $key + 1 }}', null)">
                                                    @foreach ($kecamatan as $item)
                                                        <option @if ($objekPajak->kd_kecamatan == $item['kd_kecamatan']) selected @endif
                                                            value="{{ $item['kd_kecamatan'] }}">
                                                            {{ $item['kd_kecamatan'] }} - {{ $item['nm_kecamatan'] }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa</label>
                                            <div class="col-sm-8">
                                                <select class="form-control" name="kelurahanOP[]"
                                                    id="kelurahanOP{{ $key + 1 }}">
                                                    <option value="">Silahkan Pilih</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">
                                                Jenis Tanah
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-sm-8">
                                                <select class="form-control" name="jenisTanah[]"
                                                    id="jenisTanah{{ $key + 1 }}">
                                                    @foreach ($lookupItem1 as $items)
                                                        <option @if ((int) $objekPajak->t_jenis_tanah == (int) $items['kd_lookup_item']) selected @endif
                                                            value="{{ $items['kd_lookup_item'] }}">
                                                            {{ $items['nm_lookup_item'] }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">
                                                Luas Tanah
                                                (M
                                                <sup>2</sup>)
                                            </label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control form-control-sm"
                                                    id="luasTanah{{ $key + 1 }}"
                                                    value="{{ $objekPajak->t_luas_tanah ?? '' }}" name="luasTanah[]"
                                                    onkeypress="return onlyNumberKey(event)"
                                                    onchange="cekTotalLuasPecah(event)" required="">
                                            </div>
                                        </div>
                                        {{-- <div class="form-group-sm row">
                                    <label class="col-sm-4 col-form-label-sm">
                                        Luas Bangunan
                                        (M
                                        <sup>2</sup>)
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control form-control-sm"
                                            id="luasBangunan{{ $key+1 }}"
                                            value="{{ $objekPajak->t_luas_bangunan ?? '0' }}" name="luasBangunan[]"
                                            required="">
                                    </div>
                                </div> --}}
                                    </div>

                                    <div class="col-sm-6">
                                        <hr style="background-color: #dc3545; height: 2px;">
                                        <p style="font-weight: bold">DATA WAJIB PAJAK (BARU)</p>
                                        <hr>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">NIK</label>
                                            <div class="col-sm-8">
                                                <input type="hidden" id="idWP{{ $key + 1 }}"
                                                    value="{{ $pelayanan->wajibPajaks[$key]->t_id_wp ?? '' }}"
                                                    name="idWP[]">
                                                <input type="text" class="form-control form-control-sm masked-nik"
                                                    id="nikWP{{ $key + 1 }}"
                                                    value="{{ $pelayanan->wajibPajaks[$key]->t_nik_wp }}"
                                                    name="nikWP[]" required="">
                                            </div>
                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Nama</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control form-control-sm"
                                                    id="namaWP{{ $key + 1 }}"
                                                    value="{{ $pelayanan->wajibPajaks[$key]->t_nama_wp }}"
                                                    name="namaWP[]" required="">
                                            </div>
                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">RT</label>
                                            <div class="col-sm-2">
                                                <input type="text" class="form-control form-control-sm"
                                                    id="rtWP{{ $key + 1 }}"
                                                    value="{{ $pelayanan->wajibPajaks[$key]->t_rt_wp }}"
                                                    name="rtWP[]">
                                            </div>
                                            <label class="col-sm-2 col-form-label-sm">RW</label>
                                            <div class="col-sm-2">
                                                <input type="text" class="form-control form-control-sm"
                                                    id="rwWP{{ $key + 1 }}"
                                                    value="{{ $pelayanan->wajibPajaks[$key]->t_rw_wp }}"
                                                    name="rwWP[]">
                                            </div>
                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Jalan</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control form-control-sm"
                                                    id="jalanWP{{ $key + 1 }}"
                                                    value="{{ $pelayanan->wajibPajaks[$key]->t_jalan_wp }}"
                                                    name="jalanWP[]">
                                            </div>
                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control form-control-sm"
                                                    id="kelurahanWP{{ $key + 1 }}"
                                                    value="{{ $pelayanan->wajibPajaks[$key]->t_kelurahan_wp }}"
                                                    name="kelurahanWP[]">
                                            </div>
                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Kecamatan</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control form-control-sm"
                                                    id="kecamatanWP{{ $key + 1 }}"
                                                    value="{{ $pelayanan->wajibPajaks[$key]->t_kecamatan_wp ?? '' }}"
                                                    name="kecamatanWP[]">
                                            </div>
                                        </div>
                                        <div class="form-group-sm row">
                                            <label class="col-sm-4 col-form-label-sm">Kabupaten/Kota</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control form-control-sm"
                                                    id="kabupatenWP{{ $key + 1 }}"
                                                    value="{{ $objekPajak->t_kabupaten_wp ?? '' }}"
                                                    name="kabupatenWP[]">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ModalTunggakanPBB" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title">Daftar Tunggakan
                    PBB{{ $jenisLayanan->s_lunas_pbb == true ? ' Yang Wajib Di Lunasi' : '.' }}</h4>
                <input type="hidden" id="lunasPbb" name="lunasPbb"
                    value="{{ $jenisLayanan->s_lunas_pbb ?? '' }}">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="datatunggakan"></div>
            </div>
            <div class="modal-footer right">
                <button type="button" class="btn btn-outline-light" data-dismiss="modal"
                    onclick="tutupmodaltunggakan()">Tutup</button>
            </div>
        </div>
    </div>
</div>
@include('pelayanan.pbb.components.data-input.keterangan-pemohon')

@push('scripts')
    <script>
        var kodePropinsiKab = '{{ $kdKabkota }}';

        const kodeKecamatan = JSON.parse('@json($kecamatan)');
        const jenisTanahList = JSON.parse('@json($lookupItem1)');

        // if ('[]' != 'null') {
        //     const dataOP = JSON.parse('[]');
        //     var index = 1;
        //     if (dataOP.length > 0) {
        //         dataOP.forEach(e => {
        //             console.log(e);
        //             kelurahanCari(index, e.kodeKelurahan);
        //             index++;
        //         });
        //     }
        //     console.log('asd');
        // }

        $(document).ready(() => {
            @if (isset($pelayanan))
                let i = {{ count($pelayanan->objekPajaks) }};
                for (let index = 1; index <= i; index++) {
                    kelurahanCari(index, '{{ $pelayanan->OpLama->kd_kelurahan }}')
                }
            @endif
            setDati()

            $("#nopLama").val('');
            $("#rtOPLama").val('');
            $("#rwOPLama").val('');
            $("#jalanOPLama").val('');
            $("#blokNoOPLama").val('');
            $("#kecamatanOPLama").val('');
            $("#kelurahanOPLama").val('');
            $("#luasTanahLama").val('');
            $("#luasBangunanLama").val('');
            $("#nikWPLama").val('');
            $("#namaWPLama").val('');
            $("#rtWPLama").val('');
            $("#rwWPLama").val('');
            $("#jalanWPLama").val('');
            $("#kelurahanWPLama").val('');
            $("#kecamatanWPLama").val('');
            $("#kabupatenWPLama").val('');

            $("#nikWPLama").attr("readonly", true);
            $("#namaWPLama").attr("readonly", true);
            $("#rtWPLama").attr("readonly", true);
            $("#rwWPLama").attr("readonly", true);
            $("#jalanWPLama").attr("readonly", true);
            $("#kelurahanWPLama").attr("readonly", true);
            $("#kecamatanWPLama").attr("readonly", true);
            $("#kabupatenWPLama").attr("readonly", true);
            $("#statusLama").val(0);
        })

        function tambahNopBaru() {
            var html = '';
            var no = parseInt($("#counterNopBaru").val());
            html += '<div id="divNopBaru' + no +
                '" class="card card-danger card-outline" style="border-top:3px solid #dc3545;">' +
                '<div class="card-body">' +
                '<div class="row">' +
                '<div class="col-sm-12">' +
                '<div class="form-group-sm row">' +
                '<div class="col-sm-4">' +
                '<button type="button" class="btn btn-sm btn-danger" onclick="hapusDivNopBaru(' + no + ')">Hapus</button>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-sm-6">' +
                '<hr style="background-color: #dc3545; height: 2px;">' +
                '<p style="font-weight: bold">DATA OBJEK PAJAK (BARU)</p>' +
                '<hr>' +
                '<div class="form-group-sm row">' +
                '<label class="col-sm-4 col-form-label-sm">RT</label>' +
                '<div class="col-sm-2">' +
                '<input type="hidden" id="idOP' + no + '" name="idOP[]">' +
                '<input type="text" class="form-control form-control-sm" id="rtOP' + no + '" name="rtOP[]" value="' + $(
                    "#rtOPLama").val() + '">' +
                '</div>' +
                '<label class="col-sm-2 col-form-label-sm">RW</label>' +
                '<div class="col-sm-2">' +
                '<input type="text" class="form-control form-control-sm" id="rwOP' + no + '" name="rwOP[]" value="' + $(
                    "#rwOPLama").val() + '">' +
                '</div>' +
                '</div>' +
                '<div class="form-group-sm row">' +
                '<label class="col-sm-4 col-form-label-sm">Jalan</label>' +
                '<div class="col-sm-8">' +
                '<input type="text" class="form-control form-control-sm" id="jalanOP' + no + '" name="jalanOP[]" value="' +
                $("#jalanOPLama").val() + '">' +
                '</div>' +
                '</div>' +
                '<div class="form-group-sm row">' +
                '<label class="col-sm-4 col-form-label-sm">Blok/No/Kav</label>' +
                '<div class="col-sm-8">' +
                '<input type="text" class="form-control form-control-sm" id="blokNoOP' + no +
                '" name="blokNoOP[]" value="' + $("#blokNoOPLama").val() + '">' +
                '</div>' +
                '</div>' +
                '<div class="form-group-sm row">' +
                '<label class="col-sm-4 col-form-label-sm">Kecamatan</label>' +
                '<div class="col-sm-8">' +
                '<select class="form-control" id="kecamatanOP' + no + '" name="kecamatanOP[]" onChange="kelurahanCari(' +
                no + ', this)">' +
                '<option value="">Silahkan Pilih</option>';
            kodeKecamatan.forEach(e => {
                var selected = "";
                if ($("#kecamatanOPLama").val() != "") {
                    if (e.nm_kecamatan == $("#kecamatanOPLama").val()) {
                        selected = " selected";
                    }
                }
                html += '<option value=' + e.kd_kecamatan + '' + selected + '>' + e.kd_kecamatan + ' - ' + e
                    .nm_kecamatan + '</option>';
            });
            html += '</select>' +
                '</div>' +
                '</div>' +
                '<div class="form-group-sm row">' +
                '<label class="col-sm-4 col-form-label-sm">Kelurahan/Desa</label>' +
                '<div class="col-sm-8">' +
                '<select class="form-control" id="kelurahanOP' + no + '" name="kelurahanOP[]">' +
                '<option value="">Silahkan Pilih</option>' + '</select>' +
                '</div>' +
                '</div>' +
                '<div class="form-group-sm row">' +
                '<label class="col-sm-4 col-form-label-sm">Jenis Tanah <span class="text-danger">*</span></label>' +
                '<div class="col-sm-8">' +
                '<select class="form-control" name="jenisTanah[]" id="jenisTanah' + no + '">' +
                '<option value="">Silahkan Pilih</option>';
            jenisTanahList.forEach(e => {
                html += '<option value=' + e.kd_lookup_item + '>' + e.nm_lookup_item + '</option>';
            });
            html += '</select>' +
                '</div>' +
                '</div>' +
                '<div class="form-group-sm row">' +
                '<label class="col-sm-4 col-form-label-sm">Luas Tanah (M<sup>2</sup>)</label>' +
                '<div class="col-sm-8">' +
                '<input type="text" class="form-control form-control-sm" id="luasTanah' + no +
                '" name="luasTanah[]" onkeypress="return onlyNumberKey(event)" onchange="cekTotalLuasPecah(event)" required>' +
                '</div>' +
                '</div>';
            // html +=
            //     '<div class="form-group-sm row">' +
            //     '<label class="col-sm-4 col-form-label-sm">Luas Bangunan (M<sup>2</sup>)</label>' +
            //     '<div class="col-sm-8">' +
            //     '<input type="text" class="form-control form-control-sm" id="luasBangunan' + no + '" name="luasBangunan" required>' +
            //     '</div>' +
            //     '</div>';
            html +=
                '</div>' +
                '<div class="col-sm-6">' +
                '<hr style="background-color: #dc3545; height: 2px;">' +
                '<p style="font-weight: bold">DATA WAJIB PAJAK (BARU)</p>' +
                '<hr>' +
                '<div class="form-group-sm row">' +
                '<label class="col-sm-4 col-form-label-sm">NIK</label>' +
                '<div class="col-sm-8">' +
                '<input type="hidden" id="idWP' + no + '" name="idWP[]">' +
                '<input type="text" class="form-control form-control-sm masked-nik" id="nikWP' + no +
                '" name="nikWP[]" required>' +
                '</div>' +
                '</div>' +
                '<div class="form-group-sm row">' +
                '<label class="col-sm-4 col-form-label-sm">Nama</label>' +
                '<div class="col-sm-8">' +
                '<input type="text" class="form-control form-control-sm" id="namaWP' + no + '" name="namaWP[]" required>' +
                '</div>' +
                '</div>' +
                '<div class="form-group-sm row">' +
                '<label class="col-sm-4 col-form-label-sm">RT</label>' +
                '<div class="col-sm-2">' +
                '<input type="text" class="form-control form-control-sm masked-number-3" id="rtWP' + no +
                '" name="rtWP[]">' +
                '</div>' +
                '<label class="col-sm-2 col-form-label-sm">RW</label>' +
                '<div class="col-sm-2">' +
                '<input type="text" class="form-control form-control-sm masked-number-2" id="rwWP' + no +
                '" name="rwWP[]">' +
                '</div>' +
                '</div>' +
                '<div class="form-group-sm row">' +
                '<label class="col-sm-4 col-form-label-sm">Jalan</label>' +
                '<div class="col-sm-8">' +
                '<input type="text" class="form-control form-control-sm" id="jalanWP' + no + '" name="jalanWP[]">' +
                '</div>' +
                '</div>' +
                '<div class="form-group-sm row">' +
                '<label class="col-sm-4 col-form-label-sm">Kelurahan/Desa</label>' +
                '<div class="col-sm-8">' +
                '<input type="text" class="form-control form-control-sm" id="kelurahanWP' + no + '" name="kelurahanWP[]">' +
                '</div>' +
                '</div>' +
                '<div class="form-group-sm row">' +
                '<label class="col-sm-4 col-form-label-sm">Kecamatan</label>' +
                '<div class="col-sm-8">' +
                '<input type="text" class="form-control form-control-sm" id="kecamatanWP' + no + '" name="kecamatanWP[]">' +
                '</div>' +
                '</div>' +
                '<div class="form-group-sm row">' +
                '<label class="col-sm-4 col-form-label-sm">Kabupaten/Kota</label>' +
                '<div class="col-sm-8">' +
                '<input type="text" class="form-control form-control-sm" id="kabupatenWP' + no + '" name="kabupatenWP[]">' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';

            html += '<script>';
            html += '$(".masked-nop").mask(\'99.99.999.999.999.9999.9\');';
            html += '$(".masked-nik").mask(\'9999999999999999\');';
            html += '<' + '/script>';


            $("#dataNopAkanPecah").append(html);
            kelurahanCari(no, "", $("#kelurahanOPLama").val());

            no = no + 1;
            $("#counterNopBaru").val(no)
        }

        function hapusDivNopBaru(a) {
            $("#divNopBaru" + a).remove();
        }

        function cariNopLama() {
            $("#nopLama").val('');
            $("#rtOPLama").val('');
            $("#rwOPLama").val('');
            $("#jalanOPLama").val('');
            $("#blokNoOPLama").val('');
            $("#kecamatanOPLama").val('');
            $("#kelurahanOPLama").val('');
            $("#luasTanahLama").val('');
            $("#luasBangunanLama").val('');
            $("#nikWPLama").val('');
            $("#namaWPLama").val('');
            $("#rtWPLama").val('');
            $("#rwWPLama").val('');
            $("#jalanWPLama").val('');
            $("#kelurahanWPLama").val('');
            $("#kecamatanWPLama").val('');
            $("#kabupatenWPLama").val('');

            $.get("/dat-objek-pajak", {
                    nop: $("#nopCariLama").val()
                })
                .then(function(data) {
                    // var data = res.data;

                    if (!data.nop) {
                        alert("NOP " + $("#nopCariLama").val() + " Tidak ditemukan!");

                    } else {
                        $("#nopLama").val(data.nop);
                        $("#rtOPLama").val(data.rt_op);
                        $("#rwOPLama").val(data.rw_op);
                        $("#jalanOPLama").val(data.jalan_op);
                        $("#blokNoOPLama").val(data.blok_kav_no_op);
                        $("#kecamatanOPLama").val(data.nm_kecamatan);
                        $("#kelurahanOPLama").val(data.nm_kelurahan);
                        $("#luasTanahLama").val(data.total_luas_bumi);
                        $("#luasBangunanLama").val(data.total_luas_bng);
                        $("#nikWPLama").val(data.subjek_pajak.subjek_pajak_id);
                        $("#namaWPLama").val(data.subjek_pajak.nm_wp);
                        $("#rtWPLama").val(data.subjek_pajak.rt_wp);
                        $("#rwWPLama").val(data.subjek_pajak.rw_wp);
                        $("#jalanWPLama").val(data.subjek_pajak.jalan_wp);
                        $("#kelurahanWPLama").val(data.subjek_pajak.kelurahan_wp);
                        $("#kecamatanWPLama").val(data.subjek_pajak.kecamatanWP);
                        $("#kabupatenWPLama").val(data.subjek_pajak.kota_wp);

                        $("#nikWPLama").attr("readonly", true);
                        $("#namaWPLama").attr("readonly", true);
                        $("#rtWPLama").attr("readonly", true);
                        $("#rwWPLama").attr("readonly", true);
                        $("#jalanWPLama").attr("readonly", true);
                        $("#kelurahanWPLama").attr("readonly", true);
                        $("#kecamatanWPLama").attr("readonly", true);
                        $("#kabupatenWPLama").attr("readonly", true);
                        $("#statusLama").val(0);
                        // sudahAjukanPermohonan();
                    }
                })
        }

        function kelurahanCari(a, b, c) {
            $.get('/kelurahan/getkelurahan-bykecamatan/' + $("#kecamatanOP" + a).val()).then(function(response) {
                let kelurahan = response.data;

                if (kelurahan) {
                    $("#kelurahanOP" + a).find('option').remove().end().append(
                        '<option value="">Silahkan Pilih</option>');
                    kelurahan.forEach((kel) => {
                        var selected = "";
                        if (b == kel.kd_kelurahan) {
                            selected = " selected";
                        }
                        if (c == kel.nm_kelurahan) {
                            selected = " selected";
                        }
                        $("#kelurahanOP" + a).append('<option value="' + kel.kd_kelurahan + '"' + selected +
                            '>' + kel.kd_kelurahan + ' - ' + kel.nm_kelurahan + '</option>');
                    })
                }
            })
        }

        function cekTotalLuasPecah(event) {
            let arr = $("input[name='luasTanah[]']").map(function() {
                return parseInt(this.value);
            }).get()
            let ltTot = arr.reduce((lt, a) => lt + a, 0)

            if ($("#luasTanahLama").val() <= ltTot) {
                $("input[name='luasTanah[]']").val('');
                alert("Total luas tanah yang dipecah harus kurang dari objek asal");
            }
        }

        function setDati() {
            let dati = '{{ substr($kdKabkota, 0, 2) . '.' . substr($kdKabkota, 2) }}';

            @if (isset($pelayanan->opLama))
                dati = '{{ $pelayanan->opLama->nopFormated }}'
            @endif
            console.log(dati);
            $('#nopCariLama').val(dati)
        }


        $('#cariNopLama').click(function() {
            // getDataPbb();
            getTunggakanSppt();
        });

        function getTunggakanSppt() {
            let currentUrl = "/pelayanan/cektunggakanpbb"

            $.ajax({
                url: currentUrl,
                type: 'POST',
                data: {
                    nop: $('#nopCariLama').val()
                }
            }).then(function(response) {
                var aa = response;
                console.log(response);
                if (aa.PBB_YG_HARUS_DIBAYAR_SPPT != '' && aa.datatunggakan != 3) {
                    jQuery('#ModalTunggakanPBB').modal('show');
                    $('#datatunggakan').html(aa.datatunggakan);
                    if ($("#lunasPbb").val() == 1) {
                        $('#btnSimpanDraft').attr('disabled', 'disabled');
                        cariNopLama();
                    } else {
                        cariNopLama();
                    }
                } else if (aa.PBB_YG_HARUS_DIBAYAR_SPPT == '' && aa.datatunggakan != 3) {
                    alert('Tidak Ada Tunggakan');
                    cariNopLama();
                    $("#btnSimpanDraft").removeAttr('disabled');
                }
            });
        }

        function tutupmodaltunggakan() {
            jQuery('#ModalTunggakanPBB').modal('hide');
        }

        function openSubjekLama() {
            $("#nikWPLama").removeAttr("readonly");
            $("#namaWPLama").removeAttr("readonly");
            $("#rtWPLama").removeAttr("readonly");
            $("#rwWPLama").removeAttr("readonly");
            $("#jalanWPLama").removeAttr("readonly");
            $("#kelurahanWPLama").removeAttr("readonly");
            $("#kecamatanWPLama").removeAttr("readonly");
            $("#kabupatenWPLama").removeAttr("readonly");
            $("#statusLama").val(1);
        }
    </script>
@endpush
