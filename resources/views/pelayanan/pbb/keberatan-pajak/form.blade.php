{{-- </div>
</div> --}}
@include('pelayanan.pbb.components.data-input.data-pbb')
{{-- <h4 class="mt-4 mb-2">Data PBB</h4> --}}
<div class="card card-danger card-outline" style="border-top:3px solid #dc3545;">
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <hr style="background-color: #dc3545; height: 2px;">
                <p style="font-weight: bold">DATA OBJEK PAJAK (Baru)</p>
                <hr>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">RT<span class="float-right">:</span></label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm datasppt" id="t_rt_op_lama" name="t_rt_op_lama" />
                    </div>
                    <label class="col-sm-2 col-form-label-sm">RW<span class="float-right">:</span></label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm datasppt" id="t_rw_op_lama" name="t_rw_op_lama" />
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Jalan<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm datasppt" id="t_jalan_op_lama"/>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Blok/No/Kav<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm datasppt" id="kd_blok_kav_lama" name="kd_blok_kav_lama"/>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm datasppt" id="t_kelurahan_op_lama" name="t_kelurahan_op_lama"/>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kecamatan<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm datasppt" id="t_kecamatan_op_lama" name="t_kecamatan_op_lama"/>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Luas Tanah
                        (M<sup>2</sup>)<span class="float-right">:</span></label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm datasppt" id="t_luas_tanah_lama"/>
                    </div>
                    <label class="col-sm-3 col-form-label-sm">Luas Bng. (M<sup>2</sup>)<span
                            class="float-right">:</span></label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control form-control-sm datasppt" id="t_luas_bangunan_lama" name="t_luas_bangunan_lama"/>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Pajak<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm datasppt" id="t_pajak_pbb" name="t_pajak_pbb"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <hr style="background-color: #dc3545; height: 2px;">
                <p style="font-weight: bold">DATA WAJIB PAJAK (Baru)</p>
                <hr>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">NIK<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm datasppt" id="t_nik_wp_lama" nama="t_nik_wp_lama"/>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Nama<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm datasppt" id="t_nama_wp_lama" name="t_nama_wp_lama"/>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">RT<span class="float-right">:</span></label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm datasppt" id="t_rt_wp_lama" name="t_rt_wp_lama"/>
                    </div>
                    <label class="col-sm-2 col-form-label-sm">RW<span class="float-right">:</span></label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm datasppt" id="t_rw_wp_lama" name="t_rw_wp_lama"/>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Jalan<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm datasppt" id="t_jalan_wp_lama" name="t_jalan_wp_lama"/>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Blok/No/Kav<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm datasppt" id="kd_blok_kav_wp" name="kd_blok_kav_wp"/>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kelurahan/Desa<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm datasppt" id="t_kelurahan_wp_lama" name="t_kelurahan_wp_lama"/>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kecamatan<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm datasppt" id="t_kecamatan_wp_lama" name="t_kecamatan_wp_lama"/>
                    </div>
                </div>
                <div class="form-group-sm row">
                    <label class="col-sm-4 col-form-label-sm">Kabupaten/Kota<span class="float-right">:</span></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm datasppt" id="t_kabupaten_wp_lama" name="t_kabupaten_wp_lama"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('pelayanan.pbb.components.data-input.keterangan-pemohon')
