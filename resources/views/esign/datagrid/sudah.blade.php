<div data-th-fragment="tabpanel" class="tab-pane" id="navtab2" role="tabpanel">
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-bordered table-striped table-hover" id="datagrid2">
                <thead>
                    <tr>
                        <th style="background-color: #1fc8e3" class="text-center">No</th>
                        <th style="background-color: #1fc8e3" class="text-center">#</th>
                        <th style="background-color: #1fc8e3" class="text-center">Nama Pelayanan</th>
                        <th style="background-color: #1fc8e3" class="text-center">Tgl. Permohonan</th>
                        <th style="background-color: #1fc8e3" class="text-center">No. Pelayanan</th>
                        <th style="background-color: #1fc8e3" class="text-center">Nama Pemohon</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>
                            <select class="form-control form-control-sm" id="filter-idJenisPelayanan2">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($jenisPelayanan as $pelayanan)
                                    <option value="{{ $pelayanan->s_id_jenis_pelayanan }}">
                                        {{ $pelayanan->s_nama_jenis_pelayanan }}</option>
                                @endforeach
                            </select>
                        </th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-tglPelayanan2">
                        </th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-noPelayanan2">
                        </th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-namaPemohon2">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="8"> Tidak ada data.</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-12 pagination-footer"></div>
    </div>
</div>

@push('scripts')
    <script th:inline="javascript" data-th-fragment="js">
        $('#filter-tglPelayanan2').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-tglPelayanan2').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search2();
        });

        $('#filter-tglPelayanan2').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            search2();
        });

        $("#filter-idJenisPajak1, #filter-idJenisPelayanan2").change(function() {
            search2();
        });

        $("#filter-noPelayanan2, #filter-namaPemohon2, #filter-nikPemohon2").keyup(function() {
            search2();
        });

        var datatables2 = datagrid({
            url: '/esign-tte/datagrid-sudah',
            table: "#datagrid2",
            columns: [{
                    class: "text-center"
                },
                {
                    class: ""
                },
                {
                    class: "text-center"
                },
                {
                    class: "text-center"
                },
                {
                    class: "text-center"
                },
                {
                    class: ""
                },
            ],
            orders: [{
                    sortable: false
                },
                {
                    sortable: false,
                    name: "idJenisPajak"
                },
                {
                    sortable: false,
                    name: "idJenisPelayanan"
                },
                {
                    sortable: false,
                    name: "tglPelayanan"
                },
                {
                    sortable: false,
                    name: "noPelayanan"
                },
                {
                    sortable: false,
                    name: "nikPemohon"
                },
                {
                    sortable: false,
                    name: "namaPemohon"
                },
                {
                    sortable: false
                },
            ]

        });

        function search2() {
            datatables2.setFilters({
                tglPelayanan: $("#filter-tglPelayanan2").val(),
                noPelayanan: $("#filter-noPelayanan2").val(),
                idJenisPelayanan: $("#filter-idJenisPelayanan2").val(),
                namaPemohon: $("#filter-namaPemohon2").val(),
                nikPemohon: $("#filter-nikPemohon2").val(),
            });
            datatables2.reload();
        }

        search2();

        function printdaftarhasilpelayananbelum(type) {
            window.open('/report/esign-tte/printdaftarhasilpelayananbelum' + type +
                '?tglPelayanan=' + $("#filter-tglPelayanan2").val() +
                '&noPelayanan=' + $("#filter-noPelayanan2").val() +
                '&namaPemohon=' + $("#filter-namaPemohon2").val() +
                '&idJenisPelayanan=' + $("#filter-idJenisPelayanan2").val() +
                '&nikPemohon=' + $("#filter-nikPemohon2").val() +
                '');
        }
    </script>
@endpush
