<div data-th-fragment="tabpanel" class="tab-pane fade active show" id="navtab1" role="tabpanel">
    <div class="row">
        <div class="col-12" style="margin-bottom: 10px;text-align:right">
            <button type="button" class="btn btn-primary btn-sm" onclick="modalTte()"><i class="fas fa-pen"></i> Proses
                TTE</button>
        </div>
    </div>
    <form method="post" action="{{ url('esign-tte/create') }}" id="formsubmit">
        @csrf
        {{-- target="_blank" --}}
        <div class="row">
            <div class="col-12 table-responsive">
                <table class="table table-bordered table-striped table-hover" id="datagrid1">
                    <thead>
                        <tr>
                            <th style="background-color: #1fc8e3" class="text-center">No</th>
                            <th style="background-color: #1fc8e3" class="text-center"><input type="checkbox"
                                    id="selectAll" style="transform: scale(1.3);"></th>
                            <th style="background-color: #1fc8e3" class="text-center">Tgl. Penetapan</th>
                            <th style="background-color: #1fc8e3" class="text-center">Nip Perekam</th>
                            <th style="background-color: #1fc8e3" class="text-center">Nop</th>
                            <th style="background-color: #1fc8e3" class="text-center">Tahun Pajak</th>
                            <th style="background-color: #1fc8e3" class="text-center">User Penetapan</th>
                        </tr>
                        <tr>
                            <th></th>
                            <th></th>
                            <th><input class="form-control form-control-sm" onchange="search2()" type="text"
                                    id="filter-tglPenetapan"></th>
                            <th><input class="form-control form-control-sm" onchange="search2()" type="text"
                                    id="filter-nip"></th>
                            <th><input class="form-control form-control-sm" onchange="search2()" type="text"
                                    id="filter-nop">
                            </th>
                            <th><input class="form-control form-control-sm" onchange="search2()" type="text"
                                    id="filter-tahun">
                            </th>
                            <th><input class="form-control form-control-sm" onchange="search2()" type="text"
                                    id="filter-user">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center" colspan="8"> Tidak ada data.</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-12 pagination-footer"></div>
        </div>
</div>

<div class="modal fade" id="modal-confirm">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white"><i class="fas fa-exclamation-circle"></i> Proses TTE
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5 class="text-center">Silahkan Lengkapi data dibawah ini:</h5>
                <div class="row mt-2 mb-1">
                    <div class="col-sm-4">Pejabat</div>
                    <div class="col-sm-8">
                        <select class="form-control form-control-sm" name="s_id_pegawai" id="s_id_pegawai">
                            @foreach ($pegawais as $item)
                                <option value="{{ $item->s_id_pegawai }}">{{ $item->s_nama_pegawai }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">Passphrase</div>
                    <div class="col-sm-8">
                        <input type="text" required class="form-control form-control-sm" id="passphrase"
                            name="passphrase">
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i>
                    Batal</button>
                <button type="submit" id="submitbutton" class="btn btn-success"><i class="fa fa-check"></i>
                    Simpan</button>
            </div>
        </div>
    </div>
</div>
</form>

@push('scripts')
    <script th:inline="javascript" data-th-fragment="js">
        $(document).ready(function() {
            let selectedRowIds = [];
            $('#selectAll').click(function(event) {
                if (this.checked) {
                    $(':checkbox').each(function() {
                        this.checked = true;
                        var index = $.inArray($(this).val(), selectedRowIds);
                        if (index === -1 && $(this).val() != "on") {
                            selectedRowIds.push($(this).val());
                        }
                    });

                } else {
                    $(':checkbox').each(function() {
                        this.checked = false;
                        selectedRowIds.splice($(this).val(), 1);
                    });
                }
                console.log(selectedRowIds);
            });

            $('#datagrid-table tbody').on('change', 'input[type="checkbox"]', (e) => {
                var index = $.inArray($(e.target).val(), selectedRowIds);

                if (index === -1) {
                    selectedRowIds.push($(e.target).val());
                } else {
                    selectedRowIds.splice(index, 1);
                }

                updateSlectionRows();
                console.log(selectedRowIds);
            });

            $('#datagrid-table tbody').on('DOMSubtreeModified', () => {

                $('tbody input[type="checkbox"]').each(function() {
                    if ($.inArray($(this).val(), selectedRowIds) !== -1) {
                        $(this).prop('checked', true);
                    }
                    updateSlectionRows();
                });

            });

            function updateSlectionRows() {
                var $chkbox_all = $('tbody input[type="checkbox"]', '#datagrid-table');
                var $chkbox_checked = $('tbody input[type="checkbox"]:checked');
                var chkbox_select_all = $('thead input[id="selectAll"]', '#datagrid-table').get(0);

                if ($chkbox_checked.length === 0) {
                    chkbox_select_all.checked = false;
                    if ('indeterminate' in chkbox_select_all) {
                        chkbox_select_all.indeterminate = false;
                    }
                } else if ($chkbox_checked.length === $chkbox_all.length) {
                    chkbox_select_all.checked = true;
                    if ('indeterminate' in chkbox_select_all) {
                        chkbox_select_all.indeterminate = false;
                    }
                } else {
                    chkbox_select_all.checked = true;
                    if ('indeterminate' in chkbox_select_all) {
                        chkbox_select_all.indeterminate = true;
                    }
                }
            }

            document.addEventListener('DOMContentLoaded', () => {
                document.getElementById('selectAll').checked = false;
            });

        });

        function modalTte() {
            $('#modal-confirm').modal('show');
        }

        $('#filter-tglPelayanan1').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-tglPelayanan1').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search1();
        });

        $('#filter-tglPelayanan1').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            search1();
        });

        $("#filter-idJenisPajak1, #filter-idJenisPelayanan1").change(function() {
            search1();
        });

        $("#filter-noPelayanan1, #filter-namaPemohon1, #filter-nikPemohon1").keyup(function() {
            search1();
        });

        var datatables1 = datagrid({
            url: '/esign-tte/datagrid-belum',
            table: "#datagrid1",
            columns: [{
                    class: "text-center"
                },
                {
                    class: "text-center"
                },
                {
                    class: "text-center"
                },
                {
                    class: "text-center"
                },
                {
                    class: "text-center"
                },
                {
                    class: ""
                },
            ],
            orders: [{
                    sortable: false
                },
                {
                    sortable: false,
                    name: "idJenisPajak"
                },
                {
                    sortable: false,
                    name: "idJenisPelayanan"
                },
                {
                    sortable: false,
                    name: "tglPelayanan"
                },
                {
                    sortable: false,
                    name: "noPelayanan"
                },
                {
                    sortable: false,
                    name: "nikPemohon"
                },
                {
                    sortable: false,
                    name: "namaPemohon"
                },
                {
                    sortable: false
                },
            ]

        });

        function search1() {
            datatables1.setFilters({
                idJenisPajak: $("#filter-idJenisPajak1").val(),
                tglPelayanan: $("#filter-tglPelayanan1").val(),
                noPelayanan: $("#filter-noPelayanan1").val(),
                idJenisPelayanan: $("#filter-idJenisPelayanan1").val(),
                namaPemohon: $("#filter-namaPemohon1").val(),
                nikPemohon: $("#filter-nikPemohon1").val(),
            });
            datatables1.reload();
        }

        search1();

        function printdaftarhasilpelayananbelum(type) {
            window.open('/report/esign-tte/printdaftarhasilpelayananbelum' + type +
                '?idJenisPajak=' + $("#filter-idJenisPajak1").val() +
                '&tglPelayanan=' + $("#filter-tglPelayanan1").val() +
                '&noPelayanan=' + $("#filter-noPelayanan1").val() +
                '&namaPemohon=' + $("#filter-namaPemohon1").val() +
                '&idJenisPelayanan=' + $("#filter-idJenisPelayanan1").val() +
                '&nikPemohon=' + $("#filter-nikPemohon1").val() +
                '');
        }
    </script>
@endpush
