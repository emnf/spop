@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Setting Pegawai
@endsection

@section('content')
    <section class="content">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Setting</a></li>
                            <li class="breadcrumb-item active">Grid</li>
                        </ol>
                    </div><!-- /.col -->
                    <div class="col-12">
                        <div class="info-box" id="background2"
                            style="background: url({{ asset('/asset/images/bgpelayanan.gif') }}) #fff no-repeat center center;
                            -moz-background-size: cover;
                            -o-background-size: cover;
                            background-size: cover;">
                            <div class="col-sm-3">
                                <div class="description-block" style="float: left;">
                                    <h1 class="description-header">SETTING PEGAWAI</h1>
                                </div>
                                <!-- /.description-block -->
                            </div>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-outline card-info">
                            <div class="card-header">
                                <div class="col-sm-12" style="margin-bottom: 10px;">
                                    Form Setting Pegawai [Edit]
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <form class="form-horizontal form-validation" method="post"
                                action="{{ route('setting-jenis-pelayanan.update', $data->s_id_jenis_pelayanan) }}">
                                @csrf
                                @method('PUT')
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_id_jenis_pelayanan">Jenis
                                            Pajak</label>
                                        <div class="col-sm-4">
                                            <input id="s_id_jenis_pelayanan" name="s_id_jenis_pelayanan" type="hidden"
                                                value="{{ $data->s_id_jenis_pelayanan }}">
                                            <select class="form-control @error('s_id_jenis_pajak') is-invalid @enderror"
                                                id="s_id_jenis_pajak" name="s_id_jenis_pajak" required>
                                                <option value="">Silahkan Pilih</option>
                                                @foreach ($jenisPajak as $row)
                                                    <option
                                                        {{ $row['s_id_jenis_pajak'] == $data->s_id_jenis_pajak ? 'selected' : '' }}
                                                        value="{{ $row['s_id_jenis_pajak'] }}">
                                                        {{ $row['s_nama_jenis_pajak'] }}</option>
                                                @endforeach
                                            </select>
                                            @error('s_id_jenis_pajak')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_nama_jenis_pelayanan">Nama Jenis
                                            Pelayanan</label>
                                        <div class="col-sm-4">
                                            <input
                                                class="form-control @error('s_nama_jenis_pelayanan') is-invalid @enderror"
                                                value="{{ old('s_nama_jenis_pelayanan') ?? $data->s_nama_jenis_pelayanan }}"
                                                id="s_nama_jenis_pelayanan" name="s_nama_jenis_pelayanan" type="text"
                                                required>
                                            @error('s_nama_jenis_pelayanan')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_lunas_pbb" required>Lunas PBB</label>
                                        <div class="col-sm-4">
                                            <select class="form-control @error('s_lunas_pbb') is-invalid @enderror"
                                                id="s_lunas_pbb" name="s_lunas_pbb">
                                                <option {{ $data->s_lunas_pbb == true ? 'selected' : '' }} value="true">
                                                    Ya</option>
                                                <option {{ $data->s_lunas_pbb == false ? 'selected' : '' }} value="false">
                                                    Tidak</option>
                                            </select>
                                            @error('s_lunas_pbb')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    {{-- {{ dd($data); }} --}}
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_active" required>Active</label>
                                        <div class="col-sm-4">
                                            <select class="form-control @error('s_active') is-invalid @enderror"
                                                id="s_active" name="s_active">
                                                <option {{ $data->s_active == true ? 'selected' : '' }} value="true">Ya
                                                </option>
                                                <option {{ $data->s_active == false ? 'selected' : '' }} value="false">
                                                    Tidak</option>
                                            </select>
                                            @error('s_active')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_keterangan_jenis_pelayanan">Keterangan
                                            Jenis Pelayanan</label>
                                        <div class="col-sm-4">
                                            <textarea class="form-control @error('s_keterangan_jenis_pelayanan') is-invalid @enderror"
                                                id="s_keterangan_jenis_pelayanan" name="s_keterangan_jenis_pelayanan" rows="3">{{ old('s_keterangan_jenis_pelayanan') ?? $data->s_keterangan_jenis_pelayanan }}</textarea>
                                            @error('s_keterangan_jenis_pelayanan')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_order">Order</label>
                                        <div class="col-sm-4">
                                            <input class="form-control @error('s_order') is-invalid @enderror"
                                                id="s_order" name="s_order" type="text" readonly
                                                value="{{ $data->s_order }}">
                                            @error('s_order')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-info" type="submit">SIMPAN</button>
                                    <a class="btn btn-danger btn-default float-right"
                                        href="{{ route('setting-jenis-pelayanan') }}">BATAL</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
