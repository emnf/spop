@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Setting Jenis Pelayanan
@endsection

@section('content')
    <section class="content">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Setting</a></li>
                            <li class="breadcrumb-item active">Grid</li>
                        </ol>
                    </div><!-- /.col -->
                    <div class="col-12">
                        <div class="info-box" id="background2">
                            <div class="col-sm-3">
                                <div class="description-block" style="float: left;">
                                    <h1 class="description-header">SETTING JENIS PELAYANAN</h1>
                                </div>
                                <!-- /.description-block -->
                            </div>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-outline card-info">
                            <div class="card-header">
                                <div class="col-sm-12" style="margin-bottom: 10px;">
                                    <a href="{{ route('setting-jenis-pelayanan.tambah') }}"
                                        class="btn btn-info btn-sm"><span class="fas fa-plus-square"></span> Tambah</a>
                                    <button class="btn btn-danger btn-sm" id="btnCetakPDF"><span
                                            class="fas fa-file-pdf"></span> &nbsp;PDF</button>
                                    <button class="btn btn-success btn-sm" id="btnCetakExcel"><span
                                            class="fas fa-file-excel"></span> &nbsp;XLS</button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive">
                                <table class="table table-bordered table-striped table-hover" id="datagrid-table">
                                    <thead>
                                        <tr>
                                            <th style="background-color: #1fc8e3" class="text-center">No</th>
                                            <th style="background-color: #1fc8e3" class="text-center">Nama Jenis Pajak</th>
                                            <th style="background-color: #1fc8e3" class="text-center">Nama Jenis Pelayanan
                                            </th>
                                            <th style="background-color: #1fc8e3" class="text-center">Lunas PBB</th>
                                            <th style="background-color: #1fc8e3" class="text-center">Active</th>
                                            <th style="background-color: #1fc8e3" class="text-center">Keterangan Jenis
                                                Pelayanan</th>
                                            <th style="background-color: #1fc8e3" class="text-center">Order</th>
                                            <th style="background-color: #1fc8e3" class="text-center">Action</th>
                                        </tr>
                                        <tr>
                                            <th></th>
                                            <th>
                                                <select class="form-control form-control-sm" id="filter-s_id_jenis_pajak">
                                                    <option value="">Silahkan Pilih</option>
                                                    @foreach ($jenisPajak as $row)
                                                        <option value="{{ $row['s_id_jenis_pajak'] }}">
                                                            {{ $row['s_nama_jenis_pajak'] }}</option>
                                                    @endforeach
                                                </select>
                                            </th>
                                            <th>
                                                <select class="form-control form-control-sm" id="filter-idJenisPelayanan">
                                                    <option value="">Silahkan Pilih</option>
                                                    @foreach ($jenispelayanan as $jp)
                                                        <option value="{{ $jp->s_id_jenis_pelayanan }}">
                                                            {{ $jp->s_nama_jenis_pelayanan }}</option>
                                                    @endforeach
                                                </select>
                                            </th>
                                            <th>
                                                <select class="form-control form-control-sm" id="filter-s_lunas_pbb">
                                                    <option value="">Pilih</option>
                                                    <option value="true">Ya</option>
                                                    <option value="false">Tidak</option>
                                                </select>
                                            </th>
                                            <th>
                                                <select class="form-control form-control-sm" id="filter-s_active">
                                                    <option value="">Pilih</option>
                                                    <option value="true">Ya</option>
                                                    <option value="false">Tidak</option>
                                                </select>
                                            </th>
                                            <th></th>
                                            <th><input class="form-control form-control-sm" id="filter-s_order"
                                                    type="text" /></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center" colspan="8"> Tidak ada data.</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer clearfix pagination-footer">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
@push('scripts')
    <script type="text/javascript">
        var datatables = datagrid({
            url: '/setting-jenis-pelayanan/datagrid',
            table: "#datagrid-table",
            serverSide: true,
            columns: [{
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
            ],
            orders: [{
                    sortable: false
                },
                {
                    sortable: true,
                    name: "idJenisPelayanan"
                },
                {
                    sortable: true,
                    name: "s_id_jenis_pajak"
                },
                {
                    sortable: true,
                    name: "s_lunas_pbb"
                },
                {
                    sortable: true,
                    name: "s_active"
                },
                {
                    sortable: true,
                    name: "s_keterangan_jenis_pelayanan"
                },
                {
                    sortable: true,
                    name: "s_order"
                },
                {
                    sortable: false
                }
            ],
            action: [{
                    name: 'edit',
                    btnClass: 'btn btn-outline-primary btn-sm mx-1',
                    btnText: '<i class="fas fa-pen"></i>'
                },
                {
                    name: 'delete',
                    btnClass: 'btn btn-outline-danger btn-sm mx-1',
                    btnText: '<i class="fas fa-trash"></i>'
                }
            ]

        });

        $("#filter-s_nama_jenis_pelayanan, #filter-s_order").keyup(function() {
            search();
        });

        $("#filter-s_id_jenis_pajak, #filter-s_active, #filter-s_lunas_pbb, #filter-idJenisPelayanan").change(function() {
            search();
        });

        function search() {
            datatables.setpageNumber(0);
            datatables.setFilters({
                s_nama_jenis_pelayanan: $("#filter-s_nama_jenis_pelayanan").val(),
                s_id_jenis_pajak: $("#filter-s_id_jenis_pajak").val(),
                id_jenis_pelayanan: $("#filter-idJenisPelayanan").val(),
                s_lunas_pbb: $("#filter-s_lunas_pbb").val(),
                s_active: $("#filter-s_active").val(),
                s_order: $("#filter-s_order").val()
            });
            datatables.reload();
        }
        search();

        function showDeleteDialog(a) {
            Swal.fire({
                title: 'Hapus Data',
                text: "Apa anda Yakin ingin menghapus ?",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Tidak',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '/setting-jenis-pelayanan/delete/' + a,
                        type: 'delete',
                        dataType: "JSON",
                        data: {
                            s_id_jenis_pelayanan: a
                        },
                        success: function(request, error) {
                            if (request == 'success') {
                                Toast.fire({
                                    icon: 'success',
                                    title: 'Data Berhasil Dihapus'
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: 'Data Gagal Dihapus'
                                });
                            }
                            datatables.reload();
                        }
                    });
                }
            })
        }

        function cetakData(a) {
            window.open(
                `{{ url('setting/pegawai/cetak-daftar') }}?type-cetak=${a}`
            );
        }
    </script>
@endPush
