@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Setting FAQ
@endsection

@section('content')
    <section class="content">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('setting-faq') }}">Setting</a></li>
                            <li class="breadcrumb-item active">Grid</li>
                        </ol>
                    </div>
                    <div class="col-12">
                        <div class="info-box" id="background2">
                            <div class="col-sm-3">
                                <div class="description-block" style="float: left;">
                                    <h1 class="description-header">SETTING FAQ</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-outline card-info">
                            <div class="card-header">
                                <div class="col-sm-12" style="margin-bottom: 10px;">
                                    Form Setting FAQ [ {{ isset($s_id_faq) ? 'Edit' : 'Tambah' }} ]
                                </div>
                            </div>
                            <form class="form-horizontal form-validation" method="POST"
                                action="{{ route('setting-faq.store') }} ">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group row">
                                        <input id="s_id_faq" name="s_id_faq" type="hidden" value="{{ $s_id_faq ?? '' }}">
                                        <label class="col-sm-2 col-form-label" for="s_pertanyaan">Pertanyaan</label>
                                        <div class="col-sm-4">
                                            <textarea class="form-control @error('s_pertanyaan') is-invalid @enderror" id="s_pertanyaan" name="s_pertanyaan"
                                                required>{{ $s_pertanyaan ?? '' }}</textarea>
                                            @error('s_pertanyaan')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_jawaban">Jawaban</label>
                                        <div class="col-sm-4">
                                            <textarea class="form-control @error('s_jawaban') is-invalid @enderror" id="s_jawaban" name="s_jawaban" required>{{ $s_jawaban ?? '' }}</textarea>
                                            @error('s_jawaban')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <button class="btn btn-info" type="submit">SIMPAN</button>
                                        <a class="btn btn-danger btn-default float-right"
                                            href="{{ route('setting-faq') }}">BATAL</a>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    @endsection
    @push('scripts')
        <script type="text/javascript"></script>
    @endPush
