@extends('layouts.app')

@section('title')
    Kepoin || Pelacakan Dokumen
@endsection

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a th:href="@{/tracking}">Home</a></li>
                        <li class="breadcrumb-item active">Pelacakan</li>
                    </ol>
                </div><!-- /.col -->

            </div><!-- /.container-fluid -->
            <div class="alert alert-dismissible alert_customs_success d-none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-check"></i> Success!</h5>
                <span th:text="${success}"></span>
            </div>
            <div class="alert alert-dismissible alert_customs_danger d-none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-exclamation-triangle"></i> Gagal!</h5>
                <span th:text="${warning}"></span>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-danger card-outline" style="border-top:3px solid #dc3545;">
                        <div class="card-header">
                            <h3 class="card-title">Data Pengajuan</h3>
                            <div class="card-tools">
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-2">
                            <table class="table table-hover table-bordered table-striped" id="tracking-table"
                                style="font-size: 10pt;">
                                <thead>
                                    <tr>
                                        <th style="width: 10px; background-color: #1fc8e3">No</th>
                                        <th style="width: 10px; background-color: #1fc8e3">Jenis Pajak</th>
                                        <th style="width: 10px; background-color: #1fc8e3">Jenis Layanan</th>
                                        <th style="width: 10px; background-color: #1fc8e3">Tanggal Permohonan</th>
                                        <th style="width: 10px; background-color: #1fc8e3">No Pelayanan</th>
                                        <th style="width: 10px; background-color: #1fc8e3">NIK</th>
                                        <th style="width: 10px; background-color: #1fc8e3">Nama Pemohon</th>
                                        <th style="width: 10px; background-color: #1fc8e3">Status</th>
                                        <th style="width: 10px; background-color: #1fc8e3">Action</th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        {{-- <th>
                                        <select class="form-control form-control-sm" id="filter-idJenisPajak">
                                            <option value="">Silahkan Pilih</option>
                                            @foreach ($jenispajak as $jp)
                                                <option value="{{ $jp->s_id_jenis_pajak }}">{{ $jp->s_nama_jenis_pajak }}</option>
                                            @endforeach
                                        </select>
                                    </th> --}}
                                        <th>
                                            <select class="form-control form-control-sm" id="filter-idJenisPelayanan">
                                                <option value="">Silahkan Pilih</option>
                                                @foreach ($jenispelayanan as $jp)
                                                    <option value="{{ $jp->s_id_jenis_pelayanan }}">
                                                        {{ $jp->s_nama_jenis_pelayanan }}</option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th><input class="form-control form-control-sm" id="filter-tglPelayanan"
                                                readonly="true" /></th>
                                        <th><input class="form-control form-control-sm" id="filter-noPelayanan" />
                                        </th>
                                        <th><input class="form-control form-control-sm masked-nik" id="filter-nikPemohon" />
                                        </th>
                                        <th><input class="form-control form-control-sm" id="filter-namaPemohon" />
                                        </th>
                                        <th>
                                            <select class="form-control form-control-sm" id="filter-status">
                                                <option value="">Silahkan Pilih</option>
                                                <option value="1">Draft</option>
                                                <option value="2">Proses</option>
                                                <option value="3">Ditolak</option>
                                                <option value="4">Diterima</option>
                                                <option value="5">Selesai</option>
                                            </select>
                                        </th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center" colspan="10"> Tidak ada data.</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix pagination-footer">

                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-tracking">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h6 class="modal-title" id="modal-image-title">&nbsp;PELACAKAN DOKUMEN</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header"><strong class="text-danger">DETAIL PELAYANAN</strong>
                                    </div>
                                    <div class="card-body">
                                        <div class="row invoice-info">
                                            <div class="col-sm-4 invoice-col">
                                                <address>
                                                    Tanggal Permohonan:<br> <strong id="tglPelayanan"></strong><br><br>
                                                    No. Pelayanan:<br> <strong class="text-danger"
                                                        id="noPelayanan">2020</strong>
                                                </address>
                                            </div>
                                            <div class="col-sm-8 invoice-col">
                                                <address>
                                                    <div class="col-md-12">
                                                        <div class="form-group-sm row">
                                                            <div class="col-sm-5">Jenis Pajak</div>
                                                            <div class="col-sm-7">: <strong id="jenisPajak"></strong>
                                                            </div>
                                                        </div>
                                                        <div class="form-group-sm row">
                                                            <div class="col-sm-5">Jenis Pelayanan</div>
                                                            <div class="col-sm-7">: <strong id="jenisPelayanan"></strong>
                                                            </div>
                                                        </div>
                                                        <div class="form-group-sm row">
                                                            <div class="col-sm-5">Tanggal Perkiraan Selesai</div>
                                                            <div class="col-sm-7">: <strong
                                                                    id="tglPerkiraanSelesai"></strong></div>
                                                        </div>
                                                        <div class="form-group-sm row">
                                                            <div class="col-sm-5">NOP</div>
                                                            <div class="col-sm-7">: <strong id="nop"></strong></div>
                                                        </div>
                                                        <div class="form-group-sm row">
                                                            <div class="col-sm-5">Keterangan</div>
                                                            <div class="col-sm-7">: <strong id="keterangan"></strong>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </address>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header"><strong>SYARAT PENGAJUAN</strong></div>
                                    <div class="card-body">
                                        <div class="row invoice-info">
                                            <div class="col-sm-12 invoice-col table-responsive">
                                                <table class="table table-striped table-bordered table-hovered"
                                                    style="width:100%;">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 4rem;">No.</th>
                                                            <th>Jenis Persyaratan</th>
                                                            <th>File Persyaratan</th>
                                                            <th>Unduh File</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="syaratUploadedFiles">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header"><strong class="text-danger">DETAIL PEMOHON</strong>
                                    </div>
                                    <div class="card-body">
                                        <div class="row invoice-info">
                                            <address>
                                                <strong id="namaPemohon" style="font-size:16pt;"></strong><br>
                                                NIK : <span id="nikPemohon"></span><br>
                                                <span id="alamatPemohon"></span><br>
                                                <span id="kecamatanPemohon"></span><br>
                                                <span id="kabupatenPemohon"></span><br>
                                                No. HP : <span id="noHpPemohon"></span><br>
                                                Email : <span id="emailPemohon"></span>
                                            </address>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header"><strong class="text-danger">DETAIL PELACAKAN
                                            DOKUMEN</strong></div>
                                    <div class="card-body" style="background:#f7f7f7">
                                        <div class="timeline">
                                            <div>
                                                <i class="fas fa-envelope bg-blue"></i>
                                                <div class="timeline-item">
                                                    <h3 class="timeline-header"><strong>Pengajuan Berkas</strong>
                                                    </h3>

                                                    <div class="timeline-body">
                                                        <span id="statusPermohonan"></span><br>
                                                        <span id="keteranganPermohonan"></span>
                                                    </div>
                                                    <div class="timeline-footer">
                                                        <small class="text-danger"><i class="fas fa-clock"></i>
                                                            <span id="createDatePermohonan"></span></small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <i class="fas fa-archive bg-danger"></i>
                                                <div class="timeline-item">
                                                    <h3 class="timeline-header"><strong>Verifikasi Data</strong>
                                                    </h3>

                                                    <div class="timeline-body">
                                                        <span id="headerVerifikasi"></span><br>
                                                        <span id="keteranganVerifikasi"></span><br>
                                                        <span id="statusVerifikasi"></span>
                                                    </div>
                                                    <div class="timeline-footer">
                                                        <small class="text-danger"><i class="fas fa-clock"></i>
                                                            <span id="createDateVerifikasi"></span></small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="verlapData">
                                                <i class="fas fa-clipboard-check bg-success"></i>
                                                <div class="timeline-item">
                                                    <h3 class="timeline-header"><strong>Verifikasi Lapangan</strong>
                                                    </h3>

                                                    <div class="timeline-body">
                                                        <span id="headerVerlap"></span>
                                                        <span id="keteranganVerlap"></span>
                                                        <span id="statusVerlap"></span>
                                                        <span id="dokumenPendukung"></span>
                                                    </div>
                                                    <div class="timeline-footer">
                                                        <small class="text-danger"><i class="fas fa-clock"></i>
                                                            <span id="createDateVerlap"></span></small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="validasiData">
                                                <i class="fas fa-clipboard-check bg-success"></i>
                                                <div class="timeline-item">
                                                    <h3 class="timeline-header"><strong>Validasi Data</strong></h3>

                                                    <div class="timeline-body">
                                                        <span id="headerValidasi"></span>
                                                        <span id="keteranganValidasi"></span>
                                                        <span id="statusValidasi"></span>
                                                        <span id="downloadHasilValidasi"></span>
                                                    </div>
                                                    <div class="timeline-footer">
                                                        <small class="text-danger"><i class="fas fa-clock"></i>
                                                            <span id="createDateValidasi"></span></small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="hasilPelayananData">
                                                <i class="fas fa-check bg-success"></i>
                                                <div class="timeline-item">
                                                    <h3 class="timeline-header"><strong>Hasil Pelayanan</strong>
                                                    </h3>

                                                    <div class="timeline-body">
                                                        <span id="headerHasilPelayanan"></span>
                                                        <span id="downloadHasilPelayanan"></span>
                                                    </div>
                                                    <div class="timeline-footer">
                                                        <small class="text-danger"><i class="fas fa-clock"></i>
                                                            <span id="createDateHasilPelayanan"></span></small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal" aria-label="Close"><i
                            class="fas fa-minus-circle"></i> Tutup</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal-image">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h6 class="modal-title" id="modal-image-title">&nbsp;FILE PERSYARATAN</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center" id="imagePlaceholder">

                </div>
                <div class="modal-footer justify-content-between">

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $('#filter-tglPelayanan').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-tglPelayanan').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-tglPelayanan').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            search();
        });

        $("#filter-tglPelayanan, #filter-idJenisPajak, #filter-idJenisPelayanan, #filter-status").change(function() {
            search();
        });

        $("#filter-noPelayanan").keyup(function() {
            search();
        });

        $("#filter-nikPemohon").keyup(function() {
            search();
        });

        $("#filter-namaPemohon").keyup(function() {
            search();
        });

        var datatables = datagrid({
            url: '/tracking/datagrid',
            table: "#tracking-table",
            columns: [{
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: "text-center"
                },
                {
                    class: "text-center"
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: "text-center"
                },
                {
                    class: ""
                },
            ],
            orders: [{
                    sortable: false
                },
                {
                    sortable: true,
                    name: "idJenisPajak"
                },
                {
                    sortable: true,
                    name: "idJenisPelayanan"
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
            ],
            action: [{
                name: 'tracking-dokumen',
                btnClass: 'btn btn-danger btn-xs',
                btnText: '<i class="fas fa-search"> Lacak</i>'
            }, ]

        });

        function search() {
            datatables.setFilters({
                idJenisPelayanan: $("#filter-idJenisPelayanan").val(),
                tglPelayanan: $("#filter-tglPelayanan").val(),
                noPelayanan: $("#filter-noPelayanan").val(),
                nikPemohon: $("#filter-nikPemohon").val(),
                namaPemohon: $("#filter-namaPemohon").val(),
                status: $("#filter-status").val(),
            });
            datatables.reload();
        }
        search();

        // function showTracking(idPelayanan){
        //     $("#modal-tracking").modal('show');
        // }

        function showTracking(idPelayanan) {
            $.get("/tracking/lihat/" + idPelayanan).then(function(res) {
                var data = res;
                $("#tglPelayanan").html(data.t_tgl_pelayanan);
                $("#noPelayanan").html(data.t_no_pelayanan);
                $("#jenisPajak").html(data.nama_pajak);
                $("#jenisPelayanan").html(data.nama_jenis);
                $("#tglPerkiraanSelesai").html(data.t_tgl_perkiraan_selesai);
                $("#nop").html(data.t_nop);
                $("#keterangan").html(data.t_keterangan);
                //         $("#detailTracking").html(data.detailTracking);

                $("#namaPemohon").html(data.t_nama_pemohon);
                $("#nikPemohon").html(data.t_nik_pemohon);
                $("#alamatPemohon").html(data.alamat);
                $("#kecamatanPemohon").html(data.t_kecamatan_pemohon);
                $("#kabupatenPemohon").html(data.t_kabupaten_pemohon);
                $("#noHpPemohon").html(data.t_no_hp_pemohon);
                $("#emailPemohon").html(data.t_email_pemohon);

                $("#statusPermohonan").html(data.statusPermohonan);
                $("#keteranganPermohonan").html(data.keteranganPermohonan);
                $("#createDatePermohonan").html(data.created_at);

                $("#headerVerifikasi").html(data.header_verifikasi);
                $("#statusVerifikasi").html(data.status_verifikasi);
                $("#keteranganVerifikasi").html(data.keterangan_verifikasi);
                $("#createDateVerifikasi").html(data.created_verifikasi);

                $("#headerVerlap").html(data.header_verlap);
                $("#statusVerlap").html(data.status_verlap);
                $("#keteranganVerlap").html(data.keterangan_verlap);
                $("#createDateVerlap").html(data.created_verlap);

                $("#headerValidasi").html(data.header_validasi);
                $("#statusValidasi").html(data.status_validasi);
                $("#keteranganValidasi").html(data.keterangan_validasi);
                $("#createDateValidasi").html(data.created_validasi);

                $("#headerHasilPelayanan").html(data.header_hasil_pelayanan);
                $("#createDateHasilPelayanan").html(data.created_hasil_pelayanan);

                //         $("#dokumenPendukung").html('');
                //         if(data.namaFilePendukung!=""){
                //             var aa = '<br>Silahkan Download Dokumen Pendukung : '
                //                     + '<br><a href="'+data.dokumenPendukung+'" target="_blank"><i class="fas fa-download"></i> '+data.namaFilePendukung+'</a>';
                //             $("#dokumenPendukung").append(aa);
                //         }

                //         $("#downloadHasilPelayanan").html('');
                //         if(data.linkHasilPelayanan!=""){
                //             data.linkHasilPelayanan.forEach(sk=>{
                //                 var ds = '<br>Silahkan Download '
                //                     + '<a href="'+sk+'" target="_blank"><i class="fas fa-download"></i> Hasil Pelayanan</a>';
                //                 $("#downloadHasilPelayanan").append(ds);
                //             })

                //         }

                if (data.aktif_verlap == 1) {
                    $("#verlapData").show();
                } else {
                    $("#verlapData").hide();
                }

                if (data.aktif_validasi == 1) {
                    $("#validasiData").show();
                } else {
                    $("#validasiData").hide();
                }

                if (data.aktif_hasil_pelayanan == 1) {
                    $("#hasilPelayananData").show();
                } else {
                    $("#hasilPelayananData").hide();
                }


                $("#modal-tracking").modal('show');
                syaratUploadedFiles(idPelayanan);
            })
        }

        function syaratUploadedFiles(idPelayanan) {
            $.get("/tracking/syarat-upload/" + idPelayanan).then(function(data) {
                $("#syaratUploadedFiles").html('');
                var no = 1;
                data.forEach(file => {
                    var tr = '<tr>' +
                        '<td style="text-align: center">' + no + '</td>' +
                        '<td>' + file.t_nama_persyaratan + '</td>' +
                        '<td><a href="javascript:void(0);" link="' + file.t_lokasi_file + '" name="' + file
                        .t_nama_persyaratan + '" onclick="showImage(this)">' + file.t_nama_persyaratan +
                        '</a></td>' +
                        '<td><a href="' + file.t_lokasi_file + '" download>' + "Unduh" + '</td>' +
                        '</tr>';
                    $("#syaratUploadedFiles").append(tr);
                    no++;
                })
            });
        }

        function showImage(img) {
            var link = $(img).attr("link");
            var name = $(img).attr("name");
            $("#modal-image-title").text(name);
            var image = $('<img style="max-height:100%; max-width:100%;">');
            image.attr("src", link);
            $("#imagePlaceholder").html(image);
            $("#modal-image").modal('show');
            return false;
        }
    </script>
@endpush
