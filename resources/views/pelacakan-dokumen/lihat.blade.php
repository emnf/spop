@extends('layouts.app')

@section('title')
    Kepoin || Pelacakan Dokumen
@endsection

@section('content')
    <div class="card-body" id="modal-tracking">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h6 class="modal-title" id="modal-image-title">&nbsp;PELACAKAN DOKUMEN</h6>
                    {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button> --}}
                </div>
                <div class="modal-body">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header"><strong class="text-danger">DETAIL PELAYANAN</strong>
                                    </div>
                                    <div class="card-body">
                                        <div class="row invoice-info">
                                            <div class="col-sm-4 invoice-col">
                                                <address>
                                                    Tanggal Permohonan:<br> <strong id="tglPelayanan">
                                                        {{ $data->t_tgl_pelayanan }}</strong><br><br>
                                                    No. Pelayanan:<br> <strong class="text-danger" id="noPelayanan">
                                                        {{ $data->t_no_pelayanan }}</strong>
                                                </address>
                                            </div>
                                            <div class="col-sm-8 invoice-col">
                                                <address>
                                                    <div class="col-md-12">
                                                        <input type="hidden" name="pelayanane" id="pelayanane"
                                                            value="{{ $data->t_id_pelayanan }}">
                                                        <div class="form-group-sm row">
                                                            <div class="col-sm-5">Jenis Pajak</div>
                                                            <div class="col-sm-7">: <strong id="jenisPajak">
                                                                    {{ $data->nama_pajak }}</strong>
                                                            </div>
                                                        </div>
                                                        <div class="form-group-sm row">
                                                            <div class="col-sm-5">Jenis Pelayanan</div>
                                                            <div class="col-sm-7">: <strong id="jenisPelayanan">
                                                                    {{ $data->nama_jenis }}</strong>
                                                            </div>
                                                        </div>
                                                        <div class="form-group-sm row">
                                                            <div class="col-sm-5">Tanggal Perkiraan Selesai</div>
                                                            <div class="col-sm-7">: <strong
                                                                    id="tglPerkiraanSelesai">{{ $data->t_tgl_perkiraan_selesai }}</strong>
                                                            </div>
                                                        </div>
                                                        <div class="form-group-sm row">
                                                            <div class="col-sm-5">NOP</div>
                                                            <div class="col-sm-7">: <strong
                                                                    id="nop">{{ $data->t_nop }}</strong></div>
                                                        </div>
                                                        <div class="form-group-sm row">
                                                            <div class="col-sm-5">Keterangan</div>
                                                            <div class="col-sm-7">: <strong id="keterangan">
                                                                    {{ $data->t_keterangan }}</strong>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </address>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header"><strong>SYARAT PENGAJUAN</strong></div>
                                    <div class="card-body">
                                        <div class="row invoice-info">
                                            <div class="col-sm-12 invoice-col table-responsive">
                                                <table class="table table-striped table-bordered table-hovered"
                                                    style="width:100%;">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 4rem;">No.</th>
                                                            <th>Jenis Persyaratan</th>
                                                            <th>File Persyaratan</th>
                                                            <th>Unduh File</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="syaratUploadedFiles">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header"><strong class="text-danger">DETAIL PEMOHON</strong>
                                    </div>
                                    <div class="card-body">
                                        <div class="row invoice-info">
                                            <address>
                                                <strong id="namaPemohon"
                                                    style="font-size:16pt;">{{ $data->t_nama_pemohon }}</strong><br>
                                                NIK : <span id="nikPemohon">{{ $data->t_nik_pemohon }}</span><br>
                                                <span id="alamatPemohon">{{ $data->alamat }}</span><br>
                                                <span id="kecamatanPemohon">{{ $data->t_kecamatan_pemohon }}</span><br>
                                                <span id="kabupatenPemohon">{{ $data->t_kelurahan_pemohon }}</span><br>
                                                No. HP : <span id="noHpPemohon">{{ $data->t_no_hp_pemohon }}</span><br>
                                                Email : <span id="emailPemohon">{{ $data->t_email_pemohon }}</span>
                                            </address>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header"><strong class="text-danger">DETAIL PELACAKAN
                                            DOKUMEN</strong></div>
                                    <div class="card-body" style="background:#f7f7f7">
                                        <div class="timeline">
                                            <div>
                                                <i class="fas fa-envelope bg-blue"></i>
                                                <div class="timeline-item">
                                                    <h3 class="timeline-header"><strong>Pengajuan Berkas</strong>
                                                    </h3>

                                                    <div class="timeline-body">
                                                        <span
                                                            id="statusPermohonan">{{ $data->statusPermohonan ?? '' }}</span><br>
                                                        <span
                                                            id="keteranganPermohonan">{{ $data->keteranganPermohonan ?? '' }}</span>
                                                    </div>
                                                    <div class="timeline-footer">
                                                        <small class="text-danger"><i class="fas fa-clock"></i>
                                                            <span
                                                                id="createDatePermohonan">{{ $data->created_at ?? '' }}</span></small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <i class="fas fa-archive bg-danger"></i>
                                                <div class="timeline-item">
                                                    <h3 class="timeline-header"><strong>Verifikasi Data</strong>
                                                    </h3>

                                                    <div class="timeline-body">
                                                        <span
                                                            id="headerVerifikasi">{{ $data->header_verifikasi ?? '' }}</span><br>
                                                        <span
                                                            id="keteranganVerifikasi">{{ $data->keterangan_verifikasi ?? '' }}</span><br>
                                                        <span
                                                            id="statusVerifikasi">{{ $data->status_verifikasi ?? '' }}</span><br>
                                                        @if (isset($data->alasan_ditolak))
                                                            <span id="syarattolak"> Persyaratan : </span><br>
                                                            @foreach ($data->alasan_ditolak as $key => $v)
                                                                {{ $key + 1 }} - {{ $v }} <br>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                    <div class="timeline-footer">
                                                        <small class="text-danger"><i class="fas fa-clock"></i>
                                                            <span
                                                                id="createDateVerifikasi">{{ $data->created_verifikasi ?? '' }}</span></small>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- <div id="verlapData" {{ !empty($data->aktif_verlap) ? "" : "hidden" }}>
                                            <i class="fas fa-clipboard-check bg-success"></i>
                                            <div class="timeline-item">
                                                <h3 class="timeline-header"><strong>Verifikasi Lapangan</strong>
                                                </h3>

                                                <div class="timeline-body">
                                                    <span id="headerVerlap">{{ $data->header_verlap ?? '' }}</span>
                                                    <span id="keteranganVerlap">{{ $data->keterangan_verlap ?? '' }}</span>
                                                    <span id="statusVerlap">{{ $data->status_verlap ?? ''  }}</span>
                                                    <span id="dokumenPendukung"></span>
                                                </div>
                                                <div class="timeline-footer">
                                                    <small class="text-danger"><i class="fas fa-clock"></i>
                                                        <span id="createDateVerlap">{{ $data->created_verlap ?? '' }}</span></small>
                                                </div>
                                            </div>
                                        </div> --}}
                                            <div id="validasiData" {{ !empty($data->aktif_validasi) ? '' : 'hidden' }}>
                                                <i class="fas fa-clipboard-check bg-success"></i>
                                                <div class="timeline-item">
                                                    <h3 class="timeline-header"><strong>Validasi Data</strong></h3>

                                                    <div class="timeline-body">
                                                        <span
                                                            id="headerValidasi">{{ $data->header_validasi ?? '' }}</span></br>
                                                        <span
                                                            id="keteranganValidasi">{{ $data->t_keterangan_validasi ?? '' }}</span></br>
                                                        <span
                                                            id="statusValidasi">{{ $data->status_validasi ?? '' }}</span>
                                                        <span id="downloadHasilValidasi"></span>
                                                    </div>
                                                    <div class="timeline-footer">
                                                        <small class="text-danger"><i class="fas fa-clock"></i>
                                                            <span
                                                                id="createDateValidasi">{{ $data->created_validasi ?? '' }}</span></small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="verlapData" {{ !empty($data->aktif_verlap) ? '' : 'hidden' }}>
                                                <i class="fas fa-clipboard-check bg-danger"></i>
                                                <div class="timeline-item">
                                                    <h3 class="timeline-header"><strong>Verifikasi Lapangan</strong>
                                                    </h3>
                                                    {{-- {{ dd($data); }} --}}
                                                    <div class="timeline-body">
                                                        <span
                                                            id="headerVerlap">{{ $data->header_verlap ?? '' }}</span><br>
                                                        <span
                                                            id="keteranganVerlap">{{ $data->keterangan_verlap ?? '' }}</span><br>
                                                        <span
                                                            id="statusVerlap">{{ $data->status_verlap ?? '' }}</span><br>
                                                        {{-- <span id="dokumenPendukung"></span> --}}
                                                    </div>
                                                    <div class="timeline-footer">
                                                        <small class="text-danger"><i class="fas fa-clock"></i>
                                                            <span
                                                                id="createDateVerlap">{{ $data->created_verlap ?? '' }}</span></small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="hasilPelayananData"
                                                {{ !empty($data->aktif_hasil_pelayanan) ? '' : 'hidden' }}>
                                                <i class="fas fa-check bg-success"></i>
                                                <div class="timeline-item">
                                                    <h3 class="timeline-header"><strong>Hasil Pelayanan</strong>
                                                    </h3>
                                                    <div class="timeline-body">
                                                        <span
                                                            id="headerHasilPelayanan">{{ $data->header_hasil_pelayanan ?? '' }}</span>
                                                        @if ($data->t_id_jenis_pelayanan == 1)
                                                            <span id="nopnya">{{ $data->nopnya ?? '' }}</span>
                                                        @endif
                                                        @if ($hasilPelayanan != '')
                                                            @foreach ($hasilPelayanan as $v)
                                                                <br>Silahkan Download <a href="/{{ $v->t_lokasi_file }}"
                                                                    target="_blank" download><i
                                                                        class="fas fa-download"></i> Hasil Pelayanan </a>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                    <div class="timeline-footer">
                                                        <small class="text-danger"><i class="fas fa-clock"></i>
                                                            <span
                                                                id="createDateHasilPelayanan">{{ $data->created_hasil_pelayanan ?? '' }}</span></small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal" aria-label="Close" >close</button> --}}
                    <button type="button" class="btn btn-xs btn-danger"
                        onclick="location.href='/tracking'">Kembali</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modal-image">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h6 class="modal-title" id="modal-image-title">&nbsp;FILE PERSYARATAN</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center" id="imagePlaceholder">

                </div>
                <div class="modal-footer justify-content-between">

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            syaratUploadedFiles($("#pelayanane").val());
        });

        // $('#filter-tglPelayanan').daterangepicker({
        //     autoUpdateInput: false,
        //     locale: {
        //         cancelLabel: 'Clear'
        //     }
        // });

        // $('#filter-tglPelayanan').on('apply.daterangepicker', function(ev, picker) {
        //     $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        //     search();
        // });

        // $('#filter-tglPelayanan').on('cancel.daterangepicker', function(ev, picker) {
        //     $(this).val('');
        //     search();
        // });

        // $("#filter-tglPelayanan, #filter-idJenisPajak, #filter-idJenisPelayanan").change(function(){
        //     search();
        // });

        // $("#filter-noPelayanan").keyup(function(){
        //     search();
        // });

        // $("#filter-nikPemohon").keyup(function(){
        //     search();
        // });

        // $("#filter-namaPemohon").keyup(function(){
        //     search();
        // });

        // var datatables = datagrid({
        //     url: '/tracking/datagrid',
        //     table: "#tracking-table",
        //     columns: [
        //         {class: ""},
        //         {class: ""},
        //         {class: "text-center"},
        //         {class: "text-center"},
        //         {class: ""},
        //         {class: ""},
        //         {class: "text-center"},
        //     ],
        //     orders: [
        //         {sortable: false},
        //         {sortable: true, name: "idJenisPajak"},
        //         {sortable: true, name: "idJenisPelayanan"},
        //         {sortable: false},
        //         {sortable: false},
        //         {sortable: false},
        //         {sortable: false},
        //         {sortable: false},
        //     ],
        //     action: [
        //         {
        //             name: 'tracking-dokumen',
        //             btnClass: 'btn btn-danger btn-xs',
        //             btnText: '<i class="fas fa-search"> Lacak</i>'
        //         },
        //     ]

        // });



        // function search(){
        //     datatables.setFilters({
        //         idJenisPelayanan: $("#filter-idJenisPelayanan").val(),
        //         tglPelayanan: $("#filter-tglPelayanan").val(),
        //         noPelayanan: $("#filter-noPelayanan").val(),
        //         nikPemohon: $("#filter-nikPemohon").val(),
        //         namaPemohon: $("#filter-namaPemohon").val(),
        //     });
        //     datatables.reload();
        // }
        // search();

        // function showTracking(idPelayanan){
        //     $("#modal-tracking").modal('show');
        // }

        // function showTracking(idPelayanan){
        // console.log(idPelayanan);
        //     $.get("/tracking/lihat/" + idPelayanan).then(function(res){
        //         // console.log(res);
        //         var data = res;
        //         $("#tglPelayanan").html(data.t_tgl_pelayanan);
        //         $("#noPelayanan").html(data.t_no_pelayanan);
        //         $("#jenisPajak").html(data.nama_pajak);
        //         $("#jenisPelayanan").html(data.nama_jenis);
        //         $("#tglPerkiraanSelesai").html(data.t_tgl_perkiraan_selesai);
        //         $("#nop").html(data.t_nop);
        //         $("#keterangan").html(data.t_keterangan);
        // //         $("#detailTracking").html(data.detailTracking);

        //         $("#namaPemohon").html(data.t_nama_pemohon);
        //         $("#nikPemohon").html(data.t_nik_pemohon);
        //         $("#alamatPemohon").html(data.alamat);
        //         $("#kecamatanPemohon").html(data.t_kecamatan_pemohon);
        //         $("#kabupatenPemohon").html(data.t_kabupaten_pemohon);
        //         $("#noHpPemohon").html(data.t_no_hp_pemohon);
        //         $("#emailPemohon").html(data.t_email_pemohon);

        //         $("#statusPermohonan").html(data.statusPermohonan);
        //         $("#keteranganPermohonan").html(data.keteranganPermohonan);
        //         $("#createDatePermohonan").html(data.created_at);

        //         $("#headerVerifikasi").html(data.header_verifikasi);
        //         $("#statusVerifikasi").html(data.status_verifikasi);
        //         $("#keteranganVerifikasi").html(data.keterangan_verifikasi);
        //         $("#createDateVerifikasi").html(data.created_verifikasi);

        //         $("#headerVerlap").html(data.header_verlap);
        //         $("#statusVerlap").html(data.status_verlap);
        //         $("#keteranganVerlap").html(data.keterangan_verlap);
        //         $("#createDateVerlap").html(data.created_verlap);

        //         $("#headerValidasi").html(data.header_validasi);
        //         $("#statusValidasi").html(data.status_validasi);
        //         $("#keteranganValidasi").html(data.keterangan_validasi);
        //         $("#createDateValidasi").html(data.created_validasi);

        //         $("#headerHasilPelayanan").html(data.header_hasil_pelayanan);
        //         $("#createDateHasilPelayanan").html(data.created_hasil_pelayanan);


        // //         $("#dokumenPendukung").html('');
        // //         if(data.namaFilePendukung!=""){
        // //             var aa = '<br>Silahkan Download Dokumen Pendukung : '
        // //                     + '<br><a href="'+data.dokumenPendukung+'" target="_blank"><i class="fas fa-download"></i> '+data.namaFilePendukung+'</a>';
        // //             $("#dokumenPendukung").append(aa);
        // //         }

        // $("#downloadHasilPelayanan").html('');
        // if(data.linkHasilPelayanan!=""){
        //     data.linkHasilPelayanan.forEach(sk=>{
        //         var ds = '<br>Silahkan Download '
        //             + '<a href="'+sk+'" target="_blank"><i class="fas fa-download"></i> Hasil Pelayanan</a>';
        //         $("#downloadHasilPelayanan").append(ds);
        //     })

        // }

        // if (data.aktif_verlap == 1) {
        //     $("#verlapData").show();
        // } else {
        //     $("#verlapData").hide();
        // }

        // if (data.aktif_validasi == 1) {
        //     $("#validasiData").show();
        // } else {
        //     $("#validasiData").hide();
        // }

        // if (data.aktif_hasil_pelayanan == 1) {
        //     $("#hasilPelayananData").show();
        // } else {
        //     $("#hasilPelayananData").hide();
        // }


        //         $("#modal-tracking").modal('show');
        //         syaratUploadedFiles(idPelayanan);
        //     })
        // }

        function syaratUploadedFiles(idPelayanan) {
            $.get("/tracking/syarat-upload/" + idPelayanan).then(function(data) {
                $("#syaratUploadedFiles").html('');
                var no = 1;
                data.forEach(file => {
                    var tr = '<tr>' +
                        '<td style="text-align: center">' + no + '</td>' +
                        '<td>' + file.t_nama_persyaratan + '</td>' +
                        '<td><a href="javascript:void(0);" link="/' + file.t_lokasi_file + '" name="' + file
                        .t_nama_persyaratan + '" onclick="showImage(this)">' + file.t_nama_persyaratan +
                        '</a></td>' +
                        '<td><a href="/' + file.t_lokasi_file + '" download>' + "Unduh" + '</td>' +
                        '</tr>';
                    $("#syaratUploadedFiles").append(tr);
                    no++;
                })
            });
        }

        function showImage(img) {
            var link = $(img).attr("link");
            var name = $(img).attr("name");
            $("#modal-image-title").text(name);
            var image = $('<img style="max-height:100%; max-width:100%;">');
            image.attr("src", link);
            $("#imagePlaceholder").html(image);
            $("#modal-image").modal('show');
            return false;
        }
    </script>
@endpush
