{{-- @php
    error_reporting(0);
    ini_set('pcre.backtrack_limit', 10000000);
    if ($data->njop_sppt < $data->njop_max) {
        $v_tarif_dasar = '0,1';
    } elseif ($data->njop_sppt > $data->njop_max) {
        $v_tarif_dasar = '0,2';
    }

    function hitungDenda($tgltempo, $jmlhpajak)
    {
        $tgljatuhtempo = date('Y-m-d', strtotime($tgltempo));
        $tglsekarang = date('Y-m-d');
        $ts1 = strtotime($tgljatuhtempo);
        $ts2 = strtotime($tglsekarang);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);

        $day1 = date('d', $ts1);
        $day2 = date('d', $ts2);
        if ($day1 < $day2) {
            $tambahanbulan = 1;
        } else {
            $tambahanbulan = 0;
        }

        $jmlbulan = ($year2 - $year1) * 12 + ($month2 - $month1 + $tambahanbulan);
        if ($jmlbulan > 24) {
            $jmlbulan = 24;
            $jmldenda = (($jmlbulan * 2) / 100) * $jmlhpajak;
        } elseif ($jmlbulan <= 0) {
            $jmlbulan = 0;
            $jmldenda = (($jmlbulan * 2) / 100) * $jmlhpajak;
        } else {
            $jmldenda = (($jmlbulan * 2) / 100) * $jmlhpajak;
        }
        return round($jmldenda);
    }
@endphp
<style>
    h3 {
        text-align: center;
    }

    .background-gray-50 {
        background: rgb(226, 225, 225);
    }

    .background-black {
        background: black;
        color: white;
    }

    .text-right {
        text-align: right;
    }

    .text-center {
        text-align: center;
    }

    .border-left {
        border-left: 1px solid #000;
    }

    .border-right {
        border-right: 1px solid #000;
    }

    .border-top {
        border-top: 1px solid #000;
    }

    .border-bottom {
        border-bottom: 1px solid #000;
    }

    .posisi {
        position: absolute;
        font-size: 9pt;
    }

    .halaman1 {
        font-family: monospace;
        font-size: 14px;
        color: #848587;
        background-image: url([[@{classpath:static/images/sppt.png}]]);
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: 100%;
        background-repeat: no-repeat;
        width: 100%;
    }

    @page {
        size: 21cm 23cm;
        margin: 0px;
        padding: 0px;
    }
</style>
<div
    style="background: {{ $blanko->thumbnail }};
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: 100%;
    background-repeat: no-repeat;
    padding-bottom:153%;">

    <div class="posisi" style="top:3.4em;right:3em;">{{ $data->nm_sektor == 'PERKOTAAN' ? '411312' : '411311' }}</div>
    <div class="posisi" style="top:4.6em;right:3em;">{{ $data->nm_sektor }}</div>
    <div class="posisi" style="top:5.6em;right:2.5em;">#{{ $codingsppt . '/' . $data->siklus_sppt }}#</div>
    <div class="posisi" style="top:4.6em;right:10em;font-size:10pt;"><strong>{{ $data->thn_pajak_sppt }}</strong></div>
    <div class="posisi" style="top:5.6em;left:4em;">{{ $nop }}</div>

    Letak Objek Pajak
    <div class="posisi" style="top:9em;left:3em;width:180px;">{{ $data->jalan_op . ' ' . $data->blok_kav_no_op }}</div>
    <div class="posisi" style="top:10em;left:3em;">{{ ' RT.' . $data->rt_op . ' RW.' . $data->rw_op }}</div>
    <div class="posisi" style="top:11em;left:3em;">{{ $data->nm_kelurahan }}</div>
    <div class="posisi" style="top:12em;left:3em;">{{ $data->nm_kecamatan }}</div>

    Nama Alamat Wajib Pajak
    <div class="posisi" style="top:8em;left:23em;width:220px;">{{ $data->nm_wp_sppt }}</div>
    <div class="posisi" style="top:9em;left:23em;">{{ $data->jln_wp_sppt . ' ' . $data->blok_kav_no_wp_sppt }}</div>
    <div class="posisi" style="top:10em;left:23em;">{{ 'RT.' . $data->rt_wp_sppt . ' RW.' . $data->rw_wp_sppt }}</div>
    <div class="posisi" style="top:11em;left:23em;">{{ $data->kelurahan_wp_sppt }}</div>
    <div class="posisi" style="top:12em;left:23em;">{{ $data->kota_wp_sppt }}</div>
    <div class="posisi" style="top:13em;left:23em;">{{ $data->npwp_sppt }}</div>

    Detail Objek Pajak
    <div class="posisi" style="top:15.5em;left:3em;">BUMI</div>
    <div class="posisi" style="top:15.5em;right:28.5em;width:70px;text-align:right;">{{ $data->luas_bumi_sppt }}</div>
    <div class="posisi" style="top:15.5em;right:25em;width:30px;text-align:right;">{{ $data->kd_kls_tanah }}</div>
    <div class="posisi" style="top:15.5em;right:14em;width:130px;text-align:right;">
        {{ number_format($data->luas_bumi_sppt > 0 ? $data->njop_bumi_sppt / $data->luas_bumi_sppt : 0, 0, ',', '.') }}
    </div>
    <div class="posisi" style="top:15.5em;right:2.5em;width:130px;text-align:right;">
        {{ number_format($data->njop_bumi_sppt, 0, ',', '.') }}</div>

    <div class="posisi" style="top:16.5em;left:3em;">BANGUNAN</div>
    <div class="posisi" style="top:16.5em;right:28.5em;width:70px;text-align:right;">{{ $data->luas_bng_sppt }}</div>
    <div class="posisi" style="top:16.5em;right:25em;width:30px;text-align:right;">{{ $data->kd_kls_bng }}</div>
    <div class="posisi" style="top:16.5em;right:14em;width:130px;text-align:right;">
        {{ number_format($data->luas_bng_sppt > 0 ? $data->njop_bng_sppt / $data->luas_bng_sppt : 0, 0, ',', '.') }}
    </div>
    <div class="posisi" style="top:16.5em;right:2.5em;width:130px;text-align:right;">
        {{ number_format($data->njop_bng_sppt, 0, ',', '.') }}</div>

    <div class="posisi" style="top:19.8em;right:2.5em;">{{ number_format($data->njop_sppt, 0, ',', '.') }}</div>
    <div class="posisi" style="top:20.8em;right:2.5em;">{{ number_format($data->njoptkp_sppt, 0, ',', '.') }}</div>
    <div class="posisi" style="top:21.8em;right:2.5em;">
        {{ number_format($data->njop_sppt - $data->njoptkp_sppt, 0, ',', '.') }}</div>
    <div class="posisi" style="top:22.8em;left:15em;">
        {{ $v_tarif_dasar . '% x ' . number_format($data->njop_sppt - $data->njoptkp_sppt, 0, ',', '.') }}</div>
    <div class="posisi" style="top:22.8em;right:2.5em;">{{ number_format($data->pbb_terhutang_sppt, 0, ',', '.') }}
    </div>

    <div class="posisi" style="top:23.9em;left:1.8em;">Faktor Pengurang/Pengaturan Pengenaan</div>
    <div class="posisi" style="top:23.8em;right:2.5em;">{{ number_format($data->faktor_pengurang_sppt, 0, ',', '.') }}
    </div>

    <div class="posisi" style="top:24.8em;right:2.5em;">
        {{ number_format($data->pbb_yg_harus_dibayar_sppt, 0, ',', '.') }}
    </div>
    <div class="posisi" style="top:26.8em;left:3em;"><i>{{ strtoupper($terbilang) }} RUPIAH </i></div>


    TUNGGAKAN
    @php
        $nomor = 0;
        $totalBelumBayar = 0;
    @endphp
    @foreach ($tunggakan as $row)
        @php
            $jmlhDenda = hitungDenda($row['tgl_jatuh_tempo_sppt'], $row['pbb_yg_harus_dibayar_sppt']);
        @endphp
        <div class="posisi" style="top:3{{ $nomor + 2 }}em;left:2.5em;">{{ $row['thn_pajak_sppt'] }}</div>
        <div class="posisi" style="top:3{{ $nomor + 2 }}em;left:5em;width:100px;text-align:right;">
            {{ number_format($row['pbb_yg_harus_dibayar_sppt'], 0, ',', '.') }}</div>
        <div class="posisi" style="top:3{{ $nomor + 2 }}em;left:12em;width:100px;text-align:right;">
            {{ number_format($jmlhDenda, 0, ',', '.') }}</div>
        <div class="posisi" style="top:3{{ $nomor + 2 }}em;left:19em;width:100px;text-align:right;">
            {{ number_format($row['pbb_yg_harus_dibayar_sppt'] + $jmlhDenda, 0, ',', '.') }}</div>
        @php
            $nomor++;
            $totalBelumBayar += $row['pbb_yg_harus_dibayar_sppt'] + $jmlhDenda;
        @endphp
    @endforeach

    <div class="posisi" style="top:45em;right:17.3em;width:100px;text-align:right;">
        {{ $totalBelumBayar ? number_format($totalBelumBayar, 0, ',', '.') : '' }}</div>

    <div class="posisi" style="top:46.3em;left:10em;">
        {{ strtoupper(date('d M Y', strtotime($data->tgl_jatuh_tempo_sppt))) }}</div>
    <div class="posisi" style="top:48.3em;left:2.5em;text-align:center;width:180px;">
        {{ $data->nm_tp . $data->alamat_tp }}
        <table style="font-size: 9pt;">
            <tr>
                <td>-BANK BPD DIY</td>
                <td>-SHOPEE</td>
            </tr>
            <tr>
                <td>-TOKOPEDIA</td>
                <td>-JOGJAKITA</td>
            </tr>
            <tr>
                <td>-GOPAY</td>
                <td>-LINKAJA</td>
            </tr>
            <tr>
                <td>-KANTOR POS</td>
                <td>-DANA</td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;"><small>*Scan barcode ini untuk periksa riwayat pembayaran
                        PBB</small></td>
            </tr>
        </table>
    </div>

    <div class="posisi" style="top:46.3em;right:7em;">{{ $data->kota_terbit_kanwil }},
        {{ strtoupper(date('d M Y', strtotime($data->tgl_terbit_sppt))) }}</div>

    QRCODE
    <div class="posisi" style="top:48.3em;left:14.8em;">
        <table class="border-top border-left border-right border-bottom" style="font-size: 9pt;width:97.5%;">
            <tr>
                <td rowspan="2" class="border-right" style="padding: 8px;width:25%;">
                    {!! str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $qrcode) !!}
                </td>
                <td class="text-center">
                    Ditandatangani secara elektronik oleh:<br>
                    {{ $pejabat->nm_jabatan }}
                    {{ strtoupper($pemda->s_namainstansi) }}
                </td>
            </tr>
            <tr>
                <td class="text-center border-top">
                    {{ $pejabat->nm_pegawai }}<br>
                    {{ $refUmum->nm_ref }}<br>
                    NIP. {{ $pejabat->nip_baru }}
                </td>
            </tr>
        </table>
    </div>


    LEMBAR BANK
    <div class="posisi" style="top:59em;left:9em;font-size:8pt;width:180px;">{{ $data->nm_wp_sppt }}</div>
    <div class="posisi" style="top:60em;left:13.5em;font-size:8pt;">{{ $data->nm_kecamatan }}</div>
    <div class="posisi" style="top:61em;left:17em;font-size:8pt;">{{ $data->nm_kelurahan }}</div>
    <div class="posisi" style="top:62em;left:9em;font-size:8pt;">{{ $nop }}</div>
    <div class="posisi" style="top:63em;left:9em;font-size:8pt;">
        {{ $data->thn_pajak_sppt . ' - ' . $angkakontrol . ' / Rp. ' . number_format($data->pbb_yg_harus_dibayar_sppt, 0, ',', '.') }}
    </div>
</div> --}}


<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">

<head>
    <meta charset="UTF-8">
    <title>SPPT</title>
    <style>
        @page {
            size: 21cm 23cm;
            margin: 5px;
            padding: 0px;
        }
    </style>
    <style>
        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
        }
    </style>
</head>

<body>
    @php
        error_reporting(0);
        ini_set('pcre.backtrack_limit', 10000000);
        if ($data->njop_sppt < $data->njop_max) {
            $v_tarif_dasar = '0,1';
        } elseif ($data->njop_sppt > $data->njop_max) {
            $v_tarif_dasar = '0,2';
        }
    @endphp
    <div class="halaman1"
        style="font-family: monospace;
            font-size: 14px;
            color: #848587;
            background-image: {{ $blanko->thumbnail }};
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: 100%;
            background-repeat: no-repeat;
            width: 100%;">
        <table border="0" style="border-collapse: collapse; width: 100%;">
            <tr>
                <td style="padding-top: 95px;"></td>
            </tr>
        </table>
        <table border="0" style="border-collapse: collapse; width: 100%;">
            <tr>
                <td style="width: 50%"></td>
                <td style="padding-left: 160px;"><span>{{ $data->thn_pajak_sppt }}</span> &nbsp; &nbsp;
                    <span>{{ $data->nm_sektor }}</span>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 60px;">{{ $nop }}</td>
                <td style="padding-left: 30px;">{{ $codingsppt }}</td>
            </tr>
        </table>
        <table border="0" style="border-collapse: collapse; width: 100%;">
            <tr>
                <td style="width: 51%; padding-top: 20px;"></td>
                <td></td>
            </tr>
            <tr>
                <td style="padding-left: 30px; vertical-align: top;">
                    <span>{{ $data->jalan_op }}</span>
                </td>
                <td style="vertical-align: top;">
                    <span>{{ $data->nm_wp_sppt }}</span>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 30px; vertical-align: top;">RT. <span>{{ $data->rt_op }}</span> RW.
                    <span>{{ $data->rw_op }}</span>
                </td>
                <td><span>{{ $data->jln_wp_sppt }}</span> <span>{{ $data->blok_kav_no_wp_sppt }}</span></td>
            </tr>
            <tr>
                <td style="padding-left: 30px; vertical-align: top;"><span>{{ $data->nm_kelurahan }}</span>
                </td>
                <td>RT. <span>{{ $data->rt_wp_sppt ?? '000' }}</span> RW. {{ $data->rw_wp_sppt ?? '00' }}
                </td>
            </tr>
            <tr>
                <td style="padding-left: 30px; vertical-align: top;"><span>{{ $data->nm_kecamatan }}</span>
                </td>
                <td><span>{{ $data->nm_kelurahan }}</span></td>
            </tr>
            <tr>
                <td style="padding-left: 30px; vertical-align: top;">
                    <span><span>{{ strtoupper($pemda->s_nama_kabkota) }}</span>
                </td>
                <td><span>{{ $data->kota_wp_sppt }}</span></td>
            </tr>
            <tr>
                <td style="padding-left: 30px; vertical-align: top;"></td>
                <td style="padding-left: 60px; padding-top: 4px;"><span>{{ $data->npwp_sppt }}</span></td>
            </tr>
        </table>
        <br>
        <br>
        <br>
        <table border="0" style="border-collapse: collapse; width: 100%;">
            <tr>
                <td style="padding-left: 30px; width: 19%;">BUMI</td>
                <td style="width: 15%; text-align: right;">
                    <span>{{ number_format($data->luas_bumi_sppt, 0, ',', '.') }}</span>
                </td>
                <td style="width: 8%; text-align: right;"><span>{{ $data->kd_kls_tanah }}</span></td>
                <td style="width: 24%; text-align: right;">
                    <span>{{ number_format($data->njop_bumi_sppt / $data->luas_bumi_sppt, 0, ',', '.') }}</span>
                </td>
                <td style="width: 28%; text-align: right;">
                    <span>{{ number_format($data->njop_bumi_sppt, 0, ',', '.') }}</span>
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="padding-left: 30px;">BANGUNAN</td>
                <td style="text-align: right;"><span>{{ number_format($data->luas_bng_sppt, 0, ',', '.') }}</span>
                </td>
                <td style="text-align: right;"><span>{{ $data->kd_kls_bng }}</span></td>
                <td style="text-align: right;"><span>
                        @php
                            $njopbm = 0;
                            if ($data->luas_bng_sppt > 0) {
                                $njopbm = $data->njop_bng_sppt / $data->luas_bng_sppt;
                            }
                        @endphp
                        {{ number_format($njopbm, 0, ',', '.') }}
                    </span>
                </td>
                <td style="text-align: right;"><span>
                        {{ number_format($data->njop_bng_sppt, 0, ',', '.') }}
                    </span>
                </td>
                <td></td>
            </tr>
        </table>
        <br>
        <br>
        <br>
        <table border="0" style="border-collapse: collapse; width: 100%; margin-top: 4px;">
            <tr>
                <td style="width: 50%;"></td>
                <td style="text-align: right;"><span>{{ number_format($data->njop_sppt, 0, ',', '.') }}</span></td>
                <td style="width: 4%;"></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align: right;"><span>{{ number_format($data->njoptkp_sppt, 0, ',', '.') }}</span>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align: right;">
                    <span>{{ number_format($data->njop_sppt - $data->njoptkp_sppt, 0, ',', '.') }}</span>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align: right;">
                    <span>{{ $v_tarif_dasar }} %</span>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align: right;">
                    <span>{{ number_format($data->pbb_terhutang_sppt, 0, ',', '.') }}</span>
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="padding-left: 20px;">
                    <span>{{ $data->faktor_pengurang_sppt > 0 ? 'Stimulus/Pengurangan' : '' }}</span>
                </td>
                <td style="text-align: right;">
                    <span>{{ $data->faktor_pengurang_sppt > 0 ? number_format($data->faktor_pengurang_sppt, 0, ',', '.') : '' }}</span>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align: right;">
                    <span>{{ number_format($data->pbb_yg_harus_dibayar_sppt, 0, ',', '.') }}</span>
                <td></td>
            </tr>
        </table>
        <br>
        <br>
        <table border="0" style="border-collapse: collapse; width: 100%; margin-top: 4px;">
            <tr>
                <td style="padding-left: 20px;"><span>{{ $terbilang }}</span></td>
            </tr>
        </table>
        <table style="border-collapse: collapse; width: 100%; margin-top: 20px;">
            <tr style=""> {{-- border: 1px solid black; --}}
                <td style="width: 23%;"></td> {{-- border: 1px solid black; --}}
                <td style="width: 17%;"> {{-- border: 1px solid black; --}}
                    <span>{{ date('d', strtotime($data->tgl_jatuh_tempo_sppt)) }}
                        {{ $bulan_tempo['nama'] }} {{ date('Y', strtotime($data->tgl_jatuh_tempo_sppt)) }}</span>
                </td>
                <td style="width: 15%;vertical-align:top;text-align:center;" rowspan="8"> {{-- border: 1px solid black; --}}
                    {{-- <img src="{{ public_path() . '/storage/' }}22034dbc-f67b-4c4c-9ca8-844b1263fa41.png" --}}
                    {{-- style="width: 70px;"> border: 1px solid black; --}}
                    <br><br><br>
                    <span style="color: white;">#</span>
                </td>
                <td style="text-align: center;"> {{-- border: 1px solid black; --}}
                    <span>{{ str_replace('Kabupaten ', '', $pemda->s_nama_kabkota) }}</span>,
                    <span>{{ date('d', strtotime($data->tgl_terbit_sppt)) }}
                        {{ $bulan_tempo['nama'] }} {{ date('Y', strtotime($data->tgl_terbit_sppt)) }}</span>
                </td>
                <td style="width: 10%;"></td> {{-- border: 1px solid black; --}}
            </tr>
            <tr style=""> {{-- border: 1px solid black; --}}
                <td style="width: 23%;"></td> {{-- border: 1px solid black; --}}
                <td style="width: 17%;"> {{-- border: 1px solid black; --}}
                    <span>BANK JATENG</span>
                </td>

                <td style="text-align: center;"> {{-- border: 1px solid black; --}}

                </td>
                <td style="width: 10%;"></td> {{-- border: 1px solid black; --}}
            </tr>
            <tr style=""> {{-- border: 1px solid black; --}}
                <td style="padding-left: 20px;width: 37%;" colspan="2">BPR BKK BLORA, KANTOR {{-- border: 1px solid black; --}}
                    POS, GOPAY,</td>

                <td style="text-align: center;"> {{-- border: 1px solid black; --}}

                </td>
                <td style="width: 10%;"></td> {{-- border: 1px solid black; --}}
            </tr>
            <tr style=""> {{-- border: 1px solid black; --}}
                <td style="padding-left: 20px;width: 37%;" colspan="2">INDOMARET, BUKALAPAK, {{-- border: 1px solid black; --}}
                    TOKOPEDIA</td>

                <td style="text-align: center;vertical-align:middle;" rowspan="4"> {{-- border: 1px solid black; --}}
                    <img src="{{ public_path() . '/bsre/' }}visual-ttd.png" style="width: 100px;"><br>
                    <span>{{ $pejabat->nm_pegawai }}</span>
                    <span>NIP. {{ $pejabat->nip_baru }}</span>
                </td>
                <td style="width: 10%;"></td> {{-- border: 1px solid black; --}}
            </tr>
            <tr style=""> {{-- border: 1px solid black; --}}
                <td style="padding-left: 20px;width: 37%;" colspan="2">- Denda 2% per-bulan {{-- border: 1px solid black; --}}
                    dikenakan</td>


                <td style="width: 10%;"></td> {{-- border: 1px solid black; --}}
            </tr>
            <tr style=""> {{-- border: 1px solid black; --}}
                <td style="padding-left: 20px;width: 37%;" colspan="2">- setelah tanggal {{-- border: 1px solid black; --}}
                    jatuh tempo</td>


                <td style="width: 10%;"></td> {{-- border: 1px solid black; --}}
            </tr>
            <tr style=""> {{-- border: 1px solid black; --}}
                <td style="padding-left: 20px;width: 37%;" colspan="2"></td> {{-- border: 1px solid black; --}}


                <td style="width: 10%;"></td> {{-- border: 1px solid black; --}}
            </tr>
        </table>
        <br>

        <table border="0" style="border-collapse: collapse; width: 100%;margin-top:26px;">
            <tr>
                <td style="width: 20%;"></td>
                <td style="width: 30%;"><span>{{ $data->nm_wp_sppt }}</span></span></td>
                <td rowspan="5" style="padding-left: 45px; vertical-align: top;">
                </td>
            </tr>
            <tr>
                <td></td>
                <td><span>{{ $pemda->s_nama_ibukota_kabkota }}</span></td>

            </tr>
            <tr>
                <td></td>
                <td><span>{{ $data->nm_kelurahan }}</span></td>

            </tr>
            <tr>
                <td> </td>
            </tr>
            <tr>
                <td></td>
                <td><span>{{ $nop }}</span></td>
            </tr>
            <tr>
                <td> </td>
            </tr>
            <tr>
                <td> </td>
            </tr>
            <tr>
                <td> </td>
            </tr>
            <tr>
                <td></td>
                <td><span>{{ $data->thn_pajak_sppt }}</span> /
                    <span>{{ number_format($data->pbb_yg_harus_dibayar_sppt, 0, ',', '.') }}<br><br></span>
                </td>

            </tr>
        </table>
    </div>

    <div class="footer">
        <div>
            <table style="width: 100%; border-collapse: collapse; font-size: 11px;" border="0">
                <tr>
                    <td colspan="3" style="border-top: 1px solid black;"></td>
                </tr>
                <tr>
                    <td style="width: 5px; vertical-align: top;">-</td>
                    <td style="vertical-align: top;">Dokumen ini ditandatangani secara elektronik menggunakan
                        Sertifikat Elektronik yang diterbitkan oleh Balai Sertifikasi Elektronik (BSrE), Badan Siber dan
                        Sandi
                        Negara (BSSN).</td>
                    <td style="vertical-align: top;">
                        <img src="{{ public_path() . '/bsre/' }}bsre.png" style="width: 70px;">
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>

</html>
