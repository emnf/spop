<div data-th-fragment="tabpanel" class="tab-pane fade active show" id="navtab1" role="tabpanel">
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-bordered table-striped table-hover" id="datagrid2">
                <thead>
                    <tr>
                        <th style="background-color: #1fc8e3" class="text-center">No</th>
                        <th style="background-color: #1fc8e3" class="text-center">Tgl. Penetapan</th>
                        <th style="background-color: #1fc8e3" class="text-center">NIP Perekam</th>
                        <th style="background-color: #1fc8e3" class="text-center">NOP</th>
                        <th style="background-color: #1fc8e3" class="text-center">Tahun</th>
                        <th style="background-color: #1fc8e3" class="text-center">User Penetapan</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th><input class="form-control form-control-sm" onchange="search2()" type="text"
                                id="filter-tglPenetapan"></th>
                        <th><input class="form-control form-control-sm" onchange="search2()" type="text"
                                id="filter-nip"></th>
                        <th><input class="form-control form-control-sm" onchange="search2()" type="text"
                                id="filter-nop">
                        </th>
                        <th><input class="form-control form-control-sm" onchange="search2()" type="text"
                                id="filter-tahun">
                        </th>
                        <th><input class="form-control form-control-sm" onchange="search2()" type="text"
                                id="filter-user">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="8"> Tidak ada data.</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-12 pagination-footer"></div>
    </div>
</div>

@push('scripts')
    <script th:inline="javascript" data-th-fragment="js">
        $('#filter-tglPenetapan').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-tglPenetapan').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search2();
        });

        $('#filter-tglPenetapan').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            search2();
        });

        $("#filter-idJenisPajak1, #filter-idJenisPelayanan2").change(function() {
            search2();
        });

        $("#filter-noPelayanan2, #filter-namaPemohon2, #filter-nikPemohon2").keyup(function() {
            search2();
        });

        var datatables2 = datagrid({
            url: '/penetapan/datagrid',
            table: "#datagrid2",
            columns: [{
                    class: "text-center"
                },
                {
                    class: "text-center"
                },
                {
                    class: "text-center"
                },
                {
                    class: "text-center"
                },
                {
                    class: "text-center"
                },
                {
                    class: ""
                },
            ],
            orders: [{
                    sortable: false
                },
                {
                    sortable: false,
                    name: "idJenisPajak"
                },
                {
                    sortable: false,
                    name: "idJenisPelayanan"
                },
                {
                    sortable: false,
                    name: "tglPelayanan"
                },
                {
                    sortable: false,
                    name: "noPelayanan"
                },
                {
                    sortable: false,
                    name: "nikPemohon"
                },
                {
                    sortable: false,
                    name: "namaPemohon"
                },
                {
                    sortable: false
                },
            ]

        });

        function search2() {
            datatables2.setFilters({
                tglPenetapan: $("#filter-tglPenetapan").val(),
                tglPenetapan: $('#filter-tglPenetapan').val(),
                nip: $('#filter-nip').val(),
                nop: $('#filter-nop').val(),
                tahun: $('#filter-tahun').val(),
                user: $('#filter-user').val()
            });
            datatables2.reload();
        }

        search2();

        function printdaftarhasilpelayananbelum(type) {
            window.open('/report/esign-tte/printdaftarhasilpelayananbelum' + type +
                '?tglPelayanan=' + $("#filter-tglPenetapan").val() +
                '&noPelayanan=' + $("#filter-noPelayanan2").val() +
                '&namaPemohon=' + $("#filter-namaPemohon2").val() +
                '&idJenisPelayanan=' + $("#filter-idJenisPelayanan2").val() +
                '&nikPemohon=' + $("#filter-nikPemohon2").val() +
                '');
        }
    </script>
@endpush
