<div data-th-fragment="tabpanel" class="tab-pane" id="navtab3" role="tabpanel">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="form-group-sm row">
                        <label class="col-sm-2 col-form-label-sm">NOP / TAHUN</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control form-control-sm masked-nop"
                                placeholder="XX.XX.XXX.XXX.XXX.XXXX.X" id="nopCariPenetepan">
                        </div>
                        <div class="col-sm-1">
                            <input type="text" class="form-control form-control-sm masked-number-4"
                                placeholder="XXXX" id="tahunCariPenetapan">
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-sm btn-primary" onclick="cariDataPenetapan()">
                                <span class="fas fa-search"></span>
                                CARI
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-rinci-sppt">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Informasi Rinci SPPT</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="informasi-rinci">

            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        function cariDataPenetapan() {
            $.ajax({
                type: 'POST',
                url: "{{ route('rinciSppt') }}",
                data: {
                    nop: $('#nopCariPenetepan').val(),
                    thn: $('#tahunCariPenetapan').val()
                },
                success: function(res) {
                    $('#informasi-rinci').html(res)
                },
                error: function(err) {
                    Swal.fire(
                        'Terjadi Kesalahan!',
                        '', 'error');
                }
            });
            $('#modal-rinci-sppt').modal('show');
        }
    </script>
@endpush
