<div data-th-fragment="tabpanel" class="tab-pane" id="navtab2" role="tabpanel">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group-sm row">
                        <label class="col-sm-2 col-form-label-sm">Propinsi</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control form-control-sm" value="{{ $kd_propinsi }}"
                                id="kd_propinsi" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group-sm row">
                        <label class="col-sm-2 col-form-label-sm">Dati II</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control form-control-sm" value="{{ $kd_dati2 }}"
                                id="kd_dati2" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group-sm row">
                        <label class="col-sm-2 col-form-label-sm">Kecamatan</label>
                        <div class="col-sm-4">
                            <select class="form-control form-control-sm" id="kd_kecamatan"
                                onchange="kelurahanCari(this.value)">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($kecamatan as $i)
                                    <option value="{{ $i->kd_kecamatan }}" id="kd_kecamatan">{{ $i->kd_kecamatan }} -
                                        {{ $i->nm_kecamatan }}
                                    </option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group-sm row">
                        <label class="col-sm-2 col-form-label-sm">Kelurahan</label>
                        <div class="col-sm-4">
                            <select class="form-control form-control-sm" id="kd_kelurahan">
                                <option value="">Silahkan Pilih</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group-sm row">
                        <label class="col-sm-2 col-form-label-sm">Tahun</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control form-control-sm datepicker-year"
                                value="{{ date('Y') }}" id="thn_pajak_sppt">
                            <input type="hidden" id="kodeKanwil">
                            <input type="hidden" id="kodeKantor">
                            <input type="hidden" id="kodeBankTunggal">
                            <input type="hidden" id="kodeBankPersepsi">
                            <input type="hidden" id="kodeTempatPemabayaran">
                            <input type="hidden" id="nilaiNjoptkp">
                            <input type="hidden" id="nilaiPbbMinimal">
                            <input type="hidden" id="vliMinB1">
                            <input type="hidden" id="vliMaxB1">
                            <input type="hidden" id="vliMinB2">
                            <input type="hidden" id="vliMaxB2">
                            <input type="hidden" id="vliMinB3">
                            <input type="hidden" id="vliMaxB3">
                            <input type="hidden" id="vliMinB4">
                            <input type="hidden" id="vliMaxB4">
                            <input type="hidden" id="vliMinB5">
                            <input type="hidden" id="vliMaxB5">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group-sm row">
                        <label class="col-sm-2 col-form-label-sm">Tgl Jatuh Tempo</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control form-control-sm datepicker-date"
                                value="{{ date('d-m-Y') }}" id="tgl_jatuh_tempo_sppt">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group-sm row">
                        <label class="col-sm-2 col-form-label-sm">Tgl Terbit</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control form-control-sm datepicker-date"
                                value="{{ date('d-m-Y') }}" id="tgl_terbit_sppt">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group-sm row">
                        <label class="col-sm-2 col-form-label-sm">NIP Rekam</label>
                        <div class="col-sm-4">
                            <select class="form-control form-control-sm" id="nip_pencetak_sppt">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($pegawais as $item)
                                    <option value="{{ $item->nip }}">{{ $item->nip }} -
                                        {{ $item->nm_pegawai }}
                                    </option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row pb-2">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-info btn-sm" onclick="tambahBody()">Tambah</button>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <input type="hidden" id="counterBody" value="1">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th colspan="5">NOP</th>
                                <th class="text-center">s/d</th>
                                <th colspan="5">NOP</th>
                            </tr>
                        </thead>
                        <tbody id="bodyTable">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-success btn-block" id="btnProses">Proses</button>
                </div>
            </div>
        </div>
        <div class="card-footer clearfix pagination-footer">
        </div>

    </div>
</div>

@push('scripts')
    <script>
        function kelurahanCari(a, b, c) {
            console.log(a);
            $.get('/kelurahan/getkelurahan-bykecamatan/' + a).then(function(response) {
                let kelurahan = response.data;

                if (kelurahan) {
                    $("#kd_kelurahan").find('option').remove().end().append(
                        '<option value="">Silahkan Pilih</option>');
                    kelurahan.forEach((kel) => {
                        var selected = "";
                        if (b == kel.kd_kelurahan) {
                            selected = " selected";
                        }
                        if (c == kel.nm_kelurahan) {
                            selected = " selected";
                        }
                        $("#kd_kelurahan").append('<option value="' + kel.kd_kelurahan + '"' +
                            selected +
                            '>' + kel.kd_kelurahan + ' - ' + kel.nm_kelurahan + '</option>');
                    })
                }
            })
        }

        function tambahBody() {
            var pesanError = "";
            if ($("#kodeKecamatan").val() == "") {
                pesanError = "Kode Kecamatan Silahkan Dipilih";
            }
            if ($("#kodeKelurahan").val() == "") {
                pesanError = "Kode Kelurahan Silahkan Dipilih";
            }
            if ($("#nipPerekam").val() == "") {
                pesanError = "NIP Perekam Silahkan Dipilih";
            }
            if (pesanError != "") {
                $("#bodyTable").empty();
                $("#pesanError").html(pesanError);
                $("#modal-sm").modal("show");
                return;
            }
            var counter = parseInt($("#counterBody").val());
            var html = '<tr id="tbody' + counter + '">';
            html += '<td>';
            html += '<center><button type="button" class="btn btn-danger btn-sm" onclick="hapusBody(' + counter +
                ')">Hapus</button></center>';
            html += '</td>';
            html +=
                '<td colspan="5"><input type="text" class="form-control form-control-sm masked-nop-urut" id="nopdari" name="nopdari[]">';
            html += '</td>';
            html += '<td class="text-center">s/d</td>';
            html +=
                '<td colspan="5"><input type="text" class="form-control form-control-sm masked-nop-urut" id="nophingga" name="nophingga[]">';
            html += '</td>';
            html += '</tr>';
            html += '<script>';
            html += '$(".masked-nop-urut").mask(\'999.9999.9\');'
            html += '<' + '/script>';
            $("#bodyTable").append(html);
            counter = counter + 1;
            $("#counterBody").val(counter)
        }

        function hapusBody(a) {
            $("#tbody" + a).remove();
        }


        $(document).on('click', '#btnProses', function(e) {
            var prop = $('#kd_propinsi').val()
            var dati = $('#kd_dati2').val()
            var kec = $('#kd_kecamatan').find(':selected').val()
            var kel = $('#kd_kelurahan').find(':selected').val()
            var thn = $('#thn_pajak_sppt').val()
            var tempos = $('#tgl_jatuh_tempo_sppt').val()
            var terbits = $('#tgl_terbit_sppt').val()

            var nopd = $('input[name="nopdari[]"]').map(function() {
                return $(this).val();
            }).get()
            var noph = $('input[name="nophingga[]"]').map(function() {
                return $(this).val();
            }).get()


            function showError(errorMessage) {
                Swal.fire({
                    title: 'Proses Penetapan Terseleksi Gagal!',
                    text: errorMessage,
                    icon: 'error'
                });
            }

            // validasi inputan tanggalnya
            if (tempos === '') {
                Swal.fire({
                    title: 'Error',
                    text: 'Tanggal Jatuh Tempo tidak boleh kosong kak!',
                    icon: 'error',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK'
                });
                // Stop the process
                $('#btn-submit').text('Proses').removeAttr('disabled');
            } else
            if (terbits === '') {
                Swal.fire({
                    title: 'Error',
                    text: 'Tanggal Terbit tidak boleh kosong kak!',
                    icon: 'error',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK'
                });
                // Stop the process
                $('#btn-submit').text('Proses').removeAttr('disabled');
            } else
            if (tempos < terbits) {
                Swal.fire({
                    title: 'Error',
                    text: 'Tanggal Jatuh Tempo tidak boleh lebih kecil dari Tanggal Terbit!',
                    icon: 'error',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK'
                });
                // Stop the process
                $('#btn-submit').text('Proses').removeAttr('disabled');
            } else
                // validasi inputan nopnya tidak boleh kosong
                if (nopd.some(nop => nop.trim() === '')) {
                    Swal.fire({
                        title: 'Error',
                        text: 'NOP awal tidak boleh kosong kak!',
                        icon: 'error',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'OK'
                    });
                    // Stop the process
                    $('#btn-submit').text('Proses').removeAttr('disabled');
                } else
            if (noph.some(nop => nop.trim() === '')) {
                Swal.fire({
                    title: 'Error',
                    text: 'NOP akhir tidak boleh kosong kak!',
                    icon: 'error',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK'
                });
                // Stop the process
                $('#btn-submit').text('Proses').removeAttr('disabled');
            } else {
                Swal.fire({
                    title: 'Penetapan Terseleksi',
                    text: 'Lakukan penetapan terseleksi?',
                    icon: 'info',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya!',
                    cancelButtonText: 'Tidak'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Swal.fire({
                            position: 'top',
                            title: 'Proses sedang berlangsung !',
                            html: 'Mohon Tunggu...<div id="prosesNOP"></div>',
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            didOpen: () => {
                                Swal.showLoading();
                                var hitungDat = 0;
                                var totalDat = -1;
                                var isFailed = false;

                                if (nopd.length == noph.length) {
                                    for (let i = 0; i < nopd.length; i++) {
                                        nopdari = nopd[i];
                                        nophingga = noph[i];

                                        $.ajax({
                                            type: 'POST',
                                            url: "{{ route('objekPajak') }}",
                                            data: {
                                                prop,
                                                dati,
                                                kec,
                                                kel,
                                                nopdari,
                                                nophingga
                                            },
                                            success: function(nop) {
                                                if (totalDat == -1) totalDat = 0;
                                                totalDat += nop.length;

                                                for (let j = 0; j < nop
                                                    .length; j++) {
                                                    $.ajax({
                                                        type: 'POST',
                                                        async: false,
                                                        url: "{{ route('terseleksi') }}",
                                                        dataType: "JSON",
                                                        data: {
                                                            prop: prop,
                                                            dati: dati,
                                                            kec,
                                                            kel,
                                                            thn,
                                                            tempos,
                                                            terbits,

                                                            nip: $(
                                                                    '#nip_pencetak_sppt'
                                                                )
                                                                .val(),
                                                            nop: nop[j]
                                                        },
                                                        complete: function(
                                                            data) {
                                                            hitungDat++;
                                                            $('#prosesNOP')
                                                                .html(
                                                                    '<p>Proses Penetapan NOP ' +
                                                                    nop[
                                                                        j
                                                                    ]
                                                                    .kd_propinsi +
                                                                    '.' +
                                                                    nop[
                                                                        j
                                                                    ]
                                                                    .kd_dati2 +
                                                                    '.' +
                                                                    nop[
                                                                        j
                                                                    ]
                                                                    .kd_kecamatan +
                                                                    '.' +
                                                                    nop[
                                                                        j
                                                                    ]
                                                                    .kd_kelurahan +
                                                                    '.' +
                                                                    nop[
                                                                        j
                                                                    ]
                                                                    .kd_blok +
                                                                    '-' +
                                                                    nop[
                                                                        j
                                                                    ]
                                                                    .no_urut +
                                                                    '.' +
                                                                    nop[
                                                                        j
                                                                    ]
                                                                    .kd_jns_op +
                                                                    '</p>'
                                                                );

                                                            console.log(
                                                                hitungDat +
                                                                '=' +
                                                                totalDat
                                                            );

                                                            // Cek apakah ada pesan error di dalam data yang diterima
                                                            if (data
                                                                .responseJSON &&
                                                                data
                                                                .responseJSON
                                                                .tipe ===
                                                                'error'
                                                            ) {
                                                                isFailed
                                                                    =
                                                                    true; // Tandai bahwa proses gagal

                                                                if (hitungDat ===
                                                                    totalDat
                                                                ) {
                                                                    clearInterval
                                                                        (
                                                                            inter
                                                                        );
                                                                    Swal.fire(
                                                                        "Proses Penetapan Terseleksi Gagal!",
                                                                        data
                                                                        .responseJSON
                                                                        .message,
                                                                        "error"
                                                                    );
                                                                    return false; // Menghentikan proses looping
                                                                }
                                                            }

                                                            if (hitungDat ===
                                                                totalDat
                                                            ) {
                                                                clearInterval
                                                                    (
                                                                        inter
                                                                    );
                                                                if (!
                                                                    isFailed
                                                                ) { // Jika proses tidak gagal, tampilkan notifikasi selesai
                                                                    Swal.fire(
                                                                        "Proses Penetapan Terseleksi Selesai.",
                                                                        "",
                                                                        "success"
                                                                    );
                                                                }
                                                            }
                                                        }
                                                    });
                                                }
                                            },
                                            error: function(err) {
                                                Swal.fire(
                                                    'Proses Penetapan Terseleksi Gagal!',
                                                    '', 'error');
                                            }
                                        });
                                    }
                                    inter = setInterval(() => {
                                        if (hitungDat === totalDat) {
                                            clearInterval(inter);
                                            Swal.fire(
                                                "Proses Penetapan Terseleksi Selesai.",
                                                "", "success")
                                        }
                                    }, 1000);
                                }
                            }
                        });
                    } else {
                        $('#btn-submit').text('Proses').removeAttr('disabled');
                    }
                });
            }
        });
    </script>
@endpush
