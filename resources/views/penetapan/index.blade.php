@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} | Penetapan terseleksi
@endsection

@section('content')
    <section class="content" layout:fragment="content">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <section>
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Penetapan terseleksi</li>
                            </ol>
                        </section>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                @if (session('success'))
                    <div class="alert alert-dismissible alert_customs_success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-check"></i> Success!</h5>
                        <span>{{ session('success') }}</span>
                    </div>
                @endif

                <div class="row">
                    <div class="col-12">
                        <div class="card card-danger card-outline card-outline-tabs ">
                            <div class="card-header p-0 border-bottom-0">
                                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="pill" href="#navtab1" role="tab"
                                            aria-controls="navtab1" aria-selected="true">Data</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="pill" href="#navtab2" role="tab"
                                            aria-controls="navtab2" aria-selected="true">Form</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="pill" href="#navtab3" role="tab"
                                            aria-controls="navtab2" aria-selected="true">Sudah Ditetapkan</a>
                                    </li>

                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-three-tabContent">
                                    @include('penetapan.datagrid.sudah')
                                    @include('penetapan.form.form')
                                    @include('penetapan.form.sudahditetapkan')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection
