@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Setting Jenis Hasil Pelayanan
@endsection

@section('content')
    <section class="content">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Setting</a></li>
                            <li class="breadcrumb-item active">Grid</li>
                        </ol>
                    </div>
                    <div class="col-12">
                        <div class="info-box" id="background2">
                            <div class="col-sm-3">
                                <div class="description-block" style="float: left;">
                                    <h1 class="description-header">SETTING JENIS HASIL PELAYANAN</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-outline card-info">
                            <div class="card-header">
                                <div class="col-sm-12" style="margin-bottom: 10px;">
                                    Form Setting Jenis Hasil Pelayanan [Tambah]
                                </div>
                            </div>
                            <form class="form-horizontal form-validation" method="POST"
                                action="{{ route('setting-jenis-hasil-pelayanan.store') }} ">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_id_jenis_pajak">Jenis Pajak</label>
                                        <div class="col-sm-4">
                                            <input id="s_id_hasil_pelayanan" name="s_id_hasil_pelayanan" type="hidden">
                                            <select class="form-control" id="s_id_jenis_pajak" name="s_id_jenis_pajak"
                                                onchange="comboJenisPelayanan(this.value)" required>
                                                <option value="">Silahkan Pilih</option>
                                                @foreach ($jenisPajak as $row)
                                                    <option value="{{ $row['s_id_jenis_pajak'] }}">
                                                        {{ $row['s_nama_jenis_pajak'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_id_jenis_pelayanan">Jenis
                                            Pelayanan</label>
                                        <div class="col-sm-4">
                                            <select class="form-control" name="s_id_jenis_pelayanan"
                                                id="s_id_jenis_pelayanan" required>
                                                <option value="">Silahkan Pilih</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_nama_hasil_pelayanan">Hasil
                                            Pelayanan</label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name="s_nama_hasil_pelayanan"
                                                id="s_nama_hasil_pelayanan" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-info" type="submit">SIMPAN</button>
                                    <a class="btn btn-danger btn-default float-right"
                                        href="{{ route('setting-jenis-hasil-pelayanan') }}">BATAL</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
    <script type="text/javascript">
        function comboJenisPelayanan(id) {
            $.ajax({
                url: '/setting-jenis-hasil-pelayanan/combo-jenis-pelayanan/' + id,
                type: 'POST',
                data: {
                    s_id_jenis_pajak: id
                }
            }).then(function(data) {
                $("#s_id_jenis_pelayanan option[value!='']").remove();
                $.each(data, function(key, val) {
                    $('#s_id_jenis_pelayanan').append(
                        `<option value="${val.s_id_jenis_pelayanan}">${val.s_nama_jenis_pelayanan}</option>`
                    );
                });
            });
        }
    </script>
@endpush
