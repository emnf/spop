@extends('layouts.print')

@push('styles')
<style>
    @page {
        margin-left: 80px;
        margin-right: 80px;
    }
</style>
@endpush

@section('title')
<title>
    SK-PENGURANGAN
</title>
@endsection

@section('body')
<table style="width: 100%">
    <tr>
        <td class="text_tengah" style="width: 15%; vertical-align: middle">
            {{-- <img src="{{ asset('logo-pemda/8L8T3rqQtfH5YcbCvEq4lfNQBIcETwXJzvKBcOXM.png') }}" height="120px" alt="dua"> --}}
            <img src="{{ asset($pemda->s_logo) }}" height="120px" alt="dua">
        </td>
        <td class="text_tengah" style="width: 60%">
            <span class="font_empatbelas">PEMERINTAH {{ $pemda->s_nama_kabkota }}</span><br>
            <span class="font_enambelas" style="text-transform: uppercase;">{{ $pemda->s_nama_instansi }}</span><br>
            <span style="text-transform: uppercase;">{{ $pemda->s_alamat_instansi . ' Telp/Fax ' . $pemda->s_notelp_instansi }}</span>
        </td>
        <td style="width: 15%">
            &nbsp;&nbsp;
        </td>
    </tr>
</table>
<hr class="doble_line" />

<div class="text_tengah" style="padding-bottom: 10px;">
    KEPUTUSAN KEPALA BADAN KEUANGAN DAN ASET DAERAH<br> NOMOR : KEP-{{ $pelayanan->hasilPelayanan[0]->t_no_sk ?? "......................"}}
</div>
<div class="text_tengah">
    TENTANG <br />
    PEMBERIAN PENGURANGAN<br />
    PAJAK BUMI DAN BANGUNAN YANG TERUTANG
</div>
{{-- @dd($pemda) --}}
<table class="font_duabelas" style="width: 100%; text-align: justify;margin-top: 20px;">
    <tr>
        <td>Memperhatikan</td>
        <td>:</td>
        <td>Surat permohonan pengurangan pajak terutang tanggal {{ !is_null($pelayanan->t_tgl_pelayanan) ? date('d/m/Y',
            strtotime($pelayanan->t_tgl_pelayanan)) : "../../...." }}, Nomor {{
            $pelayanan->noPelayanan ?? "........................"}} dari
            Wajib Pajak {{ $pelayanan->wajibPajak->t_nama_wp ?? "................" }}.</td>
    </tr>
    <tr>
        <td>Menimbang</td>
        <td>:</td>
        <td>
            <ol type="a" style="margin-top: 0;">
                <li>hasil pemeriksaan sederhana kantor permohonan pengurangan pajak sebagaimana
                    dalam Berita Acara Pemeriksaan Sederhana Kantor tanggal 10 Oktober 2022, Nomor :
                    {{ $pelayanan->hasilPelayanan[0]->t_no_sk ?? "......................"}}.</li>
                <li>bahwa setelah dipertimbangkan, maka besarnya pajak yang disetujui perlu ditetapkan
                    dengan Keputusan Kepala Dinas.</li>
            </ol>
        </td>
    </tr>
    <tr>
        <td>Mengingat</td>
        <td>:</td>
        <td>
            Pasal 2 Peraturan Bupati Kulon Progo No. 78 Tahun 2013 tentang Tatacara Pengurangan,Keberatan, dan Banding,
            Pajak Bumi dan Bangunan Perdesaan dan Perkotaan
        </td>
    </tr>
    <tr>
        <td colspan="3" class="text_tengah">MEMUTUSKAN</td>
    </tr>
    <tr>
        <td>Menetapkan</td>
        <td>:</td>
        <td>
            KEPUTUSAN KEPALA BADAN KEUANGAN DAN ASET DAERAH TENTANG
            PENGURANGAN PAJAK BUMI DAN BANGUNAN YANG TERUTANG.
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td class="text_tengah">
            Pasal 1
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            (1) Mengabulkan Permohonan Pengurangan Pajak Bumi dan Bangunan yang
            terutang kepada :
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <table style="width: 100%">
                <tr>
                    <td style="width: 20px;">a. </td>
                    <td style="width: 30%">Nama Wajib Pajak</td>
                    <td>: {{ $pelayanan->wajibPajak->t_nama_wp  }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Alamat Wajib Pajak</td>
                    <td>: {{ $pelayanan->wajibPajak->alamatLengkap }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>NOP</td>
                    <td>: {{ $pelayanan->objekPajak->nopFormated }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Tahun Pajak</td>
                    <td>: {{ $pelayanan->t_tahun_pajak }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Pajak Terhutang</td>
                    <td>: Rp. {{ number_format($pelayanan->t_his_pbb_yg_harus_dibayar, 0, ',', '.') }}</td>
                </tr>
                <tr>
                    <td>&nbsp; </td>
                    <td>&nbsp;</td>
                    <td>&nbsp; </td>
                </tr>
                <tr>
                    <td>b. </td>
                    <td>Letak Objek Pajak</td>
                    <td>: {{ $pelayanan->objekPajak->t_jalan_op }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>RT/RW</td>
                    <td>: {{ $pelayanan->objekPajak->t_rt_op .'/'. $pelayanan->objekPajak->t_rw_op }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Kelurahan/Desa</td>
                    <td>: {{ $pelayanan->objekPajak->t_kelurahan_op }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Kecamatan</td>
                    <td>: {{ $pelayanan->objekPajak->t_kecamatan_op }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Kota/Kabupaten</td>
                    <td>: {{ $pemda->s_nama_kabkota }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Propinsi</td>
                    <td>: {{ $pemda->s_nama_prov }}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            (2) Sesuai dengan ketentuan ayat (1) di atas, maka besarnya PBB terutang adalah sebagai
            berikut :
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <table style="width: 100%">
                <tr>
                    <td>a. </td>
                    <td>Pajak terutang menurut</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td>SPPT</td>
                    <td>Rp. </td>
                    <td style="text-align: right">{{ number_format($pelayanan->t_his_pbb_yg_harus_dibayar, 0, ',', '.') }}</td>
                </tr>
                <tr>
                    <td>b. </td>
                    <td>Besarnya pengurangan</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td>({{ $pelayanan->validasi->t_besar_pengurangan }}% x Rp. {{ number_format($pelayanan->t_his_pbb_yg_harus_dibayar, 0, ',', '.') }})</td>
                    <td>Rp. </td>
                    @php
                        $besarPengurangan = round(($pelayanan->validasi->t_besar_pengurangan/100) * $pelayanan->t_his_pbb_yg_harus_dibayar);
                        $sisaPajak = $pelayanan->t_his_pbb_yg_harus_dibayar-$besarPengurangan;
                    @endphp
                    <td style="text-align: right">{{ number_format($besarPengurangan, 0, ',', '.') }}</td>
                </tr>
                <tr>
                    <td>c. </td>
                    <td>Jumlah pajak terutang setelah pengurangan (a-b)</td>
                    <td>Rp.</td>
                    <td style="text-align: right">{{ number_format($sisaPajak, 0, ',', '.') }}</td>
                </tr>
                <tr>
                    <td colspan="4" style="text-transform: uppercase;">({{ App\Helpers\PelayananHelper::kekata($sisaPajak) }} RUPIAH)</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td class="text_tengah">
            Pasal 2
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            Apabila dikemudian hari ternyata terdapat kekeliruan dalam Keputusan ini maka akan dibetulkan sebagaimana
            mestinya.
        </td>
    </tr>
</table>

<table style="width: 100%; margin-top: 15px;">
    <tr>
        <td style="width: 50%">&nbsp;</td>
        <td>
            Ditetapkan di {{ ucfirst(strtolower($pemda->s_nama_ibukota_kabkota)) }}<br />
            Pada tanggal {{ date('d-m-Y', strtotime($pelayanan->hasilPelayanan[0]->t_tgl_sk)) }}<br />
            KEPALA BADAN<br />
            <br /><br /><br />
            <span style="text-decoration: underline; font-weight: 400">NAMA KEPALA BADAN</span><br />
            <span style="font-weight: 700">NIP KEPALA BADAN</span>
        </td>
    </tr>
</table>

<div style="font-size: 10pt;">
    Tembusan Surat Keputusan ini disampaikan kepada Yth.:
    <ol>
        <li>Bupati Kulon Progo</li>
        <li>Kepala BKAD Kabupaten Kulon progo</li>
        <li>Kepala Bidang PAJAK Kabupaten Kulon Progo</li>
        <li>Arsip</li>

    </ol>
</div>
@endsection
