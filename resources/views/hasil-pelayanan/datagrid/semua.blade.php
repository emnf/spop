<div data-th-fragment="tabpanel" class="tab-pane fade" id="navtab3" role="tabpanel">
    <div class="row">
        <div class="col-12" style="margin-bottom: 10px;">
            <button class="btn btn-danger btn-sm" onclick="printdaftarhasilpelayanansemua('PDF')"><span
                    class="fas fa-file-pdf"></span> &nbsp;PDF</button>
            <button class="btn btn-success btn-sm" onclick="printdaftarhasilpelayanansemua('XLS')"><span
                    class="fas fa-file-excel"></span> &nbsp;XLS</button>
        </div>
    </div>
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-bordered table-striped table-hover" id="datagrid3">
                <thead>
                    <tr>
                        <th style="background-color: #1fc8e3" class="text-center">No</th>
                        <th style="background-color: #1fc8e3" class="text-center">Jenis Pajak</th>
                        <th style="background-color: #1fc8e3" class="text-center">Nama Pelayanan</th>
                        <th style="background-color: #1fc8e3" class="text-center">Tgl. Permohonan</th>
                        <th style="background-color: #1fc8e3" class="text-center">No. Pelayanan</th>
                        <th style="background-color: #1fc8e3" class="text-center">NIK Pemohon</th>
                        <th style="background-color: #1fc8e3" class="text-center">Nama Pemohon</th>
                        <th style="background-color: #1fc8e3" class="text-center">Status</th>
                        <th style="background-color: #1fc8e3" class="text-center">No SK</th>
                        <th style="background-color: #1fc8e3" class="text-center">Tgl. SK</th>
                        <th style="background-color: #1fc8e3" class="text-center">Ket. SK</th>
                        <th style="background-color: #1fc8e3" class="text-center">Action</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>
                            {{-- <select class="form-control form-control-sm" id="filter-idJenisPajak3">
                                <option value="">Silahkan Pilih</option>
                                @foreach($jenisPajak as $pajak)
                                <option value="{{ $pajak->s_id_jenis_pajak }}">{{ $pajak->s_nama_jenis_pajak }}</option>
                                @endforeach
                            </select> --}}
                        </th>
                        <th>
                            <select class="form-control form-control-sm" id="filter-idJenisPelayanan3">
                                <option value="">Silahkan Pilih</option>
                                @foreach($jenisPelayanan as $pelayanan)
                                <option value="{{ $pelayanan->s_id_jenis_pelayanan }}">{{ $pelayanan->s_nama_jenis_pelayanan }}</option>
                                @endforeach
                            </select>
                        </th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-tglPelayanan3"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-noPelayanan3"></th>
                        <th><input class="form-control form-control-sm masked-nik" type="text" id="filter-nikPemohon3"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-namaPemohon3"></th>
                        <th>
                            <select class="form-control form-control-sm" id="filter-status3">
                                <option value="">Silahkan Pilih</option>
                                <option value="1">Belum</option>
                                <option value="2">Sudah</option>
                            </select>
                        </th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-noSk3"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-tglSk3"></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="12"> Tidak ada data.</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-12 pagination-footer"></div>
    </div>
</div>

@push('scripts')
<script th:inline="javascript" data-th-fragment="js">
    $('#filter-tglPelayanan3, #filter-tglSk3').daterangepicker({
        autoUpdateInput: false,
         locale: {
            cancelLabel: 'Clear'
         }
    });

    $('#filter-tglPelayanan3, #filter-tglSk3').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        search3();
    });

    $('#filter-tglPelayanan3, #filter-tglSk3').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        search3();
    });

    $("#filter-idJenisPajak3, #filter-idJenisPelayanan3, #filter-status3").change(function(){
        search3();
    });

    $("#filter-noPelayanan3, #filter-namaPemohon3, #filter-nikPemohon3, #filter-noSk3").keyup(function(){
        search3();
    });

    var datatables3 = datagrid({
        url: '/hasil-pelayanan/datagrid-semua',
        table: "#datagrid3",
        columns: [
            {class: ""},
            {class: ""},
            {class: ""},
            {class: ""},
            {class: ""},
            {class: ""},
            {class: ""},
            {class: ""},
            {class: ""},
            {class: ""},
            {class: ""},
        ],
        orders: [
            {sortable: false},
            {sortable: true, name: "idJenisPajak"},
            {sortable: true, name: "idJenisPelayanan"},
            {sortable: true, name: "tglPelayanan"},
            {sortable: true, name: "noPelayanan"},
            {sortable: true, name: "nikPemohon"},
            {sortable: true, name: "namaPemohon"},
            {sortable: true, name: "suratKeputusan.caseStatus"},
            {sortable: true, name: "suratKeputusan.noSk"},
            {sortable: true, name: "suratKeputusan.tglSk"},
            {sortable: false},
            {sortable: false},
        ],
        action: [
            {
                name: 'sk-download',
                btnClass: 'btn btn-primary btn-xs',
                btnText: '<i class="fas fa-file-download"> Download SK</i>'
            },
            {
                name: 'sk-tambah',
                btnClass: 'btn btn-danger btn-xs',
                btnText: '<i class="fas fa-file-upload"> Unggah Hasil Pelayanan</i>'
            },
        ]

    });

    function search3(){
        datatables3.setFilters({
            idJenisPajak: $("#filter-idJenisPajak3").val(),
            tglPelayanan: $("#filter-tglPelayanan3").val(),
            noPelayanan: $("#filter-noPelayanan3").val(),
            idJenisPelayanan: $("#filter-idJenisPelayanan3").val(),
            namaPemohon: $("#filter-namaPemohon3").val(),
            nikPemohon: $("#filter-nikPemohon3").val(),
            status: $("#filter-status3").val(),
            noSk: $("#filter-noSk3").val(),
            tglSk: $("#filter-tglSk3").val(),
        });
        datatables3.reload();
    }

    search3();

    function printdaftarhasilpelayanansemua(type){
        window.open('/hasil-pelayanan/cetak-daftar-hasil-pelayanan?format=' + type
        + '&idJenisPajak=' + $("#filter-idJenisPajak3").val()
        + '&tglPelayanan=' + $("#filter-tglPelayanan3").val()
        + '&noPelayanan=' + $("#filter-noPelayanan3").val()
        + '&namaPemohon=' + $("#filter-namaPemohon3").val()
        + '&idJenisPelayanan=' + $("#filter-idJenisPelayanan3").val()
        + '&nikPemohon=' + $("#filter-nikPemohon3").val()
        + '&noSk=' + $("#filter-noSk3").val()
        + '&tglSk=' + $("#filter-tglSk3").val()
        + '&status=' + $("#filter-status3").val()
        + '');
    }
</script>
@endpush
