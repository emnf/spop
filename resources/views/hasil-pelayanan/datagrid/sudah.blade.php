<div data-th-fragment="tabpanel" class="tab-pane fade" id="navtab2" role="tabpanel">
    <div class="row">
        <div class="col-12" style="margin-bottom: 10px;">
<!--            <button class="btn btn-danger btn-sm" onclick="printdaftarhasilpelayanansudah('PDF')"><span class="fas fa-file-pdf"></span> &nbsp;PDF</button>-->
<!--            <button class="btn btn-success btn-sm" onclick="printdaftarhasilpelayanansudah('XLS')"><span class="fas fa-file-excel"></span> &nbsp;XLS</button>-->
        </div>
    </div>
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-bordered table-striped table-hover" id="datagrid2">
                <thead>
                    <tr>
                        <th style="background-color: #1fc8e3" class="text-center">No</th>
                        <th style="background-color: #1fc8e3" class="text-center">Jenis Pajak</th>
                        <th style="background-color: #1fc8e3" class="text-center">Nama Pelayanan</th>
                        <th style="background-color: #1fc8e3" class="text-center">Tgl. Permohonan</th>
                        <th style="background-color: #1fc8e3" class="text-center">No. Pelayanan</th>
                        <th style="background-color: #1fc8e3" class="text-center">NIK Pemohon</th>
                        <th style="background-color: #1fc8e3" class="text-center">Nama Pemohon</th>
                        <th style="background-color: #1fc8e3" class="text-center">No SK</th>
                        <th style="background-color: #1fc8e3" class="text-center">Tgl. SK</th>
                        <th style="background-color: #1fc8e3" class="text-center">Ket. SK</th>
                        <th style="background-color: #1fc8e3" class="text-center">Action</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>
                            {{-- <select class="form-control form-control-sm" id="filter-idJenisPajak2">
                                <option value="">Silahkan Pilih</option>
                                @foreach($jenisPajak as $pajak)
                                <option value="{{ $pajak->s_id_jenis_pajak }}">{{ $pajak->s_nama_jenis_pajak }}</option>
                                @endforeach
                            </select> --}}
                        </th>
                        <th>
                            <select class="form-control form-control-sm" id="filter-idJenisPelayanan2">
                                <option value="">Silahkan Pilih</option>
                                @foreach($jenisPelayanan as $pelayanan)
                                <option value="{{ $pelayanan->s_id_jenis_pelayanan }}">{{ $pelayanan->s_nama_jenis_pelayanan }}</option>
                                @endforeach
                            </select>
                        </th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-tglPelayanan2"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-noPelayanan2"></th>
                        <th><input class="form-control form-control-sm masked-nik" type="text" id="filter-nikPemohon2"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-namaPemohon2"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-noSk2"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-tglSk2"></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="11"> Tidak ada data.</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-12 pagination-footer"></div>
    </div>
</div>

@push('scripts')
<script th:inline="javascript" data-th-fragment="js">
    $('#filter-tglPelayanan2, #filter-tglSk2').daterangepicker({
        autoUpdateInput: false,
         locale: {
            cancelLabel: 'Clear'
         }
    });

    $('#filter-tglPelayanan2, #filter-tglSk2').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        search2();
    });

    $('#filter-tglPelayanan2, #filter-tglSk2').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        search2();
    });

    $("#filter-idJenisPajak2, #filter-idJenisPelayanan2").change(function(){
        search2();
    });

    $("#filter-noPelayanan2, #filter-namaPemohon2, #filter-nikPemohon2, #filter-noSk2").keyup(function(){
        search2();
    });

    var datatables2 = datagrid({
        url: '/hasil-pelayanan/datagrid-sudah',
        table: "#datagrid2",
        columns: [
            {class: ""},
            {class: ""},
            {class: ""},
            {class: ""},
            {class: ""},
            {class: ""},
            {class: ""},
            {class: ""},
            {class: ""},
            {class: ""},
        ],
        orders: [
            {sortable: false},
            {sortable: true, name: "idJenisPajak"},
            {sortable: true, name: "idJenisPelayanan"},
            {sortable: true, name: "tglPelayanan"},
            {sortable: true, name: "noPelayanan"},
            {sortable: true, name: "nikPemohon"},
            {sortable: true, name: "namaPemohon"},
            {sortable: true, name: "suratKeputusan.noSk"},
            {sortable: true, name: "suratKeputusan.tglSk"},
            {sortable: false},
            {sortable: false},
        ],
        action: [
            {
                name: 'sk-download',
                btnClass: 'btn btn-primary btn-xs',
                btnText: '<i class="fas fa-file-download"> Download SK</i>'
            },
            {
                name: 'cetak-sk-pengurangan',
                btnClass: 'btn btn-primary btn-xs',
                btnText: '<i class="fas fa-print"> Cetak SK</i>'
            },
        ]

    });

    function search2(){
        datatables2.setFilters({
            idJenisPajak: $("#filter-idJenisPajak2").val(),
            tglPelayanan: $("#filter-tglPelayanan2").val(),
            noPelayanan: $("#filter-noPelayanan2").val(),
            idJenisPelayanan: $("#filter-idJenisPelayanan2").val(),
            namaPemohon: $("#filter-namaPemohon2").val(),
            nikPemohon: $("#filter-nikPemohon2").val(),
            noSk: $("#filter-noSk2").val(),
            tglSk: $("#filter-tglSk2").val(),
        });
        datatables2.reload();
    }

    search2();

    function printdaftarhasilpelayanansudah(type){
        window.open('/report/hasil-pelayanan/printdaftarhasilpelayanansudah' + type
        + '?idJenisPajak=' + $("#filter-idJenisPajak2").val()
        + '&tglPelayanan=' + $("#filter-tglPelayanan2").val()
        + '&noPelayanan=' + $("#filter-noPelayanan2").val()
        + '&namaPemohon=' + $("#filter-namaPemohon2").val()
        + '&idJenisPelayanan=' + $("#filter-idJenisPelayanan2").val()
        + '&nikPemohon=' + $("#filter-nikPemohon2").val()
        + '&noSk=' + $("#filter-noSk2").val()
        + '&tglSk=' + $("#filter-tglSk2").val()
        + '');
    }

    function cetakskpengurangan(a) {
        window.open('/hasil-pelayanan/cetak-sk-pengurangan/' + a, "_blank");
    }

    function downloadsk(url, name) {
        let a = $("<a>").attr("href", url).attr("download", name).appendTo("body");
        a[0].click();
        a.remove();
    }
</script>
@endpush
<div data-th-fragment="tabpanel" class="tab-pane fade" id="navtab2" role="tabpanel">
    <div class="row">
        <div class="col-12" style="margin-bottom: 10px;">
<!--            <button class="btn btn-danger btn-sm" onclick="printdaftarhasilpelayanansudah('PDF')"><span class="fas fa-file-pdf"></span> &nbsp;PDF</button>-->
<!--            <button class="btn btn-success btn-sm" onclick="printdaftarhasilpelayanansudah('XLS')"><span class="fas fa-file-excel"></span> &nbsp;XLS</button>-->
        </div>
    </div>
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-bordered table-striped table-hover" id="datagrid2">
                <thead>
                    <tr>
                        <th style="background-color: #1fc8e3" class="text-center">No</th>
                        <th style="background-color: #1fc8e3" class="text-center">Jenis Pajak</th>
                        <th style="background-color: #1fc8e3" class="text-center">Nama Pelayanan</th>
                        <th style="background-color: #1fc8e3" class="text-center">Tgl. Permohonan</th>
                        <th style="background-color: #1fc8e3" class="text-center">No. Pelayanan</th>
                        <th style="background-color: #1fc8e3" class="text-center">NIK Pemohon</th>
                        <th style="background-color: #1fc8e3" class="text-center">Nama Pemohon</th>
                        <th style="background-color: #1fc8e3" class="text-center">No SK</th>
                        <th style="background-color: #1fc8e3" class="text-center">Tgl. SK</th>
                        <th style="background-color: #1fc8e3" class="text-center">Ket. SK</th>
                        <th style="background-color: #1fc8e3" class="text-center">Action</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>
                            {{-- <select class="form-control form-control-sm" id="filter-idJenisPajak2">
                                <option value="">Silahkan Pilih</option>
                                @foreach($jenisPajak as $pajak)
                                <option value="{{ $pajak->s_id_jenis_pajak }}">{{ $pajak->s_nama_jenis_pajak }}</option>
                                @endforeach
                            </select> --}}
                        </th>
                        <th>
                            <select class="form-control form-control-sm" id="filter-idJenisPelayanan2">
                                <option value="">Silahkan Pilih</option>
                                @foreach($jenisPelayanan as $pelayanan)
                                <option value="{{ $pelayanan->s_id_jenis_pelayanan }}">{{ $pelayanan->s_nama_jenis_pelayanan }}</option>
                                @endforeach
                            </select>
                        </th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-tglPelayanan2"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-noPelayanan2"></th>
                        <th><input class="form-control form-control-sm masked-nik" type="text" id="filter-nikPemohon2"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-namaPemohon2"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-noSk2"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-tglSk2"></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="11"> Tidak ada data.</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-12 pagination-footer"></div>
    </div>
</div>

@push('scripts')
<script th:inline="javascript" data-th-fragment="js">
    $('#filter-tglPelayanan2, #filter-tglSk2').daterangepicker({
        autoUpdateInput: false,
         locale: {
            cancelLabel: 'Clear'
         }
    });

    $('#filter-tglPelayanan2, #filter-tglSk2').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        search2();
    });

    $('#filter-tglPelayanan2, #filter-tglSk2').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        search2();
    });

    $("#filter-idJenisPajak2, #filter-idJenisPelayanan2").change(function(){
        search2();
    });

    $("#filter-noPelayanan2, #filter-namaPemohon2, #filter-nikPemohon2, #filter-noSk2").keyup(function(){
        search2();
    });

    var datatables2 = datagrid({
        url: '/hasil-pelayanan/datagrid-sudah',
        table: "#datagrid2",
        columns: [
            {class: ""},
            {class: ""},
            {class: ""},
            {class: ""},
            {class: ""},
            {class: ""},
            {class: ""},
            {class: ""},
            {class: ""},
            {class: ""},
        ],
        orders: [
            {sortable: false},
            {sortable: true, name: "idJenisPajak"},
            {sortable: true, name: "idJenisPelayanan"},
            {sortable: true, name: "tglPelayanan"},
            {sortable: true, name: "noPelayanan"},
            {sortable: true, name: "nikPemohon"},
            {sortable: true, name: "namaPemohon"},
            {sortable: true, name: "suratKeputusan.noSk"},
            {sortable: true, name: "suratKeputusan.tglSk"},
            {sortable: false},
            {sortable: false},
        ],
        action: [
            {
                name: 'sk-download',
                btnClass: 'btn btn-primary btn-xs',
                btnText: '<i class="fas fa-file-download"> Download SK</i>'
            },
            {
                name: 'cetak-sk',
                btnClass: 'btn btn-primary btn-xs',
                btnText: '<i class="fas fa-print"> Cetak SK</i>'
            },
        ]

    });

    function search2(){
        datatables2.setFilters({
            idJenisPajak: $("#filter-idJenisPajak2").val(),
            tglPelayanan: $("#filter-tglPelayanan2").val(),
            noPelayanan: $("#filter-noPelayanan2").val(),
            idJenisPelayanan: $("#filter-idJenisPelayanan2").val(),
            namaPemohon: $("#filter-namaPemohon2").val(),
            nikPemohon: $("#filter-nikPemohon2").val(),
            noSk: $("#filter-noSk2").val(),
            tglSk: $("#filter-tglSk2").val(),
        });
        datatables2.reload();
    }

    search2();

    function printdaftarhasilpelayanansudah(type){
        window.open('/report/hasil-pelayanan/printdaftarhasilpelayanansudah' + type
        + '?idJenisPajak=' + $("#filter-idJenisPajak2").val()
        + '&tglPelayanan=' + $("#filter-tglPelayanan2").val()
        + '&noPelayanan=' + $("#filter-noPelayanan2").val()
        + '&namaPemohon=' + $("#filter-namaPemohon2").val()
        + '&idJenisPelayanan=' + $("#filter-idJenisPelayanan2").val()
        + '&nikPemohon=' + $("#filter-nikPemohon2").val()
        + '&noSk=' + $("#filter-noSk2").val()
        + '&tglSk=' + $("#filter-tglSk2").val()
        + '');
    }

    function cetaksk(a) {
        window.open('/hasil-pelayanan/cetak-sk/' + a, "_blank");
    }

    function downloadsk(url, name) {
        let a = $("<a>").attr("href", url).attr("download", name).appendTo("body");
        a[0].click();
        a.remove();
    }
</script>
@endpush
