
<div data-th-fragment="tabpanel" class="tab-pane fade active show" id="navtab1" role="tabpanel">
    <div class="row">
        <div class="col-12" style="margin-bottom: 10px;">
<!--            <button class="btn btn-danger btn-sm" onclick="printdaftarhasilpelayananbelum('PDF')"><span class="fas fa-file-pdf"></span> &nbsp;PDF</button>-->
<!--            <button class="btn btn-success btn-sm" onclick="printdaftarhasilpelayananbelum('XLS')"><span class="fas fa-file-excel"></span> &nbsp;XLS</button>-->
        </div>
    </div>
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-bordered table-striped table-hover" id="datagrid1">
                <thead>
                    <tr>
                        <th style="background-color: #1fc8e3" class="text-center">No</th>
                        <th style="background-color: #1fc8e3" class="text-center">Jenis Pajak</th>
                        <th style="background-color: #1fc8e3" class="text-center">Nama Pelayanan</th>
                        <th style="background-color: #1fc8e3" class="text-center">Tgl. Permohonan</th>
                        <th style="background-color: #1fc8e3" class="text-center">No. Pelayanan</th>
                        <th style="background-color: #1fc8e3" class="text-center">NIK Pemohon</th>
                        <th style="background-color: #1fc8e3" class="text-center">Nama Pemohon</th>
                        <th style="background-color: #1fc8e3" class="text-center">Action</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>
                            {{-- <select class="form-control form-control-sm" id="filter-idJenisPajak1">
                                <option value="">Silahkan Pilih</option>
                                @foreach($jenisPajak as $pajak)
                                <option value="{{ $pajak->s_id_jenis_pajak }}">{{ $pajak->s_nama_jenis_pajak }}</option>
                                @endforeach
                            </select> --}}
                        </th>
                        <th>
                            <select class="form-control form-control-sm" id="filter-idJenisPelayanan1">
                                <option value="">Silahkan Pilih</option>
                                @foreach($jenisPelayanan as $pelayanan)
                                <option value="{{ $pelayanan->s_id_jenis_pelayanan }}">{{ $pelayanan->s_nama_jenis_pelayanan }}</option>
                                @endforeach
                            </select>
                        </th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-tglPelayanan1"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-noPelayanan1"></th>
                        <th><input class="form-control form-control-sm masked-nik" type="text" id="filter-nikPemohon1"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-namaPemohon1"></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="8"> Tidak ada data.</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-12 pagination-footer"></div>
    </div>
</div>

@push('scripts')
<script th:inline="javascript" data-th-fragment="js">
    $('#filter-tglPelayanan1').daterangepicker({
        autoUpdateInput: false,
         locale: {
            cancelLabel: 'Clear'
         }
    });

    $('#filter-tglPelayanan1').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        search1();
    });

    $('#filter-tglPelayanan1').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        search1();
    });

    $("#filter-idJenisPajak1, #filter-idJenisPelayanan1").change(function(){
        search1();
    });

    $("#filter-noPelayanan1, #filter-namaPemohon1, #filter-nikPemohon1").keyup(function(){
        search1();
    });

    var datatables1 = datagrid({
        url: '/hasil-pelayanan/datagrid-belum',
        table: "#datagrid1",
        columns: [
            {class: ""},
            {class: ""},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: ""},
        ],
        orders: [
            {sortable: false},
            {sortable: true, name: "idJenisPajak"},
            {sortable: true, name: "idJenisPelayanan"},
            {sortable: true, name: "tglPelayanan"},
            {sortable: true, name: "noPelayanan"},
            {sortable: true, name: "nikPemohon"},
            {sortable: true, name: "namaPemohon"},
            {sortable: false},
        ],
        action: [
            {
                name: 'sk-tambah',
                btnClass: 'btn btn-danger btn-xs',
                btnText: '<i class="fas fa-file-upload"> Input Hasil Pelayanan</i>'
            },
        ]

    });

    function search1(){
        datatables1.setFilters({
            idJenisPajak: $("#filter-idJenisPajak1").val(),
            tglPelayanan: $("#filter-tglPelayanan1").val(),
            noPelayanan: $("#filter-noPelayanan1").val(),
            idJenisPelayanan: $("#filter-idJenisPelayanan1").val(),
            namaPemohon: $("#filter-namaPemohon1").val(),
            nikPemohon: $("#filter-nikPemohon1").val(),
        });
        datatables1.reload();
    }

    search1();

    function printdaftarhasilpelayananbelum(type){
        window.open('/report/hasil-pelayanan/printdaftarhasilpelayananbelum' + type
        + '?idJenisPajak=' + $("#filter-idJenisPajak1").val()
        + '&tglPelayanan=' + $("#filter-tglPelayanan1").val()
        + '&noPelayanan=' + $("#filter-noPelayanan1").val()
        + '&namaPemohon=' + $("#filter-namaPemohon1").val()
        + '&idJenisPelayanan=' + $("#filter-idJenisPelayanan1").val()
        + '&nikPemohon=' + $("#filter-nikPemohon1").val()
        + '');
    }
</script>
@endpush
