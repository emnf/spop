@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Hasil Pelayanan
@endsection

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <section>
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Hasil Pelayanan</li>
                        </ol>
                    </section>
                </div>
                {{-- <div class="col-12">
                    <div class="info-box callout callout-danger" id="background2">
                        <div class="col-sm-3">
                            <div class="description-block" style="float: left;">
                                <h1 class="description-header" style="font-size: 15pt;">REKAM HASIL PELAYANAN</h1>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="col-md-12 bg-white">
                <div class="row">
                    <div class="col-12">
                        <div class="p-0 mt-5 ml-4 mr-4 mb-2">
                            <h5 class="text-danger text-bold" style="letter-spacing: 1pt;">DETAIL PELAYANAN</h5>
                        </div>
                        <div class="invoice p-3 ml-4 mr-4">
                            <div class="row invoice-info">
                                <div class="col-12 animated fadeInLeft" style="font-family: Arial, Helvetica, sans-serif;">
                                    <h5 style="letter-spacing: 1pt; font-weight: bolder;">DATA PELAYANAN</h5>
                                    <hr style="height:3px;border-width:0;color: #ffffff;background-color:rgba(0,0,0,.125);">
                                    <div class="row">
                                        <div class="col-sm-4 invoice-col">
                                            <address>
                                                <span class="text-muted" style="font-size: 12pt;">Tanggal
                                                    Permohonan:</span> <br>
                                                <b
                                                    style="font-size: large;">{{ date('d-m-Y', strtotime($pelayanan->t_tgl_pelayanan)) }}</b><br>
                                            </address>
                                            <address>
                                                <span class="text-muted" style="font-size: 12pt;">No. Pelayanan:</span>
                                                <br>
                                                <span class="text-danger"
                                                    style="font-size: x-large; font-weight: bold;">{{ \App\Helpers\PelayananHelper::formatNoPelayanan(
                                                        date('Y', strtotime($pelayanan->t_tgl_pelayanan)),
                                                        $pelayanan->t_id_jenis_pajak,
                                                        $pelayanan->t_no_pelayanan,
                                                    ) }}</span><br>
                                            </address>
                                        </div>
                                        <div class="col-sm-8 invoice-col">
                                            <address>
                                                <div class="row">
                                                    <div class="col-4">Jenis Pajak</div>
                                                    <div class="col-8">: <span class="text-bold">
                                                            {{ $pelayanan->jenisPajak->s_nama_jenis_pajak }} </span></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">Jenis Pelayanan</div>
                                                    <div class="col-8">: <span class="text-bold">
                                                            {{ $pelayanan->jenisPelayanan->s_nama_jenis_pelayanan }} </span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">Perkiraan Selesai</div>
                                                    <div class="col-8">: <span class="text-bold">
                                                            {{ $pelayanan->pelayananAprove->t_tgl_perkiraan_selesai }}
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">NOP</div>
                                                    <div class="col-8">: <span class="text-bold">
                                                            {{ $pelayanan->objekPajak->nopFormated }} </span></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">Keterangan</div>
                                                    <div class="col-8">: <span class="text-bold">
                                                            {{ $pelayanan->t_keterangan }} </span></div>
                                                </div>
                                                @if (isset($pelayanan->t_besar_pengurangan))
                                                    <div class="row">
                                                        <div class="col-4">Pengurangan diajukan</div>
                                                        <div class="col-8">: <span
                                                                style="font-size: large; font-weight: bold;">{{ $pelayanan->t_besar_pengurangan }}%</span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-4">Pengurangan diterima</div>
                                                        <div class="col-8">: <span
                                                                style="font-size: large; font-weight: bold;">{{ $pelayanan->validasi->t_besar_pengurangan }}%</span>
                                                        </div>
                                                    </div>
                                                @endif
                                            </address>
                                        </div>
                                    </div>
                                    <hr style="height:3px;border-width:0;color: #ffffff;background-color:rgba(0,0,0,.125);">
                                </div>
                                <div class="col-12 animated fadeInLeft" style="font-family: Arial, Helvetica, sans-serif;">
                                    <div class="row">
                                        <div class="col-sm-6 invoice-col">
                                            <h5 style="letter-spacing: 1pt; font-weight: bolder;">DATA OBJEK PAJAK</h5>
                                            <hr
                                                style="height:3px;border-width:0;color: #ffffff;background-color:rgba(0,0,0,.125);">
                                            <address>
                                                <span class="text-muted">NOP:</span> <br>
                                                <span
                                                    class="text-bold text-danger">{{ $pelayanan->objekPajak->nopFormated }}</span><br>
                                            </address>
                                            <address>
                                                <span class="text-muted">NIK:</span> <br>
                                                <span class="text-bold"></span>{{ $pelayanan->wajibPajak->t_nik_wp }}<br>
                                            </address>
                                            <address>
                                                <span class="text-muted">Nama:</span> <br>
                                                <span class="text-bold">{{ $pelayanan->wajibPajak->t_nama_wp }}</span><br>
                                            </address>
                                            <address>
                                                <span class="text-muted float-left">Alamat:</span><br>
                                                <span class="text-bold">
                                                    {{ $pelayanan->wajibPajak->t_jalan_wp .
                                                        ', Rt. ' .
                                                        $pelayanan->wajibPajak->t_rt_wp .
                                                        ', Rw. ' .
                                                        $pelayanan->wajibPajak->t_rw_wp .
                                                        ', ' .
                                                        $pelayanan->wajibPajak->t_kelurahan_wp .
                                                        ', ' .
                                                        $pelayanan->wajibPajak->t_kecamatan_wp }}
                                                </span>
                                                <br>
                                                <span
                                                    class="text-bold">{{ 'Kab./Kota ' . $pelayanan->wajibPajak->t_kabupaten_wp }}</span>
                                                <br>
                                            </address>
                                            <address>
                                                <span class="text-muted">Luas Tanah:</span> <br>
                                                <span
                                                    class="text-bold">{{ $pelayanan->objekPajak->t_luas_tanah . ' M2' }}</span><br>
                                            </address>
                                        </div>

                                        <th:block th:switch="${pelayanan.idJenisPelayanan}">
                                            <th:block th:case="'2'">
                                                <div data-th-replace="hasil-pelayanan/common/objek-lama :: data"></div>
                                            </th:block>
                                            <th:block th:case="'3'">
                                                <div data-th-replace="hasil-pelayanan/common/objek-lama :: data"></div>
                                            </th:block>
                                        </th:block>
                                    </div>
                                    <hr style="height:3px;border-width:0;color: #ffffff;background-color:rgba(0,0,0,.125);">
                                </div>
                                <div class="col-12 animated fadeInRight" style="font-family: Arial, Helvetica, sans-serif;">
                                    <h5 style="letter-spacing: 1pt; font-weight: bolder;">SYARAT PENGAJUAN</h5>
                                    <hr style="height:3px;border-width:0;color: #ffffff;background-color:rgba(0,0,0,.125);">
                                    <div class="row">
                                        <div class="col-sm-12 invoice-col">
                                            <address>
                                                <div class="col-12 table-responsive">
                                                    <table class="table table-striped">
                                                        <tbody id="dataupload">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </address>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 animated fadeInLeft">
                        <div class="p-0 mt-5 ml-4 mr-4 mb-2">
                            <h5 class="text-danger text-bold" style="letter-spacing: 1pt;">DETAIL PEMOHON</h5>
                        </div>
                        <div class="invoice p-3 ml-4 mr-4">
                            <div class="row invoice-info">
                                <div class="col-sm-12 invoice-col" style="font-family: Arial, Helvetica, sans-serif;">
                                    <address>
                                        <strong
                                            style="font-size: 16pt; letter-spacing: 3pt;">{{ $pelayanan->t_nama_pemohon }}</strong><br>
                                        <span>{{ $pelayanan->t_jalan_pemohon }}</span>
                                        RT <span>{{ $pelayanan->t_rt_pemohon }}</span>/
                                        RW <span>{{ $pelayanan->t_rt_pemohon }}</span>,
                                        <span>{{ $pelayanan->t_kelurahan_pemohon }}</span>,
                                        <span>{{ $pelayanan->t_kecamatan_pemohon }}</span><br>
                                        <span>{{ $pelayanan->t_kabupaten_pemohon }}</span>,
                                        <span>{{ $pelayanan->t_kode_pos_pemohon }}</span><br>
                                        No. HP : <span>{{ $pelayanan->t_no_hp_pemohon }}</span><br>
                                        Email : <span>{{ $pelayanan->t_email_pemohon }}</span><br>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form class="form-horizontal form-validation" method="post" action="/hasil-pelayanan/save"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-12 animated fadeInLeft">
                            <div class="p-0 mt-5 ml-4 mr-4 mb-2">
                                <h5 class="text-danger text-bold" style="letter-spacing: 1pt;">DETAIL SK DOKUMEN</h5>
                            </div>
                            <div class="invoice p-3 ml-4 mr-4">
                                <div class="row invoice-info">
                                    <div class="col-sm-12 invoice-col" style="font-family: Arial, Helvetica, sans-serif;">
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label" for="noSk">No SK</label>
                                            <div class="col-md-auto">
                                                <input type="hidden" name="idSk" id="idSk">
                                                <input type="hidden" name="idPelayanan" id="idPelayanan"
                                                    value="{{ $pelayanan->t_id_pelayanan }}">
                                                <input type="hidden" name="idValidasi" id="idValidasi"
                                                    value="{{ $pelayanan->validasi->t_id_validasi }}">
                                                <input type="text" class="form-control" name="noSk" id="noSk"
                                                    value="{{ old('noSk') ?? '' }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 invoice-col" style="font-family: Arial, Helvetica, sans-serif;">
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label" for="tglSk">Tanggal SK</label>
                                            <div class="col-md-auto">
                                                <input type="text" class="form-control datepicker-date-fixed"
                                                    name="tglSk" id="tglSk" value="{{ date('d-m-Y') }}" readonly
                                                    required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 invoice-col" style="font-family: Arial, Helvetica, sans-serif;">
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label" for="keteranganSk">Keterangan
                                                SK</label>
                                            <div class="col-md-auto">
                                                <textarea class="form-control" name="keteranganSk" id="keteranganSk">{{ old('keteranganSk') ?? '' }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    @if (in_array($pelayanan->t_id_jenis_pelayanan, [8, 4, 5, 7]))
                                        <button type="button" class="btn btn-block btn-primary" data-toggle="modal"
                                            data-target="#modal-save">
                                            Simpan
                                        </button>
                                    @else
                                        <div class="col-sm-12 invoice-col"
                                            style="font-family: Arial, Helvetica, sans-serif;">
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label" for="fileinput">File
                                                    Dokumen</label>
                                                <div class="col-md-auto">
                                                    <div class="input-group">
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input"
                                                                id="fileinput" name="fileinput"
                                                                accept="application/pdf, image/jpeg, image/jpg, image/png"
                                                                required multiple>
                                                            <label class="custom-file-label" for="fileinput">Pilih
                                                                File</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 invoice-col animated fadeInUp">
                                            <div class="info-box bg-danger">
                                                <div class="col-sm-1">
                                                    <div
                                                        class="description-block text-center justify-content-center align-self-center">
                                                        <span class="icon info-box-icon animated rubberBand">
                                                            <i class="fa fa-inbox"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="description-block float-left">
                                                        <h1 class="description-header"
                                                            style="letter-spacing: 1pt; text-align: left;">UNGGAH HASIL
                                                            PELAYANAN</h1>
                                                        <span>Silahkan unggah hasil pelayanan yang sudah bertanda tangan
                                                            resmi.</span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="description-block">
                                                        <button type="button" class="btn btn-sm btn-block btn-default"
                                                            data-toggle="modal" data-target="#modal-save">
                                                            Unggah
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modal-save">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header bg-danger">
                                    <h6 class="modal-title"><span class="fas fa-send"></span>
                                        &nbsp;UNGGAH HASIL PELAYANAN</h6>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p>Apakah anda ingin menggunggah dokumen sk untuk permohonan ini?</p>
                                    <br>
                                    <div class="invoice p-3 ml-4 mr-4">
                                        <div class="row invoice-info">
                                            <div class="col-sm-12 invoice-col"
                                                style="font-family: Arial, Helvetica, sans-serif;">
                                                <address>
                                                    <span class="text-muted" style="font-size: 12pt;">No.
                                                        Pelayanan:</span> <br>
                                                    <span class="text-danger"
                                                        style="font-size: x-large; font-weight: bold;">{{ \App\Helpers\PelayananHelper::formatNoPelayanan(
                                                            date('Y', strtotime($pelayanan->t_tgl_pelayanan)),
                                                            $pelayanan->t_id_jenis_pajak,
                                                            $pelayanan->t_no_pelayanan,
                                                        ) }}</span><br>
                                                </address>
                                                <address>
                                                    <span class="text-muted" style="font-size: 12pt;">Nama
                                                        Pemohon:</span> <br>
                                                    <span
                                                        style="font-size: x-large; font-weight: bold;">{{ $pelayanan->t_nama_pemohon }}</span><br>
                                                </address>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer justify-content-between">
                                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span
                                            class="fas fa-times-circle"></span>
                                        &nbsp;Tutup
                                    </button>
                                    <button type="submit" class="btn btn-danger btn-sm"><span
                                            class="fas fa-check"></span>
                                        &nbsp;IYA
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <div class="modal fade" id="modal-image">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h6 class="modal-title" id="modal-image-title">&nbsp;FILE PERSYARATAN</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center" id="imagePlaceholder"></div>
                <div class="modal-footer justify-content-between"></div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        function resetFormUpload() {
            $("input[type=file]").each(function() {
                $(this).val("");
            })
            $(".custom-file-label").each(function() {
                $(this).text("Pilih file");
            })
        }

        function poplateFileUpload() {
            $("#dataupload").html("");
            var idPelayanan = $("#idPelayanan").val();
            no = 1;
            $.get('/pelayanan/upload-file/' + idPelayanan).then(function(data) {
                data.forEach(r => {
                    $("#dataupload").append("<tr>");
                    $("#dataupload").append("<td style=\"text-align: center;\">" + no + "</td>");
                    $("#dataupload").append("<td>" + r.t_nama_persyaratan + "</td>");
                    $("#dataupload").append("<td class=\"text-blue text-right\"><a href=\"#\" link=\"/" + r
                        .t_lokasi_file + "\" name=\"" + r.t_nama_persyaratan +
                        "\" onclick=\"showImage(this)\">" + r.t_nama_persyaratan + "</a></td>");
                    $("#dataupload").append("</tr>");
                    no++;
                });
            });
        }

        function showImage(img) {
            var link = $(img).attr("link");
            var name = $(img).attr("name");
            $("#modal-image-title").text(name);
            var image = $('<img style="max-height:100%; max-width:100%;">');
            image.attr("src", link);
            $("#imagePlaceholder").html(image);
            $("#modal-image").modal('show');
            return false;
        }

        poplateFileUpload();
    </script>
@endpush
