@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Hasil Pelayanan
@endsection

@section('content')
    <section class="content" layout:fragment="content">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <section>
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Hasil Pelayanan</li>
                            </ol>
                        </section>
                    </div>
                    {{-- <div class="col-12">
                        <div class="info-box callout callout-danger" id="background2">
                            <div class="col-sm-6">
                                <div class="" style="float: left;">
                                    <h5 class="">Hasil Pelayanan</h5>
                                    <span class="float">Mengunggah hasil pelayanan</span>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                @if (session('success'))
                    <div class="alert alert-dismissible alert_customs_success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-check"></i> Success!</h5>
                        <span>{{ session('success') }}</span>
                    </div>
                @endif

                <div class="row">
                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-file"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Sudah Divalidasi</span>
                                <span class="info-box-number"> {{ $totalValidasi ?? 0 }} </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-info"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Belum Diunggah</span>
                                <span class="info-box-number"> {{ $totalBelumDiUnggah ?? 0 }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-success elevation-1"><i class="fas fa-thumbs-up"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Selesai Unggah</span>
                                <span class="info-box-number">{{ $totalSudahDiUnggah ?? 0 }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card card-danger card-outline card-outline-tabs ">
                            <div class="card-header p-0 border-bottom-0">
                                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="pill" href="#navtab1" role="tab"
                                            aria-controls="navtab1" aria-selected="true">Belum</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="pill" href="#navtab2" role="tab"
                                            aria-controls="navtab2" aria-selected="true">Sudah</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="pill" href="#navtab3" role="tab"
                                            aria-controls="navtab3" aria-selected="true">Semua</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-three-tabContent">
                                    @include('hasil-pelayanan.datagrid.belum')
                                    @include('hasil-pelayanan.datagrid.sudah')
                                    @include('hasil-pelayanan.datagrid.semua')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection
