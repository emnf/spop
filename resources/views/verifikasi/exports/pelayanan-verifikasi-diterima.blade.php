@extends('layouts.print')

@section('body')
<table style="width: 100%; border-bottom: 5px double #000" cellspacing="0" cellpadding="0">
    <tr>
        <td style="text-align: center; width: 20%">
            <img style="width: 25%" title="LOGO" src="{{ $pemda->s_logo }}" />
        </td>
        <td style="text-align: center">
            <span style="font-size: 11pt;font-weight: bold">PEMERINTAH {{ strtoupper($pemda->s_nama_kabkota) }}</span><br>
            <span style="font-size: 14pt;font-weight: bold">{{ strtoupper($pemda->s_nama_instansi) }}</span><br>
            <span style="font-size: 9pt;">
                {{ ucwords(strtolower($pemda->s_alamat_instansi)) . ' Telp/Fax. ' . $pemda->s_notelp_instansi .', Kode Pos '. $pemda->s_kode_pos }}
            </span><br><br>
        </td>
        <td style="width: 20%">
            &nbsp;
        </td>
    </tr>
</table>
<table style="width: 100%" border="0">
    <tr>
        <td colspan="6" class="text-center">
            <h4>DATA VERIFIKASI DITERIMA</h4>
        </td>
    </tr>
</table>

<table id="table" style="width: 100%; border-collapse: collapse;">
    <thead>
        <tr>
            <th class="border_kiri border_atas border_kanan border_bawah text-center">No.</th>
            <th class="border_kiri border_atas border_kanan border_bawah text-center">Jenis Pajak</th>
            <th class="border_kiri border_atas border_kanan border_bawah text-center">Jenis Pelayanan</th>
            <th class="border_kiri border_atas border_kanan border_bawah text-center">No. Pelayanan</th>
            <th class="border_kiri border_atas border_kanan border_bawah text-center">Tanggal Pelayanan</th>
            <th class="border_kiri border_atas border_kanan border_bawah text-center">Nama Pemohon</th>
            <th class="border_kiri border_atas border_kanan border_bawah text-center">Tgl. Verifikasi</th>
            <th class="border_kiri border_atas border_kanan border_bawah text-center">Keterangan</th>
        </tr>
    </thead>
    <tbody>
        @if($pelayanan->count() > 0)
            @foreach ($pelayanan as $i => $row)
            <tr>
                <td class="border_kiri border_bawah text-center">{{ $i+1 }}</td>
                <td class="border_kiri border_bawah text-center">{{ $row->jenisPajak->s_nama_singkat_pajak }}</td>
                <td class="border_kiri border_bawah">{{ $row->jenisPelayanan->s_nama_jenis_pelayanan }}</td>
                <td class="border_kiri border_bawah">{{ App\Helpers\PelayananHelper::formatNoPelayanan(date('Y',
                    strtotime($row->t_tgl_pelayanan)), $row->t_id_jenis_pajak, $row->t_no_pelayanan) }}</td>
                <td class="border_kiri border_bawah text-center">{{ date('d-m-Y', strtotime($row->t_tgl_pelayanan)) }}
                </td>
                <td class="border_kiri border_bawah border_kanan">{{ $row->t_nama_pemohon }}</td>
                <td class="border_kiri border_bawah border_kanan">{{ date('d-m-Y', strtotime($row->verifikasi->t_tgl_verifikasi)) }}</td>
                <td class="border_kiri border_bawah border_kanan">{{ $row->verifikasi->t_keterangan_verifikasi }}</td>
            </tr>
            @endforeach
        @else
        <tr>
            <td class="border_kiri border_bawah">&nbsp;</td>
            <td class="border_kiri border_bawah">&nbsp;</td>
            <td class="border_kiri border_bawah">&nbsp;</td>
            <td class="border_kiri border_bawah">&nbsp;</td>
            <td class="border_kiri border_bawah">&nbsp;</td>
            <td class="border_kiri border_bawah border_kanan">&nbsp;</td>
            <td class="border_kiri border_bawah border_kanan">&nbsp;</td>
            <td class="border_kiri border_bawah border_kanan">&nbsp;</td>
        </tr>
        @endif
    </tbody>
</table>
<table style="width: 100%; border-collapse: collapse; margin-top: 20px;">
    <tr>
        <td class="text-center" style="width: 50%;"></td>
        <td class="text-center" style="width: 50%;" >
            <span>{{ $pemda->s_nama_ibukota_kabkota . ', ' . Carbon\Carbon::parse($tglCetak)->translatedFormat('d F Y') }}</span>
            <br>
            <span>{{ $pegawai->s_jabatan_pegawai ?? '' }}</span>
            <br><br><br><br><br>
            <u>{{ $pegawai->s_nama_pegawai ?? '_______________________' }}</u>
            <br>
            <span>{{ is_null($pegawai) ? '' : $pegawai->s_nip_pegawai }}</span>
        </td>
    </tr>
</table>

@endsection

