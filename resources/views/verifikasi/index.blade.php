@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Verifkasi Berkas
@endsection

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('setting-pemda') }}">Setting</a></li>
                        <li class="breadcrumb-item active">Verifikasi</li>
                    </ol>
                </div><!-- /.col -->
                {{-- <div class="col-12">
                    <div class="info-box callout callout-danger" id="background2">
                        <div class="col-sm-4">
                            <div class="" style="float: left;">
                                <h5 class="description-header">Verifikasi Pelayanan</h5>
                                <span class="float">Menu untuk memverifikasi berkas pengajuan.</span>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->

        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-info elevation-1"><i class="fas fa-file"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Sudah Diajukan</span>
                            <span class="info-box-number">{{ $totalPelayanan ?? 0 }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-info"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Belum Verifikasi</span>
                            <span class="info-box-number">{{ $totalBelumVerifikasi ?? 0 }}</span>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-book"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Perbaikan*</span>
                        <span class="info-box-number">{{ $totalPending ?? 0 }}</span>
                    </div>
                </div>
            </div> --}}
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-success elevation-1"><i class="fas fa-thumbs-up"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Selesai</span>
                            <span class="info-box-number">{{ $totalVerifikasi ?? 0 }}</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-12">
                            <div class="card card-danger card-outline card-outline-tabs">
                                <div class="card-header p-0 border-bottom-0">
                                    <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="pill" href="#navtab1" role="tab"
                                                aria-controls="navtab1" aria-selected="true">Belum</a>
                                        </li>
                                        {{-- <li class="nav-item">
                                        <a class="nav-link" data-toggle="pill" href="#navtab2" role="tab"
                                            aria-controls="navtab2" aria-selected="false">Perbaikan</a>
                                    </li> --}}
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="pill" href="#navtab3" role="tab"
                                                aria-controls="navtab3" aria-selected="false">Ditolak</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="pill" href="#navtab4" role="tab"
                                                aria-controls="navtab4" aria-selected="false">Diterima</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="pill" href="#navtab5" role="tab"
                                                aria-controls="navtab5" aria-selected="false">Ajukan Validasi</a>
                                        </li>
                                        <!--                                <li class="nav-item">-->
                                        <!--                                    <a class="nav-link" data-toggle="pill" href="#navtab6" role="tab" aria-controls="navtab6" aria-selected="false">Ajukan Verlap</a>-->
                                        <!--                                </li>-->
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="pill" href="#navtab7" role="tab"
                                                aria-controls="navtab7" aria-selected="false">Semua</a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="tab-content" id="custom-tabs-three-tabContent">
                                        @include('verifikasi.datagrid.belum')
                                        {{-- @include('verifikasi.datagrid.pending') --}}
                                        @include('verifikasi.datagrid.ditolak')
                                        @include('verifikasi.datagrid.diterima')
                                        @include('verifikasi.datagrid.validasi')
                                        @include('verifikasi.datagrid.semua')
                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer clearfix pagination-footer">
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
