@extends('layouts.app')

@section('title')
    Kepoin || Verifikasi Pengajuan
@endsection

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <section>
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Verifikasi</li>
                        </ol>
                    </section>
                </div>
                {{-- <div class="col-12">
                    <div class="info-box callout callout-danger" id="background2">
                        <div class="col-sm-6">
                            <div class="" style="float: left;">
                                <h5 class="">FORM VERIFIKASI PELAYANAN</h5>
                                <span class="float">Periksalah dengan teliti dan benar.</span>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            @include('verifikasi.form', [
                'actionUrl' => url('verifikasi/update'),
                'jenisLayanan' => $pelayanan->jenisPelayanan,
                'jenisPajak' => $pelayanan->jenisPajak,
                'objekPajak' => $pelayanan->objekPajak,
            ]);
        </div>
    </section>
@endsection
{{-- <div layout:fragment="js">
    <div data-th-replace="verifikasi/form :: js"></div>
</div> --}}
