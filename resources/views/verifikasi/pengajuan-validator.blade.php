@extends('layouts.app')

@section('title')
    Kepoin || Pengajuan Verifikasi
@endsection

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <section>
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/verifikasi">Home</a></li>
                            <li class="breadcrumb-item">Verifikasi</li>
                        </ol>
                    </section>
                </div>
                <div class="col-12">
                    <div class="info-box callout callout-danger">
                        <div class="col-sm-12">
                            <div class="" style="float: left;">
                                <h5 class="">FORM PENGAJUAN VERIFIKASI </h5>
                                <span class="float">Telitilah data yang akan anda ajukan dengan baik dan benar. </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                @if (session('success'))
                    <div class="col-12">
                        <div class="alert alert-dismissible alert_customs_success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h5><i class="icon fas fa-check"></i> Berhasil !!</h5>
                            <span>{{ session('success') }}</span>
                        </div>
                    </div>
                @endif
                <div class="col-12">
                    <div class="card" style="border-top: 5px solid #bd2130;">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="invoice p-3 mb-3">
                                        <div class="row invoice-info">
                                            <div class="col-sm-6 invoice-col border-right">
                                                @include('pelayanan.pbb.components.data-pemohon')
                                            </div>
                                            <div class="col-sm-6 invoice-col">
                                                <h5>Data Pelayanan</h5>
                                                <hr>
                                                @include('pelayanan.pbb.components.data-view.data-pelayanan')
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <div class="card" style="border-top:5px solid #dc3545;">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="invoice p-3 mb-3">
                                        <div class="row invoice-info">
                                            <div class="col-sm-6 invoice-col border-right">
                                                <input type="hidden" name="idPelayanan" id="idPelayanan"
                                                    value="{{ $pelayanan->t_id_pelayanan }}">
                                                @include('pelayanan.pbb.components.data-view.objek-pajak')
                                            </div>
                                            <div class="col-sm-6 invoice-col">
                                                @include('pelayanan.pbb.components.data-view.wajib-pajak')
                                            </div>
                                        </div>
                                        @if (!in_array($pelayanan->t_id_jenis_pelayanan, [5, 6]))
                                            @if (count($pelayanan->OpLamas) > 0)
                                                <div class="row invoice-info pt-3">
                                                    <div class="col-sm-6 invoice-col border-right">
                                                        @include('pelayanan.pbb.components.data-view.objek-pajak-lama')
                                                    </div>
                                                    <div class="col-sm-6 invoice-col">
                                                        @include('pelayanan.pbb.components.data-view.wajib-pajak-lama')
                                                    </div>
                                                </div>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if ($pelayanan->detailBangunan->count() > 0)
                <div class="row">
                    <div class="col-12">
                        <div class="invoice p-3 mb-3" style="border-top:3px solid #dc3545;">
                            <div class="row invoice-info">
                                <div class="col-sm-6 invoice-col border-right">
                                    <h5>DATA BANGUNAN</h5>
                                    <hr>
                                    <address>
                                        @foreach ($pelayanan->detailBangunan as $key => $item)
                                            <div class="row">
                                                <div class="col-5">Luas Bangunan Ke-{{ $item->t_no_urut_bangunan }}</div>
                                                <div class="col-1">:</div>
                                                <div class="col-6"><b>{{ $item->t_luas }}</b> (M<sup>2</sup>)</div>
                                            </div>
                                        @endforeach
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="card card-danger card-outline">
                        <div class="card-header">
                            <div class="card-title">
                                <h5>Persyaratan Permohonan Pelayanan</h5>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="width: 4rem;">No.</th>
                                            <th>Jenis Persyaratan</th>
                                            <th>File Persyaratan</th>
                                            <th>Unduh File</th>
                                        </tr>
                                    </thead>
                                    <tbody id="uploadedFiles">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-danger card-outline">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12" style="text-align: center"><u>
                                        <h5>Keterangan Verifikasi</h5>
                                    </u>
                                </div>
                            </div>
                            {{-- <div class="row">
                            <div class="col-2">Kejelasan</div>
                            <div class="col-1">:
                                <label> {{ ($pelayanan->verifikasi->t_kejelasan == 1 ) ? "YA" : "TIDAK"
                                    }}</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-2">Kebenaran</div>
                            <div class="col-1">:
                                <label> {{ ($pelayanan->verifikasi->t_kebenaran == 1 ) ? "YA" : "TIDAK"
                                    }}</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-2">Keabsahan</div>
                            <div class="col-1">:
                                <label> {{ ($pelayanan->verifikasi->t_keabsahan == 1 ) ? "YA" : "TIDAK"
                                    }}</label>
                            </div>
                        </div> --}}
                            <div class="row">
                                <div class="col-2">
                                    Catatan
                                </div>
                                <div class="col-10"> :
                                    <label> {{ $pelayanan->verifikasi->t_keterangan_verifikasi }} </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12">
                <form class="form-horizontal form-validation" method="post" action="{{ url('verifikasi/ajukan') }}">
                    @csrf
                    <input type="hidden" id="t_id_pelayanan" name="t_id_pelayanan"
                        value="{{ $pelayanan->t_id_pelayanan }}">
                    <input type="hidden" id="t_id_verifikasi" name="t_id_verifikasi"
                        value="{{ $pelayanan->verifikasi->t_id_verifikasi }}">
                    <input type="hidden" id="t_id_status_verifikasi" name="t_id_status_verifikasi" value="5">

                    <div class="row no-print">
                        <div class="col-12">
                            <button type="button" class="btn btn-danger" data-toggle="modal"
                                data-target="#modal-cancel">
                                <i class="fas fa-backspace"></i> Kembali
                            </button>
                            <div class="modal fade" id="modal-cancel">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header bg-danger">
                                            <h6 class="modal-title"><span class="fas fa-exclamation"></span> BERKAS
                                                VERIFIKASI ?</h6>
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p>Apakah anda yakin bahwa ingin kembali ke verifikasi berkas ?</p>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default btn-sm"
                                                data-dismiss="modal"><span class="fas fa-times-circle"></span>
                                                &nbsp;Tutup
                                            </button>
                                            <a href="{{ '/verifikasi/edit/' . $pelayanan->uuid }}"
                                                class="btn btn-danger btn-sm"><span class="fas fa-exclamation"></span>
                                                &nbsp;IYA</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;"
                                data-toggle="modal" data-target="#modal-save">
                                <i class="fas fa-upload"></i> Ajukan</button>
                            <div class="modal fade" id="modal-save">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header bg-primary">
                                            <h6 class="modal-title"><span class="fas fa-send"></span> &nbsp;PENGAJUAN
                                                VERIFIKASI</h6>
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p>Pastikan data yang anda isi sudah benar. Apakah anda ingin mengajukan
                                                permohonan ini?</p>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default btn-sm"
                                                data-dismiss="modal"><span class="fas fa-times-circle"></span>
                                                &nbsp;Tutup
                                            </button>
                                            <button type="submit" class="btn btn-primary btn-sm"><span
                                                    class="fas fa-check"></span> &nbsp;IYA
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="modal fade" id="modal-image">
                    <div class="modal-dialog modal-lg modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header bg-info">
                                <h6 class="modal-title" id="modal-image-title">&nbsp;FILE PERSYARATAN</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body text-center" id="imagePlaceholder">

                            </div>
                            <div class="modal-footer justify-content-between">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div layout:fragment="js">
        <th:block th:switch="${pelayanan.jenisPajak.idKategoriPajak}">
            <th:block th:case="'2'">
                <script data-th-replace="pelayanan/common/data-objek/__${pelayanan.jenisPelayanan.alias}__ :: js"></script>
            </th:block>
        </th:block>
    </div>
@endsection


@push('scripts')
    <script>
        $(document).ready(function() {
            if ($('#nopPelayanan').val() !== '') {
                getDataSppt();
            }
        });

        function getDataSppt(params) {
            $.get('/pelayanan/getDataSppt', {
                nop: $('#nopPelayanan').val(),
                tahunPajak: $('#tahunPajakPelayanan').val(),
            }, (res) => {
                $("#op_nomor").html($('#nopPelayanan').val());
                $("#op_jalan").html(res.jalan_op);
                $("#op_rt").html(res.rt_op);
                $("#op_rw").html(res.rw_op);
                $("#op_block").html(res.blok_kav_no_op);
                $("#op_desa").html(res.nm_kelurahan);
                $("#op_kecamatan").html(res.nm_kecamatan);
                $("#op_luas_tanah").html(res.luas_bumi_sppt);
                $("#op_luas_bangunan").html(res.luas_bng_sppt);
                $("#op_nomor").html($('#nopPelayanan').val());

                $("#wp_jalan").html(res.jalan_op);
                $("#wp_rt").html(res.rt_op);
                $("#wp_rw").html(res.rw_op);
                $("#wp_desa").html(res.nm_kelurahan);
                $("#wp_kecamatan").html(res.nm_kecamatan);
                $("#wp_kab_kota").html(res.t_kabupaten_wp);
                $("#wp_telp").html(res.t_no_hp_wp);
                $("#wp_email").html(res.t_email);
                $("#npwpd").html(res.t_npwpd);

            })
        }
    </script>
    <script type="text/javascript">
        $(function() {
            updateUploadedFiles($("#idPelayanan").val());
            // updateUploadedDokumenPendukung();
        })

        $("#btnReject").click(function() {
            $("#idStatusValidasi").val(3);
        })

        $("#btnApprove").click(function() {
            $("#idStatusValidasi").val(4);
        })

        function updateUploadedFiles(idPelayanan) {
            $.get("/pelayanan/upload-file/" + idPelayanan).then(function(data) {
                $("#uploadedFiles").html('');
                var no = 1;
                data.forEach(file => {
                    $wajib = (file.s_is_optional) ? '' : '<span class="text-danger">*</span>';
                    var tr = '<tr>' +
                        '<td style="text-align: center">' + no + '</td>' +
                        '<td>' + file.t_nama_persyaratan + ' ' + $wajib + '</td>' +
                        '<td><a type="button" class="text-primary" link="/' + file.t_lokasi_file +
                        '" name="' + file.t_nama_persyaratan + '" onclick="showImage(this)">' + file
                        .t_nama_persyaratan + '</a></td>' +
                        '<td><a href="/' + file.t_lokasi_file + '" target="_blank" download>' + "Unduh" +
                        '</td>' +
                        '</tr>';
                    $("#uploadedFiles").append(tr);
                    no++;
                })

            });
        }

        function showImage(img) {
            var link = $(img).attr("link");
            var name = $(img).attr("name");
            $("#modal-image-title").text(name);
            var image = $('<img style="max-height:100%; max-width:100%;">');
            image.attr("src", link);
            $("#imagePlaceholder").html(image);
            $("#modal-image").modal('show');
            return false;
        }
    </script>
@endpush
