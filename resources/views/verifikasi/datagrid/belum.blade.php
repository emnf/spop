<div class="tab-pane fade active show" id="navtab1" role="tabpanel">
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-sm-2">
            <input type="text" class="form-control form-control-sm text-center datepicker-date-fixed" id="tglcetak"
                value="{{ date('d-m-Y') }}" readonly>
        </div>
        <div class="col-sm-3">
            <select class="form-control form-control-sm" id="ttd">
                <option value="">Silahkan Pilih</option>
                @foreach ($datapegawai as $pegawai)
                    <option value="{{ $pegawai->s_id_pegawai }}">{{ $pegawai->s_nama_pegawai }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-sm-2">
            <button class="btn btn-danger btn-sm" onclick="printverifikasibelumproses('PDF')"><span
                    class="fas fa-file-pdf"></span> &nbsp;PDF</button>
            <button class="btn btn-success btn-sm" onclick="printverifikasibelumproses('XLS')"><span
                    class="fas fa-file-excel"></span> &nbsp;XLS</button>
        </div>
    </div>
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-bordered table-striped table-hover" id="datagrid">
                <thead>
                    <tr>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Jenis Pajak</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pelayanan
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Permohonan
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No. Pelayanan
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pemohon
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Wajib Pajak
                        </th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">
                            Action</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>
                            {{-- <select class="form-control form-control-sm" id="filter-idJenisPajak">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($jenispajak as $jp)
                                    <option value="{{ $jp->s_id_jenis_pajak }}">{{ $jp->s_nama_jenis_pajak }}</option>
                                @endforeach
                            </select> --}}
                        </th>
                        <th>
                            <select class="form-control form-control-sm" id="filter-idJenisPelayanan">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($jenispelayanan as $jp)
                                    <option value="{{ $jp->s_id_jenis_pelayanan }}">{{ $jp->s_nama_jenis_pelayanan }}</option>
                                @endforeach
                            </select>
                        </th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-tglPelayanan"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-noPelayanan"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-namaPemohon"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-namaWp"></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="7"> Tidak ada data.</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-12 pagination-footer"></div>
    </div>
</div>

@push('scripts')
<script>
    $('#filter-tglPelayanan').daterangepicker({
        autoUpdateInput: false,
         locale: {
            cancelLabel: 'Clear'
         }
    });

    $('#filter-tglPelayanan').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        search();
    });

    $('#filter-tglPelayanan').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        search();
    });

    $("#filter-tglPelayanan, #filter-idJenisPajak, #filter-idJenisPelayanan").change(function(){
        search();
    });

    $("#filter-namaPemohon, #filter-namaWp").keyup(function(){
        search();
    });

    $("#filter-noPelayanan").keyup(function(){
        // if ($(this).val().length >= 3) {
            search();
        // }
    });

    var datatables = datagrid({
        url: '/verifikasi/datagrid-belum-proses',
        table: "#datagrid",
        columns: [
            {class: ""},
            {class: ""},
            {class: "text-center"},
            {class: "text-center"},
            {class: ""},
            {class: ""},
            {class: "text-center"},
        ],
        orders: [
            {sortable: false},
            {sortable: true, name: "idJenisPajak"},
            {sortable: true, name: "idJenisPelayanan"},
            {sortable: true, name: "tglPelayanan"},
            {sortable: true, name: "noPelayanan"},
            {sortable: true, name: "namaPemohon"},
            {sortable: true, name: "namaWp"},
            {sortable: false},
        ],
        action: [
            {
                name: 'tambah-verifikasibelum',
                btnClass: 'btn btn-warning btn-xs',
                btnText: '<i class="fas fa-edit"> Verifikasi</i>'
            },
        ]

    });

    function search(){
        datatables.setFilters({
            idJenisPajak: $("#filter-idJenisPajak").val(),
            tglPelayanan: $("#filter-tglPelayanan").val(),
            noPelayanan: $("#filter-noPelayanan").val(),
            idJenisPelayanan: $("#filter-idJenisPelayanan").val(),
            namaPemohon: $("#filter-namaPemohon").val(),
            namaWp: $("#filter-namaWp").val(),
        });
        datatables.reload();
    }
    search();

    function printverifikasibelumproses(type) {
        window.open('/verifikasi/cetak-verifikasi-belum-proses?format=' + type
        + '&idJenisPajak=' + $("#filter-idJenisPajak").val()
        + '&tglPelayanan=' + $("#filter-tglPelayanan").val()
        + '&noPelayanan=' + $("#filter-noPelayanan").val()
        + '&namaPemohon=' + $("#filter-namaPemohon").val()
        + '&idJenisPelayanan=' + $("#filter-idJenisPelayanan").val()
        + '&tglcetak=' + $("#tglcetak").val()
        + '&ttd=' + $("#ttd").val()
        + '');
    }
</script>

@endpush
