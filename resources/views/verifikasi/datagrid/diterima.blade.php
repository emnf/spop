<div class="tab-pane fade" id="navtab4" role="tabpanel">
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-sm-2">
            <input type="text" class="form-control form-control-sm text-center datepicker-date-fixed" id="tglcetak3"
            value="{{ date('d-m-Y') }}" readonly>
        </div>
        <div class="col-sm-3">
            <select class="form-control form-control-sm" id="ttd3">
                <option value="">Silahkan Pilih</option>
                @foreach ($datapegawai as $pegawai)
                    <option value="{{ $pegawai->s_id_pegawai }}"> {{ $pegawai->s_nama_pegawai }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-sm-2">
            <button class="btn btn-danger btn-sm" onclick="printverifikasidiverifikasi('PDF')">
                <span class="fas fa-file-pdf"></span> &nbsp;PDF</button>
            <button class="btn btn-success btn-sm" onclick="printverifikasidiverifikasi('XLS')">
                <span class="fas fa-file-excel"></span> &nbsp;XLS</button>
        </div>
    </div>
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-bordered table-striped table-hover" id="datagrid3">
                <thead>
                    <tr>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Jenis Pajak</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pelayanan</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Permohonan</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No. Pelayanan</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pemohon</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Wajib Pajak</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Verifikaasi</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Keterangan</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Action&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>
                            {{-- <select class="form-control form-control-sm" id="filter-idJenisPajak3">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($jenispajak as $jp)
                                    <option value="{{ $jp->s_id_jenis_pajak }}">{{ $jp->s_nama_jenis_pajak }}</option>
                                @endforeach
                            </select> --}}
                        </th>
                        <th>
                            <select class="form-control form-control-sm" id="filter-idJenisPelayanan3">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($jenispelayanan as $jp)
                                    <option value="{{ $jp->s_id_jenis_pelayanan }}">{{ $jp->s_nama_jenis_pelayanan }}</option>
                                @endforeach
                            </select>
                        </th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-tglPelayanan3"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-noPelayanan3"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-namaPemohon3"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-namaWp3"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-tglVerifikasi3"></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="7"> Tidak ada data.</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-12 pagination-footer"></div>
    </div>
</div>

@push('scripts')
<script>
    $('#filter-tglPelayanan3, #filter-tglVerifikasi3').daterangepicker({
        autoUpdateInput: false,
         locale: {
            cancelLabel: 'Clear'
         }
    });

    $('#filter-tglPelayanan3, #filter-tglVerifikasi3').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        search3();
    });

    $('#filter-tglPelayanan3, #filter-tglVerifikasi3').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        search3();
    });

    $("#filter-idJenisPajak3, #filter-idJenisPelayanan3").change(function(){
        search3();
    });

    $("#filter-noPelayanan3, #filter-namaPemohon3, #filter-namaWp3").keyup(function(){
        // if ($(this).val().length >= 3) {
            search3();
        // }
    });

    var datatables3 = datagrid({
        url: '/verifikasi/datagrid-diterima',
        table: "#datagrid3",
        columns: [
            {class: ""},
            {class: ""},
            {class: "text-center"},
            {class: "text-center"},
            {class: ""},
            {class: "text-center"},
            {class: ""},
            {class: "text-center"},
        ],
        orders: [
            {sortable: false},
            {sortable: true, name: "idJenisPajak"},
            {sortable: true, name: "idJenisPelayanan"},
            {sortable: true, name: "tglPelayanan"},
            {sortable: true, name: "noPelayanan"},
            {sortable: true, name: "namaPemohon"},
            {sortable: true, name: "namaWp"},
            {sortable: true, name: "verifikasi.tglVerifikasi"},
            {sortable: false},
            {sortable: false},
        ],
        action: [
            {
                name: 'tambah-pengajuan',
                btnClass: 'btn btn-success btn-xs',
                btnText: '<i class="fas fa-edit"> Pengajuan Validator</i>'
            },
        ]
    });

    function search3(){
        datatables3.setFilters({
            idJenisPajak: $("#filter-idJenisPajak3").val(),
            tglPelayanan: $("#filter-tglpelayanan3").val(),
            noPelayanan: $("#filter-noPelayanan3").val(),
            namaPemohon: $("#filter-namaPemohon3").val(),
            tglVerifikasi: $("#filter-tglVerifikasi3").val(),
            idJenisPelayanan: $("#filter-idJenisPelayanan3").val(),
            namaWp: $("#filter-namaWp3").val(),
        });
        datatables3.reload();
    }

    search3();

    function printverifikasidiverifikasi(type){
        window.open('/verifikasi/cetak-verifikasi-diterima?format=' + type
        + '&idJenisPajak=' + $("#filter-idJenisPajak3").val()
        + '&tglPelayanan=' + $("#filter-tglPelayanan3").val()
        + '&noPelayanan=' + $("#filter-noPelayanan3").val()
        + '&namaPemohon=' + $("#filter-namaPemohon3").val()
        + '&idJenisPelayanan=' + $("#filter-idJenisPelayanan3").val()
        + '&tglVerifikasi=' + $("#filter-tglVerifikasi3").val()
        + '&tglcetak=' + $("#tglcetak3").val()
        + '&ttd=' + $("#ttd3").val()
        + '');
    }
</script>
@endpush
