<div class="tab-pane fade" id="navtab2" role="tabpanel">
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-sm-2">
            <input type="text" class="form-control form-control-sm text-center datepicker-date-fixed" id="tglcetak1"
                value="{{ date('d-m-Y') }}" readonly>
        </div>
        <div class="col-sm-3">
            <select class="form-control form-control-sm" id="ttd1">
                <option value="">Silahkan Pilih</option>
                @foreach ($datapegawai as $pegawai)
                    <option value="{{ $pegawai->s_id_pegawai }}">{{ $pegawai->s_nama_pegawai }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-sm-2">
            <button class="btn btn-danger btn-sm" onclick="printverifikasipending('PDF')">
                <span class="fas fa-file-pdf"></span> &nbsp;PDF</button>
            <button class="btn btn-success btn-sm" onclick="printverifikasipending('XLS')">
                <span class="fas fa-file-excel"></span> &nbsp;XLS</button>
        </div>
    </div>

    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-bordered table-striped table-hover" id="datagrid1">
                <thead>
                    <tr>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Jenis Pajak</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pelayanan</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Permohonan</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No. Pelayanan</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pemohon</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Verifikasi</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Keterangan</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Action&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>
                            {{-- <select class="form-control form-control-sm" id="filter-idJenisPajak1">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($jenispajak as $jp)
                                    <option value="{{ $jp->s_id_jenis_pajak }}">{{ $jp->s_nama_jenis_pajak }}</option>
                                @endforeach
                            </select> --}}
                        </th>
                        <th>
                            <select class="form-control form-control-sm" id="filter-idJenisPelayanan1">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($jenispelayanan as $jp)
                                    <option value="{{ $jp->s_id_jenis_pelayanan }}">{{ $jp->s_nama_jenis_pelayanan }}</option>
                                @endforeach
                            </select>
                        </th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-tglPelayanan1"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-noPelayanan1"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-namaPemohon1"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-tglVerifikasi1"></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="8"> Tidak ada data.</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-12 pagination-footer"></div>
    </div>
</div>

@push('scripts')
<script>
    $('#filter-tglPelayanan1, #filter-tglVerifikasi1').daterangepicker({
        autoUpdateInput: false,
         locale: {
            cancelLabel: 'Clear'
         }
    });

    $('#filter-tglPelayanan1, #filter-tglVerifikasi1').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        search1();
    });

    $('#filter-tglPelayanan1, #filter-tglVerifikasi1').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        search1();
    });

    $("#filter-idJenisPajak1, #filter-idJenisPelayanan1").change(function(){
        search1();
    });

    $("#filter-namaJenisPelayanan1, #filter-noPelayanan1, #filter-namaPemohon1").keyup(function(){
        // if ($(this).val().length >= 3) {
            search1();
        // }
    });

    var datatables1 = datagrid({
        url: '/verifikasi/datagrid-perbaikan',
        table: "#datagrid1",
        columns: [
            {class: ""},
            {class: ""},
            {class: "text-center"},
            {class: "text-center"},
            {class: ""},
            {class: "text-center"},
            {class: ""},
            {class: "text-center"},
        ],
        orders: [
            {sortable: false},
            {sortable: true, name: "idJenisPajak"},
            {sortable: true, name: "idJenisPelayanan"},
            {sortable: true, name: "tglPelayanan"},
            {sortable: true, name: "noPelayanan"},
            {sortable: true, name: "namaPemohon"},
            {sortable: true, name: "verifikasi.tglVerifikasi"},
            {sortable: false},
            {sortable: false},
        ],
        action: [
            {
                name: 'edit-verifikasi-pending',
                btnClass: 'btn btn-info btn-xs',
                btnText: '<i class="fas fa-edit"> Verifikasi Ulang</i>'
            },
        ]

    });

    function search1(){
        datatables1.setFilters({
            idJenisPajak: $("#filter-idJenisPajak1").val(),
            tglPelayanan: $("#filter-tglPelayanan1").val(),
            noPelayanan: $("#filter-noPelayanan1").val(),
            namaPemohon: $("#filter-namaPemohon1").val(),
            tglVerifikasi: $("#filter-tglVerifikasi1").val(),
            idJenisPelayanan: $("#filter-idJenisPelayanan1").val(),
        });
        datatables1.reload();
    }

    search1();

    function printverifikasipending(type){
        window.open('/verifikasi/printverifikasipending?format=' + type
        + '&idJenisPajak=' + $("#filter-idJenisPajak1").val()
        + '&tglPelayanan=' + $("#filter-tglPelayanan1").val()
        + '&noPelayanan=' + $("#filter-noPelayanan1").val()
        + '&namaPemohon=' + $("#filter-namaPemohon1").val()
        + '&idJenisPelayanan=' + $("#filter-idJenisPelayanan1").val()
        + '&tglVerifikasi=' + $("#filter-tglVerifikasi1").val()
        + '&tglcetak=' + $("#tglcetak1").val()
        + '&ttd=' + $("#ttd1").val()
        + '');
    }
</script>
@endpush
