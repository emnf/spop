<div class="tab-pane fade" id="navtab6" role="tabpanel">
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-sm-2">
            <input type="text" class="form-control form-control-sm text-center datepicker-date-fixed" id="tglcetak4" th:value="${#dates.format(#dates.createNow(), 'dd-MM-yyyy')}" readonly>
        </div>
        <div class="col-sm-3">
            <select class="form-control form-control-sm" id="ttd6">
                <option value="">Silahkan Pilih</option>
                <option th:each="datapegawai: ${datapegawai}"
                    th:text="${datapegawai.namaPegawai + ' - ' + datapegawai.jabatanPegawai}"
                    th:value="${datapegawai.idPegawai}"></option>
            </select>
        </div>
        <div class="col-sm-2">
            <button class="btn btn-danger btn-sm" onclick="printverifikasilapangan('PDF')"><span class="fas fa-file-pdf"></span> &nbsp;PDF</button>
            <button class="btn btn-success btn-sm" onclick="printverifikasilapangan('XLS')"><span class="fas fa-file-excel"></span> &nbsp;XLS</button>
        </div>
    </div>
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-bordered table-striped table-hover" id="datagrid6">
                <thead>
                    <tr>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Jenis Pajak</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pelayanan</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Permohonan</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No. Pelayanan</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pemohon</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Verifikaasi</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Keterangan</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>
                            <select class="form-control form-control-sm" id="filter-idJenisPajak6">
                                <option value="">Silahkan Pilih</option>
                                <option th:each="jenispajak: ${jenispajak}"
                                    th:text="${jenispajak.namaJenisPajak}"
                                    th:value="${jenispajak.idJenisPajak}"></option>
                            </select>
                        </th>
                        <th>
                            <select class="form-control form-control-sm" id="filter-idJenisPelayanan6">
                                <option value="">Silahkan Pilih</option>
                                <option th:each="jenispelayanan: ${jenispelayanan}"
                                    th:text="${jenispelayanan.namaJenisPelayanan}"
                                    th:value="${jenispelayanan.idJenisPelayanan}"></option>
                            </select>
                        </th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-tglPelayanan6"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-noPelayanan6"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-namaPemohon6"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-tglVerifikasi6"></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="7"> Tidak ada data.</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-12 pagination-footer"></div>
    </div>
</div>

@push('scripts')
<script>
    $('#filter-tglPelayanan6, #filter-tglVerifikasi6').daterangepicker({
        autoUpdateInput: false,
         locale: {
            cancelLabel: 'Clear'
         }
    });

    $('#filter-tglPelayanan6, #filter-tglVerifikasi6').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        search6();
    });

    $('#filter-tglPelayanan6, #filter-tglVerifikasi6').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        search6();
    });

    $("#filter-idJenisPajak6, #filter-idJenisPelayanan6").change(function(){
        search6();
    });

    $("#filter-namaJenisPelayanan4, #filter-noPelayanan6, #filter-namaPemohon6").keyup(function(){
        search6();
    });

    var datatables6 = datagrid({
        url: '/verifikasi/datagrid-verifikasi-lapangan',
        table: "#datagrid6",
        columns: [
            {class: ""},
            {class: ""},
            {class: "text-center"},
            {class: "text-center"},
            {class: ""},
            {class: "text-center"},
            {class: ""},
        ],
        orders: [
            {sortable: false},
            {sortable: true, name: "idJenisPajak"},
            {sortable: true, name: "idJenisPelayanan"},
            {sortable: true, name: "tglPelayanan"},
            {sortable: true, name: "noPelayanan"},
            {sortable: true, name: "namaPemohon"},
            {sortable: true, name: "verifikasi.tglVerifikasi"},
            {sortable: false},
        ],
        action: [
        ]
    });

    function search6(){
        datatables6.setFilters({
            idJenisPajak: $("#filter-idJenisPajak6").val(),
            tglPelayanan: $("#filter-tglPelayanan6").val(),
            noPelayanan: $("#filter-noPelayanan6").val(),
            namaPemohon: $("#filter-namaPemohon6").val(),
            tglVerifikasi: $("#filter-tglVerifikasi6").val(),
            idJenisPelayanan: $("#filter-idJenisPelayanan6").val(),
        });
        datatables6.reload();
    }

    search6();

    function printverifikasilapangan(type){
        window.open('/report/printverifikasilapangan' + type
        + '?idJenisPajak=' + $("#filter-idJenisPajak6").val()
        + '&tglPelayanan=' + $("#filter-tglPelayanan6").val()
        + '&noPelayanan=' + $("#filter-noPelayanan6").val()
        + '&namaPemohon=' + $("#filter-namaPemohon6").val()
        + '&idJenisPelayanan=' + $("#filter-idJenisPelayanan6").val()
        + '&tglVerifikasi=' + $("#filter-tglVerifikasi6").val()
        + '&tglcetak=' + $("#tglcetak4").val()
        + '&ttd=' + $("#ttd6").val()
        + '');
    }
</script>
@endpush
