<div data-th-fragment="datagrid" class="tab-pane fade" id="navtab3" role="tabpanel">
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-sm-2">
            <input type="text" class="form-control form-control-sm text-center datepicker-date-fixed" id="tglcetak2"
                value="{{ date('d-m-Y') }}" readonly>
        </div>
        <div class="col-sm-3">
            <select class="form-control form-control-sm" id="ttd2">
                <option value="">Silahkan Pilih</option>
                @foreach ($datapegawai as $pegawai)
                    <option value="{{ $pegawai->s_id_pegawai }}">{{ $pegawai->s_nama_pegawai }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-sm-2">
            <button class="btn btn-danger btn-sm" onclick="printverifikasiditolak('PDF')"><span
                    class="fas fa-file-pdf"></span> &nbsp;PDF</button>
            <button class="btn btn-success btn-sm" onclick="printverifikasiditolak('XLS')"><span
                    class="fas fa-file-excel"></span> &nbsp;XLS</button>
        </div>
    </div>
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-bordered table-striped table-hover" id="datagrid2">
                <thead>
                    <tr>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Jenis Pajak</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pelayanan</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Permohonan</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No. Pelayanan</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pemohon</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Wajib Pajak</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Verifikaasi</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Keterangan</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>
                            {{-- <select class="form-control form-control-sm" id="filter-idJenisPajak2">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($jenispajak as $jp)
                                    <option value="{{ $jp->s_id_jenis_pajak }}">{{ $jp->s_nama_jenis_pajak }}</option>
                                @endforeach
                            </select> --}}
                        </th>
                        <th>
                            <select class="form-control form-control-sm" id="filter-idJenisPelayanan2">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($jenispelayanan as $jp)
                                    <option value="{{ $jp->s_id_jenis_pelayanan }}">{{ $jp->s_nama_jenis_pelayanan }}</option>
                                @endforeach
                            </select>
                        </th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-tglPelayanan2"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-noPelayanan2"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-namaPemohon2"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-namaWp2"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-tglVerifikasi2"></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="7"> Tidak ada data.</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-12 pagination-footer"></div>
    </div>
</div>

@push('scripts')
<script>
    $('#filter-tglPelayanan2, #filter-tglVerifikasi2').daterangepicker({
        autoUpdateInput: false,
         locale: {
            cancelLabel: 'Clear'
         }
    });

    $('#filter-tglPelayanan2, #filter-tglVerifikasi2').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        search2();
    });

    $('#filter-tglPelayanan2, #filter-tglVerifikasi2').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        search2();
    });

    $("#filter-idJenisPajak2, #filter-idJenisPelayanan2").change(function(){
        search2();
    });

    $("#filter-namaJenisPelayanan2, #filter-noPelayanan2").keyup(function(){
        // if ($(this).val().length >= 3) {
            search2();
        // }
    });

    $("#filter-namaPemohon2, #filter-namaWp2").keyup(function(){
        if ($(this).val().length >= 3 || $(this).val().length < 1) {
            search2();
        }
    });

    var datatables2 = datagrid({
        url: '/verifikasi/datagrid-ditolak',
        table: "#datagrid2",
        columns: [
            {class: ""},
            {class: ""},
            {class: "text-center"},
            {class: "text-center"},
            {class: ""},
            {class: ""},
            {class: "text-center"},
            {class: ""},
        ],
        orders: [
            {sortable: false},
            {sortable: true, name: "idJenisPajak"},
            {sortable: true, name: "idJenisPelayanan"},
            {sortable: true, name: "tglPelayanan"},
            {sortable: true, name: "noPelayanan"},
            {sortable: true, name: "namaPemohon"},
            {sortable: true, name: "namaWp"},
            {sortable: true, name: "verifikasi.tglVerifikasi"},
            {sortable: false},
        ],
        action: [
        ]
    });

    function search2(){
        datatables2.setFilters({
            idJenisPajak: $("#filter-idJenisPajak2").val(),
            tglPelayanan: $("#filter-tglPelayanan2").val(),
            noPelayanan: $("#filter-noPelayanan2").val(),
            namaPemohon: $("#filter-namaPemohon2").val(),
            tglVerifikasi: $("#filter-tglVerifikasi2").val(),
            idJenisPelayanan: $("#filter-idJenisPelayanan2").val(),
            namaWp: $("#filter-namaWp2").val()
        });
        datatables2.reload();
    }

    search2();

    function printverifikasiditolak(type){
        window.open('/verifikasi/cetak-verifikasi-ditolak?format=' + type
        + '&idJenisPajak=' + $("#filter-idJenisPajak2").val()
        + '&tglPelayanan=' + $("#filter-tglPelayanan2").val()
        + '&noPelayanan=' + $("#filter-noPelayanan2").val()
        + '&namaPemohon=' + $("#filter-namaPemohon2").val()
        + '&idJenisPelayanan=' + $("#filter-idJenisPelayanan2").val()
        + '&tglVerifikasi=' + $("#filter-tglVerifikasi2").val()
        + '&tglcetak=' + $("#tglcetak2").val()
        + '&ttd=' + $("#ttd2").val()
        + '');
    }
</script>
@endpush
