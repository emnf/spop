<div class="tab-pane fade" id="navtab7" role="tabpanel">
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-sm-2">
            <input type="text" class="form-control form-control-sm text-center datepicker-date-fixed" id="tglcetak5"
            value="{{ date('d-m-Y') }}" readonly>
        </div>
        <div class="col-sm-3">
            <select class="form-control form-control-sm" id="ttd5">
                <option value="">Silahkan Pilih</option>
                @foreach ($datapegawai as $pegawai)
                    <option value="{{ $pegawai->s_id_pegawai }}"> {{ $pegawai->s_nama_pegawai }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-sm-2">
            <button class="btn btn-danger btn-sm" onclick="printverifikasisemuadata('PDF')">
                <span class="fas fa-file-pdf"></span> &nbsp;PDF</button>
            <button class="btn btn-success btn-sm" onclick="printverifikasisemuadata('XLS')">
                <span class="fas fa-file-excel"></span> &nbsp;XLS</button>
        </div>
    </div>
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-bordered table-striped table-hover" id="datagrid5">
                <thead>
                    <tr>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Jenis Pajak</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pelayanan</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Permohonan</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">No. Pelayanan</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Pemohon</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Nama Wajib Pajak</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Status</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Tgl. Verifikasi</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">Keterangan</th>
                        <th style="background-color: #1fc8e3; vertical-align: top;" class="text-center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Action&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>
                            {{-- <select class="form-control form-control-sm" id="filter-idJenisPajak5">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($jenispajak as $jp)
                                    <option value="{{ $jp->s_id_jenis_pajak }}">{{ $jp->s_nama_jenis_pajak }}</option>
                                @endforeach
                            </select> --}}
                        </th>
                        <th>
                            <select class="form-control form-control-sm" id="filter-idJenisPelayanan5">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($jenispelayanan as $jp)
                                    <option value="{{ $jp->s_id_jenis_pelayanan }}">{{ $jp->s_nama_jenis_pelayanan }}</option>
                                @endforeach
                            </select>
                        </th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-tglPelayanan5"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-noPelayanan5"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-namaPemohon5"></th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-namaWp5"></th>
                        <th>
                            <select class="form-control form-control-sm" id="filter-idStatusVerifikasi5">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($statusVerifikasi as $status)
                                    <option value="{{ $status->s_id_status_verifikasi }}">{{ $status->s_nama_status_verifikasi}}</option>
                                @endforeach
                            </select>
                        </th>
                        <th><input class="form-control form-control-sm" type="text" id="filter-tglVerifikasi5"></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="8"> Tidak ada data.</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-12 pagination-footer"></div>
    </div>
</div>

@push('scripts')
<script th:inline="javascript" data-th-fragment="js">
    $('#filter-tglPelayanan5, #filter-tglVerifikasi5').daterangepicker({
        autoUpdateInput: false,
         locale: {
            cancelLabel: 'Clear'
         }
    });

    $('#filter-tglPelayanan5, #filter-tglVerifikasi5').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        search5();
    });

    $('#filter-tglPelayanan5, #filter-tglVerifikasi5').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        search5();
    });

    $("#filter-idJenisPajak5, #filter-idStatusVerifikasi5, #filter-idJenisPelayanan5").change(function(){
        search5();
    });

    $("#filter-noPelayanan5").keyup(function(){
        search5();
    });

    $("#filter-namaPemohon5, #filter-namaWp5").keyup(function(){
        if ($(this).val().length >= 3 || $(this).val().length < 1) {
            search5();
        }
    });

    var datatables5 = datagrid({
        url: '/verifikasi/datagrid-semua',
        table: "#datagrid5",
        columns: [
            {class: ""},
            {class: ""},
            {class: "text-center"},
            {class: "text-center"},
            {class: ""},
            {class: "text-center"},
            {class: "text-center"},
            {class: ""},
            {class: "text-center"},
        ],
        orders: [
            {sortable: false},
            {sortable: true, name: "idJenisPajak"},
            {sortable: true, name: "idJenisPelayanan"},
            {sortable: true, name: "tglPelayanan"},
            {sortable: true, name: "noPelayanan"},
            {sortable: true, name: "namaPemohon"},
            {sortable: true, name: "namaWp"},
            {sortable: true, name: "verifikasi.caseStatusVerifikasi"},
            {sortable: true, name: "verifikasi.tglVerifikasi"},
            {sortable: false},
            {sortable: false},
        ],
        action: [
            {
                name: 'tambah-verifikasibelum',
                btnClass: 'btn btn-warning btn-xs',
                btnText: '<i class="fas fa-edit"> Verifikasi</i>'
            },
            {
                name: 'edit-verifikasi-pending',
                btnClass: 'btn btn-info btn-xs',
                btnText: '<i class="fas fa-edit"> Verifikasi Ulang</i>'
            },
            {
                name: 'tambah-pengajuan',
                btnClass: 'btn btn-success btn-xs',
                btnText: '<i class="fas fa-edit"> Pengajuan Validator</i>'
            },
        ]

    });

    function search5(){
        datatables5.setFilters({
            idJenisPajak: $("#filter-idJenisPajak5").val(),
            tglPelayanan: $("#filter-tglPelayanan5").val(),
            noPelayanan: $("#filter-noPelayanan5").val(),
            namaPemohon: $("#filter-namaPemohon5").val(),
            idStatusVerifikasi: $("#filter-idStatusVerifikasi5").val(),
            tglVerifikasi: $("#filter-tglVerifikasi5").val(),
            idJenisPelayanan: $("#filter-idJenisPelayanan5").val(),
            namaWp: $("#filter-namaWp5").val()
        });
        datatables5.reload();
    }

    search5();

    function printverifikasisemuadata(type){
        window.open('/verifikasi/cetak-verifikasi-semua-data?format=' + type
        + '&idJenisPajak=' + $("#filter-idJenisPajak5").val()
        + '&tglPelayanan=' + $("#filter-tglPelayanan5").val()
        + '&noPelayanan=' + $("#filter-noPelayanan5").val()
        + '&namaPemohon=' + $("#filter-namaPemohon5").val()
        + '&idJenisPelayanan=' + $("#filter-idJenisPelayanan5").val()
        + '&tglVerifikasi=' + $("#filter-tglVerifikasi5").val()
        + '&idStatusVerifikasi=' + $("#filter-idStatusVerifikasi5").val()
        + '&tglcetak=' + $("#tglcetak5").val()
        + '&ttd=' + $("#ttd5").val()
        + '');
    }
</script>
@endpush

