@push('styles')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css"
    integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="" />
@endpush
<form class="" method="post" action="{{ $actionUrl }}" id="formverifikasi">
    @csrf
    <div class="row">
        @include('components.data-view.data-user')
        <div class="col-12">
            <div class="card" style="border-top: 5px solid #bd2130;">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="invoice p-3 mb-3">
                                <div class="row invoice-info">
                                    <div class="col-sm-6 invoice-col border-right">
                                        @include('pelayanan.pbb.components.data-pemohon')
                                    </div>
                                    <div class="col-sm-6 invoice-col">
                                        <h5>Data Pelayanan</h5>
                                        <hr>
                                        @include('pelayanan.pbb.components.data-view.data-pelayanan')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="card" style="border-top:5px solid #dc3545;">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="invoice p-3 mb-3">
                                <div class="row invoice-info">
                                    <div class="col-sm-6 invoice-col border-right">
                                        @include('pelayanan.pbb.components.data-view.objek-pajak')
                                    </div>
                                    <div class="col-sm-6 invoice-col">
                                        @include('pelayanan.pbb.components.data-view.wajib-pajak')
                                    </div>
                                </div>
                                @if (!in_array($pelayanan->t_id_jenis_pelayanan, [5, 6]))
                                    @if (count($pelayanan->OpLamas) > 0)
                                        <div class="row invoice-info pt-3">
                                            <div class="col-sm-6 invoice-col border-right">
                                                @include('pelayanan.pbb.components.data-view.objek-pajak-lama')
                                            </div>
                                            <div class="col-sm-6 invoice-col">
                                                @include('pelayanan.pbb.components.data-view.wajib-pajak-lama')
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if(!in_array($pelayanan->t_id_jenis_pelayanan, [3,5,6,7,8,9]))
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <b>LOKASI OBJEK</b>
                    </div>
                    <div class="card-body">
                        <div id="map" style="width: 100%; height: 400px">
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>

    @if(!isset($pelayanan->detailBangunan))
    <div class="row">
        <div class="col-12">
            <div class="invoice p-3 mb-3" style="border-top:3px solid #dc3545;">
                <div class="row invoice-info">
                    <div class="col-sm-6 invoice-col border-right">
                        <h5>DATA BANGUNAN</h5>
                        <hr>
                        <address>
                            @foreach ($pelayanan->detailBangunan as $key => $item)
                            <div class="row">
                                <div class="col-5">Luas Bangunan Ke-{{ $item->t_no_urut_bangunan }}</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b>{{ $item->t_luas }}</b> (M<sup>2</sup>)</div>
                            </div>
                            @endforeach
                        </address>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    <div class="row d-none">
        <div class="col-12" th:if="${count > 1}" th:each="databangunan,p : ${databangunan}">
            <div class="invoice p-3 mb-3" style="border-top:3px solid #dc3545;">
                <div class="row invoice-info">

                    <div class="col-sm-6 invoice-col border-right">
                        <h5 th:text="'DATA BANGUNAN KE-'+${p.index+1}"></h5>
                        <hr>
                        <address>
                            <div class="row">
                                <div class="col-5">Jenis Penggunaan Bangunan</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.jenisPenggunaanBangunan}"></b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-5">Jumlah Lantai</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.jumlahLantai}"></b></div>
                            </div>
                            <div class="row">
                                <div class="col-5">Tahun Bibangunan</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.tahunBangunan}"></b></div>
                            </div>
                            <div class="row">
                                <div class="col-5">Tahun Direnovasi</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.tahunRenovasi}"></b></div>
                            </div>
                            <div class="row">
                                <div class="col-5">Kondisi Pada Umumnya</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.kondisiBangunan}"></b></div>
                                <div class="col-5">Konstruksi</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.Konstruksi}"></b></div>
                            </div>
                            <div class="row">
                                <div class="col-5">Atap</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.atap}"></b></div>
                            </div>
                            <div class="row">
                                <div class="col-5">Dinding</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.dinding}"></b></div>
                            </div>
                            <div class="row">
                                <div class="col-5">Lantai</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.lantai}"></b></div>
                            </div>
                            <div class="row">
                                <div class="col-5">Langit-Langit</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.langitLangit}"></b></div>
                            </div>

                        </address>
                    </div>
                    <div class="col-sm-6 invoice-col border-right">
                        <h5>FASILITAS</h5>
                        <hr>
                        <address>
                            <div class="row">
                                <div class="col-5">AC Split</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.acSplit}"></b></div>
                            </div>
                            <div class="row">
                                <div class="col-5">AC Window</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.acWindow}"></b></div>
                            </div>
                            <div class="row">
                                <div class="col-5">Panjang Pagar (M)</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.panjangPagar}"></b></div>
                            </div>
                            <div class="row">
                                <div class="col-5">Bahan Pagar</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.bahanPagar}"></b></div>

                            </div>
                        </address>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="invoice p-3 mb-3" style="border-top:3px solid #dc3545;">
                <div class="row invoice-info">

                    <div class="col-sm-6 invoice-col border-right">
                        <h5>DATA BANGUNAN</h5>
                        <hr>
                        <address>
                            <div class="row">
                                <div class="col-5">Jenis Penggunaan Bangunan</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.jenisPenggunaanBangunan}"></b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-5">Jumlah Lantai</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.jumlahLantai}"></b></div>
                            </div>
                            <div class="row">
                                <div class="col-5">Tahun Bibangunan</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.tahunBangunan}"></b></div>
                            </div>
                            <div class="row">
                                <div class="col-5">Tahun Direnovasi</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.tahunRenovasi}"></b></div>
                            </div>
                            <div class="row">
                                <div class="col-5">Kondisi Pada Umumnya</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.kondisiBangunan}"></b></div>
                                <div class="col-5">Konstruksi</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.Konstruksi}"></b></div>
                            </div>
                            <div class="row">
                                <div class="col-5">Atap</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.atap}"></b></div>
                            </div>
                            <div class="row">
                                <div class="col-5">Dinding</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.dinding}"></b></div>
                            </div>
                            <div class="row">
                                <div class="col-5">Lantai</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.lantai}"></b></div>
                            </div>
                            <div class="row">
                                <div class="col-5">Langit-Langit</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.langitLangit}"></b></div>
                            </div>

                        </address>
                    </div>
                    <div class="col-sm-6 invoice-col border-right">
                        <h5>FASILITAS</h5>
                        <hr>
                        <address>
                            <div class="row">
                                <div class="col-5">AC Split</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.acSplit}"></b></div>
                            </div>
                            <div class="row">
                                <div class="col-5">AC Window</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.acWindow}"></b></div>
                            </div>
                            <div class="row">
                                <div class="col-5">Panjang Pagar (M)</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.panjangPagar}"></b></div>
                            </div>
                            <div class="row">
                                <div class="col-5">Bahan Pagar</div>
                                <div class="col-1">:</div>
                                <div class="col-6"><b th:text="${databangunan.bahanPagar}"></b></div>

                            </div>
                        </address>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-12">
            <div class="card" style="border-top: 5px solid #bd2130;">
                <div class="card-header">
                    <b>Persyaratan Permohonan</b>
                </div>
                <div class="card-body">
                    <div class="row table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="width: 4rem;">No</th>
                                    <th>Nama Persyaratan</th>
                                    <th>File Persyaratan</th>
                                    <th>Persyaratan Sesuai</th>
                                </tr>
                            </thead>
                            <tbody id="dataupload">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="card" style="border-top: 5px solid #bd2130;">
                <div class="card-body">
                    <div class="row">
                        <label class="col-sm-12">PERSETUJUAN VERIFIKASI</label>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-12" style="padding-bottom: 20px;">
                            Berdasarkan informasi yang sudah diajukan diatas. Apakah verifikator menyetujui dengan
                            permohonan tersebut? Apabila sudah, maka permohonan ini tidak akan bisa dilakukan perubahan.
                            Apabila belum, maka bisa diberikan keterangan atau alasan kenapa permohonan
                            ini dipending, ditolak, atau diajukan validator.
                        </div>
                    </div>
                    {{-- <div class="row">
                        <div class="col-12">
                            <div>
                                <div>
                                    <div class="row">
                                        <div class="col-2">Kejelasan</div>
                                        <div class="col-1">:</div>
                                        <input type="hidden" name="t_kejelasan" id="t_kejelasan">
                                        <div class="col-1">
                                            <input type="checkbox" class="radio" onclick="ceklisKejelasan(this)" id="t_kejelasan_ya" name="t_kejelasan_value" value="1"> Ya
                                        </div>
                                        <div>
                                            <input type="checkbox" class="radio" onclick="ceklisKejelasan(this)" id="t_kejelasan_tidak" name="t_kejelasan_value" value="2"> Tidak
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-2">Kebenaran</div>
                                        <div class="col-1">:</div>
                                        <input type="hidden" name="t_kebenaran" id="t_kebenaran">
                                        <div class="col-1">
                                            <input type="checkbox" class="radio" onclick="ceklisKejelasan(this)" id="t_kebenaran_ya" name="t_kebenaran_value" value="1"> Ya
                                        </div>
                                        <div>
                                            <input type="checkbox" class="radio" onclick="ceklisKejelasan(this)" id="t_kebenaran_tidak" name="t_kebenaran_value" value="2"> Tidak
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-2">Keabsahan</div>
                                        <div class="col-1">:</div>
                                        <input type="hidden" name="t_keabsahan" id="t_keabsahan">
                                        <div class="col-1">
                                            <input type="checkbox" class="radio" onclick="ceklisKejelasan(this)" id="t_keabsahan_ya" name="t_keabsahan_value" value="1"> Ya
                                        </div>
                                        <div>
                                            <input type="checkbox" class="radio" onclick="ceklisKejelasan(this)" id="t_keabsahan_tidak" name="t_keabsahan_value" value="2"> Tidak
                                        </div>
                                    </div><br>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="keteranganVerifikasi">Catatan :</label> {{-- <span class="text-danger">*</span> --}}
                                <input type="hidden" name="t_id_verifikasi" id="t_id_verifikasi" value="{{ $verifikasi->t_id_verifikasi }}">
                                <input type="hidden" name="t_id_pelayanan" id="t_id_pelayanan" value="{{ $pelayanan->t_id_pelayanan }}">
                                <input type="hidden" name="t_no_verifikasi" id="t_no_verifikasi" value="{{ $verifikasi->t_no_verifikasi ?? ''}}">
                                <input type="hidden" name="t_tgl_verifikasi" id="t_tgl_verifikasi" value="{{ date('d-m-Y') }}">
                                <input type="hidden" name="t_id_status_verifikasi" id="t_id_status_verifikasi" value="{{ $verifikasi->t_id_status_verifikasi ?? '' }}">
                                <textarea class="form-control" name="t_keterangan_verifikasi" id="t_keterangan_verifikasi" {{-- required  --}}>{{ $verifikasi->t_keterangan_verifikasi ?? '' }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="col-12 d-flex justify-content-between">
                        <button type="button" class="btn btn-danger" id="btnReject" data-toggle="modal" data-target="#modal-cancel">
                            <i class="fas fa-times-circle"></i> Tolak Permohonan
                        </button>
                        <div class="modal fade" id="modal-cancel">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header bg-danger">
                                        <h6 class="modal-title"><span class="fas fa-exclamation"></span> &nbsp;TOLAK
                                            PENGAJUAN PERMOHONAN ?</h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Apakah anda yakin ingin menolak pengajuan data permohonan tersebut?
                                        </p>
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="fas fa-times-circle"></span>
                                            &nbsp;Tutup
                                        </button>
                                        <button type="submit" class="btn btn-danger btn-sm"><span class="fas fa-exclamation"></span> &nbsp;IYA</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-primary float-right" id="btnApprove" style="margin-right: 5px;" data-toggle="modal" data-target="#modal-save">  {{-- disabled="true" --}}
                            <i class="fas fa-check-circle"></i> Proses Permohonan</button>
                        <div class="modal fade" id="modal-save">
                            <div class="modal-dialog">
                                <div class="modal-content" style="width: 101.5%;">
                                    <div class="modal-header bg-primary">
                                        <h6 class="modal-title"><span class="fas fa-send"></span> &nbsp;PROSES
                                            PERMOHONAN
                                            PELAYANAN</h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <span class="text-danger" id="peringatanPersyaratan"></span>
                                        <p>Apakah anda ingin memproses layanan ini?</p>
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="fas fa-times-circle"></span>
                                            &nbsp;Tutup
                                        </button>
                                        <button type="submit" class="btn btn-primary btn-sm"><span class="fas fa-check"></span> &nbsp;IYA
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</form>

<div class="modal fade" id="modal-image">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h6 class="modal-title" id="modal-image-title">&nbsp;FILE PERSYARATAN</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center" id="imagePlaceholder"></div>
            <div class="modal-footer justify-content-between"></div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-SudahAjukan">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-exclamation"></span> &nbsp;PEMBERITAHUAN
                </h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="tbodyMessages">
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-info btn-sm" data-dismiss="modal"><span class="fas fa-check"></span>
                    &nbsp;OK
                </button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
{{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyq1l-IUYHhDWoWwF4HqTTVw-ixal-JzQ&callback=initMap&libraries=&v=weekly" async defer></script> --}}

{{-- <script type="text/javascript">
    $(function() {
        initMap();
    });

    function initMap() {
        let latitude = {{ $objekPajak->t_latitude ?? 0 }};
        let longitude = {{ $objekPajak->t_longitude ?? 0 }};

        if (latitude == 0 && longitude == 0) {
            var latlng = {
                lat: -3.4278,
                lng: 115.9978
            };
        } else {
            var latlng = {
                lat: latitude,
                lng: longitude
            };
        }

        var map = new google.maps.Map(document.getElementById('map'), {
            center: latlng,
            scrollwheel: false,
            zoom: 17,
        });

        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            draggable: false,
            title: "Lokasi Objek Pajak"
        });

        google.maps.event.trigger(map, "resize");
    }
</script> --}}

<script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js"
    integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>
<script>
    let latitude = {{ $objekPajak->t_latitude ?? 0 }};
    let longitude = {{ $objekPajak->t_longitude ?? 0 }};
    const map = new L.map('map').setView([latitude, longitude], 13);

    const tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);

    const marker = L.marker([latitude, longitude], {
        draggable: true
    }).addTo(map);

    marker.on('dragend', function (e) {
        document.getElementById('t_latitude').value = marker.getLatLng().lat;
        document.getElementById('t_longitude').value = marker.getLatLng().lng;
    });

</script>

<script type="text/javascript">
    var no;

    var idpelayanan = $("#t_id_pelayanan").val();

    $("#btnReject").click(function() {
        $("#t_id_status_verifikasi").val(3);
    });

    $("#btnApprove").click(function() {
        const persyaratan = $('input[name^="checklistPersyaratan"]')
        let checked = 0;
        for (let index = 0; index < persyaratan.length; index++) {
            if ($("#" + persyaratan[index].id).is(":checked")) {
                checked += 1;
            }
        }
        $("#peringatanPersyaratan").html('')
        if (checked == 0) {
            $("#peringatanPersyaratan").html('Jika persyaratan tidak ada yang di ceklis, permohonan ini akan ditolak.');
        }

        $("#t_id_status_verifikasi").val(2);
    });

    function poplateFileUpload() {
        $("#dataupload").html("");
        no = 1;

        $.get('/pelayanan/upload-file/' + idpelayanan).then(function(data) {
            $span = '<span class="text-danger">*</span>';

            data.forEach(r => {
                row = `<tr>
                        <td style="text-align: center;">${no}</td>
                        <td>${ r.t_nama_persyaratan} ${(r.s_is_optional == false) ? $span : ''}</td>
                        <td>
                            <a type="button" class="text-primary" link="/${ r.t_lokasi_file }" id="labelImage_${ r.t_id_persyaratan }" name="${ r.t_nama_persyaratan}" onclick="showImage(this)"> ${ r.t_nama_persyaratan}</a>
                        </td>
                        <td style="text-align: center;">
                            <input type="checkbox" id="checklistPersyaratan_${ r.t_id_persyaratan }" name="checklistPersyaratan[]" value="${r.t_id_persyaratan}" disabled="true">
                        </td>
                    </tr>`;
                $("#dataupload").append(row);
                no++;
            });

            if ($("#t_id_verifikasi").val()) {
                checkedUpload();
            }

        });
    }

    function checkedUpload() {
        $.get('/verifikasi/checked-upload/' + idpelayanan).then(function(res) {

            if (res.data.length > 0) {
                res.data.map(a => {
                    $("#checklistPersyaratan" + a).prop('checked', true);
                })
            }
        });
    }

    function showImage(img) {
        var link = $(img).attr("link");
        var name = $(img).attr("name");
        var id = img.id.split("_");

        $("#modal-image-title").text(name);
        var image = $('<img style="max-height:100%; max-width:100%;">');
        image.attr("src", link);
        $("#imagePlaceholder").html(image);
        $("#modal-image").modal('show');
        $("#checklistPersyaratan_" + id[1]).attr('disabled', false);
        return false;
    }

    poplateFileUpload();


    //tambahan
// kulon progo minta di hilangkan ceklistnya.

    // function ceklisKejelasan(a) {
    //     let target = a.name.replace("_value", "");
    //     if (a.checked) {
    //         $("#" + target).val(a.value);
    //     } else {
    //         $("#" + target).val('');
    //     }
    //     enableProsesBtn();
    // }

    // function enableProsesBtn() {
    //     const kejelasan = $("#t_kejelasan").val();
    //     const kebenaran = $("#t_kebenaran").val();
    //     const keabsahan = $("#t_keabsahan").val();

    //     if (keabsahan != '' && kejelasan != '' && kebenaran != '') {
    //         $("#btnApprove").prop('disabled', false);
    //     } else {
    //         $("#btnApprove").prop('disabled', true);
    //     }
    // }


    $(document).ready(function() {
        if ($('#nopPelayanan').val() !== '') {
            getDataSppt();
        }
    });

    function getDataSppt(params) {
        $.get('/pelayanan/getDataSppt', {
            nop: $('#nopPelayanan').val(),
            tahunPajak: $('#tahunPajakPelayanan').val(),
        }, (res) => {
            $("#op_nomor").html($('#nopPelayanan').val());
            $("#op_jalan").html(res.jalan_op);
            $("#op_rt").html(res.rt_op);
            $("#op_rw").html(res.rw_op);
            $("#op_block").html(res.blok_kav_no_op);
            $("#op_desa").html(res.nm_kelurahan);
            $("#op_kecamatan").html(res.nm_kecamatan);
            $("#op_luas_tanah").html(res.luas_bumi_sppt);
            $("#op_luas_bangunan").html(res.luas_bng_sppt);
            $("#op_nomor").html($('#nopPelayanan').val());

            $("#wp_jalan").html(res.jalan_op);
            $("#wp_rt").html(res.rt_op);
            $("#wp_rw").html(res.rw_op);
            $("#wp_desa").html(res.nm_kelurahan);
            $("#wp_kecamatan").html(res.nm_kecamatan);
            $("#wp_kab_kota").html(res.t_kabupaten_wp);
            $("#wp_telp").html(res.t_no_hp_wp);
            $("#wp_email").html(res.t_email);
            $("#npwpd").html(res.t_npwpd);

        })
    }


    $("input:checkbox").on('click', function() {
        var $box = $(this);
        if ($box.is(":checked")) {
            var group = "input:checkbox[name='" + $box.attr("name") + "']";
            $(group).prop("checked", false);
            $box.prop("checked", true);
        } else {
            $box.prop("checked", false);
        }
    });
</script>
@endpush

{{-- <th:block th:switch="${pelayanan.jenisPajak.idKategoriPajak}">
    <th:block th:case="'2'">
        <script data-th-replace="pelayanan/common/data-objek/__${pelayanan.jenisPelayanan.alias}__ :: js"></script>
    </th:block>
</th:block> --}}
