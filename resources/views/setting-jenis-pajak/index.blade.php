@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Setting Jenis Pajak
@endsection

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Setting</a></li>
                        <li class="breadcrumb-item active">Grid</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-12">
                    <div class="info-box" id="background2">
                        <div class="col-sm-3">
                            <div class="description-block" style="float: left;">
                                <h1 class="description-header">SETTING JENIS PAJAK</h1>
                            </div>
                            <!-- /.description-block -->
                        </div>
                    </div>
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-body table-responsive">
                            <table class="table table-bordered table-striped table-hover" id="datagrid-table">
                                <thead>
                                    <tr>
                                        <th style="width: 10px; background-color: #1fc8e3">No</th>
                                        <th style="width: 10px; background-color: #1fc8e3">Icon</th>
                                        <th style="background-color: #1fc8e3">Nama Jenis Pajak</th>
                                        <th style="background-color: #1fc8e3">Nama Singkat</th>
                                        <th style="background-color: #1fc8e3">Kategori Pajak</th>
                                        <th style="background-color: #1fc8e3">Active</th>
                                        <th style="background-color: #1fc8e3">Action</th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th><input class="form-control form-control-sm" id="filter-s_nama_jenis_pajak"
                                                type="text" /></th>
                                        <th><input class="form-control form-control-sm" id="filter-s_nama_singkat_pajak"
                                                type="text" /></th>
                                        <th><select class="form-control form-control-sm" id="filter-s_id_kategori_pajak">
                                                <option value="">Silahkan Pilih</option>
                                                @foreach ($kategori as $row)
                                                    <option value="{{ $row['s_id_kategori_pajak'] }}">
                                                        {{ $row['s_nama_kategori_pajak'] }}</option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th><select class="form-control form-control-sm" id="filter-s_active">
                                                <option value="">Pilih</option>
                                                <option value="true">Ya</option>
                                                <option value="false">Tidak</option>
                                            </select></th>
                                        <th>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center" colspan="7"> Tidak ada data.</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer clearfix pagination-footer">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        var datatables = datagrid({
            url: '/setting-jenis-pajak/datagrid',
            table: "#datagrid-table",
            columns: [{
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: "text-center"
                },
                {
                    class: "text-center"
                },
                {
                    class: ""
                },
            ],
            orders: [{
                    sortable: false
                },
                {
                    sortable: true,
                    name: "s_icon"
                },
                {
                    sortable: true,
                    name: "s_nama_jenis_pajak"
                },
                {
                    sortable: true,
                    name: "s_nama_singkat_pajak"
                },
                {
                    sortable: true,
                    name: "s_nama_kategori_pajak"
                },
                {
                    sortable: true,
                    name: "s_active"
                },
                {
                    sortable: false
                }
            ],
            action: [{
                name: 'edit',
                btnClass: 'btn btn-outline-primary btn-sm mx-1',
                btnText: '<i class="fas fa-pen"></i>'
            }]

        });

        $("#filter-s_nama_jenis_pajak, #filter-s_nama_singkat_pajak, #filter-s_nama_kategori_pajak, #filter-s_active")
            .keyup(function() {
                search();
            });

        $("#filter-s_id_kategori_pajak, #filter-s_active").change(function() {
            search();
        });

        function search() {
            datatables.setpageNumber(0);
            datatables.setFilters({
                s_nama_jenis_pajak: $("#filter-s_nama_jenis_pajak").val(),
                s_nama_singkat_pajak: $("#filter-s_nama_singkat_pajak").val(),
                s_nama_kategori_pajak: $("#filter-s_nama_kategori_pajak").val(),
                s_id_kategori_pajak: $("#filter-s_id_kategori_pajak").val(),
                s_active: $("#filter-s_active").val()
            });
            datatables.reload();
        }
        search();

        function showDeleteDialog(a) {
            Swal.fire({
                title: 'Hapus Data',
                text: "Apa anda Yakin ingin menghapus ?",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Tidak',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '/setting-pegawai/delete/' + a,
                        type: 'delete',
                        dataType: "JSON",
                        data: {
                            s_id_pegawai: a
                        },
                        success: function(request, error) {
                            if (request == 'success') {
                                Toast.fire({
                                    icon: 'success',
                                    title: 'Data Berhasil Dihapus'
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: 'Data Gagal Dihapus'
                                });
                            }
                            datatables.reload();
                        }
                    });
                }
            })
        }

        function cetakData(a) {
            window.open(
                `{{ url('setting/pegawai/cetak-daftar') }}?type-cetak=${a}`
            );
        }
    </script>
@endPush
