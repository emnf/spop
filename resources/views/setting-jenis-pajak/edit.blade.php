@extends('layouts.app')

@section('title')
    KEPOIN | {{ $pemda->s_nama_singkat_instansi }} | {{ strtoupper($pemda->s_nama_kabkota) }} |Setting Pegawai
@endsection

@section('content')
    <section>
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a th:href="@{/setting-jenis-pajak}">Setting</a></li>
                            <li class="breadcrumb-item active">Edit</li>
                        </ol>
                    </div><!-- /.col -->
                    <div class="col-12">
                        <div class="info-box" id="background2"
                            style="background: url({{ asset('/asset/images/bgpelayanan.gif') }}) #fff no-repeat center center;
                            -moz-background-size: cover;
                            -o-background-size: cover;
                            background-size: cover;">
                            <div class="col-sm-3">
                                <div class="description-block" style="float: left;">
                                    <h1 class="description-header">SETTING JENIS PAJAK [EDIT]</h1>
                                </div>
                                <!-- /.description-block -->
                            </div>
                        </div>
                    </div>
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-outline card-info">
                            <form class="form-horizontal form-validation" method="POST"
                                action="{{ route('setting-jenis-pajak.update', $jenisPajak->s_id_jenis_pajak) }}">
                                @csrf
                                @method('PUT')
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_icon">Icon Pajak</label>
                                        <div class="col-sm-6">
                                            <i class="{{ $jenisPajak->s_icon }}"></i>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_nama_jenis_pajak">Nama Jenis
                                            Pajak</label>
                                        <div class="col-sm-8">
                                            <input id="s_id_jenis_pajak" name="s_id_jenis_pajak"
                                                value="{{ $jenisPajak->s_id_jenis_pajak }}" type="hidden">
                                            <input class="form-control" id="s_nama_jenis_pajak" name="s_nama_jenis_pajak"
                                                required type="text"
                                                value="{{ old('s_nama_jenis_pajak') ?? $jenisPajak->s_nama_jenis_pajak }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_nama_singkat_pajak">Nama Singkat
                                            Pajak</label>
                                        <div class="col-sm-4">
                                            <input class="form-control" id="s_nama_singkat_pajak"
                                                name="s_nama_singkat_pajak" required type="text"
                                                value="{{ old('s_nama_singkat_pajak') ?? $jenisPajak->s_nama_singkat_pajak }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_id_kategori_pajak">Kategori
                                            Pajak</label>
                                        <div class="col-sm-4">
                                            <select class="form-control" id="s_id_kategori_pajak" name="s_id_kategori_pajak"
                                                required>
                                                <option value="">Silahkan Pilih</option>
                                                @foreach ($kategori as $row)
                                                    <option
                                                        {{ $jenisPajak->s_id_kategori_pajak == $row['s_id_kategori_pajak'] ? 'selected' : '' }}
                                                        value="{{ $row['s_id_kategori_pajak'] }}">
                                                        {{ $row['s_nama_kategori_pajak'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="s_active" required>Active</label>
                                        <div class="col-sm-4">
                                            <select class="form-control" id="s_active" name="s_active">
                                                <option {{ $jenisPajak->s_active == true ? 'selected' : '' }}
                                                    value="true">Ya</option>
                                                <option {{ $jenisPajak->s_active == false ? 'selected' : '' }}
                                                    value="false">Tidak</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-info" type="submit">SIMPAN</button>
                                    <a class="btn btn-danger float-right"
                                        href="{{ route('setting-jenis-pajak') }}">KEMBALI</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
