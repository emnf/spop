<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Dokumen\DokumenController;
use App\Http\Controllers\Esign\EsignController;
use App\Http\Controllers\HasilPelayanan\HasilPelayananController;
use App\Http\Controllers\LandingPage\LandingpageController;
use App\Http\Controllers\Notifikasi\NotifikasiController;
use App\Http\Controllers\Pbb\DatObjekPajakController;
use App\Http\Controllers\PelacakanDokumen\PelacakanDokumenController;
use App\Http\Controllers\PelayananController;
use App\Http\Controllers\Penetapan\PenetapanController;
use App\Http\Controllers\RolePermission\RolesController;
use App\Http\Controllers\Settings\AboutController;
use App\Http\Controllers\Settings\JenisHasilPelayananController;
use App\Http\Controllers\Settings\JenisPajakController;
use App\Http\Controllers\Settings\JenisPelayananController;
use App\Http\Controllers\Settings\KecamatanController;
use App\Http\Controllers\Settings\KelurahanController;
use App\Http\Controllers\Settings\PegawaiController;
use App\Http\Controllers\Settings\PemdaController;
use App\Http\Controllers\Settings\PersyaratanController;
use App\Http\Controllers\Settings\DokumenPendukungController;
use App\Http\Controllers\Settings\UserController;
use App\Http\Controllers\Settings\AlurPelayananController;
use App\Http\Controllers\Settings\FaqController;
use App\Http\Controllers\Settings\UserManualController;
use App\Http\Controllers\Validasi\ValidasiController;
use App\Http\Controllers\Validasi\ValidasiFinalController;
use App\Http\Controllers\Verifikasi\VerifikasiController;
use App\Http\Controllers\Verifikasi\VerifikasiLapanganController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__ . '/auth.php';

Route::get('/', [LandingpageController::class, 'index']);
Route::get('jenis-pelayanan/{pajak}', [LandingpageController::class, 'jenisPelayanan']);


Route::middleware(['auth', 'verified'])->group(function () {

    Route::get('/home', [DashboardController::class, 'index'])->name('home');

    Route::prefix('pelayanan')->middleware('permission:Pelayanan')->controller(PelayananController::class)->group(function () {
        Route::get('', 'index')->name('pelayanan');
        Route::get('jenis-layanan/{jenisPajak}', 'jenisLayanan')->name('pelayanan.jenis-layanan');
        Route::post('datagrid', 'datagrid');
        Route::get('tambah/{jenisPajak}/{jenisPelayanan}', 'create')->name('pelayanan.tambah');

        // OP BARU
        Route::post('save-op-baru', 'storeObjekBaru')->name('pelayanan.saveopbaru');
        Route::get('bangunan/{pelayanan}', 'createBangunan')->name('pelayanan.bangunan-op');
        Route::post('bangunan/{pelayanan}/save', 'storeBangunan')->name('pelayanan.bangunan-op-save');
        Route::get('detailBangunan/{bangunanId}', 'getDetileBangunan');
        // MUTASI WP
        Route::post('save-mutasi-wp', 'storeMutasiWp')->name('pelayanan.savemutasiwp');
        // PEMBETULAN SPPT
        Route::post('save-pembetulan', 'storePembetulanSppt')->name('pelayanan.save-pembetulan');
        // PEMBATALAN SPPT
        Route::post('save-pembatalan', 'storePembatalanSppt')->name('pelayanan.save-pembatalan');
        // KEBERATAN PAJAK
        Route::post('save-keberatan-pajak', 'storeKeberatanPajak')->name('pelayanan.save-keberatan');
        // SALINAN SPPT
        Route::post('save-salinan-sppt', 'storeSalinanSppt')->name('pelayanan.save-salinan-sppt');
        // PENGURANGAN KETETAPAN
        Route::post('save-pengurangan-ketetapan', 'storePenguranganKetetapan')->name('pelayanan.save-pengurangan-ketetapan');
        // PENENTUAN JATUH TEMPO
        Route::post('save-penentuan-jatuh-tempo', 'storePenentuanJatuhTempo')->name('pelayanan.save-penentuan-jatuh-tempo');
        // MUTASI PECAH
        Route::post('save-mutasi-pecah', 'storeMutasiPecah')->name('pelayanan.save-mutasi-pecah');
        // MUTASI GABUNG
        Route::post('save-mutasi-gabung', 'storeMutasiGabung')->name('pelayanan.save-mutasi-gabung');

        Route::post('delete', 'delete')->name('pelayanan.delete');

        Route::get('upload/{pelayanan:uuid}', 'uploadPersyaratan')->name('pelayanan.upload-persyaratan');
        // Route::get('upload-file/{pelayanan}', 'getUploadPersyaratan')->name('pelayanan.upload-file-persyaratan');
        Route::get('lengkapsyarat/{pelayanan}', 'cekLengkapSyarat')->name('pelayanan.cek-lengkap-syarat');
        Route::post('upload-file/multiple', 'uploadMultiFile')->name('pelayanan.upload-multi-file-persyaratan');
        Route::post('upload-file/cekuser', 'cekUser')->name('pelayanan.cekuser');
        Route::post('delete-upload-file', 'deleteUploadFile')->name('pelayanan.upload-delete-file');

        Route::post('store', 'store')->name('pelayanan.store');
        Route::get('edit/{pelayanan:uuid}', 'edit')->name('pelayanan.edit');
        Route::post('combo-jenis-pelayanan/{idJenisPajak}', 'comboJenisPelayanan');
        Route::post('combo-kelurahan', 'comboKelurahan');
        Route::get('check-nik', 'checkNik');
        Route::get('pengajuan/{pelayanan:uuid}', 'pengajuan');
        Route::post('ajukan', 'ajukanPelayanan');
        Route::delete('delete/{pelayanan}', 'destroy');

        // Route::get('tambah/{jenisPajak}/{jenisPelayanan}/data-objek-pajak/bangunan/{nop}', 'dataObjekBangunan');
        Route::get('tambah/{jenisPajak}/{jenisPelayanan}/get-data-pbb', 'getDataPbb');
        Route::get('tambah/{jenisPajak}/{jenisPelayanan}/get-data-sppt', 'getDataSppt');
        Route::get('edit/{jenisPelayanan}/get-data-pbb', 'getDataPbb');
        Route::get('edit/{jenisPelayanan}/get-data-sppt', 'getDataSppt');
        Route::get('getDataSppt', 'getDataSppt');
        Route::get('cek-data-mutasi', 'cekDataMutas');

        // Cek Pengajuan sebelumnay
        Route::post('pengajuan-sebelumnya-opbaru', 'cekPelayananSebelumnyaOpBaru')->name('pelayanan.pengajuan-sebelumnya-opbaru');
        // Cetakan
        Route::get('{pelayanan}/cetak-tanda-terima', 'cetakTandaTerima');
        Route::get('cetak-daftar-pelayanan', 'cetakDaftarPelayanan');
        Route::get('cetak-spop/{pelayanan:uuid}', 'cetakSpop');
        Route::get('sppt/{pelayanan:uuid}', 'sppt');
    });

    Route::get('pelayanan/upload-file/{pelayanan}', [PelayananController::class, 'getUploadPersyaratan'])->name('pelayanan.upload-file-persyaratan');
    Route::get('pelayanan/tambah/{jenisPajak}/{jenisPelayanan}/get-data-pbb', [PelayananController::class, 'getDataPbb']);
    Route::get('pelayanan/tambah/{jenisPajak}/{jenisPelayanan}/get-data-sppt', [PelayananController::class, 'getDataSppt']);
    Route::get('pelayanan/edit/{jenisPelayanan}/get-data-pbb', [PelayananController::class, 'getDataPbb']);
    Route::get('pelayanan/edit/{jenisPelayanan}/get-data-sppt', [PelayananController::class, 'getDataSppt']);
    Route::get('pelayanan/getDataSppt', [PelayananController::class, 'getDataSppt']);
    Route::get('pelayanan/cek-data-mutasi', [PelayananController::class, 'cekDataMutas']);
    Route::post('pelayanan/cektunggakanpbb', [PelayananController::class, 'cektunggakanpbb']);

    Route::prefix('dat-objek-pajak')->controller(DatObjekPajakController::class)->group(function () {
        Route::get('', 'getObjekPajak');
        Route::get('bangunan', 'getBangunan');
        Route::get('bangunan/{nop}', 'getBangunan');
    });

    Route::prefix('tracking')->middleware('permission:TrackingPelayanan')->controller(PelacakanDokumenController::class)->group(function () {
        Route::get('', 'index');
        Route::post('datagrid', 'datagrid');
        Route::get('lihat/{pelayanan:uuid}', 'lihat');
        Route::get('syarat-upload/{idPelayanan}', 'syaratUpload');
    });

    Route::prefix('verifikasi')->middleware('permission:Verfikasi')->controller(VerifikasiController::class)->group(function () {
        Route::get('', 'index');
        Route::post('datagrid-belum-proses', 'datagridBelumProses');
        Route::post('datagrid-perbaikan', 'datagirdPerbaikan');
        Route::post('datagrid-ditolak', 'datagirdDitolak');
        Route::post('datagrid-diterima', 'datagirdDiterima');
        Route::post('datagrid-diajukan-validasi', 'datagirdDiajukanValidasi');
        Route::post('datagrid-semua', 'datagirdSemuaData');

        Route::get('tambah/{pelayanan:uuid}', 'create');
        Route::post('save', 'save')->name('verifikasi.save');
        Route::get('edit/{pelayanan:uuid}', 'edit');
        Route::post('update', 'update')->name('verifikasi.update');
        Route::get('pengajuan-validator/{pelayanan:uuid}', 'pengajuanValidator');
        Route::post('ajukan', 'ajukan');
        Route::get('checked-upload/{pelayanan:uuid}', 'checkedUpload');
        // CETAKAN
        Route::get('cetak-verifikasi-belum-proses', 'cetakVerifikasiBelumProses');
        Route::get('cetak-verifikasi-ditolak', 'cetakVerifikasiDitolak');
        Route::get('cetak-verifikasi-diterima', 'cetakVerifikasiDiterima');
        Route::get('cetak-verifikasi-diajukan', 'cetakVerifikasiDiajukan');
        Route::get('cetak-verifikasi-semua-data', 'cetakVerifikasiSemuaData');
    });

    Route::prefix('verifikasi-lapangan')->middleware('permission:VerifiaksiLapangan')->controller(VerifikasiLapanganController::class)->group(function () {
        Route::get('', 'index');
        Route::get('tambah/{pelayanan:uuid}', 'create');
        Route::post('save', 'store');

        Route::post('datagrid-belum-proses', 'datagridBelumProses');
        Route::post('datagrid-ditolak', 'datagirdDitolak');
        Route::post('datagrid-sudah', 'datagirdDiterima');
        Route::post('datagrid-semua', 'datagirdSemuaData');

        Route::get('/cari-data-znt/{nop}', 'findDataZnt');
        Route::get('/cari-kelas-tanah/{nop}/{kd_znt}/{luas_tanah}', 'getKelasBumi');
        // Route::post('/cari-kelas-tanah', 'getKelasBumi');

        // CETAKAN
        Route::get('cetak-verlap-belum-proses', 'cetakVerlapBelum');
        Route::get('cetak-verlap-ditolak', 'cetakVerlapDitolak');
        Route::get('cetak-verlap-sudah', 'cetakVerlapSudah');
        Route::get('cetak-verlap-semua', 'cetakVerlapSemua');
    });

    Route::prefix('validasi')->middleware('permission:Validasi')->controller(ValidasiController::class)->group(function () {
        Route::get('', 'index');
        Route::get('tambah/{pelayanan}', 'create');
        Route::post('save', 'store');
        Route::get('edit/{pelayanan}', 'edit');

        Route::post('datagrid-belum-proses', 'datagridBelumProses');
        Route::post('datagrid-diajukan-verlap', 'datagridDiajukanVerlap');
        Route::post('datagrid-sudah-verlap', 'datagridSudahVerlap');
        Route::post('datagrid-diterima', 'datagridDiterima');
        Route::post('datagrid-ditolak', 'datagridDitolak');
        Route::post('datagrid-validasi', 'datagrid');

        Route::get('cetak-hasil-validasi/{pelayanan}/{ttd}', 'cetakHasilValidasi');

        Route::get('data-pengajuan/{pelayanan:uuid}', 'getDataPelayanan');
        Route::post('batal-verlap', 'batalDitolakDanPengajuanVerlap');
        Route::post('batal-ditolak', 'batalDitolakDanPengajuanVerlap');
    });
    Route::get('validasi/upload-file/{pelayanan}', [ValidasiController::class, 'getUploadFilePersyaratan']);

    Route::prefix('validasi-final')->middleware('permission:ValidasiFinal')->controller(ValidasiFinalController::class)->group(function () {
        Route::get('', 'index');
        Route::post('datagrid-belum', 'datagridBelum');
    });

    Route::prefix('hasil-pelayanan')->middleware('permission:HasilPelayanan')->controller(HasilPelayananController::class)->group(function () {
        Route::get('', 'index');
        Route::get('tambah/{pelayanan}', 'create');
        Route::post('save', 'store');

        Route::post('datagrid-belum', 'datagridBelum');
        Route::post('datagrid-sudah', 'datagridSudah');
        Route::post('datagrid-semua', 'datagridSemua');

        Route::get('cetak-daftar-hasil-pelayanan', 'cetakDaftarHasilPelayanan');
    });
    Route::get('hasil-pelayanan/cetak-sk-pengurangan/{pelayanan:uuid}', [HasilPelayananController::class, 'cetakSkPenguranagan']);

    Route::prefix('penetapan')->middleware('permission:HasilPelayanan')->controller(PenetapanController::class)->group(function () {
        Route::get('', 'index');
        Route::post('objekPajak', 'objekPajak')->name('objekPajak');
        Route::post('terseleksi', 'terseleksi')->name('terseleksi');
        Route::post('datagrid-belum', 'datagridBelum');
        Route::post('rinciSppt', 'rinciSppt')->name('rinciSppt');
        Route::post('datagrid', 'datagridSudah');
    });

    Route::prefix('esign-tte')->middleware('permission:Penandatangan')->controller(EsignController::class)->group(function () {
        Route::get('', 'index');
        Route::post('create', 'create');
        Route::post('datagrid-belum', 'datagridBelum');
        Route::post('datagrid-sudah', 'datagridSudah');
    });

    Route::prefix('dokumen')->middleware('permission:DokumenTte')->controller(DokumenController::class)->group(function () {
        Route::get('download/{dokumen?}', 'download');
    });

    Route::prefix('setting-pemda')->middleware('permission:SettingPemda')->controller(PemdaController::class)->group(function () {
        Route::get('/', 'index')->name('setting-pemda');
        Route::post('/update/{pemda}', 'update')->name('setting-pemda.update');
    });

    Route::prefix('setting-pegawai')->middleware('permission:SettingPemda')->controller(PegawaiController::class)->group(function () {
        Route::get('/', 'index')->name('setting-pegawai');
        Route::post('datagrid', 'datagrid');
        Route::get('tambah', 'create')->name('setting-pegawai.tambah');
        Route::post('store', 'store')->name('setting-pegawai.store');
        Route::get('edit/{pegawai}', 'edit')->name('setting-pegawai.edit');
        Route::put('update/{pegawai}', 'update')->name('setting-pegawai.update');
        Route::delete('delete/{pegawai}', 'destroy');
    });

    Route::prefix('setting-jenis-pajak')->middleware('permission:SettingJenisPajak')->controller(JenisPajakController::class)->group(function () {
        Route::get('/', 'index')->name('setting-jenis-pajak');
        Route::post('datagrid', 'datagrid');
        Route::get('edit/{jenisPajak}', 'edit')->name('setting-jenis-pajak.edit');
        Route::put('update/{jenisPajak}', 'update')->name('setting-jenis-pajak.update');
    });

    Route::prefix('setting-jenis-pelayanan')->middleware('permission:SettingJenisPelayanan')->controller(JenisPelayananController::class)->group(function () {
        Route::get('/', 'index')->name('setting-jenis-pelayanan');
        Route::post('datagrid', 'datagrid');
        Route::get('tambah', 'create')->name('setting-jenis-pelayanan.tambah');
        Route::post('store', 'store')->name('setting-jenis-pelayanan.store');
        Route::get('edit/{jenisPelayanan}', 'edit')->name('setting-jenis-pelayanan.edit');
        Route::put('update/{jenisPelayanan}', 'update')->name('setting-jenis-pelayanan.update');
        Route::delete('delete/{jenisPelayanan}', 'destroy');
    });

    Route::prefix('setting-jenis-hasil-pelayanan')->middleware('permission:SettingHasilPelayanan')->controller(JenisHasilPelayananController::class)->group(function () {
        Route::get('/', 'index')->name('setting-jenis-hasil-pelayanan');
        Route::post('datagrid', 'datagrid');
        Route::get('tambah', 'create')->name('setting-jenis-hasil-pelayanan.tambah');
        Route::post('store', 'store')->name('setting-jenis-hasil-pelayanan.store');
        Route::get('edit/{jenisHasilPelayanan}', 'edit')->name('setting-jenis-hasil-pelayanan.edit');
        Route::put('update/{jenisHasilPelayanan}', 'update')->name('setting-jenis-hasil-pelayanan.update');
        Route::post('combo-jenis-pelayanan/{idJenisPajak}', 'comboJenisPelayanan');
        Route::delete('delete/{jenisHasilPelayanan}', 'destroy');
    });

    Route::prefix('setting-persyaratan')->middleware('permission:SettingPersyaratan')->controller(PersyaratanController::class)->group(function () {
        Route::get('/', 'index')->name('setting-persyaratan');
        Route::post('datagrid', 'datagrid');
        Route::get('tambah', 'create')->name('setting-persyaratan.tambah');
        Route::post('store', 'store')->name('setting-persyaratan.store');
        Route::get('edit/{persyaratan}', 'edit')->name('setting-persyaratan.edit');
        Route::put('update/{persyaratan}', 'update')->name('setting-persyaratan.update');
        Route::post('combo-jenis-pelayanan/{idJenisPajak}', 'comboJenisPelayanan');
        Route::delete('delete/{persyaratan}', 'destroy');
    });

    Route::prefix('setting-dokumen-pendukung')->middleware('permission:SettingDokumenPendukung')->controller(DokumenPendukungController::class)->group(function () {
        Route::get('/', 'index')->name('setting-dokumen-pendukung');
        Route::post('datagrid', 'datagrid');
        Route::get('tambah', 'create')->name('setting-dokumen-pendukung.tambah');
        Route::post('store', 'store')->name('setting-dokumen-pendukung.store');
        Route::get('edit/{dokumenPendukung}', 'edit')->name('setting-dokumen-pendukung.edit');
        Route::put('update/{dokumenPendukung}', 'update')->name('setting-dokumen-pendukung.update');
        Route::post('combo-jenis-pelayanan/{idJenisPajak}', 'comboJenisPelayanan');
        Route::delete('delete/{dokumenPendukung}', 'destroy');
    });

    Route::prefix('setting-user')->middleware('permission:SettingPengguna')->controller(UserController::class)->group(function () {
        Route::get('/', 'index')->name('setting-user');
        Route::post('datagrid', 'datagrid');
        Route::get('tambah', 'create')->name('setting-user.tambah');
        Route::post('save', 'store')->name('setting-user.store');
        Route::get('{user}/edit', 'edit')->name('setting-user.edit');
        Route::put('{user}/update', 'store')->name('setting-user.update');
        Route::delete('{user}/delete', 'destroy');
    });

    Route::prefix('setting-alur-pelayanan')->middleware('permission:SettingAlurPelayanan')->controller(AlurPelayananController::class)->group(function () {
        Route::get('/', 'index')->name('setting-alur-pelayanan');
        Route::post('datagrid', 'datagrid');
        Route::get('tambah', 'create')->name('setting-alur-pelayanan.tambah');
        Route::post('save', 'store')->name('setting-alur-pelayanan.store');
        Route::get('{alurPelayanan}/edit', 'edit')->name('setting-alur-pelayanan.edit');
        Route::delete('delete/{alurPelayanan}', 'destroy');
    });

    Route::prefix('setting-faq')->middleware('permission:SettingFaq')->controller(FaqController::class)->group(function () {
        Route::get('/', 'index')->name('setting-faq');
        Route::post('datagrid', 'datagrid');
        Route::get('tambah', 'create')->name('setting-faq.tambah');
        Route::post('save', 'store')->name('setting-faq.store');
        Route::get('{faq}/edit', 'edit')->name('setting-faq.edit');
        Route::delete('delete/{faq}', 'destroy');
    });

    Route::prefix('setting-user-manual')->middleware('permission:SettingUserManual')->controller(UserManualController::class)->group(function () {
        Route::get('/', 'index')->name('setting-user-manual');
        Route::post('datagrid', 'datagrid');
        Route::get('tambah', 'create')->name('setting-user-manual.tambah');
        Route::post('save', 'store')->name('setting-user-manual.store');
        Route::get('{userManual}/edit', 'edit')->name('setting-user-manual.edit');
        Route::delete('delete/{userManual}', 'destroy');
    });

    Route::prefix('setting-about')->middleware('permission:SettingAbout')->controller(AboutController::class)->group(function () {
        Route::get('/', 'index')->name('setting-about');
        Route::post('save', 'store')->name('setting-about.save');
    });

    Route::prefix('kecamatan')->group(function () {
        Route::get('getkecamatan-all', [KecamatanController::class, 'getKecamatan'])->name('kecamatan.getkecamatan-all');
        Route::get('getkecamatan-byid/{kecamatanId}', [KecamatanController::class, 'getKecamatanById'])->name('kecamatan.getkecamatan-byid');
    });

    Route::prefix('kelurahan')->group(function () {
        Route::get('getkelurahan-bykecamatan/{kecamatanId}', [KelurahanController::class, 'getKelurahanByKecamatan'])->name('kelurahan-by-kecamatanid');
        Route::get('getkelurahan-byid/{kelurahanId}', [KelurahanController::class, 'getKelurahanById'])->name('kelurahan-byid');
    });

    Route::prefix('setting-roles')->middleware('permission:RolePermission')->group(function () {
        Route::get('', [RolesController::class, 'index']);
        Route::get('{role}', [RolesController::class, 'getRole']);
        Route::post('sync-permit', [RolesController::class, 'syncPermission'])->name('setting-roles.sync-permit');
    });

    Route::prefix('notifikasi')->group(function () {
        Route::get('', [NotifikasiController::class, 'getNotifikasi']);
        Route::post('mark-as-read', [NotifikasiController::class, 'markAsRead']);
    });

    Route::prefix('ganti-password')->group(function () {
        Route::get('', [UserController::class, 'changePassword']);
        Route::post('save', [UserController::class, 'savePassword']);
    });
});

Route::get('logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index'])->middleware(['auth']);
Route::get('/artisan-link', function () {
    Artisan::call('storage:link');

    return 'Selesai';
});

Route::get('test-query', function () {
    // $users = \App\Models\User::role(['Wajib Pajak', 'Validator'])->get();
    $urut = \App\Models\Pbb\MaxUrutPst::getMaxNoUrutPst();

    return $urut;
});
